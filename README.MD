# Auzmor HCM PR Builder Test 2

# Pre-requisites
* jdk8
* mvn
* mysql(5.7)
* postgresql (> 9.2)
* [elasticsearch](https://www.elastic.co/) 
* [kong](https://getkong.org/)

# Build
1. mvn clean package
2. convert the json template in src/main/resources/conf/ to json

# Run
1. java -jar target/auzmor-1.0-SNAPSHOT-fat.jar

# With Nginx for localhost (Intermediate kong server)
1. Update your kong.conf for postgresql
2. Perform kong miigrations (kong migrations up -c <destination_to_kong_conf>) `Note: Configure your kong.conf to work with postgresql`
3. Run Onetime job for kong setup (java -jar <path_to_project_target>/OneTimeJob-fat.jar <date_of_script>)
4. Start the kong (kong start -c <destination_to_kong_conf>)
5. Copy the template(nginx_local.conf.template) to your destination
6. Start the nginx with the command (nginx -c <destination_to_nginx_conf>)
7. add the subdomain on /etc/hosts on 127.0.2.1
    Examples:
    * 127.0.2.1 ats.auzmorhost.com
    * 127.0.2.1 xedflix.auzmorhost.com
    
    `Note: make sure the domain(i.e. auzmorhost.com) is not an actual domain on the internet`
8. Now go to the browser and type your [host](http://ats.auzmorhost.com)


# Integrate with frontend
1. Clone the [auzmor_frontend](https://bitbucket.org/auzmorhcm/auzmor_frontend_v2/) v2.
2. npm install
3. npm run devmode `Note: make sure the build gets complete`.
4. Done click [here](http://ats.auzmorhost.com)


