package com.auzmor;

import com.auzmor.impl.builder.DbBuilder;
import com.auzmor.impl.helper.ConfigurationJsonHelper;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.KongServiceHelper;
import com.auzmor.impl.onetimejob.OneTimeJobBaseVerticle;
import io.vertx.core.Context;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Verticle;
import io.vertx.core.Vertx;

import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class OneTimeJobExecutor {
  private static long MAX_TIME = 300000; // 5 Mins the max time for the verticle


  /**
   * The Executor of the one time job
   *
   * @param args 1 argument of the job date
   * @throws ClassNotFoundException
   */
  public static void main(String... args) throws ClassNotFoundException,
      FileNotFoundException, NoSuchMethodException, IllegalAccessException,
      InvocationTargetException, InstantiationException {
    String date;

    // If no args get the current data
    if (args.length == 1) {
      date = args[0];
    } else {
      date = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
    }

    // Getting the class
    Class executingClass = Class.forName("com.auzmor.impl.onetimejob.job.Job" + date,
        false, ClassLoader.getSystemClassLoader());

    // Creating and deploying the verticle with dbHandle and KongClient
    Vertx vertx = Vertx.vertx();
    Verticle verticle = (OneTimeJobBaseVerticle) executingClass.getConstructor().newInstance();
    vertx.deployVerticle(verticle,
        new DeploymentOptions().setWorker(true).setMaxWorkerExecuteTime(MAX_TIME).setConfig(ConfigurationJsonHelper
            .getConf("kong.json", "db.json", "db_secrets.json", "services.json")), asyncResult -> {
          if (asyncResult.succeeded()) {
            System.exit(0);
          } else if (asyncResult.failed()) {
            System.exit(42);
          }
        });
  }
}