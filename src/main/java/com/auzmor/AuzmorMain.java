package com.auzmor;

import com.auzmor.impl.helper.ConfigurationJsonHelper;
import com.auzmor.service.ATSServer;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;

/**
 * This is a verticle. A verticle is a _Vert.x component_. This verticle is implemented in Java, but you can
 * implement them in JavaScript, Groovy or even Ruby.
 */
public class AuzmorMain extends AbstractVerticle {

  private static final Logger logger = LoggerFactory.getLogger(AuzmorMain.class);

  public static void main(String... args) {
    // Logger initialization across the ATS
    /*System.setProperty("vertx.logger-delegate-factory-class-name",
        "io.vertx.core.logging.Log4j2LogDelegateFactory");*/
    //  public static void getStatesNameswithCountryId(RoutingContext routingContext)  {

    Vertx vertx = Vertx.vertx();
    try {
      vertx.deployVerticle(new ATSServer(), new DeploymentOptions()
          .setConfig(ConfigurationJsonHelper.getConf("ats.json", "db.json", "db_secrets.json",
              "secrets.json", "services.json")), atsAsyncResult -> {
        if (!atsAsyncResult.succeeded()) {
          logger.info("Deployment Failure");
        } else {
          logger.info("Deployed Successfully");
        }
      });
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } finally {
    }
  }

}
