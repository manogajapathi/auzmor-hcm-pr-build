package com.auzmor.util.icalendar.component;

import java.text.ParseException;

public interface ICalendarEvent {
  ICalendarEvent setStartDate(String startDate, String format) throws ParseException;
  ICalendarEvent setEndDate(String endDate, String format) throws ParseException;
  ICalendarEvent setStartTime(String startTime, String format) throws ParseException;
  ICalendarEvent setEndTime(String endTime, String format) throws ParseException;
  ICalendarEvent setSummary(String summary);
  ICalendarEvent setDescription(String description);
  ICalendarEvent setLocation(String location);
  ICalendarEvent setTzId(String tzId);
  ICalendarEvent setOrganizer(String organizer);
}
