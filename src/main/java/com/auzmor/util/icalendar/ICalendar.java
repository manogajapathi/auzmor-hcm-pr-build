package com.auzmor.util.icalendar;

import com.auzmor.util.icalendar.component.ICalendarEvent;
import com.auzmor.util.icalendar.component.ICalendarTimeZone;

public interface ICalendar {
  ICalendar addEvent(ICalendarEvent iCalendarEvent);
  ICalendar setTimeZone(ICalendarTimeZone iCalendarTimeZone);
}
