package com.auzmor.util.validator;

import java.util.regex.Pattern;

public interface FieldOptions {
	Pattern getExpectedPattern();
	boolean doAllowEmpty();
	boolean doHtmlEscape();
	FieldOptions setHtmlEscape(boolean htmlEscape);
	boolean doLengthCheck();
	long getMinLength();
	long getMaxLength();
}