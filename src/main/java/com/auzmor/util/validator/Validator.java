package com.auzmor.util.validator;

import java.util.List;
import java.util.Map;

public interface Validator {
  List<String> getParams();
  Map<String, FieldOptions> getValidationMap();
}