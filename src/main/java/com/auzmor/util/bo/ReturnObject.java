package com.auzmor.util.bo;

/**
 *
 * @param <E>
 * @param <T>
 *
 * @author Charles Sam Dilip
 * @lastModifiedDate 05/FEB/2018
 */
public interface ReturnObject<E, T> {
  E getStatusEnum();
  T getData();
}
