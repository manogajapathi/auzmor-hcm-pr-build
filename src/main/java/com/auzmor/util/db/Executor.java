package com.auzmor.util.db;

import com.auzmor.impl.exception.db.DbHandleException;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerResponse;import java.sql.SQLException;
import java.text.ParseException;

/**
 *
 * @author Charles Sam Dilip
 * @lastModifiedDate 05/FEB/2018
 */
public interface Executor {

  void method(HttpServerResponse response, MultiMap params,  DbHandle dbHandle)
      throws SQLException, DbHandleException, ParseException;
}

