package com.auzmor.util.db.serialier;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 *
 * @param <E>
 *
 * @author Charles Sam Dilip
 * @lastModifiedDate 05/FEB/2018
 */
public interface ResultSetSerializer<E> {
  E serialize(ResultSet resultSet) throws SQLException;
}
