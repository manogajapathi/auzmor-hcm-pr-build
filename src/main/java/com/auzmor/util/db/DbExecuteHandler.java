package com.auzmor.util.db;

import com.auzmor.impl.exception.db.DbHandleException;

import java.sql.SQLException;

public interface DbExecuteHandler<E> {
  void handle(E handler)
      throws SQLException, DbHandleException;
}
