package com.auzmor.service;

import com.auzmor.impl.helper.ServiceDiscoveryHelper;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.types.HttpEndpoint;

public class Kong extends AbstractVerticle {

  private static final Logger logger = LoggerFactory.getLogger(Kong.class);
  private String recordId;

  @Override
  public void start(Future<Void> fut) {
    ServiceDiscovery discovery = ServiceDiscoveryHelper.getDiscoveryObject(this.vertx);
    if (null == discovery) {
      logger.error("Unable to deploy Kong Service");
      fut.fail("Unable to deploy Kong Service");
    }
    Record record = HttpEndpoint.createRecord(config().getString("kong.name"),
        config().getString("kong.http.admin_host"), config().getInteger("kong.http.admin_port"),
        config().getString("kong.http.admin_root"));

    discovery.publish(record, ar -> {
      if (ar.succeeded() && null != ar.result()) {
        this.recordId = ar.result().getRegistration();
        fut.complete();
      } else {
        logger.error("Unable to deploy Kong Verticle");
        fut.fail("Unable to start Kong service");
      }
    });
    discovery.close();
  }

  @Override
  public void stop(Future<Void> fut) {
    ServiceDiscovery discovery = ServiceDiscoveryHelper.getDiscoveryObject(this.vertx);
    if (null != discovery) {
      discovery.unpublish(this.recordId, ar -> {
        if (ar.succeeded()) {
          System.out.println("Unpublished");
          fut.complete();
        } else {
          fut.fail("Unable to deploy DbSource");
        }
      });
      discovery.close();
    } else {
      fut.complete();
    }
  }
}
