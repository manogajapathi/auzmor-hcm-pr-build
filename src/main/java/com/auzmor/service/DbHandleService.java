package com.auzmor.service;

import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.spi.ServiceType;

public interface DbHandleService extends ServiceType {
  String TYPE = "dbHandle";

  static Record createRecord(String name) {
    return new Record().setName(name).setType(TYPE);
  }
}
