package com.auzmor.service;

import com.auzmor.impl.builder.DbBuilder;
import com.auzmor.impl.helper.ServiceDiscoveryHelper;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscovery;

public class DataSource extends AbstractVerticle {

  private static final Logger logger = LoggerFactory.getLogger(DataSource.class);
  private String recordId;

  @Override
  public void start(Future<Void> fut) {
    DbBuilder.initializeAtsSqlDb();

    ServiceDiscovery discovery = ServiceDiscoveryHelper.getDiscoveryObject(this.vertx);
    if (null == discovery) {
      logger.error("Unable to deploy DbSource");
      fut.fail("Unable to deploy DbSource");
    }
    Record record = DbHandleService.createRecord("ats.mysql");
    discovery.publish(record, ar -> {
      if (ar.succeeded() && null != ar.result()) {
        this.recordId = ar.result().getRegistration();
        fut.complete();
      } else {
        fut.fail("Unable to deploy DbSource");
      }
    });
    discovery.close();
  }

  @Override
  public void stop(Future<Void> fut) {
    ServiceDiscovery discovery = ServiceDiscoveryHelper.getDiscoveryObject(this.vertx);
    if (null != discovery) {
      discovery.unpublish(this.recordId, ar -> {
        if (ar.succeeded()) {
          System.out.println("Unpublished");
          fut.complete();
        } else {
          fut.fail("Unable to deploy DbSource");
        }
      });
      discovery.close();
    } else {
      fut.complete();
    }
  }
}