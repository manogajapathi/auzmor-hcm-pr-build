package com.auzmor.bo;

import com.auzmor.impl.enumarator.TitleEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.TitleModel;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;

public interface TitleBO {
  ReturnObject<TitleEnum, JsonObject> addTitle(DbHandle dbHandle, TitleModel titleModel)
      throws SQLException,DbHandleException;
  ReturnObject<TitleEnum, JsonObject> getTitle(DbHandle dbHandle, TitleModel titleModel)
      throws SQLException,DbHandleException;
  ReturnObject<TitleEnum, JsonObject> updateTitle(DbHandle dbHandle, MultiMap requestParams)
      throws SQLException,DbHandleException;
  ReturnObject<TitleEnum, JsonObject> deleteTitle(DbHandle dbHandle, TitleModel titleModel)
      throws SQLException,DbHandleException;
  ReturnObject<TitleEnum, JsonArray> getAllTitles(DbHandle dbHandle, TitleModel titleModel)
      throws SQLException,DbHandleException;
}
