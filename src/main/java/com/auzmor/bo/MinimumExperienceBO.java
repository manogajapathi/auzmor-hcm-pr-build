package com.auzmor.bo;


import com.auzmor.impl.enumarator.bo.MinimumExperienceEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;

public interface MinimumExperienceBO {
  ReturnObject<MinimumExperienceEnum, JsonObject> addMinimumExperience(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
  ReturnObject<MinimumExperienceEnum, JsonObject> getMinimumExperience(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
  ReturnObject<MinimumExperienceEnum, JsonArray> getAllMinimumExperience(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
  ReturnObject<MinimumExperienceEnum, JsonObject> updateMinimumExperience(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
  ReturnObject<MinimumExperienceEnum, JsonObject> deleteMinimumExperience(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
}
