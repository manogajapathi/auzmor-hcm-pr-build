package com.auzmor.bo;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.employee.Employee;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.web.RoutingContext;

import io.vertx.core.json.JsonObject;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public interface EmployeeBO {
  Employee addNewEmployee(MultiMap requestParams)
      throws SQLException, DbHandleException;
  boolean isEmployeeExists(String email, long organizationId)
      throws SQLException, DbHandleException;
  JsonArray getAllEmployees(MultiMap requestParams)
      throws SQLException, DbHandleException;
  Employee getEmployeeDetails(long employeeId) throws SQLException,DbHandleException;
  Employee getEmployeeDetailsByLoginId(long loginId) throws SQLException, DbHandleException;
  int deleteUser(MultiMap requestParams) throws  SQLException,DbHandleException;
  JsonArray getAllUsersList(MultiMap requestParams)
      throws SQLException, DbHandleException;
  int editUser(Vertx vertx, MultiMap requestParams)
      throws  SQLException,DbHandleException;
  int addNewUser(MultiMap requestParams)
     throws  SQLException,DbHandleException;
  int uploadProfileImage(String imagePath, long organizationId, int employeeId)
      throws SQLException, DbHandleException;
  List<HashMap<String,String>> getEmployeeEmailIds(long collaboratorId, long adminId) throws SQLException, DbHandleException;
//    JsonArray getEmployee(long employeeId)
//            throws SQLException, DbHandleException;
  JsonObject getDashBoardCount(MultiMap requestParams) throws SQLException, DbHandleException;
}

