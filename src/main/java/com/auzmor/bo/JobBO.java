package com.auzmor.bo;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

import com.auzmor.impl.enumarator.bo.JobBOEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public interface JobBO {
  void moveCandidatesToTalentPool(int jobId, int creatorId , String organizationName)
      throws SQLException, DbHandleException;

  JsonObject getJobHiringDetails(long candidateId)
      throws SQLException,DbHandleException;

  Job getJobDetails(long jobId) throws SQLException,DbHandleException;

  Job getJobDetailsWithoutStatus(long jobId) throws SQLException,DbHandleException;

  JsonArray getJobStatus(String status,long jobId,long organizationId)
                                                throws SQLException,DbHandleException;

  JsonArray getAvailableJobStatus(String constraintString) throws SQLException;

  long getJobCount(long organizationId) throws SQLException, DbHandleException;

  List<String> updateCollaborators(int id, String collaboratorParam)
      throws DecodeException, SQLException, DbHandleException;

  List<HashMap<String,String>> getJobWithCandidateLoginId(long candidateLoginId,long organizationId)
      throws SQLException,DbHandleException;

  boolean doesJobPostExists(Job job) throws SQLException, DbHandleException;

  boolean validateJobStatus(int id, String jobStatus) throws SQLException, DbHandleException;

  ReturnObject<JobBOEnum, JsonObject> getCollaboratorList(Job job)
      throws SQLException, DbHandleException;

  boolean isHiringLeadValid(int organizationId, String hiringLeadId)
      throws SQLException, DbHandleException;

  void notifyNewHiringLeadViaEmail(int hiringLead, int jobId) throws SQLException, DbHandleException;

  void notifyNewCollaboratorsViaEmail(String collaboratorID, int jobId)
      throws SQLException, DbHandleException;

  JsonArray getJobDetailsForEmployee(long employeeId, String format)
      throws SQLException, DbHandleException, ParseException;

  ReturnObject<JobBOEnum, JsonObject> cloneJob(DbHandle dbHandle, Job job)
      throws SQLException,DbHandleException;

}
