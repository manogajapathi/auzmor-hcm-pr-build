package com.auzmor.bo;

import com.auzmor.impl.enumarator.OnboardingWelcomeEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.onboarding.OnboardingPacket;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.RoutingContext;

import java.sql.SQLException;
import java.util.Set;

public interface OnboardingWelcomeBO {
  ReturnObject<OnboardingWelcomeEnum, JsonObject> updateWelcomeScreen(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
  ReturnObject<OnboardingWelcomeEnum, JsonObject> uploadVideo(DbHandle dbHandle,
                                                              Set<FileUpload> fileUploadSet,
                                                              MultiMap params)
      throws SQLException, DbHandleException;
  ReturnObject<OnboardingWelcomeEnum, JsonObject> getWelcomeDetails(DbHandle dbHandle,
                                                                    OnboardingPacket onboardingPacket)
      throws SQLException, DbHandleException;
  ReturnObject<OnboardingWelcomeEnum, JsonObject> appMapAddress(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
}
