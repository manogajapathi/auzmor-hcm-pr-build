package com.auzmor.bo;

import com.auzmor.impl.enumarator.bo.HighestEducationEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;


import java.sql.SQLException;

public interface HighestEducationBO {
  ReturnObject<HighestEducationEnum, JsonObject> addHighestEducation(DbHandle dbHandle, MultiMap params)
    throws SQLException, DbHandleException;
  ReturnObject<HighestEducationEnum, JsonObject> getHighestEducation(DbHandle dbHandle, MultiMap params)
    throws SQLException, DbHandleException;
  ReturnObject<HighestEducationEnum, JsonArray> getAllHighestEducation(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
  ReturnObject<HighestEducationEnum, JsonObject> updateHighestEducation(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
  ReturnObject<HighestEducationEnum, JsonObject> deleteHighestEducation(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
}
