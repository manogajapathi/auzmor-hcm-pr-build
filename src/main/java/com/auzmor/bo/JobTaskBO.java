package com.auzmor.bo;

import com.auzmor.impl.enumarator.bo.JobTaskBOEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.Job.JobTaskModel;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public interface JobTaskBO {
  ReturnObject<JobTaskBOEnum, JsonObject> addJobTask(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException, ParseException;
  ReturnObject<JobTaskBOEnum, JsonArray> getTasksForEmployee(DbHandle dbHandle,  long employeeId)
      throws SQLException, DbHandleException, ParseException;
  ReturnObject<JobTaskBOEnum, JsonObject> completeTask(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
  List<Long> getTaskIdByEmployeeId(DbHandle dbHandle, long employeeId) throws SQLException,
      DbHandleException;
}
