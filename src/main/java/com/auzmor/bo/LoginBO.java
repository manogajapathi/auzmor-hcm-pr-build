package com.auzmor.bo;

import com.auzmor.impl.enumarator.bo.LoginBOEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.Login;
import com.auzmor.util.bo.ReturnObject;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonObject;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

/**
 * @author Charles Sam Dilip
 * @lastModifiedDate 05/FEB/2018
 */
public interface LoginBO {
  long addNewUser(MultiMap requestParams) throws SQLException, DbHandleException;

  int addNewUser(Login login) throws SQLException, DbHandleException;


  void updateConsumerId(String consumerId, long loginId) throws SQLException,
      DbHandleException;

  boolean isUserExists(String email, long organizationId) throws SQLException, DbHandleException;

  LoginBOEnum changePassword(MultiMap requestParams) throws SQLException, DbHandleException;

  LoginBOEnum approveEmployeeSignup(MultiMap params) throws SQLException, DbHandleException;

  Login validateCredentialsAndGetDetails(String email, String password, long organizationId)
      throws SQLException, DbHandleException;

  LoginBOEnum resetPassword(MultiMap params)
      throws SQLException, DbHandleException, UnsupportedEncodingException;

  ReturnObject<LoginBOEnum, JsonObject> checkResetMail(MultiMap params)
      throws SQLException, DbHandleException, UnsupportedEncodingException;

  ReturnObject<LoginBOEnum, JsonObject> changePasswordForReset(MultiMap params)
      throws SQLException, DbHandleException;

  Login getLoginById(long organizationId) throws SQLException, DbHandleException;

  boolean candidateLoginCheck(Login login) throws SQLException, DbHandleException;

  List<HashMap<String, String>> GetAdminList(Long organization_id) throws SQLException, DbHandleException;

}