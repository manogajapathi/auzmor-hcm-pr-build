package com.auzmor.bo;

import com.auzmor.impl.enumarator.bo.CandidateStatusEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.models.CandidateR.CandidateCustomQuestionR;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface CandidateBO {

  CandidateStatusEnum createCandidate(RoutingContext routingContext, CandidateR candidateModel)
      throws SQLException, DbHandleException;

  CandidateR getCandidateId(DbHandle dbHandle, long candidateId, long organizationId)
      throws SQLException, DbHandleException;

  CandidateR getCandidate(DbHandle dbHandle, long candidateId)
      throws SQLException, DbHandleException;

  // CandidateR getCandidateByLoginId(long loginId) throws SQLException, DbHandleException;

  List<HashMap<String, String>> getUserId(DbHandle dbHandle, long userId, long organizationId)
      throws SQLException, DbHandleException;

  List<HashMap<String, String>> getJobId(DbHandle dbHandle, long jobId, long organizationId)
      throws SQLException, DbHandleException;

  CandidateStatusEnum deleteCandidate(long candidate)
      throws SQLException, DbHandleException;

  List<HashMap<String, String>> getPostJobFilterDetails(DbHandle dbHandle, long organizationId)
      throws SQLException, DbHandleException;

  CandidateStatusEnum updateRating(int rating, long candidateId, long userId)
      throws SQLException, DbHandleException;

  CandidateStatusEnum updateForwardEmployee(long employeeId, String notes, String id, long userId)
      throws SQLException, DbHandleException;

  CandidateStatusEnum updateMoveCandidate(long jobId, long candidateId, long userId)
      throws SQLException, DbHandleException;

  CandidateStatusEnum updateCandidate(String dateFormat, MultiMap multiMap)
      throws SQLException, DbHandleException;

  JsonObject getCandidateList(String dateFormat, MultiMap multiMap)
      throws SQLException, DbHandleException;

  int updateStatus(String status, long candidateId) throws SQLException, DbHandleException;

  List<HashMap<String, String>> getCandidateTalentList(long organizationId, String format, String searchFilter,
                                                       int limit, int offset,String stateId)
      throws SQLException, DbHandleException;

  ReturnObject<CandidateStatusEnum, JsonObject> getCandidateListBasedOnStatus(DbHandle dbHandle,
                                                                              CandidateR candidateModel,
                                                                              MultiMap requestParams)
      throws SQLException, DbHandleException;

  int getCandidatewithLoginId(long organizationId) throws SQLException, DbHandleException;

  void sendStatusUpdatesToCollaborator(String existingStatus, String newStatus,
                                       String subStatus, int candidateId)
      throws SQLException, DbHandleException;

  ReturnObject<CandidateStatusEnum, JsonArray> getAllResumes(DbHandle dbHandle, CandidateR candidateR)
      throws DbHandleException, SQLException;

  List<CandidateCustomQuestionR> getCustomQuestions(Map<String, String> customQuestionMap) throws DbHandleException;

  List<HashMap<String, String>> getCustomQuestionsList(long candidateId)
      throws SQLException, DbHandleException;

  void sendStatusUpdatesToHiringLead(String existingStatus, String requiredStatus,
                                     String subStatus, int candidateId)
      throws SQLException, DbHandleException;

  void sendStatusUpdatesToAdmin(String existingStatus, String requiredStatus,
                                String subStatus, long candidateId,long user_id)
      throws SQLException, DbHandleException;

}
