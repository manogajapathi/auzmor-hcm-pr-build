package com.auzmor.bo;

import com.auzmor.impl.enumarator.NewsFeedEnum;
import com.auzmor.util.bo.ReturnObject;
import io.vertx.core.json.JsonObject;
import org.elasticsearch.client.RestClient;

import java.io.IOException;

/**
 * @author Pradeep Balasubramanian
 * @lastModifiedDate 13-MAR-2018
 */
public interface NewsFeedBO {

  ReturnObject<NewsFeedEnum, JsonObject> getNewsFeedForAdmin(RestClient restClient,
                                                             int organizationId) throws IOException;

}
