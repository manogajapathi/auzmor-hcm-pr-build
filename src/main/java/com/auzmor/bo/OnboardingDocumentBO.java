package com.auzmor.bo;

import com.auzmor.impl.enumarator.bo.OnboardingDocumentEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;

public interface OnboardingDocumentBO {
  ReturnObject<OnboardingDocumentEnum, JsonObject> addDocument(DbHandle dbHandle, MultiMap params)
    throws SQLException, DbHandleException;
  ReturnObject<OnboardingDocumentEnum, JsonObject> deleteDocument(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
  ReturnObject<OnboardingDocumentEnum, JsonObject> updateDocument(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
}
