package com.auzmor.bo;

import com.auzmor.impl.enumarator.bo.CandidateInterviewEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.models.CandidateR.InterviewProcessCandidate;
import io.vertx.core.MultiMap;
import io.vertx.ext.web.RoutingContext;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

public interface CandidateInterviewBO {

  long addInterviewProcess(MultiMap paramsMap)
      throws SQLException, DbHandleException, ParseException;

  CandidateInterviewEnum updateInterviewProcess(MultiMap paramsMap,RoutingContext routingContext)
      throws SQLException, DbHandleException;

  InterviewProcessCandidate getInterviewDetails(long candidateId)
      throws SQLException,DbHandleException;

  List<HashMap<String,String>> getListOfInterviewDetails(String dateFormat, String fieldList,
                                                         long organizationId, int limit, int offset, long collaboratorId)
      throws SQLException,DbHandleException;

  List<HashMap<String,String>> getInterviewMailDetails(long candidateId)
      throws SQLException,DbHandleException;
}
