package com.auzmor.bo;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.Role;

import java.sql.SQLException;

public interface RoleBO {
  public Role getByName(String name, long organizationId) throws SQLException, DbHandleException;
}
