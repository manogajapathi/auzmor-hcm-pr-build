package com.auzmor.bo;

import com.auzmor.impl.enumarator.LocationEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.LocationModel;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;

  public interface LocationBO {
    ReturnObject<LocationEnum, JsonObject> addLocation(DbHandle dbHandle, LocationModel locationModel)
        throws SQLException,DbHandleException;
    ReturnObject<LocationEnum, JsonObject> getLocation(DbHandle dbHandle, LocationModel locationModel)
        throws SQLException,DbHandleException;
    ReturnObject<LocationEnum, JsonObject> updateLocation(DbHandle dbHandle, MultiMap requestParams)
        throws SQLException,DbHandleException;
    ReturnObject<LocationEnum, JsonObject> deleteLocation(DbHandle dbHandle, LocationModel locationModel)
        throws SQLException,DbHandleException;
    ReturnObject<LocationEnum, JsonArray> getAllLocations(DbHandle dbHandle, LocationModel locationModel)
        throws SQLException,DbHandleException;
  }