package com.auzmor.bo;

import com.auzmor.impl.enumarator.bo.OrganizationBOEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.OrganizationR;

import java.sql.SQLException;
import java.util.Map;

public interface OrganizationBO {
  Organization getOrganizationDetails(Long organizationId) throws SQLException, DbHandleException;

  Organization getOrganizationWithDomainName(String domain) throws SQLException, DbHandleException;

  boolean isDomainAvailable(String domain) throws SQLException, DbHandleException;

  void addNewAtsDomain(String atsDomain, long organizationId);

  OrganizationBOEnum updateDomains(String atsDomain, String careerDomain,
                                   long organizationId, String auzmorKeyFile, String auzmorCrtFile,
                                   String careerKeyFile, String careerCrtFile)
      throws SQLException, DbHandleException;

  OrganizationBOEnum addOrganizationDetails(OrganizationR organization) throws SQLException, DbHandleException;

  Map<Long, String> getAllRolesAsMap(long organizationId) throws SQLException, DbHandleException;

}
