package com.auzmor.bo;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.EmailTemplate;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;
import java.util.List;

public interface EmailTemplateBO {
  long addTemplate(String name, String subject, String header, String footer,
                   String template, long organizationId) throws SQLException, DbHandleException;
  Boolean editTemplate(String name, String subject, String header, String footer,
                       String template, long emailTemplateId) throws SQLException, DbHandleException;
  Boolean deleteTemplate(long emailTemplateId) throws SQLException, DbHandleException;
  JsonObject getTemplate(long emailTemplateId) throws SQLException, DbHandleException;
  JsonArray getTemplates(long organizationId) throws SQLException, DbHandleException;
  List<String> getTemplateVariables();
  JsonArray generateEmails(List<Long> candidateIds, long templateId) throws SQLException,
      DbHandleException;
  void sendEmails(List<Long> candidateIds, long templateId) throws SQLException,
      DbHandleException;
}
