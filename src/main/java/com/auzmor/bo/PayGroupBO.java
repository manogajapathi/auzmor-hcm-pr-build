package com.auzmor.bo;

import com.auzmor.impl.enumarator.PayGroupEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.PayGroupModel;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;

public interface PayGroupBO {
  ReturnObject<PayGroupEnum, JsonObject> addPayGroup(DbHandle dbHandle,
                                                     PayGroupModel payGroupModel)
      throws SQLException,DbHandleException;
  ReturnObject<PayGroupEnum, JsonObject> getPayGroup(DbHandle dbHandle,
                                                     PayGroupModel payGroupModel)
      throws SQLException,DbHandleException;
  ReturnObject<PayGroupEnum, JsonObject> updatePayGroup(DbHandle dbHandle,
                                                        MultiMap requestParams)
      throws SQLException,DbHandleException;
  ReturnObject<PayGroupEnum, JsonObject> deletePayGroup(DbHandle dbHandle,
                                                        PayGroupModel payGroupModel)
      throws SQLException,DbHandleException;
  ReturnObject<PayGroupEnum, JsonArray> getAllPayGroups(DbHandle dbHandle,
                                                        PayGroupModel payGroupModel)
      throws SQLException,DbHandleException;
}
