package com.auzmor.bo;

import com.auzmor.impl.enumarator.bo.OnboardingFormEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;


public interface OnboardingFormBO {
  ReturnObject<OnboardingFormEnum, JsonObject> addForm(DbHandle dbHandle, MultiMap params)
    throws SQLException, DbHandleException;
  ReturnObject<OnboardingFormEnum, JsonObject> deleteForm(DbHandle dbHandle, MultiMap params)
    throws SQLException, DbHandleException;
  ReturnObject<OnboardingFormEnum, JsonObject> updateForm(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
  ReturnObject<OnboardingFormEnum, JsonObject> getAuzmorForm(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
}
