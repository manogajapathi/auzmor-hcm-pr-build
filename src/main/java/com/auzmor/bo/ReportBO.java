package com.auzmor.bo;

import com.auzmor.impl.enumarator.ReportEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;

/**
 * Report BO - Report Business Object
 * @author Pradeep Balasubramanian
 * @lastModifiedDate 01/MAR/2018
 */
public interface ReportBO {

  ReturnObject<ReportEnum,JsonObject> getJobPostReports(Job jobModel)
      throws SQLException, DbHandleException;

  ReturnObject<ReportEnum,JsonArray> getCandidateStatusReports(CandidateR candidate)
      throws SQLException, DbHandleException;
}
