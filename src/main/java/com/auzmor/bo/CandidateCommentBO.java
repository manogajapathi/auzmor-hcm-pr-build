package com.auzmor.bo;

import com.auzmor.impl.enumarator.bo.CandidateCommentEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.models.CandidateR.CommentCandidate;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public interface CandidateCommentBO {

  CandidateCommentEnum addCommentDetails(CommentCandidate commentCandidate, Boolean flag)
      throws SQLException, DbHandleException;

  CandidateCommentEnum updateCommentDetails(CommentCandidate commentCandidate)
      throws SQLException, DbHandleException;

  CandidateCommentEnum deleteCommentDetails(long candidateId, long userId, long id)
      throws SQLException, DbHandleException;

  List<HashMap<String, String>> getCommentList(String format, long candidateId)
      throws SQLException, DbHandleException;

  List<HashMap<String, String>> getHistoryList(String format, long candidateId)
      throws SQLException, DbHandleException;
}
