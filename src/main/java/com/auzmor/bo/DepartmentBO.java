package com.auzmor.bo;

import com.auzmor.impl.enumarator.DepartmentEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.DepartmentModel;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;

public interface DepartmentBO {
  ReturnObject<DepartmentEnum, JsonObject> addDepartment(DbHandle dbHandle, 
                                                         DepartmentModel departmentModel)
      throws SQLException,DbHandleException;
  ReturnObject<DepartmentEnum, JsonObject> getDepartment(DbHandle dbHandle, 
                                                         DepartmentModel departmentModel)
      throws SQLException,DbHandleException;
  ReturnObject<DepartmentEnum, JsonObject> updateDepartment(DbHandle dbHandle, 
                                                            MultiMap requestParams) 
      throws SQLException,DbHandleException;
  ReturnObject<DepartmentEnum, JsonObject> deleteDepartment(DbHandle dbHandle, 
                                                            DepartmentModel departmentModel)
      throws SQLException,DbHandleException;
  ReturnObject<DepartmentEnum, JsonArray> getAllDepartments(DbHandle dbHandle, 
                                                            DepartmentModel departmentModel)
      throws SQLException,DbHandleException;
}
