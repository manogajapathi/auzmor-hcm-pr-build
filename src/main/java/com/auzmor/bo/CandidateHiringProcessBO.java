package com.auzmor.bo;

import com.auzmor.impl.enumarator.bo.CandidateHiringProcessEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.models.CandidateR.HiringProcessCandidate;
import com.auzmor.util.db.DbHandle;

import java.sql.SQLException;

public interface CandidateHiringProcessBO {
  CandidateHiringProcessEnum insertCandidateStatus(DbHandle dbHandle,
                                             HiringProcessCandidate candidateHiringProcessModel)
      throws SQLException, DbHandleException;

  HiringProcessCandidate getHiringProcess(long candidateId) throws SQLException,DbHandleException;

  String getStatus(long candidateId,String status) throws  SQLException,DbHandleException;
}
