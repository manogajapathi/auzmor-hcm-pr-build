package com.auzmor.bo;

import com.auzmor.impl.enumarator.FieldOptionsEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;

public interface FieldOptionsBO {
  ReturnObject<FieldOptionsEnum, JsonArray> getFieldOptionValues(DbHandle dbHandle,
                                                           MultiMap requestParams)
      throws SQLException,DbHandleException;
}
