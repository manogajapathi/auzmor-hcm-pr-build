package com.auzmor.bo;

import com.auzmor.impl.enumarator.bo.CandidateOfferEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.models.CandidateR.OfferProcessCandidate;
import io.vertx.core.MultiMap;
import io.vertx.ext.web.RoutingContext;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public interface CandidateOfferBO {

  long addOfferProcess(MultiMap paramsMap)
      throws SQLException, DbHandleException;

  CandidateOfferEnum updateOfferProcess(MultiMap paramsMap, RoutingContext routingContext)
      throws SQLException, DbHandleException;

  OfferProcessCandidate getOfferDetails(long candidateId)
      throws SQLException,DbHandleException;

  CandidateOfferEnum mailSent(long candidateId,String subjectName) throws SQLException,DbHandleException;

   List<HashMap<String,String>> getOfferMailDetails(long candidateId)
      throws SQLException, DbHandleException;

  // String getStatus(long candidateId,String status) throws SQLException,DbHandleException;
}


