package com.auzmor.bo;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.models.CandidateR.MailDetails;
import io.vertx.core.MultiMap;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public interface MailDetailsBO {

  List<HashMap<String,String>> getMailDetails(long candidateId)
      throws SQLException,DbHandleException;

  int create(MailDetails mailDetails) throws SQLException,DbHandleException;
}
