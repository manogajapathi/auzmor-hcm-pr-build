package com.auzmor.bo;

import com.auzmor.impl.enumarator.bo.EmployeePTOEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;
import java.text.ParseException;

public interface EmployeePTOBO {
  ReturnObject<EmployeePTOEnum, JsonObject> addRequest(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException, ParseException;
  ReturnObject<EmployeePTOEnum, JsonArray> allRequest(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException, ParseException;
  ReturnObject<EmployeePTOEnum, JsonArray> allUsedRequest(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException, ParseException;
  ReturnObject<EmployeePTOEnum, JsonObject> getAvailableCount(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
  ReturnObject<EmployeePTOEnum, JsonArray> getUpcomingTimeOff(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException, ParseException;
}
