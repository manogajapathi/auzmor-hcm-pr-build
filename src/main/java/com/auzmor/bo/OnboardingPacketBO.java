package com.auzmor.bo;

import com.auzmor.impl.enumarator.bo.OnboardingPacketEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.onboarding.OnboardingPacket;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;

public interface OnboardingPacketBO {
  ReturnObject<OnboardingPacketEnum, JsonObject> addPacket(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
  ReturnObject<OnboardingPacketEnum, JsonObject> getPacket(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
  ReturnObject<OnboardingPacketEnum, JsonObject> sendRemainderMail(DbHandle dbHandle,
                                                                   MultiMap params)
      throws SQLException, DbHandleException;
  ReturnObject<OnboardingPacketEnum, JsonObject> deletePacket(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
  ReturnObject<OnboardingPacketEnum, JsonObject> sendPacket(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
  ReturnObject<OnboardingPacketEnum, JsonObject> updatePacket(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
}
