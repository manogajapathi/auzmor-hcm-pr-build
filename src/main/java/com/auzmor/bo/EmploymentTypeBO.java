package com.auzmor.bo;

import com.auzmor.impl.enumarator.EmploymentTypeEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.EmploymentModel;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;

public interface EmploymentTypeBO {
  ReturnObject<EmploymentTypeEnum, JsonObject> addEmploymentType(DbHandle dbHandle,
                                                             EmploymentModel employmentModel)
      throws SQLException,DbHandleException;
  ReturnObject<EmploymentTypeEnum, JsonObject> getEmploymentType(DbHandle dbHandle,
                                                             EmploymentModel employmentModel)
      throws SQLException,DbHandleException;
  ReturnObject<EmploymentTypeEnum, JsonObject> updateEmploymentType(DbHandle dbHandle,
                                                            MultiMap requestParams)
      throws SQLException,DbHandleException;
  ReturnObject<EmploymentTypeEnum, JsonObject> deleteEmploymentType(DbHandle dbHandle, 
                                                                    EmploymentModel employmentModel)
      throws SQLException,DbHandleException;
  ReturnObject<EmploymentTypeEnum, JsonArray> getAllEmploymentTypes(DbHandle dbHandle, 
                                                                    EmploymentModel employmentModel)
      throws SQLException,DbHandleException;
}
