package com.auzmor.bo;

import com.auzmor.impl.enumarator.bo.OnboardingTaskEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;

public interface OnboardingTaskBO {
  ReturnObject<OnboardingTaskEnum, JsonObject> addTask(DbHandle dbHandle, MultiMap params)
    throws SQLException, DbHandleException;
  ReturnObject<OnboardingTaskEnum, JsonObject> deleteTask(DbHandle dbHandle, MultiMap params)
    throws SQLException, DbHandleException;
  ReturnObject<OnboardingTaskEnum, JsonObject> updateTask(DbHandle dbHandle, MultiMap params)
    throws SQLException, DbHandleException;
}
