package com.auzmor.bo;


import com.auzmor.impl.enumarator.DivisionEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.FieldOptions.DivisionModel;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;

public interface DivisionBO {
  ReturnObject<DivisionEnum, JsonObject> addDivision(DbHandle dbHandle,
                                                       DivisionModel divisionModel)
      throws SQLException,DbHandleException;
  ReturnObject<DivisionEnum, JsonObject> getDivision(DbHandle dbHandle,
                                                       DivisionModel divisionModel)
      throws SQLException,DbHandleException;
  ReturnObject<DivisionEnum, JsonObject> updateDivision(DbHandle dbHandle,
                                                            MultiMap requestParams)
      throws SQLException,DbHandleException;
  ReturnObject<DivisionEnum, JsonObject> deleteDivision(DbHandle dbHandle,
                                                          DivisionModel divisionModel)
      throws SQLException,DbHandleException;
  ReturnObject<DivisionEnum, JsonArray> getAllDivisions(DbHandle dbHandle,
                                                          DivisionModel divisionModel)
      throws SQLException,DbHandleException;
}
