package com.auzmor.bo;

import com.auzmor.impl.enumarator.bo.JobDescriptionEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;

public interface JobDescriptionBO {
  ReturnObject<JobDescriptionEnum, JsonObject> addJobDescription(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
  ReturnObject<JobDescriptionEnum, JsonArray> getAllJobDescriptions(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
  ReturnObject<JobDescriptionEnum, JsonObject> getJobDescription(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
  ReturnObject<JobDescriptionEnum, JsonObject> updateJobDescription(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
  ReturnObject<JobDescriptionEnum, JsonObject> deleteJobDescription(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
}
