package com.auzmor.bo;

import com.auzmor.impl.enumarator.bo.PTOCommentsEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.PTO.PTOComments;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;
import java.text.ParseException;

public interface PTOCommentBO {
  ReturnObject<PTOCommentsEnum, JsonObject> addComment(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
  ReturnObject<PTOCommentsEnum, JsonArray> getAllComments(DbHandle dbHandle, MultiMap params) 
      throws SQLException, DbHandleException, ParseException;
  ReturnObject<PTOCommentsEnum, JsonObject> deleteComment(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
  ReturnObject<PTOCommentsEnum, JsonObject> updateComment(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException;
}
