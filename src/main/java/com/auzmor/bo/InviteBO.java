package com.auzmor.bo;

import com.auzmor.impl.enumarator.bo.InviteBOEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.Invite;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.util.bo.ReturnObject;
import io.vertx.core.MultiMap;

import java.sql.SQLException;

/**
 *
 * @author Charles Sam Dilip
 * @lastModifiedDate 10/FEB/2018
 */
public interface InviteBO {
  InviteBOEnum inviteNewEmployee(String email, Long organizationId) throws SQLException,
      DbHandleException;
  ReturnObject<InviteBOEnum, Employee> addInvitedEmployee(MultiMap paramsMap) throws SQLException, DbHandleException;
  InviteBOEnum inviteResend(String email, Long organizationId) throws SQLException,
      DbHandleException;
  InviteBOEnum cancelInvite(String email, Long organizationId) throws SQLException,
      DbHandleException;


  Invite addInvite(String email, long organizationId) throws SQLException,
      DbHandleException;
  boolean hasInviteForEmail(String email, long organizationId) throws SQLException,
      DbHandleException;
}