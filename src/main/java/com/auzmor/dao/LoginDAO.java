package com.auzmor.dao;

import com.auzmor.impl.enumarator.bo.LoginBOEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.Login;
import io.vertx.core.MultiMap;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

/**
 * @author Charles Sam Dilip
 * @lastModifiedDate 05/FEB/2018
 */
public interface LoginDAO {
  int create(Login login) throws SQLException, DbHandleException;

  int createWithToken(Login login) throws SQLException, DbHandleException;

  void updateConsumerId(String consumerId, long loginId) throws SQLException, DbHandleException;
  String getConsumerId(int userId) throws SQLException,DbHandleException;

  // TODO conver int id to long
  boolean isLoginAlreadyExists(String email, int organization_id) throws SQLException, DbHandleException;

  // Retain only this function
  boolean isLoginAlreadyExists(String email, Long organization_id) throws SQLException,
      DbHandleException;

  Login isValidCredentialAndGetLogin(String email, String password, Long organizationId)
      throws SQLException, DbHandleException;

  Login getLoginDetails(long loginId) throws SQLException, DbHandleException;

  boolean isOldPasswordCorrect(String oldPassword, int loginId) throws SQLException, DbHandleException;

  int updatePassword(String changePassword, int loginId) throws SQLException, DbHandleException;

  LoginBOEnum approveEmployeeSignup(MultiMap params) throws SQLException, DbHandleException;

  Login getLoginDetails(String email, int organizationId) throws SQLException, DbHandleException;

  int updateKeyValue(Login login) throws SQLException, DbHandleException;

  Login getKeyValues(Login login) throws SQLException, DbHandleException;

  long getLoginIdWithKeyValue(Login login) throws SQLException, DbHandleException;

  int updatePasswordWithKeyValue(Login login) throws SQLException, DbHandleException;

  boolean candidateLoginCheck(Login login) throws SQLException, DbHandleException;

  Login getLoginUserDetails(long loginId) throws SQLException, DbHandleException;

  int updateHashValue(Login login) throws SQLException, DbHandleException;
  List<HashMap<String, String>> GetAdminList(Long organization_id) throws SQLException, DbHandleException;

}
