package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.Role;

import java.sql.SQLException;
import java.util.List;

public interface RoleDAO {
  List<Role> getRoles(List<Long> roleIds) throws SQLException, DbHandleException;
  Role getRoleByName(String name, long organizationId) throws SQLException, DbHandleException;
}
