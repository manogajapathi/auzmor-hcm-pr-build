package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.Job.JobTaskModel;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;
import java.util.List;

public interface JobTaskDAO {
  int createTask(JobTaskModel jobTaskModel) throws SQLException, DbHandleException;
  boolean isTaskAlreadyExists(JobTaskModel jobTaskModel) throws SQLException, DbHandleException;
  List<Long> getTaskIdsForEmployee(long employeeId) throws SQLException, DbHandleException;
  List<JsonObject> getTasksForEmployee(JobTaskModel jobTaskModel)
      throws SQLException, DbHandleException;
  int updateTask(String taskArray) throws SQLException, DbHandleException;
  JsonObject getJobTaskForJob(long jobId) throws SQLException, DbHandleException;
  JsonObject getJobTaskDetails(JobTaskModel jobTaskModel) throws SQLException, DbHandleException;
  int updateCompletedFlag(long employeeId) throws SQLException,DbHandleException;
  JsonObject getJobTaskForCandidateInterview(JobTaskModel jobTaskModel) throws SQLException, DbHandleException;
  int updateTaskForInterview(JobTaskModel jobTaskModel) throws SQLException, DbHandleException;
}

