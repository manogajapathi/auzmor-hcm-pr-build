package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.FieldOptions.HighestEducationModel;
import io.vertx.core.json.JsonObject;


import java.sql.SQLException;
import java.util.List;

public interface HighestEducationDAO {
  int insert(HighestEducationModel highestEducationModel) throws SQLException, DbHandleException;
  boolean isAlreadyHighestEducation(HighestEducationModel highestEducationModel)
    throws SQLException, DbHandleException;
  JsonObject getHighestEducation(HighestEducationModel highestEducationModel)
      throws SQLException, DbHandleException;
  List<JsonObject> getAllHighestEducation(HighestEducationModel highestEducationModel)
      throws SQLException, DbHandleException;
  int update(HighestEducationModel highestEducationModel) throws SQLException, DbHandleException;
  int delete(HighestEducationModel highestEducationModel) throws SQLException, DbHandleException;
}
