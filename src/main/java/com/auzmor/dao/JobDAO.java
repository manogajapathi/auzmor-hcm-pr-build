package com.auzmor.dao;

import java.sql.SQLException;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.Job.Collaborator;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.model.employee.Employee;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.HashMap;
import java.util.List;



public interface JobDAO {

  List<HashMap<String,String>> getJobHiringDetails(long candidateId)
      throws SQLException,DbHandleException;

  Job getJobDetails(long jobId) throws SQLException,DbHandleException;

  Job getJobDetailsWithoutStatus(long jobId) throws SQLException,DbHandleException;

  void moveCandidatesToTalentPool(int id, int creatorId, String organizationName)
      throws SQLException, DbHandleException;

  String getNameBasedOnEmployeeId(int employeeId) throws SQLException, DbHandleException;

  JsonArray getJobStatus(String status,long jobId,long organizationId)
      throws SQLException,DbHandleException;

  JsonArray getJobStatus(String constraintString) throws SQLException;

  long getJobCount(long organizationId) throws SQLException, DbHandleException;

  boolean isCollaboratorAvailable(String collaboratorId, int jobId)
      throws SQLException, DbHandleException;

  void updateCollaborator(int id, String collaborator, Boolean collabEmailUpdate)
      throws SQLException, DbHandleException;

  void insertCollaborator(int id, String collaborator, Boolean collabEmailUpdate)
      throws SQLException, DbHandleException;

  JsonObject getHiringLeadName(String hiringLeadId) throws SQLException;

  String getTitleBasedOnId(long jobId) throws SQLException;

  List<HashMap<String,String>> getJobWithCandidateLoginId(long candidateLoginId,long organizationId)
      throws SQLException,DbHandleException;

  List<Employee> getCollaboratorsWithEmailUpdates(long candidateId)
      throws SQLException, DbHandleException;

  List<Collaborator> getRemovableCollaboratorList(int id, List<String> availableCollaborators)
      throws SQLException, DbHandleException;

  void removeCollaborator(int id) throws SQLException, DbHandleException;

  boolean doesJobPostExists(Job job) throws SQLException, DbHandleException;

  boolean isJobIDValid(Job job) throws SQLException, DbHandleException;

  List<JsonObject> getHiringLeadAndCollaboratorsWithJobId(Job job)
      throws SQLException, DbHandleException;

  List<HashMap<String,String>> getCollaboratorList(long jobId) throws SQLException,DbHandleException;

  //boolean isCollaborator(long creatorId, long jobId) throws SQLException, DbHandleException;

  List<JsonObject> getJobDetailsForEmployee(long employeeId) throws SQLException, DbHandleException;

  String getHiringLeadBasedOnCandidate(long candidateId) throws SQLException, DbHandleException;

  JsonObject getCollaboratorInformation(String collaboratorId, int jobId)
      throws SQLException, DbHandleException;

  boolean checkFieldOption(String parameterColumn, String parameterValue, long organizationId)
      throws SQLException, DbHandleException;

  int cloneJob(Job job) throws SQLException, DbHandleException;

}
