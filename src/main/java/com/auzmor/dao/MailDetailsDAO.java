package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.EmailFormat;
import com.auzmor.impl.models.CandidateR.MailDetails;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public interface MailDetailsDAO {

  int create(MailDetails mailDetails) throws SQLException, DbHandleException;

  List<HashMap<String,String>> getMailDetails(long candidateId)
      throws SQLException,DbHandleException;

  int updateActiveStatus(long candidateId) throws SQLException,DbHandleException;
  int insertRemainder(EmailFormat emailFormat, String ccAddress)
      throws SQLException, DbHandleException;
}
