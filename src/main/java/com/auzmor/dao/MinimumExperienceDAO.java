package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.FieldOptions.MinimumExperienceModel;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;
import java.util.List;

public interface MinimumExperienceDAO {
  int insert(MinimumExperienceModel minimumExperienceModel) throws SQLException, DbHandleException;
  boolean isAlreadyMinimumExperience(MinimumExperienceModel minimumExperienceModel)
      throws SQLException, DbHandleException;
  JsonObject getMinimumExperience(MinimumExperienceModel minimumExperienceModel)
      throws SQLException, DbHandleException;
  List<JsonObject> getAllMinimumExperience(MinimumExperienceModel minimumExperienceModel)
      throws SQLException, DbHandleException;
  int update(MinimumExperienceModel minimumExperienceModel) throws SQLException, DbHandleException;
  int delete(MinimumExperienceModel minimumExperienceModel) throws SQLException, DbHandleException;
}
