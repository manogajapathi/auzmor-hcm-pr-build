package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.EmailTemplate;

import java.sql.SQLException;
import java.util.List;

public interface EmailTemplateDAO {
  long create(String name, String subject, String header, String footer, String template, long organizationId)
      throws SQLException, DbHandleException;
  int update(String name, String subject, String header, String footer, String template, long templateId)
      throws SQLException, DbHandleException;
  int delete(long templateId) throws SQLException, DbHandleException;
  EmailTemplate getTemplate(long templateId) throws SQLException, DbHandleException;
  List<EmailTemplate> getTemplates(long organizationId) throws SQLException, DbHandleException;
}
