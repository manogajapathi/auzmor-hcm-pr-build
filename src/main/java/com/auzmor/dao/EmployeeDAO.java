package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.employee.Employee;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public interface EmployeeDAO {
  int create(Employee employee) throws SQLException, DbHandleException;

  boolean isEmployeeExistsInOrganization(String email, Long organizationId)
      throws DbHandleException, SQLException;

  boolean isEmployeeExistsInOrganization(long employee, Long organizationId)
      throws DbHandleException, SQLException;

  JsonArray getAllEmployees(MultiMap params) throws SQLException, DbHandleException;

  JsonArray getAllUsersList(MultiMap params) throws SQLException, DbHandleException;

  boolean isFieldOptionPresent(String fieldOptionColumn, String fieldOptionValue,
                               int organizationId)
      throws DbHandleException, SQLException;

  JsonArray getEmployeeCountForFieldOptions(String fieldRequired, String tableName,
                                            String fieldColumn, int organizationId)
      throws DbHandleException, SQLException;

  Employee getEmployeeDetails(long employeeId) throws SQLException, DbHandleException;

  Employee getEmployeeDetailsByLoginId(long loginId) throws SQLException, DbHandleException;

  int getUserIdForEmployee(int employeeId) throws DbHandleException, SQLException;
  int updateOrganizationDetails(int organizationId, String workEmail) throws SQLException,DbHandleException;
  int deleteUser(int organizationId, int employeeId) throws  SQLException,DbHandleException;
  int editUser(MultiMap params) throws  SQLException,DbHandleException;
  int addNewUser(MultiMap params) throws  SQLException,DbHandleException;
  Employee getWorkEmailAndAccessLevel(int employeeId, int organizationId)  throws  SQLException,DbHandleException;
  int uploadProfileImage(String imagePath, long organizationId, int employeeId)
      throws SQLException, DbHandleException;

  //JsonArray getEmployee(long employeeId) throws SQLException, DbHandleException;

  int updateEmployeePacket(Employee employee) throws SQLException, DbHandleException;

  List<HashMap<String,String>> getEmployeeEmailIds(long collaboratorId, long adminId) throws SQLException, DbHandleException;

  JsonObject getDashBoardCount(Employee employee) throws SQLException, DbHandleException;
}
