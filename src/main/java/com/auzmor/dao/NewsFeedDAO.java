package com.auzmor.dao;

import io.vertx.core.json.JsonArray;
import org.elasticsearch.client.RestClient;

import java.io.IOException;

public interface NewsFeedDAO {

  JsonArray getAnnouncementDetails(RestClient restClient, int organizationId) throws IOException;

  JsonArray getOnboardingDetails(RestClient restClient, int organizationId) throws IOException;

  JsonArray getJobTasksDetails(RestClient restClient, int organizationId) throws IOException;
}
