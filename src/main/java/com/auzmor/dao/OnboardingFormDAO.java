package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.onboarding.OnboardingForm;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;
import java.util.List;

public interface OnboardingFormDAO {
  boolean isAlreadyExists(OnboardingForm onboardingForm) throws SQLException, DbHandleException;
  int addForm(OnboardingForm onboardingForm) throws SQLException, DbHandleException;
  List<String> getAvailableForms(OnboardingForm onboardingForm)
      throws SQLException, DbHandleException;
  JsonObject getForm(OnboardingForm onboardingForm) throws SQLException, DbHandleException;
  int deletePacket(OnboardingForm onboardingForm) throws SQLException, DbHandleException;
  int deleteForm(OnboardingForm onboardingForm) throws SQLException, DbHandleException;
  int updateForm(MultiMap requestParams) throws SQLException, DbHandleException;
  JsonArray getAuzmorForms(MultiMap params) throws SQLException, DbHandleException;
}
