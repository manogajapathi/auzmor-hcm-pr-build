package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.PTO.PTOComments;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;
import java.util.List;

public interface PTOCommentsDAO {
  boolean alreadyExists(PTOComments ptoComments) throws SQLException, DbHandleException;
  int addComment(PTOComments ptoComments) throws SQLException, DbHandleException;
  PTOComments getPTOComment(PTOComments ptoComments) throws SQLException, DbHandleException;
  List<JsonObject> getAllComments(PTOComments ptoComments) throws SQLException, DbHandleException;
  int deleteComment(PTOComments ptoComments) throws SQLException, DbHandleException;
  int updateComment(PTOComments ptoComments) throws SQLException, DbHandleException;
}
