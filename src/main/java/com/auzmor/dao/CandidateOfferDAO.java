package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.models.CandidateR.OfferProcessCandidate;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public interface CandidateOfferDAO {

  int create(OfferProcessCandidate offerProcessCandidate)
      throws SQLException, DbHandleException;

  OfferProcessCandidate getOfferProcessDetails(long candidateId)
      throws SQLException,DbHandleException;

  int updateCancelStatus(String cancelReason,String status,long candidateId,long userId)
      throws  SQLException,DbHandleException;

  int updateStatus(String status,long candidateId) throws SQLException,DbHandleException;

  int updateOfferDetails(OfferProcessCandidate offerProcessCandidate)
      throws SQLException,DbHandleException;

  List<HashMap<String, String>> getOfferMailList(long candidateId)
      throws SQLException,DbHandleException;

 // String getStatus(long candidateId,String status) throws SQLException,DbHandleException;

  int updateActiveStatus(long candidateId) throws SQLException,DbHandleException;
}
