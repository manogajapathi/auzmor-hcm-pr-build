package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.DepartmentModel;
import io.vertx.core.MultiMap;

import java.sql.SQLException;
import java.util.List;

public interface DepartmentDAO {
  public int insert(DepartmentModel departmentModel) throws SQLException,DbHandleException;
  public boolean isDepartmentExistsInOrganization(DepartmentModel departmentModel)
      throws SQLException,DbHandleException;
  public DepartmentModel getDepartmentDetails(DepartmentModel departmentModel)
      throws SQLException,DbHandleException;
  public List<DepartmentModel> getAllDepartments(DepartmentModel departmentModel)
      throws SQLException,DbHandleException;
  public int updateDepartmentDetails(MultiMap requestParams) throws SQLException,
      DbHandleException;
  public int deleteDepartment(DepartmentModel departmentModel) throws SQLException,
      DbHandleException;
}
