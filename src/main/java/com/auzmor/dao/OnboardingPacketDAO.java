package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.onboarding.OnboardingPacket;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;

import java.sql.SQLException;

public interface OnboardingPacketDAO {
  boolean isAlreadyExists(OnboardingPacket onboardingPacket) throws SQLException, DbHandleException;
  int addPacket(OnboardingPacket onboardingPacket) throws SQLException, DbHandleException;
  JsonArray getPackets(OnboardingPacket onboardingPacket) throws SQLException, DbHandleException;
  int deletePacket(OnboardingPacket onboardingPacket) throws SQLException, DbHandleException;
  int updatePacket(MultiMap requestParams) throws SQLException, DbHandleException;
}
