package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.PayGroupModel;
import io.vertx.core.MultiMap;

import java.sql.SQLException;
import java.util.List;

public interface PayGroupDAO {
  public int insert(PayGroupModel payGroupModel) throws SQLException,DbHandleException;
  public boolean isPayGroupExistsInOrganization(PayGroupModel payGroupModel)
      throws SQLException,DbHandleException;
  public PayGroupModel getPayGroupDetails(PayGroupModel payGroupModel)
      throws SQLException,DbHandleException;
  public List<PayGroupModel> getAllPayGroups(PayGroupModel payGroupModel)
      throws SQLException,DbHandleException;
  public int updatePayGroupDetails(MultiMap requestParams) throws SQLException,
      DbHandleException;
  public int deletePayGroup(PayGroupModel payGroupModel) throws SQLException,
      DbHandleException;
}
