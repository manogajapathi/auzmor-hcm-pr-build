package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.TitleModel;
import io.vertx.core.MultiMap;

import java.sql.SQLException;
import java.util.List;

public interface TitleDAO {
  public int insert(TitleModel titleModel) throws SQLException,DbHandleException;
  public boolean isTitleExistsInOrganization(TitleModel titleModel)
      throws SQLException,DbHandleException;
  public TitleModel getTitleDetails(TitleModel titleModel)
      throws SQLException,DbHandleException;
  public List<TitleModel> getAllTitles(TitleModel titleModel)
      throws SQLException,DbHandleException;
  public int updateTitleDetails(MultiMap requestParams) throws SQLException,
      DbHandleException;
  public int deleteTitle(TitleModel titleModel) throws SQLException,
      DbHandleException;
}
