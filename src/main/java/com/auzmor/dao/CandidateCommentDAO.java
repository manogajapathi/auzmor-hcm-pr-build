package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.models.CandidateR.CommentCandidate;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public interface CandidateCommentDAO {
  int create(CommentCandidate commentCandidate)
      throws SQLException, DbHandleException;

  List<HashMap<String,String>> getCommentsDetails(String dateFormat, long candidateId)
      throws SQLException,DbHandleException;

  List<HashMap<String,String>> getCommentsDetailsInHistoryTab(String dateFormat, long candidateId)
      throws SQLException,DbHandleException;

  int updateComment(CommentCandidate commentCandidate)
      throws SQLException,DbHandleException;

  int deleteComment(long candidateId,long userId,long id)
      throws SQLException, DbHandleException;

  boolean checkUserId(long id,long userId) throws SQLException,DbHandleException;
}
