package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.LocationModel;
import io.vertx.core.MultiMap;

import java.sql.SQLException;
import java.util.List;

public interface LocationDAO {
  public int insert(LocationModel locationModel) throws SQLException,DbHandleException;
  public boolean isLocationExistsInOrganization(LocationModel locationModel)
      throws SQLException,DbHandleException;
  public LocationModel getLocationDetails(LocationModel locationModel)
      throws SQLException,DbHandleException;
  public List<LocationModel> getAllLocations(LocationModel locationModel)
      throws SQLException,DbHandleException;
  public int updateLocationDetails(MultiMap requestParams) throws SQLException,
      DbHandleException;
  public int deleteLocation(LocationModel locationModel) throws SQLException,
      DbHandleException;
  String getAddress(String locationName,long organizationId) throws SQLException,DbHandleException;
}
