package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.models.CandidateR.HiringProcessCandidate;

import java.sql.SQLException;

public interface CandidateHiringDAO {
  int create(HiringProcessCandidate hiringProcessCandidate)
      throws SQLException, DbHandleException;

  HiringProcessCandidate getHiringProcess(long candidateId) throws SQLException,DbHandleException;

  boolean getStatusCheck(String status, String statusReason, long candidateId)
      throws SQLException,DbHandleException;
  String getReasonCheck(String status, String statusValue, long id)
      throws SQLException,DbHandleException;
  String getCandidateProcess(long candidateId,String status) throws SQLException,DbHandleException;

  int updateStatus(long candidateId) throws SQLException,DbHandleException;

}
