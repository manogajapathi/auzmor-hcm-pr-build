package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;
import java.util.List;

/**
 * Report DAO Interface
 *
 * @author Pradeep Balasubramanian
 * @lastModifiedDate 07/MAR/2018
 *
 */
public interface ReportDAO {
  List<JsonObject> getJobReports(Job job) throws SQLException, DbHandleException;

  List<JsonObject> getCandidateStatusReports(CandidateR candidateModel) throws SQLException,
      DbHandleException;
}

