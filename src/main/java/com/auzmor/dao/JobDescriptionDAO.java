package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.JobDescriptionModel;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;
import java.util.List;

public interface JobDescriptionDAO {
  int insert(JobDescriptionModel jobDescriptionModel) throws SQLException, DbHandleException;
  boolean isJobDescriptionAlreadyExists(JobDescriptionModel jobDescriptionModel)
    throws SQLException, DbHandleException;
  List<JobDescriptionModel> getAllJobDescriptions(JobDescriptionModel jobDescriptionModel)
      throws SQLException, DbHandleException;
  JsonObject getJobDescription(JobDescriptionModel jobDescriptionModel)
      throws SQLException, DbHandleException;
  int update(JobDescriptionModel jobDescriptionModel) throws SQLException, DbHandleException;
  int delete(JobDescriptionModel jobDescriptionModel) throws SQLException, DbHandleException;
  JobDescriptionModel getJobDescriptionDetails(JobDescriptionModel jobDescriptionModel)
    throws SQLException, DbHandleException;
}
