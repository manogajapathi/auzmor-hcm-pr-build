package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.onboarding.OnboardingDocument;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;

import java.sql.SQLException;

public interface OnboardingDocumentDAO {
  boolean isAlreadyExists(OnboardingDocument onboardingDocument)
      throws SQLException, DbHandleException;
  int addDocument(OnboardingDocument onboardingDocument) throws SQLException, DbHandleException;
  JsonArray getDocuments(OnboardingDocument onboardingDocument)
      throws SQLException, DbHandleException;
  int deletePacket(OnboardingDocument onboardingDocument) throws SQLException, DbHandleException;
  int deleteDocument(OnboardingDocument onboardingDocument) throws SQLException, DbHandleException;
  int updateDocument(MultiMap params) throws SQLException, DbHandleException;
}
