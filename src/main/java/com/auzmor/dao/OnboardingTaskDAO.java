package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.onboarding.OnboardingTask;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;

import java.sql.SQLException;

public interface OnboardingTaskDAO {
  boolean isAlreadyExists(OnboardingTask onboardingTask) throws SQLException, DbHandleException;
  int addTask(OnboardingTask onboardingTask) throws SQLException, DbHandleException;
  JsonArray getTasks(OnboardingTask onboardingTask) throws SQLException, DbHandleException;
  int deletePacket(OnboardingTask onboardingTask) throws SQLException, DbHandleException;
  int deleteTask(OnboardingTask onboardingTask) throws SQLException, DbHandleException;
  int updateTask(MultiMap params) throws SQLException, DbHandleException;
}
