package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.onboarding.OnboardingPacket;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;

public interface OnboardingWelcomeDAO {
  int updateWelcomeScreen(MultiMap params) throws SQLException, DbHandleException;
  OnboardingPacket getWelcomeDetails(OnboardingPacket onboardingPacket) throws SQLException,
      DbHandleException;
  boolean updateLatLongValues(MultiMap params) throws SQLException, DbHandleException;
}
