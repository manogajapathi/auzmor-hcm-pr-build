package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.Invite;

import java.sql.SQLException;

/**
 *
 * @author Charles Sam Dilip
 * @lastModifiedDate 05/FEB/2018
 */
public interface InviteDAO {
  int create(Invite invite) throws SQLException, DbHandleException;
  Invite getInvite(String inviteKey) throws SQLException, DbHandleException;
  Invite getActiveInviteForEmail(String email, long organizationId) throws SQLException, DbHandleException;
  boolean hasInviteForEmail(String email, long organizationId) throws SQLException,
      DbHandleException;
  boolean isInvitePending(String inviteKey, String email, Long organizationId)
      throws SQLException, DbHandleException;
  int updateInviteAsAccepted(String inviteKey) throws SQLException, DbHandleException;
  int delete(Invite invite) throws SQLException, DbHandleException;
}
