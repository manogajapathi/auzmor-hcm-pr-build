package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.EmploymentModel;
import io.vertx.core.MultiMap;

import java.sql.SQLException;
import java.util.List;

public interface EmploymentTypeDAO {
  public int insert(EmploymentModel employmentModel) throws SQLException,DbHandleException;
  public boolean isEmploymentTypeExistsInOrganization(EmploymentModel employmentModel)
      throws SQLException,DbHandleException;
  public EmploymentModel getEmploymentTypeDetails(EmploymentModel employmentModel)
      throws SQLException,DbHandleException;
  public List<EmploymentModel> getAllEmploymentTypes(EmploymentModel employmentModel)
      throws SQLException,DbHandleException;
  public int updateEmploymentTypeDetails(MultiMap requestParams) throws SQLException,
      DbHandleException;
  public int deleteEmploymentType(EmploymentModel employmentModel) throws SQLException,
      DbHandleException;
}
