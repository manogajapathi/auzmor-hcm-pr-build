package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.OrganizationR;
import com.auzmor.impl.model.Role;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Charles Sam Dilip
 * @lastModifiedDate 05/FEB/2018;
 */
public interface OrganizationDAO {
  Organization getOrganizationById(long organizationId) throws DbHandleException, SQLException;
  Organization getOrganizationByDomainName(String domainName) throws DbHandleException, SQLException;
  public boolean isDomainRegistered(String domain) throws SQLException, DbHandleException;
  public int updateDomains(String atsDomain, String careerDomain, long organizationId)
      throws DbHandleException, SQLException;
  Organization getOrganizationDetails(String hrisUrl) throws SQLException, DbHandleException;
  int addOrganizationDetails(OrganizationR organization) throws SQLException,DbHandleException;
  List<Role> getRoles(long organizationId) throws SQLException, DbHandleException;
}
