package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.PTO.EmployeePTORequests;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;
import java.util.List;

public interface EmployeePTORequestsDAO {
  int addRequest(EmployeePTORequests employeePTORequests) throws SQLException, DbHandleException;
  boolean alreadyExists(EmployeePTORequests employeePTORequests)
      throws SQLException, DbHandleException;
  double getUsedHours(EmployeePTORequests employeePTORequests)
      throws SQLException, DbHandleException;
  List<JsonObject> allRequests(EmployeePTORequests employeePTORequests)
      throws SQLException, DbHandleException;
  List<JsonObject> allUsed(EmployeePTORequests employeePTORequests)
      throws SQLException, DbHandleException;
  List<JsonObject> upcomingPTO(EmployeePTORequests employeePTORequests)
      throws SQLException, DbHandleException;
}
