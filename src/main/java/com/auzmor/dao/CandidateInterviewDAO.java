package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.models.CandidateR.InterviewProcessCandidate;
import io.vertx.ext.web.RoutingContext;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public interface CandidateInterviewDAO {
  int create(InterviewProcessCandidate interviewProcessCandidate)
      throws SQLException, DbHandleException;
  int updateActiveStatus(long candidateId) throws SQLException, DbHandleException;

  InterviewProcessCandidate getInterviewProcessDetails(long candidateId)
      throws SQLException,DbHandleException;

  int updateCancelStatus(String cancelReason,String status,long candidateId,long userId)
      throws  SQLException,DbHandleException;

  int updateStatus(String status,long candidateId) throws SQLException,DbHandleException;

  int updateInterviewDetails(InterviewProcessCandidate interviewProcessCandidate)
      throws SQLException,DbHandleException;

  List<HashMap<String, String>> getListOfInterviewProcess(String dateFormat,
                                                          StringBuilder fieldsName,
                                                          long organizationId,
                                                          int limit, int offset,
                                                          List<String> fieldValue,long collaboratorId)
      throws SQLException, DbHandleException;

  List<HashMap<String, String>> getInterviewMailList(long candidateId)
      throws SQLException,DbHandleException;

  int updateCompletedStatus(long candidateId) throws SQLException, DbHandleException;

  int updateCompletedStatus(long candidateId,String subStatus) throws SQLException, DbHandleException;

  boolean checkStatus(String subStatus) throws SQLException, DbHandleException;

}
