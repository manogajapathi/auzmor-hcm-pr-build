package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.FieldOptions.DivisionModel;
import io.vertx.core.MultiMap;

import java.sql.SQLException;
import java.util.List;

public interface DivisionDAO {
  public int insert(DivisionModel divisionModel) throws SQLException,DbHandleException;
  public boolean isDivisionExistsInOrganization(DivisionModel divisionModel)
      throws SQLException,DbHandleException;
  public DivisionModel getDivisionDetails(DivisionModel divisionModel)
      throws SQLException,DbHandleException;
  public List<DivisionModel> getAllDivisions(DivisionModel divisionModel)
      throws SQLException,DbHandleException;
  public int updateDivisionDetails(MultiMap requestParams) throws SQLException,
      DbHandleException;
  public int deleteDivision(DivisionModel divisionModel) throws SQLException,
      DbHandleException;
}
