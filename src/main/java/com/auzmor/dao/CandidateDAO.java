package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.exception.util.ApplicationUtilException;
import com.auzmor.impl.models.CandidateR.CandidateCustomQuestionR;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.impl.util.StringUtil;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public interface CandidateDAO {

  int create(CandidateR candidate) throws SQLException, DbHandleException;

  CandidateR getCandidateDetails(long candidateId, long organizationId)
      throws SQLException, DbHandleException;

  //CandidateR getCandidateDetailsByLoginId(long loginId) throws SQLException, DbHandleException;

  CandidateR getCandidate(long candidateId)
      throws SQLException, DbHandleException;

  int getUserName(String firstName,String email, long organizationId)
      throws SQLException,DbHandleException, ApplicationUtilException;

   List<HashMap<String,String>> getUserIdCandidateDetails(long userId, long organizationId)
       throws SQLException,DbHandleException;

  List<HashMap<String,String>> getCandidateId(long userId)
      throws SQLException,DbHandleException;

  List<HashMap<String,String>> getJobIdCandidateDetails(long jobId, long organizationId)
      throws SQLException,DbHandleException;

  List<HashMap<String,String>> getJobIdCandidateDetails(long jobId)
      throws SQLException,DbHandleException;

   int deleteCandidate(long candidateId) throws SQLException,DbHandleException;

  List<HashMap<String,String>> getPostJobFilterDetails(long userId)
      throws SQLException,DbHandleException;

  int updateRating(int rating, long candidateId) throws SQLException,DbHandleException;

  int updateForwardEmployee(long employeeId, String notes, String id)
      throws SQLException,DbHandleException;

  int updateMoveCandidate(long jobId, long candidateId, String status)
                                              throws SQLException,DbHandleException;

  int updateCandidate(CandidateR candidateR) throws SQLException,DbHandleException;

  List<HashMap<String, String>> getCandidateList(String searchFilter,
                                                        int limit,int offset,
                                                        long organization,String candidateValues,
                                                        String hiringLeadValues,int collaboratorId,
                                                        int id,int employeeId,String stateId)
      throws SQLException,DbHandleException;

  List<HashMap<String,String>> getCandidateFilterCount(StringBuilder selectFilter)
                                            throws SQLException,DbHandleException;

  int getCandidateListCount(StringBuilder limit) throws SQLException,DbHandleException;

  int getCandidateDateCount(long organizationId) throws SQLException,DbHandleException;

  int updateCandidateStatus(String status,long candidateId) throws SQLException,DbHandleException;

  CandidateR getUserName(long candidateId) throws SQLException, DbHandleException;

  List<HashMap<String,String>> getCandidateTalentList(long organizationId, String format, String searchFilter,
                                                      int limit, int offset,String stateId)
      throws SQLException,DbHandleException;

  JsonArray getCandidateStatusValues(CandidateR candidateModel) throws SQLException, DbHandleException;

  List<JsonObject> getCandidateListBasedOnStatus(CandidateR candidateModel, MultiMap requestParams)
    throws SQLException, DbHandleException;

  int getCandidatewithLoginId(long organizationId) throws SQLException,DbHandleException;

  List<CandidateR> getAllResumes(CandidateR candidateR) throws SQLException, DbHandleException;

  int create(CandidateCustomQuestionR candidateCustomQuestionR, long candidateId) throws SQLException,DbHandleException;

  List<HashMap<String,String>> getCustomQuestionsList(long candidateId) throws SQLException, DbHandleException;

  long getCandidateCountForJob(long jobId) throws SQLException, DbHandleException;

  boolean checkFieldOptions(String parameterColumn, String parameterValue, long organizationId)
    throws SQLException, DbHandleException;

  boolean isDocumentAssociated(String filePath) throws SQLException, DbHandleException;

  int candidateLoginId(String email,long organizationId) throws SQLException, DbHandleException;

  boolean checkJobId(long userId,long jobId, long organizationId) throws SQLException, DbHandleException;

}
