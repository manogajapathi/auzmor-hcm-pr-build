package com.auzmor.dao;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.PTO.PTODescriptor;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;
import java.util.List;

public interface PTODescriptorDAO {
  JsonObject getPTOMaxHours(PTODescriptor ptoDescriptor) throws SQLException, DbHandleException;
  List<JsonObject> getAllHours(PTODescriptor ptoDescriptor) throws SQLException, DbHandleException;
}
