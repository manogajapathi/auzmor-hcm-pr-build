package com.auzmor.impl.models.CandidateR;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CommentCandidate {

  private long id;
  private long candidateId;
  private String adminName;
  private String customComments;
  private String comments;
  private Date commentsDate;
  private String status;
  private String modified;
  private long userId;
  private boolean isSystemGenerated;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getCandidateId() {
    return candidateId;
  }

  public void setCandidateId(long candidateId) {
    this.candidateId = candidateId;
  }

  public String getAdminName() {
    return adminName;
  }

  public void setAdminName(String adminName) {
    this.adminName = adminName;
  }

  public String getCustomComments() {
    return customComments;
  }

  public void setCustomComments(String customComments) {
    this.customComments = customComments;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public Date getCommentsDate() {
    return commentsDate;
  }

  public void setCommentsDate(Date commentsDate) {
    this.commentsDate = commentsDate;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getModified() {
    return modified;
  }

  public void setModified(String modified) {
    this.modified = modified;
  }

  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }

  public boolean isSystemGenerated() {
    return isSystemGenerated;
  }

  public void setSystemGenerated(boolean systemGenerated) {
    isSystemGenerated = systemGenerated;
  }

  public static Map<String, FieldOptions> getValidatorMap() {
    return commentFieldOptionsMap;
  }

  private static Map<String, FieldOptions> commentFieldOptionsMap = new HashMap<String, FieldOptions>()
  {{
    put("id", FieldOptionsCommon.NUMBER);
    put("candidate_id", FieldOptionsCommon.ANY);
    put("status", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETS_HTML_ESCAPE, 0, 500));
    put("customComments", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETS_HTML_ESCAPE, 0, 500));
    put("admin_name",new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, 0, 50));
    put("comments",new FieldOptionsImpl(FieldOptionsCommon.ANY, 0, 500));
    put("user_id", FieldOptionsCommon.NUMBER);
    put("creator_id", FieldOptionsCommon.NUMBER);
    put("employee_id", FieldOptionsCommon.NUMBER);
  }};
}
