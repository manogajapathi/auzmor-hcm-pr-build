package com.auzmor.impl.models.CandidateR;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class InterviewProcessCandidate {

  private Long id;
  private Long candidateId;
  private Date interviewDate;
  private String interviewTime;
  private Date interviewEndDate;
  private String interviewEndTime;
  private String interviewDuration;
  private String interviewLocation;
  private String interviewType;
  private String interviewDetails;
  private String interviewMailDescription;
  private String interviewCancelReason;
  private String status;
  private String type;
  private String adminName;
  private String subjectName;
  private Date created;
  private Date modified;
  private String rejectedBy;
  private long userId;
  private long collaboratorId;
  private String subStatus;
  private boolean isCompleted;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getCandidateId() {
    return candidateId;
  }

  public void setCandidateId(Long candidateId) {
    this.candidateId = candidateId;
  }

  public Date getInterviewDate() {
    return interviewDate;
  }

  public void setInterviewDate(Date interviewDate) {
    this.interviewDate = interviewDate;
  }

  public String getInterviewTime() {
    return interviewTime;
  }

  public void setInterviewTime(String interviewTime) {
    this.interviewTime = interviewTime;
  }

  public String getInterviewDuration() {
    return interviewDuration;
  }

  public void setInterviewDuration(String interviewDuration) {
    this.interviewDuration = interviewDuration;
  }

  public String getInterviewLocation() {
    return interviewLocation;
  }

  public void setInterviewLocation(String interviewLocation) {
    this.interviewLocation = interviewLocation;
  }

  public String getInterviewType() {
    return interviewType;
  }

  public void setInterviewType(String interviewType) {
    this.interviewType = interviewType;
  }

  public String getInterviewDetails() {
    return interviewDetails;
  }

  public void setInterviewDetails(String interviewDetails) {
    this.interviewDetails = interviewDetails;
  }

  public String getInterviewMailDescription() {
    return interviewMailDescription;
  }

  public void setInterviewMailDescription(String interviewMailDescription) {
    this.interviewMailDescription = interviewMailDescription;
  }

  public String getInterviewCancelReason() {
    return interviewCancelReason;
  }

  public void setInterviewCancelReason(String interviewCancelReason) {
    this.interviewCancelReason = interviewCancelReason;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getAdminName() {
    return adminName;
  }

  public void setAdminName(String adminName) {
    this.adminName = adminName;
  }

  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public Date getModified() {
    return modified;
  }

  public void setModified(Date modified) {
    this.modified = modified;
  }

  public String getRejectedBy() {
    return rejectedBy;
  }

  public void setRejectedBy(String rejectedBy) {
    this.rejectedBy = rejectedBy;
  }

  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }

  public long getCollaboratorId() {
    return collaboratorId;
  }

  public void setCollaboratorId(long collaboratorId) {
    this.collaboratorId = collaboratorId;
  }

  public Date getInterviewEndDate() {
    return interviewEndDate;
  }

  public void setInterviewEndDate(Date interviewEndDate) {
    this.interviewEndDate = interviewEndDate;
  }

  public String getInterviewEndTime() {
    return interviewEndTime;
  }

  public void setInterviewEndTime(String interviewEndTime) {
    this.interviewEndTime = interviewEndTime;
  }

  public boolean isCompleted() {
    return isCompleted;
  }

  public void setCompleted(boolean completed) {
    isCompleted = completed;
  }

  public String getSubStatus() {
    return subStatus;
  }

  public void setSubStatus(String subStatus) {
    this.subStatus = subStatus;
  }

  public static Map<String, FieldOptions> getValidatorMap() {
    return interviewFieldOptionsMap;
  }

  private static Map<String, FieldOptions> interviewFieldOptionsMap = new HashMap<String, FieldOptions>()
  {{
    put("candidate_id", FieldOptionsCommon.NUMBERWITHCOMMA);
    put("interview_time", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 20));
    put("interview_date", FieldOptionsCommon.DATE_FORMAT);
    put("interview_duration", new FieldOptionsImpl(FieldOptionsCommon.ALPHANUMERICWITHSPACE,
        -1, 20));
    put("interview_location", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 100));
    put("interview_details", new FieldOptionsImpl(FieldOptionsCommon.ANY_VALUES, -1, 500));
    put("interview_type", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 50));
    put("interview_mail_description", FieldOptionsCommon.ANY);
    put("type", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 150));
    put("subject_name", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
    put("admin_name", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 150));
    put("limit",FieldOptionsCommon.NUMBER);
    put("offset",FieldOptionsCommon.NUMBER);
    put("fields",FieldOptionsCommon.ANY);
    put("interview_cancel_reason",new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 500));
    put("interview_other_reason",new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 500));
    put("status", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 25));
    put("organization_id",FieldOptionsCommon.NUMBER);
    put("user_id",FieldOptionsCommon.NUMBER);
    put("collaborator_id",FieldOptionsCommon.NUMBER);
    put("candidateId", FieldOptionsCommon.NUMBERWITHCOMMA);
    put("interview_end_time", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 25));
    put("sub_status", new FieldOptionsImpl(FieldOptionsCommon.ALPHANUMERICWITHSPACE, -1, 25));
    put("is_completed",FieldOptionsCommon.ANY);
  }};

}

