package com.auzmor.impl.models.CandidateR;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class OfferProcessCandidate {

  private long id;
  private long candidateId;
  private String offerPosition;
  private String offerPayRate;
  private Date offerStartDate;
  private String offerJobLocation;
  private String offerContractDetails;
  private String offerAttachmentName;
  private String offerDeclineReason;
  private String offerMailDescription;
  private String subjectName;
  private String status;
  private String adminName;
  private String comments;
  private Date created;
  private String currencyType;
  private Date modified;
  private long userId;
  private String subStatus;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getCandidateId() {
    return candidateId;
  }

  public void setCandidateId(long candidateId) {
    this.candidateId = candidateId;
  }

  public String getOfferPosition() {
    return offerPosition;
  }

  public void setOfferPosition(String offerPosition) {
    this.offerPosition = offerPosition;
  }

  public String getOfferPayRate() {
    return offerPayRate;
  }

  public void setOfferPayRate(String offerPayRate) {
    this.offerPayRate = offerPayRate;
  }

  public Date getOfferStartDate() {
    return offerStartDate;
  }

  public void setOfferStartDate(Date offerStartDate) {
    this.offerStartDate = offerStartDate;
  }

  public String getOfferJobLocation() {
    return offerJobLocation;
  }

  public void setOfferJobLocation(String offerJobLocation) {
    this.offerJobLocation = offerJobLocation;
  }

  public String getOfferContractDetails() {
    return offerContractDetails;
  }

  public void setOfferContractDetails(String offerContractDetails) {
    this.offerContractDetails = offerContractDetails;
  }

  public String getOfferAttachmentName() {
    return offerAttachmentName;
  }

  public void setOfferAttachmentName(String offerAttachmentName) {
    this.offerAttachmentName = offerAttachmentName;
  }

  public String getOfferDeclineReason() {
    return offerDeclineReason;
  }

  public void setOfferDeclineReason(String offerDeclineReason) {
    this.offerDeclineReason = offerDeclineReason;
  }

  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getAdminName() {
    return adminName;
  }

  public void setAdminName(String adminName) {
    this.adminName = adminName;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public Date getModified() {
    return modified;
  }

  public void setModified(Date modified) {
    this.modified = modified;
  }

  public String getOfferMailDescription() {
    return offerMailDescription;
  }

  public void setOfferMailDescription(String offerMailDescription) {
    this.offerMailDescription = offerMailDescription;
  }

  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }

  public String getCurrencyType() {
    return currencyType;
  }

  public void setCurrencyType(String currencyType) {
    this.currencyType = currencyType;
  }

  public String getSubStatus() {
    return subStatus;
  }

  public void setSubStatus(String subStatus) {
    this.subStatus = subStatus;
  }

  public static Map<String, FieldOptions> getValidatorMap() {
    return offerFieldOptionsMap;
  }

  private static Map<String, FieldOptions> offerFieldOptionsMap = new HashMap<String, FieldOptions>()
  {{
    put("candidate_id", FieldOptionsCommon.NUMBER);
   // put("offer_position", FieldOptionsCommon.ALPHANUMERICWITHSPACE);
    put("offer_position", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 100));
    put("offer_start_date", FieldOptionsCommon.DATE_FORMAT);
    put("offer_pay_rate", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 100));
    put("offer_job_location", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE,
        -1, 100));
    put("offer_contract_details", new FieldOptionsImpl(FieldOptionsCommon.ANY_VALUES,
        -1, 500));
    put("offer_decline_reason", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE,
        -1, 500));
    put("offer_other_reason",new FieldOptionsImpl(FieldOptionsCommon.ANY,
        -1, 500));
    put("subject_name", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE,
        -1, 200));
    put("admin_name", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE,
        -1, 100));
    put("status", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE,
        -1, 100));
    put("limit", FieldOptionsCommon.NUMBER);
    put("offset", FieldOptionsCommon.NUMBER);
    put("fields", FieldOptionsCommon.ALPHABETSWITHSPACE);
    put("organization_id", FieldOptionsCommon.NUMBER);
    put("offer_attachment_name", new FieldOptionsImpl(FieldOptionsCommon.ANY,
        -1, 250));
    put("user_id", FieldOptionsCommon.NUMBER);
    put("currency_type", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 3));
    put("candidateId", FieldOptionsCommon.NUMBERWITHCOMMA);
    put("sub_status", new FieldOptionsImpl(FieldOptionsCommon.ALPHANUMERICWITHSPACE,
        -1, 200));
  }};

}
