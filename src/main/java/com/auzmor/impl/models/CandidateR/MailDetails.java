package com.auzmor.impl.models.CandidateR;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.util.HashMap;
import java.util.Map;

public class MailDetails {

  private long id;
  private long candidateId;
  private long userId;
  private String adminName;
  private String subjectName;
  private String created;
  private String attachmentName;
  private String mailDescription;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getCandidateId() {
    return candidateId;
  }

  public void setCandidateId(long candidateId) {
    this.candidateId = candidateId;
  }

  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }

  public String getAdminName() {
    return adminName;
  }

  public void setAdminName(String adminName) {
    this.adminName = adminName;
  }

  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }

  public String getCreated() {
    return created;
  }

  public void setCreated(String created) {
    this.created = created;
  }

  public String getAttachmentName() {
    return attachmentName;
  }

  public void setAttachmentName(String attachmentName) {
    this.attachmentName = attachmentName;
  }

  public String getMailDescription() {
    return mailDescription;
  }

  public void setMailDescription(String mailDescription) {
    this.mailDescription = mailDescription;
  }

  public static Map<String, FieldOptions> getValidatorMap() {
    return interviewFieldOptionsMap;
  }

  private static Map<String, FieldOptions> interviewFieldOptionsMap = new HashMap<String, FieldOptions>()
  {{
    put("candidate_id", FieldOptionsCommon.NUMBERWITHCOMMA);
    put("mail_description", FieldOptionsCommon.ANY);
    put("subject_name", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
    put("creator_id",FieldOptionsCommon.NUMBER);
    put("attachment_name", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
  }};

}
