package com.auzmor.impl.models.CandidateR;

public class CandidateCustomQuestionR {
  private int id;
  private int candidateId;
  private String customQuestion;
  private String customAnswer;
  private String created;
  private String modified;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getCandidateId() {
    return candidateId;
  }

  public void setCandidateId(int candidateId) {
    this.candidateId = candidateId;
  }

  public String getCustomQuestion() {
    return customQuestion;
  }

  public void setCustomQuestion(String customQuestion) {
    this.customQuestion = customQuestion;
  }

  public String getCustomAnswer() {
    return customAnswer;
  }

  public void setCustomAnswer(String customAnswer) {
    this.customAnswer = customAnswer;
  }

  public String getCreated() {
    return created;
  }

  public void setCreated(String created) {
    this.created = created;
  }

  public String getModified() {
    return modified;
  }

  public void setModified(String modified) {
    this.modified = modified;
  }
}
