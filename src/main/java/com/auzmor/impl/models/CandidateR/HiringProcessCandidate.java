package com.auzmor.impl.models.CandidateR;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.util.HashMap;
import java.util.Map;

public class HiringProcessCandidate {
  private Long id;
  private Long candidateId;
  private String status;
  private String adminName;
  private String candidateProcess;
  private String reason;
  private String otherCancelReason;
  private Long userId;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getCandidateId() {
    return candidateId;
  }

  public void setCandidateId(Long candidateId) {
    this.candidateId = candidateId;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getAdminName() {
    return adminName;
  }

  public void setAdminName(String adminName) {
    this.adminName = adminName;
  }

  public String getCandidateProcess() {
    return candidateProcess;
  }

  public void setCandidateProcess(String candidateProcess) {
    this.candidateProcess = candidateProcess;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getOtherCancelReason() {
    return otherCancelReason;
  }

  public void setOtherCancelReason(String otherCancelReason) {
    this.otherCancelReason = otherCancelReason;
  }

  public static Map<String, FieldOptions> getValidatorMap() {
    return inviteFieldOptionsMap;
  }

  private static Map<String, FieldOptions> inviteFieldOptionsMap = new HashMap<String, FieldOptions>()
  {{
    put("organization_id", FieldOptionsCommon.NUMBER);
    put("candidate_id", FieldOptionsCommon.NUMBER);
    put("status", FieldOptionsCommon.ALPHABETSWITHSPACE);
    put("candidate_process", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
    put("admin_name", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 150));
    put("reason", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 150));
    put("token", FieldOptionsCommon.ANY);
    put("user_id",FieldOptionsCommon.NUMBER);
  }};
}
