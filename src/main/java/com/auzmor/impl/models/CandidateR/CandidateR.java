package com.auzmor.impl.models.CandidateR;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.models.CandidateR.CandidateCustomQuestionR;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CandidateR {

  private Long candidateId;
  private String firstName;
  private String lastName;
  private String email;
  private String phoneNumber;
  private String profileImage;
  private String unit;
  private String addressLine;
  private String city;
  private String state;
  private String country;
  private String zip;
  private String resume;
  private String dateAvailable;
  private String coverLetter;
  private String highestEducation;
  private String reference;
  private String desiredSalary;
  private String college;
  private String referredBy;
  private String linkedinUrl;
  private String blog;
  private String companyTypeCheck;
  private Date created;
  private Date modified;
  private String ethnicity;
  private String gender;
  private String veteranStatus;
  private String disability;
  private Long hiringLeadId;
  private String internalJobCode;
  private Long userId;
  private Long jobId;
  private Long organizationId;
  private String comments;
  private Long collaboratorId;
  private String employeeNotes;
  private Date collaboratorDate;
  private String source;
  private String status;
  private Long adminId;
  private long rating;
  private String resumeName;

  private List<CandidateCustomQuestionR> customQuestionModels;

  public Long getCandidateId() {
    return candidateId;
  }

  public void setCandidateId(Long candidateId) {
    this.candidateId = candidateId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getProfileImage() {
    return profileImage;
  }

  public void setProfileImage(String profileImage) {
    this.profileImage = profileImage;
  }

  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public String getAddressLine() {
    return addressLine;
  }

  public void setAddressLine(String addressLine) {
    this.addressLine = addressLine;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getZip() {
    return zip;
  }

  public void setZip(String zip) {
    this.zip = zip;
  }

  public String getResume() {
    return resume;
  }

  public void setResume(String resume) {
    this.resume = resume;
  }

  public String getDateAvailable() {
    return dateAvailable;
  }

  public void setDateAvailable(String dateAvailable) {
    this.dateAvailable = dateAvailable;
  }

  public String getCoverLetter() {
    return coverLetter;
  }

  public void setCoverLetter(String coverLetter) {
    this.coverLetter = coverLetter;
  }

  public String getHighestEducation() {
    return highestEducation;
  }

  public void setHighestEducation(String highestEducation) {
    this.highestEducation = highestEducation;
  }

  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  public String getDesiredSalary() {
    return desiredSalary;
  }

  public void setDesiredSalary(String desiredSalary) {
    this.desiredSalary = desiredSalary;
  }

  public String getCollege() {
    return college;
  }

  public void setCollege(String college) {
    this.college = college;
  }

  public String getReferredBy() {
    return referredBy;
  }

  public void setReferredBy(String referredBy) {
    this.referredBy = referredBy;
  }

  public String getLinkedinUrl() {
    return linkedinUrl;
  }

  public void setLinkedinUrl(String linkedinUrl) {
    this.linkedinUrl = linkedinUrl;
  }

  public String getBlog() {
    return blog;
  }

  public void setBlog(String blog) {
    this.blog = blog;
  }

  public String getCompanyTypeCheck() {
    return companyTypeCheck;
  }

  public void setCompanyTypeCheck(String companyTypeCheck) {
    this.companyTypeCheck = companyTypeCheck;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public Date getModified() {
    return modified;
  }

  public void setModified(Date modified) {
    this.modified = modified;
  }

  public String getEthnicity() {
    return ethnicity;
  }

  public void setEthnicity(String ethnicity) {
    this.ethnicity = ethnicity;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getDisability() {
    return disability;
  }

  public void setDisability(String disability) {
    this.disability = disability;
  }

  public Long getHiringLeadId() {
    return hiringLeadId;
  }

  public void setHiringLeadId(Long hiringLeadId) {
    this.hiringLeadId = hiringLeadId;
  }

  public String getInternalJobCode() {
    return internalJobCode;
  }

  public void setInternalJobCode(String internalJobCode) {
    this.internalJobCode = internalJobCode;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getJobId() {
    return jobId;
  }

  public void setJobId(Long jobId) {
    this.jobId = jobId;
  }

  public Long getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(Long organizationId) {
    this.organizationId = organizationId;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public Long getCollaboratorId() {
    return collaboratorId;
  }

  public void setCollaboratorId(Long collaboratorId) {
    this.collaboratorId = collaboratorId;
  }

  public String getEmployeeNotes() {
    return employeeNotes;
  }

  public void setEmployeeNotes(String employeeNotes) {
    this.employeeNotes = employeeNotes;
  }

  public Date getCollaboratorDate() {
    return collaboratorDate;
  }

  public void setCollaboratorDate(Date collaboratorDate) {
    this.collaboratorDate = collaboratorDate;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getVeteranStatus() {
    return veteranStatus;
  }

  public void setVeteranStatus(String veteranStatus) {
    this.veteranStatus = veteranStatus;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public List<CandidateCustomQuestionR> getCustomQuestionModels() {
    return customQuestionModels;
  }

  public void setCustomQuestionModels(List<CandidateCustomQuestionR> customQuestionModels) {
    this.customQuestionModels = customQuestionModels;
  }

  public Long getAdminId() {
    return adminId;
  }

  public void setAdminId(Long adminId) {
    this.adminId = adminId;
  }

  public long getRating() {
    return rating;
  }

  public void setRating(long rating) {
    this.rating = rating;
  }

  public String getResumeName() {return resumeName;}

  public void setResumeName(String resumeName) {this.resumeName=resumeName;}

  public static Map<String, FieldOptions> getValidatorMap() {
    return candidateFieldOptionsMap;
  }


  private static Map<String, FieldOptions> candidateFieldOptionsMap = new HashMap<String, FieldOptions>()
  {{
    put("candidate_id", FieldOptionsCommon.NUMBER);
    put("id", FieldOptionsCommon.NUMBER);
    put("first_name", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 100));
    put("last_name", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 100));
    put("email", new FieldOptionsImpl(FieldOptionsCommon.EMAIL, -1, 100));
    put("phone_number", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 20));
    put("region_code", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 10));
    put("unit", FieldOptionsCommon.NUMBER);
    put("source", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETS_HTML_ESCAPE, -1, 50));
    put("address_line", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 255));
    put("city", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 100));
    put("state", FieldOptionsCommon.NUMBER);
    put("country", FieldOptionsCommon.NUMBER);
    put("zip", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 50));
    put("gender", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 20));
    put("resume", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
    put("cover_letter", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
    put("hiring_lead", FieldOptionsCommon.NUMBER);
    put("internal_jobcode", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 100));
    put("employment_type", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 50));
    put("location", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 50));
    put("job_title", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 50));
    put("job_id", FieldOptionsCommon.NUMBER);
    put("user_id", FieldOptionsCommon.NUMBER);
    put("department", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 50));
    put("minimum_experience", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 50));
    put("job_status", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 100));
    put("reference", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 255));
    put("highest_education", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 255));
    put("desired_salary", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
    put("linkedin_url", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 500));
    put("date_available", new FieldOptionsImpl(FieldOptionsCommon.DATE_FORMAT, -1, 20));
    put("blog", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
    put("referred_by", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 255));
    put("college", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 255));
    put("company_type_check", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 255));
    put("veteran_status", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 50));
    put("ethnicity", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 50));
    put("disability", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 50));
    put("organization_id", FieldOptionsCommon.NUMBER);
    put("custom_answer", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 500));
    put("custom_question", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 500));
    put("limit", FieldOptionsCommon.NUMBER);
    put("offset", FieldOptionsCommon.NUMBER);
    put("EmailList", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 100));
    put("InterviewDetails", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 100));
    put("OfferDetails", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 100));
    put("CommentsList", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 100));
    put("fields", FieldOptionsCommon.ANY);
    put("cand_status", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 100));
    put("customComments", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 500));
    put("admin_name",FieldOptionsCommon.ALPHABETSWITHSPACE);
    put("comments", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 500));
    put("creator_id", FieldOptionsCommon.NUMBER);
    put("employee_id", FieldOptionsCommon.NUMBER);
    put("notes", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 500));
    put("rating", FieldOptionsCommon.NUMBER);
    put("candidatestatus",new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 500));
    put("title", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 100));
    put("status", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 80));
    put("candidateId", FieldOptionsCommon.NUMBER);
    put("searchFilter",FieldOptionsCommon.ANY);
    put("location",FieldOptionsCommon.ANY);
    put("position",FieldOptionsCommon.ANY);
    put("resume_name",FieldOptionsCommon.ANY);
    put("collaborator_id", FieldOptionsCommon.NUMBER);
  }};

}
