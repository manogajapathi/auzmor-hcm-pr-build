package com.auzmor.impl.common;

import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

public class FieldOptionsCommon {
  public static FieldOptions EMAIL = new FieldOptionsImpl(
      "^[_A-Za-z0-9-+]+(.[_A-Za-z0-9-]+)*@" +
          "[A-Za-z0-9-]+(.[A-Za-z0-9]+)*(.[A-Za-z]{2,})$");
  public static FieldOptions NUMBER = new FieldOptionsImpl("^[0-9]+$");
  public static FieldOptions DECIMAL = new FieldOptionsImpl(
      "^([0-9]*(\\.[0-9]+))|([0-9]+)$");

  public static FieldOptions DATE_FORMAT = new FieldOptionsImpl(
      "(0|1)[0-9]\\/[0-3][0-9]\\/(19|20)[0-9]{2}");
  public static FieldOptions ALPHABETSWITHSPACE = new FieldOptionsImpl("^[a-zA-z,\\s]+$");
  public static FieldOptions ALPHANUMERICWITHSPACE = new FieldOptionsImpl("^[a-zA-z0-9,\\s]+$");
  public static FieldOptions NUMBERWITHCOMMA = new FieldOptionsImpl("^[0-9,]+$");


  public static FieldOptions ALPHANUMERIC = new FieldOptionsImpl("^[a-zA-Z0-9]$");
  public static FieldOptions ALPHABETS = new FieldOptionsImpl("^[a-zA-Z]$");
  public static FieldOptions ANY = new FieldOptionsImpl("^(.+(\\n)*)+$");

  public static FieldOptions ALPHABETS_HTML_ESCAPE = new FieldOptionsImpl("^[a-zA-Z]$")
      .setHtmlEscape(true);
  public static FieldOptions ANY_HTML_ESCAPE = new FieldOptionsImpl("^(.+(\\n)*)+$").setHtmlEscape(true);
  public static FieldOptions SPECIAL_CHARACTERS = new FieldOptionsImpl("[^a-zA-Z0-9 ]");
  public static FieldOptions PASSWORD_LENGTH =
      new FieldOptionsImpl("^(?=.*[a-zA-Z0-9]).{8,20}",
          false, -1, 8);

  public static FieldOptions ANY_ALLOW_EMPTY = new FieldOptionsImpl("^.+$", true);
  public static FieldOptions ANY_VALUES = new FieldOptionsImpl("^(.+(\\n)*)+$", true);
  public static FieldOptions DOMAIN = new FieldOptionsImpl("[^\\.]+\\.[^\\.]+\\.[^\\.]{2,}");
  //public static FieldOptions I18_PHONE_NUMBER = new FieldOptionsImpl("");
  public static FieldOptions BOOLEAN = new FieldOptionsImpl("^(true|false)$");
  public static FieldOptions NUMBER_ALLOW_EMPTY = new FieldOptionsImpl("^[0-9]+$", true);
}

