package com.auzmor.impl.handler;

import com.auzmor.bo.EmployeeBO;
import com.auzmor.bo.LoginBO;
import com.auzmor.bo.OrganizationBO;
import com.auzmor.dao.LoginDAO;
import com.auzmor.impl.bo.*;
import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.dao.LoginDAOImpl;
import com.auzmor.impl.enumarator.bo.InviteBOEnum;
import com.auzmor.impl.enumarator.kong.KongHttpStatusCode;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.*;
import com.auzmor.bo.InviteBO;
import com.auzmor.impl.model.Invite;
import com.auzmor.impl.model.Login;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.Role;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.FieldOptions;
import com.auzmor.util.validator.Validator;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.*;

public class InviteHandler {

  private static Logger logger = LoggerFactory.getLogger(InviteHandler.class);

  /**
   * Sends a new Invite to the employee
   *
   * @param routingContext
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */
  public static void inviteNewEmployee(RoutingContext routingContext) {
    logger.info("Routing a invite a new employee");
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"email", "organization_id"};
    String[] requiredParams = {"email", "organization_id"};
    String[] inviteParams = {"email", "organization_id"};

    List<Validator> validators = new ArrayList<>();
    Validator inviteValidator = new ValidatorImpl(inviteParams, Invite.getValidatorMap());
    validators.add(inviteValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, true,
        (httpServerResponse, requestParams, dbHandle) -> {
      String email = params.get("email");
      Long organizationId = Long.parseLong(params.get("organization_id"));

      InviteBO inviteBO = new InviteBOImpl(dbHandle);
      InviteBOEnum inviteBOEnum = inviteBO.inviteNewEmployee(email, organizationId);

      InviteHandler.writeResponse(params, response, dbHandle, null,
          inviteBOEnum);
    });
  }

  /**
   * Validates the token status
   *
   * @param routingContext
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */
  public static void validateTokenStatus(RoutingContext routingContext) {
    logger.info("Routing check valid status token");
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"token", "email", "organization_id"};
    String[] requiredParams = {"token", "email", "organization_id"};
    String[] inviteParams = {"email", "organization_id"};
    String[] tokenParams = {"token"};

    List<Validator> validators = new ArrayList<>();
    Validator inviteValidator = new ValidatorImpl(inviteParams, Invite.getValidatorMap());
    Validator tokenValidator = new ValidatorImpl(tokenParams, new HashMap<String, FieldOptions>() {{
      put("token", FieldOptionsCommon.ANY);
    }});
    validators.add(inviteValidator);
    validators.add(tokenValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          InviteBOEnum inviteBOEnum = InviteBOEnum.TOKEN_NOT_VALID;
          InviteBO inviteBO = new InviteBOImpl(dbHandle);

          if (inviteBO.hasInviteForEmail(params.get("email"),
              Long.parseLong(params.get("organization_id")))) {
            inviteBOEnum = InviteBOEnum.TOKEN_VALID;
          }
          InviteHandler.writeResponse(params, response, dbHandle, null,
              inviteBOEnum);
        });
  }

  /**
   * Validates the user with token and add the user
   *
   * @param routingContext
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */
  public static void addInvitedEmployee(RoutingContext routingContext) {
    logger.info("Routing add invited employee");
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"token", "organization_id", "email", "password", "confirm_password",
        "first_name", "last_name","user_type"};
    String[] requiredParams = {"token", "organization_id", "email", "password", "confirm_password",
        "first_name", "last_name"};
    String[] inviteParams = {"organization_id", "email"};
    String[] anyTypeParams = {"token", "password", "confirm_password"};
    String[] employeeParams = {"first_name", "last_name","user_type"};

    List<Validator> validators = new ArrayList<>();
    Validator inviteValidator = new ValidatorImpl(inviteParams, Invite.getValidatorMap());
    Validator anyTypeValidator = new ValidatorImpl(anyTypeParams, new HashMap<String, FieldOptions>() {{
      put("token", FieldOptionsCommon.ANY);
      put("password", new FieldOptionsImpl(FieldOptionsCommon.ANY, 8, 20));
      put("confirm_password", new FieldOptionsImpl(FieldOptionsCommon.ANY, 8, 20));
    }});
    Validator employeeValidator = new ValidatorImpl(employeeParams, new HashMap<String, FieldOptions>() {{
      put("first_name", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 50));
      put("last_name", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 50));
      put("user_id", FieldOptionsCommon.NUMBER);
      put("organization_id", FieldOptionsCommon.NUMBER);
      put("limit", FieldOptionsCommon.NUMBER);
      put("offset", FieldOptionsCommon.NUMBER);
      put("profile_image", FieldOptionsCommon.ANY);
      put("email", FieldOptionsCommon.ANY);
      put("primary_phone", FieldOptionsCommon.ANY);
      put("access_level", FieldOptionsCommon.ANY);
      put("department", FieldOptionsCommon.ANY);
      put("job_title", FieldOptionsCommon.ANY);
      put("city", FieldOptionsCommon.ANY);
      put("state", FieldOptionsCommon.ANY);
      put("country", FieldOptionsCommon.ANY);
      put("user_type",new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 100));
    }});
    validators.add(inviteValidator);
    validators.add(anyTypeValidator);
    validators.add(employeeValidator);


    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHandle dbHandle = DbHelper.getDBHandle(routingContext, false);
    if (null == dbHandle) {
      return;
    }
    InviteBO inviteBO = new InviteBOImpl(dbHandle);
    ReturnObject<InviteBOEnum, Employee> returnObject = null;
    try {
      returnObject = inviteBO.addInvitedEmployee(params);
    } catch (SQLException e) {
      logger.error(e.getStackTrace().toString());
      DbHelper.safeRollback(dbHandle);
      dbHandle.checkAndCloseConnection();
      ResponseHelper.writeInternalServerError(response);
    } catch (DbHandleException e) {
      logger.error(e.getStackTrace().toString());
      DbHelper.safeRollback(dbHandle);
      dbHandle.checkAndCloseConnection();
      ResponseHelper.writeInternalServerError(response);
    }
    ReturnObject<InviteBOEnum, Employee> inviteBOReturnObject = returnObject;
    if (inviteBOReturnObject.getStatusEnum() == InviteBOEnum.EMPLOYEE_ADDED) {
      HttpClient kongClient = KongServiceHelper.executeWithKongClient(routingContext.vertx());
      logger.info("Adding consumer to Api gateway");
      KongServiceHelper.addConsumer(kongClient, inviteBOReturnObject.getData().getUser_id(), clientResponse -> {
          if (clientResponse.statusCode() == KongHttpStatusCode.ADD.getStatusCode()) {
            logger.info("Consumer added successfully");
            clientResponse.bodyHandler(buffer -> {
              String consumerId = new JsonObject(buffer).getString("id");
              Role role = null;
              try {
                new LoginBOImpl(dbHandle).updateConsumerId(consumerId, inviteBOReturnObject.getData().getUser_id());
                role = new RoleBOImpl(dbHandle).getByName("Employee", Long.parseLong(params.get("organization_id")));
              } catch (SQLException e) {
                logger.error(e.getStackTrace().toString());
                DbHelper.safeRollback(dbHandle);
                dbHandle.checkAndCloseConnection();
                kongClient.close();
                ResponseHelper.writeInternalServerError(response);
              } catch (DbHandleException e) {
                logger.error(e.getStackTrace().toString());
                DbHelper.safeRollback(dbHandle);
                dbHandle.checkAndCloseConnection();
                kongClient.close();
                ResponseHelper.writeInternalServerError(response);
              }

              boolean consumerAdded = false;
              KongServiceHelper.addConsumerRoles(kongClient, consumerId, "" + role.getId(), httpClientResponse -> {
                logger.info("Adding consumer rolea as employee");
               try {
                  /*InviteHandler.writeResponse(params, response, dbHandle, null,
                      inviteBOReturnObject.getStatusEnum());*/
                 dbHandle.commit();


                 long organizationId = Long.parseLong(params.get("organization_id"));
                 LoginDAO loginDAO = new LoginDAOImpl(dbHandle);
                 Login loginModel = loginDAO.getLoginUserDetails(inviteBOReturnObject.getData().getUser_id());
                 OrganizationBO organizationBO = new OrganizationBOImpl(dbHandle);

                 Organization organization = organizationBO.getOrganizationDetails(organizationId);
                 long jobCount = new JobBOImpl(dbHandle).getJobCount(organizationId);

                 String key = "auzmor-" + UUID.randomUUID().toString() + "-hcm";
                 String secret = UUID.randomUUID().toString();
                 HttpClient httpClient = KongServiceHelper.executeWithKongClient(routingContext.vertx());
                 logger.info("Creating a session");
                 Map<Long, String> rolesMap = organizationBO.getAllRolesAsMap(organizationId);
                 KongServiceHelper.createCredential(httpClient, loginModel.getConsumerId(), key, secret,
                     httpClientResponseForLogin -> {
                       if (httpClientResponseForLogin != null && httpClientResponseForLogin.statusCode() ==
                           KongHttpStatusCode.ADD.getStatusCode()) {
                         httpClientResponseForLogin.bodyHandler(bufferForLogin -> {
                           KongServiceHelper.getConsumerRoles(httpClient, loginModel.getConsumerId(), httpClientAclResponse -> {
                             if (httpClientAclResponse != null && httpClientAclResponse.statusCode() ==
                                 KongHttpStatusCode.GET.getStatusCode()) {
                               httpClientAclResponse.bodyHandler(bufferGroups -> {
                                 try {
                                   JsonObject jsonObject = new JsonObject(bufferGroups);
                                   JsonArray groupArray = jsonObject.getJsonArray("data");
                                   List<String> roles = new ArrayList<>();
                                   for (Map<String, String> groupObject : (List<Map<String, String>>) groupArray.getList()) {
                                     roles.add(rolesMap.get(Long.parseLong(groupObject.get("group"))).toLowerCase());
                                   }
                                   routingContext.addCookie(JwtHelper.generateCookie(key, secret,
                                       request.getHeader("X-Real-Host") != null ? request.getHeader("X-Real-Host") : ""));
                                   httpClient.close();
                                   JsonObject successObject = new JsonObject().put("token",
                                       JwtHelper.getUserInfoJwt(key, secret, jobCount, inviteBOReturnObject.getData(),
                                           roles, loginModel, organization));
                                   dbHandle.checkAndCloseConnection();
                                   ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK, "Success", successObject,
                                       routingContext.response());
                                 } catch (UnsupportedEncodingException e) {
                                   logger.error("Unable to create session");
                                   httpClient.close();
                                   ResponseHelper.writeInternalServerError(routingContext.response());
                                 }
                               });
                             }
                           });
                         });
                       } else {
                         logger.error("Unable to create session in kong");
                         httpClient.close();
                         ResponseHelper.writeInternalServerError(routingContext.response());
                       }
                     });
                  dbHandle.checkAndCloseConnection();
                  kongClient.close();
                }  catch (SQLException e) {
                  logger.error(e.getStackTrace().toString());
                  DbHelper.safeRollback(dbHandle);
                  dbHandle.checkAndCloseConnection();
                  kongClient.close();
                  ResponseHelper.writeInternalServerError(response);
                } catch (DbHandleException e) {
                  logger.error(e.getStackTrace().toString());
                  DbHelper.safeRollback(dbHandle);
                  dbHandle.checkAndCloseConnection();
                  kongClient.close();
                  ResponseHelper.writeInternalServerError(response);
                }

              });
            });
          } else {
            logger.error("Adding Consumer failed");
            kongClient.close();
            DbHelper.safeRollback(dbHandle);
            dbHandle.checkAndCloseConnection();
            ResponseHelper.writeInternalServerError(response);
          }
      });
    } else {
      try {
        InviteHandler.writeResponse(params, response, dbHandle, null, inviteBOReturnObject.getStatusEnum());
        dbHandle.checkAndCloseConnection();
      } catch (SQLException e) {
        DbHelper.safeRollback(dbHandle);
        dbHandle.checkAndCloseConnection();
        ResponseHelper.writeInternalServerError(response);
      } catch (DbHandleException e) {
        DbHelper.safeRollback(dbHandle);
        dbHandle.checkAndCloseConnection();
        ResponseHelper.writeInternalServerError(response);
      }
    }
  }

  /**
   * Sends a new Invite to the employee
   *
   * @param routingContext
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */
  public  static void inviteResend(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"email", "organization_id"};
    String[] requiredParams = {"email", "organization_id"};
    String[] inviteParams = {"email", "organization_id"};

    List<Validator> validators = new ArrayList<>();
    Validator inviteValidator = new ValidatorImpl(inviteParams, Invite.getValidatorMap());
    validators.add(inviteValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          /*InviteBO inviteBO = new InviteBOImpl(dbHandle);
          ReturnObject<InviteBOEnum, JsonObject> returnObject = inviteBO.inviteResend(
              requestParams.get("email"), Long.parseLong(requestParams.get("organization_id")));
          InviteHandler.writeResponse(requestParams, httpServerResponse, dbHandle, returnObject);*/
        });
  }

  /**
   * Writes corresponding response based on the InviteBOEnum
   *
   * @param params
   * @param httpServerResponse
   * @param dbHandle
   * @param inviteBOEnum
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */
  public static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle, JsonObject jsonObject,
                                   InviteBOEnum inviteBOEnum)
      throws SQLException, DbHandleException {

    switch (inviteBOEnum) {
      case EXISTS_ALREADY:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_CONFLICT,
            params.get("email") + " is already a part of the organization",
            "Cannot invite as the invited person already exists in your organization",
            httpServerResponse);
        break;
      case PENDING:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_CONFLICT,
            "Invitation already sent for " + params.get("email"),
            "Already an existing invite is found for the email in database",
            httpServerResponse);
        break;
      case SEND_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_CONFLICT,
            "Invitation already sent for " + params.get("email"),
            "Sending failed due to ",
            httpServerResponse);
        break;
      case INVITE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Invitation successfully sent", (JsonObject) null, httpServerResponse);
        break;
      case TOKEN_NOT_VALID:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_NOT_ACCEPTABLE,
            "Token not valid",
            "Token could be either used or the admin has deleted",
            httpServerResponse);
        break;
      case TOKEN_VALID:
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Token is active", jsonObject, httpServerResponse);
        break;
      case EMPLOYEE_ADD_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_CONFLICT,
            "Unable to signup", "User cannot be added",
            httpServerResponse);
        break;
      case EMPLOYEE_ADDED:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Sign up successful", jsonObject, httpServerResponse);
        break;
      case CANCEL_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Cancelled the invite", jsonObject, httpServerResponse);
        break;

      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }

}