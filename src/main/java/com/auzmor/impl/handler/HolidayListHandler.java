package com.auzmor.impl.handler;

import com.auzmor.impl.builder.ResponseBuilder;
import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.model.Holiday.HolidayListModel;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.impl.util.ModelUtil;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.builder.ApiBuilder;

import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.RoutingContext;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HolidayListHandler extends ApiBuilder {
  private static final Logger logger = LoggerFactory.getLogger(HolidayListHandler.class);
  /**
   * To create a new list for particular a year
   * which is organization specific
   *
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @modifiedDate 22/01/2018
   */
  public void addHolidayList(RoutingContext routingContext) {
    HolidayListModel listModel = new HolidayListModel();
    MultiMap params = routingContext.request().params();
    String departmentParam = "";
    String locationParam = "";
    String divisionParam = "";
    String payGroupParam = "";
    try {
      Connection connectionObject = new MysqldbConf().getConnection();
      for (String key : params.names()) {
        if (key.contains("department")) {
          departmentParam = params.get(key);
        } else if (key.contains("location")) {
          locationParam = params.get(key);
        } else if (key.contains("division")) {
          divisionParam = params.get(key);
        } else if (key.contains("pay_group")) {
          payGroupParam = params.get(key);
        } else {
          ModelUtil.setValues(key, params.get(key), listModel);
        }
      }

      if (params.get("assign_to").contains("Specific group of people")) {
        String departmentString = "";
        String divisionString = "";
        String locationString = "";
        String payGroupString = "";

        if (!StringUtil.printValidString(departmentParam).equals("{}")
            && !StringUtil.printValidString(departmentParam).equals("")) {
          departmentString = calculateFromArray(departmentParam);
          listModel.setDepartment(departmentString);
          params.add("department", departmentString);
        } else {
          departmentString = "";
          listModel.setDepartment(departmentString);
          params.add("department", "");
        }

        if (!StringUtil.printValidString(locationParam).equals("{}")
            && !StringUtil.printValidString(locationParam).equals("")) {
          locationString = calculateFromArray(locationParam);
          listModel.setLocation(locationString);
          params.add("location", locationString);
        } else {
          locationString = "";
          listModel.setLocation(locationString);
          params.add("location", locationString);
        }

        if (!StringUtil.printValidString(divisionParam).equals("{}")
            && !StringUtil.printValidString(divisionParam).equals("")) {
          divisionString = calculateFromArray(divisionParam);
          listModel.setDivision(divisionString);
          params.add("division", divisionString);
        } else {
          divisionString = "";
          listModel.setDivision(divisionString);
          params.add("division", divisionString);
        }

        if (!StringUtil.printValidString(payGroupParam).equals("{}")
            && !StringUtil.printValidString(payGroupParam).equals("")) {
          payGroupString = calculateFromArray(payGroupParam);
          listModel.setPay_group(payGroupString);
          params.add("pay_group", payGroupString);
        } else {
          payGroupString = "";
          listModel.setPay_group(payGroupString);
          params.add("pay_group", payGroupString);
        }
      }

      List<String> constraintList = new ArrayList<>();

      constraintList.add("inactive_status = 0");
      constraintList.add("list_name = '" + listModel.getList_name() + "'");
      constraintList.add("year = '" + listModel.getYear() + "'");
      constraintList.add("organization_id = " + listModel.getOrganization_id());

      String constraintString = DatabaseUtil.buildConstraintString(constraintList);

      Map<String, String> queryStringParameters = new HashMap<>();
      queryStringParameters.put("fields", "list_id");
      queryStringParameters.put("table", "HolidayList");
      queryStringParameters.put("constraint", constraintString);

      String holidayListQueryString = buildQuery(queryStringParameters, "SELECT");  // to check list name if already existing in that year

      ResultSet resultSetHoliday = getResultSetObject(connectionObject, holidayListQueryString);

      int existingId = 0;
      while (resultSetHoliday.next()) {
        existingId = resultSetHoliday.getInt("list_id");
      }

      if (existingId != 0) {
        throw new IllegalArgumentException("List Name and Year already exists");  // already a specific list exists for that year
      }

      if (params.get("assign_to").contains("Specific group of people")) {
        System.out.println(StringUtil.printValidString(listModel.getDepartment()).equals(""));
        if (StringUtil.printValidString(listModel.getLocation()).equals("")
            && StringUtil.printValidString(listModel.getDivision()).equals("")
            && StringUtil.printValidString(listModel.getDepartment()).equals("")
            && StringUtil.printValidString(listModel.getPay_group()).equals("")) {
          throw new IllegalArgumentException("Please select any group");
        }
      }

      Map<String, Boolean> paramMapper = new HashMap<>();

            /*
            following parameters are assign as true will be treated as mandatory parameters
            and will throw exception
            if the parameter values are missing
             */

      paramMapper.put("list_name", true);
      paramMapper.put("year", true);
      paramMapper.put("organization_id", true);
      paramMapper.put("assign_to", false);
      paramMapper.put("division", false);
      paramMapper.put("department", false);
      paramMapper.put("pay_group", false);
      paramMapper.put("location", false);
      // paramMapper.put("employees",false);
      paramMapper.put("paid_flag", false);

      Map<String, Object> insertionObjects = generateInsertionObject(params, paramMapper, listModel);

      List<String> queryStringValueList = DatabaseUtil.prepareQueryStringValues(insertionObjects);
      String columnValueString = queryStringValueList.get(0);
      String entryValueString = queryStringValueList.get(1);

      columnValueString = columnValueString + ",completed_status";  // assigning boolean true for completed status
      entryValueString = entryValueString + ",0";                  // since it is a new list

      Map<String, String> insertQueryParameters = new HashMap<>();
      insertQueryParameters.put("table", "HolidayList");
      insertQueryParameters.put("column_names", columnValueString);
      insertQueryParameters.put("column_values", entryValueString);

      String sqlQuery = buildQuery(insertQueryParameters, "INSERT");  // inserting new list for particular year

      int id = insertRecord(sqlQuery);
      listModel.setList_id(id);

      JsonObject successObject = new JsonObject();
      JsonArray tempArray;
      successObject.put("list_id", listModel.getList_id());
      successObject.put("list_name", listModel.getList_name());
      successObject.put("year", listModel.getYear());
      successObject.put("organization_id", listModel.getOrganization_id());
      successObject.put("assign_to", listModel.getAssign_to());

      if (!(StringUtil.printValidString(listModel.getLocation()).equals(""))) {
        String locationFromDB = listModel.getLocation();
        String[] locations = locationFromDB.split(",");

        tempArray = new JsonArray();
        for (String location : locations) {
          tempArray.add(location);
        }

        successObject.put("location", tempArray);
      } else {
        successObject.put("location", new JsonArray());
      }

      if (!(StringUtil.printValidString(listModel.getDepartment()).equals(""))) {
        String departmentFromDB = listModel.getDepartment();
        String[] departments = departmentFromDB.split(",");

        tempArray = new JsonArray();
        for (String department : departments) {
          tempArray.add(department);
        }

        successObject.put("department", tempArray);
      } else {
        successObject.put("department", new JsonArray());
      }

      if (!(StringUtil.printValidString(listModel.getDivision()).equals(""))) {
        String divisionFromDB = listModel.getDivision();
        String[] divisions = divisionFromDB.split(",");

        tempArray = new JsonArray();
        for (String division : divisions) {
          tempArray.add(division);
        }

        successObject.put("division", tempArray);
      } else {
        successObject.put("division", new JsonArray());
      }

      if (!(StringUtil.printValidString(listModel.getPay_group()).equals(""))) {
        String[] payGroups = listModel.getPay_group().split(",");
        tempArray = new JsonArray();
        for (String payGroup : payGroups) {
          tempArray.add(payGroup);
        }

        successObject.put("pay_group", tempArray);
      } else {
        successObject.put("pay_group", new JsonArray());
      }

            /*successObject.put("division", listModel.getDivision());
            successObject.put("department", listModel.getDepartment());
            successObject.put("location", listModel.getLocation());
            successObject.put("pay_group", listModel.getPay_group());*/
      //  successObject.put("employees", listModel.getEmployees());
      successObject.put("paid_flag", listModel.isPaid_flag());
      successObject.put("completed_status", true);

      connectionObject.close();
      getSuccessObject(successObject, routingContext.response(), "POST");


    } catch (SQLException exception) {
      logger.error(exception.getMessage());
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (IllegalArgumentException exception) {
      logger.error(exception.getMessage());
      String errorDescription = exception.getMessage();
      getErrorObject("Invalid Data", errorDescription,
          routingContext.response(), "POST");
    } catch (ClassNotFoundException exception){
      logger.error(exception.getMessage());
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (Exception exception) {
      logger.error(exception.getMessage());
      ResponseBuilder.writeInternalServerError(routingContext.response());
    }

  }

  /**
   * to update particular holiday list present, where list id, list name and year are provided
   * as mandatory parameters
   *
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @modifiedDate 22/01/2018
   */
  public void updateHolidayList(RoutingContext routingContext) {
    String list_id = routingContext.request().getParam("listId");

    MultiMap params = routingContext.request().params();
    params.set("list_id", list_id);
    JsonObject jsonResponseObject = new JsonObject();

    String updateString = "";
    String departmentParam = "";
    String locationParam = "";
    String divisionParam = "";
    String payGroupParam = "";
    try {
      Connection connection = new MysqldbConf().getConnection();

      for (String key : params.names()) {
        if (key.contains("department")) {
          departmentParam = params.get(key);
        } else if (key.contains("location")) {
          locationParam = params.get(key);
        } else if (key.contains("division")) {
          divisionParam = params.get(key);
        } else if (key.contains("pay_group")) {
          payGroupParam = params.get(key);
        }
      }

      String departmentString = "";
      String divisionString = "";
      String locationString = "";
      String payGroupString = "";


      if (!StringUtil.printValidString(departmentParam).equals("{}")
          && !StringUtil.printValidString(departmentParam).equals("")) {
        departmentString = calculateFromArray(departmentParam);
        params.add("department", departmentString);
      } else {
        departmentString = "";
        params.add("department", departmentString);
      }

      if (!StringUtil.printValidString(locationParam).equals("{}")
          && !StringUtil.printValidString(locationParam).equals("")) {
        locationString = calculateFromArray(locationParam);
        params.add("location", locationString);
      } else {
        locationString = "";
        params.add("location", locationString);
      }

      if (!StringUtil.printValidString(divisionParam).equals("{}")
          && !StringUtil.printValidString(divisionParam).equals("")) {
        divisionString = calculateFromArray(divisionParam);
        params.add("division", divisionString);
      } else {
        divisionString = "";
        params.add("division", divisionString);
      }

      if (!StringUtil.printValidString(payGroupParam).equals("{}")
          && !StringUtil.printValidString(payGroupParam).equals("")) {
        payGroupString = calculateFromArray(payGroupParam);
        params.add("pay_group", payGroupString);
      } else {
        payGroupString = "";
        params.add("pay_group", payGroupString);
      }


      String updateStatement = generateUpdateStatement(params, new HolidayListModel());

      if (list_id.equals("") || list_id.equals(null) ||
          params.get("list_name").equals(null) || params.get("list_name").equals("") ||
          params.get("year").equals(null) || params.get("year").equals("")) {
        throw new IllegalArgumentException("Required Params Missing");  // list id, list name and year are mandatory parameters
      }

      if (params.get("assign_to").contains("Specific group of people")) {
        if (StringUtil.printValidString(params.get("location")).equals("")
            && StringUtil.printValidString(params.get("department")).equals("")
            && StringUtil.printValidString(params.get("division")).equals("")
            && StringUtil.printValidString(params.get("pay_group")).equals("")
            ) {

          throw new IllegalArgumentException("Please select any group");
        }


      }

      String constraintString = "where list_id = " + list_id;  // adding constraint for particular list based on id

      Map<String, String> updateQueryParameters = new HashMap<>();
      updateQueryParameters.put("table", "HolidayList");
      updateQueryParameters.put("key_value_pair", updateStatement);
      updateQueryParameters.put("constraint", constraintString);

      updateString = buildQuery(updateQueryParameters,
          "UPDATE");  // query for updating particular list

      int updatedRows = getUpdatedRows(updateString);
      if (updatedRows == 0) {
        throw new IllegalArgumentException("Invalid request to update Holiday List");
      }

      connection.close();

      DatabaseUtil dbutil = new DatabaseUtil();
      String name = dbutil.getTablename("HolidayList",
          "list_name",
          "list_id = " + list_id); // to retritive the list name which will be shown in the message

      jsonResponseObject.put("message", " " + name + " updated Successfully! ");

      getSuccessObject(jsonResponseObject, routingContext.response(), "PUT");

    } catch (IllegalArgumentException exception) {
      logger.error(exception.getMessage());
      String errorDescription = exception.getMessage();
      getErrorObject("Invalid Data", errorDescription, routingContext.response(), "PUT");
    } catch (SQLException exception) {
      logger.error(exception.getMessage());
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (Exception exception) {
      logger.error(exception.getMessage());
      ResponseBuilder.writeInternalServerError(routingContext.response());
    }

  }

  /**
   * Gets the details of particular list which will be fetched
   * with the use of list id passed as mandatory parameter
   *
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @modifiedDate 23/12/2017
   */
  public void getHolidayList(RoutingContext routingContext) {
    try {
      Connection connectionObject = new MysqldbConf().getConnection();
      HttpServerRequest request = routingContext.request();

      String id = request.getParam("organization_id");

      String listIdString = request.getParam("list_id");

      List<String> constraintList = new ArrayList<>();

      if (id != null) {
        constraintList.add("organization_id = " + id);
      }

      constraintList.add("inactive_status = 0");

      if (listIdString.equals("") || listIdString.equals(null))
        throw new IllegalArgumentException("Required Params missing");

      if (listIdString != null || !(listIdString.equals(""))) {
        constraintList.add("list_id = " + listIdString);
      } else {
        throw new IllegalArgumentException("Required Params missing");  // list id is mandatory parameter.
      }

      String constraintString = DatabaseUtil.buildConstraintString(constraintList);

      Map<String, String> queryStringParameters = new HashMap<>();
      queryStringParameters.put("fields", "list_id,list_name,year,assign_to,location,department," +
          "division,pay_group,paid_flag");
      queryStringParameters.put("table", "HolidayList");
      queryStringParameters.put("constraint", constraintString);

      String holidayListQueryString = buildQuery(queryStringParameters,
          "SELECT");  // to fetch the list details according to the list id

      String columnName = "";
      String columnType = "";


      ResultSet resultSetHoliday = getResultSetObject(connectionObject, holidayListQueryString);

      JsonObject resultObject = new JsonObject();
      JsonArray tempArray;

      while (resultSetHoliday.next()) {
        ResultSetMetaData resultSetMetaData = resultSetHoliday.getMetaData();
        for (int i = 1; i <= resultSetHoliday.getMetaData().getColumnCount(); i++) {
          columnName = resultSetMetaData.getColumnName(i);
          columnType = resultSetMetaData.getColumnTypeName(i);

                    /*  result data are retrieved according to data types   */
          if ("BIT".equalsIgnoreCase(columnType)) {
            resultObject.put(columnName, resultSetHoliday.getBoolean(columnName));
          } else if ("INT".equalsIgnoreCase(columnType)) {
            resultObject.put(columnName, resultSetHoliday.getInt(columnName));
          } else if ("VARCHAR".equalsIgnoreCase(columnType) || "BLOB".equalsIgnoreCase(columnType)) {
            String columnValue = resultSetHoliday.getString(columnName);
            resultObject.put(columnName, columnValue);
          } else if ("Date".equalsIgnoreCase(columnType)) {
            resultObject.put(columnName, resultSetHoliday.getString(columnName));
          }
        }

        if (!(StringUtil.printValidString(resultObject.getString("location")).equals(""))) {
          String locationFromDB = resultObject.getString("location");
          String[] locations = locationFromDB.split(",");

          tempArray = new JsonArray();
          for (String location : locations) {
            tempArray.add(location);
          }

          resultObject.put("location", tempArray);
        } else {
          resultObject.put("location", new JsonArray());
        }

        if (!(StringUtil.printValidString(resultObject.getString("department")).equals(""))) {
          String departmentFromDB = resultObject.getString("department");
          String[] departments = departmentFromDB.split(",");

          tempArray = new JsonArray();
          for (String department : departments) {
            tempArray.add(department);
          }
          StringBuilder builder = new StringBuilder();

          resultObject.put("department", tempArray);
        } else {
          resultObject.put("department", new JsonArray());
        }

        if (!(StringUtil.printValidString(resultObject.getString("division")).equals(""))) {
          String divisionFromDB = resultObject.getString("division");
          String[] divisions = divisionFromDB.split(",");

          tempArray = new JsonArray();
          for (String division : divisions) {
            tempArray.add(division);
          }

          resultObject.put("division", tempArray);
        } else {
          resultObject.put("division", new JsonArray());
        }

        if (!(StringUtil.printValidString(resultObject.getString("pay_group")).equals(""))) {
          String[] payGroups = resultObject.getString("pay_group").split(",");
          tempArray = new JsonArray();
          for (String payGroup : payGroups) {
            tempArray.add(payGroup);
          }

          resultObject.put("pay_group", tempArray);
        } else {
          resultObject.put("pay_group", new JsonArray());
        }
                /* if the list is assigned to a particular employees, details of employees are fetched
                 * according to the employee ids saved in database *//*
                JsonArray employeeArray = new JsonArray();
                if(!(resultObject.getString("employees").equals(""))){
                    String listOfEmployees = resultObject.getString("employees");

                    String[] employees = listOfEmployees.split(",");
                    for(String employee:employees){
                        queryStringParameters = new HashMap<>();
                        queryStringParameters.put("fields","first_name,last_name");
                        queryStringParameters.put("table","Employee");
                        queryStringParameters.put("constraint", "where employee_id = "+employee);

                        String employeeString = buildQuery(queryStringParameters,
                                "SELECT");  //select query to fetch employee details

                        ResultSet employeeResult = getResultSetObject(connectionObject,employeeString);
                        JsonObject employeeObject = new JsonObject();

                        while (employeeResult.next()) {
                            employeeObject.put("first_name",employeeResult.getString(1));
                            employeeObject.put("last_name",employeeResult.getString(2));
                            employeeObject.put("employee_id",employee);
                        }
                        employeeArray.add(employeeObject);
                    }
                }
                resultObject.put("employees",employeeArray);*/

      }

      connectionObject.close();

      getSuccessObject(resultObject, routingContext.response(), "GET");

    } catch (IllegalArgumentException exception) {
      logger.error(exception.getMessage());
      exception.printStackTrace();
      String errorDescription = exception.getMessage();
      getErrorObject("Invalid Data", errorDescription, routingContext.response(), "GET");
    } catch (SQLException exception) {
      logger.error(exception.getMessage());
      exception.printStackTrace();
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (ClassNotFoundException exception) {
      logger.error(exception.getMessage());
      exception.printStackTrace();
      ResponseBuilder.writeInternalServerError(routingContext.response());
    }
  }

  /**
   * To fetch all the holiday lists present in a particular organization
   * along with the details of assigned parameters.
   *
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @modifiedDate 23/12/2017
   */
  public void getAll(RoutingContext routingContext) {
    try {
      Connection connectionObject = new MysqldbConf().getConnection();
      HttpServerRequest request = routingContext.request();

      String id = request.getParam("organization_id");
      String fieldString = request.getParam("fields");

      if (id == null || id.equals("")) {
        throw new IllegalArgumentException("Required Params missing");  // organization id is mandatory
      }

      List<String> constraintList = new ArrayList<>();

      constraintList.add("organization_id = " + id);
      constraintList.add("inactive_status = 0");  // to remove deleted holiday lists

      String constraintString = DatabaseUtil.buildConstraintString(constraintList);
      Map<String, String> queryStringParameters = new HashMap<>();
      if (fieldString != null) {
        queryStringParameters.put("fields", fieldString);
      } else {
        queryStringParameters.put("fields", "list_id,list_name,year,assign_to,location,department," +
            "division,pay_group,paid_flag,completed_status");
      }

      queryStringParameters.put("table", "HolidayList");
      queryStringParameters.put("constraint", constraintString);

      String holidaysQuery = buildQuery(queryStringParameters
          , "SELECT"); // select query for all holiday list which are ordered by year
      holidaysQuery = holidaysQuery + " order by year";

      ResultSet resultSetObject = getResultSetObject(connectionObject, holidaysQuery);

      JsonArray HolidayArray = new JsonArray();
      String columnName = "";
      String columnType = "";

      JsonObject tempObject;
      JsonArray tempArray;

      while (resultSetObject.next()) {
        JsonObject jsonDataObject = new JsonObject();

        jsonDataObject.put("list_id", resultSetObject.getInt("list_id"));
        jsonDataObject.put("list_name", resultSetObject.getString("list_name"));
        jsonDataObject.put("year", resultSetObject.getString("year"));
        jsonDataObject.put("assign_to", resultSetObject.getString("assign_to"));
        String locationFromDB = resultSetObject.getString("location");
        if (!StringUtil.printValidString(locationFromDB).equals("")) {

          String[] locations = locationFromDB.split(",");

          tempArray = new JsonArray();
          for (String location : locations) {
            tempArray.add(location);
          }

          jsonDataObject.put("location", tempArray);
        } else {
          jsonDataObject.put("location", new JsonArray());
        }

        String departmentFromDB = resultSetObject.getString("department");
        if (!StringUtil.printValidString(departmentFromDB).equals("")) {
          String[] departments = departmentFromDB.split(",");

          tempArray = new JsonArray();
          for (String department : departments) {
            tempArray.add(department);
          }

          jsonDataObject.put("department", tempArray);
        } else {
          jsonDataObject.put("department", new JsonArray());
        }

        String divisionFromDB = resultSetObject.getString("division");

        if (!StringUtil.printValidString(divisionFromDB).equals("")) {
          String[] divisions = divisionFromDB.split(",");

          tempArray = new JsonArray();
          for (String division : divisions) {
            tempArray.add(division);
          }

          jsonDataObject.put("division", tempArray);
        } else {
          jsonDataObject.put("division", new JsonArray());
        }
        String payGroupFromDB = resultSetObject.getString("pay_group");
        if (!StringUtil.printValidString(payGroupFromDB).equals("")) {
          String[] payGroups = resultSetObject.getString("pay_group").split(",");
          tempArray = new JsonArray();
          for (String payGroup : payGroups) {
            tempArray.add(payGroup);
          }

          jsonDataObject.put("pay_group", tempArray);
        } else {
          jsonDataObject.put("pay_group", new JsonArray());
        }
        //jsonDataObject.put("employees",resultSetObject.getString("employees"));
        jsonDataObject.put("paid_flag", resultSetObject.getBoolean("paid_flag"));

        if (resultSetObject.getString("completed_status").equals("0")) {
          jsonDataObject.put("completed_status", true);  // holiday details are completely filled in the list
        } else
          jsonDataObject.put("completed_status", false); // some details of holidays are missing in the list


                /* if the list is assigned to a particular employees, details of employees are fetched
                 * according to the employee ids saved in database *//*
                JsonArray employeeArray = new JsonArray();
                if(!(jsonDataObject.getString("employees").equals(""))){
                    String listOfEmployees = jsonDataObject.getString("employees");
                    String[] employees = listOfEmployees.split(",");
                    for(String employee:employees){
                        queryStringParameters = new HashMap<>();
                        queryStringParameters.put("fields","first_name,last_name");
                        queryStringParameters.put("table","Employee");
                        queryStringParameters.put("constraint", "where employee_id = "+employee);

                        String employeeString = buildQuery(queryStringParameters,"SELECT");

                        ResultSet employeeResult = getResultSetObject(connectionObject,employeeString);
                        JsonObject employeeObject = new JsonObject();

                        while (employeeResult.next()) {
                            employeeObject.put("first_name",employeeResult.getString(1));
                            employeeObject.put("last_name",employeeResult.getString(2));
                            employeeObject.put("employee_id",employee);
                        }
                        employeeArray.add(employeeObject);
                    }
                }

                jsonDataObject.put("employees",employeeArray);*/

        HolidayArray.add(jsonDataObject);
      }
      connectionObject.close();

      getSuccessObject(HolidayArray, routingContext.response(), "GET");


    } catch (SQLException exception) {
      logger.error(exception.getMessage());
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (IllegalArgumentException exception) {
      logger.error(exception.getMessage());
      String errorDescription = exception.getMessage();
      getErrorObject("Invalid Data", errorDescription,
          routingContext.response(), "DELETE");
    } catch (ClassNotFoundException exception) {
      logger.error(exception.getMessage());
      ResponseBuilder.writeInternalServerError(routingContext.response());
    }

  }

  /**
   * Delete a particular holiday list where list id is provided as
   * mandatory parameter
   *
   * @param routingContext
   */
  public void deleteHolidayList(RoutingContext routingContext) {
    MultiMap params = routingContext.request().params();
    try {
      int list_id = Integer.valueOf(params.get("list_id"));

      String constraintString = "where list_id = " + list_id;

      Map<String, String> updateQueryParameters = new HashMap<>();
      updateQueryParameters.put("table", "HolidayList");
      updateQueryParameters.put("key_value_pair", "inactive_status = 1");
      updateQueryParameters.put("constraint", constraintString);

      String deletePacketQuery = buildQuery(updateQueryParameters, "UPDATE");

      int deletedRecords = getUpdatedRows(deletePacketQuery);

      if (deletedRecords == 0) {
        throw new IllegalArgumentException("Invalid Request to delete List");
      }
      DatabaseUtil dbutil = new DatabaseUtil();
      String name = dbutil.getTablename("HolidayList", "list_name", "list_id = " + list_id);
      JsonObject dataObject = new JsonObject();
      dataObject.put("message", "" + name + " Holiday List Deleted");

      getSuccessObject(dataObject, routingContext.response(), "DELETE");

    } catch (IllegalArgumentException exception) {
      exception.printStackTrace();
      String errorDescription = exception.getMessage();
      getErrorObject("Invalid Data", errorDescription, routingContext.response(), "DELETE");
    } catch (Exception exception) {
      logger.error(exception.getMessage());
      ResponseBuilder.writeInternalServerError(routingContext.response());
    }
  }

  public void copyHolidayList(RoutingContext routingContext) {
    HolidayListModel listModel = new HolidayListModel();
    MultiMap params = routingContext.request().params();
    try {
      Connection connectionObject = new MysqldbConf().getConnection();
      for (String key : params.names()) {
        ModelUtil.setValues(key, params.get(key), listModel);
      }

      int existingListId = listModel.getList_id();
      if (listModel.getList_id() == 0
          || listModel.getList_name().equals("") ||
          listModel.getYear().equals("") || listModel.getList_name().equals(null) ||
          listModel.getYear().equals(null)) {
        throw new Exception("Required params missing");
      }

      List<String> constraintList = new ArrayList<>();

      constraintList.add("inactive_status = 0");
      constraintList.add("list_name = '" + listModel.getList_name() + "'");
      constraintList.add("year = '" + listModel.getYear() + "'");
      constraintList.add("organization_id = " + listModel.getOrganization_id());

      String constraintString = DatabaseUtil.buildConstraintString(constraintList);

      Map<String, String> queryStringParameters = new HashMap<>();
      queryStringParameters.put("fields", "list_id");
      queryStringParameters.put("table", "HolidayList");
      queryStringParameters.put("constraint", constraintString);

      String holidayListQueryString = buildQuery(queryStringParameters, "SELECT");

      ResultSet resultSetHoliday = getResultSetObject(connectionObject, holidayListQueryString);

      int existingId = 0;
      while (resultSetHoliday.next()) {
        existingId = resultSetHoliday.getInt("list_id");
      }

      if (existingId != 0) {
        throw new Exception("List Name and Year already exists");
      }

      constraintList = new ArrayList<>();
      constraintList.add("inactive_status = 0");
      constraintList.add("list_id = " + existingListId);
      constraintList.add("organization_id = " + listModel.getOrganization_id());
      String holidayConstraint = DatabaseUtil.buildConstraintString(constraintList);

      queryStringParameters = new HashMap<>();
      queryStringParameters.put("fields", "holiday_id");
      queryStringParameters.put("table", "Holiday");
      queryStringParameters.put("constraint", holidayConstraint);

      String CheckHolidayQueryString = buildQuery(queryStringParameters, "SELECT");
      System.out.println(CheckHolidayQueryString);

      ResultSet resultHoliday = getResultSetObject(connectionObject, CheckHolidayQueryString);

      int existingHolidayId = 0;
      while (resultHoliday.next()) {
        existingHolidayId = resultHoliday.getInt("holiday_id");
      }

      if (existingHolidayId == 0) {
        throw new Exception("This list does not contain holidays, Please add holidays to the existing list");
      }

      JsonArray tempArray;
      JsonObject existingList = getExistingHolidayList(connectionObject, String.valueOf(listModel.getList_id()));

      listModel.setList_name(listModel.getList_name());
      listModel.setYear(listModel.getYear());
      listModel.setAssign_to(existingList.getString("assign_to"));
      listModel.setLocation(existingList.getString("location"));
      listModel.setDepartment(existingList.getString("department"));
      listModel.setDivision(existingList.getString("division"));
      listModel.setPay_group(existingList.getString("pay_group"));
      //listModel.setEmployees(existingList.getString("employees"));
      listModel.setPaid_flag(existingList.getBoolean("paid_flag"));
      listModel.setOrganization_id(existingList.getInteger("organization_id"));
      listModel.setCompleted_status(false);

      Map<String, Boolean> paramMapper = new HashMap<>();

      paramMapper.put("list_name", true);
      paramMapper.put("year", true);
      paramMapper.put("organization_id", true);
      paramMapper.put("assign_to", false);
      paramMapper.put("division", false);
      paramMapper.put("department", false);
      paramMapper.put("pay_group", false);
      paramMapper.put("location", false);
      //paramMapper.put("employees",false);
      paramMapper.put("paid_flag", false);

      Map<String, Object> insertionObjects = generateInsertionObject(params, paramMapper, listModel);

      List<String> queryStringValueList = DatabaseUtil.prepareQueryStringValues(insertionObjects);
      String columnValueString = queryStringValueList.get(0);
      String entryValueString = queryStringValueList.get(1);

      columnValueString = columnValueString + ",completed_status";
      entryValueString = entryValueString + ",1";

      Map<String, String> insertQueryParameters = new HashMap<>();
      insertQueryParameters.put("table", "HolidayList");
      insertQueryParameters.put("column_names", columnValueString);
      insertQueryParameters.put("column_values", entryValueString);

      String sqlQuery = buildQuery(insertQueryParameters, "INSERT");

      int id = insertRecord(sqlQuery);

      if (id == 0) {
        throw new SQLException("Database connection error");
      }

      String holidayQueryString = "insert into Holiday(holiday_name,date,assign_to,division,department,pay_group," +
          "location,paid_flag,organization_id) " +
          "select holiday_name,'00/00/0000',assign_to,division,department,pay_group,location," +
          "paid_flag,organization_id from Holiday where list_id=" + existingListId;

      int holidayInsertId = insertRecord(holidayQueryString);
      if (holidayInsertId == 0) {
        throw new IllegalArgumentException("Holiday data did not inserted");
      }

      String constraintUpdateString = "where list_id = 0";

      Map<String, String> updateQueryParameters = new HashMap<>();
      updateQueryParameters.put("table", "Holiday");
      updateQueryParameters.put("key_value_pair", "list_id = " + id);
      updateQueryParameters.put("constraint", constraintUpdateString);


      String updateString = buildQuery(updateQueryParameters, "UPDATE");

      int updatedRows = getUpdatedRows(updateString);
      if (updatedRows == 0) {
        throw new IllegalArgumentException("Invalid request to copy List");
      }


      JsonObject successObject = new JsonObject();

      successObject.put("list_id", id);
      successObject.put("list_name", listModel.getList_name());
      successObject.put("year", listModel.getYear());
      successObject.put("organization_id", listModel.getOrganization_id());
      successObject.put("assign_to", listModel.getAssign_to());

      if (!(listModel.getLocation().equals(""))) {
        String locationFromDB = listModel.getLocation();
        String[] locations = locationFromDB.split(",");

        tempArray = new JsonArray();
        for (String location : locations) {
          tempArray.add(location);
        }

        successObject.put("location", tempArray);
      } else {
        successObject.put("location", new JsonArray());
      }

      if (!(listModel.getDepartment().equals(""))) {
        String departmentFromDB = listModel.getDepartment();
        String[] departments = departmentFromDB.split(",");

        tempArray = new JsonArray();
        for (String department : departments) {
          tempArray.add(department);
        }

        successObject.put("department", tempArray);
      } else {
        successObject.put("department", new JsonArray());
      }

      if (!(listModel.getDivision().equals(""))) {
        String divisionFromDB = listModel.getDivision();
        String[] divisions = divisionFromDB.split(",");

        tempArray = new JsonArray();
        for (String division : divisions) {
          tempArray.add(division);
        }

        successObject.put("division", tempArray);
      } else {
        successObject.put("division", new JsonArray());
      }

      if (!(listModel.getPay_group().equals(""))) {
        String[] payGroups = listModel.getPay_group().split(",");
        tempArray = new JsonArray();
        for (String payGroup : payGroups) {
          tempArray.add(payGroup);
        }

        successObject.put("pay_group", tempArray);
      } else {
        successObject.put("pay_group", new JsonArray());
      }

            /*successObject.put("division", listModel.getDivision());
            successObject.put("department", listModel.getDepartment());
            successObject.put("location", listModel.getLocation());
            successObject.put("pay_group", listModel.getPay_group());*/
      //successObject.put("employees", listModel.getEmployees());
      successObject.put("paid_flag", listModel.isPaid_flag());
      successObject.put("completed_status", false);

      connectionObject.close();
      getSuccessObject(successObject, routingContext.response(), "POST");

    } catch (IllegalArgumentException exception) {
      logger.error(exception.getMessage());
      String errorDescription = exception.getMessage();
      getErrorObject("Invalid Data", errorDescription, routingContext.response(),
          "POST");
    } catch (SQLException exception) {
      logger.error(exception.getMessage());
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (Exception exception){
      logger.error(exception.getMessage());
      ResponseBuilder.writeInternalServerError(routingContext.response());
    }

  }

  public JsonObject getExistingHolidayList(Connection connection, String list_id) throws SQLException {
    JsonObject resultObject = new JsonObject();

    List<String> constraintList = new ArrayList<>();

    constraintList.add("inactive_status = 0");
    constraintList.add("list_id = " + list_id);

    String constraintString = DatabaseUtil.buildConstraintString(constraintList);

    Map<String, String> queryStringParameters = new HashMap<>();
    queryStringParameters.put("fields", "list_id,list_name,year,assign_to,location,department," +
        "division,pay_group,paid_flag,organization_id");
    queryStringParameters.put("table", "HolidayList");
    queryStringParameters.put("constraint", constraintString);

    String holidayListQueryString = buildQuery(queryStringParameters, "SELECT");

    String columnName = "";
    String columnType = "";

    ResultSet resultSetHoliday = getResultSetObject(connection, holidayListQueryString);

    while (resultSetHoliday.next()) {
      ResultSetMetaData resultSetMetaData = resultSetHoliday.getMetaData();
      for (int i = 1; i <= resultSetHoliday.getMetaData().getColumnCount(); i++) {
        columnName = resultSetMetaData.getColumnName(i);
        columnType = resultSetMetaData.getColumnTypeName(i);

        if ("BIT".equalsIgnoreCase(columnType)) {
          resultObject.put(columnName, resultSetHoliday.getBoolean(columnName));
        } else if ("INT".equalsIgnoreCase(columnType)) {
          resultObject.put(columnName, resultSetHoliday.getInt(columnName));
        } else if ("VARCHAR".equalsIgnoreCase(columnType) || "BLOB".equalsIgnoreCase(columnType)) {
          String columnValue = resultSetHoliday.getString(columnName);
          resultObject.put(columnName, columnValue);
        } else if ("Date".equalsIgnoreCase(columnType)) {
          resultObject.put(columnName, resultSetHoliday.getString(columnName));
        }
      }
    }

    return resultObject;
  }

  public String calculateFromArray(String stringToBeSplitted) {
    JsonObject splitObject = new JsonObject(stringToBeSplitted);
    JsonArray tempList = splitObject.getJsonArray("list");
    String returnString = "";

    for (int i = 0; i < tempList.size(); i++) {
      returnString = returnString + tempList.getString(i) + ",";
    }

    StringBuilder returnBuilder = new StringBuilder(returnString);
    returnBuilder.deleteCharAt(returnBuilder.length() - 1);
    returnString = returnBuilder.toString();

    return returnString;
  }
}
