package com.auzmor.impl.handler;

import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.model.onboarding.OnboardingForm;
import com.auzmor.impl.model.onboarding.OnboardingPacket;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.impl.util.ModelUtil;
import com.auzmor.impl.builder.ApiBuilder;

import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;

public class DemoHandler extends ApiBuilder {

    public void getDemoPacket(RoutingContext context) {

        ResultSet resultSetObject = null;

        try {
            Connection connectionObject = new MysqldbConf().getConnection();
            HttpServerRequest request = context.request();

            String id = request.getParam("organization_id");

            String packetIdString = request.getParam("packet_id");

            List<String> constraintList = new ArrayList<>();

            if (id != null) {
                constraintList.add("organization_id = " + id);
            }

            constraintList.add("inactive_status = 0");

            if (packetIdString != null) {
                constraintList.add("packet_id = " + packetIdString);
            }

            String constraintString = DatabaseUtil.buildConstraintString(constraintList);

            Map<String, String> queryStringParameters = new HashMap<>();

            queryStringParameters.put("fields", "packet_id,packet_name");
            queryStringParameters.put("table", "OnboardingPacket");
            queryStringParameters.put("constraint", constraintString);

            String queryString = buildQuery(queryStringParameters,"SELECT");

            resultSetObject = getResultSetObject(connectionObject,queryString);

            JsonObject jsonDataObject = new JsonObject();
            JsonArray packetArray = new JsonArray();


            while (resultSetObject.next()) {

                int packetId = resultSetObject.getInt("packet_id");

                Map<String, String> formQueryParameters = new HashMap<>();
                formQueryParameters.put("fields", "form_id,form_name,form_required");
                formQueryParameters.put("table", "OnboardingForm");

                constraintList = new ArrayList<>();
                constraintList.add("packet_id = " + packetId);
                constraintList.add("inactive_status = 0");

                formQueryParameters.put("constraint", DatabaseUtil.buildConstraintString(constraintList));

                String formQueryString = buildQuery(formQueryParameters,"SELECT");

                ResultSet resultSetForPacketComponents = getResultSetObject(connectionObject,formQueryString);

                JsonArray formArray = new JsonArray();
                while (resultSetForPacketComponents.next()) {
                    JsonObject formObject = new JsonObject();
                    formObject.put("form_id", resultSetForPacketComponents.getInt("form_id"));
                    formObject.put("form_name", resultSetForPacketComponents.getString("form_name"));
                    formObject.put("form_required", resultSetForPacketComponents.getBoolean("form_required"));
                    formArray.add(formObject);
                }

                Map<String, String> taskQueryParameters = new HashMap<>();
                taskQueryParameters.put("fields", "task_id,task_name,task_description,viewing_required,allow_doc_upload,doc_upload_required");
                taskQueryParameters.put("table", "OnboardingTask");
                taskQueryParameters.put("constraint", DatabaseUtil.buildConstraintString(constraintList));

                String taskQueryString = buildQuery(taskQueryParameters,"SELECT");

                resultSetForPacketComponents = getResultSetObject(connectionObject,taskQueryString);

                JsonArray taskArray = new JsonArray();
                while (resultSetForPacketComponents.next()) {
                    JsonObject taskObject = new JsonObject();
                    taskObject.put("task_id", resultSetForPacketComponents.getInt("task_id"));
                    taskObject.put("task_name", resultSetForPacketComponents.getString("task_name"));
                    taskObject.put("task_description", resultSetForPacketComponents.getString("task_description"));

                    taskObject.put("viewing_required", resultSetForPacketComponents.getBoolean("viewing_required"));
                    taskObject.put("allow_doc_upload", resultSetForPacketComponents.getBoolean("allow_doc_upload"));
                    taskObject.put("doc_upload_required", resultSetForPacketComponents.getBoolean("doc_upload_required"));


                    taskArray.add(taskObject);
                }

                JsonArray documentArray = new JsonArray();

                Map<String, String> documentQueryParameters = new HashMap<>();
                documentQueryParameters.put("fields", "document_id,document_name,document_path,viewing_reqd");
                documentQueryParameters.put("table", "OnboardingDocuments");
                documentQueryParameters.put("constraint", DatabaseUtil.buildConstraintString(constraintList));

                String documentQueryString = buildQuery(documentQueryParameters,"SELECT");

                ResultSet documentsResultSet = getResultSetObject(connectionObject,documentQueryString);

                while (documentsResultSet.next()) {
                    JsonObject documentObject = new JsonObject();
                    documentObject.put("document_id", documentsResultSet.getInt("document_id"));
                    documentObject.put("document_name", documentsResultSet.getString("document_name"));
                    documentObject.put("document_path", documentsResultSet.getString("document_path"));
                    documentObject.put("viewing_reqd", documentsResultSet.getBoolean("viewing_reqd"));
                    documentArray.add(documentObject);
                }


                JsonObject packetComponentObject = new JsonObject();

                packetComponentObject.put("id", resultSetObject.getInt("packet_id"));
                packetComponentObject.put("name", resultSetObject.getString("packet_name"));
                packetComponentObject.put("forms", formArray);
                packetComponentObject.put("documents", documentArray);
                packetComponentObject.put("tasks", taskArray);

                packetArray.add(packetComponentObject);

            }


            jsonDataObject.put("packets", packetArray);

            connectionObject.close();

            getSuccessObject(jsonDataObject,context.response(),"GET");


        } catch (Exception onboardingPacketsException) {

            onboardingPacketsException.printStackTrace();

            String description = onboardingPacketsException.getMessage();

            getErrorObject("Bad Request",description,context.response(),"GET");

        }
    }

    public void addDemoPacket(RoutingContext context){
        OnboardingPacket packet = new OnboardingPacket();

        MultiMap params = context.request().params();
        try {
            for (String key : params.names()) {
                ModelUtil.setValues(key, params.get(key), packet);
            }


            Map<String,Boolean> paramMapper = new HashMap<>();
            paramMapper.put("packet_name",true);
            paramMapper.put("organization_id",true);
            paramMapper.put("creator_id",true);

            Map<String, Object> insertionObjects = generateInsertionObject(params,paramMapper,packet);

            List<String> queryStringValueList = DatabaseUtil.prepareQueryStringValues(insertionObjects);
            String columnValueString = queryStringValueList.get(0);
            String entryValueString = queryStringValueList.get(1);

            Map<String,String> insertQueryParameters = new HashMap<>();
            insertQueryParameters.put("table","OnboardingPacket");
            insertQueryParameters.put("column_names",columnValueString);
            insertQueryParameters.put("column_values",entryValueString);

            String sqlQuery = buildQuery(insertQueryParameters,"INSERT");

            List<String> constraintList = new ArrayList<>();
            constraintList.add("packet_name = '" + packet.getPacket_name()  + "'");
            constraintList.add("organization_id = " + params.get("organization_id") );
            constraintList.add("inactive_status = 0");


            Map<String,String> selectQueryParameters = new HashMap<>();
            selectQueryParameters.put("fields", "count(packet_name) as count");
            selectQueryParameters.put("table", "OnboardingPacket");
            selectQueryParameters.put("constraint", DatabaseUtil.buildConstraintString(constraintList));

            String query = buildQuery(selectQueryParameters,"SELECT");

            /*Settings settings = Settings.builder().put("cluster.name", "elasticsearch").build();
            TransportClient transportClient = new PreBuiltTransportClient(settings);
            TransportAddress address = new TransportAddress(InetAddress.getByName("localhost"),9300);
//            transportClient = transportClient.addTransportAddress(new InetSocketTransportAddress("localhost", 9300));
            transportClient = transportClient.addTransportAddress(address);s
            Client client = (Client) transportClient;
            CreateIndexRequestBuilder createIndex = client.admin().indices().prepareCreate("simplyjava");
            CreateIndexResponse response = createIndex.execute().actionGet();*/

          /*  RestClient restClient = RestClient.builder(
                new HttpHost("localhost",9200,"http"),
                new HttpHost("localhost",9201,"http")).build();*/
            Map<String, String> parameters = Collections.emptyMap();
            JsonObject object = new JsonObject();
            object.put("user","Sadhana");
            object.put("postDate","2017-01-30");
            object.put("message","Elastic Search Trial");
            object.put("timestamp",new Date().toString());

            String jsonString = object.toString();

            /*String jsonString = "{" +
                "\"user\":\"Pradeep Sudhakaran\"," +
                "\"postDate\":\"2017-01-30\"," +
                "\"message\":\"trying out Elasticsearch\"," +
                "\"timestamp\":\""+new Date()+"\""+
                "}"; */
         /*   HttpEntity entity = new NStringEntity(jsonString, ContentType.APPLICATION_JSON);

            Response response = restClient.performRequest("PUT", "/posts/doc/5", parameters, entity);

            System.out.println("Response : " + response.toString());
            response = restClient.performRequest("GET","/posts/_search");
            System.out.println("REsponse GET : " + EntityUtils.toString(response.getEntity()));
            System.out.println(response.getEntity());
            String testString = convertStreamToString(response.getEntity().getContent());
            System.out.println("Json String : " + testString);
//            JsonObject jsonObject = new JsonObject(jsonStringq);
//            JsonArray array = jsonObject.getJsonArray("hits");
//            System.out.println(jsonObject);
            restClient.close();*/
//            System.out.println("Response Index : " + response.index());

            Connection connection = new MysqldbConf().getConnection();

            ResultSet resultSet = getResultSetObject(connection,query);

            int count = -1;
            while(resultSet.next()){
                count = resultSet.getInt("count");
            }

            connection.close();

            if(count>0){
                String deleteDuplicate = "delete from OnboardingPacket " +
                    "where organization_id = " + params.get("organization_id") + " "
                    + " and packet_name = '"+packet.getPacket_name()+"'" ;
                int deletedDuplicateRows = getUpdatedRows(deleteDuplicate);
            }

            int generatedPacketId = insertRecord(sqlQuery);
            packet.setPacket_id(generatedPacketId);

            JsonObject successObject = new JsonObject();
            successObject.put("id", packet.getPacket_id());

            getSuccessObject(successObject,context.response(),"POST");


        } catch (Exception addPacketException) {
            addPacketException.printStackTrace();
            String errorDescription = addPacketException.getMessage();

            if(errorDescription!=null && errorDescription.contains("Duplicate entry")){
                errorDescription = "Packet '" + packet.getPacket_name() + "' already exists ";
            }

            getErrorObject("Invalid Data",errorDescription,context.response(),"POST");
        }
    }


    public void updateDemoForm(RoutingContext routingContext) {

        String formId = routingContext.request().getParam("form_id");
        MultiMap params = routingContext.request().params();
        JsonObject jsonResponseObject = new JsonObject();

        String updateString = "";

        try {

            Connection connection = new MysqldbConf().getConnection();

            String updateStatement = generateUpdateStatement(params,new OnboardingForm());

            String constraintString = " where form_id = " + formId;

            Map<String,String> updateQueryParameters = new HashMap<>();
            updateQueryParameters.put("table", "OnboardingForm");
            updateQueryParameters.put("key_value_pair",updateStatement);
            updateQueryParameters.put("constraint", constraintString);

            updateString = buildQuery(updateQueryParameters,"UPDATE");

            int updatedRows = getUpdatedRows(updateString);
            if (updatedRows == 0) {
                throw new Exception("Invalid request to update form");
            }

            String queryString = "select form_name from OnboardingForm " + constraintString;
            ResultSet resultSet = getResultSetObject(connection,queryString);
            String formName = "";
            while (resultSet.next()) {
                formName = resultSet.getString("form_name");
            }


            connection.close();

            jsonResponseObject.put("message","Form '"+formName+"' updated ");
            getSuccessObject(jsonResponseObject,routingContext.response(),"PUT");

        } catch (Exception exception) {
            exception.printStackTrace();
            getErrorObject("Invalid Data",exception.getMessage(),routingContext.response(),"PUT");
        }
    }

    public void deleteDemoPacket(RoutingContext context) {

        MultiMap params = context.request().params();
        try {
            int packetID = Integer.valueOf(params.get("packet_id"));

            String constraintString = "where packet_id = " + packetID;

            Map<String,String> updateQueryParameters = new HashMap<>();
            updateQueryParameters.put("table","OnboardingPacket");
            updateQueryParameters.put("key_value_pair","inactive_status = 1");
            updateQueryParameters.put("constraint",constraintString);

            String deletePacketQuery = buildQuery(updateQueryParameters,"UPDATE");

            int deletedRecords = getUpdatedRows(deletePacketQuery);

            if (deletedRecords == 0) {
                throw new Exception("Invalid Request to delete Packet");
            }

            String queryString = "select packet_name from OnboardingPacket " + constraintString;
            ResultSet resultSet = getResultSetObject(new MysqldbConf().getConnection(), queryString);
            String packetName = "";
            while (resultSet.next()) {
                packetName = resultSet.getString("packet_name");
            }

            JsonObject dataObject = new JsonObject();
            dataObject.put("message","Packet '" + packetName + "' Deleted");

            getSuccessObject(dataObject,context.response(),"DELETE");

        } catch (Exception deletePacketException) {
            deletePacketException.printStackTrace();
            getErrorObject("Deletion Error",deletePacketException.getMessage(),
                context.response(),"DELETE");

        }

    }

    private static String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

}
