package com.auzmor.impl.handler;

import com.auzmor.bo.OnboardingFormBO;
import com.auzmor.impl.bo.OnboardingFormBOImpl;
import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.enumarator.bo.OnboardingFormEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.onboarding.OnboardingForm;
import com.auzmor.impl.util.ModelUtil;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.FieldOptions;
import com.auzmor.util.validator.Validator;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OnboardingFormHandler {
  public static void addForm(RoutingContext context) {
    HttpServerRequest request = context.request();
    HttpServerResponse response = context.response();
    MultiMap params = request.params();
    OnboardingForm onboardingForm = new OnboardingForm();

    String[] allowedParams = {"packet_id","form_name","form_required"};
    String[] requiredParams = {"packet_id","form_name"};
    String[] formParams = {"packet_id","form_name","form_required"};

    List<Validator> validators = new ArrayList<>();
    Validator formValidator = new ValidatorImpl(formParams, onboardingForm.getValidatorMap());
    validators.add(formValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(context, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          OnboardingFormBO onboardingFormBO = new OnboardingFormBOImpl(dbHandle);
          ReturnObject<OnboardingFormEnum, JsonObject> boResponse =
              onboardingFormBO.addForm(dbHandle, requestParams);
          OnboardingFormHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  /**
   * deleteOnboardingForm Delete Onboarding Form
   *
   * @param context
   * @author Pradeep Balasubramanian
   */
  public static void deleteOnboardingForm(RoutingContext context) {
    HttpServerRequest request = context.request();
    HttpServerResponse response = context.response();
    MultiMap params = context.request().params();

    String[] allowedParams = {"formId"};
    String[] requiredParams = {"formId"};
    String[] taskParams = {"formId"};

    List<Validator> validators = new ArrayList<>();
    Validator fieldValidator = new ValidatorImpl(taskParams, new HashMap<String, FieldOptions>() {{
      put("formId", FieldOptionsCommon.NUMBER);
    }});
    validators.add(fieldValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(context, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          OnboardingFormBO onboardingFormBO = new OnboardingFormBOImpl(dbHandle);
          ReturnObject<OnboardingFormEnum, JsonObject> boResponse
              = onboardingFormBO.deleteForm(dbHandle, requestParams);
          OnboardingFormHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  /**
   * updateOnboardingForm - update onboarding form
   *
   * @param routingContext
   * @author Pradeep Balasubramanian
   */
  public static void updateOnboardingForm(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    OnboardingForm onboardingForm = new OnboardingForm();

    String[] allowedParams = {"formId","packet_id","form_name","form_required"};
    String[] requiredParams = {"formId","packet_id","form_name"};
    String[] formParams = {"formId","packet_id","form_name","form_required"};

    List<Validator> validators = new ArrayList<>();
    Validator formValidator = new ValidatorImpl(formParams, onboardingForm.getValidatorMap());
    validators.add(formValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
        OnboardingFormBO onboardingFormBO = new OnboardingFormBOImpl(dbHandle);
        ReturnObject<OnboardingFormEnum, JsonObject> boResponse
              = onboardingFormBO.updateForm(dbHandle, requestParams);
        OnboardingFormHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  /**
   * getAuzmorForms - Get the List of Auzmor Forms for an onboarding Packet
   *
   * @param context
   * @author Pradeep Balasubramanian
   */
  public static void getAuzmorForms(RoutingContext context) {
    HttpServerRequest request = context.request();
    HttpServerResponse response = context.response();
    MultiMap params = request.params();
    OnboardingForm onboardingForm = new OnboardingForm();

    String[] allowedParams = {"packet_id"};
    String[] requiredParams = {"packet_id"};
    String[] formParams = {"packet_id"};

    List<Validator> validators = new ArrayList<>();
    Validator formValidator = new ValidatorImpl(formParams, onboardingForm.getValidatorMap());
    validators.add(formValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(context, false,
        (httpServerResponse, requestParams, dbHandle) -> {
      OnboardingFormBO onboardingFormBO = new OnboardingFormBOImpl(dbHandle);
      ReturnObject<OnboardingFormEnum, JsonObject> boResponse
              = onboardingFormBO.getAuzmorForm(dbHandle, requestParams);
      OnboardingFormHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  public static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   ReturnObject<OnboardingFormEnum, JsonObject> boResponse)
      throws SQLException, DbHandleException {
    switch (boResponse.getStatusEnum()) {
      case ADD_FAILURE:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
      case UPDATE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_ACCEPTED,
            "Form details updated successfully", boResponse.getData(), httpServerResponse);
        break;
      case ADD_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Form details added successfully", boResponse.getData(), httpServerResponse);
        break;
      case VALUE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "Details retrived", boResponse.getData(), httpServerResponse);
        break;
      case NO_SUCH_VALUE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "No Form details present", boResponse.getData(), httpServerResponse);
        break;
      case UPDATE_FAILURE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "No values to be updated", new JsonObject(), httpServerResponse);
        break;
      case ALREADY_EXISTS:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_BAD_REQUEST,
            "Form Already exists",
            "Form Already exists",
            httpServerResponse);
        break;
      case DELETE_FAILURE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "No values to be deleted", boResponse.getData(), httpServerResponse);
        break;
      case DELETE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "Form deleted successfully", boResponse.getData(), httpServerResponse);
        break;
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }
}
