package com.auzmor.impl.handler;

import com.auzmor.bo.TitleBO;
import com.auzmor.impl.bo.TitleBOImpl;
import com.auzmor.impl.enumarator.TitleEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.TitleModel;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.Validator;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TitleHandler {
  private static Logger logger = LoggerFactory.getLogger(TitleHandler.class);

  /**
   * Add a job title for particular organization
   * @param routingContext
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 12/FEB/2018
   *
   */
  public static void addTitle(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    TitleModel titleModel = new TitleModel();

    String[] allowedParams = {"title", "organization_id", "active_status"};
    String[] requiredParams = {"title", "organization_id"};
    String[] titleParams = {"title", "organization_id", "active_status"};

    List<Validator> validators = new ArrayList<>();
    Validator titleValidator = new ValidatorImpl(titleParams, TitleModel.getValidatorMap());
    validators.add(titleValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          TitleBO titleBO = new TitleBOImpl();
          titleModel.setTitle(requestParams.get("title"));
          titleModel.setOrganizationId(Integer.valueOf(requestParams.get("organization_id")));
          ReturnObject<TitleEnum, JsonObject> boResponse = titleBO.addTitle(dbHandle,titleModel);
          TitleHandler.writeResponse(requestParams,httpServerResponse,dbHandle,boResponse);
        });
  }
  /**
   * Get job title details for particular id
   * @param routingContext
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 12/FEB/2018
   *
   */
  public static void getTitle(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    TitleModel titleModel = new TitleModel();

    String[] allowedParams = {"id"};
    String[] requiredParams = {"id"};
    String[] titleParams = {"id"};

    List<Validator> validators = new ArrayList<>();
    Validator titleValidator = new ValidatorImpl(titleParams, TitleModel.getValidatorMap());
    validators.add(titleValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          titleModel.setId(Integer.valueOf(requestParams.get("id")));
          TitleBO titleBO = new TitleBOImpl();
          ReturnObject<TitleEnum, JsonObject> boResponse = titleBO.getTitle(dbHandle,titleModel);
          TitleHandler.writeResponse(requestParams,httpServerResponse,dbHandle,boResponse);
        });

  }
  /**
   * Update job title details
   * @param routingContext
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 12/FEB/2018
   *
   */
  public static void updateTitle(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    TitleModel titleModel = new TitleModel();

    String[] allowedParams = {"id", "title", "organization_id", "active_status"};
    String[] requiredParams = {"id", "title"};
    String[] titleParams = {"id", "title", "organization_id", "active_status"};

    List<Validator> validators = new ArrayList<>();
    Validator titleValidator = new ValidatorImpl(titleParams, TitleModel.getValidatorMap());
    validators.add(titleValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          TitleBO titleBO = new TitleBOImpl();
          ReturnObject<TitleEnum, JsonObject> boResponse = titleBO.updateTitle(dbHandle,requestParams);
          TitleHandler.writeResponse(requestParams,httpServerResponse,dbHandle,boResponse);
        });

  }
  /**
   * Get all job titles in a particular organization
   * @param routingContext
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 12/FEB/2018
   *
   */
  public static void getAllTitles(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    TitleModel titleModel = new TitleModel();

    String[] allowedParams = {"organization_id"};
    String[] requiredParams = {"organization_id"};
    String[] titleParams = {"organization_id"};

    List<Validator> validators = new ArrayList<>();
    Validator titleValidator = new ValidatorImpl(titleParams, TitleModel.getValidatorMap());
    validators.add(titleValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          TitleBO titleBO = new TitleBOImpl();
          titleModel.setOrganizationId(Integer.valueOf(requestParams.get("organization_id")));
          ReturnObject<TitleEnum, JsonArray> boResponse = titleBO.getAllTitles(dbHandle,titleModel);
          switch (boResponse.getStatusEnum()) {
            case VALUE_SUCCESS:
              logger.info("Available Titles :"+boResponse);
              dbHandle.commit();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  "", boResponse.getData(), httpServerResponse);
              break;
            case NO_SUCH_VALUE:
              logger.info("unavailable Titles :"+boResponse);
              dbHandle.rollback();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  "no_data is true", boResponse.getData(), httpServerResponse);
              break;
            default:
              dbHandle.rollback();
              logger.error(""+boResponse);
              ResponseHelper.writeInternalServerError(httpServerResponse);
              break;
          }
        });
  }
  /**
   * Delete job title details
   * @param routingContext
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 12/FEB/2018
   *
   */
  public static void deleteTitle(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    TitleModel titleModel = new TitleModel();

    String[] allowedParams = {"id"};
    String[] requiredParams = {"id"};
    String[] titleParams = {"id"};

    List<Validator> validators = new ArrayList<>();
    Validator titleValidator = new ValidatorImpl(titleParams, TitleModel.getValidatorMap());
    validators.add(titleValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          TitleBO titleBO = new TitleBOImpl();
          titleModel.setId(Integer.valueOf(requestParams.get("id")));
          ReturnObject<TitleEnum, JsonObject> boResponse = titleBO.deleteTitle(dbHandle,titleModel);
          TitleHandler.writeResponse(requestParams,httpServerResponse,dbHandle,boResponse);
        });

  }

  /**
   * Write response according to the enum values
   * @param params
   * @param httpServerResponse
   * @param dbHandle
   * @param boResponse
   * @throws SQLException
   * @throws DbHandleException
   */
  public static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   ReturnObject<TitleEnum, JsonObject> boResponse)
      throws SQLException, DbHandleException {

    JsonObject dataObject = new JsonObject();
    dataObject.put("message", boResponse.getStatusEnum().getDescription());
    switch (boResponse.getStatusEnum()) {
      case EXISTS_ALREADY:
        dbHandle.rollback();
        logger.error("Bad Request :"+boResponse);
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            params.get("title") + " already exists",
            params.get("title") + " already exists",
            httpServerResponse);
        break;
      case ADDITION_SUCCESS:
        dbHandle.commit();
        logger.info("Title added successfully :"+boResponse);
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Title added successfully", dataObject, httpServerResponse);
        break;
      case VALUE_SUCCESS:
        dbHandle.commit();
        logger.info("Title retrieved :"+boResponse);
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Title retrieved successfully", boResponse.getData(), httpServerResponse);
        break;
      case ADDITION_FAILED:
        dbHandle.rollback();
        logger.error("could not able to add Title :"+boResponse);
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
      case UPDATE_SUCCESS:
        dbHandle.commit();
        logger.info("TItle updated Successfully :"+boResponse);
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Title updated successfully", dataObject, httpServerResponse);
        break;
      case NO_CHANGES_MADE:
        dbHandle.commit();
        logger.info("No Changes made :"+boResponse);
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "No Changes made", dataObject, httpServerResponse);
        break;
      case NO_SUCH_VALUE:
        dbHandle.rollback();
        logger.error("Could not able to find Title"+boResponse);
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_BAD_REQUEST,
            "Unable to find Title", dataObject,
            httpServerResponse);
        break;
      case CANNOT_DELETE:
        dbHandle.rollback();
        logger.error("Could not able to Delete Title :"+boResponse);
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Title cannot be deleted, Please reassign Employees",
            "Title cannot be deleted, Please reassign Employees",
            httpServerResponse);
        break;
      case DELETE_SUCCESS:
        dbHandle.commit();
        logger.info("Title Deleted Successfully :"+boResponse);
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Title deleted successfully", dataObject, httpServerResponse);
        break;
      case RESTRICTED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Published Job Title Cannot be Changed",
            "Published Job Title Cannot be Changed",
            httpServerResponse);
      default:
        dbHandle.rollback();
        logger.error(""+boResponse);
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }
}
