package com.auzmor.impl.handler;

import com.auzmor.bo.ReportBO;
import com.auzmor.impl.bo.ReportBOImpl;
import com.auzmor.impl.builder.ResponseBuilder;
import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.enumarator.ReportEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.model.SourceReport;

import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.FieldOptions;
import com.auzmor.util.validator.Validator;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ReportHandler {

  private static final Logger logger = LoggerFactory.getLogger(ReportHandler.class);

  public void getJobReportDetails(String limit, String offset, RoutingContext routingContext) {

    try {
      Connection connectionObject = new MysqldbConf().getConnection();
      PreparedStatement preparedStatement = null;
      JsonObject jsonSuccessObject = new JsonObject();
      jsonSuccessObject.put("code", 200);
      jsonSuccessObject.put("success", true);

      String constraintString = "";

      constraintString += "order by id desc";
      if ("".equals(limit) || "".equals(offset)) {
        if (limit.length() > 0) {
          constraintString = " LIMIT " + limit;
        }
        if (offset.length() > 0) {
          constraintString = " OFFSET " + offset;
        }
      } else {
        constraintString = " LIMIT " + limit + " OFFSET " + offset;
      }

      String organizationId = routingContext.request().params().get("organization_id");

      String organizationFilter = "";

      if (organizationId != null) {
        organizationFilter = " and organization_id = " + organizationId + " ";
      }

      String queryString = "";
      queryString = "Select title,job_status,employment_type," +
          "location,hiring_lead,date_posted,modified_date " +
          "  from Job where job_active_status=1  " + organizationFilter + constraintString;

      preparedStatement = connectionObject.prepareStatement(queryString);
      ResultSet resultSetObject = preparedStatement.executeQuery();
      JsonArray resultSetArray = new JsonArray();
      while (resultSetObject.next()) {
        JsonObject dataObject = new JsonObject();
        dataObject.put("title", resultSetObject.getString("title"));
        dataObject.put("job_status", resultSetObject.getString("job_status"));
        dataObject.put("employment_type", resultSetObject.getString("employment_type"));
        dataObject.put("location", resultSetObject.getString("location"));
        dataObject.put("hiring_lead", resultSetObject.getString("hiring_lead"));
        dataObject.put("date_posted", resultSetObject.getString("date_posted"));
        dataObject.put("modified_date", resultSetObject.getString("modified_date"));

        resultSetArray.add(dataObject);
      }


      jsonSuccessObject.put("data", resultSetArray);

      if (resultSetArray.size() == 0) {
        jsonSuccessObject.put("no_data", true);
      } else {
        jsonSuccessObject.put("no_data", false);
      }

      connectionObject.close();

      routingContext.response()
          .setStatusCode(200)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonSuccessObject.toString());

    } catch (Exception e) {


      JsonObject jsonResponseObject = new JsonObject();

      JsonObject jsonFailureObject = new JsonObject();
      jsonFailureObject.put("code", 400);
      jsonFailureObject.put("message", "Wrong Field Name");
      jsonFailureObject.put("description", e.getMessage());

      jsonResponseObject.put("error", jsonFailureObject);

      routingContext.response()
          .setStatusCode(jsonFailureObject.getInteger("code"))
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
      e.printStackTrace();

    }
  }


  public void getCandidateCurrentStatusReport(RoutingContext routingContext) {
    try {
      Connection connectionObject = new MysqldbConf().getConnection();
      PreparedStatement preparedStatement = null;

      JsonObject jsonSuccessObject = new JsonObject();
      jsonSuccessObject.put("code", 200);
      jsonSuccessObject.put("success", true);

      String queryString = "";
      String organizationId = (routingContext.request()).params().get("organization_id");
      String organizationFilter = "";
      if (organizationId != null) {
        organizationFilter = " and organization_id = " + organizationId + " ";
      }


      queryString = " SELECT title FROM Job " +
          "where job_status != 'filled'  and job_active_status ='1'" +
          organizationFilter + " group by title";
      preparedStatement = connectionObject.prepareStatement(queryString);
      ResultSet resultSetObject = preparedStatement.executeQuery();
      JsonArray resultSetArray = new JsonArray();
      while (resultSetObject.next()) {
        JsonObject dataObject = new JsonObject();
        dataObject.put("title", resultSetObject.getString("title"));
        dataObject.put("data", getApplicantStatus(connectionObject,
            resultSetObject.getString("title")));
        resultSetArray.add(dataObject);
      }
      jsonSuccessObject.put("data", resultSetArray);
      if (resultSetArray.size() == 0) {
        jsonSuccessObject.put("no_data", true);
      } else {
        jsonSuccessObject.put("no_data", false);
      }
      routingContext.response()
          .setStatusCode(200)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonSuccessObject.toString());
      connectionObject.close();

    } catch (Exception e) {

      JsonObject jsonResponseObject = new JsonObject();

      JsonObject jsonFailureObject = new JsonObject();
      jsonFailureObject.put("code", 400);
      jsonFailureObject.put("message", "Wrong Field Name");
      jsonFailureObject.put("description", e.getMessage());
      jsonResponseObject.put("error", jsonFailureObject);

      routingContext.response()
          .setStatusCode(200)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
      e.printStackTrace();

    }
  }

  public JsonArray getApplicantStatus(Connection connectionObject, String title) {
    try {
      PreparedStatement preparedStatement = null;
      JsonArray CandidateArray = new JsonArray();
      String queryString = "";
      queryString = " SELECT count(status),status FROM Candidate where status !='' and " +
          " job_title='" + title + "' and  active_status ='0' group by status";
      preparedStatement = connectionObject.prepareStatement(queryString);
      ResultSet resultSetObject1 = preparedStatement.executeQuery();
      while (resultSetObject1.next()) {
        JsonObject dataObject = new JsonObject();

        dataObject.put("key", resultSetObject1.getString("status"));
        dataObject.put("count", resultSetObject1.getInt("count(status)"));
        CandidateArray.add(dataObject);
      }
      return CandidateArray;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return new JsonArray();
  }

  public void getCandidateAverageStatusReport(RoutingContext routingContext) {
    try {
      Connection connectionObject = new MysqldbConf().getConnection();
      PreparedStatement preparedStatement = null;
      JsonObject jsonSuccessObject = new JsonObject();
      jsonSuccessObject.put("code", 200);
      jsonSuccessObject.put("success", true);
      String queryString = "";
      String organizationId = routingContext.request().params().get("organization_id");
      String organizationFilter = "";
      if (organizationId != null) {
        organizationFilter = " and organization_id = " + organizationId + " ";
      }
      queryString = " SELECT title FROM Job where job_status != 'filled'  " + organizationFilter
          + "and job_active_status ='1' group by title";
      preparedStatement = connectionObject.prepareStatement(queryString);
      ResultSet resultSetObject = preparedStatement.executeQuery();
      JsonArray resultSetArray = new JsonArray();
      JsonObject dataObject = new JsonObject();
      int count = 0;
      int interviewcount = 0;
      int screeningcount = 0;
      int hirecount = 0;
      int offercount = 0;
      JsonArray titleArray = new JsonArray();
      JsonArray interviewArray = new JsonArray();
      JsonArray applicantArray = new JsonArray();
      JsonArray screeningArray = new JsonArray();
      JsonArray hireArray = new JsonArray();
      JsonArray offerArray = new JsonArray();
      List<String> hiringprocess = new ArrayList<String>();
      hiringprocess = this.getCandidateStatusList(connectionObject);
      while (resultSetObject.next()) {
        titleArray.add(resultSetObject.getString("title"));
        for (int i = 0; i < hiringprocess.size(); i++) {
          queryString = "select a.status,a.created_date as date,b.created_date, b.job_title " +
              "from HiringprocessCandidate a,Candidate b" +
              " where a.candidate_id = b.candidate_id and b.status = '" + hiringprocess.get(i) +
              "' and   " +
              " b.job_title='" + resultSetObject.getString("title") + "'  and " +
              " b.active_status ='0' ";
          preparedStatement = connectionObject.prepareStatement(queryString);
          ResultSet resultSet = preparedStatement.executeQuery();
          while (resultSet.next()) {
            String status = resultSet.getString("status");
            String dayscount = "SELECT DATEDIFF('" + resultSet.getString("date") +
                "','" + resultSet.getString("created_date") + "') as count ";
            preparedStatement = connectionObject.prepareStatement(dayscount);
            ResultSet resultSetObject2 = preparedStatement.executeQuery();
            while (resultSetObject2.next()) {

              count = 0;
              interviewcount = 0;
              screeningcount = 0;
              hirecount = 0;
              offercount = 0;

              if (status.equalsIgnoreCase("Interview")) {
                interviewcount = resultSetObject2.getInt("count");

              } else if (status.equalsIgnoreCase("applicant")) {
                count = resultSetObject2.getInt("count");

              } else if (status.equalsIgnoreCase("screening")) {
                screeningcount = resultSetObject2.getInt("count");

              } else if (status.equalsIgnoreCase("offer")) {
                offercount = resultSetObject2.getInt("count");

              } else if (status.equalsIgnoreCase("hire")) {
                hirecount = resultSetObject2.getInt("count");

              }
            }
          }


        }


        interviewArray.add(interviewcount);
        hireArray.add(hirecount);
        offerArray.add(offercount);
        screeningArray.add(screeningcount);
        applicantArray.add(count);


        dataObject.put("title", titleArray);
        dataObject.put("applicant", applicantArray);
        dataObject.put("screening", screeningArray);
        dataObject.put("interview", interviewArray);
        dataObject.put("hire", hireArray);
        dataObject.put("offer", offerArray);

      }


      jsonSuccessObject.put("data", dataObject);
      routingContext.response()
          .setStatusCode(200)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonSuccessObject.toString());
      connectionObject.close();

    } catch (Exception e) {
      JsonObject jsonResponseObject = new JsonObject();
      JsonObject jsonFailureObject = new JsonObject();
      jsonFailureObject.put("code", 400);
      jsonFailureObject.put("message", "Wrong Field Name");
      jsonFailureObject.put("description", e.getMessage());
      jsonResponseObject.put("error", jsonFailureObject);

      routingContext.response()
          .setStatusCode(400)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
      e.printStackTrace();

    }
  }

  public List<String> getCandidateStatusList(Connection connectionObject) {
    List<String> hiringprocess = new ArrayList<String>();
    try {
      PreparedStatement preparedStatement = null;
      JsonArray CandidateArray = new JsonArray();
      String queryString = "";
      queryString = " select status from Candidate where (status is not null or status != '') " +
          "group by status";
      preparedStatement = connectionObject.prepareStatement(queryString);
      ResultSet resultSetObject1 = preparedStatement.executeQuery();
      while (resultSetObject1.next()) {
        hiringprocess.add(resultSetObject1.getString("status"));
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return hiringprocess;
  }


  public JsonArray getDateDifference(Connection connectionObject, String title, String status) {
    try {
      PreparedStatement preparedStatement = null;
      JsonArray CandidateArray = new JsonArray();
      String queryString = "";
      queryString = " select a.status,max(a.created_date) as date,b.created_date,b.job_title " +
          "from HiringprocessCandidate a,Candidate b" +
          " where a.candidate_id = b.candidate_id  and b.status = '" + status + "' and   " +
          " job_title='" + title + "'  and b.active_status ='0' group by b.status";

      preparedStatement = connectionObject.prepareStatement(queryString);
      ResultSet resultSetObject1 = preparedStatement.executeQuery();
      while (resultSetObject1.next()) {
        JsonObject dataObject = new JsonObject();
        String dayscount = "SELECT DATEDIFF('" + resultSetObject1.getString("date") +
            "','" + resultSetObject1.getString("created_date") + "') as count ";
        preparedStatement = connectionObject.prepareStatement(dayscount);
        ResultSet resultSetObject2 = preparedStatement.executeQuery();
        while (resultSetObject2.next()) {
          String count = resultSetObject2.getString("count");
          dataObject.put("key", status);
          dataObject.put("count", count);
        }
        CandidateArray.add(dataObject);
      }
      return CandidateArray;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return new JsonArray();
  }


  /**
   * updateJobSources - update sources whenever a career page is loaded from the source
   *
   * @param routingContext
   */
  public void updateJobSources(RoutingContext routingContext) {
    Connection connectionObject = null;
    Statement preparedStatement = null;

    HttpServerRequest request = routingContext.request();

    JsonObject jsonResponseObject = new JsonObject();

    SourceReport reportObject = new SourceReport();

    try {
      connectionObject = new MysqldbConf().getConnection();
      preparedStatement = connectionObject.createStatement();
      int jobId = Integer.valueOf(request.getParam("jobId") != null ?
          request.getParam("jobId") : "-1");

      if (jobId <= 0) {
        throw new IllegalArgumentException("Enter Missing Required Fields");
      }
      String source = request.getParam("source");
      source = (source != null) ? source : "direct";

      reportObject.setSourceDate(new java.util.Date());
      String sourceDate = reportObject.getSourceDate().toString();
      String constraintString = " where source_date = '" + sourceDate
          + "' and job_id = " + jobId;
      String checkQuery = "select count(*) as count from Sources " + constraintString;
      ResultSet resultSet = preparedStatement.executeQuery(checkQuery);

      int sourceRecords = -1;

      while (resultSet.next()) {
        sourceRecords = resultSet.getInt("count");
      }

      if (sourceRecords == 0) {
        String newDateEntryQuery = "insert into Sources(source_date,job_id," + source + ") " +
            " values('" + sourceDate + "'," + jobId + ",1)";

        preparedStatement.executeUpdate(newDateEntryQuery);
      } else if (sourceRecords > 0) {
        String updateSourceQuery = "update Sources set " + source + "=" + source + " + 1" +
            constraintString;
        preparedStatement.executeUpdate(updateSourceQuery);
      }
      connectionObject.close();

      JsonObject jsonSuccessObject = new JsonObject();
      jsonSuccessObject.put("code", HttpStatus.SC_ACCEPTED);
      jsonSuccessObject.put("success", true);
      jsonSuccessObject.put("message", "Sources Updated");

      routingContext.response()
          .setStatusCode(jsonSuccessObject.getInteger("code"))
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonSuccessObject.toString());


    } catch (SQLException updateSourceSQLException) {

      logger.error("SQL Exception in Update Report Sources");
      logger.error("Wrong Field Name");
      logger.error(updateSourceSQLException.getMessage());

      ResponseBuilder.writeInternalServerError(routingContext.response());

    } catch (ClassNotFoundException updateSourceException) {

      logger.error("Class Not Found Exception in Update Report Sources");
      logger.error(updateSourceException.getMessage());

      ResponseBuilder.writeInternalServerError(routingContext.response());

    } catch (IllegalArgumentException illegalParametersException){

      logger.error("Illegal Argument Exception in Update Report Sources");
      logger.error(illegalParametersException.getMessage());

      JsonObject jsonFailureObject = new JsonObject();
      jsonFailureObject.put("code", HttpStatus.SC_NOT_ACCEPTABLE);
      jsonFailureObject.put("message", "Wrong Field Name");
      jsonFailureObject.put("description", illegalParametersException.getMessage());

      jsonResponseObject.put("error", jsonFailureObject);

      routingContext.response()
          .setStatusCode(jsonFailureObject.getInteger("code"))
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
    }
  }

  /**
   * getSourceReport - get the source reports based on organization
   *
   * @param context
   */
  public void getSourceReport(RoutingContext context) {
    HttpServerRequest request = context.request();

    Connection connectionObject = null;
    Statement preparedStatement = null;

    JsonObject jsonResponseObject = new JsonObject();
    try {
      connectionObject = new MysqldbConf().getConnection();
      preparedStatement = connectionObject.createStatement();

      String startDateString = request.getParam("start_date");
      String endDateString = request.getParam("end_date");

      String dateConstraintString = "";

      String organizationId = request.getParam("organization_id");

      if (startDateString != null && endDateString != null) {
        String dateFormatString = "dd/MM/yyyy";
        java.util.Date startDate = new SimpleDateFormat(dateFormatString).parse(startDateString);
        java.util.Date endDate = new SimpleDateFormat(dateFormatString).parse(endDateString);

        java.sql.Date sqlStartDate = new java.sql.Date(startDate.getTime());
        java.sql.Date sqlEndDate = new java.sql.Date(endDate.getTime());

        dateConstraintString = " source_date between '" +
            sqlStartDate + "' and '" + sqlEndDate + "'";
      }

      int jobId = Integer.valueOf(request.getParam("id") != null ?
          request.getParam("id") : "-1");

      String constraintString = "";
      if (dateConstraintString.length() > 0 && jobId > 0) {
        constraintString = " where job_id = " + jobId + " and " + dateConstraintString;
      } else if (dateConstraintString.length() > 0) {
        constraintString = " where " + dateConstraintString;
      } else {
        if (jobId > 0) {
          constraintString = " where job_id = " + jobId;
        }
      }

      if (organizationId != null && organizationId.length() > 0) {
        constraintString = "where job_id in (select id from Job " +
            "where organization_id = " + organizationId + ") ";
        if(dateConstraintString.length()>0){
          constraintString += " and " + dateConstraintString;
        }
      }

      String sourceReportQuery = "select sum(facebook) as facebook_count," +
          "sum(linkedin) as linkedin_count, " +
          "sum(twitter) as twitter_count," +
          "sum(monster) as monster_count," +
          "sum(glassdoor) as glassdoor_count," +
          "sum(direct) as direct_count," +
          "sum(careerbuilder) as careerbuilder_count," +
          "sum(usjobs) as usjobs_count," +
          "sum(indeed) as indeed_count," +
          "sum(googleplus) as googleplus_count," +
          "sum(linkedin+googleplus+facebook+direct) as total"+
          " from Sources " + constraintString;

      ResultSet resultSet = preparedStatement.executeQuery(sourceReportQuery);

      int facebookCount = 0;
      int linkedinCount = 0;
      int directCount = 0, indeedCount = 0, googleplusCount = 0, usjobsCount = 0, twitterCount = 0;
      int monsterCount = 0, glassdoorCount = 0, careerbuilderCount = 0,total=0;
      JsonArray jsonSourceArray = new JsonArray();
      while (resultSet.next()) {
        total = resultSet.getInt("total");
        facebookCount = resultSet.getInt("facebook_count");
        jsonSourceArray.add(new JsonObject().put("source", "Facebook")
            .put("count", facebookCount)
            .put("share",getSourceShare(facebookCount,total)));

        linkedinCount = resultSet.getInt("linkedin_count");
        jsonSourceArray.add(new JsonObject().put("source", "Linkedin")
            .put("count", linkedinCount)
            .put("share",getSourceShare(linkedinCount,total)));

        twitterCount = resultSet.getInt("twitter_count");
        jsonSourceArray.add(new JsonObject().put("source", "Twitter").
            put("count", twitterCount));

        monsterCount = resultSet.getInt("monster_count");
        jsonSourceArray.add(new JsonObject().put("source", "Monster")
            .put("count", monsterCount));

        glassdoorCount = resultSet.getInt("glassdoor_count");
        jsonSourceArray.add(new JsonObject().put("source", "Glassdoor")
            .put("count", glassdoorCount));

        directCount = resultSet.getInt("direct_count");
        jsonSourceArray.add(new JsonObject().put("source", "Direct")
            .put("count", directCount)
            .put("share",getSourceShare(directCount,total)));

        careerbuilderCount = resultSet.getInt("careerbuilder_count");
        jsonSourceArray.add(new JsonObject().put("source", "Career Builder")
            .put("count", careerbuilderCount));

        usjobsCount = resultSet.getInt("usjobs_count");
        jsonSourceArray.add(new JsonObject().put("source", "US Jobs")
            .put("count", usjobsCount));

        indeedCount = resultSet.getInt("indeed_count");
        jsonSourceArray.add(new JsonObject().put("source", "Indeed")
            .put("count", indeedCount));

        googleplusCount = resultSet.getInt("googleplus_count");
        jsonSourceArray.add(new JsonObject().put("source", "Google Plus")
            .put("count", googleplusCount)
            .put("share",getSourceShare(googleplusCount,total)));
      }

      JsonObject jsonSuccessObject = new JsonObject();
      jsonSuccessObject.put("code", HttpStatus.SC_OK);
      jsonSuccessObject.put("success", true);
      jsonSuccessObject.put("data", jsonSourceArray);

      if((googleplusCount == 0) && (indeedCount ==0) &&
          (usjobsCount == 0) && (careerbuilderCount == 0 ) &&
          (directCount == 0) &&  (glassdoorCount == 0) &&
          (monsterCount == 0 ) && (twitterCount == 0 ) &&
          (linkedinCount == 0 ) &&  (facebookCount == 0)){

        jsonSuccessObject.put("no_data", true);
      } else {
        jsonSuccessObject.put("no_data", false);
      }

      connectionObject.close();

      context.response()
          .setStatusCode(jsonSuccessObject.getInteger("code"))
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonSuccessObject.toString());

    } catch (SQLException updateSourceSQLException) {

      logger.error("SQL Exception in Display Report Sources");
      logger.error(updateSourceSQLException.getMessage());

      ResponseBuilder.writeInternalServerError(context.response());

    } catch (ClassNotFoundException updateSourceException) {

      logger.error("Class Not Found Exception in Display Report Sources");
      logger.error(updateSourceException.getMessage());

      ResponseBuilder.writeInternalServerError(context.response());

    } catch (ParseException e) {

      logger.error("Date Format Exception in Display Report Sources");
      logger.error(e.getMessage());

      JsonObject jsonFailureObject = new JsonObject();
      jsonFailureObject.put("code", HttpStatus.SC_BAD_REQUEST);
      jsonFailureObject.put("message", "Get Source Information Failed");
      jsonFailureObject.put("description", e.getMessage());
      jsonResponseObject.put("error", jsonFailureObject);

      context.response()
          .setStatusCode(jsonFailureObject.getInteger("code"))
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());

    }
  }

  /**
   * Get job post reports
   * @param routingContext
   *
   * @author Pradeep Balasubramanian
   * @lastModifiedDate 28/FEB/2018
   *
   */
  public static void getJobPostReports(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();

    MultiMap params = request.params();

    Job jobModel = new Job();

    String[] requiredParams = {"organization_id"};
    String[] allowedParams = {"organization_id"};
    String[] jobPostParams = {"organization_id"};

    List<Validator> validators = new ArrayList<>();
    Validator jobValidator = new ValidatorImpl(jobPostParams, new HashMap<String, FieldOptions>() {{
      put("organization_id", FieldOptionsCommon.NUMBER);
    }});
    validators.add(jobValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams,
        requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          jobModel.setOrganization_id(Integer.parseInt(requestParams.get("organization_id")));
          ReportBO reportBO = new ReportBOImpl(dbHandle);
          ReturnObject<ReportEnum, JsonObject> boResponse = reportBO.getJobPostReports(jobModel);
          switch (boResponse.getStatusEnum()) {
            case VALUE_SUCCESS:
              dbHandle.commit();
              ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
                  "", boResponse.getData(), httpServerResponse);
              break;
            case DATA_NOT_AVAILABLE:
              dbHandle.commit();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  boResponse.getStatusEnum().getDescription(),
                  boResponse.getData(), true, httpServerResponse);
              break;
            case DATA_AVAILABLE:
              dbHandle.commit();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  boResponse.getStatusEnum().getDescription(),
                  boResponse.getData(), false, httpServerResponse);
              break;
            case NO_SUCH_VALUE:
              dbHandle.rollback();
              ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_BAD_REQUEST,
                  "Unable to find Reports", "Unable to find Reports",
                  httpServerResponse);
              break;
            default:
              dbHandle.rollback();
              ResponseHelper.writeInternalServerError(httpServerResponse);
              break;
          }
        });
  }

  /**
   * Get Applicant Status Reports
   * @param routingContext
   *
   * @author Pradeep Balasubramanian
   * @lastModifiedDate 06/MAR/2018
   *
   */
  public static void getCandidateStatusReports(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();

    MultiMap params = request.params();

    CandidateR candidate = new CandidateR();

    String[] requiredParams = {"organization_id"};
    String[] allowedParams = {"organization_id"};
    String[] applicantParams = {"organization_id"};

    List<Validator> validators = new ArrayList<>();
    Validator candidateValidator = new ValidatorImpl(applicantParams,
        new HashMap<String, FieldOptions>() {{
          put("organization_id", FieldOptionsCommon.NUMBER);
        }});

    validators.add(candidateValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams,
        requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          candidate.setOrganizationId(Long.parseLong(requestParams.get("organization_id")));
          ReportBO reportBO = new ReportBOImpl(dbHandle);
          ReturnObject<ReportEnum, JsonArray> boResponse = reportBO.getCandidateStatusReports(
              candidate);
          switch (boResponse.getStatusEnum()) {
            case VALUE_SUCCESS:
              dbHandle.commit();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  "", boResponse.getData(), httpServerResponse);
              break;
            case DATA_NOT_AVAILABLE:
            case DATA_AVAILABLE:
              dbHandle.commit();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  boResponse.getStatusEnum().getDescription(),
                  boResponse.getData(), httpServerResponse);
              break;
            case NO_SUCH_VALUE:
              dbHandle.rollback();
              ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
                  "Unable to find Reports", "Unable to find Reports",
                  httpServerResponse);
              break;
            default:
              dbHandle.rollback();
              ResponseHelper.writeInternalServerError(httpServerResponse);
              break;
          }
        });
  }

  private static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                    DbHandle dbHandle,
                                    ReturnObject<ReportEnum, JsonObject> boResponse)
      throws SQLException, DbHandleException {
    switch (boResponse.getStatusEnum()) {
      case EXISTS_ALREADY:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_CONFLICT,
            "Report already exists","Report already exists",
            httpServerResponse);
        break;
      case ADDITION_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_CREATED,
            "Report added successfully", boResponse.getData(), httpServerResponse);
        break;
      case VALUE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "Report added successfully", boResponse.getData(), httpServerResponse);
        break;
      case ADDITION_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_CONFLICT,
            "Unable to add Report", "Report cannot be added",
            httpServerResponse);
        break;
      case UPDATE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_ACCEPTED,
            "Report updated successfully", boResponse.getData(), httpServerResponse);
        break;
      case NO_SUCH_VALUE:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_BAD_REQUEST,
            "Unable to find Report", "Unable to find Report",
            httpServerResponse);
        break;
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }

  }

  /**
   * getSourceShare returns the share of requested source
   * @param sourceCount
   * @param totalCount
   * @return
   */
  public Double getSourceShare(int sourceCount,int totalCount){
    if(totalCount==0){
      return 0.0;
    }
    return Double.valueOf(String.format("%.2f",
        Double.valueOf(sourceCount)/Double.valueOf(totalCount)*100.0));
  }

}