package com.auzmor.impl.handler;

import com.auzmor.bo.OnboardingDocumentBO;
import com.auzmor.bo.OnboardingFormBO;
import com.auzmor.bo.OnboardingPacketBO;
import com.auzmor.bo.OnboardingTaskBO;
import com.auzmor.impl.bo.OnboardingDocumentBOImpl;
import com.auzmor.impl.bo.OnboardingFormBOImpl;
import com.auzmor.impl.bo.OnboardingPacketBOImpl;
import com.auzmor.impl.bo.OnboardingTaskBOImpl;
import com.auzmor.impl.builder.ResponseBuilder;
import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.enumarator.OnboardingWelcomeEnum;
import com.auzmor.impl.enumarator.bo.OnboardingDocumentEnum;
import com.auzmor.impl.enumarator.bo.OnboardingFormEnum;
import com.auzmor.impl.enumarator.bo.OnboardingPacketEnum;
import com.auzmor.impl.enumarator.bo.OnboardingTaskEnum;
import com.auzmor.impl.enumarator.file.FileTypeEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.*;
import com.auzmor.impl.util.*;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.impl.model.EmailFormat;
import com.auzmor.impl.model.onboarding.OnboardingDocument;
import com.auzmor.impl.model.onboarding.OnboardingForm;
import com.auzmor.impl.model.onboarding.OnboardingPacket;
import com.auzmor.impl.model.onboarding.OnboardingTask;
import com.auzmor.impl.builder.ApiBuilder;

import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.FieldOptions;
import com.auzmor.util.validator.Validator;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.httpclient.HttpStatus;
import org.codehaus.plexus.util.StringUtils;

import java.sql.*;
import java.util.*;

/**
 * OnboardingPacketHandler - A class which contains API Implementation for the Onboarding Packets
 */

public class OnboardingPacketHandler extends ApiBuilder {

  private static final Logger logger = LoggerFactory.getLogger(OnboardingPacketHandler.class);
  /**
   * addPacket - A Post Method REST API Implementation for adding Packets
   *
   * @param context
   * @author Pradeep Balasubramanian
   * @lastModifiedDate Mar 8, 2018
   */
  public static void addPacket(RoutingContext context) {
    HttpServerRequest request = context.request();
    HttpServerResponse response = context.response();
    MultiMap params = request.params();
    OnboardingPacket onboardingPacket = new OnboardingPacket();

    String[] allowedParams = {"packet_name","organization_id","creator_id"};
    String[] requiredParams = {"packet_name","organization_id","creator_id"};
    String[] packetParams = {"packet_name","organization_id","creator_id"};

    List<Validator> validators = new ArrayList<>();
    Validator packetValidator = new ValidatorImpl(packetParams, OnboardingPacket.getValidatorMap());
    validators.add(packetValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(context, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          OnboardingPacketBO onboardingPacketBO = new OnboardingPacketBOImpl();
          ReturnObject<OnboardingPacketEnum, JsonObject> boResponse =
              onboardingPacketBO.addPacket(dbHandle, requestParams);
          OnboardingPacketHandler.
              writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  /**
   * addForm - A Post Method REST API Implementation for adding form
   *
   * @param routingContext
   * @author Pradeep Balasubramanian
   * @lastModifiedDate Mar 8, 2018
   */

  public static void uploadReferralDocForTask(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = routingContext.request().params();

    String[] allowedParams = {"packet_id","organization_id"};
    String[] requiredParams = {"packet_id","organization_id"};
    String[] taskParams = {"packet_id", "organization_id"};

    List<Validator> validators = new ArrayList<>();
    Validator fieldValidator = new ValidatorImpl(taskParams, new HashMap<String, FieldOptions>() {{
      put("packet_id", FieldOptionsCommon.NUMBER);
      put("organization_id", FieldOptionsCommon.NUMBER);
    }});
    validators.add(fieldValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    int organizationId = Integer.valueOf(params.get("organization_id"));
    int packetId = Integer.valueOf(params.get("packet_id"));
    String taskDocumentPath = "";

    Set<FileUpload> fileUploadSet = routingContext.fileUploads();
    for (FileUpload fileUpload : fileUploadSet) {
      FileTypeEnum fileTypeEnum = FileHelper.getFileType(fileUpload);
      if (FileUploadHelper.isSupportedDocFormat(fileTypeEnum)) {
        String sourceFile = fileUpload.uploadedFileName();
        String destinationFile = fileUpload.uploadedFileName().split("/")[1] +
              fileTypeEnum.getExtension();
        String destinationFolderPath = "private_static/" + organizationId + "/packet_referral_docs/" + packetId + "/";
        FileHelper.move(sourceFile, destinationFolderPath, destinationFile);
        taskDocumentPath = destinationFolderPath + destinationFile;
      } else {
        ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_BAD_REQUEST,
            "Please upload with supported file format",
            "Please upload with supported file format",
            response);
      }
    }

    routingContext.response()
          .setStatusCode(org.apache.http.HttpStatus.SC_CREATED)  // 201
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(taskDocumentPath);
  }



  /**
   * getOnboardingPackets - gets the list of onboarding packets
   *
   * @param context the routing context
   * @author Pradeep Balasubramanian
   * @lastModifiedDate Mar 8, 2018
   */
  public static  void getOnboardingPackets(RoutingContext context) {
    HttpServerRequest request = context.request();
    HttpServerResponse response = context.response();

    MultiMap params = context.request().params();

    String[] allowedParams = {"packet_id","organization_id"};
    String[] requiredParams = {"organization_id"};
    String[] taskParams = {"packet_id", "organization_id"};

    List<Validator> validators = new ArrayList<>();
    Validator fieldValidator = new ValidatorImpl(taskParams, new HashMap<String, FieldOptions>() {{
      put("packet_id", FieldOptionsCommon.NUMBER);
      put("organization_id", FieldOptionsCommon.NUMBER);
    }});
    validators.add(fieldValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(context, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          OnboardingPacketBO onboardingPacketBO = new OnboardingPacketBOImpl();
          ReturnObject<OnboardingPacketEnum, JsonObject> boResponse =
              onboardingPacketBO.getPacket(dbHandle, requestParams);
          OnboardingPacketHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }


  /**
   * SendRemainderMail - Send Email Remainder
   *
   * @param routingContext
   */

  public static void SendRemainderMail(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = routingContext.request().params();

    String[] allowedParams = {"from_address", "to_address", "subject_name",
        "mail_description","user_name"};
    String[] requiredParams = { "to_address", "subject_name", "mail_description"};
    String[] taskParams = {"from_address", "to_address", "subject_name",
        "mail_description","user_name"};

    List<Validator> validators = new ArrayList<>();
    Validator fieldValidator = new ValidatorImpl(taskParams, new HashMap<String, FieldOptions>() {{
      put("from_address", new FieldOptionsImpl(FieldOptionsCommon.EMAIL, -1, 150));
      put("to_address", new FieldOptionsImpl(FieldOptionsCommon.EMAIL, -1, 150));
      put("subject_name", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
      put("mail_description", FieldOptionsCommon.ANY);
      put("user_name", FieldOptionsCommon.ANY);
    }});
    validators.add(fieldValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
         OnboardingPacketBO onboardingPacketBO = new OnboardingPacketBOImpl();
          ReturnObject<OnboardingPacketEnum, JsonObject> boResponse =
           onboardingPacketBO.sendRemainderMail(dbHandle, requestParams);
          OnboardingPacketHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }


  /**
   * uploadDocuments - Upload Onboarding Documents
   *
   * @param context the routing context
   * @author Pradeep Balasubramanian
   */
  public static void uploadDocuments(RoutingContext context) {
    JsonObject errorObject = new JsonObject();
    JsonObject jsonResponseObject = new JsonObject();
    try {
      Set<FileUpload> fileUploadSet = context.fileUploads();
      for (FileUpload upload : fileUploadSet) {

        context.response()
            .setStatusCode(202)
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .end(upload.uploadedFileName());
      }
    } catch (Exception uploadDocumentException) {

      errorObject.put("code", 406);
      errorObject.put("message", "Invalid Data");
      errorObject.put("description", uploadDocumentException.getMessage());

      jsonResponseObject.put("error", errorObject);

      context.response()
          .setStatusCode(406)
          .putHeader("Content-Type", "application/json; charset=utf-8;")
          .end(jsonResponseObject.toString());
    }
  }


  /**
   * deleteOnboardingPacket - Delete OnboardingPacket
   *
   * @param context the routing context
   * @author Pradeep Balasubramanian
   */
  public static void deleteOnboardingPacket(RoutingContext context) {
    HttpServerRequest request = context.request();
    HttpServerResponse response = context.response();
    MultiMap params = context.request().params();

    String[] allowedParams = {"packetId","organization_id"};
    String[] requiredParams = {"packetId"};
    String[] taskParams = {"packetId", "organization_id"};

    List<Validator> validators = new ArrayList<>();
    Validator fieldValidator = new ValidatorImpl(taskParams, new HashMap<String, FieldOptions>() {{
      put("packetId", FieldOptionsCommon.NUMBER);
      put("organization_id", FieldOptionsCommon.NUMBER);
    }});
    validators.add(fieldValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(context, false,
        (httpServerResponse, requestParams, dbHandle) -> {
        OnboardingPacketBO onboardingPacketBO = new OnboardingPacketBOImpl();
        ReturnObject<OnboardingPacketEnum, JsonObject> boResponse =
            onboardingPacketBO.deletePacket(dbHandle, requestParams);
        OnboardingPacketHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  /**
   * sendOnboardingPacket - send onboarding packet to Employee
   *
   * @param routingContext
   * @author Pradeep Sudhakaran
   */
  public static void sendOnboardingPacket(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = routingContext.request().params();

    String[] allowedParams = {"employeeId", "packet_name"};
    String[] requiredParams = {"employeeId", "packet_name"};
    String[] employeeParams = {"employeeId", "packet_name"};

    List<Validator> validators = new ArrayList<>();
    Validator fieldValidator = new ValidatorImpl(employeeParams, new HashMap<String, FieldOptions>() {{
      put("employeeId", FieldOptionsCommon.NUMBER);
      put("packet_name", FieldOptionsCommon.NUMBER);
    }});
    validators.add(fieldValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
        OnboardingPacketBO onboardingPacketBO = new OnboardingPacketBOImpl();
        ReturnObject<OnboardingPacketEnum, JsonObject> boResponse =
              onboardingPacketBO.sendPacket(dbHandle, requestParams);
        OnboardingPacketHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }


  /**
   * To Update welcome parameters of a packet
   *
   * @param routingContext
   */
  public static void updateOnboardingPacket(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    OnboardingPacket onboardingPacket = new OnboardingPacket();

    String[] allowedParams = {"packetId","packet_name","organization_id","creator_id"};
    String[] requiredParams = {"packetId","packet_name"};
    String[] packetParams = {"packetId","packet_name","organization_id","creator_id"};

    List<Validator> validators = new ArrayList<>();
    Validator packetValidator = new ValidatorImpl(packetParams, OnboardingPacket.getValidatorMap());
    validators.add(packetValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
        OnboardingPacketBO onboardingPacketBO = new OnboardingPacketBOImpl();
        ReturnObject<OnboardingPacketEnum, JsonObject> boResponse =
              onboardingPacketBO.updatePacket(dbHandle, requestParams);
        OnboardingPacketHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }


  /**
   * getPacketForEmployee - Get Packet Details for Employee
   *
   * @param routingContext
   * @author Pradeep Sudhakaran
   */
  public void getPacketForEmployee(RoutingContext routingContext) {
    JsonObject jsonResponseObject = new JsonObject();
    JsonObject errorObject = new JsonObject();
    errorObject.put("code", 406);
    JsonObject jsonSuccessObject = new JsonObject();
    ResultSet resultSetObject = null;
    DBCRUDUtil dbcrudUtil = new DBCRUDUtil();
    String queryString = "";
    String packet = "";
    List<String> constraintList = new ArrayList<>();
    try {
      Connection connection = new MysqldbConf().getConnection();
      final Integer employee_id = Integer.valueOf(routingContext.request().
          getParam("employee_id"));
      final Integer organization_id = Integer.valueOf(routingContext.request().
          getParam("organization_id"));

      queryString = "select onboarding_packet from Employee where employee_id = " + employee_id;
      packet = dbcrudUtil.returnValueForQuery(queryString);

      if (StringUtil.printValidString(packet).trim().length()==0) {
        throw new NoSuchFieldException("Onboarding Packet not been assigned to Employee");
      } else {
        queryString = "";
        final Integer packet_id = Integer.valueOf(packet);

        if (packet_id == 0) {
          throw new NoSuchFieldException("Onboarding Packet not been assigned to Employee");
        }

        Map<String, String> formQueryParameters = new HashMap<>();
        formQueryParameters.put("fields", "form_id,form_name,form_required");
        formQueryParameters.put("table", "OnboardingForm");

        constraintList = new ArrayList<>();
        constraintList.add("packet_id = " + packet_id);
        constraintList.add("inactive_status = 0");

        formQueryParameters.put("constraint", DatabaseUtil.buildConstraintString(constraintList));

        String formQueryString = buildQuery(formQueryParameters, "SELECT");

        ResultSet resultSetForPacketComponents = DatabaseUtil.
            getResultSetForSelectQuery(connection, formQueryString);


        JsonArray formArray = new JsonArray();
        while (resultSetForPacketComponents.next()) {
          JsonObject formObject = new JsonObject();
          formObject.put("form_id", resultSetForPacketComponents.getInt("form_id"));
          formObject.put("form_name",
              resultSetForPacketComponents.getString("form_name"));
          formObject.put("form_required", true);
          formArray.add(formObject);
        }
        HashMap<String, JsonObject> objectMap = new HashMap<>();


        for (int i = 0; i < formArray.size(); i++) {
          JsonObject form = formArray.getJsonObject(i);
          if (form.getString("form_name").equals("Welcome Page")) {
            objectMap.put("1", form);
          } else if (form.getString("form_name").equals("Employment Information")) {
            objectMap.put("2", form);
          } else if (form.getString("form_name").equals("Personal Information")) {
            objectMap.put("3", form);
          } else if (form.getString("form_name").equals("Emergency Contacts")) {
            objectMap.put("4", form);
          } else if (form.getString("form_name").equals("Direct Deposit")) {
            objectMap.put("5", form);
          } else if (form.getString("form_name").equals("Voluntary Information (EEO)")) {
            objectMap.put("6", form);
          } else {
            objectMap.put("7", form);
          }
        }

        Collection<JsonObject> tempList = objectMap.values();
        JsonArray sortedArray = new JsonArray();
        for (JsonObject tempForm : tempList) {
          sortedArray.add(tempForm);
        }

        Map<String, String> formWelcomeParameters = new HashMap<>();
        formWelcomeParameters.put("fields",
            "welcome_header,welcome_description,welcome_video,latitude,longitude");
        formWelcomeParameters.put("table", "OnboardingPacket");

        constraintList = new ArrayList<>();
        constraintList.add("packet_id = " + packet_id);
        constraintList.add("inactive_status = 0");

        formWelcomeParameters.put("constraint", DatabaseUtil.buildConstraintString(constraintList));

        String welcomeQuery = buildQuery(formWelcomeParameters, "SELECT");

        ResultSet resultSetForWelcomeComponents = DatabaseUtil.getResultSetForSelectQuery(connection, welcomeQuery);
        JsonObject welcomeObject = new JsonObject();
        if (resultSetForWelcomeComponents.next() && null != resultSetForWelcomeComponents.getString("welcome_header")) {
          welcomeObject.put("welcome_header",
              resultSetForWelcomeComponents.getString("welcome_header"));
          welcomeObject.put("welcome_description",
              resultSetForWelcomeComponents.getString("welcome_description"));
          welcomeObject.put("welcome_video",
              resultSetForWelcomeComponents.getString("welcome_video"));
          welcomeObject.put("latitude",
              resultSetForWelcomeComponents.getDouble("latitude"));
          welcomeObject.put("longitude",
              resultSetForWelcomeComponents.getDouble("longitude"));
        } else {
          String organizationName = new DatabaseUtil().getTablename("Organization",
              "organization_name", "organization_id = " + organization_id);
          welcomeObject.put("welcome_header", "Welcome to " + organizationName);
          welcomeObject.put("welcome_description",
              "We are excited to see you onboarded with our organization.");
          welcomeObject.put("latitude", -1);
          welcomeObject.put("longitude", -1);
        }

        Map<String, String> taskQueryParameters = new HashMap<>();
        taskQueryParameters.put("fields",
            "task_id,task_name,task_description,viewing_required,allow_doc_upload,doc_upload_required");
        taskQueryParameters.put("table", "OnboardingTask");
        taskQueryParameters.put("constraint", DatabaseUtil.buildConstraintString(constraintList));

        String taskQueryString = buildQuery(taskQueryParameters, "SELECT");

        resultSetForPacketComponents = DatabaseUtil.
            getResultSetForSelectQuery(connection, taskQueryString);
        JsonArray taskArray = new JsonArray();

        while (resultSetForPacketComponents.next()) {
          JsonObject taskObject = new JsonObject();
          taskObject.put("task_id", resultSetForPacketComponents.getInt("task_id"));
          taskObject.put("task_name",
              resultSetForPacketComponents.getString("task_name"));
          String checkTask = "select 1 from EmployeeTaskDocuments where task_name = ? " +
              "and employee_id = ?";
          PreparedStatement taskStatement = connection.prepareStatement(checkTask);
          taskStatement.setString(1,
              resultSetForPacketComponents.getString("task_name"));
          taskStatement.setInt(2, employee_id);

          ResultSet taskSet = taskStatement.executeQuery();
          boolean isCompleted = false;

          while (taskSet.next()) {
            isCompleted = taskSet.getBoolean(1);
          }

          taskObject.put("task_description",
              resultSetForPacketComponents.getString("task_description"));
          boolean viewingRequired = resultSetForPacketComponents
              .getBoolean("viewing_required");
          taskObject.put("viewing_required", viewingRequired);

          boolean allowDocUpload = resultSetForPacketComponents
              .getBoolean("allow_doc_upload");
          taskObject.put("allow_doc_upload", allowDocUpload);

          boolean docUploadRequired = resultSetForPacketComponents
              .getBoolean("doc_upload_required");
          taskObject.put("doc_upload_required", docUploadRequired);
          taskObject.put("is_completed", isCompleted);

          taskArray.add(taskObject);
        }

        Map<String, String> documentQueryParameters = new HashMap<>();
        documentQueryParameters.put("fields", "document_name,document_path,viewing_reqd," +
            "signing_required,page,x_coordinate,y_coordinate");
        documentQueryParameters.put("table", "OnboardingDocuments");
        documentQueryParameters.put("constraint",
            DatabaseUtil.buildConstraintString(constraintList));

        String documentQueryString = buildQuery(documentQueryParameters, "SELECT");

        PreparedStatement documentsPreparedStatement = connection.prepareStatement(documentQueryString);
        ResultSet documentsResultSet = documentsPreparedStatement.executeQuery();

        JsonArray documentArray = new JsonArray();
        while (documentsResultSet.next()) {
          JsonObject documentObject = new JsonObject();
          documentObject.put("document_name", documentsResultSet.getString("document_name"));
          String checkDocument = "select 1,document_file from EmployeeSignDocuments where document_name = ? " +
              " and employee_id = ?";
          PreparedStatement taskStatement = connection.prepareStatement(checkDocument);
          taskStatement.setString(1,
              documentsResultSet.getString("document_name"));
          taskStatement.setInt(2, employee_id);

          ResultSet docSet = taskStatement.executeQuery();
          boolean isCompleted = false;
          String documentSignedPath = "";

          while (docSet.next()) {
            isCompleted = docSet.getBoolean(1);
            documentSignedPath = docSet.getString("document_file");
          }
          docSet.close();
          taskStatement.close();
          documentObject.put("document_path", documentsResultSet.getString("document_path"));
          if (documentsResultSet.getBoolean("viewing_reqd") == true)
            documentObject.put("viewing_required", true);
          else
            documentObject.put("viewing_required", false);

          documentObject.put("is_completed", isCompleted);
          documentObject.put("signed_doc_path", documentSignedPath);
          documentObject.put("signing_required",
              documentsResultSet.getBoolean("signing_required"));
          documentObject.put("page", documentsResultSet.getInt("page"));
          documentObject.put("x_coordinate", documentsResultSet.getDouble("x_coordinate"));
          documentObject.put("y_coordinate", documentsResultSet.getDouble("y_coordinate"));
          documentArray.add(documentObject);
        }
        documentsResultSet.close();
        documentsPreparedStatement.close();

        Map<String, String> queryParameters = new HashMap<>();
        queryParameters.put("fields", "employee_profile_image");
        queryParameters.put("table", "Organization");

        constraintList = new ArrayList<>();
        constraintList.add("organization_id = " + organization_id);
        constraintList.add("active_status = 0");
        queryParameters.put("constraint", DatabaseUtil.buildConstraintString(constraintList));
        String organizationParameter = buildQuery(queryParameters, "SELECT");

        PreparedStatement orgStatement = connection.prepareStatement(organizationParameter);
        ResultSet orgResultSet = orgStatement.executeQuery();
        boolean employee_profile_image = false;
        while (orgResultSet.next()) {
          employee_profile_image = orgResultSet.getBoolean("employee_profile_image");
        }
        orgResultSet.close();
        orgStatement.close();

        jsonSuccessObject.put("welcome_page", welcomeObject);
        jsonSuccessObject.put("forms", sortedArray);
        jsonSuccessObject.put("tasks", taskArray);
        jsonSuccessObject.put("documents", documentArray);
        jsonSuccessObject.put("employee_profile_image", employee_profile_image);

        jsonResponseObject.put("code", 200);
        jsonResponseObject.put("status", true);
        jsonResponseObject.put("data", jsonSuccessObject);

        routingContext.response()
            .setStatusCode(202)
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .end(jsonResponseObject.toString());

        connection.close();
      }
    } catch (SQLException exception) {
      logger.error(exception.getMessage());
      ResponseHelper.writeInternalServerError(routingContext.response());
    } catch (NoSuchFieldException exception) {
      logger.error(exception.getMessage());
      errorObject.put("message", exception.getMessage());
      errorObject.put("description", exception.getMessage());
      jsonResponseObject.put("error", errorObject);
      routingContext.response()
          .setStatusCode(406)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
    } catch (ClassNotFoundException exception) {
      logger.error(exception.getMessage());
      ResponseHelper.writeInternalServerError(routingContext.response());
    }
  }

  /**
   * getAuzmorDocuments - Display the list of auzmor documents for an onboarding Packet
   *
   * @param context
   * @author Pradeep Balasubramanian
   */
  public void getAuzmorDocuments(RoutingContext context) {

    JsonObject jsonSuccessObject = new JsonObject();
    ResultSet resultSetObject = null;

    try {

      String packetId = context.request().params().get("packet_id");

      if (packetId == null || packetId.length() == 0) {
        throw new Exception("Required Fields are missing");
      }

      Connection connectionObject = new MysqldbConf().getConnection();

      Map<String, String> queryStringParameters = new HashMap<>();

      queryStringParameters.put("fields", "document_name,document_path");
      queryStringParameters.put("table", "AuzmorDocuments");

      String subQueryString = "select document_name from OnboardingDocuments " +
          "where packet_id = " + packetId + " "
          + " and inactive_status = 0";
      String constraintString = " where document_name not in (" + subQueryString + ")";

      queryStringParameters.put("constraint", constraintString);

      String queryString = buildQuery(queryStringParameters, "SELECT");

      resultSetObject = DatabaseUtil.getResultSetForSelectQuery(connectionObject, queryString);

      JsonObject jsonDataObject = new JsonObject();
      JsonArray formArray = new JsonArray();


      while (resultSetObject.next()) {
        JsonObject object = new JsonObject();
        object.put("document_name", resultSetObject.getString("document_name"));
        object.put("document_path", resultSetObject.getString("document_path"));
        formArray.add(object);
      }


      jsonDataObject.put("auzmor_documents", formArray);

      jsonSuccessObject.put("code", 200);
      jsonSuccessObject.put("success", true);
      jsonSuccessObject.put("data", jsonDataObject);

      connectionObject.close();

      context.response()
          .setStatusCode(200)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonSuccessObject.toString());

    } catch (Exception auzmorDocumentsException) {

      auzmorDocumentsException.printStackTrace();

      JsonObject jsonResponseObject = new JsonObject();
      JsonObject jsonErrorObject = new JsonObject();
      jsonErrorObject.put("code", 400);
      jsonErrorObject.put("message", "Bad Request");
      jsonErrorObject.put("description", auzmorDocumentsException.getMessage());
      jsonResponseObject.put("error", jsonErrorObject);

      context.response()
          .setStatusCode(jsonErrorObject.getInteger("code"))
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
    }
  }

  /**
   * getInhouseDocuments - Display the list of inhouse documents available for onboarding packet (organization specific)
   *
   * @param context The Routing Context
   * @author Pradeep Balasubramanian
   */
  public void getInhouseDocuments(RoutingContext context) {

    JsonObject jsonSuccessObject = new JsonObject();
    ResultSet resultSetObject = null;
    Connection connectionObject = null;

    try {
      String organizationId = context.request().getParam("organization_id");
      if (organizationId != null) {
        if (!StringUtils.isNumeric(organizationId)) {
          throw new Exception("Invalid Parameter entered");
        }
      } else {
        throw new Exception("Required Parameters are missing");
      }

      String packetId = context.request().getParam("packet_id");
      if (packetId != null) {
        if (!StringUtils.isNumeric(packetId)) {
          throw new Exception("Invalid Parameter entered");
        }
      } else {
        throw new Exception("Required Parameters are missing");
      }

      connectionObject = new MysqldbConf().getConnection();

      String queryString = "select distinct document_path from OnboardingDocuments " +
          "where packet_id in (select packet_id from OnboardingPacket " +
          "where organization_id = " + organizationId + " and inactive_status = 0) " +
          "and document_name not in (select document_name from AuzmorDocuments) " +
          "and document_name not in (select document_name from OnboardingDocuments " +
          "where packet_id = " + packetId + " and inactive_status = 0)";


      resultSetObject = DatabaseUtil.getResultSetForSelectQuery(connectionObject, queryString);

      List<String> documentPathList = new ArrayList<>();

      while (resultSetObject.next()) {
        documentPathList.add(resultSetObject.getString("document_path"));
      }
      JsonObject jsonDataObject = new JsonObject();
      JsonArray documentArray = new JsonArray();

      for (int i = 0; i < documentPathList.size(); i++) {

        queryString = "select distinct document_name,document_path from OnboardingDocuments " +
            "where document_path = '" + documentPathList.get(i) + "' " +
            " and document_name not in (select document_name from AuzmorDocuments) limit 1";
        resultSetObject = DatabaseUtil.getResultSetForSelectQuery(connectionObject, queryString);
        while (resultSetObject.next()) {
          JsonObject resultSetJsonObject = new JsonObject();
          resultSetJsonObject.put("document_name",
              resultSetObject.getString("document_name"));
          resultSetJsonObject.put("document_path",
              resultSetObject.getString("document_path"));

          documentArray.add(resultSetJsonObject);
        }

      }

      jsonDataObject.put("inhouse_documents", documentArray);

      jsonSuccessObject.put("code", 200);
      jsonSuccessObject.put("success", true);
      jsonSuccessObject.put("data", jsonDataObject);

      context.response()
          .setStatusCode(200)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonSuccessObject.toString());

    } catch (Exception auzmorDocumentsException) {

      auzmorDocumentsException.printStackTrace();

      JsonObject jsonResponseObject = new JsonObject();
      JsonObject jsonErrorObject = new JsonObject();
      jsonErrorObject.put("code", 400);
      jsonErrorObject.put("message", "Bad Request");
      jsonErrorObject.put("description", auzmorDocumentsException.getMessage());
      jsonResponseObject.put("error", jsonErrorObject);

      context.response()
          .setStatusCode(jsonErrorObject.getInteger("code"))
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
    } finally {
      DbUtils.closeQuietly(connectionObject);
    }

  }

  /**
   * Write response based on OnboardingPacketEnum values
   * @param params
   * @param httpServerResponse
   * @param dbHandle
   * @param boResponse
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 01/MAR/2018
   */
  public static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   ReturnObject<OnboardingPacketEnum, JsonObject> boResponse)
      throws SQLException, DbHandleException {

    switch (boResponse.getStatusEnum()) {
      case ADD_FAILURE:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
      case UPDATE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_ACCEPTED,
            "packet details updated successfully",boResponse.getData(), httpServerResponse);
        break;
      case ADD_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_CREATED,
            "packet details added successfully", boResponse.getData(), httpServerResponse);
        break;
      case VALUE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "Details retrived", boResponse.getData(), httpServerResponse);
        break;
      case NO_SUCH_VALUE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "No packet details present", new JsonObject(), httpServerResponse);
        break;
      case UPDATE_FAILURE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "No values to be updated", new JsonObject(), httpServerResponse);
        break;
      case ALREADY_EXISTS:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_BAD_REQUEST,
            "Packet Already exists",
            "Packet Already exists",
            httpServerResponse);
        break;
      case REMAINDER_SENT:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "Remainder Sent", boResponse.getData(), httpServerResponse);
        break;
      case DELETE_FAILURE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "No values to be deleted", new JsonObject(), httpServerResponse);
        break;
      case DELETE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "packet deleted successfully", boResponse.getData(), httpServerResponse);
        break;
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }
  // end of Class OnboardingPacketHandler
}
