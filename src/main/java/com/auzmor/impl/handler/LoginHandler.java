package com.auzmor.impl.handler;

import com.auth0.jwt.exceptions.InvalidClaimException;
import com.auzmor.bo.LoginBO;
import com.auzmor.bo.OrganizationBO;
import com.auzmor.dao.EmployeeDAO;
import com.auzmor.dao.OrganizationDAO;
import com.auzmor.impl.bo.*;
import com.auzmor.impl.builder.ResponseBuilder;
import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.dao.EmployeeDAOImpl;
import com.auzmor.impl.dao.OrganizationDAOImpl;
import com.auzmor.impl.enumarator.UserTypeEnum;
import com.auzmor.impl.enumarator.bo.LoginBOEnum;
import com.auzmor.impl.enumarator.kong.KongHttpStatusCode;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.*;
import com.auzmor.impl.model.Login;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.Role;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.util.Mail;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.FieldOptions;
import com.auzmor.util.validator.Validator;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.sql.*;
import java.util.*;

public class LoginHandler {

  private static final Logger logger = LoggerFactory.getLogger(LoginHandler.class);
//
//  /**
//   * Writes corresponding response based on the LoginBOEnum
//   *
//   * @param requestParams
//   * @param httpServerResponse
//   * @param dbHandle
//   * @param returnObject
//   * @throws SQLException
//   * @throws DbHandleException
//   * @author Pradeep Balasubramanian
//   * @lastModifiedDate 13/FEB/2018
//   */
//  /*private static void writeResponse(MultiMap requestParams, HttpServerResponse httpServerResponse,
//                                    DbHandle dbHandle,
//                                    ReturnObject<LoginBOEnum, JsonObject> returnObject)
//      throws SQLException, DbHandleException {
//
//    switch (returnObject.getStatusEnum()) {
//
//    }
//  }*/

  public static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   LoginBOEnum loginBOEnum)
      throws SQLException, DbHandleException {
    JsonObject dataObject = new JsonObject();
    switch (loginBOEnum) {
      case ALREADY_EXISTS:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_CONFLICT,
            params.get("email") + " is already a part of the organization",
            "Cannot invite as the invited person already exists in your organization",
            httpServerResponse);
        break;
      case EMPLOYEE_REJECTED:
        dbHandle.rollback();
        dataObject.put("message", "Access has been rejected");
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Employee rejected", dataObject, httpServerResponse);
        break;
      case EMPLOYEE_APPROVED:
        dbHandle.rollback();
        dataObject.put("message", "Access has been approved");
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "employee approved",
            dataObject, httpServerResponse);
        break;
      case EMPLOYEE_APPROVED_PACKETSENT:
        dbHandle.rollback();
        dataObject.put("message", "Access has been approved and packet has been sent");
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "employee approved and packet sent",
            dataObject, httpServerResponse);
        break;
      case INVALID_DATA:
        dbHandle.rollback();
        dataObject.put("message", "Invalid data entered");
        ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_CONFLICT,
            "invalid data",
            dataObject, httpServerResponse);
        break;
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }

  /**
   * @param params
   * @param httpServerResponse
   * @param dbHandle
   * @param jsonObject
   * @param loginBOEnum
   * @throws SQLException
   * @throws DbHandleException
   * @author sadhanaVenkatesan
   */
  public static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle, JsonObject jsonObject,
                                   LoginBOEnum loginBOEnum)
      throws SQLException, DbHandleException {
    switch (loginBOEnum) {
      case USER_LOGIN_ADDED:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_CREATED,
            "Registered successfully", jsonObject, httpServerResponse);
        break;
      case CANDIDATE_LOGIN_FAIL:
        dbHandle.rollback();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "candidate not registered with this organization",
            jsonObject, httpServerResponse);
        break;
      case CANDIDATE_LOGIN_SUCCESS:
        dbHandle.rollback();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "candidate is already registered with this organization",
            jsonObject, httpServerResponse);
        break;


      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;

    }

  }

  /*public static String generateLoginToken(Login loginObject) throws Exception {
    String generatedToken = "";
    java.sql.Date expirationDate = new java.sql.Date(System.currentTimeMillis() + (24 * 60 * 60 * 1000));
    String organizationName = "";
    String organisationAddress = "";
    String imageUrl = "";
    String logo = "";
    String favicon = "";
    String createdDate = "";
    String employeeProfileImage = "";
    boolean temp_password = false;
    Connection connectionObject = new MysqldbConf().getConnection();
    Statement preparedStatement = null;

    int organizationId = (int) loginObject.getOrganizationId();
    String constraintString = " where organization_id = " + organizationId;
    String queryString = "select organization_name,hris_url,logo,favicon,created_date " +
        "from Organization";
    preparedStatement = connectionObject.createStatement();
    ResultSet resultSetObject = preparedStatement.executeQuery(queryString + constraintString);

    while (resultSetObject.next()) {
      organizationName = resultSetObject.getString("organization_name");
      organisationAddress = resultSetObject.getString("hris_url");
      logo = StringUtil.printValidString(resultSetObject.getString("logo"));
      favicon = StringUtil.printValidString(resultSetObject.getString("favicon"));
      createdDate = StringUtil.printValidString(resultSetObject.getString("created_date"));
    }

    //sadhana create these for login check in the candidate
    int id = 0;
    int user_type_id = 0;
    queryString = "select status,candidate_id from Candidate,Login where id=user_id and " +
        "user_type ='candidate' and" +
        "  user_id = " + loginObject.getId() + " and (status ='Interview' or status ='Offer') ";
    preparedStatement = connectionObject.createStatement();
    ResultSet resultSetObject1 = preparedStatement.executeQuery(queryString);
    String status = "";
    int statusCheck = 0;
    while (resultSetObject1.next()) {
      id = resultSetObject1.getInt("candidate_id");
      if (resultSetObject1.getString("status").equalsIgnoreCase("Interview")) {
        statusCheck = new LoginHandler().
            getstatuscheck(connectionObject, "InterviewProcessCandidate",
                " and interview_cancel_reason is null ", id, "order by interview_id desc limit 1");
        if (statusCheck == 1) {
          status = "Interview";
        } else {
          status = "none";
        }
      } else {
        statusCheck = new LoginHandler().getstatuscheck(connectionObject,
            "OfferProcessCandidate", "and " +
                "offer_decline_reason is null ", id, "order by id desc limit 1");
        if (statusCheck == 1) {
          status = "Offer";
        } else {
          status = "none";
        }
      }
    }
    if (status.equals("")) {
      status = "none";
    }

    if (StringUtil.printValidString(loginObject.getConfirmPassword()).trim().length() == 0) {
      temp_password = true;
    } else {
      temp_password = false;
    }

    int empId = 0;


    if (loginObject.getUserType().equalsIgnoreCase("employee") ||
        loginObject.getUserType().equalsIgnoreCase("admin")) {

      String empQuery = "select employee_id,profile_image from Employee " +
          "where user_id = ?";
      PreparedStatement empPrepareStatement = connectionObject.prepareStatement(empQuery);
      empPrepareStatement.setLong(1, loginObject.getId());
      ResultSet empResultSet = empPrepareStatement.executeQuery();
      while (empResultSet.next()) {
        empId = empResultSet.getInt("employee_id");
        employeeProfileImage = empResultSet.getString("profile_image");
      }
      user_type_id = empId;
    } else {
      user_type_id = (int) loginObject.getId();
    }
    //else if (loginObject.getUserType().equalsIgnoreCase("candidate")) {
    //  user_type_id = id;
    //}

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
    java.util.Date dateFromDb = simpleDateFormat.parse(createdDate);
    java.util.Date current = new java.util.Date();
    String candidateCreateDateString = simpleDateFormat.format(current);
    java.util.Date currentDate = simpleDateFormat.parse(candidateCreateDateString);

    long dateDiff = currentDate.getTime() - dateFromDb.getTime();

    float days = (dateDiff / (1000 * 60 * 60 * 24));

    boolean isExpired = false;
    boolean isJobCountEmpty = false;

    if (days > 31.0) {
      isExpired = true;
    }

    String jobQueryCount = "select count(*) as job_count from Job where organization_id = ?";
    PreparedStatement jobPreparedStatement = connectionObject.prepareStatement(jobQueryCount);
    jobPreparedStatement.setLong(1, loginObject.getOrganizationId());
    ResultSet jobResultSet = jobPreparedStatement.executeQuery();

    int jobCount = 0;
    while (jobResultSet.next()) {
      jobCount = jobResultSet.getInt("job_count");
    }

    if (jobCount == 0) {
      isJobCountEmpty = true;
    }

    connectionObject.close();
    Algorithm algorithm = Algorithm.HMAC256("auzmor");
    generatedToken = JWT.create().withIssuer("auth0").withExpiresAt(expirationDate)
        .withClaim("email", loginObject.getEmail())
        .withClaim("organization_id", new Double(loginObject.getOrganizationId()))
        .withClaim("user_id", user_type_id)
        .withClaim("first_name", loginObject.getFirstName())
        .withClaim("last_name", loginObject.getLastName())
        .withClaim("organization_name", organizationName)
        .withClaim("user_type", loginObject.getUserType())
        .withClaim("organization_url", organisationAddress)
        .withClaim("image_url", logo)
        .withClaim("favicon", favicon)
        .withClaim("status", status)
        .withClaim("profile_image", StringUtil.printValidString(employeeProfileImage))
        .withClaim("isTemporaryPassword", temp_password)
        .withClaim("organizationIsExpired", isExpired)
        .withClaim("jobCountIsNull", isJobCountEmpty)
        .sign(algorithm);
    return generatedToken;
  }*/

  public static void authenticate(RoutingContext routingContext) {
    logger.info("Authenticating user");
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"email", "password", "organization_id", "user_type"};
    String[] requiredParams = {"email", "password", "organization_id", "user_type"};

    List<Validator> validators = new ArrayList<>();
    Validator emailTemplateValidator = new ValidatorImpl(allowedParams, Login.getValidatorMap());
    validators.add(emailTemplateValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, reauestParams, dbHandle) -> {
          long organizationId = Long.parseLong(params.get("organization_id"));
          logger.info("Get login with credentials");
          String userType11 = params.get("user_type");
          Login login = new LoginBOImpl(dbHandle).validateCredentialsAndGetDetails(
              params.get("email"), params.get("password"), organizationId);
          if (null == login) {
            logger.info("Invalid details to login");
            ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST, "Invalid Credentials",
                "Either username/password is wrong", response);
            return;
          } else if (null == login.getConsumerId()) {
            logger.info("kong is not configured properly");
            ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST, "Account Issue",
                "Your account is not properly configured in the system", response);
            return;
          }
          OrganizationBO organizationBO = new OrganizationBOImpl(dbHandle);
          Organization organization = organizationBO.getOrganizationDetails(organizationId);
          long jobCount = new JobBOImpl(dbHandle).getJobCount(organizationId);

          if (login.getUserType().equalsIgnoreCase("employee awaiting approval") && userType11.equalsIgnoreCase("employee")) {
            logger.info("Access request awaiting approval from administrator");
            ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST, "Login blocked. " +
                    "Access request awaiting approval from administrator.",
                "Login blocked. Access request awaiting approval from administrator.", response);
            return;
          }
          if (login.getUserType().equalsIgnoreCase("employee awaiting approval") && userType11.equalsIgnoreCase("candidate")) {
            login.setUserType("candidate");
          }
          String userType = (login.getUserType().startsWith("employee") || login.getUserType().equals("admin")) ?
              "employee" : "candidate";
          Employee employee = (userType.equals("employee")) ?
              new EmployeeBOImpl(dbHandle).getEmployeeDetailsByLoginId(login.getId()) : null;
          if (userType.equals("employee") && employee == null) {
            logger.info("Your account has been rejected by site admin");
            ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST, "Access Denied by Admin",
                "Your account has been rejected by site admin", response);
            return;
          }
          String key = "auzmor-" + UUID.randomUUID().toString() + "-hcm";
          String secret = UUID.randomUUID().toString();
          HttpClient httpClient = KongServiceHelper.executeWithKongClient(routingContext.vertx());
          Map<Long, String> rolesMap = organizationBO.getAllRolesAsMap(organizationId);
          logger.info("Creating a session");
          KongServiceHelper.createCredential(httpClient, login.getConsumerId(), key, secret,
              httpClientResponse -> {
                if (httpClientResponse != null && httpClientResponse.statusCode() == KongHttpStatusCode.ADD.getStatusCode()) {
                  httpClientResponse.bodyHandler(buffer -> {
                    KongServiceHelper.getConsumerRoles(httpClient, login.getConsumerId(), httpClientAclResponse -> {
                      if (httpClientAclResponse != null && httpClientAclResponse.statusCode() == KongHttpStatusCode.GET.getStatusCode()) {
                        httpClientAclResponse.bodyHandler(bufferGroups -> {
                          try {
                            JsonObject jsonObject = new JsonObject(bufferGroups);
                            JsonArray groupArray = jsonObject.getJsonArray("data");
                            List<String> roles = new ArrayList<>();
                            for (Map<String, String> groupObject : (List<Map<String, String>>) groupArray.getList()) {
                              roles.add(rolesMap.get(Long.parseLong(groupObject.get("group"))).toLowerCase());
                            }
                            routingContext.addCookie(JwtHelper.generateCookie(key, secret,
                                request.getHeader("X-Real-Host") != null ? request.getHeader("X-Real-Host") : ""));
                            httpClient.close();
                            JsonObject successObject = new JsonObject().put("token",
                                JwtHelper.getUserInfoJwt(key, secret, jobCount, employee, roles, login, organization));
                            ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK, "Success", successObject,
                                routingContext.response());
                          } catch (UnsupportedEncodingException e) {
                            logger.error("Unable to create session");
                            httpClient.close();
                            ResponseHelper.writeInternalServerError(routingContext.response());
                          }
                        });
                      } else {

                        logger.error("Unable to create session in kong");
                        httpClient.close();
                        ResponseHelper.writeInternalServerError(routingContext.response());
                      }
                    });
                  });
                } else {
                  logger.error("Unable to create session in kong");
                  httpClient.close();
                  ResponseHelper.writeInternalServerError(routingContext.response());
                }
              });
        });
  }

  public static void candidateLoginCheck(RoutingContext routingContext) {
    logger.info("email check for all users");
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"email", "organization_id"};
    String[] requiredParams = {"email", "organization_id"};
    String[] loginApproveParams = {"email", "organization_id"};

    List<Validator> validators = new ArrayList<>();
    Validator loginApproveValidator = new ValidatorImpl(loginApproveParams, Login.getValidatorMap());
    validators.add(loginApproveValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }
    logger.info("All params are valid");
    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          Login login = new Login();
          login.setOrganizationId(Long.parseLong(params.get("organization_id")));
          login.setEmail(params.get("email"));
          LoginBO loginBO = new LoginBOImpl(dbHandle);
          JsonObject jsonObject = new JsonObject();
          if (loginBO.candidateLoginCheck(login)) {
            logger.info("email id exists with organization");
            jsonObject.put("candidate_check", true);
            LoginHandler.writeResponse
                (requestParams, httpServerResponse, dbHandle, jsonObject, LoginBOEnum.CANDIDATE_LOGIN_SUCCESS);
          } else {
            logger.info("email id not exists with organization");
            jsonObject.put("candidate_check", false);
            LoginHandler.writeResponse
                (requestParams, httpServerResponse, dbHandle, jsonObject, LoginBOEnum.CANDIDATE_LOGIN_FAIL);
          }

        });

  }

  /*public Login getLoginUserDetails(Map<String, String> loginUserMapper, Login loginObject)
      throws Exception {
    logger.info("getting the login details");
    int count = 0;
    Connection connectionObject = new MysqldbConf().getConnection();
    PreparedStatement preparedStatement = null;
    String constraintString = "";
    String passwordString = "";
    for (Map.Entry<String, String> loginUserEntries : loginUserMapper.entrySet()) {
      if ("password".equals(loginUserEntries.getKey())) {
        String encryptString = getEncryptedForm(connectionObject,
            AESUtil.getAESEncyptString(loginUserEntries.getValue()));
        constraintString += loginUserEntries.getKey() + " = '" + encryptString + "' and ";
      } else {
        constraintString += loginUserEntries.getKey() + " = '" +
            loginUserEntries.getValue() + "' and ";
      }
    }
    constraintString = constraintString.substring(0, constraintString.length() - 4);

    // String checkEmailString = "select id from Login where email = ? and organization_id = ?";
    // preparedStatement = connectionObject.prepareStatement(checkEmailString);
    // preparedStatement.setString(1,loginUserMapper.get("email"));
    // preparedStatement.setInt(2,Integer.valueOf(loginUserMapper.get("organization_id")));
    // ResultSet emailResultSet = preparedStatement.executeQuery();
    // int email_id = 0;

    // while(emailResultSet.next()) {
    //  email_id = emailResultSet.getInt("id");
    // }

    // if(email_id==0) {
    // throw new Exception("E-Mail Address is not registered");
    //}

    String queryString = "SELECT email,id,first_name,last_name,user_type,organization_id," +
        "confirm_password FROM " +
        " Login WHERE " + constraintString;
    preparedStatement = connectionObject.prepareStatement(queryString);
    ResultSet resultSetObject = preparedStatement.executeQuery();
    while (resultSetObject.next()) {
      loginObject.setEmail(resultSetObject.getString("email"));
      loginObject.setId(resultSetObject.getInt("id"));
      loginObject.setFirstName(resultSetObject.getString("first_name"));
      loginObject.setLastName(resultSetObject.getString("last_name"));
      loginObject.setPassword(passwordString);
      loginObject.setUserType(resultSetObject.getString("user_type"));
      loginObject.setOrganizationId(resultSetObject.getInt("organization_id"));
      ++count;
    }
    if (count == 0) {
      logger.info("Invalid login credentials");
      throw new Exception("Invalid login credentials");
    } else {
      // inserting into LoginDetails table done by sadhana on 20/11/2017
      InetAddress localhost = InetAddress.getLocalHost();
      String ipaddr = (localhost.getHostAddress()).trim();     // getting local ip address.done by sadhana on 21/11/2017
      String ip = "";
      try {
        URL whatismyip = new URL("http://checkip.amazonaws.com");
        BufferedReader in = new BufferedReader(new InputStreamReader(
            whatismyip.openStream()));

        ip = in.readLine(); //you get the IP as a String
      } catch (Exception e) {
        e.printStackTrace();
      }

      java.util.Date dNow = new Date();
      SimpleDateFormat DateFormat =
          new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
      logger.info("inserting login details");
      String insertqurey = "insert into LoginDetails(login_id,ip_address,date_time) values ('"
          + loginObject.getId() + "'," +
          "  '" + ip + "','" + DateFormat.format(dNow) + "')";

      Statement stmt = connectionObject.createStatement();
      stmt.executeUpdate(insertqurey, Statement.RETURN_GENERATED_KEYS);
      ResultSet rs = stmt.getGeneratedKeys();

      int loginid = 0;

      if (rs.next()) {
        loginid = rs.getInt(1);
      }


    }
    return loginObject;
  }*/

  /**
   * Signup user to ATS
   *
   * @param routingContext
   */
  public void registerUser(RoutingContext routingContext) {
    logger.info("signup user to ATS");
    boolean isExceptionOccured = false;
    Connection connection = null;
    try {
      MultiMap params = (routingContext.request()).params();
      HttpServerResponse response = routingContext.response();

      connection = new MysqldbConf().getConnection();
      connection.setAutoCommit(false);

      String userType = params.get("user_type");
      params.remove("user_type");

      String email = params.get("email");
      int organizationId = Integer.parseInt(params.get("organization_id"));
      StringBuilder stringBuilder = new StringBuilder("select organization_name,hris_url,logo_image from Organization");
      stringBuilder.append(" where organization_id = ?");

      PreparedStatement preparedStatement = connection.prepareStatement(stringBuilder.toString());
      preparedStatement.setLong(1, organizationId);
      String organizationName = "";

      ResultSet resultSetObject = preparedStatement.executeQuery();
      String organizationUrl = "";
      while (resultSetObject.next()) {
        organizationUrl = resultSetObject.getString("hris_url");
        organizationName = resultSetObject.getString("organization_name");
      }

      resultSetObject.close();
      preparedStatement.close();

      String message = "Registration Successful";
      if (("employee").equalsIgnoreCase(userType)) {
        userType = "employee awaiting approval";
      } else {
        userType = "candidate";
      }

      logger.info("Registration Successful for the userType=" + userType);
      stringBuilder = new StringBuilder("SELECT COUNT(1) FROM `Login` WHERE `email` = ? and ");
      stringBuilder.append("`active_status` = 1 and `organization_id` = ?");

      preparedStatement = connection.prepareStatement(stringBuilder.toString());
      preparedStatement.setString(1, email);
      preparedStatement.setLong(2, organizationId);
      ResultSet countResultSet = preparedStatement.executeQuery();
      int emailCount = -1;
      while (countResultSet.next()) {
        emailCount = countResultSet.getInt(1);
      }
      countResultSet.close();
      preparedStatement.close();
      if (emailCount != 0) {
        logger.info("Username exists with email");
        throw new IllegalArgumentException("Username exists with email");
      }

      Mail mail = new Mail();

      if ("candidate".equalsIgnoreCase(userType)) {

        String candidateMailBody = "<html><body>Hello , <br> " +
            "<p>Congratulations! Your registration with " + organizationUrl + " " +
            "as a candidate is successful.<br/>" +
            "Thank you,<br/> " + organizationName + " Team <br/>" +
            "--------------------------------------------------------------\n<br/>" +
            "This is system generated mail,please do not reply to this mail\n<br/>" +
            "-------------------------------------------------------------- " +
            "</body></html>";

        String candidateMailDeliveryStatus = mail.sendMail(Vertx.vertx(), candidateMailBody,
            email, "Candidate Registration");


        if (!"success".equals(candidateMailDeliveryStatus)) {
          throw new IllegalArgumentException("Unable to send mail to Candidate");
        }
      } else if ("employee awaiting approval".equalsIgnoreCase(userType)) {
        stringBuilder = new StringBuilder("select email from Login where organization_id = ? ");
        stringBuilder.append("and user_type = 'admin' limit 1");
        String recipientAddress = "";

        preparedStatement = connection.prepareStatement(stringBuilder.toString());
        preparedStatement.setLong(1, organizationId);
        ResultSet adminEmailResultSet = preparedStatement.executeQuery();
        while (adminEmailResultSet.next()) {
          recipientAddress = adminEmailResultSet.getString("email");
        }

        adminEmailResultSet.close();
        preparedStatement.close();
        String employeeName = params.get("first_name") + " " + params.get("last_name");
        String adminMailBody = "<html><body>A new employee (" + employeeName + ") " +
            "has registered with email id " + email + " on " + organizationName + "." +
            "\n Please click <a href='" + organizationUrl +
            "/settings/profile'>here</a> to verify the registration and approve.<br>" +
            "Thank you,<br/> " + organizationName + " Team <br/>" +
            "--------------------------------------------------------------\n<br/>" +
            "This is system generated mail,please do not reply to this mail\n<br/>" +
            "-------------------------------------------------------------- " +
            "</body></html>";

        String emailMailBody = "<html><body>Hello " + "<b>" + employeeName + "</b>, <br> " +
            "<p>Thanks for signing up with " + organizationUrl + ". " +
            "Your request for access has been sent to" +
            " admin. <br>Once approved, you will receive an email notification.<br>" +
            "Thank you, <br/> " + organizationName + " Team <br/>" +
            "--------------------------------------------------------------\n<br/>" +
            "This is system generated mail,please do not reply to this mail\n<br/>" +
            "-------------------------------------------------------------- " +
            "</body></html>";


        String subject = "New Employee Registration";
        String employeeMailSubject = "New Employee Registration";
        String adminMailDeliveryStatus = mail.sendMail(Vertx.vertx(), adminMailBody,
            recipientAddress, subject);
        String employeeMailDeliveryStatus = mail.sendMail(Vertx.vertx(), emailMailBody,
            email, employeeMailSubject);

        if (!"success".equals(employeeMailDeliveryStatus)) {
          throw new IllegalArgumentException("Unable to send email to employee");
        }
        if (!"success".equals(adminMailDeliveryStatus)) {
          throw new IllegalArgumentException("Unable to send mail to administrator");
        }
      } else {
        throw new IllegalArgumentException("access type is worng");
      }

      stringBuilder = new StringBuilder("INSERT INTO `Login` (`email`, `first_name`, `last_name`,");
      stringBuilder.append("`organization_id`, `password`, `user_type`) VALUES (?, ?, ?, ?, SHA1(?), ?)");
      preparedStatement = connection.prepareStatement(stringBuilder.toString(),
          Statement.RETURN_GENERATED_KEYS);
      preparedStatement.setString(1, email);
      preparedStatement.setString(2, params.get("first_name"));
      preparedStatement.setString(3, params.get("last_name"));
      preparedStatement.setLong(4, organizationId);
      preparedStatement.setString(5, params.get("password"));
      preparedStatement.setString(6, userType);
      preparedStatement.executeUpdate();
      ResultSet rs = preparedStatement.getGeneratedKeys();
      int registerId = 0;
      if (rs.next()) {
        registerId = rs.getInt(1);
      }
      rs.close();
      preparedStatement.close();
      String registerIdString = String.valueOf(registerId);
      if ("employee awaiting approval".equals(userType)) {
        DbHelper.safeExecuteDbHandleMethod(routingContext, false,
            (httpServerResponse, requestParams, dbHandle) -> {
              EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
              Employee employee = new Employee();
              employee.setFirst_name(params.get("first_name"));
              employee.setLast_name(params.get("last_name"));
              employee.setPersonal_email("");
              employee.setWork_email(email);
              employee.setJob_title("");
              employee.setManager("");
              employee.setLocation("");
              employee.setPay_rate("");
              employee.setPay_type("");
              employee.setHire_date(" ");
              employee.setEmployment_status("");
              employee.setPrimary_phone("");
              employee.setUser_id(Integer.valueOf(registerIdString));
              employee.setStatus("awaiting approval");
              employee.setOnboarding_status("Self Registered");
              employee.setOrganization_id(organizationId);
              int createdRowId = employeeDAO.create(employee);
              if (createdRowId > 0) {
                dbHandle.commit();
              } else {
                logger.info("insertion of employee details failed");
                dbHandle.rollback();
                throw new IllegalArgumentException("Unable to add new employee");
              }
            });
      }

      connection.commit();

      JsonObject dataObject = new JsonObject();
      dataObject.put("id", registerId);

      if ("employee awaiting approval".equals(userType)) {
        message += " Access request sent to admin for approval";
      }
      LoginBOEnum loginBOEnum = null;
      DbHandle dbHandle = DbHelper.getDBHandle(routingContext, false);
      if (null == dbHandle) {
        return;
      }
      String loginId = "" + registerId;
      if (registerId > 0) {
        HttpClient kongClient = KongServiceHelper.executeWithKongClient(routingContext.vertx());
        String finalUserType = userType;
        long finalLoginId = registerId;
        String finalMessage = message;
        logger.info("adding kong to user");
        KongServiceHelper.addConsumer(kongClient, Long.parseLong("" + registerId), clientResponse -> {
          if (clientResponse.statusCode() == KongHttpStatusCode.ADD.getStatusCode()) {
            clientResponse.bodyHandler(buffer -> {
              String consumerId = new JsonObject(buffer).getString("id");
              String accessRole;
              if (finalUserType.equals("candidate")) {
                accessRole = "Candidate";
              } else {
                accessRole = "Employee";
              }
              Role role = null;
              try {
                new LoginBOImpl(dbHandle).updateConsumerId(consumerId, Long.parseLong(loginId));
                role = new RoleBOImpl(dbHandle).getByName(accessRole, Long.parseLong(params.get("organization_id")));
                dbHandle.commit();   //commit db changes
              } catch (SQLException e) {
                DbHelper.safeRollback(dbHandle);
                ResponseHelper.writeInternalServerError(response);
              } catch (DbHandleException e) {
                DbHelper.safeRollback(dbHandle);
                ResponseHelper.writeInternalServerError(response);
              }
              KongServiceHelper.addConsumerRoles(kongClient, consumerId, "" + role.getId(), httpClientResponse -> {
                dataObject.put("message", finalMessage);
                try {
                  if (finalUserType.equals(UserTypeEnum.CANDIDATE.getUserTypeValue())) {
                    LoginHandler.generateAuthToken(routingContext, response, dbHandle, finalLoginId);
                  } else {
                    LoginHandler.writeResponse(params, response,
                        dbHandle, dataObject, LoginBOEnum.USER_LOGIN_ADDED);
                  }
                  kongClient.close();

                } catch (SQLException e) {
                  kongClient.close();
                  DbHelper.safeRollback(dbHandle);
                  ResponseHelper.writeInternalServerError(response);
                } catch (DbHandleException e) {
                  kongClient.close();
                  DbHelper.safeRollback(dbHandle);
                  ResponseHelper.writeInternalServerError(response);
                }
              });
            });
          } else {
            DbHelper.safeRollback(dbHandle);
            dbHandle.checkAndCloseConnection();
            kongClient.close();
          }
        });
      }


    } catch (IllegalArgumentException registerException) {
      isExceptionOccured = true;
      logger.error("Illegal Argument Exception in User Register");
      ResponseBuilder.getErrorObject("Invalid Data", registerException.getMessage(),
          routingContext.response(), "POST");

    } catch (SQLException sqlException) {
      isExceptionOccured = true;
      logger.error("SQL Exception in User Register");
      ResponseBuilder.writeInternalServerError(routingContext.response());

    } catch (ClassNotFoundException classNotFoundException) {
      isExceptionOccured = true;
      logger.error("Class Not Found Exception in User Register");
      ResponseBuilder.writeInternalServerError(routingContext.response());

    } finally {
      try {
        if (isExceptionOccured) {
          connection.rollback();
        }
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException sqlException) {
        logger.error("Connection close Exception in User Register");
      }
    }
  }

  /*public String getEncryptedForm(Connection connection, String string) throws Exception {
    Statement preparedStatement = connection.createStatement();
    String query = "select SHA1('" + string + "') as encryptString from dual";
    ResultSet resultSet = preparedStatement.executeQuery(query);
    String encryptedString = "";
    while (resultSet.next()) {
      encryptedString = resultSet.getString("encryptString");
    }

    return encryptedString;
  }*/

  /**
   * Providing a link to the user for resetting password though the user's email id
   *
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 23-10-2017
   */

  public void resetPwd(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    String origin = request.getHeader("X-Real-Host");  //to extract hris_url from request header

    String[] allowedParams = {"email", "hris_url"};
    String[] requiredParams = {"email", "hris_url"};
    String[] loginParams = {"email", "hris_url"};

    params.remove("hris_url");
    params.add("hris_url", origin);

    List<Validator> validators = new ArrayList<>();
    Validator fieldValidator = new ValidatorImpl(loginParams, new HashMap<String, FieldOptions>() {{
      put("email", new FieldOptionsImpl(FieldOptionsCommon.EMAIL, -1, 100));
      put("hris_url", FieldOptionsCommon.ANY);
    }});
    validators.add(fieldValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }
    logger.info("All params are Valid");
    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          LoginBO loginBO = new LoginBOImpl(dbHandle);
          try {
            LoginBOEnum loginBOEnum = loginBO.resetPassword(requestParams);
            JsonObject dataObject = new JsonObject();
            dataObject.put("message", loginBOEnum.getDescription());
            switch (loginBOEnum) {
              case INVALID_DATA:
                dbHandle.rollback();
                ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
                    loginBOEnum.getDescription(),
                    loginBOEnum.getDescription(),
                    httpServerResponse);
                break;
              case RESET_MAIL_SENT:
                dbHandle.commit();
                ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
                    "Reset mail sent", dataObject, httpServerResponse);
                break;
              case EMPLOYEE_NOT_EXISTS:
                dbHandle.rollback();
                ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
                    loginBOEnum.getDescription(),
                    loginBOEnum.getDescription(),
                    httpServerResponse);
                break;
              default:
                dbHandle.commit();
                ResponseHelper.writeInternalServerError(httpServerResponse);
                break;
            }
          } catch (UnsupportedEncodingException e) {
            logger.error(e.getMessage());
            ResponseHelper.writeInternalServerError(httpServerResponse);
          }
        });
  }

  /**
   * Verify the user to whom a hyper link has been provided
   *
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 23-10-2017
   */
  public void checkResetPwd(RoutingContext routingContext) {
    logger.info("checking the reset password");
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"token"};
    String[] requiredParams = {"token"};
    String[] loginParams = {"token"};

    List<Validator> validators = new ArrayList<>();
    Validator fieldValidator = new ValidatorImpl(loginParams, new HashMap<String, FieldOptions>() {{
      put("token", FieldOptionsCommon.ANY);
    }});
    validators.add(fieldValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          LoginBO loginBO = new LoginBOImpl(dbHandle);
          try {
            ReturnObject<LoginBOEnum, JsonObject> boResponse =
                loginBO.checkResetMail(requestParams);

            switch (boResponse.getStatusEnum()) {
              case REDIRECT:
                dbHandle.commit();
                JsonObject jsonObject = new JsonObject();
                jsonObject = boResponse.getData();
                String organizationUrl = jsonObject.getString("hris_url");
                String keyValue = jsonObject.getString("key_value");
                routingContext.response()
                    .setStatusCode(HttpStatus.SC_MOVED_TEMPORARILY)
                    .putHeader("location", "https://" + organizationUrl +
                        "/auth/reset-password/" + keyValue)
                    .end();
                logger.info("reset done");
                break;
              case LINK_USED_CANDIDATE:
                dbHandle.rollback();
                jsonObject = boResponse.getData();
                organizationUrl = jsonObject.getString("hris_url");
                routingContext.response()
                    .setStatusCode(HttpStatus.SC_MOVED_TEMPORARILY)
                    .putHeader("location", "https://" + organizationUrl +
                        "/410-error/candidate")
                    .end();
              case LINK_USED_EMPLOYEE:
                dbHandle.rollback();
                jsonObject = boResponse.getData();
                organizationUrl = jsonObject.getString("hris_url");
                routingContext.response()
                    .setStatusCode(HttpStatus.SC_MOVED_TEMPORARILY)
                    .putHeader("location", "https://" + organizationUrl +
                        "/410-error/employee")
                    .end();
              default:
                dbHandle.commit();
                ResponseHelper.writeInternalServerError(httpServerResponse);
                break;
            }
          } catch (UnsupportedEncodingException e) {
            logger.error(e.getMessage());
            ResponseHelper.writeInternalServerError(httpServerResponse);
          } catch (InvalidClaimException e) {
            logger.error(e.getMessage());
            logger.info("Link has expired");
            String encryptedString = requestParams.get("token");
            String[] parts = encryptedString.split("\\.");
            String payLoad = StringUtils.newStringUtf8(Base64.decodeBase64(parts[1]));
            JsonObject payloadJson = new JsonObject(payLoad);
            long organizationId = Long.valueOf(payloadJson.getString("organization_id"));
            String userType = payloadJson.getString("user_type");

            Organization organization = new Organization();
            OrganizationDAO organizationDAO = new OrganizationDAOImpl(dbHandle);
            organization = organizationDAO.getOrganizationById(organizationId);

            if (userType.equals(LoginBOEnum.CANDIDATE)) {
              routingContext.response()
                  .setStatusCode(HttpStatus.SC_MOVED_PERMANENTLY)
                  .putHeader("location", "https://" + organization.getHris_url() +
                      "/auth/link-expired?candidate")
                  .end();
            } else {
              routingContext.response()
                  .setStatusCode(HttpStatus.SC_MOVED_PERMANENTLY)
                  .putHeader("location", "https://" + organization.getHris_url() +
                      "/auth/link-expired?employee")
                  .end();
            }
          }
        });
  }

  /**
   * Update password after reset mail successful verification
   *
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 21-03-2018
   */
  public void updatePasswordForReset(RoutingContext routingContext) {
    logger.info("update password for reset");
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"user_id", "password", "confirm_password"};
    String[] requiredParams = {"user_id", "password", "confirm_password"};
    String[] loginParams = {"user_id", "password", "confirm_password"};

    List<Validator> validators = new ArrayList<>();
    Validator fieldValidator = new ValidatorImpl(loginParams, new HashMap<String, FieldOptions>() {{
      put("password", FieldOptionsCommon.ANY);
      put("confirm_password", FieldOptionsCommon.ANY);
      put("user_id", FieldOptionsCommon.ANY);
    }});
    validators.add(fieldValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          LoginBO loginBO = new LoginBOImpl(dbHandle);
          ReturnObject<LoginBOEnum, JsonObject> boResponse = loginBO.changePasswordForReset(requestParams);
          JsonObject dataObject = new JsonObject();
          dataObject.put("message", boResponse.getStatusEnum().getDescription());

          if (boResponse.getStatusEnum() == LoginBOEnum.PASSWORD_CHANGED ||
              boResponse.getStatusEnum() == LoginBOEnum.PASSWORD_NOCHANGE) {
            Login login = new Login();
            dbHandle.commit();
            long organizationId = Long.valueOf(boResponse.getData().getLong("organization_id"));
            OrganizationBO organizationBO = new OrganizationBOImpl(dbHandle);
            Organization organization = organizationBO.getOrganizationDetails(organizationId);

            long loginId = Long.valueOf(boResponse.getData().getLong("id"));
            long jobCount = new JobBOImpl(dbHandle).getJobCount(organizationId);
            String userTypeFromDB = boResponse.getData().getString("user_type");

            login.setId(loginId);
            login.setUserType(userTypeFromDB);
            login.setConsumerId(boResponse.getData().getString("consumer_id"));
            login.setEmail(boResponse.getData().getString("email"));
            login.setFirstName(boResponse.getData().getString("first_name"));
            login.setLastName(boResponse.getData().getString("last_name"));

            if (userTypeFromDB.equalsIgnoreCase("employee awaiting approval")) {
              logger.info("Access request awaiting approval from administrator");
              ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST, "Login blocked. " +
                      "Access request awaiting approval from administrator.",
                  "Login blocked. Access request awaiting approval from administrator.", response);
              return;
            }
            String userType = (userTypeFromDB.startsWith("employee") || userTypeFromDB.equals("admin")) ?
                "employee" : "candidate";
            Employee employee = (userType.equals("employee")) ?
                new EmployeeBOImpl(dbHandle).getEmployeeDetailsByLoginId(loginId) : null;
            if (userType.equals("employee") && employee == null) {
              logger.info("Your account has been rejected by site admin");
              ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST, "Access Denied by Admin",
                  "Your account has been rejected by site admin", response);
              return;
            }

            String key = "auzmor-" + UUID.randomUUID().toString() + "-hcm";
            String secret = UUID.randomUUID().toString();
            HttpClient httpClient = KongServiceHelper.executeWithKongClient(routingContext.vertx());
            logger.info("Creating a session");
            Map<Long, String> rolesMap = organizationBO.getAllRolesAsMap(organizationId);
            KongServiceHelper.createCredential(httpClient, login.getConsumerId(), key, secret,
                httpClientResponse -> {
                  if (httpClientResponse != null && httpClientResponse.statusCode() == KongHttpStatusCode.ADD.getStatusCode()) {
                    httpClientResponse.bodyHandler(buffer -> {
                      KongServiceHelper.getConsumerRoles(httpClient, login.getConsumerId(), httpClientAclResponse -> {
                        if (httpClientAclResponse != null && httpClientAclResponse.statusCode() == KongHttpStatusCode.GET.getStatusCode()) {
                          httpClientAclResponse.bodyHandler(bufferGroups -> {
                            try {
                              JsonObject jsonObject = new JsonObject(bufferGroups);
                              JsonArray groupArray = jsonObject.getJsonArray("data");
                              List<String> roles = new ArrayList<>();
                              for (Map<String, String> groupObject : (List<Map<String, String>>) groupArray.getList()) {
                                roles.add(rolesMap.get(Long.parseLong(groupObject.get("group"))).toLowerCase());
                              }
                              routingContext.addCookie(JwtHelper.generateCookie(key, secret,
                                  request.getHeader("X-Real-Host") != null ? request.getHeader("X-Real-Host") : ""));
                              httpClient.close();
                              JsonObject successObject = new JsonObject().put("token",
                                  JwtHelper.getUserInfoJwt(key, secret, jobCount, employee, roles, login, organization));
                              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK, "Success", successObject,
                                  routingContext.response());
                            } catch (UnsupportedEncodingException e) {
                              logger.error("Unable to create session");
                              httpClient.close();
                              ResponseHelper.writeInternalServerError(routingContext.response());
                            }
                          });
                        }
                      });
                    });
                  } else {
                    logger.error("Unable to create session in kong");
                    httpClient.close();
                    ResponseHelper.writeInternalServerError(routingContext.response());
                  }
                });

          } else if (boResponse.getStatusEnum() == LoginBOEnum.PASSWORD_MATCH_FAILED) {
            dbHandle.rollback();
            ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
                "Passwords does not match",
                "Passwords does not match",
                httpServerResponse);
          } else {
            dbHandle.rollback();
            ResponseHelper.writeInternalServerError(httpServerResponse);
          }
        });
  }

  /**
   * Updating user password. While updation any exceptions thrown are handled
   *
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 23-10-2017
   */

  public void updatePassword(RoutingContext routingContext) {
    logger.info("updating the password");
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"id", "old_password", "password", "confirm_password", "user_type"};
    String[] requiredParams = {"id", "old_password", "password", "confirm_password", "user_type"};
    String[] loginParams = {"id", "old_password", "password", "confirm_password", "user_type"};

    List<Validator> validators = new ArrayList<>();
    Validator fieldValidator = new ValidatorImpl(loginParams, new HashMap<String, FieldOptions>() {{
      put("password", FieldOptionsCommon.ANY);
      put("confirm_password", FieldOptionsCommon.ANY);
      put("old_password", FieldOptionsCommon.ANY);
      put("id", FieldOptionsCommon.NUMBER);
      put("organization_id", FieldOptionsCommon.NUMBER);
      put("user_type", FieldOptionsCommon.ANY);
    }});
    validators.add(fieldValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          LoginBO loginBO = new LoginBOImpl(dbHandle);
          LoginBOEnum loginBOEnum = loginBO.changePassword(requestParams);
          JsonObject dataObject = new JsonObject();
          dataObject.put("message", loginBOEnum.getDescription());
          switch (loginBOEnum) {
            case PASSWORD_CHANGED:
              dbHandle.commit();
              logger.info("password changed");
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  "Password Updated Successfully", dataObject, httpServerResponse);
              break;
            case PASSWORD_NOCHANGE:
              dbHandle.commit();
              logger.info("no change in password changed");
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  "Password Updated Successfully", dataObject, httpServerResponse);
              break;
            case PASSWORD_INCORRECT:
              dbHandle.rollback();
              logger.info("Incorrect old password entered");
              ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
                  "Incorrect old password entered",
                  "Incorrect old password entered",
                  httpServerResponse);
              break;
            case PASSWORD_MATCH_FAILED:
              dbHandle.rollback();
              logger.info("Passwords does not match");
              ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
                  "Passwords does not match",
                  "Passwords does not match",
                  httpServerResponse);
              break;
            default:
              dbHandle.rollback();
              ResponseHelper.writeInternalServerError(httpServerResponse);
              break;
          }
        });
  }

  // sadhana created these login table on 09/11/2017
  /*public int addLoginDetails(Login loginModel) throws SQLException, ClassNotFoundException {
    Connection connectionObject = new MysqldbConf().getConnection();
    Map<String, Object> insertionObjects = new LinkedHashMap<>();
    java.util.Date dNow = new java.util.Date();
    SimpleDateFormat ft =
        new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

    insertionObjects.put("first_name", loginModel.getFirstName());
    insertionObjects.put("last_name", loginModel.getLastName());
    insertionObjects.put("user_type", loginModel.getUserType());
    insertionObjects.put("email", loginModel.getEmail());
    insertionObjects.put("password", loginModel.getPassword());
    insertionObjects.put("confirm_password", "");
    insertionObjects.put("organization_id", loginModel.getOrganizationId());
    insertionObjects.put("key_value", loginModel.getKeyValue());
    insertionObjects.put("hash_value", loginModel.getHashValue());
    //insertionObjects.put("created_date", loginModel.getCreated_date());

    String columnValueString = "";
    String entryValueString = "";
    String appender = "";
    for (Map.Entry<String, Object> columnEntries : insertionObjects.entrySet()) {
      columnValueString += columnEntries.getKey() + ",";
      if ("password".equals(columnEntries.getKey())) {
        appender = "SHA1('" + AESUtil.getAESEncyptString((String) columnEntries.getValue()) + "')";
      } else {
        appender = "'" + columnEntries.getValue() + "'";
      }

      entryValueString += appender + ",";
    }

    entryValueString = entryValueString.substring(0, entryValueString.length() - 1);
    columnValueString = columnValueString.substring(0, columnValueString.length() - 1);
    String sqlQueryForEmployee = "insert into Login (" + columnValueString + ") values(" + entryValueString + ");";
    Statement preparedStatement = connectionObject.createStatement();
    preparedStatement.executeUpdate(sqlQueryForEmployee, Statement.RETURN_GENERATED_KEYS);
    ResultSet rs = preparedStatement.getGeneratedKeys();

    int loginId = 0;

    if (rs.next()) {
      loginId = rs.getInt(1);
    }
    loginModel.setId(loginId);
    connectionObject.close();
    return loginId;
  }

  public int getstatuscheck(Connection connectionObject, String tablename, String constraint, int id, String orderby) {
    int count = 0;
    try {
      PreparedStatement preparedStatement = null;
      String queryString = "";

      queryString = "SELECT * FROM " + tablename + " where status is null " + constraint + " and candidate_id = " + id + " " + orderby;

      preparedStatement = connectionObject.prepareStatement(queryString);
      ResultSet resultSetObject = preparedStatement.executeQuery();
      while (resultSetObject.next()) {
        count = 1;
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
    return count;
  }*/

  public void approveUserSignup(RoutingContext routingContext) {
    logger.info("approve signup for self registered employee");
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"user_id", "organization_id", "onboarding_packet", "approve_status",
        "approve_onboardingstatus"};
    String[] requiredParams = {"user_id", "onboarding_packet", "approve_status", "approve_onboardingstatus"};
    String[] loginApproveParams = {"user_id", "organization_id", "onboarding_packet", "approve_status",
        "approve_onboardingstatus"};

    List<Validator> validators = new ArrayList<>();
    Validator loginApproveValidator = new ValidatorImpl(loginApproveParams, Login.getValidatorMap());
    validators.add(loginApproveValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }
    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          LoginBO loginBO = new LoginBOImpl(dbHandle);
          LoginBOEnum loginBOEnum = loginBO.approveEmployeeSignup(params);
          LoginHandler.writeResponse(requestParams, httpServerResponse, dbHandle, loginBOEnum);
        });

  }

  /*public void checkLogin(RoutingContext context) {
    Login loginObject = new Login();

    try {
      MultiMap params = (context.request()).params();
      Map<String, String> loginUserMapper = new HashMap<>();
      for (String key : params.names()) {
        loginUserMapper.put(key, params.get(key));
      }
      String emailParam = params.get("email");
      String password = params.get("password");
      String organizationId = params.get("organization_id");

      if (emailParam == null || password == null || organizationId == null) {

        if (organizationId == null) {
          logger.info("Organization is not registered");
          throw new Exception("Organization is not registered");
        } else if (emailParam == null) {
          logger.info("e-mail ID is Missing");
          throw new Exception("e-mail ID is Missing");
        } else if (password == null) {
          logger.info("Password is Missing");
          throw new Exception("Password is Missing");
        } else {
          logger.info("Required Fields are Missing");
          throw new Exception("Required Fields are Missing");
        }
      }

      JsonObject adminJsonObject = new JsonObject();

      LoginHandler loginHandlerObject = new LoginHandler();

      String loginCheckQuery = "select user_type,email from Login " +
          "where email = ? and organization_id = ?";

      Connection connection = new MysqldbConf().getConnection();
      PreparedStatement preparedStatement = connection.prepareStatement(loginCheckQuery);
      preparedStatement.setString(1, loginUserMapper.get("email"));
      preparedStatement.setString(2, loginUserMapper.get("organization_id"));

      ResultSet resultSet = preparedStatement.executeQuery();

      String userName = "", userType = "";
      while (resultSet.next()) {
        userName = resultSet.getString("email");
        userType = resultSet.getString("user_type");
      }

      if ("employee awaiting approval".equals(userType)) {
        throw new Exception("Unable to login . Awaiting approval from Administrator");
      } else if ("reject".equals(userType)) {
        throw new Exception("Self Registration has been rejected");
      }

      if (!"".equals(userName)) {
        logger.info("username is exists");
        loginObject = loginHandlerObject.getLoginUserDetails(loginUserMapper, loginObject);
        JsonObject successObject = new JsonObject();
        successObject.put("code", HttpStatus.SC_OK);
        successObject.put("success", true);

        loginObject.setToken(generateLoginToken(loginObject));
        adminJsonObject.put("token", loginObject.getToken());
        successObject.put("data", adminJsonObject);

        connection.close();
        context.response()
            .setStatusCode(HttpStatus.SC_CREATED)
            .putHeader("Content-Type", "application/json; charset=utf-8; id=" + loginObject.getId())
            .end(successObject.toString());
      } else {
        logger.info("email id not registered");
        throw new Exception("e-mail ID Not Registered");
      }
    } catch (Exception e) {
      e.printStackTrace();
      JsonObject errorObject = new JsonObject();
      errorObject.put("code", HttpStatus.SC_NOT_ACCEPTABLE);
      errorObject.put("message", e.getMessage());
      errorObject.put("description", e.getMessage());
      JsonObject jsonFailureObject = new JsonObject();
      jsonFailureObject.put("error", errorObject);
      context.response()
          .setStatusCode(HttpStatus.SC_NOT_FOUND)
          .putHeader("Content-Type", "application/json; charset=utf-8;")
          .end(jsonFailureObject.toString());
    }
  }*/

  public static void generateAuthToken(RoutingContext routingContext, HttpServerResponse response, DbHandle dbHandle, long loginId) {

    try {

      LoginBO loginBO =  new LoginBOImpl(dbHandle);
      Login login = loginBO.getLoginById(loginId);
      OrganizationBO organizationBO = new OrganizationBOImpl(dbHandle);
      Organization organization = organizationBO.getOrganizationDetails(login.getOrganizationId());
      long jobCount = new JobBOImpl(dbHandle).getJobCount(login.getOrganizationId());

      Employee employee = new Employee();

      String key = "auzmor-" + UUID.randomUUID().toString() + "-hcm";
      String secret = UUID.randomUUID().toString();
      HttpClient httpClient = KongServiceHelper.executeWithKongClient(routingContext.vertx());
      logger.info("Creating a session");
      Map<Long, String> rolesMap = organizationBO.getAllRolesAsMap(login.getOrganizationId());
      KongServiceHelper.createCredential(httpClient, login.getConsumerId(), key, secret,
          httpClientResponse -> {
            if (httpClientResponse != null && httpClientResponse.statusCode() == KongHttpStatusCode.ADD.getStatusCode()) {
              httpClientResponse.bodyHandler(buffer -> {
                KongServiceHelper.getConsumerRoles(httpClient, login.getConsumerId(), httpClientAclResponse -> {
                  if (httpClientAclResponse != null && httpClientAclResponse.statusCode() == KongHttpStatusCode.GET.getStatusCode()) {
                    httpClientAclResponse.bodyHandler(bufferGroups -> {
                      try {
                        JsonObject jsonObject = new JsonObject(bufferGroups);
                        JsonArray groupArray = jsonObject.getJsonArray("data");
                        List<String> roles = new ArrayList<>();
                        for (Map<String, String> groupObject : (List<Map<String, String>>) groupArray.getList()) {
                          roles.add(rolesMap.get(Long.parseLong(groupObject.get("group"))).toLowerCase());
                        }
                        routingContext.addCookie(JwtHelper.generateCookie(key, secret,
                            routingContext.request().getHeader("X-Real-Host") != null ?
                                routingContext.request().getHeader("X-Real-Host") : ""));
                        httpClient.close();
                        JsonObject successObject = new JsonObject().put("token",
                            JwtHelper.getUserInfoJwt(key, secret, jobCount, employee, roles, login, organization));
                        dbHandle.checkAndCloseConnection();
                        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK, "Success", successObject,
                            routingContext.response());
                      } catch (UnsupportedEncodingException e) {
                        logger.error("Unable to create session");
                        httpClient.close();
                        ResponseHelper.writeInternalServerError(routingContext.response());
                      }
                    });
                  }
                });
              });
            } else {
              logger.error("Unable to create session in kong");
              httpClient.close();
              ResponseHelper.writeInternalServerError(routingContext.response());
            }
          });

    } catch (DbHandleException exception) {
      logger.error(exception.getMessage());
      ResponseHelper.writeInternalServerError(response);
    } catch (SQLException exception) {
      logger.error(exception.getMessage());
      ResponseHelper.writeInternalServerError(response);
    }
  }

  public static void getLoggedInUserRoles(RoutingContext routingContext) {
    Login login = (Login)routingContext.data().get("login");

    HttpClient kongClient = KongServiceHelper.executeWithKongClient(routingContext.vertx());
    KongServiceHelper.getConsumerRoles(kongClient, login.getConsumerId(), httpClientAclResponse -> {
      if (httpClientAclResponse != null && httpClientAclResponse.statusCode() == KongHttpStatusCode.GET.getStatusCode()) {
        httpClientAclResponse.bodyHandler(bufferGroups -> {
          JsonObject jsonObject = new JsonObject(bufferGroups);
          DbHelper.safeExecuteDbHandleMethod(routingContext,false, (response, params, dbHandle) -> {
            OrganizationBO organizationBO = new OrganizationBOImpl(dbHandle);
            Map<Long, String> rolesMap = organizationBO.getAllRolesAsMap(login.getOrganizationId());
            JsonArray groupArray = jsonObject.getJsonArray("data");
            JsonArray jsonArray = new JsonArray();
            for (Map<String, String> groupObject : (List<Map<String, String>>) groupArray.getList()) {
               jsonArray.add(rolesMap.get(Long.parseLong(groupObject.get("group"))).toLowerCase());
            }
            ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK, "User Roles", jsonArray,
                routingContext.response());
          });
        });
      } else {
        ResponseHelper.writeInternalServerError(routingContext.response());
      }
    });
  }
}