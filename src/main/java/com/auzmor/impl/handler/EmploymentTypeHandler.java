package com.auzmor.impl.handler;

import com.auzmor.bo.EmploymentTypeBO;
import com.auzmor.impl.bo.EmploymentTypeBOImpl;
import com.auzmor.impl.enumarator.EmploymentTypeEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.EmploymentModel;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.Validator;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EmploymentTypeHandler {
  private static Logger logger = LoggerFactory.getLogger(EmploymentTypeHandler.class);

  /**
   * Adding a new employment type in a organization
   * @param routingContext
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  public static void addEmploymentType(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    EmploymentModel employmentModel = new EmploymentModel();

    String[] allowedParams = {"employmentType", "organization_id","active_status"};
    String[] requiredParams = {"employmentType", "organization_id"};
    String[] employmentParams = {"employmentType", "organization_id", "active_status"};

    List<Validator> validators = new ArrayList<>();
    Validator employmentValidator = new ValidatorImpl(employmentParams, EmploymentModel.getValidatorMap());
    validators.add(employmentValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          EmploymentTypeBO employmentTypeBO = new EmploymentTypeBOImpl();
          employmentModel.setEmploymentType(requestParams.get("employmentType"));
          employmentModel.setOrganizationId(Integer.valueOf(requestParams.get("organization_id")));

          ReturnObject<EmploymentTypeEnum, JsonObject> boResponse =
              employmentTypeBO.addEmploymentType(dbHandle,employmentModel);
          EmploymentTypeHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  /**
   * Get particular details of employment type
   * @param routingContext
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  public static void getEmploymentType(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    EmploymentModel employmentModel = new EmploymentModel();

    String[] allowedParams = {"id"};
    String[] requiredParams = {"id"};
    String[] employmentParams = {"id"};

    List<Validator> validators = new ArrayList<>();
    Validator employmentValidator = new ValidatorImpl(employmentParams, EmploymentModel.getValidatorMap());
    validators.add(employmentValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          employmentModel.setId(Integer.valueOf(requestParams.get("id")));
          EmploymentTypeBO employmentTypeBO = new EmploymentTypeBOImpl();
          ReturnObject<EmploymentTypeEnum, JsonObject> boResponse =
              employmentTypeBO.getEmploymentType(dbHandle, employmentModel);
          EmploymentTypeHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });

  }

  /**
   * Update a particular employment type
   * @param routingContext
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  public static void updateEmploymentType(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    EmploymentModel employmentModel = new EmploymentModel();

    String[] allowedParams = {"id","employmentType","organization_id", "active_status"};
    String[] requiredParams = {"id","employmentType"};
    String[] employmentParams = {"id","employmentType", "organization_id", "active_status"};

    List<Validator> validators = new ArrayList<>();
    Validator employmentValidator = new ValidatorImpl(employmentParams, EmploymentModel.getValidatorMap());
    validators.add(employmentValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          EmploymentTypeBO employmentTypeBO = new EmploymentTypeBOImpl();
          ReturnObject<EmploymentTypeEnum, JsonObject> boResponse =
              employmentTypeBO.updateEmploymentType(dbHandle,requestParams);
          EmploymentTypeHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });

  }

  /**
   * Delete a particular employment type
   * @param routingContext
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  public static void deleteEmploymentType(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    EmploymentModel employmentModel = new EmploymentModel();

    String[] allowedParams = {"id"};
    String[] requiredParams = {"id"};
    String[] employmentParams = {"id"};

    List<Validator> validators = new ArrayList<>();
    Validator employmentValidator = new ValidatorImpl(employmentParams, EmploymentModel.getValidatorMap());
    validators.add(employmentValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          EmploymentTypeBO employmentTypeBO = new EmploymentTypeBOImpl();
          employmentModel.setId(Integer.valueOf(requestParams.get("id")));
          ReturnObject<EmploymentTypeEnum, JsonObject> boResponse =
              employmentTypeBO.deleteEmploymentType(dbHandle, employmentModel);
          EmploymentTypeHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });

  }

  /**
   * Get all employment types in a particular organization
   * @param routingContext
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  public static void getAllEmploymentTypes(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    EmploymentModel employmentModel = new EmploymentModel();

    String[] allowedParams = {"organization_id"};
    String[] requiredParams = {"organization_id"};
    String[] employmentParams = {"organization_id"};

    List<Validator> validators = new ArrayList<>();
    Validator employmentValidator = new ValidatorImpl(employmentParams, EmploymentModel.getValidatorMap());
    validators.add(employmentValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          EmploymentTypeBO employmentTypeBO = new EmploymentTypeBOImpl();
          employmentModel.setOrganizationId(Integer.valueOf(requestParams.get("organization_id")));
          ReturnObject<EmploymentTypeEnum, JsonArray> boResponse =
              employmentTypeBO.getAllEmploymentTypes(dbHandle, employmentModel);
          switch (boResponse.getStatusEnum()) {
            case VALUE_SUCCESS:
              dbHandle.commit();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  "", boResponse.getData(), httpServerResponse);
              break;
            case NO_SUCH_VALUE:
              dbHandle.rollback();
              ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
                  "Unable to find Employment Type", "Unable to find Employment Type",
                  httpServerResponse);
              break;
            default:
              dbHandle.rollback();
              ResponseHelper.writeInternalServerError(httpServerResponse);
              break;
          }
        });
  }

  /**
   * Writing correct response according to enum values
   * @param params
   * @param httpServerResponse
   * @param dbHandle
   * @param boResponse
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  public static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   ReturnObject<EmploymentTypeEnum, JsonObject> boResponse)
      throws SQLException, DbHandleException {

    JsonObject dataObject = new JsonObject();
    dataObject.put("message", boResponse.getStatusEnum().getDescription());
    switch (boResponse.getStatusEnum()) {
      case EXISTS_ALREADY:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            params.get("employmentType") + " already exists",
            params.get("employmentType") + " already exists",
            httpServerResponse);
        break;
      case ADDITION_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Employment Type added successfully", dataObject, httpServerResponse);
        break;
      case VALUE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Employment Type added successfully", boResponse.getData(), httpServerResponse);
        break;
      case ADDITION_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
      case UPDATE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Employment Type updated successfully", dataObject, httpServerResponse);
        break;
      case NO_CHANGES_MADE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "No changes made", dataObject, httpServerResponse);
        break;
      case NO_SUCH_VALUE:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Unable to find Employment Type", "Unable to find Employment Type",
            httpServerResponse);
        break;
      case CANNOT_DELETE:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Employment Type cannot be deleted", "Employment Type cannot be deleted",
            httpServerResponse);
        break;
      case DELETE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Employment Type deleted successfully", dataObject, httpServerResponse);
        break;
      case RESTRICTED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Published Employment Type cannot be changed",
            "Published Employment Type cannot be changed",
            httpServerResponse);
        break;
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }
}
