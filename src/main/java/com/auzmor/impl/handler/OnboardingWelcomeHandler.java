package com.auzmor.impl.handler;

import com.auzmor.bo.OnboardingWelcomeBO;
import com.auzmor.impl.bo.OnboardingWelcomeBOImpl;
import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.enumarator.OnboardingWelcomeEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.onboarding.OnboardingPacket;
import com.auzmor.impl.util.StringUtil;

import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.FieldOptions;
import com.auzmor.util.validator.Validator;
import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderGeometry;
import com.google.code.geocoder.model.GeocoderRequest;

import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class OnboardingWelcomeHandler {
  private static Logger logger = LoggerFactory.getLogger(OnboardingWelcomeHandler.class);
  /**
   * To Update welcome parameters of a packet
   *
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 07-11-2017
   */
  public static void updateWelcomeScreen(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    OnboardingPacket onboardingPacket = new OnboardingPacket();

    String[] allowedParams = {"welcome_header","welcome_description","welcome_video","latitude",
        "longitude","organizationId"};
    String[] requiredParams = {"organizationId"};
    String[] welcomeParams = {"welcome_header","welcome_description","welcome_video","latitude",
        "longitude","organizationId"};

    List<Validator> validators = new ArrayList<>();
    Validator welcomeValidator = new ValidatorImpl(welcomeParams, OnboardingPacket.getValidatorMap());
    validators.add(welcomeValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          OnboardingWelcomeBO onboardingWelcomeBO = new OnboardingWelcomeBOImpl();
          ReturnObject<OnboardingWelcomeEnum, JsonObject> boResponse =
              onboardingWelcomeBO.updateWelcomeScreen(dbHandle,requestParams);
          OnboardingWelcomeHandler.writeResponse(requestParams,httpServerResponse,
              dbHandle,boResponse);
        });
  }

  public static void uploadVideo(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    OnboardingPacket onboardingPacket = new OnboardingPacket();

    String[] allowedParams = {"welcome_header","welcome_description","welcome_video","latitude",
        "longitude","organizationId"};
    String[] requiredParams = {"organizationId"};
    String[] welcomeParams = {"welcome_header","welcome_description","welcome_video","latitude",
        "longitude","organizationId"};

    List<Validator> validators = new ArrayList<>();
    Validator welcomeValidator = new ValidatorImpl(welcomeParams, OnboardingPacket.getValidatorMap());
    validators.add(welcomeValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    Set<FileUpload> fileUploadSet = routingContext.fileUploads();
    if (fileUploadSet.isEmpty()) {
      ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
          "File not available for upload",
          "File not available for upload",
          response);
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
        OnboardingWelcomeBO onboardingWelcomeBO = new OnboardingWelcomeBOImpl();
        ReturnObject<OnboardingWelcomeEnum, JsonObject> boResponse =
            onboardingWelcomeBO.uploadVideo(dbHandle,fileUploadSet,requestParams);
        OnboardingWelcomeHandler.writeResponse(requestParams,httpServerResponse,
              dbHandle,boResponse);
        });
  }

  public static void getMapFrame(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    OnboardingPacket onboardingPacket = new OnboardingPacket();

    String[] allowedParams = {"organization_id"};
    String[] requiredParams = {"organization_id"};
    String[] welcomeParams = {"organization_id"};

    List<Validator> validators = new ArrayList<>();
    Validator welcomeValidator = new ValidatorImpl(welcomeParams, OnboardingPacket.getValidatorMap());
    validators.add(welcomeValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    onboardingPacket.setOrganization_id(Integer.valueOf(params.get("organization_id")));

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          OnboardingWelcomeBO onboardingWelcomeBO = new OnboardingWelcomeBOImpl();
          ReturnObject<OnboardingWelcomeEnum, JsonObject> boResponse =
              onboardingWelcomeBO.getWelcomeDetails(dbHandle, onboardingPacket);
          OnboardingWelcomeHandler.writeResponse(requestParams,httpServerResponse,
              dbHandle,boResponse);
        });
  }

  public static void appMapAddress(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    OnboardingPacket onboardingPacket = new OnboardingPacket();

    String[] allowedParams = {"company_address_line1","company_address_line2","city","state",
        "zip","country","organization_id"};
    String[] requiredParams = {"organization_id"};
    String[] welcomeParams = {"company_address_line1","company_address_line2","city","state",
        "zip","country","organization_id"};

    List<Validator> validators = new ArrayList<>();
    Validator fieldValidator = new ValidatorImpl(welcomeParams, new HashMap<String, FieldOptions>() {{
      put("company_address_line1", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 350));
      put("company_address_line2", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
      put("city", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 30));
      put("zip", FieldOptionsCommon.NUMBER);
      put("country", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 30));
      put("state", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 20));
      put("organization_id", FieldOptionsCommon.NUMBER);
    }});
    validators.add(fieldValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          OnboardingWelcomeBO onboardingWelcomeBO = new OnboardingWelcomeBOImpl();
          ReturnObject<OnboardingWelcomeEnum, JsonObject> boResponse =
              onboardingWelcomeBO.appMapAddress(dbHandle, requestParams);
          OnboardingWelcomeHandler.writeResponse(requestParams,httpServerResponse,
              dbHandle,boResponse);
        });
  }

  public static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   ReturnObject<OnboardingWelcomeEnum, JsonObject> boResponse)
      throws SQLException, DbHandleException {

    JsonObject dataObject = new JsonObject();
    dataObject.put("message", boResponse.getStatusEnum().getDescription());
    switch (boResponse.getStatusEnum()) {
      case VIDEO_FORMAT_FAILURE:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Please update video in mp4 format",
            "Please update video in mp4 format",
            httpServerResponse);
        break;
      case VIDEO_UPLOAD_FAILURE:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
      case UPDATE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Welcome details updated successfully", boResponse.getData(), httpServerResponse);
        break;
      case VALUE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Details retrived", boResponse.getData(), httpServerResponse);
        break;
      case NO_SUCH_VALUE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Welcome details not present", boResponse.getData(), httpServerResponse);
        break;
      case UPDATE_FAILURE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "No values to be updated", boResponse.getData(), httpServerResponse);
        break;
      case NO_FILE_PRESENT:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Required Video file not available",
            "Required Video file not available",
            httpServerResponse);
        break;
      case VIDEO_UPLOAD_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Welcome Video uploaded successfully", dataObject, httpServerResponse);
        break;
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }
}