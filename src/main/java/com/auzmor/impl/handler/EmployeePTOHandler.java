package com.auzmor.impl.handler;

import com.auzmor.bo.EmployeePTOBO;
import com.auzmor.impl.bo.EmployeePTOBOImpl;
import com.auzmor.impl.enumarator.bo.EmployeePTOEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.PTO.EmployeePTORequests;
import com.auzmor.impl.model.PTO.PTOComments;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.Validator;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class EmployeePTOHandler {
  private static Logger logger = LoggerFactory.getLogger(EmployeePTOHandler.class);

  /**
   * Add new PTO Request
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  public static void addRequest(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    EmployeePTORequests employeePTORequests = new EmployeePTORequests();

    String[] allowedParams = {"employee_id", "from_date", "to_date", "hours", "description",
        "time_off_type"};
    String[] requiredParams = {"employee_id", "from_date", "to_date", "hours", "time_off_type"};
    String[] ptoParams = {"employee_id", "from_date", "to_date", "hours", "description",
        "time_off_type"};

    List<Validator> validators = new ArrayList<>();
    Validator employeePTOValidator = new ValidatorImpl(ptoParams, employeePTORequests.getValidatorMap());
    validators.add(employeePTOValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    if (!params.get("time_off_type").equals("Vacation")
        && !params.get("time_off_type").equals("Sick") &&
        !params.get("time_off_type").equals("Unpaid Time-Off")) {
      ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST, "Please use only allowed time off types",
          "Please use only allowed time off types", response);
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          EmployeePTOBO employeePTOBO = new EmployeePTOBOImpl();
          try {
            ReturnObject<EmployeePTOEnum, JsonObject> boResponse =
                employeePTOBO.addRequest(dbHandle, requestParams);
            EmployeePTOHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
          } catch (ParseException e) {
            logger.error("Parse Exception occurs "+e.getMessage());
            ResponseHelper.writeInternalServerError(httpServerResponse);
          }
        });
  }

  /**
   * Get all requests for employee
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  public static void allRequests(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    EmployeePTORequests employeePTORequests = new EmployeePTORequests();

    String[] allowedParams = {"employee_id"};
    String[] requiredParams = {"employee_id"};
    String[] ptoParams = {"employee_id"};

    List<Validator> validators = new ArrayList<>();
    Validator employeePTOValidator = new ValidatorImpl(ptoParams, employeePTORequests.getValidatorMap());
    validators.add(employeePTOValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
      EmployeePTOBO employeePTOBO = new EmployeePTOBOImpl();
          try {
            ReturnObject<EmployeePTOEnum, JsonArray> boResponse =
                employeePTOBO.allRequest(dbHandle, requestParams);
            switch (boResponse.getStatusEnum()) {
              case TABLE_EMPTY:
                dbHandle.commit();
                ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                    "no_data is true", boResponse.getData(), httpServerResponse);
                break;
              case TABLE_VALUES:
                dbHandle.commit();
                ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                    "no_data is false", boResponse.getData(), httpServerResponse);
                break;
              default:
                dbHandle.rollback();
                ResponseHelper.writeInternalServerError(httpServerResponse);
                break;
            }
          } catch (ParseException e) {
            logger.error("Parse Exception occurs "+e.getMessage());
            ResponseHelper.writeInternalServerError(httpServerResponse);
          }
        });
  }

  /**
   * Get all Used requests for employee
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  public static void allUsedRequests(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    EmployeePTORequests employeePTORequests = new EmployeePTORequests();

    String[] allowedParams = {"employee_id"};
    String[] requiredParams = {"employee_id"};
    String[] ptoParams = {"employee_id"};

    List<Validator> validators = new ArrayList<>();
    Validator employeePTOValidator = new ValidatorImpl(ptoParams, employeePTORequests.getValidatorMap());
    validators.add(employeePTOValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
      EmployeePTOBO employeePTOBO = new EmployeePTOBOImpl();
          try {
            ReturnObject<EmployeePTOEnum, JsonArray> boResponse =
                employeePTOBO.allUsedRequest(dbHandle, requestParams);
            switch (boResponse.getStatusEnum()) {
              case TABLE_EMPTY:
                dbHandle.commit();
                ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                    "no_data is true", boResponse.getData(), httpServerResponse);
                break;
              case TABLE_VALUES:
                dbHandle.commit();
                ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                    "no_data is false", boResponse.getData(), httpServerResponse);
                break;
              default:
                dbHandle.rollback();
                ResponseHelper.writeInternalServerError(httpServerResponse);
                break;
            }
          } catch (ParseException e) {
            logger.error("Parse Exception occurs "+e.getMessage());
            ResponseHelper.writeInternalServerError(httpServerResponse);
          }
        });
  }

  /**
   * Get maximum available hours for employee
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  public static void getAvailableHours(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    EmployeePTORequests employeePTORequests = new EmployeePTORequests();

    String[] allowedParams = {"employee_id"};
    String[] requiredParams = {"employee_id"};
    String[] ptoParams = {"employee_id"};

    List<Validator> validators = new ArrayList<>();
    Validator employeePTOValidator = new ValidatorImpl(ptoParams, employeePTORequests.getValidatorMap());
    validators.add(employeePTOValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
      EmployeePTOBO employeePTOBO = new EmployeePTOBOImpl();
      ReturnObject<EmployeePTOEnum, JsonObject> boResponse =
              employeePTOBO.getAvailableCount(dbHandle, requestParams);
      EmployeePTOHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  /**
   * Get upcoming time off for employee
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  public static void getUpcomingTimeOff(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    EmployeePTORequests employeePTORequests = new EmployeePTORequests();

    String[] allowedParams = {"employee_id"};
    String[] requiredParams = {"employee_id"};
    String[] ptoParams = {"employee_id"};

    List<Validator> validators = new ArrayList<>();
    Validator employeePTOValidator = new ValidatorImpl(ptoParams, employeePTORequests.getValidatorMap());
    validators.add(employeePTOValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
      EmployeePTOBO employeePTOBO = new EmployeePTOBOImpl();
          try {
            ReturnObject<EmployeePTOEnum, JsonArray> boResponse =
                employeePTOBO.getUpcomingTimeOff(dbHandle, requestParams);
            switch (boResponse.getStatusEnum()) {
              case TABLE_EMPTY:
                dbHandle.commit();
                ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                    "no_data is true", new JsonArray(), httpServerResponse);
                break;
              case TABLE_VALUES:
                dbHandle.commit();
                ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                    "no_data is false", boResponse.getData(), httpServerResponse);
                break;
              default:
                dbHandle.rollback();
                ResponseHelper.writeInternalServerError(httpServerResponse);
                break;
            }
          } catch (ParseException e) {
            logger.error("Parse Exception occurs "+e.getMessage());
            ResponseHelper.writeInternalServerError(httpServerResponse);
          }
        });
  }

  /**
   * Write response according to EmployeePTOEnum
   * @param params
   * @param httpServerResponse
   * @param dbHandle
   * @param boResponse
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  public static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   ReturnObject<EmployeePTOEnum, JsonObject> boResponse)
      throws SQLException, DbHandleException {
    JsonObject dataObject = new JsonObject();
    dataObject.put("message", boResponse.getStatusEnum().getDescription());
    switch (boResponse.getStatusEnum()) {
      case PARAMS_MISSING:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            boResponse.getStatusEnum().getDescription(),
            boResponse.getStatusEnum().getDescription(),
            httpServerResponse);
        break;
      case FAILED:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
      case ALREADY_EXISTS:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            boResponse.getStatusEnum().getDescription(),
            boResponse.getStatusEnum().getDescription(),
            httpServerResponse);
        break;
      case REQUEST_ADD_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
      case MAX_VALUE_MISSING:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            boResponse.getStatusEnum().getDescription(),
            boResponse.getStatusEnum().getDescription(),
            httpServerResponse);
        break;
      case EXCEEDED_MAX_VALUE:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            boResponse.getStatusEnum().getDescription(),
            boResponse.getStatusEnum().getDescription(),
            httpServerResponse);
        break;
      case REQUEST_ADDED:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Request Created Successfully", dataObject, httpServerResponse);
        break;
      case TABLE_EMPTY:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "no_data is true", boResponse.getData(), httpServerResponse);
        break;
      case TABLE_VALUES:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Available retrived", boResponse.getData(), httpServerResponse);
        break;
      case FUTURE_DATE_NOT_APPLICABLE:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            boResponse.getStatusEnum().getDescription(),
            boResponse.getStatusEnum().getDescription(),
            httpServerResponse);
        break;
      default:
        dbHandle.commit();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }
}
