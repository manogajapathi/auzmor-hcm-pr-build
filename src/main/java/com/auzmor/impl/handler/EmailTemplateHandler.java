package com.auzmor.impl.handler;

import com.auzmor.impl.bo.EmailTemplateBOImpl;
import com.auzmor.impl.builder.ApiBuilder;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.EmailTemplate;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.Validator;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class EmailTemplateHandler {
  private static Logger logger = LoggerFactory.getLogger(EmailTemplateHandler.class);

  /**
   *
   * @param routingContext
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 04/MAR/2018
   */
  public static void addTemplate(RoutingContext routingContext) {
    logger.info("Routing Add email template");
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"name", "subject", "header", "footer", "template", "organization-id"};
    String[] requiredParams = {"name", "subject", "template", "organization-id"};

    List<Validator> validators = new ArrayList<>();
    Validator emailTemplateValidator = new ValidatorImpl(allowedParams, EmailTemplate.getValidatorMap());
    validators.add(emailTemplateValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
      long id = new EmailTemplateBOImpl(dbHandle).addTemplate(params.get("name"), params.get("subject"),
          params.get("header"), params.get("footer"),  params.get("template"),
          Long.parseLong(params.get("organization-id")));
      if (id > 0) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("template-id", id);
        jsonObject.put("message", "Added successfully");
        EmailTemplateHandler.writeResponse(routingContext.response(), dbHandle, HttpStatus.SC_CREATED,
            "Created successfully", "Template creation successful", jsonObject,
            true);
      } else {
        EmailTemplateHandler.writeResponse(routingContext.response(), dbHandle, HttpStatus.SC_INTERNAL_SERVER_ERROR,
            "Creation failed", "Template creation failed", null, false);
      }
    });
  }

  /**
   *
   * @param routingContext
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 04/MAR/2018
   */
  public static void updateTemplate(RoutingContext routingContext) {
    logger.info("Routing Update email template");
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"name", "subject", "template", "templateId"};
    String[] requiredParams = {"templateId"};

    List<Validator> validators = new ArrayList<>();
    Validator emailTemplateValidator = new ValidatorImpl(allowedParams, EmailTemplate.getValidatorMap());
    validators.add(emailTemplateValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          Boolean updated = new EmailTemplateBOImpl(dbHandle).editTemplate(params.get("name"),
              params.get("subject"), params.get("header"), params.get("footer"),
              params.get("template"), Long.parseLong(params.get("templateId")));
          if (null == updated) {
            EmailTemplateHandler.writeResponse(routingContext.response(), dbHandle, HttpStatus.SC_NOT_FOUND,
                "Unable to update", "Template not found/deleted", null,
                true);
          } else if (updated) {
            EmailTemplateHandler.writeResponse(routingContext.response(), dbHandle, HttpStatus.SC_ACCEPTED,
                "Updated successfully", "Template update successful", null,
                true);
          } else {

            EmailTemplateHandler.writeResponse(routingContext.response(), dbHandle, HttpStatus.SC_ACCEPTED,
                "Nothing to update", "Update failed", null,
                false);
          }
        });
  }

  /**
   *
   * @param routingContext
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 04/MAR/2018
   */
  public static void deleteTemplate(RoutingContext routingContext) {
    logger.info("Routing Delete email template");
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"templateId"};
    String[] requiredParams = {"templateId"};

    List<Validator> validators = new ArrayList<>();
    Validator emailTemplateValidator = new ValidatorImpl(allowedParams, EmailTemplate.getValidatorMap());
    validators.add(emailTemplateValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          Boolean deleted = new EmailTemplateBOImpl(dbHandle).deleteTemplate(Long.parseLong(params.get("templateId")));
          if (null == deleted) {
            EmailTemplateHandler.writeResponse(routingContext.response(), dbHandle, HttpStatus.SC_NOT_FOUND,
                "Unable to delete", "Template not found/deleted", null,
                true);
          } else if (deleted) {
            EmailTemplateHandler.writeResponse(routingContext.response(), dbHandle, HttpStatus.SC_ACCEPTED,
                "Deleted successfully", "Template deletion successful", null,
                true);
          } else {
            EmailTemplateHandler.writeResponse(routingContext.response(), dbHandle, HttpStatus.SC_INTERNAL_SERVER_ERROR,
                "Deleted successfully", "Deletion failed", null,
                false);
          }
        });

  }

  /**
   *
   * @param routingContext
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 08/MAR/2018
   */
  public static void getTemplate(RoutingContext routingContext) {
    logger.info("Routing Get email template with id");
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"templateId"};
    String[] requiredParams = {"templateId"};

    List<Validator> validators = new ArrayList<>();
    Validator emailTemplateValidator = new ValidatorImpl(allowedParams, EmailTemplate.getValidatorMap());
    validators.add(emailTemplateValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          EmailTemplateHandler.writeResponse(routingContext.response(), dbHandle, HttpStatus.SC_OK,
              "Retrieved", "", new EmailTemplateBOImpl(dbHandle).getTemplate(Long
                  .parseLong(params.get("templateId"))), true);
        });
  }

  /**
   *
   * @param routingContext
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 08/MAR/2018
   */
  public static void getTemplates(RoutingContext routingContext) {
    logger.info("Routing Add email template for organization");
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"organizationId"};
    String[] requiredParams = {"organizationId"};

    List<Validator> validators = new ArrayList<>();
    Validator emailTemplateValidator = new ValidatorImpl(allowedParams, EmailTemplate.getValidatorMap());
    validators.add(emailTemplateValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          EmailTemplateHandler.writeResponse(routingContext.response(), HttpStatus.SC_OK,
              new EmailTemplateBOImpl(dbHandle).getTemplates(Long.parseLong(params.get("organizationId"))));
        });
  }

  /**
   *
   * @param routingContext
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 07/MAR/2018
   */
  public static void getTemplateVariable(RoutingContext routingContext) {
    logger.info("Routing Get email template variabless");
    EmailTemplateHandler.writeResponse(routingContext.response(), HttpStatus.SC_OK,
        new JsonArray(new EmailTemplateBOImpl(null).getTemplateVariables()));
  }

  /**
   *
   * @param routingContext
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 07/MAR/2018
   */
  public static void getEmailPreviews(RoutingContext routingContext) {
    logger.info("Routing Get email previews");
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"templateId", "ids"};
    String[] requiredParams = {"templateId", "ids"};

    List<Validator> validators = new ArrayList<>();
    Validator emailTemplateValidator = new ValidatorImpl(allowedParams, EmailTemplate.getValidatorMap());
    validators.add(emailTemplateValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          List<Long> candidateIds = Arrays.stream(params.get("ids").split(","))
              .map(id -> Long.parseLong(id)).collect(Collectors.toList());
          EmailTemplateHandler.writeResponse(routingContext.response(), HttpStatus.SC_OK,
              new EmailTemplateBOImpl(dbHandle).generateEmails(candidateIds,
                  Long.parseLong(params.get("templateId"))));
        });
  }

  /**
   *
   * @param routingContext
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 08/MAR/2018
   */
  public static void sendEmails(RoutingContext routingContext) {
    logger.info("Routing Send email with template");
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"templateId", "ids"};
    String[] requiredParams = {"templateId", "ids"};

    List<Validator> validators = new ArrayList<>();
    Validator emailTemplateValidator = new ValidatorImpl(allowedParams, EmailTemplate.getValidatorMap());
    validators.add(emailTemplateValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          List<Long> candidateIds = Arrays.stream(params.get("ids").split(","))
              .map(id -> Long.parseLong(id)).collect(Collectors.toList());

          new EmailTemplateBOImpl(dbHandle).sendEmails(candidateIds,
              Long.parseLong(params.get("templateId")));
          EmailTemplateHandler.writeResponse(httpServerResponse, dbHandle, HttpStatus.SC_CREATED,
              "Mail has been successfully sent", "Mail Sent", null,
              true);
        });
  }
  /**
   *
   * @param httpServerResponse
   * @param code
   * @param jsonArray
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 04/MAR/2018
   */
  public static void writeResponse(HttpServerResponse httpServerResponse, int code,
                                   JsonArray jsonArray) {
    ResponseHelper.writeSuccessResponse(code, null, jsonArray, httpServerResponse);
  }

  /**
   *
   * @param httpServerResponse
   * @param code
   * @param jsonObject
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 04/MAR/2018
   */
  public static void writeResponse(HttpServerResponse httpServerResponse, int code,
                                   JsonObject jsonObject) {
    ResponseHelper.writeSuccessResponse(code, null, jsonObject, httpServerResponse);
  }

  /**
   *
   * @param httpServerResponse
   * @param dbHandle
   * @param code
   * @param message
   * @param description
   * @param jsonObject
   * @param success
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 04/MAR/2018
   */
  public static void writeResponse(HttpServerResponse httpServerResponse, DbHandle dbHandle,
                                   int code, String message, String description,
                                   JsonObject jsonObject, boolean success)
      throws SQLException, DbHandleException {

    if (success) {
      dbHandle.commit();
      ResponseHelper.writeSuccessResponse(code, message, jsonObject, httpServerResponse);
    } else {
      dbHandle.commit();
      ResponseHelper.writeErrorResponse(code, message, description, httpServerResponse);
    }
  }
}