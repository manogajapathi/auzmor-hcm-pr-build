package com.auzmor.impl.handler.Candidate;

import com.auzmor.bo.CandidateCommentBO;
import com.auzmor.bo.EmployeeBO;
import com.auzmor.bo.LoginBO;
import com.auzmor.bo.OrganizationBO;
import com.auzmor.dao.CandidateDAO;
import com.auzmor.dao.JobDAO;
import com.auzmor.impl.bo.CandidateCommentBOImpl;
import com.auzmor.impl.bo.EmployeeBOImpl;
import com.auzmor.impl.bo.LoginBOImpl;
import com.auzmor.impl.bo.OrganizationBOImpl;
import com.auzmor.impl.dao.CandidateDAOImpl;
import com.auzmor.impl.dao.EmployeeDAOImpl;
import com.auzmor.impl.dao.JobDAOImpl;
import com.auzmor.impl.enumarator.bo.CandidateCommentEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.impl.models.CandidateR.CommentCandidate;
import com.auzmor.impl.util.DateUtil;
import com.auzmor.impl.util.JWTUtil;
import com.auzmor.impl.util.Mail;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.Validator;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;

import static com.auzmor.impl.enumarator.bo.CandidateCommentEnum.*;

public class CandidateCommentsHandlerR {

  private static final Logger logger = LoggerFactory.getLogger(CandidateCommentsHandlerR.class);

  public static void addComments(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"comments", "candidate_id", "admin_name", "employee_id"};
    String[] requiredParams = {"comments", "candidate_id"};
    String[] commentsParams = {"comments", "candidate_id", "admin_name", "employee_id"};

    List<Validator> validators = new ArrayList<>();
    Validator inviteValidator = new ValidatorImpl(commentsParams, CommentCandidate.getValidatorMap());
    validators.add(inviteValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }
    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          CandidateCommentBO candidateCommentBO = new CandidateCommentBOImpl(dbHandle);
          String candidateId = requestParams.get("candidate_id");
          JsonObject successObject = new JsonObject();
          if (!candidateId.equals("0")) {
            CandidateCommentEnum candidateCommentEnum = null;
            if (candidateId.contains(",")) {
              String[] items = candidateId.split(",");
              List<String> container = Arrays.asList(items);

              for (String id : container) {
                CommentCandidate commentCandidate = new CommentCandidate();
                java.util.Date dNow = new java.util.Date();
                String currentDate = new DateUtil().getDate().format(dNow);
                try {
                  commentCandidate.setCommentsDate(new DateUtil().getDate().parse(currentDate));
                } catch (ParseException e) {
                  e.printStackTrace();
                }
                commentCandidate.setCandidateId(Long.parseLong(id));
                commentCandidate.setComments(requestParams.get("comments"));
                commentCandidate.setAdminName(requestParams.get("admin_name"));
                commentCandidate.setUserId(Long.parseLong(requestParams.get("employee_id")));
                commentCandidate.setSystemGenerated(false);

                candidateCommentEnum = candidateCommentBO.
                    addCommentDetails(commentCandidate, false);

                if (candidateCommentEnum == SUCCESS) {
                  JsonObject dataObject = new JsonObject();
                  dataObject.put("message", "Comments added Successfully!");
                  CandidateCommentsHandlerR.writeResponse
                      (httpServerResponse, dbHandle, SUCCESS, dataObject);
                  logger.info("Comments added Successfully!");
                } else {
                  CandidateCommentsHandlerR.writeResponse
                      (httpServerResponse, dbHandle, candidateCommentEnum.FAILED, null);
                  logger.error("Adding Comments Failed!");
                }
              }
            } else {
              CommentCandidate commentCandidate = new CommentCandidate();
              java.util.Date dNow = new java.util.Date();
              String currentDate = new DateUtil().getDate().format(dNow);
              try {
                commentCandidate.setCommentsDate(new DateUtil().getDate().parse(currentDate));
              } catch (ParseException e) {
                e.printStackTrace();
              }
              commentCandidate.setCandidateId(Long.parseLong(requestParams.get("candidate_id")));
              commentCandidate.setComments(requestParams.get("comments"));
              commentCandidate.setAdminName(requestParams.get("admin_name"));
              commentCandidate.setUserId(Long.parseLong(requestParams.get("employee_id")));
              commentCandidate.setSystemGenerated(false);

              candidateCommentEnum = candidateCommentBO.
                  addCommentDetails(commentCandidate, false);
              CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
              CandidateR candidateR = candidateDAO.
                  getCandidate(Long.parseLong(requestParams.get("candidate_id")));
              String adminName = "";
              if (candidateCommentEnum == SUCCESS) {
                CandidateCommentBO commentBO = new CandidateCommentBOImpl(dbHandle);
                String format = ((Organization) routingContext.data().get("organization")).getDate_format();
//              String format ="MM-yy-yyyy";
                List<HashMap<String, String>> commentDetails = commentBO.
                    getCommentList(format, Long.parseLong(requestParams.get("candidate_id")));
                JsonArray commentArray = new JsonArray();

                for (HashMap<String, String> map : commentDetails) {
                  JsonObject dataObject = new JsonObject();
                  for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                    String key = mapEntry.getKey();
                    String value = mapEntry.getValue();
                    EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
                    Employee employee;
                    long userId = 0;
                    if (key.equals("user_id")) {
                      if (Integer.parseInt(value) > 0) {
                        userId = Long.parseLong(value);
                        employee = employeeBO.getEmployeeDetails(userId);

                        if (!StringUtil.printValidString(employee.getProfile_image()).equals("")) {
                          dataObject.put("user_profile_image", employee.getProfile_image());
                        } else {
                          String profileImage = JWTUtil.getProfileImagePath();
                          dataObject.put("user_profile_image", profileImage);
                        }
                        adminName = employee.getFirst_name() + " " + employee.getLast_name();
                        dataObject.put("admin_name", adminName);
                      } else {
                        adminName = candidateR.getFirstName() + " " + candidateR.getLastName();
                        String profileImage = JWTUtil.getProfileImagePath();
                        dataObject.put("user_profile_image", profileImage);
                        dataObject.put("admin_name", adminName);
                      }
                    }
                    dataObject.put(key, value);
                  }
                  commentArray.add(dataObject);
                }
                successObject.put("commentList", commentArray);
                successObject.put("Commentcount", commentArray.size());
                long adminid = 0;
                long user_id = Long.parseLong(requestParams.get("employee_id"));
                Employee employee_user = new EmployeeDAOImpl(dbHandle).getEmployeeDetails(user_id);
                JobDAO jobDAO = new JobDAOImpl(dbHandle);
                Job job = jobDAO.getJobDetails(candidateR.getJobId());
                // adding this line to get the organization name in every mail
                String organizationName = ((Organization) routingContext.data().get("organization")).getOrganization_name();
                organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);
                String title = job.getTitle();
                LoginBO loginBO = new LoginBOImpl(dbHandle);
                long organization_id = ((Organization) routingContext.data().get("organization")).getOrganization_id();
                List<HashMap<String, String>> adminList = loginBO.GetAdminList(organization_id);
                for (HashMap<String, String> map : adminList) {
                  for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                    String key = mapEntry.getKey();
                    String value = mapEntry.getValue();
                    if (key.equals("id")) {
                      adminid = Long.parseLong(value);
                    }
                    if (key.equals("email")) {
                      mapEntry.getValue();
                      String AdminEmail = value;
                      if (adminid != employee_user.getUser_id()) {
                        String emailBody = "<html><body>Hi   Admin,</br>" + adminName + "Added " +
                            "Comments to " + candidateR.getFirstName() + "Candidate  " +
                            "for the job post titled " + title + ". <br/>" +
                            "Thank you,<br/>" + organizationName + " Team <br/>" +
                            "--------------------------------------------------------------\n<br/>" +
                            "This is system generated mail,please do not reply to this mail\n<br/>" +
                            "-------------------------------------------------------------- " +
                            "</body></html>";
                        new Mail().sendMail(Vertx.vertx(),
                            emailBody,
                            AdminEmail, "Comments Added for Candidate-Admin");
                      }
                    }
                  }
                }

                if (StringUtil.printValidString(candidateR.getFirstName()).equals("")) {
                  successObject.put("message", "Comments added Successfully  ");
                  logger.info("Comments added Successfully!");
                } else {
                  successObject.put("message", "Comments added Successfully to " + candidateR.getFirstName());
                  logger.info("Comments added Successfully!");
                }
                CandidateCommentsHandlerR.writeResponse
                    (httpServerResponse, dbHandle, SUCCESS, successObject);
              } else {
                CandidateCommentsHandlerR.writeResponse
                    (httpServerResponse, dbHandle, candidateCommentEnum.FAILED, null);
                logger.error("Adding Comments Failed!");
              }
            }
          }
          CandidateCommentsHandlerR.writeResponse
              (httpServerResponse, dbHandle, SUCCESS, successObject);
        });
  }

  public static void writeResponse(HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   CandidateCommentEnum candidateCommentEnum, JsonObject jsonObject)
      throws SQLException, DbHandleException {

    switch (candidateCommentEnum) {
      case FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_CONFLICT,
            "Comment Creation has been Failed",
            "Comment Creation has been Failed ",
            httpServerResponse);
        break;
      case SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Comment Created Successfully", jsonObject, httpServerResponse);
        break;
      case UPDATE_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "you are not supposed to edit this comment",
            "Comment Update has been Failed ",
            httpServerResponse);
        break;
      case UPDATE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Comment Update Successfully", jsonObject, httpServerResponse);
        break;
      case DELETE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Comment Created Successfully", jsonObject, httpServerResponse);

      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }

  public static void updateCommentsDetails(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"comments", "candidate_id", "admin_name", "id", "employee_id"};
    String[] requiredParams = {"comments", "candidate_id", "id"};
    String[] commentsParams = {"comments", "candidate_id", "admin_name", "id", "employee_id"};

    List<Validator> validators = new ArrayList<>();
    Validator inviteValidator = new ValidatorImpl(commentsParams, CommentCandidate.getValidatorMap());
    validators.add(inviteValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {

          CandidateCommentBO candidateCommentBO = new CandidateCommentBOImpl(dbHandle);

          long candidateId = Long.parseLong(requestParams.get("candidate_id"));
          String adminName = StringUtil.printValidString(requestParams.get("admin_name"));
          long id = Long.parseLong(requestParams.get("id"));
          long userId = Long.parseLong(requestParams.get("employee_id"));

          CommentCandidate commentCandidate = new CommentCandidate();
          commentCandidate.setCandidateId(candidateId);
          commentCandidate.setUserId(userId);
          commentCandidate.setComments(requestParams.get("comments"));
          commentCandidate.setId(id);
          commentCandidate.setAdminName(adminName);

          CandidateCommentEnum candidateCommentEnum = candidateCommentBO.
              updateCommentDetails(commentCandidate);
          CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
          CandidateR candidateR = candidateDAO.getCandidate(candidateId);

          if (candidateCommentEnum == SUCCESS) {
            JsonObject successObject = new JsonObject();
            CandidateCommentBO commentBO = new CandidateCommentBOImpl(dbHandle);

            String format = ((Organization) routingContext.data().get("organization")).getDate_format();
            List<HashMap<String, String>> commentDetails = commentBO.
                getCommentList(format, Long.parseLong(requestParams.get("candidate_id")));

            JsonArray commentArray = new JsonArray();
            for (HashMap<String, String> map : commentDetails) {
              JsonObject dataObject = new JsonObject();
              for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
                Employee employee;
                String admin = "";

                if (key.equals("user_id")) {
                  long userIdCheck = Long.parseLong(value);
                  if (Integer.parseInt(value) > 0) {
                    employee = employeeBO.getEmployeeDetails(userIdCheck);
                    if (!StringUtil.printValidString(employee.getProfile_image()).equals("")) {
                      dataObject.put("user_profile_image", employee.getProfile_image());
                    } else {
                      dataObject.put("user_profile_image", (String) null);
                    }
                    admin = employee.getFirst_name() + " " + employee.getLast_name();
                    dataObject.put("admin_name", admin);
                  } else {
                    dataObject.put("user_profile_image", (String) null);
                    admin = candidateR.getFirstName() + " " + candidateR.getLastName();
                    dataObject.put("admin_name", admin);
                  }
                }
                dataObject.put(key, value);
              }
              commentArray.add(dataObject);
            }
            long adminid = 0;
            long user_id = Long.parseLong(requestParams.get("employee_id"));
            Employee employee_user = new EmployeeDAOImpl(dbHandle).getEmployeeDetails(user_id);
            JobDAO jobDAO = new JobDAOImpl(dbHandle);
            Job job = jobDAO.getJobDetails(candidateR.getJobId());
            String title = job.getTitle();
            // adding this line to get the organization name in every mail
            String organizationName = ((Organization) routingContext.data().get("organization")).getOrganization_name();
            organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);
            LoginBO loginBO = new LoginBOImpl(dbHandle);
            long organization_id = ((Organization) routingContext.data().get("organization")).getOrganization_id();
            List<HashMap<String, String>> adminList = loginBO.GetAdminList(organization_id);
            for (HashMap<String, String> map : adminList) {
              for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if (key.equals("id")) {
                  adminid = Long.parseLong(value);
                }
                if (key.equals("email")) {
                  mapEntry.getValue();
                  String AdminEmail = value;
                  if (adminid != employee_user.getUser_id()) {
                    String emailBody = "<html><body>Hi Admin,</br>" + adminName + "Updated Comments to " + candidateR.getFirstName() + "Candidate  " +
                        "for the job post titled " + title + ". <br/>" +
                        "Thank you,<br/>" + organizationName + " Team <br/>" +
                        "--------------------------------------------------------------\n<br/>" +
                        "This is system generated mail,please do not reply to this mail\n<br/>" +
                        "-------------------------------------------------------------- " +
                        "</body></html>";
                    new Mail().sendMail(Vertx.vertx(),
                        emailBody,
                        AdminEmail, "Comments Updated for Candidate-Admin");
                  }
                }
              }
            }
            successObject.put("commentList", commentArray);
            successObject.put("Commentcount", commentArray.size());

            if (StringUtil.printValidString(candidateR.getFirstName()).equals("")) {
              logger.info("Comments updated Successfully!");
              successObject.put("message", "Comments added Successfully  ");
            } else {
              successObject.put("message", "Comments updated Successfully to " + candidateR.getFirstName());
              logger.info("Comments updated Successfully!");
            }
            CandidateCommentsHandlerR.writeResponse
                (httpServerResponse, dbHandle, UPDATE_SUCCESS, successObject);
          } else if (candidateCommentEnum == UPDATE_NO_CHANGES) {
            JsonObject successObject = new JsonObject();
            successObject.put("message", "No changes are updated!");
            logger.info("No changes made in Comments!");
            CandidateCommentsHandlerR.writeResponse
                (httpServerResponse, dbHandle, UPDATE_SUCCESS, successObject);
          } else {
            CandidateCommentsHandlerR.writeResponse
                (httpServerResponse, dbHandle, candidateCommentEnum.UPDATE_FAILED, null);
            logger.error("Comments update failed!");
          }
        });
  }

  public static void deleteCommentsDetails(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"candidate_id", "id", "creator_id"};
    String[] requiredParams = {"creator_id", "candidate_id", "id"};
    String[] commentsParams = {"candidate_id", "id", "creator_id"};

    List<Validator> validators = new ArrayList<>();
    Validator inviteValidator = new ValidatorImpl(commentsParams, CommentCandidate.getValidatorMap());
    validators.add(inviteValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          CandidateCommentBO candidateCommentBO = new CandidateCommentBOImpl(dbHandle);

          long candidateId = Long.parseLong(requestParams.get("candidate_id"));
          long id = Long.parseLong(requestParams.get("id"));
          long userId = Long.parseLong(requestParams.get("creator_id"));

          CandidateCommentEnum candidateCommentEnum = candidateCommentBO.
              deleteCommentDetails(candidateId, userId, id);
          CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
          CandidateR candidateR = candidateDAO.
              getCandidate(candidateId);

          if (candidateCommentEnum == SUCCESS) {
            JsonObject successObject = new JsonObject();
            long adminid = 0;
            long user_id = Long.parseLong(requestParams.get("creator_id"));
            Employee employee_user = new EmployeeDAOImpl(dbHandle).getEmployeeDetails(user_id);
            JobDAO jobDAO = new JobDAOImpl(dbHandle);
            Job job = jobDAO.getJobDetails(candidateR.getJobId());
            String title = job.getTitle();
            LoginBO loginBO = new LoginBOImpl(dbHandle);
            // adding this line to get the organization name in every mail
            String organizationName = ((Organization) routingContext.data().get("organization")).getOrganization_name();
            organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);
            long organization_id = ((Organization) routingContext.data().get("organization")).getOrganization_id();
            List<HashMap<String, String>> adminList = loginBO.GetAdminList(organization_id);
            for (HashMap<String, String> map : adminList) {
              for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if (key.equals("id")) {
                  adminid = Long.parseLong(value);
                }
                if (key.equals("email")) {
                  mapEntry.getValue();
                  String AdminEmail = value;
                  if (adminid != employee_user.getUser_id()) {
                    String emailBody = "<html><body>Hi   Admin,</br>" + "Comments Deleted to " + candidateR.getFirstName() + "Candidate  " +
                        "for the job post titled " + title + ". <br/>" +
                        "Thank you,<br/>" + organizationName + " Team <br/>" +
                        "--------------------------------------------------------------\n<br/>" +
                        "This is system generated mail,please do not reply to this mail\n<br/>" +
                        "-------------------------------------------------------------- " +
                        "</body></html>";
                    new Mail().sendMail(Vertx.vertx(),
                        emailBody,
                        AdminEmail, "Comments Deleted for Candidate-Admin");
                  }
                }
              }
            }

           /* CandidateCommentBO commentBO = new CandidateCommentBOImpl(dbHandle);
            String format = "MM/dd/yyyy";
           // String format = ((Organization) routingContext.data().get("organization")).getDate_format();
            List<HashMap<String, String>> commentDetails = commentBO.
                getCommentList(format, Long.parseLong(requestParams.get("candidate_id")));

            JsonArray commentArray = new JsonArray();
            for (HashMap<String, String> map : commentDetails) {
              JsonObject dataObject = new JsonObject();
              for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
                Employee employee;
                String admin = "";

                if (key.equals("user_id")) {
                  long userIdCheck = Long.parseLong(value);
                  if (Integer.parseInt(value) > 0) {
                    employee = employeeBO.getEmployeeDetails(userIdCheck);
                    if (!StringUtil.printValidString(employee.getProfile_image()).equals("")) {
                      dataObject.put("user_profile_image", employee.getProfile_image());
                    } else {
                      dataObject.put("user_profile_image", (String) null);
                    }
                    admin = employee.getFirst_name() + " " + employee.getLast_name();
                    dataObject.put("admin_name", admin);
                  } else {
                    dataObject.put("user_profile_image", (String) null);
                    admin = candidateR.getFirstName() + " " + candidateR.getLastName();
                    dataObject.put("admin_name", admin);
                  }
                }
                dataObject.put(key, value);
              }
              commentArray.add(dataObject);
            }*/
            successObject.put("message", "Comment deleted Successfully!");
            //  successObject.put("Commentcount", commentArray.size());

            CandidateCommentsHandlerR.writeResponse
                (httpServerResponse, dbHandle, DELETE_SUCCESS, successObject);
            logger.info("Comment deleted Successfully!");
          } else {
            CandidateCommentsHandlerR.writeResponse
                (httpServerResponse, dbHandle, candidateCommentEnum, null);
            logger.error("Deleting Comments Failed!");
          }
        });
  }
}

