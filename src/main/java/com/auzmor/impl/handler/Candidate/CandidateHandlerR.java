package com.auzmor.impl.handler.Candidate;

import com.auzmor.bo.*;
import com.auzmor.dao.CandidateDAO;
import com.auzmor.impl.bo.*;
import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.constant.file.endpoint.EndpointConstant;
import com.auzmor.impl.dao.CandidateDAOImpl;
import com.auzmor.impl.dao.OrganizationDAOImpl;
import com.auzmor.impl.enumarator.UserTypeEnum;
import com.auzmor.impl.enumarator.bo.CandidateStatusEnum;
import com.auzmor.impl.enumarator.file.FileTypeEnum;
import com.auzmor.impl.enumarator.kong.KongHttpStatusCode;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.handler.CountryStateHandler;
import com.auzmor.impl.helper.*;
import com.auzmor.impl.model.Job.Collaborator;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.model.Login;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.Role;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.impl.models.CandidateR.HiringProcessCandidate;
import com.auzmor.impl.models.CandidateR.InterviewProcessCandidate;
import com.auzmor.impl.models.CandidateR.OfferProcessCandidate;
import com.auzmor.impl.util.DateUtil;
import com.auzmor.impl.util.JWTUtil;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.FieldOptions;
import com.auzmor.util.validator.Validator;
import com.google.i18n.phonenumbers.NumberParseException;
import com.mysql.cj.core.util.StringUtils;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;

import static com.auzmor.impl.enumarator.bo.CandidateStatusEnum.*;

public class CandidateHandlerR {

  private static final Logger logger = LoggerFactory.getLogger(CandidateHandlerR.class);

  // inserting candidate details
  public static void addCandidate(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse httpServerResponse = routingContext.response();
    MultiMap params = request.params();
    Map<String, String> customQuestionMap = new HashMap<>();

    String[] allowedParams = {"first_name", "last_name", "email", "phone_number", "region_code",
        "unit", "source", "address_line", "city", "state", "zip", "country", "gender",
        "hiring_lead", "internal_jobcode", "job_id",
        "user_id", "resume", "resume_name", "cover_letter",
        "highest_education", "desired_salary", "linkedin_url", "date_available", "blog",
        "reference", "referred_by", "college", "company_type_check", "veteran_status",
        "ethnicity", "disability", "organization_id", "custom_question", "custom_answer", "creator_id"};

    String[] requiredParams = {"first_name", "last_name", "email", "phone_number", "region_code",
        "organization_id"};

    String[] candidateParams = {"first_name", "last_name", "email", "phone_number", "region_code",
        "unit", "source", "address_line", "city", "state", "zip", "country", "gender",
        "hiring_lead", "internal_jobcode", "job_id",
        "user_id", "resume", "resume_name", "cover_letter",
        "highest_education", "desired_salary", "linkedin_url", "date_available", "blog",
        "reference", "referred_by", "college", "company_type_check", "veteran_status",
        "ethnicity", "disability", "organization_id", "custom_question", "custom_answer", "creator_id"};

    List<Validator> validators = new ArrayList<>();
    Validator candidateValidator = new ValidatorImpl(candidateParams, CandidateR.getValidatorMap());
    validators.add(candidateValidator);

    MultiMap requestParams = request.params();
    if (!ParamHelper.isParamsValid(httpServerResponse, params, allowedParams, requiredParams, validators)) {
      return;
    }
    CandidateR candidateModel = new CandidateR();
    candidateModel.setFirstName(requestParams.get("first_name"));
    candidateModel.setLastName(requestParams.get("last_name"));
    candidateModel.setEmail(requestParams.get("email"));
    candidateModel.setPhoneNumber(requestParams.get("phone_number"));
    candidateModel.setOrganizationId(Long.parseLong(requestParams.get("organization_id")));

    if (StringUtil.printValidString(requestParams.get("job_id")).equals("")) {
      candidateModel.setJobId(Long.parseLong("0"));
    } else {
      candidateModel.setJobId(Long.parseLong(requestParams.get("job_id")));
    }

    if (StringUtil.printValidString(requestParams.get("creator_id")).equals("")) {
      candidateModel.setAdminId(Long.parseLong("0"));
    } else {
      candidateModel.setAdminId(Long.parseLong(requestParams.get("creator_id")));
      candidateModel.setUserId((long) 0);
    }

    if (StringUtil.printValidString(requestParams.get("unit")).equals("")) {
      candidateModel.setUnit("0");
    } else {
      candidateModel.setUnit(requestParams.get("unit"));
    }
    if (StringUtil.printValidString(requestParams.get("source")).equals("")) {
      candidateModel.setSource("Direct");
    } else {
      candidateModel.setSource(requestParams.get("source"));
    }
    if (!StringUtil.printValidString(requestParams.get("address_line")).equals("")) {
      candidateModel.setAddressLine(requestParams.get("address_line"));
    }/* else {
      candidateModel.setAddressLine("-");
    }*/
    if (!StringUtil.printValidString(requestParams.get("city")).equals("")) {
      candidateModel.setCity(requestParams.get("city"));
    } /*else {
      candidateModel.setCity("-");
    }*/
    if (!StringUtil.printValidString(requestParams.get("state")).equals("")) {
      candidateModel.setState(requestParams.get("state"));
    }
    if (!StringUtil.printValidString(requestParams.get("zip")).equals("")) {
      candidateModel.setZip(requestParams.get("zip"));
    }/* else {
      candidateModel.setZip("-");
    }*/
    if (!StringUtil.printValidString(requestParams.get("country")).equals("")) {
      candidateModel.setCountry(requestParams.get("country"));
    }/* else {
      candidateModel.setCountry("-");
    }*/
    if (!StringUtil.printValidString(requestParams.get("gender")).equals("")) {
      candidateModel.setGender(requestParams.get("gender"));
    } /*else {
      candidateModel.setGender("");
    }*/
    if (!StringUtil.printValidString(requestParams.get("hiring_lead")).equals("")) {
      candidateModel.setHiringLeadId(Long.parseLong(requestParams.get("hiring_lead")));
    } else {
      candidateModel.setHiringLeadId(Long.parseLong("0"));
    }
    if (!StringUtil.printValidString(requestParams.get("internal_jobcode")).equals("")) {
      candidateModel.setInternalJobCode(requestParams.get("internal_jobcode"));
    }/* else {
      candidateModel.setInternalJobCode("");
    }*/
    /*if (!StringUtil.printValidString(requestParams.get("resume")).equals("")) {
      String  resumePath = requestParams.get("resume").substring(0,requestParams.get("resume").indexOf("_"));
      String  resumeName = requestParams.get("resume").substring(requestParams.get("resume").indexOf("_")+1);
      candidateModel.setResume(resumePath);
      candidateModel.setResumeName(resumeName);
    } else {
      candidateModel.setResume("");
    }*/
    if (!StringUtil.printValidString(requestParams.get("resume")).equals("")) {
      candidateModel.setResume(requestParams.get("resume"));
    }/* else {
      candidateModel.setResume("");
    }*/
    if (!StringUtil.printValidString(requestParams.get("resume_name")).equals("")) {
      candidateModel.setResumeName(requestParams.get("resume_name"));
    }/* else {
      candidateModel.setResumeName("");
    }*/
    if (!StringUtil.printValidString(requestParams.get("cover_letter")).equals("")) {
      candidateModel.setCoverLetter(requestParams.get("cover_letter"));
    } /*else {
      candidateModel.setCoverLetter("");
    }*/
    if (!StringUtil.printValidString(requestParams.get("highest_education")).equals("")) {
      candidateModel.setHighestEducation(requestParams.get("highest_education"));
    }/* else {
      candidateModel.setHighestEducation("");
    }*/
    if (!StringUtil.printValidString(requestParams.get("desired_salary")).equals("")) {
      candidateModel.setDesiredSalary(requestParams.get("desired_salary"));
    }/* else {
      candidateModel.setDesiredSalary("");
    }*/
    if (!StringUtil.printValidString(requestParams.get("linkedin_url")).equals("")) {
      candidateModel.setLinkedinUrl(requestParams.get("linkedin_url"));
    }/* else {
      candidateModel.setLinkedinUrl("");
    }*/
    if (!StringUtil.printValidString(requestParams.get("date_available")).equals("")) {
      candidateModel.setDateAvailable(requestParams.get("date_available"));
    } /*else {
      candidateModel.setDateAvailable("");
    }*/
    if (!StringUtil.printValidString(requestParams.get("blog")).equals("")) {
      candidateModel.setBlog(requestParams.get("blog"));
    } /*else {
      candidateModel.setBlog("");
    }*/
    if (!StringUtil.printValidString(requestParams.get("reference")).equals("")) {
      candidateModel.setReference(requestParams.get("reference"));
    }/* else {
      candidateModel.setReference("");
    }*/
    if (!StringUtil.printValidString(requestParams.get("referred_by")).equals("")) {
      candidateModel.setReferredBy(requestParams.get("referred_by"));
    } /*else {
      candidateModel.setReferredBy("");
    }*/
    if (!StringUtil.printValidString(requestParams.get("college")).equals("")) {
      candidateModel.setCollege(requestParams.get("college"));
    } /*else {
      candidateModel.setCollege("");
    }*/
    if (!StringUtil.printValidString(requestParams.get("company_type_check")).equals("")) {
      candidateModel.setCompanyTypeCheck(requestParams.get("company_type_check"));
    }/* else {
      candidateModel.setCompanyTypeCheck("");
    }*/
    if (!StringUtil.printValidString(requestParams.get("veteran_status")).equals("")) {
      candidateModel.setVeteranStatus(requestParams.get("veteran_status"));
    } /*else {
      candidateModel.setVeteranStatus("");
    }*/
    if (!StringUtil.printValidString(requestParams.get("ethnicity")).equals("")) {
      candidateModel.setEthnicity(requestParams.get("ethnicity"));
    }/* else {
      candidateModel.setEthnicity("");
    }*/

    if (!StringUtil.printValidString(requestParams.get("disability")).equals("")) {
      candidateModel.setDisability(requestParams.get("disability"));
    } /*else {
      candidateModel.setDisability("");
    }*/

    if (!StringUtil.printValidString(requestParams.get("user_id")).equals("")) {
      candidateModel.setUserId(Long.parseLong(requestParams.get("user_id")));
    } else {
      candidateModel.setUserId((long) 0);
    }

    if (!StringUtil.printValidString(requestParams.get("custom_question")).equals("")) {
      customQuestionMap.put("custom_question", requestParams.get("custom_question"));
    }

    if (!StringUtil.printValidString(requestParams.get("custom_answer")).equals("")) {
      customQuestionMap.put("custom_answer", requestParams.get("custom_answer"));
    }

    DbHandle dbHandle = DbHelper.getDBHandle(routingContext, false);
    if (null == dbHandle) {
      return;
    }
    CandidateBO candidateBO = new CandidateBOImpl(dbHandle);
    CandidateStatusEnum candidateStatusEnum = null;
    JsonObject dataObject = new JsonObject();
    boolean exception = false;
    try {
      boolean isValidPhone = StringUtil.isValidPhoneNumber(candidateModel.getPhoneNumber(),
          params.get("region_code"));
      if (!isValidPhone) {

        ResponseHelper.writeErrorResponse(400, "Please provide proper phone number",
            "Please provide proper phone number", httpServerResponse);
        return;
      }
      if (customQuestionMap.size() > 0) {
        candidateModel.setCustomQuestionModels(candidateBO.getCustomQuestions(customQuestionMap));
      }
      candidateStatusEnum = candidateBO.createCandidate(routingContext, candidateModel);

      if (candidateStatusEnum == SUCCESS) {
        long loginId = candidateBO.getCandidatewithLoginId(candidateModel.getOrganizationId());
        long loginID = loginId;
        HttpClient kongClient = KongServiceHelper.executeWithKongClient(routingContext.vertx());
        KongServiceHelper.addConsumer(kongClient, loginID, clientResponse -> {
          if (clientResponse.statusCode() == KongHttpStatusCode.ADD.getStatusCode()) {
            clientResponse.bodyHandler(buffer -> {
              String consumerId = new JsonObject(buffer).getString("id");
              Role role = null;
              try {
                new LoginBOImpl(dbHandle).updateConsumerId(consumerId, loginID);
              } catch (SQLException e) {
                kongClient.close();
                DbHelper.safeRollback(dbHandle);
                dbHandle.checkAndCloseConnection();
                ResponseHelper.writeInternalServerError(httpServerResponse);
              } catch (DbHandleException e) {
                kongClient.close();
                DbHelper.safeRollback(dbHandle);
                dbHandle.checkAndCloseConnection();
                ResponseHelper.writeInternalServerError(httpServerResponse);
              }
              try {
                role = new RoleBOImpl(dbHandle).getByName("Candidate", candidateModel.getOrganizationId());
              } catch (SQLException e) {
                kongClient.close();
                DbHelper.safeRollback(dbHandle);
                dbHandle.checkAndCloseConnection();
                ResponseHelper.writeInternalServerError(httpServerResponse);
              } catch (DbHandleException e) {
                kongClient.close();
                DbHelper.safeRollback(dbHandle);
                dbHandle.checkAndCloseConnection();
                ResponseHelper.writeInternalServerError(httpServerResponse);
              }
              KongServiceHelper.addConsumerRoles(kongClient, consumerId, "" + role.getId(), httpClientResponse -> {

                dataObject.put("message", "Thanks for applying,Login Information " +
                    "has been sent successfully to your e-mail!");
                logger.info("Candidate added Successfully and Mail Sent!");
                try {
                  CandidateHandlerR.writeResponse
                      (httpServerResponse, dbHandle, SUCCESS, dataObject);
                } catch (SQLException e) {
                  logger.info(e.getStackTrace().toString());
                  ResponseHelper.writeInternalServerError(httpServerResponse);
                } catch (DbHandleException e) {
                  logger.info(e.getStackTrace().toString());
                  ResponseHelper.writeInternalServerError(httpServerResponse);
                }
                dbHandle.checkAndCloseConnection();
                kongClient.close();
              });
            });
          } else {
            logger.info("Candidate addition Failed!");
            kongClient.close();
            DbHelper.safeRollback(dbHandle);
            dbHandle.checkAndCloseConnection();
          }
        });

      } else if (candidateStatusEnum == LOGIN_STATUS) {
        long loginId = candidateBO.getCandidatewithLoginId(candidateModel.getOrganizationId());
        long loginID = loginId;
        HttpClient kongClient = KongServiceHelper.executeWithKongClient(routingContext.vertx());
        KongServiceHelper.addConsumer(kongClient, loginID, clientResponse -> {
          if (clientResponse.statusCode() == KongHttpStatusCode.ADD.getStatusCode()) {
            clientResponse.bodyHandler(buffer -> {
              String consumerId = new JsonObject(buffer).getString("id");
              Role role = null;
              try {
                new LoginBOImpl(dbHandle).updateConsumerId(consumerId, loginID);
              } catch (SQLException e) {
                logger.error(e.getStackTrace().toString());
                kongClient.close();
                DbHelper.safeRollback(dbHandle);
                dbHandle.checkAndCloseConnection();
                ResponseHelper.writeInternalServerError(httpServerResponse);
              } catch (DbHandleException e) {
                logger.error(e.getStackTrace().toString());
                kongClient.close();
                DbHelper.safeRollback(dbHandle);
                dbHandle.checkAndCloseConnection();
                ResponseHelper.writeInternalServerError(httpServerResponse);
              }
              try {
                role = new RoleBOImpl(dbHandle).getByName("Candidate", candidateModel.getOrganizationId());
              } catch (SQLException e) {
                logger.error(e.getStackTrace().toString());
                kongClient.close();
                DbHelper.safeRollback(dbHandle);
                dbHandle.checkAndCloseConnection();
                ResponseHelper.writeInternalServerError(httpServerResponse);
              } catch (DbHandleException e) {
                logger.error(e.getStackTrace().toString());
                kongClient.close();
                DbHelper.safeRollback(dbHandle);
                dbHandle.checkAndCloseConnection();
                ResponseHelper.writeInternalServerError(httpServerResponse);
              }
              KongServiceHelper.addConsumerRoles(kongClient, consumerId, "" + role.getId(), httpClientResponse -> {
                logger.info("Candidate Successfully Moved to Talent Pool!");
                dataObject.put("message", "Candidate Successfully Moved to Talent Pool!!");
                try {
                  CandidateHandlerR.writeResponse
                      (httpServerResponse, dbHandle, SUCCESS_MESSAGE_STATUS, dataObject);
                } catch (SQLException e) {
                  logger.error(e.getStackTrace().toString());
                  ResponseHelper.writeInternalServerError(httpServerResponse);
                } catch (DbHandleException e) {
                  logger.error(e.getStackTrace().toString());
                  ResponseHelper.writeInternalServerError(httpServerResponse);
                }
                dbHandle.checkAndCloseConnection();
                kongClient.close();
              });
            });
          } else {
            kongClient.close();
            DbHelper.safeRollback(dbHandle);
            dbHandle.checkAndCloseConnection();
          }
        });

      } else if (candidateStatusEnum == LOGIN_CHECK) {
        long loginId = candidateBO.getCandidatewithLoginId(candidateModel.getOrganizationId());
        long loginID = loginId;
        HttpClient kongClient = KongServiceHelper.executeWithKongClient(routingContext.vertx());
        KongServiceHelper.addConsumer(kongClient, loginID, clientResponse -> {
          if (clientResponse.statusCode() == KongHttpStatusCode.ADD.getStatusCode()) {
            clientResponse.bodyHandler(buffer -> {
              String consumerId = new JsonObject(buffer).getString("id");
              Role role = null;
              try {
                new LoginBOImpl(dbHandle).updateConsumerId(consumerId, loginID);
              } catch (SQLException e) {
                kongClient.close();
                DbHelper.safeRollback(dbHandle);
                dbHandle.checkAndCloseConnection();
                logger.error(e.getStackTrace().toString());
                ResponseHelper.writeInternalServerError(httpServerResponse);
              } catch (DbHandleException e) {
                kongClient.close();
                DbHelper.safeRollback(dbHandle);
                dbHandle.checkAndCloseConnection();
                logger.error(e.getStackTrace().toString());
                ResponseHelper.writeInternalServerError(httpServerResponse);
              }
              try {
                role = new RoleBOImpl(dbHandle).getByName("Candidate", candidateModel.getOrganizationId());
              } catch (SQLException e) {
                kongClient.close();
                DbHelper.safeRollback(dbHandle);
                dbHandle.checkAndCloseConnection();
                logger.error(e.getStackTrace().toString());
                ResponseHelper.writeInternalServerError(httpServerResponse);
              } catch (DbHandleException e) {
                kongClient.close();
                DbHelper.safeRollback(dbHandle);
                dbHandle.checkAndCloseConnection();
                logger.error(e.getStackTrace().toString());
                ResponseHelper.writeInternalServerError(httpServerResponse);
              }
              KongServiceHelper.addConsumerRoles(kongClient, consumerId, "" + role.getId(), httpClientResponse -> {
                logger.info("Candidate Applied Successfully!");
                dataObject.put("message", "Candidate Applied Successfully!!");
                try {
                  CandidateHandlerR.writeResponse
                      (httpServerResponse, dbHandle, SUCCESS_MESSAGE, dataObject);
                } catch (SQLException e) {
                  logger.error(e.getStackTrace().toString());
                  ResponseHelper.writeInternalServerError(httpServerResponse);
                } catch (DbHandleException e) {
                  logger.error(e.getStackTrace().toString());
                  ResponseHelper.writeInternalServerError(httpServerResponse);
                }
                dbHandle.checkAndCloseConnection();
                kongClient.close();
              });
            });
          } else {
            kongClient.close();
            DbHelper.safeRollback(dbHandle);
            dbHandle.checkAndCloseConnection();
          }
        });

      } else if (candidateStatusEnum == SUCCESS_MESSAGE) {
        dataObject.put("message", "Candidate Applied Successfully!");
        CandidateHandlerR.writeResponse
            (httpServerResponse, dbHandle, SUCCESS_MESSAGE, dataObject);
      } else if (candidateStatusEnum == SUCCESS_MESSAGE_STATUS) {
        dataObject.put("message", "Candidate Successfully Moved to Talent Pool!");
        CandidateHandlerR.writeResponse
            (httpServerResponse, dbHandle, SUCCESS_MESSAGE_STATUS, dataObject);
      } else if (candidateStatusEnum == SUCCESS_MESSAGE_STATUS_EXISTS) {
        dataObject.put("message", "Candidate Already Moved to Talent Pool!");
        CandidateHandlerR.writeResponse
            (httpServerResponse, dbHandle, SUCCESS_MESSAGE_STATUS_EXISTS, dataObject);
      } else if (candidateStatusEnum == EXISTS_ALREADY) {
        dataObject.put("message", "Job Already Applied,please login to view your applied jobs!");
        CandidateHandlerR.writeResponse
            (httpServerResponse, dbHandle, EXISTS_ALREADY, dataObject);
      } else if (candidateStatusEnum == EXISTS_JOB_ALREADY) {
        dataObject.put("message", "Job Already Applied!");
        CandidateHandlerR.writeResponse
            (httpServerResponse, dbHandle, EXISTS_JOB_ALREADY, dataObject);
      } else if (candidateStatusEnum == APPLIED_JOB) {
        dataObject.put("message", "Thanks for applying,our team will contact you soon!");
        CandidateHandlerR.writeResponse
            (httpServerResponse, dbHandle, APPLIED_JOB, dataObject);

      } else if (candidateStatusEnum == FAILED) {
        CandidateHandlerR.writeResponse
            (httpServerResponse, dbHandle, candidateStatusEnum.FAILED, null);

      } else if (candidateStatusEnum == SEND_FAILED) {
        CandidateHandlerR.writeResponse
            (httpServerResponse, dbHandle, candidateStatusEnum.SEND_FAILED, null);
      } else if (candidateStatusEnum == NOT_ALLOWED_EMAIL) {
        CandidateHandlerR.writeResponse
            (httpServerResponse, dbHandle, candidateStatusEnum.NOT_ALLOWED_EMAIL, null);
      }
    } catch (SQLException e) {
      exception = true;
      logger.error("Sql Exception while creating candidate");
      ResponseHelper.writeInternalServerError(httpServerResponse);
    } catch (DbHandleException e) {
      exception = true;
      logger.error("DbHandle Exception while creating candidate");
      ResponseHelper.writeInternalServerError(httpServerResponse);
    } catch (NumberParseException e) {
      exception = true;
      logger.error(e.getStackTrace().toString());
      ResponseHelper.writeInternalServerError(httpServerResponse);
    } finally {
       if (exception) {
        dbHandle.checkAndCloseConnection();
       }
      logger.info("Candidate Fetch Data");
    }
  }

  public static void writeResponse(HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   CandidateStatusEnum candidateStatusEnum, JsonObject jsonObject)
      throws SQLException, DbHandleException {

    switch (candidateStatusEnum) {
      case EXISTS_ALREADY:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_CONFLICT,
            "Job already applied for this candidate,please login to view applied jobs",
            "Cannot apply for this job, already exists in your organization",
            httpServerResponse);
        break;
      case SEND_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_CONFLICT,
            "Sending Email to the user e-mailId is failed! ",
            "Sending Email to the user e-mailId is failed!",
            httpServerResponse);
        break;
      case FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_CONFLICT,
            "Candidate cannot be created",
            "Candidate cannot be created",
            httpServerResponse);
        break;
      case EXISTS_JOB_ALREADY:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Job Already Applied!",
            "Job Already Applied!",
            httpServerResponse);
        break;
      case APPLIED_JOB:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Candidate created successfully", jsonObject, httpServerResponse);
        break;
      case SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Candidate created successfully,Login Information send to mail",
            jsonObject, httpServerResponse);
        break;
      case SUCCESS_MESSAGE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Candidate created successfully",
            jsonObject, httpServerResponse);
        break;
      case SUCCESS_MESSAGE_STATUS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Candidate moved successfully",
            jsonObject, httpServerResponse);
        break;
      case FORWARD_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Candidate Forward to Employee Successfully",
            jsonObject, httpServerResponse);
        break;
      case MOVE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Candidate Moved Successfully",
            jsonObject, httpServerResponse);
        break;
      case UPDATE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Candidate Updated Successfully",
            jsonObject, httpServerResponse);
        break;
      case STATUS_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Candidate Status Update Successfully",
            jsonObject, httpServerResponse);
        break;
      case FORWARD_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Candidate Forward to Employee Failed ",
            "Candidate Forward to Employee Failed,Please try again",
            httpServerResponse);
        break;
      case UPDATE_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_CONFLICT,
            "Candidate Update Failed ",
            "Candidate Update Failed,Please try again",
            httpServerResponse);
        break;
      case MOVE_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Candidate Moved Failed ",
            "Candidate Moved Failed,Please try again!",
            httpServerResponse);
      case STATUS_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_NOT_ACCEPTABLE,
            "Candidate Status Update Failed ",
            "Candidate Status Update Failed,Please try again!",
            httpServerResponse);
        break;
      case GET_CANDIDATE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Getting Candidate Details",
            jsonObject, false, httpServerResponse);
        break;
      case NO_DATA_STATUS:
        dbHandle.commit();
        ResponseHelper.writeSuccessNoDataResponse(HttpStatus.SC_OK,
            jsonObject, httpServerResponse);
        break;
      case GET_CANDIDATE_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Getting Candidate Details Failed ",
            "Getting Candidate Details Failed,Please try again",
            httpServerResponse);
        break;
      case CANDIDATE_DELETED_SUCESSFULLY:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Candidate Deleted Sucessfully",
            jsonObject, httpServerResponse);
        break;
      case CANDIDATE_DELETED_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Candidate Deleted Failed ",
            "Candidate Deleted Failed,Please try again",
            httpServerResponse);
        break;
      case SUCCESS_MESSAGE_STATUS_EXISTS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Candidate Already moved to Talent Pool",
            jsonObject, httpServerResponse);
        break;
      case NOT_ALLOWED_EMAIL:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_NOT_ACCEPTABLE,
            "You are not supposed to apply for this Job!",
            "You are not supposed to apply for this Job!",
            httpServerResponse);
        break;

      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }


  public static void writeArrayResponse(HttpServerResponse httpServerResponse,
                                        DbHandle dbHandle,
                                        CandidateStatusEnum candidateStatusEnum, JsonArray jsonObject)
      throws SQLException, DbHandleException {

    switch (candidateStatusEnum) {
      case FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Not Getting candidate details,Please try again !",
            "Not Getting candidate details,Please try again !",
            httpServerResponse);
        break;
      case SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "", jsonObject, httpServerResponse);
        break;
      case GET_CANDIDATE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Getting Candidate Details",
            jsonObject, httpServerResponse);
        break;
      case GET_CANDIDATE_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Getting Candidate Details Failed ",
            "Getting Candidate Details Failed,Please try again",
            httpServerResponse);
        break;
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }

  public static void getCandidate(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"first_name", "last_name", "email", "phone_number",
        "unit", "source", "address_line", "city", "state", "zip", "country", "gender",
        "hiring_lead", "internal_jobcode", "employment_type", "location", "job_title", "job_id",
        "user_id", "department", "minimum_experience", "job_status", "resume", "cover_letter",
        "highest_education", "desired_salary", "linkedin_url", "date_available", "blog",
        "reference", "referred_by", "college", "company_type_check", "veteran_status",
        "ethnicity", "disability", "organization_id", "custom_question", "custom_answer",
        "fields", "limit", "offset", "EmailList", "InterviewDetails", "OfferDetails",
        "CommentsList", "cand_status", "id"};

    String[] requiredParams = {};

    String[] candidateParams = {"first_name", "last_name", "email", "phone_number",
        "unit", "source", "address_line", "city", "state", "zip", "country", "gender",
        "hiring_lead", "internal_jobcode", "employment_type", "location", "job_title", "job_id",
        "user_id", "department", "minimum_experience", "job_status", "resume", "cover_letter",
        "highest_education", "desired_salary", "linkedin_url", "date_available", "blog",
        "reference", "referred_by", "college", "company_type_check", "veteran_status",
        "ethnicity", "disability", "organization_id", "custom_question", "custom_answer",
        "fields", "limit", "offset", "EmailList", "InterviewDetails", "OfferDetails", "CommentsList",
        "cand_status", "id"};

    List<Validator> validators = new ArrayList<>();
    Validator candidateValidator = new ValidatorImpl(candidateParams, CandidateR.getValidatorMap());
    validators.add(candidateValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    if ("0".equals(StringUtil.printValidString(params.get("job_id"))) &&
        StringUtil.printValidString(params.get("organization_id")).length() > 0) {
      DbHandle dbHandle = DbHelper.getDBHandle(routingContext, false);
      int jobCount = Integer.parseInt(new DatabaseUtil().
          getTablename("Job", "count(1)",
              "organization_id =" + params.get("organization_id")));
      Organization organization = null;
      try {
        organization = new OrganizationDAOImpl(dbHandle)
            .getOrganizationById(Long.parseLong(params.get("organization_id")));
      } catch (SQLException | DbHandleException e) {
        if (null != dbHandle) {
          dbHandle.checkAndCloseConnection();
        }
        ResponseHelper.writeInternalServerError(routingContext.response());
      }
      boolean isOrganizationAvailable = (organization != null);
      if (isOrganizationAvailable && jobCount == 0) {
        JsonObject listObject = new JsonObject();
        JsonArray listArray = new JsonArray();
        JsonObject dumpData = new JsonObject();
        dumpData.put("first_name", "John");
        dumpData.put("last_name", "Smith");
        dumpData.put("phone_number", "98954522556");
        dumpData.put("email", "xxx@gmail.com");
        dumpData.put("applied_date", "30-Jan-2018");
        dumpData.put("modified_date", "30-Jan-2018");
        dumpData.put("job_title", "Software Engineer");
        dumpData.put("rating", "0");
        dumpData.put("reject_reason", "-");
        dumpData.put("candidate_id", "0");
        dumpData.put("desired_salary", "1000");
        dumpData.put("college", "Stanford University");
        dumpData.put("country", "United States");
        dumpData.put("employment_type", "");
        dumpData.put("gender", "");
        dumpData.put("ethnicity", "");
        dumpData.put("city", "");
        dumpData.put("disability", "");
        dumpData.put("highest_education", "");
        dumpData.put("source", "Direct");
        dumpData.put("blog", "");
        dumpData.put("hiring_lead", "0");
        dumpData.put("reference", "");
        dumpData.put("cover_letter", "");
        dumpData.put("company_type_check", "");
        dumpData.put("minimum_experience", "");
        dumpData.put("state", "-");
        dumpData.put("department", "");
        dumpData.put("email", "thiru.aztest@gmail.com");
        dumpData.put("zip", "-");
        dumpData.put("resume", "");
        dumpData.put("address_line", "-");
        dumpData.put("internal_jobcode", "");
        dumpData.put("modified_date", "04/23/2018");
        dumpData.put("applied_date", "04/23/2018");
        dumpData.put("job_status", "Published");
        dumpData.put("unit", "0");
        dumpData.put("user_id", "1");
        dumpData.put("veteran_status", "");
        dumpData.put("organization_id", params.get("organization_id"));
        dumpData.put("location", "New Delhi");
        dumpData.put("linkedin_url", "");
        dumpData.put("date_available", "");
        dumpData.put("referred_by", "");
        dumpData.put("status", "applicant");
        listArray.add(dumpData);
        listObject.put("totalcount", 1);
        listObject.put("JobIdList", listArray);

        JsonObject jsonObject = new JsonObject();
        jsonObject.put("code", HttpStatus.SC_OK);
        jsonObject.put("success", true);
        jsonObject.put("no_data", false);
        jsonObject.put("data", listObject);
        logger.info("No Candidate Available in the System.Sample Candidate Fetched " +
            "Successfully!");
        try {
          CandidateHandlerR.writeResponse(response,
              dbHandle, NO_DATA_STATUS, jsonObject);
        } catch (SQLException | DbHandleException e) {
          if (null != dbHandle) {
            dbHandle.checkAndCloseConnection();
          }
          ResponseHelper.writeInternalServerError(routingContext.response());
        }
        return;

      }
    }

    if ("0".equals(StringUtil.printValidString(params.get("id"))) &&
        StringUtil.printValidString(params.get("organization_id")).length() > 0) {
      DbHandle dbHandler = DbHelper.getDBHandle(routingContext, false);
      Organization organization = null;
      try {
        organization = new OrganizationDAOImpl(dbHandler).
            getOrganizationById(Long.parseLong(params.get("organization_id")));
        boolean doesOrganizationExists = (organization != null);
        int jobCount = Integer.parseInt(new DatabaseUtil().
            getTablename("Job", "count(1)",
                "organization_id =" + params.get("organization_id")));
        dbHandler.checkAndCloseConnection();
        if (jobCount == 0 && doesOrganizationExists) {
          String fieldString = StringUtil.printValidString(params.get("fields"));
          JsonObject dataObject = getSampleData(organization);
          if (fieldString.length() > 0) {
            if (fieldString.contains("EmailList")) {
              JsonObject object = new JsonObject();
              object.put("EmailList", dataObject.getJsonArray("EmailList"));
              object.put("Emailcount", dataObject.getInteger("Emailcount"));

              JsonObject responseObject = new JsonObject();
              responseObject.put("code", HttpStatus.SC_OK);
              responseObject.put("success", true);
              responseObject.put("data", object);
              responseObject.put("no_data", false);
              routingContext.response().setStatusCode(HttpStatus.SC_OK)
                  .putHeader("Content-Type", "application/json; charset=utf-8")
                  .end(responseObject.toString());
              return;
            } else if (fieldString.contains("CommentsList")) {
              JsonObject object = new JsonObject();
              object.put("CommentsList", dataObject.getJsonArray("CommentsList"));
              object.put("Commentcount", dataObject.getInteger("Commentcount"));

              JsonObject responseObject = new JsonObject();
              responseObject.put("code", HttpStatus.SC_OK);
              responseObject.put("success", true);
              responseObject.put("data", object);
              responseObject.put("no_data", false);
              routingContext.response().setStatusCode(HttpStatus.SC_OK)
                  .putHeader("Content-Type", "application/json; charset=utf-8")
                  .end(responseObject.toString());
              return;
            } else if (fieldString.contains("InterviewDetails") && fieldString.contains("status")) {
              JsonObject interviewDataObject = new JsonObject();
              interviewDataObject.put("status", "Interview^Schedule Interview");
              interviewDataObject.put("no_data", false);
              interviewDataObject.put("interview_date", "04/27/2018");
              interviewDataObject.put("interview_time", "10:45:50 am");
              interviewDataObject.put("interview_details", "");
              interviewDataObject.put("interview_duration", "45 minutes");
              interviewDataObject.put("interview_type", "Web");
              interviewDataObject.put("interview_location", "");
              interviewDataObject.put("interview_cancel_reason", "-");
              interviewDataObject.put("statusCheck", "none");
              interviewDataObject.put("modified_date", "04/23/2018");
              interviewDataObject.put("interview_end_time", "11:45:20 am");
              interviewDataObject.put("collaborator_id", "0");
              interviewDataObject.put("isCompleted", false);
              interviewDataObject.put("interview_mail_description",
                  "Congratulations John Smith!<br/>" +
                      "<br/> AuzmorHCM would like to have an web inteview with you!<br/>" +
                      "<br/>Details for your interview will appear below: " +
                      "<br/> <br/>Your interview has been scheduled for Friday, Apr 27th, 2018 " +
                      "at 10:45:50 am. <br/>" +
                      "<br/>Please respond to this invite by accepting or denying the invitation. " +
                      "<a href=\"https://ats.auzmor-hcm.com\" > Click here </a> <br/>" +
                      "<br/>We look forward to our interview with you! <br/>" +
                      "<br/>Sincerely, <br/><br/>Administrator, at AuzmorHCM");
              interviewDataObject.put("subject_name", "Interview Schedule Details");
              JsonObject interviewDetailsObject = new JsonObject();
              interviewDetailsObject.put("InterviewDetails", new JsonArray().add(interviewDataObject));
              JsonObject responseObject = new JsonObject();
              responseObject.put("code", HttpStatus.SC_OK);
              responseObject.put("success", true);
              responseObject.put("data", interviewDataObject);
              responseObject.put("no_data", false);
              routingContext.response().setStatusCode(HttpStatus.SC_OK)
                  .putHeader("Content-Type", "application/json; charset=utf-8")
                  .end(responseObject.toString());
              return;
            } else if (fieldString.contains("OfferDetails") && fieldString.contains("status")) {
              JsonObject offerDataObject = new JsonObject();
              offerDataObject.put("offer_position", "");
              offerDataObject.put("offer_pay_rate", "");
              offerDataObject.put("offer_start_date", "");
              offerDataObject.put("offer_job_location", "");
              offerDataObject.put("offer_contract_details", "-");
              offerDataObject.put("offer_decline_reason", "-");
              offerDataObject.put("modified_date", "");
              offerDataObject.put("statusCheck", "none");
              JsonObject offerDetailsObject = new JsonObject()
                  .put("OfferDetails", new JsonArray().add(offerDataObject));
              JsonObject responseObject = new JsonObject();
              responseObject.put("code", HttpStatus.SC_OK);
              responseObject.put("success", true);
              responseObject.put("data", offerDetailsObject);
              responseObject.put("no_data", false);
              routingContext.response().setStatusCode(HttpStatus.SC_OK)
                  .putHeader("Content-Type", "application/json; charset=utf-8")
                  .end(responseObject.toString());
              return;
            }
          } else {
            JsonObject object = new JsonObject();
            object.put("code", HttpStatus.SC_OK);
            object.put("success", true);
            object.put("data", dataObject);
            object.put("no_data", false);

            routingContext.response()
                .setStatusCode(HttpStatus.SC_OK)
                .putHeader("Content-Type", "application/json; charset=utf-8")
                .end(object.toString());
            return;
          }
        }

      } catch (SQLException e) {
        if (null != dbHandler) {
          dbHandler.checkAndCloseConnection();
        }
        e.printStackTrace();
      } catch (DbHandleException e) {
        if (null != dbHandler) {
          dbHandler.checkAndCloseConnection();
        }
        e.printStackTrace();
      }
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          /*----------------checking access role for admin/HiringLead/Collaborator ---------------------*/
         /* String roles = ((Role) routingContext.data().get("roles")).getName();
          String userType = (String) routingContext.data().get("userType");*/

          CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
          JobBO jobBO = new JobBOImpl(dbHandle);
          /*int hiringLead = 0;
          int adminId = 0;*/

          CandidateBO candidateBO = new CandidateBOImpl(dbHandle);
          CandidateStatusEnum candidateStatusEnum = null;
          if ((!StringUtil.printValidString(requestParams.get("fields")).equals("") &&
              !StringUtil.printValidString(requestParams.get("id")).equals("")) ||
              (!StringUtil.printValidString(requestParams.get("fields")).equals("") &&
                  !StringUtil.printValidString(requestParams.get("id")).equals("") &&
                  !StringUtil.printValidString(requestParams.get("organization_id")).equals(""))) {

            long candidateId = Long.parseLong(requestParams.get("id"));
            CandidateR candidateDetails = candidateDAO.getCandidate(candidateId);
            Job job = jobBO.getJobDetails(candidateDetails.getJobId());

              /*int employeeId = ((Employee) routingContext.data().get("employee")).getEmployee_id();
              int loginId = (int) ((Login) routingContext.data().get("login")).getId();

              if (roles.equalsIgnoreCase("Admin") || roles.equalsIgnoreCase("Hiring Lead")
                  || roles.equalsIgnoreCase("Collaborator") || roles.equalsIgnoreCase("Candidate")) {


                long candidateLoginId = candidateDetails.getUserId();

                // checking whether admin is available in this organization
                boolean employeeCheck = new EmployeeDAOImpl(dbHandle).
                    isEmployeeExistsInOrganization(employeeId,candidateDetails.getOrganizationId());

                String hiringLeadId = job.getHiring_lead();

                if (!StringUtils.isNullOrEmpty(hiringLeadId)) {
                  hiringLead = Integer.parseInt(hiringLeadId);
                }

                //checking either admin and hiring lead for that job can cancel the interview.

                if ((employeeCheck == true) || (hiringLead == employeeId) || (candidateLoginId == loginId)) {
*/
            JsonObject candidateObject = new JsonObject();
            if (requestParams.get("fields").contains("EmailList")) {
              /* ----------------mail List start here ---------------------------------------*/
              MailDetailsBO mailDetailsBO = new MailDetailsBOImpl(dbHandle);
              List<HashMap<String, String>> mailDetail = mailDetailsBO.
                  getMailDetails(candidateId);
              CandidateR candidate = candidateBO.getCandidate(dbHandle, candidateId);
              JsonArray mailArray = new JsonArray();
              for (HashMap<String, String> map : mailDetail) {
                JsonObject dataObject = new JsonObject();
                for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                  String key = mapEntry.getKey();
                  String value = mapEntry.getValue();
                  String adminName = candidate.getFirstName() + " " + candidate.getLastName();
                  if (key.equals("user_id")) {
                    String profileImage = null;
                    if (Integer.parseInt(value) > 0) {
                      long userId = Long.parseLong(value);
                      EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
                      Employee employee = employeeBO.getEmployeeDetails(userId);
                      if (!StringUtil.printValidString(employee.getProfile_image()).equals(""))
                        dataObject.put("user_profile_image", employee.getProfile_image());
                      else
                        dataObject.put("user_profile_image", profileImage);
                      if (!StringUtil.printValidString(employee.getFirst_name()).equals(""))
                        dataObject.put("admin_name",
                            employee.getFirst_name() + " " + employee.getLast_name());
                    } else {
                      dataObject.put("user_profile_image", profileImage);
                      dataObject.put("admin_name", adminName);
                    }
                  } else if (key.equals("admin_name"))
                    continue;
                  else
                    dataObject.put(key, value);
                }
                mailArray.add(dataObject);
              }
              CandidateInterviewBO interviewBO = new CandidateInterviewBOImpl(dbHandle);
              List<HashMap<String, String>> InterviewMailDetail = interviewBO.
                  getInterviewMailDetails(candidateId);
              if (InterviewMailDetail.size() > 0) {

                for (HashMap<String, String> map : InterviewMailDetail) {
                  JsonObject dataObject = new JsonObject();
                  for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                    String key = mapEntry.getKey();
                    String value = mapEntry.getValue();
                    String adminName = candidate.getFirstName() + " " + candidate.getLastName();
                    if (key.equals("user_id")) {
                      String profileImage = null;
                      if (Integer.parseInt(value) > 0) {
                        long userId = Long.parseLong(value);
                        EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
                        Employee employee = employeeBO.getEmployeeDetails(userId);
                        if (!StringUtil.printValidString(employee.getProfile_image()).equals("")) {
                          dataObject.put("user_profile_image", employee.getProfile_image());
                        } else {
                          dataObject.put("user_profile_image", profileImage);
                        }
                        dataObject.put("admin_name", employee.getFirst_name() + " " + employee.getLast_name());

                      } else {
                        dataObject.put("user_profile_image", profileImage);
                        dataObject.put("admin_name", adminName);
                      }
                    } else if (key.equals("admin_name"))
                      continue;
                    else
                      dataObject.put(key, value);
                  }
                  mailArray.add(dataObject);
                }
              }

              CandidateOfferBO offerBO = new CandidateOfferBOImpl(dbHandle);
              List<HashMap<String, String>> offerMailDetail = offerBO.
                  getOfferMailDetails(candidateId);
              if (offerMailDetail.size() > 0) {
                for (HashMap<String, String> map : offerMailDetail) {
                  JsonObject dataObject = new JsonObject();
                  for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                    String key = mapEntry.getKey();
                    String value = mapEntry.getValue();
                    String adminName = candidate.getFirstName() + " " + candidate.getLastName();
                    if (key.equals("user_id")) {
                      if (Integer.parseInt(value) > 0) {
                        long userId = Long.parseLong(value);
                        EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
                        Employee employee = employeeBO.getEmployeeDetails(userId);
                        if (!StringUtil.printValidString(employee.getProfile_image()).equals("")) {
                          dataObject.put("user_profile_image", employee.getProfile_image());
                        } else {
                          String profileImage = JWTUtil.getProfileImagePath();
                          dataObject.put("user_profile_image", profileImage);
                        }
                        dataObject.put("admin_name", employee.getFirst_name() + " " + employee.getLast_name());
                      } else {
                        String profileImage = JWTUtil.getProfileImagePath();
                        dataObject.put("user_profile_image", profileImage);
                        dataObject.put("admin_name", adminName);
                      }
                    } else if (key.equals("admin_name"))
                      continue;
                    else
                      dataObject.put(key, value);
                  }
                  mailArray.add(dataObject);
                }
              }

              // sorting email list ascending
              // Extract the JSONObjects
              JsonObject[] objects = new JsonObject[mailArray.size()];
              for (int i = 0; i < objects.length; i++) {
                objects[i] = mailArray.getJsonObject(i);
              }
              // Sort the array of JSONObjects
              Arrays.sort(
                  objects,
                  (JsonObject o1, JsonObject o2) ->
                      (o1.getString("created_date").compareTo(o2.getString("created_date")))
              );

              for (JsonObject object : objects) {
                try {
                  String createdDate = object.getString("created_date");
                  if (null != createdDate) {
                    String[] date = object.getString("created_date").split(" at ");
                    // String formattableDate = new DateUtil().getDate().parse(date[0]).toString();
                    String formattedDate = new DateUtil().getDateFormatDB(routingContext, date[0]);
                    // checking date difference
                    long getDaysDiff = new DateUtil().DateDifferenceBetweenTwoDates(date[0]);

                    if (getDaysDiff > 7) {
                      object.put("created_date", formattedDate);
                    } else {
                      object.put("created_date", formattedDate + " at " + date[1]);
                    }
                  }
                } catch (ParseException e) {
                  dbHandle.checkAndCloseConnection();
                  ResponseHelper.writeInternalServerError(routingContext.response());
                  return;
                }
              }
              // Build a new JSONArray from the sorted array
              JsonArray sortedArray = new JsonArray();
              for (JsonObject o : objects) {
                sortedArray.add(o);
              }
              JsonArray descSortedArray = new JsonArray();

              for (int i = sortedArray.size() - 1; i >= 0; i--) {
                descSortedArray.add(sortedArray.getJsonObject(i));
              }

              candidateObject.put("EmailList", descSortedArray);
              int emailCount = InterviewMailDetail.size() + offerMailDetail.size() + mailDetail.size();
              candidateObject.put("Emailcount", emailCount);
              /* ----------------Comment List---------------------------------------*/
            } else if (requestParams.get("fields").contains("InterviewDetails") &&
                requestParams.get("fields").contains("status") &&
                requestParams.get("fields").contains("job_id")) {

              CandidateInterviewBO interviewBO = new CandidateInterviewBOImpl(dbHandle);
              InterviewProcessCandidate interviewProcessCandidate = interviewBO.getInterviewDetails(
                  candidateId);
              CandidateR candidateR = candidateBO.getCandidate(dbHandle, candidateId);
              if (!StringUtil.printValidString(candidateR.getStatus()).equals("")) {
                CandidateHiringProcessBO candidateHiringProcessBO = new
                    CandidateHiringProcessBOImpl(dbHandle);
                String statusCheck = candidateHiringProcessBO.
                    getStatus(candidateId, candidateR.getStatus());
                String status = candidateR.getStatus() + "^" + statusCheck;
                candidateObject.put("status", status);
              }

              if (candidateR.getJobId() != 0) {
                if (job != null) {
                  candidateObject.put("job_title", job.getTitle());
                } else {
                  candidateObject.put("job_title", "-");
                }
              }
              try {
                String date = new DateUtil().
                    convertDateintoString(candidateR.getCreated());
                candidateObject.put("applied_on", new DateUtil().getDateFormat(routingContext,
                    date));
                if (!StringUtil.printValidString(String.valueOf(candidateR.getModified())).equals("")) {
                  date = new DateUtil().
                      convertDateintoString(candidateR.getModified());
                  candidateObject.put("applied_on", new DateUtil().getDateFormat(routingContext,
                      date));
                }
              } catch (ParseException e) {
                e.printStackTrace();
              }

              JsonArray interviewArray = new JsonArray();
              JsonObject interviewObject = new JsonObject();

              if (interviewProcessCandidate != null) {

                String interviewDate = new DateUtil().getDate().
                    format(interviewProcessCandidate.getInterviewDate());
                interviewObject.put("interview_date", new DateUtil().getDateFormat(routingContext,
                    interviewDate));
                interviewObject.put("interview_time", interviewProcessCandidate.getInterviewTime());
                interviewObject.put("interview_details", interviewProcessCandidate.getInterviewDetails());
                interviewObject.put("interview_duration", interviewProcessCandidate.getInterviewDuration());
                interviewObject.put("interview_type", interviewProcessCandidate.getInterviewType());
                interviewObject.put("interview_location", interviewProcessCandidate.getInterviewLocation());
                interviewObject.put("interview_end_time", interviewProcessCandidate.getInterviewEndTime());

                if (interviewProcessCandidate.getCollaboratorId() > 0) {
                  EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
                  Employee employee = employeeBO.getEmployeeDetails(interviewProcessCandidate.getCollaboratorId());
                  if (employee != null)
                    interviewObject.put("collaborator_id", employee.getFirst_name() + " " + employee.getLast_name());
                  else
                    interviewObject.put("collaborator_id", "");
                } else {
                  interviewObject.put("collaborator_id", "");
                }

                if (!StringUtil.printValidString(interviewProcessCandidate.
                    getInterviewCancelReason()).equals("")) {
                  interviewObject.put("interview_cancel_reason",
                      interviewProcessCandidate.getInterviewCancelReason());
                } else if (!StringUtil.printValidString(interviewProcessCandidate.
                    getStatus()).equals("")) {

                  if (interviewProcessCandidate.getStatus().equals("reject")) {
                    CandidateHiringProcessBO candidateHiringProcessBO = new
                        CandidateHiringProcessBOImpl(dbHandle);
                    HiringProcessCandidate hiringProcessCandidate = candidateHiringProcessBO.
                        getHiringProcess(candidateId);

                    interviewObject.put("interview_cancel_reason", hiringProcessCandidate.getReason());
                  } else {
                    interviewObject.put("interview_cancel_reason", "");
                  }

                } else {
                  interviewObject.put("interview_cancel_reason", "");
                }

                if (!StringUtil.printValidString(String.valueOf(interviewProcessCandidate.getModified())).
                    equals("")) {
                  String modDate = "";
                  try {
                    modDate = new DateUtil().convertDateintoString(interviewProcessCandidate.
                        getModified());
                  } catch (ParseException e) {
                    e.printStackTrace();
                  }
                  interviewObject.put("modified_date", new DateUtil().getDateFormat(routingContext,
                      modDate));
                } else {
                  String modDate = "";
                  try {
                    modDate = new DateUtil().convertDateintoString(
                        interviewProcessCandidate.getCreated());
                  } catch (ParseException e) {
                    e.printStackTrace();
                  }
                  interviewObject.put("modified_date", new DateUtil().getDateFormat(routingContext,
                      modDate));
                }

                interviewObject.put("interview_mail_description", interviewProcessCandidate.
                    getInterviewMailDescription());
                interviewObject.put("subject_name", interviewProcessCandidate.getSubjectName());
                interviewObject.put("isCompleted", interviewProcessCandidate.isCompleted());

                interviewArray.add(interviewObject);
                candidateObject.put("no_data", false);

              } else {
                JsonObject interviewObjectDate = new JsonObject();
                String val = "-";
                interviewObjectDate.put("interview_date", "");
                interviewObjectDate.put("interview_time", "");
                interviewObjectDate.put("interview_details", "");
                interviewObjectDate.put("interview_duration", "");
                interviewObjectDate.put("interview_type", "");
                interviewObjectDate.put("interview_location", "");
                interviewObjectDate.put("interview_mail_description", "");
                interviewObjectDate.put("interview_cancel_reason", val);
                interviewObjectDate.put("modified_date", "");
                interviewObjectDate.put("subject_name", "");
                interviewObjectDate.put("statusCheck", "");
                interviewObjectDate.put("interview_end_time", "");
                interviewObjectDate.put("collaborator_id", "");
                interviewObjectDate.put("job_title", "");
                interviewObjectDate.put("applied_on", "");
                interviewObjectDate.put("isCompleted", false);
                interviewArray.add(interviewObjectDate);
                candidateObject.put("no_data", true);
              }

              candidateObject.put("InterviewDetails", interviewArray);
            } else if (requestParams.get("fields").contains("InterviewDetails") &&
                requestParams.get("fields").contains("status")) {

              CandidateInterviewBO interviewBO = new CandidateInterviewBOImpl(dbHandle);
              InterviewProcessCandidate interviewProcessCandidate = interviewBO.getInterviewDetails(
                  candidateId);
              CandidateR candidateR = candidateBO.getCandidate(dbHandle, candidateId);
              if (!StringUtil.printValidString(candidateR.getStatus()).equals("")) {
                CandidateHiringProcessBO candidateHiringProcessBO = new
                    CandidateHiringProcessBOImpl(dbHandle);
                String statusCheck = candidateHiringProcessBO.
                    getStatus(candidateId, candidateR.getStatus());
                String status = candidateR.getStatus() + "^" + statusCheck;
                candidateObject.put("status", status);
              }
              JsonArray interviewArray = new JsonArray();
              JsonObject interviewObject = new JsonObject();
              if (interviewProcessCandidate != null) {
                interviewObject.put("interview_date", new DateUtil().getDate().
                    format(interviewProcessCandidate.getInterviewDate()));
                interviewObject.put("interview_time", interviewProcessCandidate.getInterviewTime());
                interviewObject.put("interview_details", interviewProcessCandidate.getInterviewDetails());
                interviewObject.put("interview_duration", interviewProcessCandidate.getInterviewDuration());
                interviewObject.put("interview_type", interviewProcessCandidate.getInterviewType());
                interviewObject.put("interview_location", interviewProcessCandidate.getInterviewLocation());
                interviewObject.put("interview_end_time", interviewProcessCandidate.getInterviewEndTime());
                interviewObject.put("collaborator_id", interviewProcessCandidate.getCollaboratorId());


                if (!StringUtil.printValidString(interviewProcessCandidate.
                    getInterviewCancelReason()).equals("")) {
                  interviewObject.put("interview_cancel_reason",
                      interviewProcessCandidate.getInterviewCancelReason());
                } else if (!StringUtil.printValidString(interviewProcessCandidate.
                    getStatus()).equals("")) {
                  if (interviewProcessCandidate.getStatus().equals("reject")) {
                    CandidateHiringProcessBO candidateHiringProcessBO = new
                        CandidateHiringProcessBOImpl(dbHandle);
                    HiringProcessCandidate hiringProcessCandidate = candidateHiringProcessBO.
                        getHiringProcess(candidateId);
                    interviewObject.put("statusCheck", interviewProcessCandidate.getStatus());
                    interviewObject.put("interview_cancel_reason", hiringProcessCandidate.getReason());
                  } else {
                    interviewObject.put("interview_cancel_reason", "");
                  }
                } else {
                  interviewObject.put("interview_cancel_reason", "");
                  interviewObject.put("statusCheck", "none");
                }
                if (!StringUtil.printValidString(String.valueOf(interviewProcessCandidate.getModified())).
                    equals("")) {
                  String modDate = "";
                  try {
                    modDate = new DateUtil().convertDateintoString(interviewProcessCandidate.
                        getModified());
                  } catch (ParseException e) {
                    e.printStackTrace();
                  }
                  interviewObject.put("modified_date", modDate);
                } else {
                  String modDate = "";
                  try {
                    modDate = new DateUtil().convertDateintoString(
                        interviewProcessCandidate.getCreated());
                  } catch (ParseException e) {
                    e.printStackTrace();
                  }
                  interviewObject.put("modified_date", modDate);
                }

                interviewObject.put("interview_mail_description", interviewProcessCandidate.
                    getInterviewMailDescription());
                interviewObject.put("subject_name", interviewProcessCandidate.getSubjectName());
                interviewObject.put("isCompleted", interviewProcessCandidate.isCompleted());

                interviewArray.add(interviewObject);
                candidateObject.put("no_data", false);
              } else {
                JsonObject interviewObjectDate = new JsonObject();
                String val = "-";
                interviewObjectDate.put("interview_date", "");
                interviewObjectDate.put("interview_time", "");
                interviewObjectDate.put("interview_details", "");
                interviewObjectDate.put("interview_duration", "");
                interviewObjectDate.put("interview_type", "");
                interviewObjectDate.put("interview_location", "");
                interviewObjectDate.put("interview_mail_description", "");
                interviewObjectDate.put("interview_cancel_reason", val);
                interviewObjectDate.put("modified_date", "");
                interviewObjectDate.put("subject_name", "");
                interviewObjectDate.put("statusCheck", "");
                interviewObjectDate.put("interview_end_time", "");
                interviewObjectDate.put("collaborator_id", "");
                interviewObjectDate.put("isCompleted", false);
                interviewArray.add(interviewObjectDate);
                candidateObject.put("no_data", true);
              }

              candidateObject.put("InterviewDetails", interviewArray);
            } else if (requestParams.get("fields").contains("OfferDetails") &&
                requestParams.get("fields").contains("status")) {
              /* ------------------ Offer Details -------------------------------------------  */

              CandidateOfferBO offerBO = new CandidateOfferBOImpl(dbHandle);
              OfferProcessCandidate offerProcessCandidate = offerBO.
                  getOfferDetails(candidateId);
              CandidateR candidateR = candidateBO.getCandidate(dbHandle, candidateId);
              if (!StringUtil.printValidString(candidateR.getStatus()).equals("")) {

                CandidateHiringProcessBO candidateHiringProcessBO = new
                    CandidateHiringProcessBOImpl(dbHandle);

                String statusCheck = candidateHiringProcessBO.
                    getStatus(candidateId, candidateR.getStatus());
                String status = candidateR.getStatus() + "^" + statusCheck;
                candidateObject.put("status", status);
              }

              JsonArray OfferDetails = new JsonArray();

              if (offerProcessCandidate != null) {
                JsonObject offerObject = new JsonObject();

                String offerDate = new DateUtil().getDate().
                    format(offerProcessCandidate.getOfferStartDate());

                offerObject.put("offer_position", offerProcessCandidate.getOfferPosition());
                offerObject.put("offer_pay_rate", offerProcessCandidate.getOfferPayRate());
                offerObject.put("offer_start_date", offerDate);
                offerObject.put("offer_job_location", offerProcessCandidate.getOfferJobLocation());
                offerObject.put("offer_attachment_name", offerProcessCandidate.getOfferAttachmentName());
                offerObject.put("sub_status", offerProcessCandidate.getSubStatus());
                if (StringUtil.printValidString(offerProcessCandidate.getCurrencyType()).equals("")) {
                  String currencyValue = ((Organization) routingContext.data().get("organization")).getDefault_currency();
                  offerObject.put("currency_type", currencyValue);
                } else {
                  offerObject.put("currency_type", offerProcessCandidate.getCurrencyType());
                }
                if (!StringUtil.printValidString(offerProcessCandidate.
                    getOfferDeclineReason()).equals("")) {
                  offerObject.put("offer_decline_reason", offerProcessCandidate.getOfferDeclineReason());
                } else {
                  if (!StringUtil.printValidString(offerProcessCandidate.getStatus()).equals("")) {
                    if (offerProcessCandidate.getStatus().equals("reject")) {
                      CandidateHiringProcessBO candidateHiringProcessBO = new
                          CandidateHiringProcessBOImpl(dbHandle);
                      HiringProcessCandidate hiringProcessCandidate = candidateHiringProcessBO.
                          getHiringProcess(candidateId);
                      offerObject.put("offer_decline_reason", hiringProcessCandidate.getReason());
                      offerObject.put("statusCheck", offerProcessCandidate.getStatus());
                    } else {
                      offerObject.put("offer_decline_reason", "");
                      offerObject.put("statusCheck", offerProcessCandidate.getStatus());
                    }
                  } else {
                    offerObject.put("offer_decline_reason", "");
                    offerObject.put("statusCheck", "none");
                  }
                }
                if (!StringUtil.printValidString(String.valueOf(offerProcessCandidate.getModified())).
                    equals("")) {
                  String modDate = "";
                  try {
                    modDate = new DateUtil().convertDateintoString(offerProcessCandidate.
                        getModified());
                  } catch (ParseException e) {
                    e.printStackTrace();
                  }
                  offerObject.put("modified_date", modDate);
                } else {
                  String modDate = "";
                  try {
                    modDate = new DateUtil().convertDateintoString(
                        offerProcessCandidate.getCreated());
                  } catch (ParseException e) {
                    e.printStackTrace();
                  }
                  offerObject.put("modified_date", modDate);
                }
                if (StringUtil.printValidString(offerProcessCandidate.getOfferContractDetails()).
                    equals("")) {
                  offerObject.put("offer_contract_details", "");
                } else {
                  offerObject.put("offer_contract_details", offerProcessCandidate.
                      getOfferContractDetails());
                }

                OfferDetails.add(offerObject);
                candidateObject.put("no_data", false);
              }
              if (offerProcessCandidate == null) {
                JsonObject offerObject = new JsonObject();
                String val = "-";
                offerObject.put("offer_position", "");
                offerObject.put("offer_pay_rate", "");
                offerObject.put("offer_start_date", "");
                offerObject.put("offer_job_location", "");
                offerObject.put("currency_type", "");
                offerObject.put("offer_contract_details", val);
                offerObject.put("offer_decline_reason", val);
                offerObject.put("modified_date", "");
                offerObject.put("statusCheck", "none");
                offerObject.put("sub_status", "");
                OfferDetails.add(offerObject);
                candidateObject.put("no_data", true);
              }
              candidateObject.put("OfferDetails", OfferDetails);
              /*-------------------end here ------------------------------------------------*/

            } else if (requestParams.get("fields").contains("OfferDetails")) {
              /* ------------------ Offer Details -------------------------------------------  */

              CandidateOfferBO offerBO = new CandidateOfferBOImpl(dbHandle);
              OfferProcessCandidate offerProcessCandidate = offerBO.
                  getOfferDetails(candidateId);

              JsonArray OfferDetails = new JsonArray();

              if (offerProcessCandidate != null) {
                JsonObject offerObject = new JsonObject();

                String offerDate = new DateUtil().getDate().
                    format(offerProcessCandidate.getOfferStartDate());

                offerObject.put("offer_position", offerProcessCandidate.getOfferPosition());
                offerObject.put("offer_pay_rate", offerProcessCandidate.getOfferPayRate());
                offerObject.put("offer_start_date", offerDate);
                offerObject.put("offer_job_location", offerProcessCandidate.getOfferJobLocation());
                offerObject.put("offer_attachment_name", offerProcessCandidate.getOfferAttachmentName());
                offerObject.put("currency_type", offerProcessCandidate.getCurrencyType());
                offerObject.put("sub_status", offerProcessCandidate.getSubStatus());

                if (!StringUtil.printValidString(offerProcessCandidate.
                    getOfferDeclineReason()).equals("")) {
                  offerObject.put("offer_decline_reason", offerProcessCandidate.getOfferDeclineReason());
                } else {
                  if (!StringUtil.printValidString(offerProcessCandidate.getStatus()).equals("")) {
                    if (offerProcessCandidate.getStatus().equals("reject")) {
                      CandidateHiringProcessBO candidateHiringProcessBO = new
                          CandidateHiringProcessBOImpl(dbHandle);
                      HiringProcessCandidate hiringProcessCandidate = candidateHiringProcessBO.
                          getHiringProcess(candidateId);
                      offerObject.put("offer_decline_reason", hiringProcessCandidate.getReason());
                      offerObject.put("statusCheck", offerProcessCandidate.getStatus());
                    } else {
                      offerObject.put("offer_decline_reason", "");
                      offerObject.put("statusCheck", offerProcessCandidate.getStatus());
                    }
                  } else {
                    offerObject.put("offer_decline_reason", "");
                    offerObject.put("statusCheck", "none");
                  }
                }

                if (!StringUtil.printValidString(String.valueOf(offerProcessCandidate.getModified())).
                    equals("")) {
                  String modDate = "";
                  try {
                    modDate = new DateUtil().convertDateintoString(offerProcessCandidate.
                        getModified());
                  } catch (ParseException e) {
                    e.printStackTrace();
                  }
                  offerObject.put("modified_date", modDate);
                } else {
                  String modDate = "";
                  try {
                    modDate = new DateUtil().convertDateintoString(
                        offerProcessCandidate.getCreated());
                  } catch (ParseException e) {
                    e.printStackTrace();
                  }
                  offerObject.put("modified_date", modDate);
                }
                if (StringUtil.printValidString(offerProcessCandidate.getOfferContractDetails()).
                    equals("")) {
                  offerObject.put("offer_contract_details", "");
                } else {
                  offerObject.put("offer_contract_details", offerProcessCandidate.
                      getOfferContractDetails());
                }

                candidateObject.put("no_data", false);
                OfferDetails.add(offerObject);
              }
              if (offerProcessCandidate == null) {
                JsonObject offerObject = new JsonObject();
                String val = "-";
                offerObject.put("offer_position", "");
                offerObject.put("offer_pay_rate", "");
                offerObject.put("offer_start_date", "");
                offerObject.put("offer_job_location", "");
                offerObject.put("currency_type", "");
                offerObject.put("offer_contract_details", val);
                offerObject.put("offer_decline_reason", val);
                offerObject.put("sub_status", "");
                offerObject.put("modified_date", "");
                OfferDetails.add(offerObject);
                candidateObject.put("no_data", true);
              }
              candidateObject.put("OfferDetails", OfferDetails);
              /*-------------------end here ------------------------------------------------*/

            } else if (requestParams.get("fields").contains("CommentsList")) {

              CandidateCommentBO commentBO = new CandidateCommentBOImpl(dbHandle);
              String format = ((Organization) routingContext.data().get("organization")).getDate_format();
              List<HashMap<String, String>> commentDetails = commentBO.
                  getCommentList(format, candidateId);
              CandidateR candidateR = candidateBO.getCandidate(dbHandle, candidateId);

              JsonArray commentArray = new JsonArray();
              for (HashMap<String, String> map : commentDetails) {
                JsonObject dataObject = new JsonObject();
                for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                  String key = mapEntry.getKey();
                  String value = mapEntry.getValue();
                  String adminName = "";

                  if (key.equals("user_id")) {
                    if (Integer.parseInt(value) > 0) {
                      long userId = Long.parseLong(value);
                      EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
                      Employee employee = employeeBO.getEmployeeDetails(userId);
                      if (!StringUtil.printValidString(employee.getProfile_image()).equals("")) {
                        dataObject.put("user_profile_image", employee.getProfile_image());
                      } else {
                        String profileImage = JWTUtil.getProfileImagePath();
                        dataObject.put("user_profile_image", profileImage);
                      }
                      adminName = employee.getFirst_name() + " " + employee.getLast_name();
                      dataObject.put("admin_name", adminName);
                    } else {
                      String profileImage = JWTUtil.getProfileImagePath();
                      dataObject.put("user_profile_image", profileImage);
                      adminName = candidateR.getFirstName() + " " + candidateR.getLastName();
                      dataObject.put("admin_name", adminName);
                    }
                  }
                  dataObject.put(key, value);
                }
                commentArray.add(dataObject);
              }
              int commentCount = commentDetails.size();
              candidateObject.put("CommentsList", commentArray);
              candidateObject.put("Commentcount", commentCount);

              /*-------------------------end here---------------------------------*/
            } else if (requestParams.get("fields").contains("HistoryList")) {

              CandidateCommentBO commentBO = new CandidateCommentBOImpl(dbHandle);
              String format = ((Organization) routingContext.data().get("organization")).getDate_format();
              List<HashMap<String, String>> commentDetails = commentBO.
                  getHistoryList(format, candidateId);
              CandidateR candidateR = candidateBO.getCandidate(dbHandle, candidateId);

              JsonArray commentArray = new JsonArray();
              for (HashMap<String, String> map : commentDetails) {
                JsonObject dataObject = new JsonObject();
                for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                  String key = mapEntry.getKey();
                  String value = mapEntry.getValue();
                  String adminName = "";

                  if (key.equals("user_id")) {
                    if (Integer.parseInt(value) > 0) {
                      long userId = Long.parseLong(value);
                      EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
                      Employee employee = employeeBO.getEmployeeDetails(userId);
                      if (!StringUtil.printValidString(employee.getProfile_image()).equals("")) {
                        dataObject.put("user_profile_image", employee.getProfile_image());
                      } else {
                        String profileImage = JWTUtil.getProfileImagePath();
                        dataObject.put("user_profile_image", profileImage);
                      }
                      adminName = employee.getFirst_name() + " " + employee.getLast_name();
                      dataObject.put("admin_name", adminName);
                    } else {
                      String profileImage = JWTUtil.getProfileImagePath();
                      dataObject.put("user_profile_image", profileImage);
                      adminName = candidateR.getFirstName() + " " + candidateR.getLastName();
                      dataObject.put("admin_name", adminName);
                    }
                  }
                  dataObject.put(key, value);
                }
                commentArray.add(dataObject);
              }
              int commentCount = commentDetails.size();
              candidateObject.put("HistoryList", commentArray);
              candidateObject.put("Historycount", commentCount);
              /*-------------------------end here---------------------------------*/
            } else if (requestParams.get("fields").contains("cand_status")) {
              JsonObject jobHiringDetails = jobBO.
                  getJobHiringDetails(candidateId);

              JsonArray hiringArray = new JsonArray();
              hiringArray.add(jobHiringDetails);

              candidateObject.put("cand_status", jobHiringDetails);
            }

            if (candidateObject.size() > 0) {
              CandidateHandlerR.writeResponse
                  (httpServerResponse, dbHandle, GET_CANDIDATE_SUCCESS, candidateObject);
            } else {
              logger.error("getting candidate data with fields and id");
              CandidateHandlerR.writeResponse
                  (httpServerResponse, dbHandle, candidateStatusEnum.GET_CANDIDATE_FAILED, null);
            }
               /* } else {
                  ResponseHelper.writeErrorResponse(HttpStatus.SC_UNAUTHORIZED, "You Cannot Consumes this Service",
                      "You Cannot Consumes this Service", httpServerResponse);
                }
              } else {
                ResponseHelper.writeErrorResponse(HttpStatus.SC_UNAUTHORIZED, "You Cannot Consumes this Service",
                    "You Cannot Consumes this Service", httpServerResponse);
              }*/
          }

          if (!StringUtil.printValidString(requestParams.get("id")).equals("") &&
              !StringUtil.printValidString(requestParams.get("organization_id")).equals("")) {

            long candidateId = Long.parseLong(requestParams.get("id"));
            long organizationId = Long.parseLong(requestParams.get("organization_id"));
            CandidateR candidateR = candidateBO.getCandidateId(dbHandle, candidateId,
                organizationId);

            /*  int employeeId = ((Employee) routingContext.data().get("employee")).getEmployee_id();
              int loginId = (int) ((Login) routingContext.data().get("login")).getId();

              if (roles.equalsIgnoreCase("Admin") || roles.equalsIgnoreCase("Hiring Lead")
                  || roles.equalsIgnoreCase("Collaborator") || roles.equalsIgnoreCase("Candidate")) {

                String candidateIdDetails = StringUtil.printValidString(requestParams.get("id"));


                long candidateLoginId = candidateR.getUserId();

                // checking whether admin is available in this organization
                boolean employeeCheck = new EmployeeDAOImpl(dbHandle).
                    isEmployeeExistsInOrganization(employeeId,candidateR.getOrganizationId());

                String hiringLeadId = job.getHiring_lead();

                if (!StringUtils.isNullOrEmpty(hiringLeadId)) {
                  hiringLead = Integer.parseInt(hiringLeadId);
                }

                //checking either admin and hiring lead for that job can cancel the interview.

                if ((employeeCheck == true && userType.equalsIgnoreCase("admin")) ||
                    (hiringLead == employeeId && userType.equalsIgnoreCase("employee")) ||
                    (candidateLoginId == loginId && userType.equalsIgnoreCase("candidate"))) {*/

            JsonObject candidateObject = new JsonObject();
            candidateObject.put("organization_id", candidateR.getOrganizationId());
            candidateObject.put("first_name", candidateR.getFirstName());
            candidateObject.put("last_name", candidateR.getLastName());
            candidateObject.put("email", candidateR.getEmail());
            candidateObject.put("phone_number", candidateR.getPhoneNumber());
            candidateObject.put("unit", candidateR.getUnit());
            candidateObject.put("source", candidateR.getSource());
            candidateObject.put("address_line", candidateR.getAddressLine());
            candidateObject.put("city", candidateR.getCity());
            if (!StringUtil.printValidString(candidateR.getState()).equals("")) {
              candidateObject.put("state", CountryStateHandler.getStateName(candidateR.getState()));
            } else {
              candidateObject.put("state", "");
            }
            candidateObject.put("zip", candidateR.getZip());
            candidateObject.put("country", candidateR.getCountry());
            candidateObject.put("job_id", candidateR.getJobId());
            candidateObject.put("rating", candidateR.getRating());
            candidateObject.put("candidate_id", candidateR.getCandidateId());
            if (candidateR.getHiringLeadId() == 0) {
              candidateObject.put("hiring_lead", "-");
            } else {
              candidateObject.put("hiring_lead", candidateR.getHiringLeadId());
            }
            candidateObject.put("internal_jobcode", candidateR.getInternalJobCode());
            candidateObject.put("resume", candidateR.getResume());

            if (candidateR.getJobId() != 0) {
              Job job = jobBO.getJobDetails(candidateR.getJobId());
              if (job != null) {
                candidateObject.put("employment_type", job.getEmployment_type());
                candidateObject.put("location", job.getLocation());
                candidateObject.put("job_status", job.getJob_status());
                candidateObject.put("department", job.getDepartment());
                candidateObject.put("minimum_experience", job.getMinimum_experience());
                candidateObject.put("job_title", job.getTitle());
              } else {
                candidateObject.put("employment_type", "-");
                candidateObject.put("location", "-");
                candidateObject.put("job_status", "-");
                candidateObject.put("department", "-");
                candidateObject.put("minimum_experience", "-");
                candidateObject.put("job_title", "-");
              }
            } else {
              candidateObject.put("employment_type", "-");
              candidateObject.put("location", "-");
              candidateObject.put("job_status", "-");
              candidateObject.put("department", "-");
              candidateObject.put("minimum_experience", "-");
              candidateObject.put("job_title", "-");
            }
            if (!StringUtil.printValidString(candidateR.getDateAvailable()).equals("")) {
              candidateObject.put("date_available", candidateR.getDateAvailable());
            } else {
              candidateObject.put("date_available", "-");
            }
            candidateObject.put("cover_letter", candidateR.getCoverLetter());
            candidateObject.put("highest_education", candidateR.getHighestEducation());
            candidateObject.put("reference", candidateR.getReference());
            if (!StringUtil.printValidString(candidateR.getDesiredSalary()).equals("")) {
              candidateObject.put("desired_salary", candidateR.getDesiredSalary());
            } else {
              candidateObject.put("desired_salary", "-");
            }
            candidateObject.put("college", candidateR.getCollege());
            candidateObject.put("referred_by", candidateR.getReferredBy());
            candidateObject.put("linkedin_url", candidateR.getLinkedinUrl());
            candidateObject.put("blog", candidateR.getBlog());
            candidateObject.put("company_type_check", candidateR.getCompanyTypeCheck());
            candidateObject.put("gender", candidateR.getGender());
            candidateObject.put("veteran_status", candidateR.getVeteranStatus());
            candidateObject.put("ethnicity", candidateR.getEthnicity());
            candidateObject.put("disability", candidateR.getDisability());

            try {
              String date = new DateUtil().
                  convertDateintoString(candidateR.getCreated());
              candidateObject.put("created_date", new DateUtil().getDateFormat(routingContext,
                  date));
              if (!StringUtil.printValidString(String.valueOf(candidateR.getModified())).equals("")) {
                date = new DateUtil().
                    convertDateintoString(candidateR.getModified());
                candidateObject.put("modified_date", new DateUtil().getDateFormat(routingContext,
                    date));
              }
            } catch (ParseException e) {
              e.printStackTrace();
            }
            if (!StringUtil.printValidString(candidateR.getStatus()).equals("")) {

              CandidateHiringProcessBO candidateHiringProcessBO = new
                  CandidateHiringProcessBOImpl(dbHandle);

              String statusCheck = candidateHiringProcessBO.
                  getStatus(candidateId, candidateR.getStatus());
              String status = candidateR.getStatus() + "^" + statusCheck;
              candidateObject.put("status", status);
            }
            /* ------------------------cand_status------------------------------*/

            jobBO = new JobBOImpl(dbHandle);
            JsonObject jobHiringDetails = jobBO.
                getJobHiringDetails(candidateId);
            JsonArray hiringArray = new JsonArray();
            hiringArray.add(jobHiringDetails);

            JsonObject newJsonObject = new JsonObject();
            JsonArray applicantDataJsonArray = new JsonArray();
            JsonArray screeningDataJsonArray = new JsonArray();
            JsonArray interviewDataJsonArray = new JsonArray();
            JsonArray offerDataJsonArray = new JsonArray();
            JsonArray hireDataJsonArray = new JsonArray();
            JsonArray notHiredDataJsonArray = new JsonArray();
            for (int i = 0; i < hiringArray.size(); i++) {
              int orderCount = 1;
              JsonObject jsonObject = hiringArray.getJsonObject(i);
              JsonArray jsonArray = jsonObject.getJsonArray("applicant");
              JsonArray screeningJsonArray = jsonObject.getJsonArray("screening");
              JsonArray interviewJsonArray = jsonObject.getJsonArray("interview");
              JsonArray offerJsonArray = jsonObject.getJsonArray("offer");
              JsonArray hireJsonArray = jsonObject.getJsonArray("hire");
              JsonArray notHiredJsonArray = jsonObject.getJsonArray("not_hired");

              for (int j = 0; j < jsonArray.size(); j++) {
                JsonObject applicantJsonObject = jsonArray.getJsonObject(j);
                String applicant = applicantJsonObject.getString("applicant");
                if (applicant != null) {
                  JsonObject object = new JsonObject();
                  object.put("applicant", applicantJsonObject.getValue("applicant"));
                  object.put("status", applicantJsonObject.getValue("status"));
                  object.put("order", orderCount++);
                  applicantDataJsonArray.add(object);
                }
              }
              for (int j = 0; j < screeningJsonArray.size(); j++) {
                JsonObject screeningJsonObject = screeningJsonArray.getJsonObject(j);
                String screening = screeningJsonObject.getString("screening");
                if (screening != null) {
                  JsonObject Object = new JsonObject();
                  Object.put("screening", screeningJsonObject.getValue("screening"));
                  Object.put("status", screeningJsonObject.getValue("status"));
                  Object.put("order", orderCount++);
                  screeningDataJsonArray.add(Object);
                }
              }
              for (int j = 0; j < interviewJsonArray.size(); j++) {
                JsonObject interviewJsonObject = interviewJsonArray.getJsonObject(j);
                String interview = interviewJsonObject.getString("interview");
                if (interview != null) {
                  JsonObject Object = new JsonObject();
                  Object.put("interview", interviewJsonObject.getValue("interview"));
                  Object.put("status", interviewJsonObject.getValue("status"));
                  Object.put("order", orderCount++);
                  interviewDataJsonArray.add(Object);
                }
              }
              for (int j = 0; j < offerJsonArray.size(); j++) {
                JsonObject offerJsonObject = offerJsonArray.getJsonObject(j);
                String offer = offerJsonObject.getString("offer");
                if (offer != null) {
                  JsonObject jsonObjectOfferData = new JsonObject();
                  jsonObjectOfferData.put("offer", offerJsonObject.getValue("offer"));
                  jsonObjectOfferData.put("status", offerJsonObject.getValue("status"));
                  jsonObjectOfferData.put("order", orderCount++);
                  offerDataJsonArray.add(jsonObjectOfferData);
                }
              }
              for (int j = 0; j < hireJsonArray.size(); j++) {
                JsonObject hireJsonObject = hireJsonArray.getJsonObject(j);
                String hire = hireJsonObject.getString("hire");
                if (hire != null) {
                  JsonObject object = new JsonObject();
                  object.put("hire", hireJsonObject.getValue("hire"));
                  object.put("status", hireJsonObject.getValue("status"));
                  object.put("order", orderCount++);
                  hireDataJsonArray.add(object);
                }
              }
              for (int j = 0; j < notHiredJsonArray.size(); j++) {
                JsonObject notHiredJsonObject = notHiredJsonArray.getJsonObject(j);
                String notHired = notHiredJsonObject.getString("not_hired");
                if (notHired != null) {
                  JsonObject jsonObjectNotHiredData = new JsonObject();
                  jsonObjectNotHiredData.put("not_hired", notHiredJsonObject.getValue("not_hired"));
                  jsonObjectNotHiredData.put("status", notHiredJsonObject.getValue("status"));
                  jsonObjectNotHiredData.put("order", orderCount++);
                  notHiredDataJsonArray.add(jsonObjectNotHiredData);
                }
              }
            }
            newJsonObject.put("applicant", applicantDataJsonArray);
            newJsonObject.put("screening", screeningDataJsonArray);
            newJsonObject.put("interview", interviewDataJsonArray);
            newJsonObject.put("offer", offerDataJsonArray);
            newJsonObject.put("hire", hireDataJsonArray);
            newJsonObject.put("not_hired", notHiredDataJsonArray);
            //candidateObject.put("cand_status", jobHiringDetails);
            candidateObject.put("cand_status", newJsonObject);

            /* ----------------mail List start here ---------------------------------------*/
            MailDetailsBO mailDetailsBO = new MailDetailsBOImpl(dbHandle);
            List<HashMap<String, String>> mailDetail = mailDetailsBO.
                getMailDetails(candidateId);
            CandidateR candidate = candidateBO.getCandidate(dbHandle, candidateId);
            JsonArray mailArray = new JsonArray();
            for (HashMap<String, String> map : mailDetail) {
              JsonObject dataObject = new JsonObject();
              for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                String adminName = candidate.getFirstName() + " " + candidate.getLastName();
                if (key.equals("user_id")) {
                  String profileImage = null;
                  if (Integer.parseInt(value) > 0) {
                    long userId = Long.parseLong(value);
                    EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
                    Employee employee = employeeBO.getEmployeeDetails(userId);
                    if (!StringUtil.printValidString(employee.getProfile_image()).equals(""))
                      dataObject.put("user_profile_image", employee.getProfile_image());
                    else
                      dataObject.put("user_profile_image", profileImage);
                    if (!StringUtil.printValidString(employee.getFirst_name()).equals(""))
                      dataObject.put("admin_name",
                          employee.getFirst_name() + " " + employee.getLast_name());
                  } else {
                    dataObject.put("user_profile_image", profileImage);
                    dataObject.put("admin_name", adminName);
                  }
                } else if (key.equals("admin_name"))
                  continue;
                else
                  dataObject.put(key, value);
              }
              mailArray.add(dataObject);
            }
            CandidateInterviewBO interviewBO = new CandidateInterviewBOImpl(dbHandle);
            List<HashMap<String, String>> InterviewMailDetail = interviewBO.
                getInterviewMailDetails(candidateId);
            if (InterviewMailDetail.size() > 0) {

              for (HashMap<String, String> map : InterviewMailDetail) {
                JsonObject dataObject = new JsonObject();
                for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                  String key = mapEntry.getKey();
                  String value = mapEntry.getValue();
                  String adminName = candidate.getFirstName() + " " + candidate.getLastName();
                  if (key.equals("user_id")) {
                    String profileImage = null;
                    if (Integer.parseInt(value) > 0) {
                      long userId = Long.parseLong(value);
                      EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
                      Employee employee = employeeBO.getEmployeeDetails(userId);
                      if (!StringUtil.printValidString(employee.getProfile_image()).equals("")) {
                        dataObject.put("user_profile_image", employee.getProfile_image());
                      } else {
                        dataObject.put("user_profile_image", profileImage);
                      }
                      dataObject.put("admin_name", employee.getFirst_name() + " " + employee.getLast_name());

                    } else {
                      dataObject.put("user_profile_image", profileImage);
                      dataObject.put("admin_name", adminName);
                    }
                  } else if (key.equals("admin_name"))
                    continue;
                  else
                    dataObject.put(key, value);
                }
                mailArray.add(dataObject);
              }
            }

            CandidateOfferBO offerBO = new CandidateOfferBOImpl(dbHandle);
            List<HashMap<String, String>> offerMailDetail = offerBO.
                getOfferMailDetails(candidateId);
            if (offerMailDetail.size() > 0) {
              for (HashMap<String, String> map : offerMailDetail) {
                JsonObject dataObject = new JsonObject();
                for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                  String key = mapEntry.getKey();
                  String value = mapEntry.getValue();
                  String adminName = candidate.getFirstName() + " " + candidate.getLastName();
                  if (key.equals("user_id")) {
                    if (Integer.parseInt(value) > 0) {
                      long userId = Long.parseLong(value);
                      EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
                      Employee employee = employeeBO.getEmployeeDetails(userId);
                      if (!StringUtil.printValidString(employee.getProfile_image()).equals("")) {
                        dataObject.put("user_profile_image", employee.getProfile_image());
                      } else {
                        String profileImage = JWTUtil.getProfileImagePath();
                        dataObject.put("user_profile_image", profileImage);
                      }
                      dataObject.put("admin_name", employee.getFirst_name() + " " + employee.getLast_name());
                    } else {
                      String profileImage = JWTUtil.getProfileImagePath();
                      dataObject.put("user_profile_image", profileImage);
                      dataObject.put("admin_name", adminName);
                    }
                  } else if (key.equals("admin_name"))
                    continue;
                  else
                    dataObject.put(key, value);
                }
                mailArray.add(dataObject);
              }
            }

            // sorting email list ascending
            // Extract the JSONObjects
            JsonObject[] objects = new JsonObject[mailArray.size()];
            for (int i = 0; i < objects.length; i++) {
              objects[i] = mailArray.getJsonObject(i);
            }
            // Sort the array of JSONObjects
            Arrays.sort(
                objects,
                (JsonObject o1, JsonObject o2) ->
                    (o1.getString("created_date").compareTo(o2.getString("created_date")))
            );
            // Build a new JSONArray from the sorted array
            JsonArray sortedArray = new JsonArray();
            for (JsonObject o : objects) {
              sortedArray.add(o);
            }
            JsonArray descSortedArray = new JsonArray();
            for (int i = sortedArray.size() - 1; i >= 0; i--) {
              descSortedArray.add(sortedArray.getJsonObject(i));
            }
            candidateObject.put("EmailList", descSortedArray);
            int emailCount = InterviewMailDetail.size() + offerMailDetail.size() + mailDetail.size();
            candidateObject.put("Emailcount", emailCount);
            /* ----------------Comment List---------------------------------------*/
            CandidateCommentBO commentBO = new CandidateCommentBOImpl(dbHandle);
            String format = ((Organization) routingContext.data().get("organization")).getDate_format();
            List<HashMap<String, String>> commentDetails = commentBO.
                getCommentList(format, candidateId);
            JsonArray commentArray = new JsonArray();

            for (HashMap<String, String> map : commentDetails) {
              JsonObject dataObject = new JsonObject();
              for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                String adminName = "";
                if (key.equals("user_id")) {

                  if (Integer.parseInt(value) > 0) {
                    long userId = Long.parseLong(value);
                    EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
                    Employee employee = employeeBO.getEmployeeDetails(userId);
                    if (!StringUtil.printValidString(employee.getProfile_image()).equals("")) {
                      dataObject.put("user_profile_image", employee.getProfile_image());
                    } else {
                      String profileImage = JWTUtil.getProfileImagePath();
                      dataObject.put("user_profile_image", profileImage);
                    }
                    adminName = employee.getFirst_name() + " " + employee.getLast_name();
                    dataObject.put("admin_name", adminName);
                  } else {
                    String profileImage = JWTUtil.getProfileImagePath();
                    dataObject.put("user_profile_image", profileImage);
                    adminName = candidateR.getFirstName() + " " + candidateR.getLastName();
                    dataObject.put("admin_name", adminName);
                  }
                }
                dataObject.put(key, value);
              }
              commentArray.add(dataObject);
            }
            int commentCount = commentDetails.size();
            candidateObject.put("CommentsList", commentArray);
            candidateObject.put("Commentcount", commentCount);

            /*---------------------Custom Question------------------------------------------**/

            List<HashMap<String, String>> getCustomQuestionList = candidateBO.
                getCustomQuestionsList(candidateId);
            JsonArray customQuestion = new JsonArray();
            if (getCustomQuestionList.size() > 0) {
              for (HashMap<String, String> map : getCustomQuestionList) {
                JsonObject dataObject = new JsonObject();
                for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                  String key = mapEntry.getKey();
                  String value = mapEntry.getValue();
                  dataObject.put(key, value);
                }
                customQuestion.add(dataObject);
              }
            }
            candidateObject.put("CustomQuestion", customQuestion);

            /*--------------------------------------------------------------------*/

            if (candidateObject.size() > 0) {
              CandidateHandlerR.writeResponse
                  (httpServerResponse, dbHandle, GET_CANDIDATE_SUCCESS, candidateObject);
            } else {
              CandidateHandlerR.writeResponse
                  (httpServerResponse, dbHandle, candidateStatusEnum.GET_CANDIDATE_FAILED, null);
            }
               /* } else {
                  ResponseHelper.writeErrorResponse(HttpStatus.SC_UNAUTHORIZED, "You Cannot Consumes this Service",
                      "You Cannot Consumes this Service", httpServerResponse);
                }
              } else {
                ResponseHelper.writeErrorResponse(HttpStatus.SC_UNAUTHORIZED, "You Cannot Consumes this Service",
                    "You Cannot Consumes this Service", httpServerResponse);
              }*/
            /*-------------------------end here---------------------------------*/
          } else if (!StringUtil.printValidString(requestParams.get("id")).equals("") &&
              StringUtil.printValidString(requestParams.get("fields")).equals("")) {

            String candidateIdDetails = StringUtil.printValidString(requestParams.get("id"));

            long candidateId = Long.parseLong(candidateIdDetails);

            CandidateR candidateR = candidateBO.getCandidate(dbHandle, candidateId);

            JsonObject candidateObject = new JsonObject();
            candidateObject.put("organization_id", candidateR.getOrganizationId());
            candidateObject.put("first_name", candidateR.getFirstName());
            candidateObject.put("last_name", candidateR.getLastName());
            candidateObject.put("email", candidateR.getEmail());
            candidateObject.put("phone_number", candidateR.getPhoneNumber());
            candidateObject.put("unit", candidateR.getUnit());
            candidateObject.put("source", candidateR.getSource());
            candidateObject.put("address_line", candidateR.getAddressLine());
            candidateObject.put("city", candidateR.getCity());
            candidateObject.put("state", candidateR.getState());
            candidateObject.put("zip", candidateR.getZip());
            candidateObject.put("country", candidateR.getCountry());
            candidateObject.put("job_id", candidateR.getJobId());
            candidateObject.put("rating", candidateR.getRating());
            candidateObject.put("candidate_id", candidateR.getCandidateId());

            if (candidateR.getHiringLeadId() == 0) {
              candidateObject.put("hiring_lead", "-");
            } else {
              candidateObject.put("hiring_lead", candidateR.getHiringLeadId());
            }
            candidateObject.put("internal_jobcode", candidateR.getInternalJobCode());
            candidateObject.put("resume", candidateR.getResume());

            if (candidateR.getJobId() != 0) {

              Job job = jobBO.getJobDetails(candidateR.getJobId());

              candidateObject.put("employment_type", job.getEmployment_type());
              candidateObject.put("location", job.getLocation());
              candidateObject.put("job_status", job.getJob_status());
              candidateObject.put("department", job.getDepartment());
              candidateObject.put("minimum_experience", job.getMinimum_experience());
              candidateObject.put("job_title", job.getTitle());
            } else {
              candidateObject.put("employment_type", "-");
              candidateObject.put("location", "-");
              candidateObject.put("job_status", "-");
              candidateObject.put("department", "-");
              candidateObject.put("minimum_experience", "-");
              candidateObject.put("job_title", "-");
            }

            if (!StringUtil.printValidString(candidateR.getDateAvailable()).equals("")) {
              candidateObject.put("date_available", candidateR.getDateAvailable());
            } else {
              candidateObject.put("date_available", "-");
            }
            candidateObject.put("cover_letter", candidateR.getCoverLetter());
            candidateObject.put("highest_education", candidateR.getHighestEducation());
            candidateObject.put("reference", candidateR.getReference());


            if (!StringUtil.printValidString(candidateR.getDesiredSalary()).equals("")) {
              candidateObject.put("desired_salary", candidateR.getDesiredSalary());
            } else {
              candidateObject.put("desired_salary", "-");
            }
            candidateObject.put("college", candidateR.getCollege());
            candidateObject.put("referred_by", candidateR.getReferredBy());
            candidateObject.put("linkedin_url", candidateR.getLinkedinUrl());
            candidateObject.put("blog", candidateR.getBlog());
            candidateObject.put("company_type_check", candidateR.getCompanyTypeCheck());
            candidateObject.put("gender", candidateR.getGender());
            candidateObject.put("veteran_status", candidateR.getVeteranStatus());
            candidateObject.put("ethnicity", candidateR.getEthnicity());
            candidateObject.put("disability", candidateR.getDisability());

            try {
              String date = new DateUtil().
                  convertDateintoString(candidateR.getCreated());
              candidateObject.put("created_date", new DateUtil().getDateFormat(routingContext, date));
              if (!StringUtil.printValidString(String.valueOf(candidateR.getModified())).equals("")) {
                date = new DateUtil().
                    convertDateintoString(candidateR.getModified());
                candidateObject.put("modified_date", new DateUtil().getDateFormat(routingContext, date));
              }
            } catch (ParseException e) {
              e.printStackTrace();
            }

            if (!StringUtil.printValidString(candidateR.getStatus()).equals("")) {
              CandidateHiringProcessBO candidateHiringProcessBO = new
                  CandidateHiringProcessBOImpl(dbHandle);

              String statusCheck = candidateHiringProcessBO.
                  getStatus(candidateId, candidateR.getStatus());

              String status = candidateR.getStatus() + "^" + statusCheck;
              candidateObject.put("status", status);
            }

            if (candidateObject.size() > 0) {
              CandidateHandlerR.writeResponse
                  (httpServerResponse, dbHandle, GET_CANDIDATE_SUCCESS, candidateObject);
            } else {
              logger.error("getting candidate data with id");
              CandidateHandlerR.writeResponse
                  (httpServerResponse, dbHandle, candidateStatusEnum.GET_CANDIDATE_FAILED, null);
            }
          }
          if (!StringUtil.printValidString(requestParams.get("user_id")).equals("") &&
              !StringUtil.printValidString(requestParams.get("organization_id")).equals("")) {

            String userType = routingContext.data().get("userType").toString();

            long userId = Long.parseLong(requestParams.get("user_id"));
            long organizationId = Long.parseLong(requestParams.get("organization_id"));

            if (!userType.equalsIgnoreCase("candidate")) {
              int loginId = (int) ((Login) routingContext.data().get("login")).getId();
              userId = loginId;
            }

            //  if (userType.equalsIgnoreCase("candidate") && userId == loginId) {

            List<HashMap<String, String>> candidateList = candidateBO.
                getUserId(dbHandle, userId, organizationId);
            JsonArray userArray = new JsonArray();

            for (HashMap<String, String> map : candidateList) {
              JsonObject dataObject = new JsonObject();
              long candidateId = 0;
              for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if (key.equals("candidate_id")) {
                  candidateId = Long.parseLong(value);
                }
                if (key.equals("status")) {
                  CandidateHiringProcessBO candidateHiringProcessBO = new
                      CandidateHiringProcessBOImpl(dbHandle);
                  String candidateProcess = StringUtil.printValidString(candidateHiringProcessBO.
                      getStatus(candidateId, value));
                  String status = "";
                  if (StringUtil.printValidString(candidateProcess).equals("Schedule Interview")) {
                    CandidateInterviewBO interviewBO = new CandidateInterviewBOImpl(dbHandle);
                    InterviewProcessCandidate interviewProcessCandidate = interviewBO.
                        getInterviewDetails(candidateId);
                    if (interviewProcessCandidate != null) {
                      status = StringUtil.printValidString(interviewProcessCandidate.getStatus());
                    }
                  }

                  if (StringUtil.printValidString(candidateProcess).equals("Offer Extended")) {
                    CandidateOfferBO offerBO = new CandidateOfferBOImpl(dbHandle);
                    OfferProcessCandidate offerProcessCandidate = offerBO.getOfferDetails(candidateId);
                    if (offerProcessCandidate != null) {
                      status = StringUtil.printValidString(offerProcessCandidate.getStatus());
                    }
                  }

                  if ("".equals(status)) {
                    status = "none";
                  }

                  value = value + "^" + candidateProcess;
                  dataObject.put("statuscheck", status);
                  dataObject.put(key, value);
                } else {
                  dataObject.put(key, value);
                }
              }
              userArray.add(dataObject);
            }

            JsonObject successObject = new JsonObject();
            successObject.put("code", HttpStatus.SC_OK);
            successObject.put("success", true);
            successObject.put("totalcount", userArray.size());
            successObject.put("no_data", false);
            JsonObject userObject = new JsonObject();
            userObject.put("UserList", userArray);
            successObject.put("data", userObject);

            if (userArray.size() > 0) {
              CandidateHandlerR.writeResponse
                  (httpServerResponse, dbHandle, NO_DATA_STATUS, successObject);
            } else {
              if (userArray.size() == 0) {
                JsonObject emptyArray = new JsonObject();
                JsonArray emptyObject = new JsonArray();
                emptyArray.put("UserList", emptyObject);
                JsonObject jsonObject = new JsonObject();
                jsonObject.put("code", HttpStatus.SC_OK);
                jsonObject.put("success", true);
                jsonObject.put("totalcount", 0);
                jsonObject.put("no_data", true);
                // messageObject.put("message","No Job's Applied Yet!");
                jsonObject.put("data", emptyArray);

                CandidateHandlerR.writeResponse
                    (httpServerResponse, dbHandle, NO_DATA_STATUS, jsonObject);
              } else {
                logger.error("getting candidate data with user id");
                CandidateHandlerR.writeArrayResponse
                    (httpServerResponse, dbHandle, candidateStatusEnum.GET_CANDIDATE_FAILED, null);
              }
            }
           /* }else {
              ResponseHelper.writeErrorResponse(HttpStatus.SC_UNAUTHORIZED, "You Cannot Consumes this Service",
                  "You Cannot Consumes this Service", httpServerResponse);
            }*/
          }

          if (!StringUtil.printValidString(requestParams.get("job_id")).equals("") &&
              !StringUtil.printValidString(requestParams.get("organization_id")).equals("")) {

            Job job = jobBO.getJobDetails(Long.parseLong(requestParams.get("job_id")));
            long jobId = Long.parseLong(requestParams.get("job_id"));
            long organizationId = Long.parseLong(requestParams.get("organization_id"));
          /*  if (userType.equalsIgnoreCase("employee") || userType.equalsIgnoreCase("admin")) {

              int employeeId = ((Employee) routingContext.data().get("employee")).getEmployee_id();

              if (roles.equalsIgnoreCase("Admin") || roles.equalsIgnoreCase("Hiring Lead")
                  || roles.equalsIgnoreCase("Collaborator")) {

                boolean employeeCheck = new EmployeeDAOImpl(dbHandle).isEmployeeExistsInOrganization(employeeId,organizationId);

                String hiringLeadId = job.getHiring_lead();

                if (!StringUtils.isNullOrEmpty(hiringLeadId)) {
                  hiringLead = Integer.parseInt(hiringLeadId);
                }

                //checking either admin and hiring lead for that job can cancel the interview.

                if ((employeeCheck == true && userType.equalsIgnoreCase("admin")) ||
                    (hiringLead == employeeId && userType.equalsIgnoreCase("employee"))) {
*/
            List<HashMap<String, String>> candidateList = candidateBO.
                getJobId(dbHandle, jobId, organizationId);

            JsonArray userArray = new JsonArray();
            long candidateId = 0;
            String rejectReason = "";
            String adminCancelledReason = "";
            String offerReject = "";
            String adminReason = "";

            for (HashMap<String, String> map : candidateList) {
              JsonObject dataObject = new JsonObject();
              for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();

                if (key.equals("candidate_id")) {
                  candidateId = Long.parseLong(value);
                }
                if (key.equals("status")) {

                  if (value.equalsIgnoreCase("Interview")) {
                    CandidateInterviewBO interviewBO = new CandidateInterviewBOImpl(dbHandle);
                    InterviewProcessCandidate interviewProcessCandidate = interviewBO.
                        getInterviewDetails(candidateId);
                    if (interviewProcessCandidate != null) {
                      rejectReason = StringUtil.
                          printValidString(interviewProcessCandidate.getStatus());
                      adminCancelledReason = StringUtil.
                          printValidString(interviewProcessCandidate.getInterviewCancelReason());

                    }
                  } else if (value.equalsIgnoreCase("Offer")) {
                    CandidateOfferBO offerBO = new CandidateOfferBOImpl(dbHandle);
                    OfferProcessCandidate offerProcessCandidate = offerBO.
                        getOfferDetails(candidateId);
                    if (offerProcessCandidate != null) {
                      rejectReason = StringUtil.
                          printValidString(offerProcessCandidate.getStatus());
                      offerReject = StringUtil.
                          printValidString(offerProcessCandidate.getOfferDeclineReason());
                    }
                  }

                  if ("".equals(rejectReason) || rejectReason.length() == 0) {
                    rejectReason = "-";
                  } else {
                    if (rejectReason.equals("accept")) {
                      rejectReason = "Accepted by Candidate";
                    } else {
                      rejectReason = "Rejected by Candidate";
                    }
                  }
                  // offer admin reject or accept
                  if (!"".equals(offerReject) || offerReject.length() > 0) {
                    adminReason = "Rejected by Admin";
                  }
                  // interview admin reject or accept
                  if (!"".equals(adminCancelledReason) || adminCancelledReason.length() > 0) {
                    adminReason = "Cancelled by Admin";
                  }
                  if (!adminReason.equals("") || adminReason.length() > 0) {
                    rejectReason = adminReason;
                  }
                  dataObject.put("reject_reason", rejectReason);
                }
                dataObject.put(key, value);
              }
              userArray.add(dataObject);
            }

            JsonObject successObject = new JsonObject();
            successObject.put("totalcount", userArray.size());
            successObject.put("JobIdList", userArray);
//            successObject.put("no_data", false);

            if (userArray.size() > 0) {
              CandidateHandlerR.writeResponse
                  (httpServerResponse, dbHandle, GET_CANDIDATE_SUCCESS, successObject);
              logger.info("Candidate Fetched Successfully!");
            } else {
              logger.error("getting candidate data with job id");
              JsonObject emptyArray = new JsonObject();
              JsonArray emptyObject = new JsonArray();
              emptyArray.put("JobIdList", emptyObject);
              JsonObject jsonObject = new JsonObject();
              jsonObject.put("code", HttpStatus.SC_OK);
              jsonObject.put("success", true);
              jsonObject.put("totalcount", 0);
              jsonObject.put("no_data", true);
              // messageObject.put("message","Currently no candidate available yet!");
              jsonObject.put("data", emptyArray);
              CandidateHandlerR.writeResponse
                  (httpServerResponse, dbHandle, NO_DATA_STATUS, jsonObject);
              logger.info("No Candidate Available in the System with the mentioned job id");

            }
          }
            /*  }else {
                ResponseHelper.writeErrorResponse(HttpStatus.SC_UNAUTHORIZED, "You Cannot Consumes this Service",
                    "You Cannot Consumes this Service", httpServerResponse);
              }
            }else {
              ResponseHelper.writeErrorResponse(HttpStatus.SC_UNAUTHORIZED, "You Cannot Consumes this Service",
                  "You Cannot Consumes this Service", httpServerResponse);
            }
          }else {
            ResponseHelper.writeErrorResponse(HttpStatus.SC_UNAUTHORIZED, "You Cannot Consumes this Service",
                "You Cannot Consumes this Service", httpServerResponse);
          }*/
        });
  }

  private static JsonObject getSampleData(Organization organization) {
    JsonObject dumpData = new JsonObject();
    dumpData.put("organization_id", organization.getOrganization_id());
    dumpData.put("first_name", "John");
    dumpData.put("last_name", "Smith");
    dumpData.put("phone_number", "98852322222");
    dumpData.put("email", "thiru.aztest@gmail.com");
    dumpData.put("unit", "0");
    dumpData.put("source", "Direct");
    dumpData.put("address_line", "-");
    dumpData.put("city", "-");
    dumpData.put("state", "-");
    dumpData.put("zip", "-");
    dumpData.put("country", "-");
    dumpData.put("job_id", 0);
    dumpData.put("rating", 0);
    dumpData.put("candidate_id", 0);
    dumpData.put("hiring_lead", "-");
    dumpData.put("internal_jobcode", "");
    dumpData.put("resume", "static/img/null/2e6a0567-593e-429e-9d95-e8a7b81f5e67.pdf");
    dumpData.put("employment_type", "");
    dumpData.put("location", "New Delhi");
    dumpData.put("job_status", "Published");
    dumpData.put("department", "");
    dumpData.put("minimum_experience", "");
    dumpData.put("job_title", "Software Engineer");
    dumpData.put("date_available", "-");
    dumpData.put("cover_letter", "");
    dumpData.put("highest_education", "");
    dumpData.put("reference", "");
    dumpData.put("desired_salary", "-");
    dumpData.put("college", "");
    dumpData.put("referred_by", "");
    dumpData.put("linkedin_url", "");
    dumpData.put("blog", "");
    dumpData.put("company_type_check", "");
    dumpData.put("gender", "");
    dumpData.put("veteran_status", "");
    dumpData.put("ethnicity", "");
    dumpData.put("disability", "");
    dumpData.put("modified_date", "04/23/2018");
    dumpData.put("created_date", "04/23/2018");
    dumpData.put("status", "applicant^New");
    dumpData.put("CustomQuestion", new JsonArray());
    JsonObject hiringProcessObject = new JsonObject();
    JsonObject offerObject = new JsonObject();
    offerObject.put("offer", "Offer Extended");
    offerObject.put("status", false);
    List<String> notHiredList = new ArrayList<>();
    notHiredList.add("Archive to Talent Pool");
    notHiredList.add("Declined offer");
    notHiredList.add("Not Qualified / Knockout");
    notHiredList.add("Over Qualified");
    notHiredList.add("Took another position");
    notHiredList.add("Blacklisted");

    JsonArray notHiredArray = new JsonArray();
    for (String notHired : notHiredList) {
      notHiredArray.add(new JsonObject().put("not_hired", notHired)
          .put("status", false));
    }

    hiringProcessObject.put("offer", new JsonArray().add(offerObject));
    hiringProcessObject.put("not_hired", notHiredArray);
    hiringProcessObject.put("screening", new JsonArray().add(
        new JsonObject().put("screening", "Screening").put("status", false)
    ));
    hiringProcessObject.put("hire", new JsonArray().add(
        new JsonObject().put("hire", "Hire Candidate").put("status", false)
    ));
    hiringProcessObject.put("interview", new JsonArray().add(
        new JsonObject().put("interview", "Schedule Interview").put("status", false)
    ));
    hiringProcessObject.put("applicant", new JsonArray().add(
        new JsonObject().put("applicant", "New").put("status", true)
    ));
    JsonArray emailArray = new JsonArray();
    JsonObject emailObject = new JsonObject();
    emailObject.put("user_profile_image", "");
    emailObject.put("admin_name", "Administrator");
    emailObject.put("attachment_name", "[]");
    emailObject.put("subject_name", "Re:Applied Job");
    emailObject.put("mail_description", "<html><body>Hello <b>John Smith</b>, <br> <br>" +
        "<p>Thank you for Applying for the position of  sql dev.</p> <br><br> <br>Thank you<br>" +
        "<a href=\\\"mailto:support@auzmor.com\\\">support@auzmor.com</a><br>  </body></html>");
    emailObject.put("created_date", "04/23/2018 at 07:04");
    emailArray.add(emailObject);

    dumpData.put("cand_status", hiringProcessObject);
    dumpData.put("EmailList", emailArray);
    dumpData.put("Emailcount", emailArray.size());

    JsonArray commentsArray = new JsonArray();
    JsonObject commentsObject = new JsonObject();
    commentsObject.put("comments", "moved from New to New");
    commentsObject.put("user_profile_image", "");
    commentsObject.put("admin_name", "Administrator");
    commentsObject.put("user_id", "0");
    commentsObject.put("id", "1");
    commentsObject.put("comments_date", "04/23/2018 at 07:04");
    commentsArray.add(commentsObject);

    commentsObject = new JsonObject();
    commentsObject.put("comments", "moved from No Status to New Applicant");
    commentsObject.put("user_profile_image", "");
    commentsObject.put("admin_name", "Administrator");
    commentsObject.put("user_id", "0");
    commentsObject.put("id", "1");
    commentsObject.put("comments_date", "04/23/2018 at 05:40");
    commentsArray.add(commentsObject);

    dumpData.put("CommentsList", commentsArray);
    dumpData.put("Commentcount", commentsArray.size());

    logger.info("Sample Data Fetched Successfully!");
    return dumpData;

  }

  public static void talentPoolList(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"organization_id", "limit", "offset", "searchFilter"};
    String[] requiredParams = {"organization_id"};
    String[] candidateParams = {"organization_id", "limit", "offset", "searchFilter"};

    List<Validator> validators = new ArrayList<>();
    Validator candidateValidator = new ValidatorImpl(candidateParams, CandidateR.getValidatorMap());
    validators.add(candidateValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          CandidateBO candidateBO = new CandidateBOImpl(dbHandle);
          int limit = 0;
          int offset = 0;
          String stateId = "";
          String searchFilter = requestParams.get("searchFilter");
          if (!StringUtils.isNullOrEmpty(requestParams.get("limit"))) {
            limit = Integer.parseInt(requestParams.get("limit"));
          }
          if (!StringUtils.isNullOrEmpty(requestParams.get("offset"))) {
            offset = Integer.parseInt(requestParams.get("offset"));
          }
         /* if (!StringUtils.isNullOrEmpty(requestParams.get("location"))) {
            location = requestParams.get("location");
          //  stateId = CountryStateHandler.getStateIdWithLikeCondition(location);
            selectFilter.append(" and Candidate.job_id in (select id from Job where state = '" + location + "' ) ");
          }

          if (!StringUtils.isNullOrEmpty(requestParams.get("position"))) {
            position = requestParams.get("offset");
            selectFilter.append(" and Candidate.job_id in (select id from Job where title = '" + position + "' ) ");
          }*/

          if (!StringUtils.isNullOrEmpty(searchFilter)) {
            searchFilter = searchFilter.toUpperCase();
            // searching state id from state name
            stateId = CountryStateHandler.getStateIdWithLikeCondition(searchFilter);

           /* selectFilter.append(" and ((UPPER(Candidate.first_name) like '%" + searchFilter + "%' or UPPER(");
            selectFilter.append("Candidate.last_name) like '%" + searchFilter);
            selectFilter.append("%' or UPPER(CONCAT(Candidate.first_name, ' ', Candidate.last_name)) like '%");
            selectFilter.append(searchFilter + "%')  or ");
            selectFilter.append(" (Candidate.job_id in (select id from Job where (UPPER(Job.title) like '%" + searchFilter + "%'))) ");
            if (!StringUtils.isNullOrEmpty(stateId)) {
              selectFilter.append(" or (UPPER(Candidate.state) in (" + stateId + "))");
            }
            selectFilter.append(")");*/
          }

          String format = ((Organization) routingContext.data().get("organization")).getDate_format();

          List<HashMap<String, String>> talentList = candidateBO.
              getCandidateTalentList(Long.parseLong(requestParams.get("organization_id")),
                  format, searchFilter, limit, offset, stateId);
          List<HashMap<String, String>> talentListCount = candidateBO.
              getCandidateTalentList(Long.parseLong(requestParams.get("organization_id")), format,
                  searchFilter, 0, 0, stateId);

          int totalCount = talentListCount.size();
          if (talentList.size() > 0) {
            JsonObject successObject = new JsonObject();
            JsonArray talentPool = new JsonArray();
            for (HashMap<String, String> map : talentList) {
              JsonObject dataObject = new JsonObject();
              for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                if (key.equals("job_id")) {
                  JobBO jobBO = new JobBOImpl(dbHandle);
                  Job job = jobBO.getJobDetailsWithoutStatus(Long.parseLong(value));

                  if (job != null) {
                    dataObject.put("job_title", job.getTitle());
                  } else {
                    dataObject.put("job_title", "-");
                  }
                }
                if ("state".equalsIgnoreCase(key)) {
                  String state = value;
                  if (!StringUtils.isNullOrEmpty(state)) {
                    String stateName = CountryStateHandler.getStateName(state);
                    dataObject.put("state", stateName);
                  } else {
                    dataObject.put("state", "");
                  }
                } else {
                  dataObject.put(key, value);
                }
              }
              talentPool.add(dataObject);
            }
            successObject.put("totalCount", totalCount);
            successObject.put("talentPoolList", talentPool);
            successObject.put("no_data", false);
            CandidateHandlerR.writeResponse
                (httpServerResponse, dbHandle, GET_CANDIDATE_SUCCESS, successObject);
          } else {

            long organizationId = Long.parseLong(requestParams.get("organization_id"));
            Organization organization = (Organization) new OrganizationDAOImpl(dbHandle)
                .getOrganizationById(organizationId);
            boolean doesOrganizationExists = (organization != null);
            int jobCount = Integer.parseInt(new DatabaseUtil().
                getTablename("Job", "count(1)",
                    "organization_id =" + organizationId));
            if (doesOrganizationExists && jobCount == 0) {
              if (jobCount == 0) {
                JsonObject talentPoolList = new JsonObject();
                talentPoolList.put("candidate_id", "0");
                talentPoolList.put("job_title", "Software Engineer");
                talentPoolList.put("job_id", "0");
                talentPoolList.put("rating", 0);
                talentPoolList.put("last_name", "Smith");
                talentPoolList.put("phone_number", "2984104199595");
                talentPoolList.put("created_date", "23-Apr-18");
                talentPoolList.put("modified_date", "23-Apr-18");
                talentPoolList.put("first_name", "John");
                talentPoolList.put("email", "thiru.aztest1@gmail.com");
                talentPoolList.put("status", "Not Hired");
                JsonObject dataObject = new JsonObject();
                dataObject.put("talentPoolList", new JsonArray().add(talentPoolList));
                dataObject.put("totalCount", 1);
                dataObject.put("no_data", false);

                CandidateHandlerR.writeResponse
                    (httpServerResponse, dbHandle, GET_CANDIDATE_SUCCESS, dataObject);
                logger.info("Talent pool list fetched Successfully");

              }
            } else {
              JsonObject successObject = new JsonObject();
              JsonArray jsonObject = new JsonArray();
              JsonObject jsonObjectData = new JsonObject();
              jsonObjectData.put("talentPoolList", jsonObject);
              jsonObjectData.put("totalCount", 0);
              successObject.put("code", HttpStatus.SC_OK);
              successObject.put("success", true);
              // successObject.put("totalCount", 0);
              successObject.put("data", jsonObjectData);
              successObject.put("no_data", true);
              CandidateHandlerR.writeResponse
                  (httpServerResponse, dbHandle, NO_DATA_STATUS, successObject);
              logger.error("Fetching Talent pool list failed");
            }
          }
        });
  }

  public static void deleteApplicant(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"id"};
    String[] requiredParams = {"id"};
    String[] candidateParams = {"id"};

    List<Validator> validators = new ArrayList<>();
    Validator inviteValidator = new ValidatorImpl(candidateParams, CandidateR.getValidatorMap());
    validators.add(inviteValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          CandidateBO candidateBO = new CandidateBOImpl(dbHandle);
          CandidateStatusEnum candidateStatusEnum = candidateBO.
              deleteCandidate(Long.parseLong(requestParams.get("id")));

          if (candidateStatusEnum == CANDIDATE_DELETED_SUCESSFULLY) {
            JsonObject dataObject = new JsonObject();
            dataObject.put("message", "Candidate Record Deleted Successfully");

            CandidateHandlerR.writeResponse
                (httpServerResponse, dbHandle, CANDIDATE_DELETED_SUCESSFULLY, dataObject);
            logger.info("Candidate Successfully Deleted");
          } else {
            CandidateHandlerR.writeResponse
                (httpServerResponse, dbHandle, candidateStatusEnum.CANDIDATE_DELETED_FAILED, null);
            logger.error("Candidate Deletion Failed");
          }
        });
  }

  public static void getPostJobDetails(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"organization_id"};
    String[] requiredParams = {"organization_id"};
    String[] candidateParams = {"organization_id"};

    List<Validator> validators = new ArrayList<>();
    Validator inviteValidator = new ValidatorImpl(candidateParams, CandidateR.getValidatorMap());
    validators.add(inviteValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          long organizationId = Long.parseLong(requestParams.get("organization_id"));
          CandidateBO candidateBO = new CandidateBOImpl(dbHandle);
          List<HashMap<String, String>> filterDetails = candidateBO.
              getPostJobFilterDetails(dbHandle, organizationId);
          JsonArray filterArray = new JsonArray();
          JsonObject dataObject = new JsonObject();
          for (HashMap<String, String> map : filterDetails) {
            for (Map.Entry<String, String> mapEntry : map.entrySet()) {
              String key = mapEntry.getKey();
              String value = mapEntry.getValue();
              dataObject.put(key, value);
            }
          }
          if (dataObject.size() > 0) {
            Organization organization = new OrganizationDAOImpl(dbHandle).
                getOrganizationById(organizationId);
            int jobCount = Integer.parseInt(new DatabaseUtil().
                getTablename("Job", "count(1)",
                    "organization_id =" + organizationId));
            boolean isOrganizationAvailable = (organization != null);
            if (isOrganizationAvailable && jobCount == 0) {
              dataObject.put("employmentType", "");
              dataObject.put("minimumExp", "7 years");
              dataObject.put("jobTitle", "Software Engineer");
              dataObject.put("location", "California");
              dataObject.put("department", "Information Technology");
              dataObject.put("HiringLead", "1");
              filterArray.add(dataObject);
              CandidateHandlerR.writeArrayResponse
                  (httpServerResponse, dbHandle, GET_CANDIDATE_SUCCESS, filterArray);
              logger.info("Post Job Details fetched Successfully");
            } else {
              filterArray.add(dataObject);
              CandidateHandlerR.writeArrayResponse
                  (httpServerResponse, dbHandle, GET_CANDIDATE_SUCCESS, filterArray);
              logger.info("Fetching Post Job Details Failed");
            }
          } else {
            CandidateHandlerR.writeArrayResponse
                (httpServerResponse, dbHandle, GET_CANDIDATE_FAILED, null);
            logger.error("Fetching Post Job Details Failed");
          }
        });
  }

  /**
   * @param routingContext
   * @author sadhana
   */
  public static void updateCandidateId(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"first_name", "last_name", "email", "phone_number",
        " unit", "source", "address_line", "city", "state", "zip", "country", "gender",
        " hiring_lead", "internal_jobcode", "employment_type", "location", "job_title", "job_id",
        " user_id", "department", "minimum_experience", "job_status", "resume", "cover_letter",
        " highest_education", "desired_salary", "linkedin_url", "date_available", "blog",
        " reference", "referred_by", "college", "company_type_check", "veteran_status",
        " ethnicity", "disability", "organization_id", "candidate_id"};

    String[] requiredParams = {"candidateId"};
    String[] candidateParams = {"first_name", "last_name", "email", "phone_number",
        "unit", "source", "address_line", "city", "state", "zip", "country", "gender",
        "hiring_lead", "internal_jobcode", "employment_type", "location", "job_title", "job_id",
        "user_id", "department", "minimum_experience", "job_status", "resume", "cover_letter",
        "highest_education", "desired_salary", "linkedin_url", "date_available", "blog",
        "reference", "referred_by", "college", "company_type_check", "veteran_status",
        "ethnicity", "disability", "organization_id", "candidate_id"};

    List<Validator> validators = new ArrayList<>();
    Validator candidateValidator = new ValidatorImpl(candidateParams, CandidateR.getValidatorMap());
    validators.add(candidateValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          CandidateStatusEnum candidateStatusEnum;
          CandidateBO candidateBO = new CandidateBOImpl(dbHandle);
          String dateFormat = ((Organization) routingContext.data().get("organization")).getDate_format();
          candidateStatusEnum = candidateBO.updateCandidate(dateFormat, requestParams);
          if (candidateStatusEnum == UPDATE_SUCCESS) {
            JsonObject dataObject = new JsonObject();
            dataObject.put("message", "Candidate Updated Successfully");
            logger.info("Candidate Updated Successfully");
            CandidateHandlerR.writeResponse
                (httpServerResponse, dbHandle, UPDATE_SUCCESS, dataObject);
          } else {
            logger.error("Candidate Update Failed");
            CandidateHandlerR.writeResponse
                (httpServerResponse, dbHandle, UPDATE_FAILED, null);
          }
        });
  }

  /**
   * @param routingContext
   * @author sadhanaVenkatesan
   */
  public static void updateCandidate(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"notes", "employee_id", "rating", "job_id", "candidateId", "creator_id"};
    String[] requiredParams = {"candidateId"};
    String[] candidateParams = {"notes", "employee_id", "rating", "job_id", "candidateId", "creator_id"};

    List<Validator> validators = new ArrayList<>();
    Validator inviteValidator = new ValidatorImpl(candidateParams, CandidateR.getValidatorMap());
    validators.add(inviteValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }
    try {
      DbHelper.safeExecuteDbHandleMethod(routingContext, false,
          (httpServerResponse, requestParams, dbHandle) -> {
            CandidateStatusEnum candidateStatusEnum;
            CandidateBO candidateBO = new CandidateBOImpl(dbHandle);
            long candId = 0;
            long userId = 0;
            String candidateId = "";

            /*if (!StringUtil.printValidString(requestParams.get("candidate_id")).contains(",")) {
              candidateId = requestParams.get("candidate_id");
            } else {*/
            candId = Long.parseLong(requestParams.get("candidateId"));
            /*}*/
            if (!StringUtil.printValidString(requestParams.get("creator_id")).equals("")) {
              userId = Long.parseLong(requestParams.get("creator_id"));
            }

            if (!StringUtil.printValidString(requestParams.get("rating")).equals("")) {
              int rating = Integer.parseInt(requestParams.get("rating"));
              candidateStatusEnum = candidateBO.updateRating(rating, candId, userId);
              if (candidateStatusEnum == STATUS_SUCCESS) {
                JsonObject dataObject = new JsonObject();
                dataObject.put("message", "Candidate rating updated successfully");
                CandidateHandlerR.writeResponse
                    (httpServerResponse, dbHandle, candidateStatusEnum.STATUS_SUCCESS, dataObject);
              } else {
                CandidateHandlerR.writeResponse
                    (httpServerResponse, dbHandle, candidateStatusEnum.STATUS_FAILED, null);
              }

            } else if (!StringUtil.printValidString(requestParams.get("notes")).equals("") &&
                !StringUtil.printValidString(requestParams.get("employee_id")).equals("")) {

              long employeeId = Long.parseLong(requestParams.get("employee_id"));
              String notes = requestParams.get("notes");
              candidateId = requestParams.get("candidateId");
              candidateStatusEnum = candidateBO.updateForwardEmployee(employeeId, notes,
                  candidateId, userId);
              if (candidateStatusEnum == FORWARD_SUCCESS) {
                JsonObject dataObject = new JsonObject();
                dataObject.put("message", "Candidate Forward Successfully!");
                logger.info("Candidate Forward Successfully!");
                CandidateHandlerR.writeResponse
                    (httpServerResponse, dbHandle, candidateStatusEnum.FORWARD_SUCCESS, dataObject);
              } else {
                CandidateHandlerR.writeResponse
                    (httpServerResponse, dbHandle, candidateStatusEnum.FORWARD_FAILED, null);
                logger.error("Candidate Forward Failed!");
              }
            } else if (!StringUtil.printValidString(requestParams.get("job_id")).equals("")) {
              long jobId = Long.parseLong(requestParams.get("job_id"));
              candId = Long.parseLong(requestParams.get("candidateId"));
              candidateStatusEnum = candidateBO.updateMoveCandidate(jobId, candId, userId);
              if (candidateStatusEnum == MOVE_SUCCESS) {
                JsonObject dataObject = new JsonObject();
                dataObject.put("message", "Candidate Moved Successfully!");
                CandidateHandlerR.writeResponse
                    (httpServerResponse, dbHandle, candidateStatusEnum.MOVE_SUCCESS, dataObject);
                logger.info("Candidate Moved Successfully!");
              } else {
                CandidateHandlerR.writeResponse
                    (httpServerResponse, dbHandle, candidateStatusEnum.MOVE_FAILED, null);
                logger.error("Candidate Move Failed!");
              }
            }
          });
    } catch (Exception e) {
      logger.error(e.getStackTrace().toString());
    }
  }

  /**
   * @param routingContext
   * @author sadhaanaVnekatesan
   */
  public static void getCandidateList(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"candidatestatus", "limit", "offset", "organization_id", "searchFilter",
        "collaborator_id", "hiring_lead"};
    String[] requiredParams = {"organization_id"};
    String[] candidateParams = {"candidatestatus", "limit", "offset", "organization_id", "searchFilter"
        , "collaborator_id", "hiring_lead"};

    List<Validator> validators = new ArrayList<>();
    Validator inviteValidator = new ValidatorImpl(candidateParams, CandidateR.getValidatorMap());
    validators.add(inviteValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          CandidateBO candidateBO = new CandidateBOImpl(dbHandle);
          int id = 0;
          String dateFormat = ((Organization) routingContext.data().get("organization")).getDate_format();
          if (params.get("job_id") != null) {
            id = Integer.parseInt(params.get("job_id"));
          }
          if (id > 0) {
            UserTypeEnum userType = (UserTypeEnum) (routingContext.data().get("userType"));

            if (userType != UserTypeEnum.ADMIN) {
              Employee employee = (Employee) routingContext.data().get("employee");
              JobBO jobBO = new JobBOImpl(dbHandle);
              Job jobDetails = jobBO.getJobDetails((long) id);
              if (Long.parseLong(jobDetails.getHiring_lead()) != employee.getEmployee_id()) {
                if (new Collaborator().isNotJobCollaborator(dbHandle, employee.getEmployee_id(), id)) {
                  ResponseHelper.writeErrorResponse(org.apache.commons.httpclient.HttpStatus.SC_FORBIDDEN, "cannot consume this service",
                      "cannot consume this service", routingContext.response());
                  return;
                }
              }
            }
          }

          JsonObject getCandidateDetails = candidateBO.
              getCandidateList(dateFormat, requestParams);
          if (getCandidateDetails.size() > 0) {
            CandidateHandlerR.writeResponse
                (httpServerResponse, dbHandle, NO_DATA_STATUS, getCandidateDetails);
            logger.info("Candidate list fetched Successfully!");
          } else {
            CandidateHandlerR.writeArrayResponse
                (httpServerResponse, dbHandle, FAILED, null);
            logger.error("Candidate list fetching Failed!");
          }
        });
  }

  /**
   * @param routingContext
   * @author sadhanaVenkatesan
   */
  public static void addCandidateResume(RoutingContext routingContext) {
    try {
      Set<FileUpload> fileUploadSet = routingContext.fileUploads();
      for (FileUpload fileUpload : fileUploadSet) {
        FileTypeEnum fileTypeEnum = FileHelper.getFileType(fileUpload);
        long fileSize = fileUpload.size() / 1024 / 1024;
        if (fileTypeEnum != null && FileUploadHelper.isSupportedDocFormat(fileTypeEnum)) {
          if (fileSize < 10) {
            try {
              String sourceFile = fileUpload.uploadedFileName();
              String destinationFile = fileUpload.uploadedFileName().split(File.separator)[1] +
                  fileTypeEnum.getExtension();

              String destinationFolderPath = EndpointConstant.publicStaticCandidateFile + File.separator +
                  routingContext.request().getParam("organization_id");
              /*if (FileHelper.isFileNameExists(destinationFolderPath,destinationFile))
              {
                ResponseHelper.
                    writeStringErrorResponse("File already exists"
                        , routingContext.response());
                logger.error("File already exist in the path!");
              }*/

              FileHelper.move(sourceFile, destinationFolderPath, destinationFile);
              JsonObject obj = new JsonObject();
              obj.put("resume", destinationFolderPath + File.separator + destinationFile);
              obj.put("resume_name", fileUpload.fileName());

              HttpServerResponse response = routingContext.response();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK, "", obj, response);


              //routingContext.response()
              //  .setStatusCode(HttpStatus.SC_CREATED)  // 201
              //.putHeader("Content-Type", "application/json; charset=utf-8")
              //.end(obj);
              logger.info("Candidate Resume added Successfully!");
            } catch (Exception e) {
              e.printStackTrace();
            }
          } else {
            ResponseHelper.
                writeStringErrorResponse("Large File Size, Attach file with less Size"
                    , routingContext.response());
            logger.error("Large File Size Attach file with less Size!");
          }
        } else {
          ResponseHelper.
              writeStringErrorResponse("Unsupported File Format, " +
                  "Please upload a document of format Pdf,doc ,docx", routingContext.response());
          logger.error("Please upload a document of format Pdf,doc ,docx");
        }
      }
    } catch (IllegalArgumentException e) {
      e.printStackTrace();
    }
  }


  public static void addCandidateCoverLetter(RoutingContext routingContext) {
    Set<FileUpload> fileUploadSet = routingContext.fileUploads();
    for (FileUpload fileUpload : fileUploadSet) {
      FileTypeEnum fileTypeEnum = FileHelper.getFileType(fileUpload);
      if (fileTypeEnum != null && FileUploadHelper.isSupportedDocFormat(fileTypeEnum)) {
        String sourceFile = fileUpload.uploadedFileName();
        String destinationFile = fileUpload.uploadedFileName().split(File.separator)[1] +
            fileTypeEnum.getExtension();
        String destinationFolderPath = EndpointConstant.publicStaticCandidateFile + File.separator +
            routingContext.request().getParam("organization_id");

        FileHelper.move(sourceFile, destinationFolderPath, destinationFile);
        routingContext.response()
            .setStatusCode(HttpStatus.SC_CREATED)  // 201
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .end(destinationFolderPath + File.separator + destinationFile);
        logger.info("Candidate Cover Letter added Successfully!");
      } else {
        logger.error("You can only upload PDF/DOC/DOCX file!");
        ResponseHelper.
            writeStringErrorResponse("Unsupported File Format, " +
                "Please upload a document of format Pdf,doc,docx", routingContext.response());
      }
    }
  }

  public static void addCandidateProfileImage(RoutingContext routingContext) {

    Set<FileUpload> fileUploadSet = routingContext.fileUploads();
    for (FileUpload fileUpload : fileUploadSet) {
      FileTypeEnum fileTypeEnum = FileHelper.getFileType(fileUpload);
      try {
        if (FileUploadHelper.isValidProfileImage(fileUpload, fileTypeEnum)) {
          String sourceFile = fileUpload.uploadedFileName();
          String destinationFile = fileUpload.uploadedFileName().split(File.separator)[1] +
              fileTypeEnum.getExtension();
          String destinationFolderPath = EndpointConstant.publicStaticImage + File.separator +
              routingContext.request().getParam("organization_id");

          FileHelper.move(sourceFile, destinationFolderPath, destinationFile);
          routingContext.response()
              .setStatusCode(HttpStatus.SC_CREATED)  // 201
              .putHeader("Content-Type", "application/json; charset=utf-8")
              .end(destinationFolderPath + File.separator + destinationFile);
          logger.info("Profile image added Successfully!");
        } else {
          logger.error("You can only upload PNG/JPEG images!");
          ResponseHelper.writeInternalServerError(routingContext.response());
        }
      } catch (IOException e) {
        logger.error("You can only upload PNG/JPEG images!");
        ResponseHelper.writeInternalServerError(routingContext.response());
      }
    }
  }


  /**
   * getCandidateListBasedOnStatus Get Info of List of Candidates Based on Candidate Hiring Status
   *
   * @param routingContext
   * @author Pradeep Balasubramanian
   * @lastModifiedDate 19-MAR-2018
   */

  public static void getCandidateListBasedOnStatus(RoutingContext routingContext) {
    HttpServerRequest httpServerRequest = routingContext.request();
    HttpServerResponse response = routingContext.response();

    MultiMap params = httpServerRequest.params();
    String[] allowedParams = {"organization_id", "status", "limit", "offset"};
    String[] requiredParams = {"organization_id"};
    String[] validationParams = {"organization_id", "status", "limit", "offset"};
    List<Validator> validators = new ArrayList<>();

    Map<String, FieldOptions> validatorMap = new HashMap<>();
    validatorMap.put("organization_id", FieldOptionsCommon.NUMBER);
    validatorMap.put("status", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 80));
    validatorMap.put("limit", FieldOptionsCommon.NUMBER);
    validatorMap.put("offset", FieldOptionsCommon.NUMBER);

    Validator candidateValidator = new ValidatorImpl(validationParams, validatorMap);
    validators.add(candidateValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams,
        requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          CandidateR candidateModel = new CandidateR();
          String format = ((Organization) routingContext.data().get("organization")).getDate_format();
          candidateModel.setOrganizationId(Long.parseLong(requestParams.get("organization_id")));
          String status = (requestParams.get("status"));
          candidateModel.setStatus(status);
          requestParams.add("format", format);

          CandidateBO candidateBO = new CandidateBOImpl(dbHandle);
          ReturnObject<CandidateStatusEnum, JsonObject> boResponse = candidateBO.
              getCandidateListBasedOnStatus(dbHandle, candidateModel, requestParams);

          switch (boResponse.getStatusEnum()) {
            case GET_CANDIDATE_SUCCESS:
              dbHandle.commit();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  "", boResponse.getData(), boResponse.getData().isEmpty(), httpServerResponse);
              logger.info("Candidates Successfully Fetched");
              break;
            case CANDIDATE_STATUS_INVALID:
              dbHandle.rollback();
              ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
                  "Invalid Request", "Invalid Candidate Status",
                  httpServerResponse);
              logger.info("Candidates Fetching Failed");
              break;
          }

        });
  }

  /*
    This Handler is used for retrieving all the resumes of a candidate
   */
  public static void getAllResumes(RoutingContext routingContext) {
    HttpServerRequest httpServerRequest = routingContext.request();
    HttpServerResponse response = routingContext.response();

    MultiMap params = httpServerRequest.params();
    String[] allowedParams = {"userid"};
    String[] requiredParams = {"userid"};
    String[] validationParams = {"userid"};
    List<Validator> validators = new ArrayList<>();

    Map<String, FieldOptions> validatorMap = new HashMap<>();
    validatorMap.put("userid", FieldOptionsCommon.NUMBER);

    Validator candidateValidator = new ValidatorImpl(validationParams, validatorMap);
    validators.add(candidateValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams,
        requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          CandidateR candidateModel = new CandidateR();
          long loginId = ((Login) routingContext.data().get("login")).getId();
          candidateModel.setUserId(loginId);
          long organizationId = ((Organization) routingContext.data().get("organization")).getOrganization_id();
          candidateModel.setOrganizationId(organizationId);
          CandidateBO candidateBO = new CandidateBOImpl(dbHandle);
          ReturnObject<CandidateStatusEnum, JsonArray> boResponse = candidateBO.
              getAllResumes(dbHandle, candidateModel);
          switch (boResponse.getStatusEnum()) {
            case LIST_AVAILABLE_RESUMES:
              dbHandle.commit();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  "", boResponse.getData(), httpServerResponse);
              logger.info("List of Resumes fetched Successfully");
              break;
            case NO_RESUME:
              dbHandle.rollback();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  "no data is true", new JsonArray(),
                  httpServerResponse);
              logger.info("No Resumes is available to fetch");
              break;
            default:
              dbHandle.rollback();
              ResponseHelper.writeInternalServerError(httpServerResponse);
              logger.info("Fetching list of Resumes Failed");
              break;
          }
        });
  }

  /**
   * Deletes the temporary stored files in the server
   *
   * @param routingContext
   */
  public static void deleteDocument(RoutingContext routingContext) {
    HttpServerRequest httpServerRequest = routingContext.request();
    HttpServerResponse httpServerResponse = routingContext.response();
    /*if (null != routingContext.request().getHeader("X-Consumer-Custom-ID")) {
      ResponseHelper.writeErrorResponse(HttpStatus.SC_FORBIDDEN, "Unautorized for the operation",
          "Unauthorized for the operation", httpServerResponse);
      return;
    }*/

    MultiMap params = httpServerRequest.params();
    String[] allowedParams = {"filePath"};
    String[] requiredParams = {"filePath"};
    String[] validationParams = {"filePath"};
    List<Validator> validators = new ArrayList<>();

    Map<String, FieldOptions> validatorMap = new HashMap<>();
    validatorMap.put("filePath", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));

    Validator candidateValidator = new ValidatorImpl(validationParams, validatorMap);
    validators.add(candidateValidator);

    if (!ParamHelper.isParamsValid(httpServerResponse, params, allowedParams,
        requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (response, requestParams, dbHandle) -> {
          CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
          if (!params.get("file_path").startsWith("static/img/" +
              ((Organization) routingContext.data().get("organization")).getOrganization_id())
              && !candidateDAO.isDocumentAssociated(params.get("filePath"))) {
            FileHelper.delete(params.get("filePath"));
            ResponseHelper.writeSuccessResponse(HttpStatus.SC_NO_CONTENT, "Successfully deleted the document",
                "Successfully deleted the document", response);
            return;
          }
          ResponseHelper.writeSuccessResponse(HttpStatus.SC_FORBIDDEN, "Not Authorized",
              "Not Authorized", response);
        });
  }
}