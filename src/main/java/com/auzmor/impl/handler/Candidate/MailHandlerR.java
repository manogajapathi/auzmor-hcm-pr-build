package com.auzmor.impl.handler.Candidate;

import com.auzmor.bo.*;
import com.auzmor.dao.CandidateDAO;
import com.auzmor.impl.bo.*;
import com.auzmor.impl.constant.file.endpoint.EndpointConstant;
import com.auzmor.impl.dao.CandidateDAOImpl;
import com.auzmor.impl.enumarator.bo.CandidateCommentEnum;
import com.auzmor.impl.enumarator.bo.MailEnum;
import com.auzmor.impl.enumarator.file.FileTypeEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.*;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.impl.models.CandidateR.CommentCandidate;
import com.auzmor.impl.models.CandidateR.MailDetails;
import com.auzmor.impl.util.DateUtil;
import com.auzmor.impl.util.Mail;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.Validator;
import com.mysql.cj.core.util.StringUtils;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;

public class MailHandlerR {

  private static Logger logger = LoggerFactory.getLogger(MailHandlerR.class);


  public static void SendBulkMail(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"candidate_id", "mail_description", "subject_name", "creator_id",
        "attachment_name"};

    String[] requiredParams = {"candidate_id", "mail_description", "creator_id",
        "subject_name"};

    String[] interviewParams = {"candidate_id", "mail_description", "subject_name",
        "creator_id","attachment_name"};

    List<Validator> validators = new ArrayList<>();
    Validator mailValidator = new ValidatorImpl(interviewParams, MailDetails.getValidatorMap());
    validators.add(mailValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {

          String candidateId = requestParams.get("candidate_id");
          MailEnum mailEnum = null;
          String attachmentName = StringUtil.printValidString(
              requestParams.get("attachment_name"));
          String mailMessage="";
          long userId = Long.parseLong(requestParams.get("creator_id"));

          EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
          Employee employee = employeeBO.getEmployeeDetails(userId);

          JobBO jobBO = new JobBOImpl(dbHandle);

          CandidateBO candidateBO = new CandidateBOImpl(dbHandle);
          CandidateR candidateR = candidateBO.getCandidate(dbHandle,Long.parseLong(candidateId));

          String mailDescription = requestParams.get("mail_description");
          String subjectName = requestParams.get("subject_name");

          OrganizationBO organizationBO = new OrganizationBOImpl(dbHandle);
          Organization organization = organizationBO.
              getOrganizationDetails(candidateR.getOrganizationId());

           Job job = jobBO.getJobDetails(candidateR.getJobId());
          String positionName="-";

          if (candidateId.contains(",")) {
            String[] items = candidateId.split(",");
            List<String> container = Arrays.asList(items);
            for (String id : container) {
              String firstName = candidateR.getFirstName();
              String companyName = organization.getOrganization_name();
              if(job != null){
                positionName = job.getTitle();
              }
              Map<String, String> substitutorMap = new HashMap<>();
              substitutorMap.put("company name", "<b>" + companyName + "</b>");
              substitutorMap.put("candidate first name", "<b>" + firstName + "</b>");
              substitutorMap.put("position name", "<b>" + positionName + "</b>");
              substitutorMap.put("emailers full name", "<b>" + employee.getFirst_name() + " "
                  + employee.getLast_name() + "</b>");
              substitutorMap.put("position", "<b>Tech Lead</b>");
              mailDescription = StringUtil.substituteString(mailDescription, substitutorMap);

              MailDetails mailDetails = new MailDetails();
              mailDetails.setCandidateId(Long.parseLong(id));
              mailDetails.setAdminName(StringUtil.printValidString(employee.getFirst_name()));
              mailDetails.setSubjectName(StringUtil.printValidString(requestParams.get("subject_name")));
              mailDetails.setAttachmentName(StringUtil.printValidString(
                  requestParams.get("attachment_name")));
              mailDetails.setMailDescription(mailDescription);
              mailDetails.setUserId(Long.parseLong(requestParams.get("creator_id")));

              MailDetailsBO mailDetailsBO = new MailDetailsBOImpl(dbHandle);
              int mailId = mailDetailsBO.create(mailDetails);
              if (mailId > 0) {
                Mail mail = new Mail();
                try {
                  mailMessage = mail.mailDetails(Vertx.vertx(), mailDescription,
                      candidateR.getEmail(), subjectName, attachmentName);

                } catch (FileNotFoundException e) {
                  e.printStackTrace();
                }
                /*String comments = "sent an Email";
                CommentCandidate commentsModel = new CommentCandidate();
                commentsModel.setAdminName(StringUtil.printValidString(employee.getFirst_name()));
                commentsModel.setComments(comments);
                commentsModel.setCandidateId(Long.parseLong(id));
                commentsModel.setUserId(userId);
                java.util.Date dNow = new java.util.Date();
                String currentDate = new DateUtil().getDate().format(dNow);
                try {
                  commentsModel.setCommentsDate(new DateUtil().getDate().parse(currentDate));
                } catch (ParseException e) {
                  e.printStackTrace();
                }

                CandidateCommentBO candidateCommentBO = new CandidateCommentBOImpl(dbHandle);
                CandidateCommentEnum candidateCommentEnum = candidateCommentBO.addCommentDetails(commentsModel);

                if (candidateCommentEnum == candidateCommentEnum.SUCCESS) {

                } else {
                  logger.error("sending bulk mail to multiple ids issues");
                  MailHandlerR.writeResponse
                      (httpServerResponse, dbHandle, mailEnum.SEND_MAIL_FAILED, null);
                }*/
              } else {
                logger.error("sending bulk mail to multiple ids issues");
                MailHandlerR.writeResponse
                    (httpServerResponse, dbHandle, mailEnum.SEND_MAIL_FAILED, null);
              }
            }

            if (!"success".equals(mailMessage)) {
              MailHandlerR.writeResponse
                  (httpServerResponse, dbHandle, mailEnum.SEND_MAIL_FAILED, null);
            } else {
              JsonObject dataObject = new JsonObject();
              dataObject.put("message", "Email Sent Succesfully");
              MailHandlerR.writeResponse
                  (httpServerResponse, dbHandle, mailEnum.SEND_MAIL_SUCCESS, dataObject);
            }
          } else {
            /*----------------checking access role for admin/HiringLead/Collaborator ---------------------*/
            String userType = routingContext.data().get("userType").toString();
            int hiringLead = 0;

            // checking userType not equal to admin
            if (!userType.equalsIgnoreCase("admin")) {

              int employeeId = ((Employee) routingContext.data().get("employee")).getEmployee_id();

              String hiringLeadId = job.getHiring_lead();

              if (!StringUtils.isNullOrEmpty(hiringLeadId)) {
                hiringLead = Integer.parseInt(hiringLeadId);
              }
              // checking hiringLead whether employeeId had access to this job.
              if (employeeId != hiringLead && hiringLead > 0) {
                ResponseHelper.writeErrorResponse(HttpStatus.SC_FORBIDDEN, "You Cannot Consume this Service",
                    "You Cannot Consume this Service", httpServerResponse);
                logger.info("Access check for HiringLead/Collaborator/Admin!");
              }
            }

            String firstName = candidateR.getFirstName();
            String companyName = organization.getOrganization_name();
            if(job != null){
              positionName = job.getTitle();
            }
            Map<String, String> substitutorMap = new HashMap<>();
            substitutorMap.put("company name", "<b>" + companyName + "</b>");
            substitutorMap.put("candidate first name", "<b>" + firstName + "</b>");
            substitutorMap.put("position name", "<b>" + positionName + "</b>");
            substitutorMap.put("emailers full name", "<b>" + employee.getFirst_name() + " "
                + employee.getLast_name() + "</b>");
            substitutorMap.put("position", "<b>Tech Lead</b>");
            mailDescription = StringUtil.substituteString(mailDescription, substitutorMap);

            MailDetails mailDetails = new MailDetails();
            mailDetails.setCandidateId(Long.parseLong(requestParams.get("candidate_id")));
            mailDetails.setAttachmentName(StringUtil.printValidString(requestParams.get("attachment_name")));
            mailDetails.setMailDescription(mailDescription);
            mailDetails.setAdminName(StringUtil.printValidString(employee.getFirst_name()));
            mailDetails.setSubjectName(StringUtil.printValidString(requestParams.get("subject_name")));
            mailDetails.setUserId(Long.parseLong(requestParams.get("creator_id")));

            MailDetailsBO mailDetailsBO = new MailDetailsBOImpl(dbHandle);
            int mailId = mailDetailsBO.create(mailDetails);
            if (mailId > 0) {
              Mail mail = new Mail();
              try {
                if (!"success".equals(mail.mailDetails(Vertx.vertx(), mailDescription,
                    candidateR.getEmail(), subjectName, attachmentName))) {

                  MailHandlerR.writeResponse
                      (httpServerResponse, dbHandle, mailEnum.SEND_MAIL_FAILED, null);
                } else {
                  JsonObject dataObject = new JsonObject();
                  dataObject.put("message", "Email Sent Succesfully to " + candidateR.getFirstName());

                  MailHandlerR.writeResponse
                      (httpServerResponse, dbHandle, mailEnum.SEND_MAIL_SUCCESS, dataObject);
                }
              } catch (FileNotFoundException e) {
                e.printStackTrace();
              }

              /*String comments = "sent an Email";
              CommentCandidate commentsModel = new CommentCandidate();
              commentsModel.setAdminName(StringUtil.printValidString(employee.getFirst_name()));
              commentsModel.setComments(comments);
              commentsModel.setCandidateId(Long.parseLong(requestParams.get("candidate_id")));
              commentsModel.setUserId(userId);
              java.util.Date dNow = new java.util.Date();
              String currentDate = new DateUtil().getDate().format(dNow);
              try {
                commentsModel.setCommentsDate(new DateUtil().getDate().parse(currentDate));
              } catch (ParseException e) {
                e.printStackTrace();
              }

              CandidateCommentBO candidateCommentBO = new CandidateCommentBOImpl(dbHandle);
              CandidateCommentEnum candidateCommentEnum = candidateCommentBO.addCommentDetails(commentsModel);
              if (candidateCommentEnum == candidateCommentEnum.SUCCESS) {

              } else {
                logger.error("sending bulk mail issues");
                MailHandlerR.writeResponse
                    (httpServerResponse, dbHandle, mailEnum.SEND_MAIL_FAILED, null);
              }*/
            } else {
              logger.error("sending bulk mail issues");
              MailHandlerR.writeResponse
                  (httpServerResponse, dbHandle, mailEnum.SEND_MAIL_FAILED, null);
            }
          }

        });
  }

  public static void writeResponse(HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   MailEnum mailEnum, JsonObject jsonObject)
      throws SQLException, DbHandleException {

    switch (mailEnum) {
      case SEND_MAIL_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_NOT_ACCEPTABLE,
            "Sending E-Mail has been Failed",
            "Sending E-Mail has been Failed",
            httpServerResponse);
        break;
      case SEND_MAIL_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Email has Send Successfully!", jsonObject, httpServerResponse);
        break;
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }

  public static void mailAttachment(RoutingContext routingContext) {
    Set<FileUpload> fileUploadSet = routingContext.fileUploads();

    for (FileUpload fileUpload : fileUploadSet) {
      FileTypeEnum fileTypeEnum = FileHelper.getFileTypeFoDocUpload(fileUpload);

      if (fileTypeEnum != null) {
        String sourceFile = fileUpload.uploadedFileName();
        String destinationFile = fileUpload.uploadedFileName().split(File.separator)[1] +
                fileTypeEnum.getExtension();
        String destinationFolderPath = EndpointConstant.publicStaticCandidateFile + File.separator +
                routingContext.request().getParam("organization_id");

        FileHelper.move(sourceFile, destinationFolderPath, destinationFile);
        routingContext.response()
                .setStatusCode(HttpStatus.SC_CREATED)  // 201
                .putHeader("Content-Type", "application/json; charset=utf-8")
                .end(destinationFolderPath + File.separator + destinationFile);

      } else {
        ResponseHelper.writeStringErrorResponse("Please upload PDF/DOC/JPEG/PNG/GIF/PPT/XLS/TXT file!",
            routingContext.response());
      }
    }
  }
}
