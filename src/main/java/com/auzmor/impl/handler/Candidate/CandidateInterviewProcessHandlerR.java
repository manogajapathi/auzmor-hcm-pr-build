package com.auzmor.impl.handler.Candidate;

import com.auzmor.bo.*;
import com.auzmor.dao.CandidateDAO;
import com.auzmor.dao.EmployeeDAO;
import com.auzmor.dao.LocationDAO;
import com.auzmor.impl.bo.*;
import com.auzmor.impl.dao.CandidateDAOImpl;
import com.auzmor.impl.dao.EmployeeDAOImpl;
import com.auzmor.impl.dao.LocationDAOImpl;
import com.auzmor.impl.enumarator.bo.CandidateCommentEnum;
import com.auzmor.impl.enumarator.bo.CandidateHiringProcessEnum;
import com.auzmor.impl.enumarator.bo.CandidateInterviewEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.impl.models.CandidateR.CommentCandidate;
import com.auzmor.impl.models.CandidateR.HiringProcessCandidate;
import com.auzmor.impl.models.CandidateR.InterviewProcessCandidate;
import com.auzmor.impl.util.DateUtil;
import com.auzmor.impl.util.Mail;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.icalendar.ICalendarImpl;
import com.auzmor.impl.util.icalendar.component.ICalendarEventImpl;
import com.auzmor.impl.util.icalendar.component.ICalendarTimeZoneImpl;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.icalendar.component.ICalendarEvent;
import com.auzmor.util.icalendar.component.ICalendarTimeZone;
import com.auzmor.util.validator.Validator;
import com.mysql.cj.core.util.StringUtils;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mail.MailAttachment;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;

import static com.auzmor.impl.enumarator.bo.CandidateInterviewEnum.*;

public class CandidateInterviewProcessHandlerR {

  private static Logger logger = LoggerFactory.getLogger(CandidateInterviewProcessHandlerR.class);

  public static void addInterviewProcess(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"candidate_id", "interview_date", "interview_time",
        "interview_end_time", "interview_location", "interview_type", "interview_duration",
        "interview_details", "interview_mail_description",
        "type", "admin_name", "subject_name", "user_id", "collaborator_id", "sub_status"};
    String[] requiredParams = {"candidate_id", "interview_date", "interview_time",
        "interview_type", "interview_mail_description",
        "admin_name", "subject_name", "sub_status"};
    String[] interviewParams = {"candidate_id", "interview_date", "interview_time",
        "interview_end_time", "interview_location", "interview_type",
        "interview_details", "interview_mail_description", "interview_duration",
        "type", "admin_name", "subject_name", "user_id", "collaborator_id", "sub_status"};

    List<Validator> validators = new ArrayList<>();
    Validator interviewValidator = new ValidatorImpl(interviewParams,
        InterviewProcessCandidate.getValidatorMap());
    validators.add(interviewValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }
    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {

          /*----------------checking access role for admin/HiringLead/Collaborator ---------------------*/
          String userType = routingContext.data().get("userType").toString();

          CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
          CandidateBO candidateBO = new CandidateBOImpl(dbHandle);
          JobBO jobBO = new JobBOImpl(dbHandle);
          long candidateId = Long.parseLong(requestParams.get("candidate_id"));
          int hiringLead = 0;

          CandidateR candidateR = candidateDAO.getCandidate(candidateId);
          Job job = jobBO.getJobDetails(candidateR.getJobId());

          // checking userType not equal to admin
          if (!userType.equalsIgnoreCase("admin")) {

            int employeeId = ((Employee) routingContext.data().get("employee")).getEmployee_id();

            String hiringLeadId = job.getHiring_lead();

            if (!StringUtils.isNullOrEmpty(hiringLeadId)) {
              hiringLead = Integer.parseInt(hiringLeadId);
            }
            // checking hiringLead whether employeeId had access to this job.
            if (employeeId != hiringLead && hiringLead > 0) {
              ResponseHelper.writeErrorResponse(HttpStatus.SC_FORBIDDEN, "You Cannot Consume this Service",
                  "You Cannot Consume this Service", httpServerResponse);
              logger.info("Access check for HiringLead/Collaborator/Admin!");
            }
          }
          try {
            String existingStatus = candidateBO.getCandidate(dbHandle,
                Long.parseLong(requestParams.get("candidate_id"))).getStatus();

            CandidateInterviewBO candidateInterviewBO = new CandidateInterviewBOImpl(dbHandle);
            String organizationName = ((Organization) routingContext.get("organization")).getOrganization_name();
            organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);
            String hirsUrl = ((Organization) routingContext.get("organization")).getHris_url();
            CandidateInterviewEnum candidateInterviewEnum = null;
            String companyAddress = "";
            // ADDING NEW RECORD IN HIRINGPROCESS TABLE
            HiringProcessCandidate candidateHiringProcessModel = new HiringProcessCandidate();
            candidateHiringProcessModel.setStatus("Interview");
            candidateHiringProcessModel.setCandidateProcess(requestParams.get("sub_status"));
            candidateHiringProcessModel.setCandidateId(Long.parseLong(requestParams.get("candidate_id")));
            candidateHiringProcessModel.setReason("");
            candidateHiringProcessModel.setUserId(Long.parseLong(requestParams.get("user_id")));

            CandidateHiringProcessBO candidateHiringProcessBO = new CandidateHiringProcessBOImpl(dbHandle);
            CandidateHiringProcessEnum candidateHiringProcessEnum = candidateHiringProcessBO.
                insertCandidateStatus(dbHandle, candidateHiringProcessModel);

            if (candidateHiringProcessEnum == candidateHiringProcessEnum.SUCCESS) {
              /* inserting comments a new record */
              Date dNow = new Date();
              String currentDate = new DateUtil().getDate().format(dNow);
              String comments = "Interview Scheduled on " + requestParams.get("interview_date");
              CommentCandidate commentCandidate = new CommentCandidate();
              commentCandidate.setAdminName(requestParams.get("admin_name"));
              commentCandidate.setComments(comments);
              commentCandidate.setUserId(Long.parseLong(requestParams.get("user_id")));
              commentCandidate.setSystemGenerated(true);
              try {
                commentCandidate.setCommentsDate(new DateUtil().getDate().parse(currentDate));
              } catch (ParseException e) {
                e.printStackTrace();
              }
              commentCandidate.setCandidateId(Long.parseLong(requestParams.get("candidate_id")));
              CandidateCommentBO candidateCommentBO = new CandidateCommentBOImpl(dbHandle);
              CandidateCommentEnum candidateCommentEnum = candidateCommentBO.
                  addCommentDetails(commentCandidate, true);
              if (candidateCommentEnum == candidateCommentEnum.SUCCESS) {
                long interviewId =
                    candidateInterviewBO.addInterviewProcess(requestParams);
                if (interviewId > 0) {
                  long collaboratorId = Long.parseLong(requestParams.get("collaborator_id"));
                  // long creatorId = Long.parseLong(requestParams.get("user_id"));

                  /*java.sql.Date sqldate = new java.sql.Date(new DateUtil().getDate().
                      parse(requestParams.get("interview_date")).getTime());*/

                  // as per loga said my commenting this logic
                  // adding task to collaborator.
                  /*JobTaskModel jobTaskModel = new JobTaskModel();
                  JobTaskDAO jobTaskDAO = new JobTaskDAOImpl(dbHandle);
                  jobTaskModel.setJobId(candidateR.getJobId());
                  jobTaskModel.setCandidateId(candidateId);
                  jobTaskModel.setDueDate(sqldate);
                  jobTaskModel.setTaskName("interview scheduled");
                  jobTaskModel.setEmployeeId(collaboratorId);
                  jobTaskModel.setCreatorId(creatorId);
                    int jobTaskForCollaborator = jobTaskDAO.createTask(jobTaskModel);
*/
                  logger.info("Adding Task to Collaborator");

                  Mail mail = new Mail();
            /*            JobBO jobBO = new JobBOImpl(dbHandle);
                        Job job = jobBO.getJobDetails(candidateR.getJobId());*/

                  String interviewDetails = "";
                  String calendarAddress = "";
                  if (requestParams.get("interview_type").equalsIgnoreCase("Web")) {
                    interviewDetails = requestParams.get("interview_details");
                  } else {
                    interviewDetails = requestParams.get("interview_location");
                    LocationDAO locationDAO = new LocationDAOImpl(dbHandle);
                    companyAddress = StringUtil.printValidString(locationDAO.
                        getAddress(interviewDetails, candidateR.getOrganizationId()));
                    if (companyAddress.length() == 0) {
                      companyAddress = "";
                      calendarAddress = interviewDetails;
                    } else {
                      calendarAddress = interviewDetails + "(" + companyAddress + ")";
                    }
                  }

                  // Calendar Invite Event and TimeZone
                  ICalendarEvent iCalendarEvent = new ICalendarEventImpl()
                      .setStartDate(requestParams.get("interview_date"), "MM/dd/yyyy")
                      .setEndDate(requestParams.get("interview_date"), "MM/dd/yyyy")
                      .setStartTime(requestParams.get("interview_time"), "hh:mm a")
                      .setEndTime(requestParams.get("interview_end_time"), "hh:mm a")
                      .setSummary("Interview Invite from " + organizationName).setOrganizer(organizationName)
                      .setLocation(calendarAddress);

                  ICalendarTimeZone iCalendarTimeZone = new ICalendarTimeZoneImpl();

                  String hiringLeadId = job.getHiring_lead();
                  if (collaboratorId == hiringLead) {
                    EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
                    Employee employee = employeeDAO.getEmployeeDetails(collaboratorId);
                    if (employee != null) {
                      String body = "<html><body> Dear HiringLead, <br> <br>" +
                          "<p> This is to inform you that an interview has been scheduled for the position of " +
                          " " + job.getTitle() + " on " + requestParams.get("interview_date") + " at " +
                          " " + requestParams.get("interview_time") + ".\n " +
                          "</b></p> <br><br> <p>" +
                          "Candidate details:\n <br><br>" +
                          "Name: <b>" + candidateR.getFirstName() + " " + candidateR.getLastName() + " \n<br>" +
                          "Contact: <b>" + candidateR.getPhoneNumber() + "<br> </p> <br>" +
                          "Thank You,\n<br/> " + organizationName + " Team <br/>" +
                          "--------------------------------------------------------------\n<br/>" +
                          "This is a system generated mail.\n<br/>" +
                          "Please do not reply to this mail\n<br/>" +
                          "-------------------------------------------------------------- " +
                          "</body></html>";

                      // Calendar as MailAttachment
                      iCalendarEvent.setDescription(body);
                      MailAttachment iCalAttachment = new MailAttachment().setName("invite.ics")
                          .setContentType("text/calendar;charset=\"UTF8\"")
                          .setData(Buffer.buffer(new ICalendarImpl().addEvent(iCalendarEvent).setTimeZone(iCalendarTimeZone)
                              .toString().getBytes()));
                      List<MailAttachment> mailAttachments = new ArrayList<>();
                      mailAttachments.add(iCalAttachment);
                      // same mail has to go to collaborator also
                      if (!"success".equals(mail.sendMailWithAttachment(Vertx.vertx(),
                          body, employee.getWork_email(), requestParams.get("subject_name")+"-HiringLead", mailAttachments))) {
                        CandidateInterviewProcessHandlerR.writeResponse
                            (httpServerResponse, dbHandle, candidateInterviewEnum.SEND_FAILED, null);
                        logger.error("Sending Interview Scheduled Mail Failed");
                      }
                    }
                  } else {
                    if (collaboratorId > 0) {
                      EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
                      Employee employee = employeeDAO.getEmployeeDetails(collaboratorId);
                      if (employee != null) {
                        String body = "<html><body> Dear Collaborator, <br> <br>" +
                            "<p> This is to inform you that an interview has been scheduled for the position of " +
                            " " + job.getTitle() + " on " + requestParams.get("interview_date") + " at " +
                            " " + requestParams.get("interview_time") + ".\n " +
                            "</b></p> <br><br> <p>" +
                            "Candidate details:\n <br><br>" +
                            "Name: <b>" + candidateR.getFirstName() + " " + candidateR.getLastName() + " \n<br>" +
                            "Contact: <b>" + candidateR.getPhoneNumber() + "<br> </p> <br>" +
                            "Thank You,\n<br/> " + organizationName + " Team <br/>" +
                            "------------------------------------------------------------\n<br/>" +
                            "This is a system generated mail.\n<br/>" +
                            "Please do not reply to this mail\n<br/>" +
                            "-------------------------------------------------------------- " +
                            "</body></html>";

                        // Calendar as MailAttachment
                        iCalendarEvent.setDescription(body);
                        MailAttachment iCalAttachment = new MailAttachment().setName("invite.ics")
                            .setContentType("text/calendar;charset=\"UTF8\"")
                            .setData(Buffer.buffer(new ICalendarImpl().addEvent(iCalendarEvent).setTimeZone(iCalendarTimeZone)
                                .toString().getBytes()));
                        List<MailAttachment> mailAttachments = new ArrayList<>();
                        mailAttachments.add(iCalAttachment);
                        // same mail has to go to collaborator also
                        if (!"success".equals(mail.sendMailWithAttachment(Vertx.vertx(),
                            body, employee.getWork_email(), requestParams.get("subject_name"), mailAttachments))) {
                          CandidateInterviewProcessHandlerR.writeResponse
                              (httpServerResponse, dbHandle, candidateInterviewEnum.SEND_FAILED, null);
                          logger.error("Sending Interview Scheduled Mail Failed");
                        }
                      }
                    }
                    //sending mail to hiring lead

                    if (!(null == hiringLeadId) && !(hiringLeadId.isEmpty())) {
                      EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
                      Employee employee = employeeDAO.getEmployeeDetails(Long.parseLong(hiringLeadId));
                      if (employee != null) {
                        String body = " <html><body> Dear HiringLead, <br> <br>" +
                            "<p> This is to inform you that an interview has been scheduled for the position of " +
                            " " + job.getTitle() + " on " + requestParams.get("interview_date") + " at " +
                            " " + requestParams.get("interview_time") + ".\n " +
                            "</b></p> <br><br> <p>" +
                            "Candidate details:\n <br><br>" +
                            "Name: <b>" + candidateR.getFirstName() + " " + candidateR.getLastName() + " \n<br></b>" +
                            "Contact:<b>" + candidateR.getPhoneNumber() + "<br> </p> <br></b>" +
                            "Thank You,\n<br/> " + organizationName + " Team <br/>" +
                            "--------------------------------------------------------------\n<br/>" +
                            "This is a system generated mail.\n<br/>" +
                            "Please do not reply to this mail\n<br/>" +
                            "-------------------------------------------------------------- " +
                            "</body></html> ";

                        if (!"success".equals(mail.sendMail(Vertx.vertx(),
                            body, employee.getWork_email(), (requestParams.get("subject_name") + "-For HiringLead")))) {
                          CandidateInterviewProcessHandlerR.writeResponse
                              (httpServerResponse, dbHandle, candidateInterviewEnum.SEND_FAILED, null);
                          logger.error("Sending Interview Scheduled Mail Failed for hiring lead");
                        }
                      }
                    }
                  }
                  long adminid = 0;
                  long user_id = Long.parseLong(requestParams.get("user_id"));
                  Employee employee_user = new EmployeeDAOImpl(dbHandle).getEmployeeDetails(user_id);
                  LoginBO loginBO = new LoginBOImpl(dbHandle);
                  long organization_id = ((Organization) routingContext.get("organization")).getOrganization_id();
                  List<HashMap<String, String>> adminList = loginBO.GetAdminList(organization_id);
                  for (HashMap<String, String> map : adminList) {
                    for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                      String key = mapEntry.getKey();
                      String value = mapEntry.getValue();
                      if (key.equals("id")) {
                        adminid = Long.parseLong(value);
                      }
                      if (key.equals("email")) {
                        mapEntry.getValue();
                        String AdminEmail = value;
                        if (adminid != employee_user.getUser_id()) {
                          String body = " <html><body> Dear Admin, <br> <br>" +
                              "<p> This is to inform you that an interview has been scheduled for the position of " +
                              " " + job.getTitle() + " on " + requestParams.get("interview_date") + " at " +
                              " " + requestParams.get("interview_time") + ".\n " +
                              "</b></p> <br><br> <p>" +
                              "Candidate details:\n <br><br>" +
                              "Name: <b>" + candidateR.getFirstName() + " " + candidateR.getLastName() + " \n<br></b>" +
                              "Contact:<b>" + candidateR.getPhoneNumber() + "<br> </p> <br></b>" +
                              "Thank You,\n<br/> " + organizationName + " Team <br/>" +
                              "--------------------------------------------------------------\n<br/>" +
                              "This is a system generated mail.\n<br/>" +
                              "Please do not reply to this mail\n<br/>" +
                              "-------------------------------------------------------------- " +
                              "</body></html> ";

                          // Calendar as MailAttachment
                          iCalendarEvent.setDescription(body);
                          MailAttachment iCalAttachment = new MailAttachment().setName("invite.ics")
                              .setContentType("text/calendar;charset=\"UTF8\"")
                              .setData(Buffer.buffer(new ICalendarImpl().addEvent(iCalendarEvent).setTimeZone(iCalendarTimeZone)
                                  .toString().getBytes()));
                          List<MailAttachment> mailAttachments = new ArrayList<>();
                          mailAttachments.add(iCalAttachment);
                          if (!"success".equals(mail.sendMailWithAttachment(Vertx.vertx(),
                              body, AdminEmail, (requestParams.get("subject_name")), mailAttachments))) {
                            CandidateInterviewProcessHandlerR.writeResponse
                                (httpServerResponse, dbHandle, candidateInterviewEnum.SEND_FAILED, null);
                            logger.error("Sending Interview Scheduled Mail Failed for hiring lead");
                          }
                        }
                      }
                    }
                  }
                  // SENDING EMAIL REGARDING INTERVIEW DETAILS OR THROUGH SMS
                  if (requestParams.get("type").equals("mail")) {
                    String userName = candidateR.getFirstName() + " " + candidateR.getLastName();
                    String body = "<html><body><p> Dear " + userName + ",   <br><br>       " +
                        " This is to inform you that you have been shortlisted for an interview for the position of" +
                        " " + job.getTitle() + " at " + organizationName + ". Please " +
                        " <a href='https://" + hirsUrl + "/careers/" + candidateR.getUserId() + "/details/" + candidateId + "'>click here</a> to " +
                        " respond to your Interview Details. <br/>" +
                        " Please find the details of the interview below:<br> " +
                        " Date: " + requestParams.get("interview_date") + " <br>" +
                        " Time: " + requestParams.get("interview_time") + " <br>" +
                        " Location/Web interview:" + interviewDetails + " <br> " +
                        " Kindly reach out to the support team if you face any issues." +
                        " We wish you all the very best for the interview! Hope you crack it!<br> <br>" +
                        " " + companyAddress.replace(",", "<br>") + "<br>Thank You,\n<br/>" +
                        " " + organizationName + " Team <br/></p><br>" +
                        "--------------------------------------------------------------\n<br/>" +
                        "This is a system generated mail.\n<br/>" +
                        "Please do not reply to this mail\n<br/>" +
                        "-------------------------------------------------------------- " +
                        "</body></html>";

                    // Calendar as MailAttachment
                    iCalendarEvent.setDescription(body);
                    MailAttachment iCalAttachment = new MailAttachment().setName("invite.ics")
                        .setContentType("text/calendar;charset=\"UTF8\"")
                        .setData(Buffer.buffer(new ICalendarImpl().addEvent(iCalendarEvent).setTimeZone(iCalendarTimeZone)
                            .toString().getBytes()));
                    List<MailAttachment> mailAttachments = new ArrayList<>();
                    mailAttachments.add(iCalAttachment);
                    if (!"success".equals(mail.sendMailWithAttachment(Vertx.vertx(),
                        body,
                        candidateR.getEmail(), requestParams.get("subject_name"), mailAttachments))) {
                      CandidateInterviewProcessHandlerR.writeResponse
                          (httpServerResponse, dbHandle, candidateInterviewEnum.SEND_FAILED, null);
                      logger.error("Sending Interview Scheduled Mail Failed");
                    }
                  }/* else {
              JWTUtil jwtUtil = new JWTUtil();
              try {
                Twilio.init(jwtUtil.getAccountId(), jwtUtil.getAccountToken());
              } catch (ApplicationUtilException e) {
                e.printStackTrace();
              }
              Message smsMessage = Message
                  .creator(new PhoneNumber("+918668020332"), // to
                      new PhoneNumber("+14789976411"), // from
                      "Regarding Interview Details").create();
            }*/
                  JsonObject successObject = new JsonObject();

                  InterviewProcessCandidate interviewProcessCandidate =
                      candidateInterviewBO.
                          getInterviewDetails(Long.parseLong(requestParams.get("candidate_id")));
                  String date = "";
                  try {
                    date = new DateUtil().
                        convertDateintoString(interviewProcessCandidate.getInterviewDate());
                    currentDate = new DateUtil().
                        convertDateintoString(interviewProcessCandidate.getCreated());
                  } catch (ParseException e) {
                    e.printStackTrace();
                    logger.error("Date Format Issues");
                    ResponseHelper.writeInternalServerError(httpServerResponse);
                  }
                  successObject.put("interview_date", date);
                  successObject.put("interview_time", interviewProcessCandidate.getInterviewTime());
                  successObject.put("interview_end_time", interviewProcessCandidate.getCollaboratorId());
                  successObject.put("interview_duration", interviewProcessCandidate.getInterviewDuration());
                  successObject.put("interview_type", interviewProcessCandidate.getInterviewType());
                  successObject.put("collaborator_id", interviewProcessCandidate.getCollaboratorId());

                  if (interviewProcessCandidate.getInterviewType().
                      equalsIgnoreCase("In Person")) {
                    successObject.put("interview_location",
                        interviewProcessCandidate.getInterviewLocation());
                  } else {
                    successObject.put("interview_details",
                        interviewProcessCandidate.getInterviewDetails());
                  }
                  successObject.put("interview_mail_description",
                      interviewProcessCandidate.getInterviewMailDescription());
                  successObject.put("candidate_id", requestParams.get("candidate_id"));
                  successObject.put("modified_date", currentDate);
                  successObject.put("message", "Candidate Interview Scheduled Successfully!");

                  logger.info("Candidate Interview Scheduled Successfully!");

                  candidateBO.sendStatusUpdatesToCollaborator(existingStatus,
                      candidateHiringProcessModel.getStatus(), "",
                      Integer.parseInt(requestParams.get("candidate_id")));
                  candidateBO.sendStatusUpdatesToHiringLead(existingStatus,
                      candidateHiringProcessModel.getStatus(), "",
                      Integer.parseInt(requestParams.get("candidate_id")));
                  candidateBO.sendStatusUpdatesToAdmin(existingStatus,
                      candidateHiringProcessModel.getStatus(),
                      StringUtil.printValidString(requestParams.get("candidate_process")),
                      Integer.parseInt(requestParams.get("candidate_id")), Long.parseLong(requestParams.get("user_id")));

                  logger.info("Candidate Status updates sent to Collaborator and Hiring Lead");

                  CandidateInterviewProcessHandlerR.writeResponse
                      (httpServerResponse, dbHandle, SUCCESS, successObject);
                } else {
                  CandidateInterviewProcessHandlerR.writeResponse
                      (httpServerResponse, dbHandle, candidateInterviewEnum.FAILED, null);
                  logger.info("Candidate Interview Schedule Failed!");
                }
              } else {
                CandidateInterviewProcessHandlerR.writeResponse
                    (httpServerResponse, dbHandle, candidateInterviewEnum.FAILED, null);
                logger.info("Candidate Interview Schedule Failed!");
              }
            } else {
              CandidateInterviewProcessHandlerR.writeResponse
                  (httpServerResponse, dbHandle, candidateInterviewEnum.FUTURE_DATE, null);
              logger.info("Candidate Interview Schedule Failed!");
            }
          } catch (IllegalArgumentException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            ResponseHelper.writeInternalServerError(httpServerResponse);
          } catch (ParseException e) {
            logger.error(e.getMessage());
            ResponseHelper.writeInternalServerError(httpServerResponse);
          }
        });
  }

  public static void writeResponse(HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   CandidateInterviewEnum candidateInterviewEnum, JsonObject jsonObject)
      throws SQLException, DbHandleException {

    switch (candidateInterviewEnum) {
      case FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_CONFLICT,
            "Interview Schedule has been Failed",
            "Interview Schedule has been Failed ",
            httpServerResponse);
        break;
      case SEND_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_CONFLICT,
            "Sending Interview Schedule Details Mail has been Failed",
            "Sending Mail has been Failed",
            httpServerResponse);
        break;

      case SEND_MAIL_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_NOT_ACCEPTABLE,
            "Sending E-Mail has been Failed",
            "Sending E-Mail has been Failed",
            httpServerResponse);
        break;
      case SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Interview has been Scheduled Successfully", jsonObject, httpServerResponse);
        break;

      case STATUS_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Interview Scheduled has been updated Successfully", jsonObject, httpServerResponse);
        break;

      case SEND_MAIL_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Email has Send Successfully!", jsonObject, httpServerResponse);
        break;

      case STATUS_REJECT:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Interview Scheduled has been updated  Successfully", jsonObject, httpServerResponse);
        break;
      case UPDATE_STATUS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Interview Scheduled has been updated  Successfully", jsonObject, httpServerResponse);
        break;

      case GET_INTERVIEW_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Getting Interview List Failed,Please try again later!",
            "Getting Interview List Failed,Please try again later!",
            httpServerResponse);
        break;
      case GET_INTERVIEW_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(jsonObject, httpServerResponse, jsonObject.getBoolean
            ("no_data"), jsonObject.getInteger("totalcount"));
        break;
      case FUTURE_DATE:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            candidateInterviewEnum.getDescription(),
            candidateInterviewEnum.getDescription(),
            httpServerResponse);
        break;
      case NO_UPDATE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "No Update is Done!", jsonObject, httpServerResponse);
        break;
      case COMPLETED_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Interview Completed Successfully!", jsonObject, httpServerResponse);
        break;
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }

  public static void updateInterviewProcess(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"candidateId", "interview_date", "interview_time",
        "interview_duration", "interview_location", "interview_type",
        "interview_details", "interview_mail_description", "interview_cancel_reason", "status",
        "type", "admin_name", "subject_name", "user_id", "collaborator_id", "interview_end_time",
        "interview_other_reason", "sub_status", "is_completed"};
    String[] requiredParams = {"candidateId"};
    String[] interviewParams = {"candidateId", "interview_date", "interview_time",
        "interview_duration", "interview_location", "interview_type",
        "interview_details", "interview_mail_description", "interview_cancel_reason", "status",
        "type", "admin_name", "subject_name", "user_id", "collaborator_id", "interview_end_time",
        "interview_other_reason", "sub_status", "is_completed"};

    List<Validator> validators = new ArrayList<>();
    Validator interviewValidator = new ValidatorImpl(interviewParams, InterviewProcessCandidate.getValidatorMap());
    validators.add(interviewValidator);
    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }
    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          CandidateInterviewBO candidateInterviewBO = new CandidateInterviewBOImpl(dbHandle);
          CandidateInterviewEnum candidateInterviewEnum =
              candidateInterviewBO.updateInterviewProcess(requestParams, routingContext);

          if (candidateInterviewEnum == SUCCESS) {
            JsonObject dataObject = new JsonObject();
            CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
            CandidateR candidateR = candidateDAO.getCandidate(
                Long.parseLong(requestParams.get("candidateId")));
            JobBO jobBO = new JobBOImpl(dbHandle);
            Job job = jobBO.getJobDetails(candidateR.getJobId());
            OrganizationBO organizationBO = new OrganizationBOImpl(dbHandle);
            Organization organization = organizationBO.getOrganizationDetails(candidateR.getOrganizationId());
            String organizationName = ((Organization) routingContext.data().get("organization")).getOrganization_name();
            organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);
            Mail mail = new Mail();
            EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
            String userName = candidateR.getFirstName() + " " + candidateR.getLastName();

            if (!StringUtil.printValidString(requestParams.get("interview_cancel_reason")).equals("") &&
                !StringUtil.printValidString(requestParams.get("status")).equals("")) {
              dataObject.put("message", "Thank you for your response." +
                  "  We are disappointed that you are not satisfied," +
                  "  but will take feedback into consideration! " +
                  "  If necessary,someone from our company will reach out to you.");

              CandidateInterviewProcessHandlerR.writeResponse
                  (httpServerResponse, dbHandle, candidateInterviewEnum.STATUS_REJECT, dataObject);

            } else if (!StringUtil.printValidString(requestParams.get("interview_cancel_reason")).equals("")) {
              InterviewProcessCandidate interviewProcessCandidate = candidateInterviewBO.
                  getInterviewDetails(Long.parseLong(requestParams.get("candidateId")));

              //mail for candidate
              String body = "<html><body>Dear <b>" + userName + "</b>, <br> <br>" +
                  "<p> We regret to inform you that your interview scheduled for the position of " +
                  " " + job.getTitle() + " with " + organizationName + " on " +
                  " " + interviewProcessCandidate.getInterviewDate() + " at " + interviewProcessCandidate.getInterviewTime() + " " +
                  "  has been cancelled. " +
                  " We apologize for the inconvenience and hope you get another interview soon. </p> <br> <br>" +
                  "Thank you,<br>" + organizationName + " Team <br> " +
                  " </body></html>";

              String subjectName = "Scheduled Interview Cancelled";
              if (!"success".equals(mail.sendMail(Vertx.vertx(),
                  body,
                  candidateR.getEmail(), subjectName))) {
                CandidateInterviewProcessHandlerR.writeResponse
                    (httpServerResponse, dbHandle, candidateInterviewEnum.SEND_FAILED, null);
                logger.info("Candidate Interview Cancelled Successfully!");
              }
              // need to send mail to collaborator also
              long collaboratorEmployeeId = interviewProcessCandidate.getCollaboratorId();
              long hiringLeadId = 0;
              String hiringLeadEmployeeId = job.getHiring_lead();
              if(!StringUtils.isNullOrEmpty(hiringLeadEmployeeId)){
                hiringLeadId = Long.parseLong(hiringLeadEmployeeId);
              }
              if (collaboratorEmployeeId == hiringLeadId && hiringLeadId > 0) {
                Employee employee = employeeBO.getEmployeeDetails(collaboratorEmployeeId);
                if (employee != null) {
                  body = "<html><body>Dear HiringLead, <br> <br>" +
                      "<p> This is to inform you that the interview scheduled for the position of " + job.getTitle() + " on  " +
                      " " + interviewProcessCandidate.getInterviewDate() + " at "
                      + interviewProcessCandidate.getInterviewTime() + " has been cancelled." +
                      " We apologize for the inconvenience. </p> <br> <br>" +
                      " Thank you,<br>" + organizationName + " Team <br/>" +
                      " </body></html>";
                  subjectName = "Scheduled Interview Cancelled-For HiringLead";

                  if (!"success".equals(mail.sendMail(Vertx.vertx(),
                      body,
                      employee.getWork_email(), subjectName))) {
                    CandidateInterviewProcessHandlerR.writeResponse
                        (httpServerResponse, dbHandle, candidateInterviewEnum.SEND_FAILED, null);
                    logger.info("Candidate Update Interview Mail Sending Failed!");
                  }
                }
              } else {
                if (collaboratorEmployeeId > 0) {
                  Employee employee = employeeBO.getEmployeeDetails(collaboratorEmployeeId);
                  if (employee != null) {
                    body = "<html><body>Dear Collaborator, <br> <br>" +
                        "<p> This is to inform you that the interview scheduled for the position of " + job.getTitle() + " on  " +
                        " " + interviewProcessCandidate.getInterviewDate() + " at "
                        + interviewProcessCandidate.getInterviewTime() + " has been cancelled." +
                        " We apologize for the inconvenience. </p> <br> <br>" +
                        " Thank you,<br>" + organizationName + " Team <br/>" +
                        " </body></html>";

                    subjectName = "Scheduled Interview Cancelled-For Collaborator";

                    if (!"success".equals(mail.sendMail(Vertx.vertx(),
                        body,
                        employee.getWork_email(), subjectName))) {
                      CandidateInterviewProcessHandlerR.writeResponse
                          (httpServerResponse, dbHandle, candidateInterviewEnum.SEND_FAILED, null);
                      logger.info("Candidate Update Interview Mail Sending Failed!");
                    }
                  }
                }
                //mail for hiring lead
                if (!(hiringLeadEmployeeId.equals(null)) && !(hiringLeadEmployeeId.isEmpty())) {
                  Employee employee = employeeBO.getEmployeeDetails(Long.parseLong(hiringLeadEmployeeId));
                  if (employee != null) {

                    body = "<html><body>Dear HiringLead, <br> <br>" +
                        "<p> This is to inform you that the interview scheduled for the position of " + job.getTitle() + " on  " +
                        " " + interviewProcessCandidate.getInterviewDate() + " at "
                        + interviewProcessCandidate.getInterviewTime() + " has been cancelled." +
                        " We apologize for the inconvenience. </p> <br> <br>" +
                        " Thank you,<br>" + organizationName + " Team <br/>" +
                        " </body></html>";

                    subjectName = "Scheduled Interview Cancelled -For HiringLead";

                    if (!"success".equals(mail.sendMail(Vertx.vertx(),
                        body,
                        employee.getWork_email(), subjectName))) {
                      CandidateInterviewProcessHandlerR.writeResponse
                          (httpServerResponse, dbHandle, candidateInterviewEnum.SEND_FAILED, null);
                      logger.info("Candidate Update Interview Mail Sending Failed!");
                    }
                  }
                }
              }
              //mail for Admin
              long adminid = 0;
              long user_id = Long.parseLong(requestParams.get("user_id"));
              Employee employee_user = new EmployeeDAOImpl(dbHandle).getEmployeeDetails(user_id);
              LoginBO loginBO = new LoginBOImpl(dbHandle);
              long organization_id = ((Organization) routingContext.get("organization")).getOrganization_id();
              List<HashMap<String, String>> adminList = loginBO.GetAdminList(organization_id);
              for (HashMap<String, String> map : adminList) {
                for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                  String key = mapEntry.getKey();
                  String value = mapEntry.getValue();
                  if (key.equals("id")) {
                    adminid = Long.parseLong(value);
                  }
                  if (key.equals("email")) {
                    mapEntry.getValue();
                    String AdminEmail = value;
                    if (adminid != employee_user.getUser_id()) {

                      body = "<html><body>Dear Admin, <br> <br>" +
                          "<p> This is to inform you that the interview scheduled for the position of " + job.getTitle() + " on  " +
                          " " + interviewProcessCandidate.getInterviewDate() + " at "
                          + interviewProcessCandidate.getInterviewTime() + " has been cancelled." +
                          " We apologize for the inconvenience. </p> <br> <br>" +
                          " Thank you,<br>" + organizationName + " Team <br/>" +
                          " </body></html>";

                      subjectName = "Scheduled Interview Cancelled";

                      if (!"success".equals(mail.sendMail(Vertx.vertx(),
                          body,
                          AdminEmail, subjectName))) {
                        CandidateInterviewProcessHandlerR.writeResponse
                            (httpServerResponse, dbHandle, candidateInterviewEnum.SEND_FAILED, null);
                        logger.info("admin Update Interview Mail Sending Failed!");

                      }
                    }
                  }
                }
              }

              dataObject.put("message", "Interview Scheduled has been updated Successfully");
              CandidateInterviewProcessHandlerR.writeResponse
                  (httpServerResponse, dbHandle, candidateInterviewEnum.UPDATE_STATUS, dataObject);
              logger.info("Candidate Interview Cancelled Successfully!");
            } else if (StringUtil.printValidString(requestParams.get("status")).equals("accept")) {
              dataObject.put("message", "Thank you for your response!" +
                  " We look forward to speaking with you!");

              // acceptance mail for both collaborator and hiringLead
              InterviewProcessCandidate interviewProcessCandidate = candidateInterviewBO.
                  getInterviewDetails(Long.parseLong(requestParams.get("candidateId")));

              long colloboratorEmployeeId = interviewProcessCandidate.getCollaboratorId();
              long adminId = interviewProcessCandidate.getUserId();
              String hiringLeadEmployeeId = job.getHiring_lead();
              //hiring as well as Hiring lead for job
              if (colloboratorEmployeeId == Long.parseLong(hiringLeadEmployeeId)) {
                Employee employee = employeeBO.getEmployeeDetails(colloboratorEmployeeId);
                String body = "";
                String subjectName = "Interview acceptance by Candidate";
                if (employee != null) {
                  body = "<html><body>Dear HiringLead, <br> <br>" +
                      "<p> \n" +
                      "You are just one step away from welcoming a new employee! This is to inform you" +
                      " that" + userName + "has accepted the interview invite for the position of " +
                      " " + job.getTitle() + " scheduled for" + interviewProcessCandidate.getInterviewDate() + " " +
                      " at " + interviewProcessCandidate.getInterviewTime() + ". Let's hope that this is the " +
                      " person you are looking for! </p> <br> <br>" +
                      " Thank you,<br>" + organizationName + " Team <br/>" +
                      " </body></html>";

                  if (!"success".equals(mail.sendMail(Vertx.vertx(),
                      body,
                      employee.getWork_email(), subjectName + "-For HiringLead"))) {
                    CandidateInterviewProcessHandlerR.writeResponse
                        (httpServerResponse, dbHandle, candidateInterviewEnum.SEND_FAILED, null);
                    logger.info("Candidate Update Interview Mail Sending Failed!");
                  }
                }
              } else {  //mail to Collaborator
                if (colloboratorEmployeeId > 0) {
                  Employee employee = employeeBO.getEmployeeDetails(colloboratorEmployeeId);
                  String body = "";
                  String subjectName = "Interview acceptance by Candidate";
                  if (employee != null) {
                    body = "<html><body>Dear Collaborator, <br> <br>" +
                        "<p> \n" +
                        "You are just one step away from welcoming a new employee! This is to inform you" +
                        " that" + userName + "has accepted the interview invite for the position of " +
                        " " + job.getTitle() + " scheduled for" + interviewProcessCandidate.getInterviewDate() + " " +
                        " at " + interviewProcessCandidate.getInterviewTime() + ". Let's hope that this is the " +
                        " person you are looking for! </p> <br> <br>" +
                        " Thank you,<br>" + organizationName + " Team <br/>" +
                        " </body></html>";

                    if (!"success".equals(mail.sendMail(Vertx.vertx(),
                        body,
                        employee.getWork_email(), subjectName + "-For Collaborator"))) {
                      CandidateInterviewProcessHandlerR.writeResponse
                          (httpServerResponse, dbHandle, candidateInterviewEnum.SEND_FAILED, null);
                      logger.info("Candidate Update Interview Mail Sending Failed!");
                    }
                  }
                }

                //mail for HiringLead

                if (!(null == hiringLeadEmployeeId) && !(hiringLeadEmployeeId.isEmpty())) {
                  Employee employee = employeeBO.getEmployeeDetails(Long.parseLong(hiringLeadEmployeeId));
                  String body = "";
                  String subjectName = "Interview acceptance by Candidate -For HiringLead";
                  if (employee != null) {
                    body = "<html><body>Dear HiringLead, <br> <br>" +
                        "<p> \n" +
                        "You are just one step away from welcoming a new employee! This is to inform you" +
                        " that" + userName + "has accepted the interview invite for the position of " +
                        " " + job.getTitle() + " scheduled for" + interviewProcessCandidate.getInterviewDate() + " " +
                        " at " + interviewProcessCandidate.getInterviewTime() + ". Let's hope that this is the " +
                        " person you are looking for! </p> <br> <br>" +
                        " Thank you, <br>" + organizationName + " Team <br/>" +
                        " </body></html>";

                    if (!"success".equals(mail.sendMail(Vertx.vertx(),
                        body,
                        employee.getWork_email(), subjectName))) {
                      CandidateInterviewProcessHandlerR.writeResponse
                          (httpServerResponse, dbHandle, candidateInterviewEnum.SEND_FAILED, null);
                      logger.info("Candidate Update Interview Mail Sending Failed!");
                    }
                  }
                }
              }
              Employee AdminDetails = employeeBO.getEmployeeDetails(adminId);

              if (AdminDetails != null) {
                String subjectName = "Interview acceptance by Candidate";
                String body = "<html><body>Dear Admin, <br> <br>" +
                    "<p> \n" +
                    "You are just one step away from welcoming a new employee! This is to inform you" +
                    " that" + userName + "has accepted the interview invite for the position of " +
                    " " + job.getTitle() + " scheduled for" + interviewProcessCandidate.getInterviewDate() + " " +
                    " at " + interviewProcessCandidate.getInterviewTime() + ". Let's hope that this is the " +
                    " person you are looking for! </p> <br> <br>" +
                    " Thank you,<br>" + organizationName + " Team <br/>" +
                    " </body></html>";

                if (!"success".equals(mail.sendMail(Vertx.vertx(),
                    body,
                    AdminDetails.getWork_email(), subjectName))) {
                  CandidateInterviewProcessHandlerR.writeResponse
                      (httpServerResponse, dbHandle, candidateInterviewEnum.SEND_FAILED, null);
                  logger.info("Candidate Update Interview Mail Sending Failed!");
                }
              }
              CandidateInterviewProcessHandlerR.writeResponse
                  (httpServerResponse, dbHandle, candidateInterviewEnum.STATUS_SUCCESS, dataObject);
              logger.info("Candidate Interview Updated Successfully!");
            } else {
              JsonObject successObject = new JsonObject();
              InterviewProcessCandidate interviewProcessCandidate =
                  candidateInterviewBO.
                      getInterviewDetails(Long.parseLong(requestParams.get("candidateId")));
              String date = null;
              String currentDate = null;
              String companyAddress = "";
              try {
                date = new DateUtil().
                    convertDateintoString(interviewProcessCandidate.getInterviewDate());
                currentDate = new DateUtil().
                    convertDateintoString(interviewProcessCandidate.getModified());
              } catch (ParseException e) {
                logger.error("Date Format Exception");
                ResponseHelper.writeInternalServerError(httpServerResponse);
              }
              successObject.put("interview_date", date);
              successObject.put("interview_time", interviewProcessCandidate.getInterviewTime());
              successObject.put("interview_end_time", interviewProcessCandidate.getInterviewEndTime());
              successObject.put("interview_duration", interviewProcessCandidate.getInterviewDuration());
              successObject.put("interview_type", interviewProcessCandidate.getInterviewType());
              successObject.put("collaborator_id", interviewProcessCandidate.getCollaboratorId());

              if (interviewProcessCandidate.getInterviewType().
                  equalsIgnoreCase("In Person")) {
                successObject.put("interview_location",
                    interviewProcessCandidate.getInterviewLocation());
              } else {
                successObject.put("interview_details",
                    interviewProcessCandidate.getInterviewDetails());
              }

              successObject.put("interview_mail_description",
                  interviewProcessCandidate.getInterviewMailDescription());
              successObject.put("candidate_id", requestParams.get("candidateId"));
              successObject.put("modified_date", currentDate);
              successObject.put("message", "Interview Scheduled has been updated Successfully");

              String interviewDetails = "";
              String calendarAddress = "";
              if (requestParams.get("interview_type").equalsIgnoreCase("Web")) {
                interviewDetails = requestParams.get("interview_details");
              } else {
                interviewDetails = requestParams.get("interview_location");
                LocationDAO locationDAO = new LocationDAOImpl(dbHandle);
                companyAddress = StringUtil.printValidString(locationDAO.
                    getAddress(interviewDetails, candidateR.getOrganizationId()));
                if (companyAddress.length() == 0) {
                  companyAddress = "";
                  calendarAddress = interviewDetails;
                } else {
                  calendarAddress = interviewDetails + "(" + companyAddress + ")";
                }
              }
              String body = "<html><body><p> Dear " + userName + ",   <br><br>       " +
                  " This is to inform you that you have been shortlisted for an interview for the position of" +
                  " " + job.getTitle() + " at " + organizationName + ". Please " +
                  " <a href='https://" + organization.getHris_url() + "/careers/" + candidateR.getUserId() +
                  "/details/" + Long.parseLong(requestParams.get("candidateId")) + "'>click here</a> to " +
                  " respond to your Interview Details. <br/>" +
                  " Please find the details of the interview below:<br> " +
                  " Date: " + requestParams.get("interview_date") + " <br>" +
                  " Time: " + requestParams.get("interview_time") + " <br>" +
                  " Location/Web interview:" + interviewDetails + " <br> " +
                  " Kindly reach out to the support team if you face any issues." +
                  " We wish you all the very best for the interview! Hope you crack it!<br> </p> <br>" +
                  " " + companyAddress.replace(",", "<br>") + "<br>Thank You,\n<br/>" +
                  " " + organizationName + " Team<br/></p><br>" +
                  "--------------------------------------------------------------\n<br/>" +
                  "This is a system generated mail.\n<br/>" +
                  "Please do not reply to this mail\n<br/>" +
                  "-------------------------------------------------------------- " +
                  "</body></html>";

              // Calendar Invite Event and TimeZone
              Employee employeeRouteModel = (Employee) routingContext.data().get("employee");
              Organization organizationRouteModel = (Organization) routingContext.data().get("organization");
              String timeZone = (org.apache.commons.lang3.StringUtils.isEmpty(employeeRouteModel.getTimeZoneFormat()) ?
                  organizationRouteModel.getTimezone_format() :
                  employeeRouteModel.getTimeZoneFormat()).split(" ")[0];
              ICalendarEvent iCalendarEvent = new ICalendarEventImpl()
                  .setStartDate(requestParams.get("interview_date"), "MM/dd/yyyy")
                  .setEndDate(requestParams.get("interview_date"), "MM/dd/yyyy")
                  .setStartTime(requestParams.get("interview_time"), "hh:mm a")
                  .setEndTime(requestParams.get("interview_end_time"), "hh:mm a")
                  .setTzId(timeZone)
                  .setSummary("Interview Invite from " + organizationName).
                      setOrganizer(organizationName)
                  .setLocation(calendarAddress);
              ICalendarTimeZone iCalendarTimeZone = new ICalendarTimeZoneImpl();

              // Calendar as MailAttachment
              iCalendarEvent.setDescription(body);
              MailAttachment iCalAttachment = new MailAttachment().setName("invite.ics")
                  .setContentType("text/calendar;charset=\"UTF8\"")
                  .setData(Buffer.buffer(new ICalendarImpl().addEvent(iCalendarEvent).setTimeZone(iCalendarTimeZone)
                      .toString().getBytes()));
              List<MailAttachment> mailAttachments = new ArrayList<>();
              mailAttachments.add(iCalAttachment);
              if (!"success".equals(mail.sendMailWithAttachment(Vertx.vertx(),
                  body,
                  candidateR.getEmail(), requestParams.get("subject_name"), mailAttachments))) {
                CandidateInterviewProcessHandlerR.writeResponse
                    (httpServerResponse, dbHandle, candidateInterviewEnum.SEND_FAILED, null);
              }

              long colloboratorEmployeeId = Long.parseLong(requestParams.get("collaborator_id"));
              String hiringLeadEmployeeId = job.getHiring_lead();

              // need to send mail to Hiring Lead when both collaborator and Hiring lead are same
              if (colloboratorEmployeeId == Long.parseLong(hiringLeadEmployeeId)) {
                Employee employee = employeeBO.getEmployeeDetails(colloboratorEmployeeId);
                if (employee != null) {
                  body = "<html><body><p>Dear HiringLead,<br><br><br>" +
                      " This is to inform you that the interview for the position of " + job.getTitle() + " " +
                      "has been rescheduled for " + requestParams.get("interview_date") + " at " +
                      requestParams.get("interview_time") + ".<br><br><br> " +
                      " Thank You,\n<br/>" +
                      " " + organizationName + " Team<br/></p><br>" +
                      "--------------------------------------------------------------\n<br/>" +
                      "This is a system generated mail.\n<br/>" +
                      "Please do not reply to this mail\n<br/>" +
                      "-------------------------------------------------------------- " +
                      "</body></html>";

                  if (!"success".equals(mail.sendMailWithAttachment(Vertx.vertx(),
                      body,
                      employee.getWork_email(), requestParams.get("subject_name") + "-For HiringLead", mailAttachments))) {
                    CandidateInterviewProcessHandlerR.writeResponse
                        (httpServerResponse, dbHandle, candidateInterviewEnum.SEND_FAILED, null);
                    logger.info(" Sending Candidate Interview Update Mail Failed!");
                  }
                }
              } else {
                //mail to Collaborator
                if (colloboratorEmployeeId > 0) {
                  Employee employee = employeeBO.getEmployeeDetails(Long.parseLong(hiringLeadEmployeeId));
                  if (employee != null) {
                    body = "<html><body><p>Dear Collaborator,<br><br><br>" +
                        " This is to inform you that the interview for the position of " + job.getTitle() + " " +
                        "has been rescheduled for " + requestParams.get("interview_date") + " at " +
                        requestParams.get("interview_time") + ".<br><br><br> " +
                        " Thank You,\n<br/>" +
                        " " + organizationName + " Team<br/></p><br>" +
                        "--------------------------------------------------------------\n<br/>" +
                        "This is a system generated mail.\n<br/>" +
                        "Please do not reply to this mail\n<br/>" +
                        "-------------------------------------------------------------- " +
                        "</body></html>";

                    if (!"success".equals(mail.sendMailWithAttachment(Vertx.vertx(),
                        body,
                        employee.getWork_email(), requestParams.get("subject_name") + "-For Collaborator", mailAttachments))) {
                      CandidateInterviewProcessHandlerR.writeResponse
                          (httpServerResponse, dbHandle, candidateInterviewEnum.SEND_FAILED, null);
                      logger.info(" Sending Candidate Interview Update Mail Failed!");
                    }
                  }
                }
                if (!(null == hiringLeadEmployeeId) && !(hiringLeadEmployeeId.isEmpty())) {
                  Employee employee = employeeBO.getEmployeeDetails(Long.parseLong(hiringLeadEmployeeId));
                  if (employee != null) {
                    body = "<html><body><p>Dear HiringLead,<br><br><br>" +
                        " This is to inform you that the interview for the position of " + job.getTitle() + " " +
                        "has been rescheduled for " + requestParams.get("interview_date") + " at " +
                        requestParams.get("interview_time") + ".<br><br><br> " +
                        " Thank You,\n<br/>" +
                        " " + organizationName + " Team <br/></p><br>" +
                        "--------------------------------------------------------------\n<br/>" +
                        "This is a system generated mail.\n<br/>" +
                        "Please do not reply to this mail\n<br/>" +
                        "-------------------------------------------------------------- " +
                        "</body></html>";

                    if (!"success".equals(mail.sendMail(Vertx.vertx(),
                        body,
                        employee.getWork_email(), requestParams.get("subject_name") + "-For HiringLead"))) {
                      CandidateInterviewProcessHandlerR.writeResponse
                          (httpServerResponse, dbHandle, candidateInterviewEnum.SEND_FAILED, null);
                      logger.info(" Sending Candidate Interview Update Mail Failed!");
                    }
                  }
                }
              }
              long adminid = 0;
              long user_id = Long.parseLong(requestParams.get("user_id"));
              Employee employee_user = new EmployeeDAOImpl(dbHandle).getEmployeeDetails(user_id);
              LoginBO loginBO = new LoginBOImpl(dbHandle);
              long organization_id = ((Organization) routingContext.get("organization")).getOrganization_id();
              List<HashMap<String, String>> adminList = loginBO.GetAdminList(organization_id);
              for (HashMap<String, String> map : adminList) {
                for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                  String key = mapEntry.getKey();
                  String value = mapEntry.getValue();
                  if (key.equals("id")) {
                    adminid = Long.parseLong(value);
                  }
                  if (key.equals("email")) {
                    mapEntry.getValue();
                    String AdminEmail = value;
                    if (adminid != employee_user.getUser_id()) {
                      body = "<html><body><p>Dear Admin,<br><br><br>" +
                          " This is to inform you that the interview for the position of " + job.getTitle() + " " +
                          "has been rescheduled for " + requestParams.get("interview_date") + " at " +
                          requestParams.get("interview_time") + ".<br><br><br> " +
                          " Thank You,\n<br/>" +
                          " " + organizationName + " Team <br/></p><br>" +
                          "--------------------------------------------------------------\n<br/>" +
                          "This is a system generated mail.\n<br/>" +
                          "Please do not reply to this mail\n<br/>" +
                          "-------------------------------------------------------------- " +
                          "</body></html>";

                      if (!"success".equals(mail.sendMailWithAttachment(Vertx.vertx(),
                          body,
                          AdminEmail, requestParams.get("subject_name") + "-Admin", mailAttachments))) {
                        CandidateInterviewProcessHandlerR.writeResponse
                            (httpServerResponse, dbHandle, candidateInterviewEnum.SEND_FAILED, null);
                        logger.info(" Sending Candidate Interview Update Mail Failed!");
                      }
                    }
                  }
                }
              }
              CandidateInterviewProcessHandlerR.writeResponse
                  (httpServerResponse, dbHandle, candidateInterviewEnum.UPDATE_STATUS, successObject);
              logger.info("Candidate Interview Updated Successfully!");
            }
          } else if (candidateInterviewEnum == NO_UPDATE) {
            CandidateInterviewProcessHandlerR.writeResponse
                (httpServerResponse, dbHandle, candidateInterviewEnum.NO_UPDATE, null);
            logger.info("Candidate Interview Updated Successfully!");
          } else if (candidateInterviewEnum == COMPLETED_SUCCESS) {
            CandidateInterviewProcessHandlerR.writeResponse
                (httpServerResponse, dbHandle, candidateInterviewEnum.COMPLETED_SUCCESS, null);
            logger.info("Candidate Interview Completed Successfully!");
          } else if (candidateInterviewEnum == ACCESS_CHECK) {
            ResponseHelper.writeErrorResponse(HttpStatus.SC_FORBIDDEN, "You Cannot Consume this Service",
                "You Cannot Consume this Service", httpServerResponse);
            logger.info("Access check for HiringLead/Collaborator/Admin!");
          } else {
            CandidateInterviewProcessHandlerR.writeResponse
                (httpServerResponse, dbHandle, candidateInterviewEnum.UPDATE_FAILED, null);
            logger.info("Candidate Interview Update Failed!");
          }
        });
  }

  public static void getInterviewProcessDetails(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"fields", "organization_id", "limit", "offset", "collaborator_id"};
    String[] requiredParams = {"fields", "organization_id"};

    String[] interviewParams = {"fields", "organization_id", "limit", "offset", "collaborator_id"};

    List<Validator> validators = new ArrayList<>();
    Validator interviewValidator = new ValidatorImpl(interviewParams, InterviewProcessCandidate.getValidatorMap());
    validators.add(interviewValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }
    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          String fieldList = requestParams.get("fields");
          Long organizationId = Long.parseLong(requestParams.get("organization_id"));
          int limit = 0;
          int offset = 0;
          long collaboratorId = 0;

          if (!StringUtil.printValidString(requestParams.get("limit")).equals("")) {
            limit = Integer.parseInt(requestParams.get("limit"));
          }
          if (!StringUtil.printValidString(requestParams.get("limit")).equals("")) {
            offset = Integer.parseInt(requestParams.get("offset"));
          }
          if (!StringUtil.printValidString(requestParams.get("collaborator_id")).equals("")) {
            collaboratorId = Long.parseLong(requestParams.get("collaborator_id"));
          }

          CandidateInterviewBO interviewBO = new CandidateInterviewBOImpl(dbHandle);
          String dateFormat = ((Organization) routingContext.data().get("organization")).getDate_format();

          JsonArray interviewArray = new JsonArray();

          List<HashMap<String, String>> interviewProcessCandidate =
              interviewBO.getListOfInterviewDetails(dateFormat, fieldList, organizationId, limit, offset, collaboratorId);
          List<HashMap<String, String>> CountList =
              interviewBO.getListOfInterviewDetails(dateFormat, fieldList, organizationId, 0, 0, collaboratorId);
          int totalCount = CountList.size();
          try {
            for (HashMap<String, String> map : interviewProcessCandidate) {
              JsonObject dataObject = new JsonObject();
              for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                String key = mapEntry.getKey();
                String value = mapEntry.getValue();
                dataObject.put(key, value);
              }
              interviewArray.add(dataObject);
            }
          } catch (Exception e) {
            e.printStackTrace();
          }

          JsonObject jsonSuccessObject = new JsonObject();
          if (interviewArray.size() == 0) {
            JsonObject dumpData = new JsonObject();
            dumpData.put("interview_date", "30-Jan-2018");
            dumpData.put("interview_time", "08:00 pm");
            dumpData.put("interview_duration", "45 minutes");
            dumpData.put("job_title", "Developer");
            dumpData.put("interview_location", "Web");
            dumpData.put("first_name", "John");
            dumpData.put("last_name", "Smith");
            dumpData.put("candidate_id", "0");
            dumpData.put("collaborator_id", "0");
            dumpData.put("sub_status", "Schedule Interview");
            dumpData.put("job_id", "0");
            interviewArray.add(dumpData);
            jsonSuccessObject.put("interview", interviewArray);
            jsonSuccessObject.put("totalcount", 0);
            jsonSuccessObject.put("no_data", true);
            CandidateInterviewProcessHandlerR.writeResponse
                (httpServerResponse, dbHandle, GET_INTERVIEW_SUCCESS, jsonSuccessObject);
            logger.info("No Data Available");
          } else {
            jsonSuccessObject.put("interview", interviewArray);
            jsonSuccessObject.put("totalcount", totalCount);
            jsonSuccessObject.put("no_data", false);
            CandidateInterviewProcessHandlerR.writeResponse
                (httpServerResponse, dbHandle, GET_INTERVIEW_SUCCESS, jsonSuccessObject);
            logger.info("Interview Process Details Fetched Successfully");
          }
        });

  }

}