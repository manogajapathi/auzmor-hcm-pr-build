package com.auzmor.impl.handler.Candidate;

import com.auzmor.bo.CandidateBO;
import com.auzmor.bo.CandidateCommentBO;
import com.auzmor.bo.CandidateHiringProcessBO;
import com.auzmor.impl.bo.CandidateBOImpl;
import com.auzmor.impl.bo.CandidateCommentBOImpl;
import com.auzmor.impl.bo.CandidateHiringProcessBOImpl;
import com.auzmor.impl.enumarator.bo.CandidateCommentEnum;
import com.auzmor.impl.enumarator.bo.CandidateHiringProcessEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.models.CandidateR.CommentCandidate;
import com.auzmor.impl.models.CandidateR.HiringProcessCandidate;
import com.auzmor.impl.util.DateUtil;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.Validator;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static com.auzmor.impl.enumarator.bo.CandidateHiringProcessEnum.FAILED;
import static com.auzmor.impl.enumarator.bo.CandidateHiringProcessEnum.SUCCESS;

public class CandidateHiringProcessHandlerR {

  private static Logger logger = LoggerFactory.getLogger(CandidateHiringProcessHandlerR.class);

  public static void addHiringProcessData(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"status", "candidate_process", "candidate_id", "admin_name",
        "reason", "user_id"};
    String[] requiredParams = {"status", "candidate_id"};
    String[] hiringParams = {"status", "candidate_process", "candidate_id",
        "admin_name", "reason", "user_id"};

    List<Validator> validators = new ArrayList<>();
    Validator inviteValidator = new ValidatorImpl(hiringParams, HiringProcessCandidate.getValidatorMap());
    validators.add(inviteValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {

          HiringProcessCandidate candidateHiringProcessModel = new HiringProcessCandidate();
          candidateHiringProcessModel.setStatus(requestParams.get("status"));
          candidateHiringProcessModel.setCandidateId(Long.parseLong(requestParams.get("candidate_id")));
          if (!StringUtil.printValidString(requestParams.get("candidate_process")).equals("")) {
            candidateHiringProcessModel.setCandidateProcess(requestParams.get("candidate_process"));
          }
          if (!StringUtil.printValidString(requestParams.get("admin_name")).equals("")) {
            candidateHiringProcessModel.setAdminName(requestParams.get("admin_name"));
          } else {
            candidateHiringProcessModel.setAdminName("HiringLead");   // need to confirm once with qa.
          }
          if (!StringUtil.printValidString(requestParams.get("reason")).equals("")) {
            candidateHiringProcessModel.setReason(requestParams.get("reason"));
          }
          if (!StringUtil.printValidString(requestParams.get("user_id")).equals("")) {
            candidateHiringProcessModel.setUserId(Long.parseLong(requestParams.get("user_id")));
          } else {
            candidateHiringProcessModel.setUserId((long) 0);
          }

          CandidateHiringProcessBO candidateHiringProcessBO = new CandidateHiringProcessBOImpl(dbHandle);

          HiringProcessCandidate hiringProcessCandidate;
          hiringProcessCandidate = candidateHiringProcessBO.
              getHiringProcess(Long.parseLong(requestParams.get("candidate_id")));

          String comments = "moved from " + hiringProcessCandidate.getCandidateProcess() + " to "
              + requestParams.get("candidate_process");

          CandidateHiringProcessEnum candidateHiringProcessEnum =
              candidateHiringProcessBO.insertCandidateStatus(dbHandle, candidateHiringProcessModel);

          if (candidateHiringProcessEnum == SUCCESS) {

            CommentCandidate commentsModel = new CommentCandidate();
            commentsModel.setAdminName(StringUtil.printValidString(requestParams.get("admin_name")));
            commentsModel.setComments(comments);
            commentsModel.setSystemGenerated(true);

            java.util.Date dNow = new java.util.Date();
            String currentDate = new DateUtil().getDate().format(dNow);
            try {
              commentsModel.setCommentsDate(new DateUtil().getDate().parse(currentDate));
            } catch (ParseException e) {
              e.printStackTrace();
            }
            commentsModel.setCandidateId(Long.parseLong(requestParams.get("candidate_id")));
            if (!requestParams.contains("user_id"))
              commentsModel.setUserId(0);
            else
              commentsModel.setUserId(Long.parseLong(requestParams.get("user_id")));

            CandidateCommentBO candidateCommentBO = new CandidateCommentBOImpl(dbHandle);
            CandidateCommentEnum candidateCommentEnum = candidateCommentBO.
                addCommentDetails(commentsModel, true);

            if (candidateCommentEnum == candidateCommentEnum.SUCCESS) {

              CandidateBO candidateBO = new CandidateBOImpl(dbHandle);
              candidateBO.sendStatusUpdatesToCollaborator(hiringProcessCandidate.getStatus(),
                  candidateHiringProcessModel.getStatus(),
                  StringUtil.printValidString(requestParams.get("candidate_process")),
                  Integer.parseInt(requestParams.get("candidate_id")));
              candidateBO.sendStatusUpdatesToHiringLead(hiringProcessCandidate.getStatus(),
                  candidateHiringProcessModel.getStatus(),
                  StringUtil.printValidString(requestParams.get("candidate_process")),
                  Integer.parseInt(requestParams.get("candidate_id")));
              candidateBO.sendStatusUpdatesToAdmin(hiringProcessCandidate.getStatus(),
                  candidateHiringProcessModel.getStatus(),
                  StringUtil.printValidString(requestParams.get("candidate_process")),
                  Integer.parseInt(requestParams.get("candidate_id")), Long.parseLong(requestParams.get("user_id")));

              JsonObject dataObject = new JsonObject();
              dataObject.put("message", "Candidate status updated successfully");

              CandidateHiringProcessHandlerR.writeResponse
                  (httpServerResponse, dbHandle, SUCCESS, dataObject);
            } else {
              logger.error("creating hiring process");
              CandidateHiringProcessHandlerR.writeResponse
                  (httpServerResponse, dbHandle, FAILED, null);
            }
          } else {
            logger.error("creating hiring process");
            CandidateHiringProcessHandlerR.writeResponse
                (httpServerResponse, dbHandle, FAILED, null);
          }
        });
  }

  public static void writeResponse(HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   CandidateHiringProcessEnum candidateHiringProcessEnum,
                                   JsonObject jsonObject)
      throws SQLException, DbHandleException {

    switch (candidateHiringProcessEnum) {
      case FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_CONFLICT,
            "Candidate Status Insertion Failed",
            "Insertion Failed for HiringProcess",
            httpServerResponse);
        logger.error("Candidate Status Insertion Failed");
        break;
      case SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Candidate status updated successfully",
            jsonObject, httpServerResponse);
        logger.info("Candidate status updated successfully");
        break;
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        logger.error("Internal Server Error!");
        break;
    }
  }
}
