package com.auzmor.impl.handler.Candidate;

import com.auzmor.bo.*;
import com.auzmor.dao.CandidateDAO;
import com.auzmor.impl.bo.*;
import com.auzmor.impl.constant.file.endpoint.EndpointConstant;
import com.auzmor.impl.dao.CandidateDAOImpl;
import com.auzmor.impl.enumarator.bo.CandidateCommentEnum;
import com.auzmor.impl.enumarator.bo.CandidateHiringProcessEnum;
import com.auzmor.impl.enumarator.bo.CandidateOfferEnum;
import com.auzmor.impl.enumarator.file.FileTypeEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.*;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.impl.models.CandidateR.CommentCandidate;
import com.auzmor.impl.models.CandidateR.HiringProcessCandidate;
import com.auzmor.impl.models.CandidateR.OfferProcessCandidate;
import com.auzmor.impl.util.DateUtil;
import com.auzmor.impl.util.Mail;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.Validator;
import com.mysql.cj.core.util.StringUtils;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static com.auzmor.impl.enumarator.bo.CandidateHiringProcessEnum.SUCCESS;
import static com.auzmor.impl.enumarator.bo.CandidateOfferEnum.SEND_FAILED;

public class CandidateOfferProcessHandlerR {
  private static Logger logger = LoggerFactory.getLogger(CandidateOfferProcessHandlerR.class);

  public static void addOfferProcess(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"candidate_id", "offer_position", "offer_pay_rate",
        "offer_start_date", "offer_job_location", "offer_contract_details",
        "offer_attachment_name", "admin_name", "subject_name", "user_id", "currency_type","sub_status"};
    String[] requiredParams = {"candidate_id", "offer_position", "offer_pay_rate",
        "offer_start_date", "offer_job_location", "offer_attachment_name", "admin_name", "subject_name","sub_status"};
    String[] offerParams = {"candidate_id", "offer_position", "offer_pay_rate",
        "offer_start_date", "offer_job_location", "offer_contract_details",
        "offer_attachment_name", "admin_name", "subject_name", "user_id", "currency_type","sub_status"};

    List<Validator> validators = new ArrayList<>();
    Validator offerValidator = new ValidatorImpl(offerParams, OfferProcessCandidate.getValidatorMap());
    validators.add(offerValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }
    logger.info("All Params are Validate");
    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {

          CandidateBO candidateBO = new CandidateBOImpl(dbHandle);
          String existingStatus = candidateBO.getCandidate(dbHandle,
              Long.parseLong(requestParams.get("candidate_id"))).getStatus();

          /*----------------checking access role for admin/HiringLead ---------------------*/
          String userType = routingContext.data().get("userType").toString();

          CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
          JobBO jobBO = new JobBOImpl(dbHandle);
          long candidateId = Long.parseLong(requestParams.get("candidate_id"));
          int hiringLead = 0;

          CandidateR candidateR = candidateDAO.getCandidate(candidateId);
          Job job = jobBO.getJobDetails(candidateR.getJobId());

          // checking userType not equal to admin
          if (!userType.equalsIgnoreCase("admin")) {

            int employeeId = ((Employee) routingContext.data().get("employee")).getEmployee_id();

            String hiringLeadId = job.getHiring_lead();

            if (!StringUtils.isNullOrEmpty(hiringLeadId)) {
              hiringLead = Integer.parseInt(hiringLeadId);
            }
            // checking hiringLead whether employeeId had access to this job.
            if (employeeId != hiringLead && hiringLead > 0) {
              ResponseHelper.writeErrorResponse(HttpStatus.SC_FORBIDDEN, "You Cannot Consume this Service",
                  "You Cannot Consume this Service", httpServerResponse);
              logger.info("Access check for HiringLead/Collaborator/Admin!");
            }
          }

          CandidateOfferBO candidateOfferBO = new CandidateOfferBOImpl(dbHandle);
          long offerId = candidateOfferBO.addOfferProcess(requestParams);
          CandidateOfferEnum offerEnum = null;
          if (offerId > 0) {
            logger.info("Offer Details are inserted Successfully");
            HiringProcessCandidate candidateHiringProcessModel = new HiringProcessCandidate();

            candidateHiringProcessModel.setStatus("Offer");
            candidateHiringProcessModel.setCandidateProcess(requestParams.get("sub_status"));
            candidateHiringProcessModel.setCandidateId(Long.parseLong(requestParams.get("candidate_id")));
            candidateHiringProcessModel.setReason("");
            candidateHiringProcessModel.setUserId(Long.parseLong(requestParams.get("user_id")));

            CandidateHiringProcessBO candidateHiringProcessBO = new CandidateHiringProcessBOImpl(dbHandle);
            CandidateHiringProcessEnum candidateHiringProcessEnum = candidateHiringProcessBO.
                insertCandidateStatus(dbHandle, candidateHiringProcessModel);

            if (candidateHiringProcessEnum == SUCCESS) {
              Date dNow = new Date();
              String currentDate = new DateUtil().getDate().format(dNow);
              String comments = "Offer Extended";
              CommentCandidate commentCandidate = new CommentCandidate();
              commentCandidate.setAdminName(requestParams.get("admin_name"));
              commentCandidate.setComments(comments);
              commentCandidate.setUserId(Long.parseLong(requestParams.get("user_id")));
              commentCandidate.setSystemGenerated(true);
              try {
                commentCandidate.setCommentsDate(new DateUtil().getDate().parse(currentDate));
              } catch (ParseException e) {
                e.printStackTrace();
              }
              commentCandidate.setCandidateId(Long.parseLong(requestParams.get("candidate_id")));
              CandidateCommentBO candidateCommentBO = new CandidateCommentBOImpl(dbHandle);
              CandidateCommentEnum candidateCommentEnum = candidateCommentBO.
                  addCommentDetails(commentCandidate, true);

              /* candidate status update */
              candidateBO.updateStatus("Offer",
                  Long.parseLong(requestParams.get("candidate_id")));
              OfferProcessCandidate offerProcessCandidate = candidateOfferBO.
                  getOfferDetails(Long.parseLong(requestParams.get("candidate_id")));
              String subjectName = "Re:Offer Letter";


              CandidateOfferEnum candidateOfferEnum = candidateOfferBO.
                  mailSent(Long.parseLong(requestParams.get("candidate_id")), subjectName);
              if (candidateOfferEnum == SEND_FAILED) {
                CandidateOfferProcessHandlerR.writeResponse
                    (httpServerResponse, dbHandle, SEND_FAILED, null);
              } else {
                JsonObject successObject = new JsonObject();
                String offerDate = new DateUtil().getDate().
                    format(offerProcessCandidate.getOfferStartDate());

                successObject.put("offer_pay_rate", offerProcessCandidate.getOfferPayRate());
                successObject.put("offer_position", offerProcessCandidate.getOfferPosition());
                successObject.put("offer_start_date", offerDate);
                successObject.put("offer_job_location", offerProcessCandidate.getOfferJobLocation());
                successObject.put("currency_type", offerProcessCandidate.getCurrencyType());

                if (offerProcessCandidate.getOfferContractDetails().length() == 0 ||
                    StringUtil.printValidString(offerProcessCandidate.getOfferContractDetails()).equals("")) {
                  successObject.put("offer_contract_details", "-");
                } else {
                  successObject.put("offer_contract_details", offerProcessCandidate.getOfferContractDetails());
                }
                successObject.put("offer_attachment_name", offerProcessCandidate.getOfferAttachmentName());
                try {
                  successObject.put("offer_modified_date", new DateUtil().
                      convertDateintoString(offerProcessCandidate.getCreated()));
                } catch (ParseException e) {
                  logger.error("Date Format Expection");
                  ResponseHelper.writeInternalServerError(httpServerResponse);
                }
                successObject.put("subject_name", offerProcessCandidate.getSubjectName());
                successObject.put("admin_name", offerProcessCandidate.getAdminName());
                successObject.put("message", "Offer Extended Successfully!");

                candidateBO.sendStatusUpdatesToCollaborator(existingStatus,
                    candidateHiringProcessModel.getStatus(), "",
                    Integer.parseInt(requestParams.get("candidate_id")));
                candidateBO.sendStatusUpdatesToHiringLead(existingStatus,
                    candidateHiringProcessModel.getStatus(), "",
                    Integer.parseInt(requestParams.get("candidate_id")));
                candidateBO.sendStatusUpdatesToAdmin(existingStatus,
                    candidateHiringProcessModel.getStatus(),
                    StringUtil.printValidString(requestParams.get("candidate_process")),
                    Integer.parseInt(requestParams.get("candidate_id")), Long.parseLong(requestParams.get("user_id")));

                CandidateOfferProcessHandlerR.writeResponse
                    (httpServerResponse, dbHandle, offerEnum.SUCCESS, successObject);
              }
            }
          } else {
            logger.info("Offer Details are inserted failed");
            CandidateOfferProcessHandlerR.writeResponse
                (httpServerResponse, dbHandle, offerEnum.FAILED, null);
          }
        });
  }

  public static void writeResponse(HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   CandidateOfferEnum candidateOfferEnum, JsonObject jsonObject)
      throws SQLException, DbHandleException {
    switch (candidateOfferEnum) {
      case FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_CONFLICT,
            "Offer Extended has been Failed",
            "Offer Extended has been Failed ",
            httpServerResponse);
        break;
      case UPDATE_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Offer Extended Update has been Failed",
            "Offer Extended Update has been Failed ",
            httpServerResponse);
        break;
      case SEND_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_NOT_ACCEPTABLE,
            "Sending Offer Extended Details has been Failed",
            "Sending Mail has been Failed",
            httpServerResponse);
        break;
      case SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Offer has been Extended Successfully", jsonObject, httpServerResponse);
        break;
      case STATUS_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Interview Scheduled has been updated Successfully", jsonObject, httpServerResponse);
        break;
      case STATUS_REJECT:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Offer Extended has been updated  Successfully", jsonObject, httpServerResponse);
        break;
      case UPDATE_STATUS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Offer Extended has been updated  Successfully", jsonObject, httpServerResponse);
        break;
      case NO_UPDATE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "No Update is Done!", jsonObject, httpServerResponse);
        break;
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }

  public static void updateOfferProcess(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"candidateId", "offer_position", "offer_pay_rate",
        "offer_start_date", "offer_job_location", "offer_contract_details",
        "offer_attachment_name", "admin_name", "subject_name", "offer_decline_reason",
        "status", "user_id", "offer_other_reason", "currency_type","sub_status"};
    String[] requiredParams = {"candidateId"};
    String[] offerParams = {"candidateId", "offer_position", "offer_pay_rate",
        "offer_start_date", "offer_job_location", "offer_contract_details",
        "offer_attachment_name", "admin_name", "subject_name", "offer_decline_reason",
        "status", "user_id", "offer_other_reason", "currency_type","sub_status"};

    List<Validator> validators = new ArrayList<>();
    Validator offerValidator = new ValidatorImpl(offerParams, OfferProcessCandidate.getValidatorMap());
    validators.add(offerValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }
    logger.info("All params are validate");
    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          long candidateId = Long.parseLong(requestParams.get("candidateId"));
          CandidateOfferBO candidateOfferBO = new CandidateOfferBOImpl(dbHandle);
          try {
            CandidateOfferEnum candidateOfferEnum = candidateOfferBO.updateOfferProcess(requestParams,routingContext);

            if (candidateOfferEnum == candidateOfferEnum.SUCCESS) {
              logger.info("candidate offer process updated");
              JsonObject dataObject = new JsonObject();
              CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
              CandidateR candidateR = candidateDAO.getCandidate(candidateId);
              OfferProcessCandidate offerProcessCandidate = candidateOfferBO.getOfferDetails(candidateId);
              String organizationName = ((Organization) routingContext.data().get("organization")).getOrganization_name();
              organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);

              if (!StringUtil.printValidString(requestParams.get("offer_decline_reason")).equals("") &&
                  !StringUtil.printValidString(requestParams.get("status")).equals("")) {

                dataObject.put("message", "Thank you for your response." +
                    " We are disappointed that you are not satisfied," +
                    " but will take feedback into consideration! " +
                    " If necessary,someone from our company will reach out to you.");

                CandidateOfferProcessHandlerR.writeResponse
                    (httpServerResponse, dbHandle, candidateOfferEnum.STATUS_REJECT, dataObject);

              } else if (!StringUtil.printValidString(requestParams.get("offer_decline_reason")).equals("")) {
                String userName = candidateR.getFirstName() + " " + candidateR.getLastName();
                String body = "<html><body>Hello <b>" + userName + "</b>, <br> <br>" +
                    "<p>Offer Extended was cancelled by the company </p> <br><br> <br>" +
                    "Thank you, <br> " + organizationName + " Team <br/> " +
                    " </body></html>";

                Mail mail = new Mail();
                String subjectName = "Rejection of offer";
                if (!"success".equals(mail.sendMail(Vertx.vertx(),
                    body,
                    candidateR.getEmail(), subjectName))) {
                  CandidateOfferProcessHandlerR.writeResponse
                      (httpServerResponse, dbHandle, SEND_FAILED, null);
                }
                dataObject.put("message", "Offer Extended has been updated Successfully");
                logger.info("Offer Extended has been updated Successfully");
                CandidateOfferProcessHandlerR.writeResponse
                    (httpServerResponse, dbHandle, candidateOfferEnum.UPDATE_STATUS, dataObject);
              } else if (StringUtil.printValidString(requestParams.get("status")).equals("accept")) {
                dataObject.put("message", "Thank you for your response! " +
                    " We look forward to speaking with you!");
                logger.info("Offer Accepted");
                CandidateOfferProcessHandlerR.writeResponse
                    (httpServerResponse, dbHandle, candidateOfferEnum.STATUS_SUCCESS, dataObject);
              } else {
                logger.info("Offer Extended");
                String subjectName = "Re:Offer Extended";
                candidateOfferEnum = candidateOfferBO.
                    mailSent(Long.parseLong(requestParams.get("candidateId")), subjectName);
                if (candidateOfferEnum == SEND_FAILED) {
                  CandidateOfferProcessHandlerR.writeResponse
                      (httpServerResponse, dbHandle, SEND_FAILED, null);
                } else {
                  JsonObject successObject = new JsonObject();
                  String date = "";
                  String currentDate = "";
                  try {
                    date = new DateUtil().
                        convertDateintoString(offerProcessCandidate.getOfferStartDate());
                    currentDate = new DateUtil().
                        convertDateintoString(offerProcessCandidate.getModified());
                  } catch (ParseException e) {
                    logger.error("Date Format Expection");
                    ResponseHelper.writeInternalServerError(httpServerResponse);
                  }
                  successObject.put("candidate_id", candidateId);
                  successObject.put("modified_date", currentDate);
                  if (!StringUtil.printValidString(offerProcessCandidate.getOfferDeclineReason()).equals("")) {
                    successObject.put("offer_decline_reason", offerProcessCandidate.getOfferDeclineReason());
                  } else {
                    successObject.put("offer_pay_rate", offerProcessCandidate.getOfferPayRate());
                    successObject.put("offer_position", offerProcessCandidate.getOfferPosition());
                    successObject.put("offer_start_date", date);
                    successObject.put("offer_job_location", offerProcessCandidate.getOfferJobLocation());
                    successObject.put("currency_type", offerProcessCandidate.getCurrencyType());

                    if (offerProcessCandidate.getOfferContractDetails().length() == 0 ||
                        StringUtil.printValidString(offerProcessCandidate.getOfferContractDetails()).equals("")) {
                      successObject.put("offer_contract_details", "-");
                    } else {
                      successObject.put("offer_contract_details", offerProcessCandidate.getOfferContractDetails());
                    }
                    successObject.put("offer_attachment_name", offerProcessCandidate.getOfferAttachmentName());
                    successObject.put("offer_decline_reason", "-");
                  }
                  successObject.put("message", "Offer Extended has been updated Successfully");

                  CandidateOfferProcessHandlerR.writeResponse
                      (httpServerResponse, dbHandle, candidateOfferEnum.UPDATE_STATUS, successObject);
                }
              }
            } else if (candidateOfferEnum == candidateOfferEnum.NO_UPDATE) {
              logger.info("candidate offer process No update");
              CandidateOfferProcessHandlerR.writeResponse
                  (httpServerResponse, dbHandle, candidateOfferEnum.NO_UPDATE, null);
            }
            else if (candidateOfferEnum == candidateOfferEnum.ACCESS_CHECK) {
              ResponseHelper.writeErrorResponse(HttpStatus.SC_FORBIDDEN, "You Cannot Consumes this Service",
                  "You Cannot Consumes this Service", httpServerResponse);
            } else {
              logger.info("candidate offer process updated Failed");
              CandidateOfferProcessHandlerR.writeResponse
                  (httpServerResponse, dbHandle, candidateOfferEnum.UPDATE_FAILED, null);
            }
          } catch (Exception e) {
            e.printStackTrace();
          }
        });
  }

  public static void addOfferAttachment(RoutingContext routingContext) {
    logger.info("adding attachment");
    Set<FileUpload> fileUploadSet = routingContext.fileUploads();
    for (FileUpload fileUpload : fileUploadSet) {
      FileTypeEnum fileTypeEnum = FileHelper.getFileType(fileUpload);
      if (FileUploadHelper.isSupportedDocFormat(fileTypeEnum)) {

        String sourceFile = fileUpload.uploadedFileName();
        String destinationFile = fileUpload.uploadedFileName().split(File.separator)[1] +
            fileTypeEnum.getExtension();
        String destinationFolderPath = EndpointConstant.publicStaticImage + File.separator +
            routingContext.request().getParam("organization_id");

        FileHelper.move(sourceFile, destinationFolderPath, destinationFile);
        routingContext.response()
            .setStatusCode(HttpStatus.SC_CREATED)  // 201
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .end(destinationFolderPath + File.separator + destinationFile);
        logger.info("uploading attachment successfully");
      } else {
        logger.info("You can only upload PDF/DOC/DOCX file!");
        logger.error("You can only upload PDF/DOC/DOCX file!");
        ResponseHelper.writeInternalServerError(routingContext.response());
      }
    }
  }
}
