package com.auzmor.impl.handler;

import com.auzmor.impl.enumarator.bo.CountryEnum;
import com.auzmor.impl.helper.ConfigurationJsonHelper;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;

import java.io.FileNotFoundException;
import java.util.Locale;

public class CountryStateHandler {

  private static final Logger logger = LoggerFactory.getLogger(CountryStateHandler.class);

  public static void getCountriesList(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          JsonArray jsonArray;
          try {
            JsonObject vertxConfig = ConfigurationJsonHelper.getConfForJsonFileJava("countries.json");
            logger.info("reading the countries file");
            jsonArray = vertxConfig.getJsonArray("countries");
            JsonArray countryArray = new JsonArray();
            for (int i = 0; i < jsonArray.size(); i++) {
              JsonObject jsonObject = jsonArray.getJsonObject(i);
              String id = jsonObject.getString("id");
              String sortname = jsonObject.getString("sortname");
              String name = jsonObject.getString("name");
              JsonObject stateObject = new JsonObject();
              stateObject.put("countryId", id);
              stateObject.put("countrySortName", sortname);
              stateObject.put("countryName", name);
              countryArray.add(stateObject);
            }
            JsonObject countryObject = new JsonObject();
            countryObject.put("countries_list", countryArray);
            if (countryArray.size() > 0) {
              logger.info("get the country list");
              CountryStateHandler.writeResponse
                  (httpServerResponse, CountryEnum.SUCCESS, countryObject);
            } else if (countryArray.size() == 0) {
              logger.info("no country list");
              JsonObject jsonObject = new JsonObject();
              jsonObject.put("code", HttpStatus.SC_OK);
              jsonObject.put("success", true);
              jsonObject.put("data", countryObject);
              jsonObject.put("no_data", true);
              CountryStateHandler.writeResponse
                  (httpServerResponse, CountryEnum.NO_DATA_STATUS, jsonObject);
            } else {
              logger.info("get the country list failed");
              CountryStateHandler.writeResponse
                  (httpServerResponse, CountryEnum.FAILED, null);
            }
          } catch (FileNotFoundException e) {
            e.printStackTrace();
          }
        }
    );
  }

  public static void getStatesNameswithCountryId(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    MultiMap params = request.params();
    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          JsonArray jsonArray;
          try {
            JsonObject vertxConfig = ConfigurationJsonHelper.getConfForJsonFileJava("states.json");
            logger.info("reading states file");
            jsonArray = vertxConfig.getJsonArray("states");
            JsonArray stateArray = new JsonArray();

            for (int i = 0; i < jsonArray.size(); i++) {
              JsonObject jsonObject = jsonArray.getJsonObject(i);

              String countryId = jsonObject.getString("country_id");
              if (countryId.equals(params.get("countryId"))) {
                String id = jsonObject.getString("id");
                String name = jsonObject.getString("name");
                JsonObject stateObject = new JsonObject();
                stateObject.put("stateId", id);
                stateObject.put("stateName", name);
                stateArray.add(stateObject);
              }
            }
            JsonObject countryWithState = new JsonObject();
            countryWithState.put("states", stateArray);
            if (stateArray.size() > 0) {
              logger.info("getting the states for the country id");
//              countryWithState.put("no_data", false);
              CountryStateHandler.writeResponse
                  (httpServerResponse, CountryEnum.SUCCESS, countryWithState);
            } else if (stateArray.size() == 0) {
              logger.info("No states for the country id");
              JsonObject jsonObject = new JsonObject();
              jsonObject.put("code", HttpStatus.SC_OK);
              jsonObject.put("success", true);
              jsonObject.put("data", countryWithState);
              jsonObject.put("no_data", true);
              CountryStateHandler.writeResponse
                  (httpServerResponse, CountryEnum.NO_DATA_STATUS, jsonObject);
            } else {
              logger.info("getting the states for the country id failed");
              CountryStateHandler.writeResponse
                  (httpServerResponse, CountryEnum.FAILED, null);
            }
          } catch (FileNotFoundException e) {
            e.printStackTrace();
          }
        });
  }

  /**
   * @author sadhanaVenkatesan
   * @param routingContext
   */
  public static void getStatesNamesWithCountryRegionCode(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    MultiMap params = request.params();
    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          JsonArray jsonArray;
          try {
            JsonObject vertxConfig = ConfigurationJsonHelper.getConfForJsonFileJava("countries.json");
            logger.info("reading countries json file");
            jsonArray = vertxConfig.getJsonArray("countries");
            JsonArray stateArray = new JsonArray();
            String countryId = "";
            for (int i = 0; i < jsonArray.size(); i++) {
              JsonObject jsonObject = jsonArray.getJsonObject(i);

              String countryRegionCode = jsonObject.getString("sortname");
              if (countryRegionCode.equals(params.get("country_region_code"))) {
                countryId = jsonObject.getString("id");
                logger.info("getting country id from countries.json");
              }
            }

            // taking country id from country and then taking states

            if (countryId != null) {
              JsonObject vertxConfigState = ConfigurationJsonHelper.getConfForJsonFileJava("states.json");
              jsonArray = vertxConfigState.getJsonArray("states");
              logger.info("reading states json file");
              for (int j = 0; j < jsonArray.size(); j++) {
                JsonObject stateJsonObject = jsonArray.getJsonObject(j);

                if (countryId.equals(stateJsonObject.getString("country_id"))) {
                  String id = stateJsonObject.getString("id");
                  String name = stateJsonObject.getString("name");
                  String countryID = stateJsonObject.getString("country_id");
                  JsonObject stateObject = new JsonObject();
                  stateObject.put("stateId", id);
                  stateObject.put("stateName", name);
                  stateObject.put("countryId", countryID);
                  stateArray.add(stateObject);
                }
              }
            }

            JsonObject countryWithState = new JsonObject();
            countryWithState.put("states", stateArray);
            if (stateArray.size() > 0) {
              logger.info("getting the states for the country region code");
//              countryWithState.put("no_data", false);
              CountryStateHandler.writeResponse
                  (httpServerResponse, CountryEnum.SUCCESS, countryWithState);
            } else if (stateArray.size() == 0) {
              logger.info("No states for the country region code");
              JsonObject jsonObject = new JsonObject();
              jsonObject.put("code", HttpStatus.SC_OK);
              jsonObject.put("success", true);
              jsonObject.put("data", countryWithState);
              jsonObject.put("no_data", true);
              CountryStateHandler.writeResponse
                  (httpServerResponse, CountryEnum.NO_DATA_STATUS, jsonObject);
            } else {
              logger.info("getting the states for the country id failed");
              CountryStateHandler.writeResponse
                  (httpServerResponse, CountryEnum.FAILED, null);
            }
          } catch (FileNotFoundException e) {
            ResponseHelper.writeInternalServerError(httpServerResponse);
          }
        });
  }

  /* getting state name*/
  public static String getStateName(String stateIdbyName) {
    String stateName = "";
    JsonArray jsonArray;
    try {
      JsonObject vertxConfig = ConfigurationJsonHelper.getConfForJsonFileJava("states.json");
      jsonArray = vertxConfig.getJsonArray("states");

      for (int i = 0; i < jsonArray.size(); i++) {
        JsonObject jsonObject = jsonArray.getJsonObject(i);

        String stateId = jsonObject.getString("id");
        if (stateId.equals(stateIdbyName)) {
          stateName = jsonObject.getString("name");
        }
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    return stateName;
  }


  /* searching the state id by it */
  public static String getStateIdWithLikeCondition(String stateName) {
    String stateNameValues = "";
    String stateId = "";
    JsonArray jsonArray;
    int j=1;
    try {
      JsonObject vertxConfig = ConfigurationJsonHelper.getConfForJsonFileJava("states.json");
      jsonArray = vertxConfig.getJsonArray("states");

      for (int i = 0; i < jsonArray.size(); i++) {
        JsonObject jsonObject = jsonArray.getJsonObject(i);
        stateNameValues = jsonObject.getString("name");
        if (StringUtils.containsIgnoreCase(stateNameValues, stateName) == true) {
          if (j - 1 > 0) {
            stateId+=(",");
          }
          stateId += jsonObject.getString("id");
          j = j + 1 ;
        }
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    return stateId;
  }

  public static void getCallingCode(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    JsonArray resultArray = new JsonArray();
    PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
    for (Integer callingCode : phoneNumberUtil.getSupportedCallingCodes()) {
      JsonObject dataObject = new JsonObject();
      dataObject.put("calling_code", "+" + callingCode);
      String regionCode = phoneNumberUtil.getRegionCodeForCountryCode(callingCode);
      dataObject.put("region_code", regionCode);
      Locale locale = new Locale("", regionCode);
      dataObject.put("country_name", locale.getDisplayCountry());
      resultArray.add(dataObject);
    }
    ResponseHelper.writeArraySuccessResponse(200, "no_data is false",
        resultArray, response);

  }

  public static void writeResponse(HttpServerResponse httpServerResponse,
                                   CountryEnum countryEnum, JsonObject jsonObject) {

    switch (countryEnum) {
      case FAILED:
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Not Getting states details,Please try again !",
            "Not Getting states details,Please try again !",
            httpServerResponse);
        break;
      case SUCCESS:
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Getting states details from CountryId",
            jsonObject, false, httpServerResponse);
        break;
      case NO_DATA_STATUS:
        ResponseHelper.writeSuccessNoDataResponse(HttpStatus.SC_OK,
            jsonObject, httpServerResponse);
        break;
      default:
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }
}