package com.auzmor.impl.handler;

import com.auzmor.bo.NewsFeedBO;
import com.auzmor.impl.bo.NewsFeedBOImpl;
import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.enumarator.NewsFeedEnum;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.util.ResponseUtil;

import com.auzmor.impl.builder.ApiBuilder;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.validator.FieldOptions;
import com.auzmor.util.validator.Validator;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;

import java.io.IOException;
import java.util.*;

/**
 * {@link NewsFeedHandler} A Handler class which contains Newsfeed REST API methods
 */
public class NewsFeedHandler {
  /**
   * getFeeds - A method which displays the feed items
   *
   * @param context
   */
  public static void getFeedsForAdmin(RoutingContext context) {

    HttpServerResponse httpServerResponse = context.response();
    HttpServerRequest request = context.request();

    String[] allowedParams = {"organization_id"};
    String[] requiredParams = {"organization_id"};
    String[] newsfeedParams = {"organization_id"};

    List<Validator> validators = new ArrayList<>();

    HashMap<String, FieldOptions> validationMapper = new HashMap<String, FieldOptions>()
    {{
      put("organization_id", FieldOptionsCommon.NUMBER);
    }};

    Validator newsfeedValidator = new ValidatorImpl(newsfeedParams, validationMapper);
    validators.add(newsfeedValidator);

    if(!ParamHelper.isParamsValid(httpServerResponse, request.params(), allowedParams,
        requiredParams, validators)) {
      return ;
    }

    try {
      RestClient restClient = RestClient.builder(
          new HttpHost("localhost", 9200, "http")).build();

      String id = request.getParam("organization_id");

      NewsFeedBO newsFeedBO = new NewsFeedBOImpl();
      ReturnObject<NewsFeedEnum,JsonObject> returnObject = newsFeedBO.
          getNewsFeedForAdmin(restClient, Integer.parseInt(id));
      if(returnObject.getStatusEnum() == NewsFeedEnum.VALUE_SUCCESS) {
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "", returnObject.getData(), httpServerResponse);
      }

      restClient.close();

    } catch (IOException ioException) {

      ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
          "Newsfeed not available", "Newsfeed not available", httpServerResponse);
    }
  }

  public static Map<String, Integer> sortByValue(Map<String, Integer> unsortedMap) {

    //  Convert Map to List of Map
    List<Map.Entry<String, Integer>> list =
        new LinkedList<Map.Entry<String, Integer>>(unsortedMap.entrySet());

    Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
      public int compare(Map.Entry<String, Integer> x,
                         Map.Entry<String, Integer> y) {
        return (y.getValue()).compareTo(x.getValue());
      }
    });

    //  Loop the sorted list and put it into a new insertion order Map LinkedHashMap
    Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
    for (Map.Entry<String, Integer> entry : list) {
      sortedMap.put(entry.getKey(), entry.getValue());
    }

    return sortedMap;
  }
}
