package com.auzmor.impl.handler;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auzmor.bo.EmployeeBO;
import com.auzmor.dao.LoginDAO;
import com.auzmor.impl.bo.EmployeeBOImpl;
import com.auzmor.impl.bo.LoginBOImpl;
import com.auzmor.impl.bo.RoleBOImpl;
import com.auzmor.impl.builder.ApiBuilder;
import com.auzmor.impl.builder.QueryStringBuilder;
import com.auzmor.impl.builder.ResponseBuilder;
import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.constant.file.endpoint.EndpointConstant;
import com.auzmor.impl.dao.LoginDAOImpl;
import com.auzmor.impl.enumarator.bo.EmployeeBOEnum;
import com.auzmor.impl.enumarator.file.FileTypeEnum;
import com.auzmor.impl.enumarator.kong.KongHttpStatusCode;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.exception.util.ApplicationUtilException;
import com.auzmor.impl.helper.*;
import com.auzmor.impl.model.Login;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.Role;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.model.employee.EmployeeTermination;
import com.auzmor.impl.util.*;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.FieldOptions;
import com.auzmor.util.validator.Validator;
import com.google.i18n.phonenumbers.NumberParseException;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.httpclient.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

public class EmployeeHandler extends ApiBuilder {

  private static final Logger logger = LoggerFactory.getLogger(EmployeeHandler.class);

  StringUtil util = new StringUtil();

  DatabaseUtil dbutil = new DatabaseUtil();

  public static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle, JsonObject jsonObject,
                                   EmployeeBOEnum employeeBOEnum)
      throws SQLException, DbHandleException {

    switch (employeeBOEnum) {
      case ALREADY_EXISTS:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_CONFLICT,
            params.get("email") + " is already a part of the organization",
            "Cannot invite as the invited person already exists in your organization",
            httpServerResponse);
        break;
      case FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_CONFLICT,
            "Invitation already sent for " + params.get("email"),
            "Sending failed due to ",
            httpServerResponse);
        break;
      case EMPLOYEE_ADD_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_CONFLICT,
            "Employee cannot cdded", "User cannot be added",
            httpServerResponse);
        break;
      case EMPLOYEE_ADDED:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_CREATED,
            "Employee added", jsonObject, httpServerResponse);
        break;

      case UPDATE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_CREATED,
            "Updated successfully", jsonObject, httpServerResponse);
        break;
      case UPDATE_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_CONFLICT,
            "Updated failed", "Employee updated failed", httpServerResponse);
        break;

      case USER_ALREADY_EXISTS:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_CONFLICT,
            "User is already a part of the organization",
            "can't add this employee, because user name already exists with mail",
            httpServerResponse);
        break;
      case DELETE_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_CREATED,
            "Deletion failed", "Deletion failed", httpServerResponse);
        break;
      case DELETE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "Deleted successfully", jsonObject, httpServerResponse);
        break;
      case TABLE_EMPTY:
        dbHandle.rollback();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "no data is false", "no data in table", httpServerResponse);
        break;
      case IMAGE_FORMAT_WRONG:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_CONFLICT,
            "Not a proper image", "Upload image with 300*300 pixel size", httpServerResponse);
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }

  public static void writeArrayResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                        DbHandle dbHandle,
                                        JsonArray jsonArray, EmployeeBOEnum employeeBOEnum)
      throws SQLException, DbHandleException {
    switch (employeeBOEnum) {
      case TABLE_VALUES:
        dbHandle.commit();
        ResponseHelper.writeArraySuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "no_data is false",
            jsonArray, httpServerResponse);
        break;
      case TABLE_EMPTY:
        dbHandle.rollback();
        ResponseHelper.writeArraySuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "no_data is true",
            jsonArray, httpServerResponse);
        break;
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }

  public void addEmployee(RoutingContext routingContext) {
    Employee employeeModel = new Employee();
    MultiMap requestParams = (routingContext.request()).params();
    JsonObject jsonResponseObject = new JsonObject();
    JsonObject errorObject = new JsonObject();
    errorObject.put("code", HttpStatus.SC_NOT_ACCEPTABLE);
    DbHandle dbHandle = DbHelper.getDBHandle(routingContext, false);
    if (null == dbHandle) {
      ResponseHelper.writeInternalServerError(routingContext.response());
      return;
    }
    EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
    java.util.Date currentDate = new java.util.Date();
    Mail mailObject = new Mail();
    JsonObject successObject = new JsonObject();
    SimpleDateFormat currentDateFormatter =
        new SimpleDateFormat("MM/dd/yyyy");

    try {
      for (String key : requestParams.names()) {
        if (!key.equals("primary_region_code") &&
            !key.equals("secondary_region_code") &&
            !key.equals("work_region_code")) {
          ModelUtil.setValues(key, requestParams.get(key), employeeModel);
        }
      }

      Employee employee = (Employee) routingContext.data().get("employee");

      if (employee.getAccess_level().equals(EmployeeBOEnum.EMPLOYEE_ACCESS.getDescription())) {
        ResponseHelper.writeErrorResponse(HttpStatus.SC_UNAUTHORIZED, "You cannot consume service",
            "You cannot consume service", routingContext.response());
        return;
      }

      //Access level setting
      String userAccess = "";
      if (employeeModel.getAccess_level() != null) {
        userAccess = employeeModel.getAccess_level();
      } else {
        userAccess = "employee";
      }
      int id;
      boolean isValidPrimaryPhone = StringUtil.isValidPhoneNumber(employeeModel.getPrimary_phone(),
          requestParams.get("primary_region_code"));

      if (!isValidPrimaryPhone) {
        errorObject.put("message", "Please provide proper phone number");
        errorObject.put("description", "Please provide proper phone number");
        jsonResponseObject.put("error", errorObject);
        routingContext.response()
            .setStatusCode(HttpStatus.SC_BAD_REQUEST) // 406
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .end(jsonResponseObject.toString());
        return;
      }

      if (!StringUtil.printValidString(employeeModel.getSecondary_phone()).equals("")) {
        boolean isValidSecondaryPhone = StringUtil.isValidPhoneNumber(employeeModel.getSecondary_phone(),
            requestParams.get("secondary_region_code"));

        if (!isValidSecondaryPhone) {
          errorObject.put("message", "Please provide proper phone number");
          errorObject.put("description", "Please provide proper phone number");
          jsonResponseObject.put("error", errorObject);
          dbHandle.checkAndCloseConnection();
          routingContext.response()
              .setStatusCode(HttpStatus.SC_BAD_REQUEST) // 406
              .putHeader("Content-Type", "application/json; charset=utf-8")
              .end(jsonResponseObject.toString());
          return;
        }
      }

      if (!StringUtil.printValidString(employeeModel.getWork_phone()).equals("")) {
        boolean isValidSecondaryPhone = StringUtil.isValidPhoneNumber(employeeModel.getWork_phone(),
            requestParams.get("work_region_code"));

        if (!isValidSecondaryPhone) {
          errorObject.put("message", "Please provide proper phone number");
          errorObject.put("description", "Please provide proper phone number");
          jsonResponseObject.put("error", errorObject);
          dbHandle.checkAndCloseConnection();
          routingContext.response()
              .setStatusCode(HttpStatus.SC_BAD_REQUEST) // 406
              .putHeader("Content-Type", "application/json; charset=utf-8")
              .end(jsonResponseObject.toString());
          return;
        }
      }


      //String username = this.getUserName(employeeModel);
      long organizationId = employeeModel.getOrganization_id();
      String organization_name = dbutil.OrganizationName((int) organizationId, "hris_url");
      if (!(employeeBO.isEmployeeExists(employeeModel.getWork_email(), organizationId))) {
        String pwd = util.randomString(8);

        //setting values to Login table
        Login login = new Login();
        login.setFirstName(employeeModel.getFirst_name());
        login.setLastName(employeeModel.getLast_name());
        login.setEmail(employeeModel.getWork_email());
        login.setUserType(userAccess);
        login.setPassword(pwd);
        login.setOrganizationId(employeeModel.getOrganization_id());
        login.setKeyValue(UUID.randomUUID().toString());
        login.setHashValue(UUID.randomUUID().toString());
        id = new LoginBOImpl(dbHandle).addNewUser(login);
        login.setId(id);

        //setting valus to employee table
        employeeModel.setUser_id(id);
        employeeModel.setAccess_level(userAccess);
        employeeModel.setOnboarding_progress(0);

        if (employeeModel.getOnboarding_packet() == null) {
          employeeModel.setOnboarding_status("Packet Not Sent");
          successObject.put("message", "Employee added successfully");

          String organizationURL = dbutil.OrganizationName((int) login.getOrganizationId(),
              "hris_url");
          String organizationName = dbutil.OrganizationName((int) login.getOrganizationId(),
              "organization_name");

          // String encryptedString = new LoginHandler().generateToken(login);
          // as per now we are giving as personal email id

          String encryptedString;
            encryptedString = generateToken(login);

          mailObject.registeredWithoutOnboarding(Vertx.vertx(), login.getFirstName(),
              encryptedString, requestParams.get("personal_email"), organizationName, organizationURL);

        } else {
          employeeModel.setOnboarding_status("Packet sent");
          employeeModel.setOnboarding_packet_date(currentDateFormatter.format(currentDate));
          successObject.put("message", "Employee added successfully and onboarding packet has been sent!");
          String organizationURL = dbutil.OrganizationName((int) login.getOrganizationId(),
              "hris_url");
          String organizationName = dbutil.OrganizationName((int) login.getOrganizationId(),
              "organization_name");

          // String encryptedString = new LoginHandler().generateToken(login);
          // as per now we are giving as personal email id

          String encryptedString;
          encryptedString = generateToken(login);

          mailObject.registeredWithOnboarding(Vertx.vertx(), login.getFirstName(),
              encryptedString, requestParams.get("personal_email"), organizationName, organizationURL);

        }
        employeeModel = this.addEmployeeDetails(employeeModel);

        successObject.put("id", employeeModel.getEmployee_id());
        jsonResponseObject.put("code", HttpStatus.SC_CREATED); //201
        jsonResponseObject.put("success", true);
        jsonResponseObject.put("data", successObject);
        String employeeLoginId = "" + id;

        HttpClient kongClient = KongServiceHelper.executeWithKongClient(routingContext.vertx());
        KongServiceHelper.addConsumer(kongClient, Long.parseLong(employeeLoginId), clientResponse -> {
          if (clientResponse.statusCode() == KongHttpStatusCode.ADD.getStatusCode()) {
            clientResponse.bodyHandler(buffer -> {
              String consumerId = new JsonObject(buffer).getString("id");
              Role role = null;
              boolean exception = false;
              try {
                new LoginBOImpl(dbHandle).updateConsumerId(consumerId, Long.parseLong(employeeLoginId));
                role = new RoleBOImpl(dbHandle).getByName("Employee", login.getOrganizationId());
              } catch (SQLException e) {
                DbHelper.safeRollback(dbHandle);
                ResponseHelper.writeInternalServerError(routingContext.response());
                exception = true;
              } catch (DbHandleException e) {
                DbHelper.safeRollback(dbHandle);
                ResponseHelper.writeInternalServerError(routingContext.response());
                exception = true;
              } finally {
                if (exception) {
                  kongClient.close();
                  dbHandle.checkAndCloseConnection();
                }
              }
              KongServiceHelper.addConsumerRoles(kongClient, consumerId, "" + role.getId(), httpClientResponse -> {
                if (httpClientResponse.statusCode() == KongHttpStatusCode.ADD.getStatusCode()) {
                  routingContext.response()
                      .setStatusCode(HttpStatus.SC_CREATED) // 201
                      .putHeader("Content-Type", "application/json; charset=utf-8; " +
                          "id=" + employeeLoginId)
                      .end(jsonResponseObject.toString());
                  kongClient.close();
                  DbHelper.safeCommit(dbHandle);
                  dbHandle.checkAndCloseConnection();
                } else {
                  ResponseHelper.writeInternalServerError(routingContext.response());
                  kongClient.close();
                  DbHelper.safeRollback(dbHandle);
                  dbHandle.checkAndCloseConnection();
                }
              });
            });
          } else {
            routingContext.response()
                .setStatusCode(HttpStatus.SC_CREATED) // 201
                .putHeader("Content-Type", "application/json; charset=utf-8; " +
                    "id=" + employeeLoginId)
                .end(jsonResponseObject.toString());
            dbHandle.checkAndCloseConnection();
          }
        });
      } else {
        successObject.put("message", "Employee Already exists with username!");
        successObject.put("id", employeeModel.getEmployee_id());
        jsonResponseObject.put("code", HttpStatus.SC_CONFLICT); //201
        jsonResponseObject.put("success", true);
        jsonResponseObject.put("data", successObject);
        routingContext.response()
            .setStatusCode(HttpStatus.SC_CONFLICT) // 406
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .end(jsonResponseObject.toString());
        dbHandle.checkAndCloseConnection();
      }
    } catch (SQLException addEmployeeException) {
      logger.error(addEmployeeException.getMessage());
      dbHandle.checkAndCloseConnection();
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (ApplicationUtilException e) {
      logger.error(e.getMessage());
      dbHandle.checkAndCloseConnection();
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (ClassNotFoundException e) {
      logger.error(e.getMessage());
      dbHandle.checkAndCloseConnection();
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (DbHandleException e) {
      e.printStackTrace();
      logger.error(e.getMessage());
      dbHandle.checkAndCloseConnection();
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (NumberParseException e) {
      logger.error(e.getMessage());
      dbHandle.checkAndCloseConnection();
      ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_BAD_REQUEST,
          e.getMessage(),
          e.getMessage(),
          routingContext.response());
    } catch (UnsupportedEncodingException e) {

      logger.error(e.getMessage());
      ResponseHelper.writeInternalServerError(routingContext.response());
      dbHandle.checkAndCloseConnection();
    }
  }
// sadhana created these for checking user name is already present in login table or not

  public void updateEmployee(RoutingContext routingContext) {

    JsonObject jsonResponseObject = new JsonObject();
    JsonObject errorObject = new JsonObject();
    String modifiedDate = "";
    errorObject.put("code", HttpStatus.SC_NOT_ACCEPTABLE);

    String id = routingContext.request().getParam("employeeId");
    boolean isExceptionOccured = false;
    Connection connection = null;

    try {

      connection = new MysqldbConf().getConnection();
      connection.setAutoCommit(false);

      final Integer idAsInteger = Integer.valueOf(id);
      MultiMap params = routingContext.request().params();
      params.remove("employeeId");
      params.add("employee_id", id);
      String formName = StringUtil.printValidString(params.get("form_name"));
      if (formName.length() > 0
          || StringUtil.printValidString((params.get("document_name"))).length() > 0) {
        String query = "select onboarding_status,onboarding_progress,onboarding_packet " +
            "from Employee where employee_id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, Integer.parseInt(id));
        ResultSet resultSet = preparedStatement.executeQuery();
        String packetId = "0";
        String onboardingProgress = "0", onboardingStatus = "";
        while (resultSet.next()) {
          onboardingStatus = (resultSet.getString("onboarding_status"));
          onboardingProgress = (resultSet.getString("onboarding_progress"));
          packetId = (resultSet.getString("onboarding_packet"));
        }

        resultSet.close();
        preparedStatement.close();
        String formCountQuery = "select count(form_id) as form_count from OnboardingForm " +
            "where packet_id = ? and inactive_status = ?";
        preparedStatement = connection.prepareStatement(formCountQuery);
        preparedStatement.setInt(1, Integer.parseInt(packetId));
        preparedStatement.setInt(2, 0);

        resultSet = preparedStatement.executeQuery();
        int formCount = 0;
        while (resultSet.next()) {
          formCount = Integer.parseInt(resultSet.getString("form_count"));
        }
        resultSet.close();
        preparedStatement.close();

        String documentCountQuery = "select count(document_id) as doc_count " +
            "from OnboardingDocuments where packet_id = ? and inactive_status = ?";
        preparedStatement = connection.prepareStatement(documentCountQuery);
        preparedStatement.setInt(1, Integer.parseInt(packetId));
        preparedStatement.setInt(2, 0);
        int documentCount = 0;
        resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
          documentCount = Integer.parseInt(resultSet.getString("doc_count"));
        }
        resultSet.close();
        preparedStatement.close();
        int progress = Integer.parseInt(onboardingProgress) + (100) / (formCount + documentCount + 1);
        progress = (progress >= 99) ? 100 : progress;

        if (formName.length() > 0) {
          if ("Employment Information".equalsIgnoreCase(params.get("form_name"))) {
            params.set("onboarding_status", "Viewed");
          } else {
            params.set("onboarding_status", "In Progress");
          }
        }

        params.set("onboarding_progress", String.valueOf(progress));
        if (progress == 100) {
          params.set("onboarding_status", "Completed");
        }
        params.remove("form_name");
        params.remove("document_name");
      }

      StringBuilder statement = new StringUtil().getUpdateStatement(params);

      java.util.Date presentDate = new java.util.Date();
      SimpleDateFormat dateFormatter =
          new SimpleDateFormat("MM/dd/yyyy");
      modifiedDate = (dateFormatter.format(presentDate));

      String queryString = "update Employee set " + statement + " ," +
          "modified_date ='" + modifiedDate + "'" +
          " WHERE employee_id = " + idAsInteger;

      PreparedStatement ptmt = connection.prepareStatement(queryString);
      int updatedRows = ptmt.executeUpdate();
      ptmt.close();
      //connection.close();
      if (updatedRows == 0) {
        throw new IllegalArgumentException("Bad Request : Invalid Employee ID");
      }

      JsonObject dataObject = new JsonObject();

      String constraint = " employee_id = " + id;

      String name = dbutil.getTablename("Employee", "first_name", constraint);

      dataObject.put("message", "Employee '" + name + "' Updated Sucessfully");
      dataObject.put("id", id);

      connection.commit();
      connection.close();

      jsonResponseObject.put("code", HttpStatus.SC_ACCEPTED);
      jsonResponseObject.put("success", true);
      jsonResponseObject.put("data", dataObject);

      routingContext.response()
          .setStatusCode(HttpStatus.SC_ACCEPTED)
          .putHeader("Content-Type", "application/json; charset=utf-8;")
          .end(jsonResponseObject.toString());
    } catch (SQLException updateEmployeeException) {
      logger.error("Sql Exception while updating employee data");
      ResponseBuilder.writeInternalServerError(routingContext.response());

    } catch (ClassNotFoundException e) {
      logger.error("ClassNotFoundException while updating employee data");

      ResponseBuilder.writeInternalServerError(routingContext.response());
    } finally {
      if (null != connection) {
        try {
          connection.close();
        } catch (SQLException e) {
          logger.error(e.getMessage());
        }
      }
    }
  }

  public Employee addEmployeeDetails(Employee employee) throws SQLException, ClassNotFoundException {

    Map<String, Object> insertionObjects = new LinkedHashMap<>();

    java.util.Date currentDate = new java.util.Date();
    SimpleDateFormat currentDateFormatter =
        new SimpleDateFormat("MM/dd/yyyy");

    employee.setCreated_date(currentDateFormatter.format(currentDate));


    StringBuilder query = new StringBuilder();
    query.append("insert into Employee (first_name,last_name,personal_email,work_email,primary_phone,hire_date, job_title,manager,location,pay_rate,pay_type,employment_type,employee_number,currency_type,");
    query.append("primary_region_code,preferred_name,middle_name,address_line1,address_line2,city,state,zip,country,marital_status,gender,date_of_birth,ssn,department,division,");
    query.append("work_phone,ethnicity,created_date,disability_status,veteran_status,veteran_classification,military_discharge_date,contact1_first_name,contact1_last_name,contact1_phone_number,");
    query.append("contact1_relationship,contact2_first_name,contact2_last_name,contact2_phone_number,contact2_relationship,deposit_first_name,deposit_last_name,deposit_account_number,");
    query.append("deposit_account_type,deposit_bank_name,deposit_routing_number,organization_id,candidate_id, access_level,onboarding_status,onboarding_packet,onboarding_packet_date,");
    query.append("status,user_id,onboarding_timestamp ) ");
    query.append(" values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?");
    query.append(",?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

    Connection connectionObject = new MysqldbConf().getConnection();

    PreparedStatement preparedStatement = connectionObject.prepareStatement(query.toString(), Statement.RETURN_GENERATED_KEYS);
    int index = 0;


    preparedStatement.setString(++index, employee.getFirst_name());
    preparedStatement.setString(++index, employee.getLast_name());
    preparedStatement.setString(++index, employee.getPersonal_email());
    preparedStatement.setString(++index, employee.getWork_email());
    preparedStatement.setString(++index, employee.getPrimary_phone());
    //StringUtil.checkRequiredFields(insertionObjects,"employee_number",employee.getEmployee_number());

    preparedStatement.setString(++index, StringUtil.printValidString(employee.getHire_date()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getJob_title()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getManager()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getLocation()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getPay_rate()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getPay_type()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getEmployment_status()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getEmployee_number()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getCurrency_type()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getPrimary_region_code()));

    preparedStatement.setString(++index, StringUtil.printValidString(employee.getPreferred_name()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getMiddle_name()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getAddress_line1()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getAddress_line2()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getCity()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getState()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getZip()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getCountry()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getMarital_status()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getGender()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getDate_of_birth()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getSsn()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getDepartment()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getDivision()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getWork_phone()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getEthnicity()));
    preparedStatement.setString(++index, employee.getCreated_date());
    //insertionObjects.put("onboarding_packet_date", employee.getOnboarding_packet_date());
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getDisability_status()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getVeteran_status()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getVeteran_classification()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getMilitary_discharge_date()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getContact1_first_name()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getContact1_last_name()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getContact1_phone_number()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getContact1_relationship()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getContact2_first_name()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getContact2_last_name()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getContact2_phone_number()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getContact2_relationship()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getDeposit_first_name()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getDeposit_last_name()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getDeposit_account_number()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getDeposit_account_type()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getDeposit_bank_name()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getDeposit_routing_number()));
    preparedStatement.setInt(++index, Integer.valueOf(employee.getOrganization_id()));
    preparedStatement.setInt(++index, Integer.valueOf(employee.getCandidate_id()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getAccess_level()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getOnboarding_status()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getOnboarding_packet()));
    preparedStatement.setString(++index, StringUtil.printValidString(employee.getOnboarding_packet_date()));
    preparedStatement.setString(++index, "Active");
    preparedStatement.setInt(++index, employee.getUser_id());
    SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
    preparedStatement.setString(++index, dateFormat.format(new Date()));


//    if (!StringUtil.printValidString(employee.getOnboarding_packet()).equals("")) {
//      insertionObjects.put("onboarding_packet", StringUtil.printValidString(employee.getOnboarding_packet()));
//    } else {
//      String method = new DatabaseUtil().getTablename("OnboardingPacket", "packet_id",
//          "organization_id =" + employee.getOrganization_id() + " and is_default = 1 ");
//
//      if (!StringUtil.printValidString(method).equals("")) {
//        insertionObjects.put("onboarding_packet", method);
//      }
//
//    }


    // insertionObjects.put("onboarding_status", "Packet Sent");

    /*String columnValueString = "";
    String entryValueString = "";
    String appender = "";
    for (Map.Entry<String, Object> columnEntries : insertionObjects.entrySet()) {
      columnValueString += columnEntries.getKey() + ",";
      Object insertObject = columnEntries.getValue();

      if (insertObject instanceof Boolean) {
        appender = insertObject.toString();
      } else {
        appender = "'" + insertObject + "'";
      }

      entryValueString += appender + ",";
    }

    entryValueString = entryValueString.substring(0, entryValueString.length() - 1);
    columnValueString = columnValueString.substring(0, columnValueString.length() - 1);

    String sqlQueryForEmployee = "insert into Employee(" + columnValueString + ") values(" + entryValueString + ");";

    Statement preparedStatement = connectionObject.createStatement();*/

    int employeeId = preparedStatement.executeUpdate();
    ResultSet rs = preparedStatement.getGeneratedKeys();

    if (rs.next()) {
      employeeId = rs.getInt(1);
    }

    rs.close();
    preparedStatement.close();
    employee.setEmployee_id(employeeId);
    connectionObject.close();
    return employee;
  }

  public String getUserName(Employee employee)
      throws ApplicationUtilException, IllegalAccessException {
    String username = "";
    String organizationName = "";
    Connection connectionObject = null;
    try {
      connectionObject = new MysqldbConf().getConnection();
      PreparedStatement preparedStatement = null;
      String queryString = "";
      int employeeId = 0;
      queryString = " SELECT a.employee_id,b.first_name FROM Employee a,Login b " +
          " where a.organization_id = b.organization_id and " +
          " a.first_name = '" + employee.getFirst_name() + "'" +
          " and a.first_name = b.first_name  ";
      preparedStatement = connectionObject.prepareStatement(queryString);
      ResultSet resultSetObject = preparedStatement.executeQuery();
      while (resultSetObject.next()) {
        employeeId = resultSetObject.getInt("employee_id");
      }
      resultSetObject.close();
      preparedStatement.close();
          /* queryString = "select domain_name from Organization
             where organization_id = " + employeeModel.getOrganization_id();
            preparedStatement = connectionObject.prepareStatement(queryString);
            ResultSet resultSetObject1 = preparedStatement.executeQuery();
            while (resultSetObject1.next()) {
                organizationname = resultSetObject1.getString("domain_name");
            }*/
      organizationName = dbutil.getTablename("Organization", "domain_name",
          "organization_id=" + employee.getOrganization_id());

      if (StringUtil.printValidString(organizationName).equals("")) {
        throw new IllegalAccessException("Organization Name is Invalid");
      }
      if (employeeId != employee.getEmployee_id()) {
        username = employee.getFirst_name() + employee.getLast_name() + "@" +
            organizationName.trim() + ".com";
      } else {
        username = employee.getFirst_name() + "@" + organizationName.trim() + ".com";
      }
    } catch (SQLException e) {
      logger.error("Sql Exception while updating employee data");

      throw new ApplicationUtilException("Sql Exception while updating employee data");
    } catch (ClassNotFoundException e) {
      logger.error("ClassNotFoundException while updating employee data");

      throw new ApplicationUtilException("ClassNotFoundException while updating employee data");
    } finally {
      if (null != connectionObject) {
        try {
          connectionObject.close();
        } catch (SQLException e) {
          logger.error(e.getMessage());
        }
      }
    }
    return username;
  }

  /**
   * getEmployeeDetails : Get Employee Details based on constraints
   *
   * @param fieldString    Fields for which employee details are required
   * @param limit          Number of Items to show
   * @param offset         The index of the search item to start with
   * @param routingContext the routing context
   * @lastModifiedDate Nov 11, 2017 by Pradeep Balasubramanian
   */
  public void getEmployeeDetails(String fieldString, String limit, String offset,
                                 int organizationId, RoutingContext routingContext) {
    try {
      PreparedStatement preparedStatement;
      JsonObject jsonSuccessObject = new JsonObject();
      jsonSuccessObject.put("code", HttpStatus.SC_OK); // 200
      jsonSuccessObject.put("success", true);

      MultiMap params = routingContext.request().params();
      /*String constraintString = "";
      String orderConstraintString = "";
      String limitString = "";*/

      /*String sorterString = params.get("sorter");
      String searchString = params.get("search_string");*/

           /* int organizationId = Integer.valueOf(params.get("organization_id") == null ?
                    "0" : params.get("organization_id"));*/

      /*if (organizationId != 0) {
        constraintString += " where organization_id = " + organizationId;
      }

      if (sorterString != null) {
        String sorterConstraintString = " LEFT(first_name,1) in ('" + sorterString.toUpperCase() + "'," +
            "'" + sorterString.toLowerCase() + "')";
        orderConstraintString += " order by first_name";
        if (constraintString.length() > 0) {
          constraintString += " and " + sorterConstraintString;

        } else {
          constraintString = " where " + sorterConstraintString;
        }
      }

      if (searchString != null) {
        String searchConstraintString = " concat(first_name,' ',last_name) like '%" + searchString + "%' ";
        if (constraintString.length() > 0) {
          constraintString += " and " + searchConstraintString;

        } else {
          constraintString = " where " + searchConstraintString;
        }
      }

      if (constraintString.length() > 0) {
        constraintString += " and active_status = 1 ";
      } else {
        constraintString += " where active_status = 1 ";
      }

      if ("".equals(limit) || "".equals(offset)) {
        if (limit.length() > 0) {
          limitString = " LIMIT " + limit;
        }
        if (offset.length() > 0) {
          limitString = " OFFSET " + offset;
        }

      } else {
        limitString = " LIMIT " + limit + " OFFSET " + offset;
      }*/

      String[] fields = fieldString.split(",");

      List<String> fieldList = new LinkedList<>();
      for (String field : fields) {
        fieldList.add(field);
      }

      fieldString = "employee_id";

      if (fieldList.contains("first_name")) {
        fieldString  = fieldString + ", first_name";
      }

      if (fieldList.contains("last_name")) {
        fieldString  = fieldString + ", last_name";
      }

      if (fieldList.contains("job_title")) {
        fieldString  = fieldString + ", job_title";
      }

      if (fieldList.contains("profile_image")) {
        fieldString  = fieldString + ", profile_image";
      }

      if (fieldList.contains("currency_type")) {
        fieldString  = fieldString + ", currency_type";
      }

      if (fieldList.contains("work_email")) {
        fieldString  = fieldString + ", work_email";
      }

      if (fieldList.contains("primary_phone")) {
        fieldString  = fieldString + ", primary_phone";
      }

      if (fieldList.contains("primary_region_code")) {
        fieldString  = fieldString + ", primary_region_code";
      }

      if (fieldList.contains("department")) {
        fieldString  = fieldString + ", department";
      }

      if (fieldList.contains("country")) {
        fieldString  = fieldString + ", country";
      }

      if (fieldList.contains("state")) {
        fieldString  = fieldString + ", state";
      }

      if (fieldList.contains("city")) {
        fieldString  = fieldString + ", city";
      }



      Connection connectionObject = new MysqldbConf().getConnection();

      /*String queryString = "SELECT " + fieldString + " FROM Employee "
          + constraintString + " " + orderConstraintString + " " + limitString;*/

      StringBuilder query = new StringBuilder();

      query.append("select "+fieldString+" ");
      query.append(" FROM Employee where organization_id = ? and status != 'awaiting approval' and active_status = 1 ");
      //query.append(" LIMIT ? OFFSET ?");

      preparedStatement = connectionObject.prepareStatement(query.toString());
      preparedStatement.setLong(1, (long) organizationId);
      /*preparedStatement.setLong(2, Long.valueOf(limit));
      preparedStatement.setLong(3, Long.valueOf(offset));*/

      ResultSet resultSetObject = preparedStatement.executeQuery();
      JsonArray resultSetArray = new JsonArray();
      String currencyValue = "";
      while (resultSetObject.next()) {
        JsonObject dataObject = new JsonObject();
        for (String field : fieldList) {
          if (field.equals("currency_type")) {
            currencyValue = resultSetObject.getString(field);
            if (StringUtil.printValidString(currencyValue).equals("")) {
              currencyValue = ((Organization) routingContext.data().get("organization")).getDefault_currency();
            }
            dataObject.put("currency_type", currencyValue);
          } else {
            dataObject.put(field, resultSetObject.getString(field));
          }
        }
        resultSetArray.add(dataObject);
      }
      resultSetObject.close();
      preparedStatement.close();
      jsonSuccessObject.put("data", resultSetArray);

      routingContext.response()
          .setStatusCode(HttpStatus.SC_OK)  //200
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonSuccessObject.toString());
      connectionObject.close();

    } catch (SQLException e) {

      logger.error("Sql Exception while getting employee details");

      JsonObject jsonResponseObject = new JsonObject();
      JsonObject jsonFailureObject = new JsonObject();
      jsonFailureObject.put("code", HttpStatus.SC_BAD_REQUEST); //400
      jsonFailureObject.put("message", "Bad Request");
      jsonFailureObject.put("description", e.getMessage());
      jsonResponseObject.put("error", jsonFailureObject);

      routingContext.response()
          .setStatusCode(HttpStatus.SC_BAD_REQUEST) // 400
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
      //	e.printStackTrace();

    } catch (ClassNotFoundException e) {
      logger.error("ClassNotFoundException while getting employee details");

      ResponseBuilder.writeInternalServerError(routingContext.response());
    }
  }

  public int deleteApplicantDetails(int id) throws SQLException, ClassNotFoundException {

    Connection connectionObject = new MysqldbConf().getConnection();

    String sqlQueryForApplicant = "update Employee set active_status = 0 where employee_id = " + id;

    Statement preparedStatement = connectionObject.createStatement();
    int count = preparedStatement.executeUpdate(sqlQueryForApplicant);

    preparedStatement.close();
    connectionObject.close();

    return count;
  }

  /**
   * getEmployee - Display Employee Information
   *
   * @param routingContext
   * @lastModifiedDate Oct 30, 2017
   */

  public void getEmployee(RoutingContext routingContext) {

    JsonObject jsonSuccessObject = new JsonObject();
    PreparedStatement preparedStatement = null;
    ResultSet resultSetObject = null;
    Connection connectionObject = null;
    try {

      HttpServerRequest request = routingContext.request();

      String id = request.getParam("id");

      Field[] fields = Employee.class.getDeclaredFields();

      Employee employeeModel = (Employee) routingContext.data().get("employee");

      if (employeeModel.getAccess_level().equals(EmployeeBOEnum.EMPLOYEE_ACCESS.getDescription())) {

        if (employeeModel.getEmployee_id() != Integer.valueOf(id)) {
          ResponseHelper.writeErrorResponse(HttpStatus.SC_UNAUTHORIZED, "You cannot consume service",
              "You cannot consume service", routingContext.response());
          return;
        }

      }

      StringBuilder fieldString = new StringBuilder();
      for (int i = 0; i < fields.length; i++) {
        fields[i].setAccessible(true);
        String fieldName = fields[i].getName();
        fieldString.append(fieldName);
        if (i < fields.length - 1) {
          fieldString.append(",");
        }
      }
      connectionObject = new MysqldbConf().getConnection();
      connectionObject.setAutoCommit(false);

      StringBuilder query = new StringBuilder();
      query.append("select employee_id,first_name,middle_name,preferred_name,last_name,address_line1,address_line2,city,state,zip,country,");
      query.append("marital_status,gender,date_of_birth,ssn,ethnicity,hire_date,job_title,manager,department,division,location,pay_rate,pay_type,");
      query.append("employment_status,pay_group,primary_phone,secondary_phone,work_phone,personal_email,work_email,created_date,modified_date,organization_id,candidate_id,");
      query.append("disability_status,veteran_status,veteran_classification,military_discharge_date,contact1_first_name,contact2_first_name,contact1_last_name,contact2_last_name,");
      query.append("contact1_phone_number,contact2_phone_number,contact1_relationship,contact2_relationship,deposit_first_name,deposit_last_name,deposit_bank_name,deposit_account_type,deposit_account_number,");
      query.append("deposit_routing_number,profile_image,currency_type,access_level,user_id,employee_number,status,onboarding_progress,onboarding_packet,onboarding_status,onboarding_timestamp,");
      query.append("onboarding_packet_date,primary_region_code,secondary_region_code,work_region_code,timezone_format ");
      query.append(" FROM Employee where employee_id =  ?");


      preparedStatement = connectionObject.prepareStatement(query.toString());
      preparedStatement.setLong(1, Long.valueOf(id));


      resultSetObject = preparedStatement.executeQuery();
      JsonObject jsonDataObject = new JsonObject();
      String columnName = "";
      String columnType = "";
      while (resultSetObject.next()) {
        ResultSetMetaData resultSetMetaData = resultSetObject.getMetaData();
        for (int i = 1; i <= resultSetObject.getMetaData().getColumnCount(); i++) {
          columnName = resultSetMetaData.getColumnName(i);
          columnType = resultSetMetaData.getColumnTypeName(i);

          if ("BIT".equalsIgnoreCase(columnType)) {
            jsonDataObject.put(columnName, resultSetObject.getBoolean(columnName));
          } else if ("INT".equalsIgnoreCase(columnType)) {
            jsonDataObject.put(columnName, resultSetObject.getInt(columnName));
          } else if ("VARCHAR".equalsIgnoreCase(columnType) || "BLOB".equalsIgnoreCase(columnType)) {
            String columnValue = resultSetObject.getString(columnName);

            if ("onboarding_timestamp".equalsIgnoreCase(columnName)) {
              jsonDataObject.put(columnName, StringUtil.convertDBToJsonString(columnValue));
            } else {
              jsonDataObject.put(columnName, columnValue);
            }
          } else if ("DATE".equalsIgnoreCase(columnType)) {
            jsonDataObject.put(columnName, resultSetObject.getString(columnName));
          }
          if ("state".equalsIgnoreCase(columnName)) {
            String state = resultSetObject.getString(columnName);
            if (state != null) {
              jsonDataObject.put(columnName, CountryStateHandler.getStateName(state));
            } else {
              jsonDataObject.put(columnName, " ");
            }
          }
        }
      }
      jsonDataObject.put("profile_rate", 60);

      resultSetObject.close();
      preparedStatement.close();
      //connectionObject.commit();
      connectionObject.close();

      if (jsonDataObject.size() == 0) {
        jsonSuccessObject.put("no_data", true);
      } else {
        jsonSuccessObject.put("no_data", false);
      }

      jsonSuccessObject.put("code", HttpStatus.SC_OK);
      jsonSuccessObject.put("success", true);
      jsonSuccessObject.put("data", jsonDataObject);
      routingContext.response()
          .setStatusCode(HttpStatus.SC_OK) // 200
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonSuccessObject.toString());
    } catch (SQLException employeeDetailsException) {

      try {
        connectionObject.rollback();
      } catch (SQLException e) {
        e.printStackTrace();
      }

      logger.error("Sql Exception while getting employee details");

      JsonObject jsonResponseObject = new JsonObject();
      JsonObject jsonErrorObject = new JsonObject();
      jsonErrorObject.put("code", HttpStatus.SC_BAD_REQUEST); // 400
      jsonErrorObject.put("message", "Bad Request");
      jsonErrorObject.put("description", employeeDetailsException.getMessage());
      jsonResponseObject.put("error", jsonErrorObject);

      routingContext.response()
          .setStatusCode(jsonErrorObject.getInteger("code"))
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
    } catch (ClassNotFoundException e) {
      logger.error("ClassNotFoundException while getting employee details");

      ResponseBuilder.writeInternalServerError(routingContext.response());
    } finally {
      DbUtils.closeQuietly(connectionObject);
    }
  }

  public void deleteEmployee(RoutingContext routingContext) {

    JsonObject jsonResponseObject = new JsonObject();
    JsonObject errorObject = new JsonObject();
    errorObject.put("code", HttpStatus.SC_BAD_REQUEST);
    String name = "";
    String id = routingContext.request().getParam("id");

    try {

      if (id == null) {
        throw new IllegalArgumentException("Bad Request. Invalid ID Entered");
      }
      Employee employeeModel = new Employee();
      EmployeeHandler handler = new EmployeeHandler();
      handler.deleteApplicantDetails(Integer.valueOf(id));

      name = dbutil.getTablename("Employee", "first_name", "employee_id = " + id);

      ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
          "Employee " + name + " has been deleted successfully!", new JsonObject(), routingContext.response());

    } catch (SQLException sqlException) {

      logger.error("Sql Exception while deleting employee record");

      errorObject.put("message", "Fields Not Found");
      errorObject.put("description", sqlException.getMessage());

      jsonResponseObject.put("error", errorObject);

      routingContext.response()
          .setStatusCode(HttpStatus.SC_BAD_REQUEST) // 400
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());

    } catch (ClassNotFoundException e) {
      logger.error("Class Not Found Exception while deleting employee record");

      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (IllegalArgumentException illegalArgumentException) {

      logger.error("Illegal Argument Exception in Delete Employee");

      errorObject.put("message", "Invalid params entered");
      errorObject.put("description", illegalArgumentException.getMessage());

      jsonResponseObject.put("error", errorObject);

    }
  }

  /**
   * getAllEmployees Get list of employees based on a number of constraints
   *
   * @param routingContext
   * @lastModifiedDate Nov 10, 2017 by Pradeep Balasubramanian
   */
  public void getAllEmployees(RoutingContext routingContext) {

    MultiMap params = (routingContext.request()).params();

    String fieldString = params.get("fields");
    String limit = params.get("limit") == null ? "" : params.get("limit");
    String offset = params.get("offset") == null ? "" : params.get("offset");

    int organization = 0;

    if (!(null == params.get("organization_id"))) {
      organization = Integer.parseInt(params.get("organization_id"));
    }

    getEmployeeDetails(fieldString, limit, offset, organization, routingContext);

  }

  /**
   * Gets list of employees under a particular supervisor
   *
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 09-11-2017
   */
  //deprecated
  /*public void getTeam(RoutingContext routingContext) {
    String employee_email = routingContext.request().getParam("employeeId");
    String manager = "";
    String firstName = "";
    String lastName = "";
    String jobTitle = "";
    String workEmail = "";
    int organizationId = 0;
    String employeeId = "";
    JsonObject jsonSuccessObject = new JsonObject();
    JsonObject jsonResponseObject = new JsonObject();
    JsonObject jsonFailureObject = new JsonObject();
    DBCRUDUtil dbcrudUtil = new DBCRUDUtil();
    try {
      String queryString = "select employee_id,manager,first_name,last_name,job_title,work_email,organization_id " +
          "from Employee where work_email = '" + employee_email + "' and active_status = 1";
      Connection connection = new MysqldbConf().getConnection();
      PreparedStatement preparedStatement = connection.prepareStatement(queryString);
      ResultSet resultSet = preparedStatement.executeQuery();
      while (resultSet.next()) {
        manager = resultSet.getString("manager");
        employeeId = String.valueOf(resultSet.getInt("employee_id"));
        firstName = resultSet.getString("first_name");
        lastName = resultSet.getString("last_name");
        jobTitle = resultSet.getString("job_title");
        workEmail = resultSet.getString("work_email");
        organizationId = resultSet.getInt("organization_id");
      }

      String requiredFields = "employee_id,first_name,last_name,job_title,work_email,primary_phone";

      String[] fields = requiredFields.split(",");

      List<String> fieldList = new LinkedList<>();
      for (String field : fields) {
        fieldList.add(field);
      }

      JsonArray resultArrayManager = new JsonArray();
      JsonArray resultArrayName = new JsonArray();

      String[] splitter = manager.split(" ");
      String queryTeamManagerList = "select employee_id,first_name,last_name,job_title,work_email,primary_phone " +
          " from Employee where manager = '" + manager + "' and organization_id = " + organizationId + " " +
          " and active_status = 1 " +
          " union select employee_id,first_name,last_name,job_title,work_email,primary_phone " +
          " from Employee where manager = '" + firstName + " " + lastName + "' and " +
          " organization_id = " + organizationId + " and active_status = 1";


      String queryTeamNameList = "select employee_id,first_name,last_name,job_title,work_email,primary_phone " +
          "from Employee where first_name = '" + splitter[0] + "' and organization_id = " + organizationId + " " +
          "and last_name =  '" + splitter[1] + "' and active_status = 1";


      resultArrayManager = dbcrudUtil.selectFieldsWithLimit(queryTeamManagerList, fieldList);
      resultArrayName = dbcrudUtil.selectFieldsWithLimit(queryTeamNameList, fieldList);

      resultArrayManager.addAll(resultArrayName);

      jsonSuccessObject.put("code", HttpStatus.SC_OK);
      jsonSuccessObject.put("success", true);
      jsonSuccessObject.put("data", resultArrayManager);


      routingContext.response()
          .setStatusCode(HttpStatus.SC_OK)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonSuccessObject.toString());

    } catch (SQLException exception) {

      logger.error("Sql Exception while adding teamMember list in Employee");

      jsonFailureObject.put("code", HttpStatus.SC_BAD_REQUEST);
      jsonFailureObject.put("message", "Bad Request");
      jsonFailureObject.put("description", exception.getMessage());

      jsonResponseObject.put("error", jsonFailureObject);

      routingContext.response()
          .setStatusCode(HttpStatus.SC_BAD_REQUEST)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
    } catch (ClassNotFoundException e) {
      logger.error("Sql Exception while adding teamMember list in Employee");

      ResponseBuilder.writeInternalServerError(routingContext.response());
    }
  }*/

  /**
   * Gets list of employees for organization chart
   *
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13-11-2017
   */
  //deprecated
  /*public void getOrganizationChart(RoutingContext routingContext) {
    final String organizationId = routingContext.request().getParam("id");
    String managerQuery = "";
    String adminQuery = "";
    String employeeQuery = "";
    JsonObject jsonSuccessObject = new JsonObject();
    JsonObject jsonResponseObject = new JsonObject();
    JsonObject jsonFailureObject = new JsonObject();
    JsonArray resultArray = new JsonArray();
    DBCRUDUtil dbcrudUtil = new DBCRUDUtil();
    try {
      managerQuery = "create view organization_chart as select concat(SUPERVISOR.first_name,' '," +
          "SUPERVISOR.last_name,'|',SUPERVISOR.job_title) as SUPERVISOR, " +
          "GROUP_CONCAT(concat(SUPERVISEE.first_name,' ',SUPERVISEE.last_name,'|',SUPERVISEE.job_title) " +
          "ORDER BY SUPERVISEE.first_name,SUPERVISEE.last_name,SUPERVISEE.job_title) as SUPERVISEE " +
          "FROM Employee AS SUPERVISOR INNER JOIN Employee SUPERVISEE ON " +
          "SUPERVISEE.manager = concat(SUPERVISOR.first_name,' ',SUPERVISOR.last_name) " +
          "and SUPERVISEE.organization_id =" + organizationId + " GROUP BY SUPERVISOR";

      List<String> listOfOwners = new LinkedList<>();
      adminQuery = "select CONCAT(first_name,' ',last_name,'|',job_title) as OWNERS " +
          "from Employee where (manager = '' or manager = 'null') and organization_id=" + organizationId;

      String createdView = dbcrudUtil.createView(managerQuery);

      listOfOwners = dbcrudUtil.selectFieldsForList(adminQuery, "OWNERS");
      label1:
      for (String owner : listOfOwners) {
        JsonObject resultObject = new JsonObject();
        JsonObject attributeObject = null;
        JsonArray superviseeArray = new JsonArray();
        JsonArray subSupervisees = new JsonArray();
        JsonArray secArray = new JsonArray();
        String[] stringSplitter = owner.split("\\|");
        String ownerName = stringSplitter[0];
        String ownerTitle = stringSplitter[1];

        resultObject.put("name", ownerName);
        attributeObject = new JsonObject();
        attributeObject.put("Designation", ownerTitle);
        resultObject.put("attributes", attributeObject);
        String listOfSuperVisee = dbcrudUtil.
            returnValueForQuery("select SUPERVISEE from organization_chart " +
                "where SUPERVISOR = '" + owner + "'");
        if (listOfSuperVisee.equals("") || listOfSuperVisee.equals(null)) {
          resultArray.add(resultObject);
          continue label1;
        } else {
          String[] superViseeSplitter = listOfSuperVisee.split(",");
          label2:
          for (String split : superViseeSplitter) {
            String[] tierSplitter = split.split("\\|");
            JsonObject superVisee = new JsonObject();
            attributeObject = new JsonObject();
            attributeObject.put("Designation", tierSplitter[1]);
            superVisee.put("name", tierSplitter[0]);
            superVisee.put("attributes", attributeObject);
            String getTierSupervisee = dbcrudUtil.
                returnValueForQuery("select SUPERVISEE from organization_chart " +
                    "where SUPERVISOR = '" + split + "'");
            if (getTierSupervisee.equals("") || getTierSupervisee.equals(null)) {
              superviseeArray.add(superVisee);
            } else {
              String[] tierSuperVisee = getTierSupervisee.split(",");
              for (String tierSplit : tierSuperVisee) {
                String[] tierSubSuperVisee = tierSplit.split("\\|");
                JsonObject subSupervisee = new JsonObject();
                attributeObject = new JsonObject();
                attributeObject.put("Designation", tierSubSuperVisee[1]);
                subSupervisee.put("name", tierSubSuperVisee[0]);
                subSupervisee.put("attributes", attributeObject);
                String secTierSupervisee = dbcrudUtil.
                    returnValueForQuery("select SUPERVISEE from organization_chart " +
                        "where SUPERVISOR = '" + tierSplit + "'");
                if (secTierSupervisee.equals("") || secTierSupervisee.equals(null)) {
                  subSupervisees.add(subSupervisee);
                } else {
                  String[] secSplitter = secTierSupervisee.split(",");
                  for (String secSplit : secSplitter) {
                    String[] secTier = secSplit.split("\\|");
                    JsonObject secObject = new JsonObject();
                    attributeObject = new JsonObject();
                    attributeObject.put("Designation", secTier[1]);
                    secObject.put("name", secTier[0]);
                    secObject.put("attributes", attributeObject);
                    secArray.add(secObject);
                  }
                  subSupervisee.put("children", secArray);
                  subSupervisees.add(subSupervisee);
                }
              }
              superVisee.put("children", subSupervisees);
              superviseeArray.add(superVisee);
            }
          }
          resultObject.put("children", superviseeArray);
        }
        resultArray.add(resultObject);
      }


      String dropQuery = "drop view organization_chart";
      dropQuery = dbcrudUtil.createView(dropQuery);

      jsonSuccessObject.put("code", HttpStatus.SC_OK);
      jsonSuccessObject.put("success", true);
      jsonSuccessObject.put("data", resultArray);

      routingContext.response()
          .setStatusCode(HttpStatus.SC_OK)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonSuccessObject.toString());

    } catch (IllegalArgumentException exception) {
      logger.error("IllegalArgumentException while getting Organization chart");

      jsonFailureObject.put("code", HttpStatus.SC_BAD_REQUEST);
      jsonFailureObject.put("message", "Bad Request");
      jsonFailureObject.put("description", exception.getMessage());

      jsonResponseObject.put("error", jsonFailureObject);

      routingContext.response()
          .setStatusCode(HttpStatus.SC_BAD_REQUEST)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
    }

  }*/

  /**
   * addEmployeeTermination add employee termination details
   *
   * @param routingContext the routing context
   * @author Pradeep Balasubramanian
   * @lastModifiedDate Nov 10, 2017
   */

  public void addEmployeeTermination(RoutingContext routingContext) {

    EmployeeTermination employeeTerminationModel = new EmployeeTermination();
    Employee employeeModel = new Employee();
    JsonObject jsonResponseObject = new JsonObject();
    JsonObject errorObject = new JsonObject();
    errorObject.put("code", HttpStatus.SC_NOT_ACCEPTABLE); // 406

    try {
      MultiMap params = routingContext.request().params();
      Integer employeeId = Integer.valueOf(params.get("employeeId"));
      params.remove("employeeId");
      params.set("employee_id", String.valueOf(employeeId));

      java.util.Date presentDate = new java.util.Date();
      SimpleDateFormat dateFormatter =
          new SimpleDateFormat("dd-MM-yyyy");
      employeeModel.setModified_date(dateFormatter.format(presentDate));
      StringBuilder fieldString = new StringBuilder();
      int i = 0, length = params.size();
      for (String key : params.names()) {
        String value = params.get(key);
        if ("termination_effective_date".equalsIgnoreCase(key)) {
          Date terminationDate = new SimpleDateFormat("MM/dd/yy").parse(value);
          value = new SimpleDateFormat("yyyy-MM-dd").format(terminationDate);
        }

        fieldString.append(key + "='" + value + "'");

        if (i < length - 1) {
          fieldString.append(",");
        }
        i++;
      }
      String queryString = "update Employee set " + fieldString + " ," +
          "modified_date ='" + employeeModel.getModified_date() + "', " +
          "active_status = 0 " +
          " WHERE employee_id = " + employeeId;

      int updatedRows = DatabaseUtil.updateRecord(queryString);
      if (updatedRows == 0) {
        throw new IllegalArgumentException("Bad Request : Invalid Employee ID");
      }

      jsonResponseObject.put("code", HttpStatus.SC_CREATED); // 201
      jsonResponseObject.put("success", true);
      jsonResponseObject.put("data", "Employee Terminated ");

      routingContext.response()
          .setStatusCode(jsonResponseObject.getInteger("code"))
          .putHeader("Content-Type", "application/json; charset=utf-8; " +
              "id=" + employeeTerminationModel.getEmployee_id())
          .end(jsonResponseObject.toString());
    } catch (SQLException updateEmployeeException) {

      logger.error("Sql Exception while updating employee");
      errorObject.put("message", "Invalid Data");
      errorObject.put("description", updateEmployeeException.getMessage());
      jsonResponseObject.put("error", errorObject);
      routingContext.response()
          .setStatusCode(HttpStatus.SC_NOT_ACCEPTABLE) // 406
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
    } catch (ClassNotFoundException e) {
      logger.error("ClassNotFoundException while updating employee");

      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (ParseException e) {
      logger.error("ParseException while updating employee");

      ResponseBuilder.writeInternalServerError(routingContext.response());
    }

  }

  // sadhana created these for getting onboarding status on 22/11/2017

  public void getEmployeeListBasedOnIndex(RoutingContext context) {
    try {

      Connection connectionObject = new MysqldbConf().getConnection();
      PreparedStatement preparedStatement = null;
      JsonObject jsonSuccessObject = new JsonObject();
      jsonSuccessObject.put("code", HttpStatus.SC_OK);
      jsonSuccessObject.put("success", true);

      MultiMap params = context.request().params();
      String constraintString = "";

      JsonArray resultSetArray = new JsonArray();
      JsonObject dataObject = new JsonObject();

      JsonObject searchItems = new JsonObject();

      for (int startCode = 65; startCode <= 90; startCode++) {
        String sorterString = String.valueOf(Character.toChars(startCode));

        int organizationId = Integer.valueOf(params.get("id") == null ?
            "0" : params.get("id"));

        if (organizationId != 0) {
          constraintString += " where organization_id = " + organizationId;
        } else {
          throw new IllegalArgumentException("Required Field(s) are missing ");
        }

        String sorterConstraintString = " LEFT(first_name,1) in ('" + sorterString.toUpperCase() + "'," +
            "'" + sorterString.toLowerCase() + "')";
        if (constraintString.length() > 0) {
          constraintString += " and " + sorterConstraintString;

        } else {
          constraintString = " where " + sorterConstraintString;
        }


        if (constraintString.length() > 0) {
          constraintString += " and active_status = 1 ";
        } else {
          constraintString += " where active_status = 1 ";
        }

        String queryString = "SELECT  count(*) as count  FROM Employee " + constraintString;

        preparedStatement = connectionObject.prepareStatement(queryString);
        ResultSet resultSetObject = preparedStatement.executeQuery();
        while (resultSetObject.next()) {
          int indexCount = resultSetObject.getInt("count");
          searchItems.put(sorterString, indexCount > 0 ? true : false);

        }
        resultSetObject.close();
        preparedStatement.close();
        constraintString = "";

      }

      dataObject.put("items", searchItems);
      resultSetArray.add(dataObject);


      jsonSuccessObject.put("data", resultSetArray);

      context.response()
          .setStatusCode(HttpStatus.SC_OK) // 200
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonSuccessObject.toString());
      connectionObject.close();

    } catch (SQLException e) {

      logger.error("Sql Exception while getting employee details");

      JsonObject jsonResponseObject = new JsonObject();

      JsonObject jsonFailureObject = new JsonObject();
      jsonFailureObject.put("code", HttpStatus.SC_BAD_REQUEST); // 400
      jsonFailureObject.put("message", "Wrong Field Name");
      jsonFailureObject.put("description", e.getMessage());

      jsonResponseObject.put("error", jsonFailureObject);

      context.response()
          .setStatusCode(HttpStatus.SC_BAD_REQUEST) // 400
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());

    } catch (ClassNotFoundException e) {

      logger.error("Sql Exception while getting employee details");

      ResponseBuilder.writeInternalServerError(context.response());
    }
  }

  public void updateOnboardingPacket(RoutingContext routingContext) {
    MultiMap params = routingContext.request().params();
    JsonObject errorObject = new JsonObject();
    DBCRUDUtil dbcrudUtil = new DBCRUDUtil();
    JsonArray jsonArray = new JsonArray();
    try {
      String employeeId = StringUtil.printValidString(routingContext.request().getParam("employee_id"));
      String packetId = StringUtil.printValidString(params.get("packet_id"));
      String firstName = "";
      String lastName = "";
      if (!(null == (params.get("packet_id"))) && !(null == params.get("employee_id"))) {
        if (params.get("packet_id") == "" || params.get("employee_id") == "") {
          throw new IllegalArgumentException("Required params missing");
        }
        String[] employeeSplitter = employeeId.split(",");
        for (String empId : employeeSplitter) {
          if (!(params.get("packet_id").equals("")) || !(employeeId.equals(""))) {
            String selectQuery = "select concat(onboarding_packet,'|',first_name,'|',last_name) " +
                "from Employee where employee_id=" + empId;

            String dataUtil = StringUtil.printValidString(dbcrudUtil.returnValueForQuery(selectQuery));
            String[] splitter = dataUtil.split("\\|");

            String existingPacket = StringUtil.printValidString(splitter[0]);
            firstName = splitter[1];
            lastName = splitter[2];

            if (existingPacket.equals(packetId)) {
              JsonObject jsonResponseObject = new JsonObject();
              jsonResponseObject.put("code", HttpStatus.SC_CONFLICT);//202
              jsonResponseObject.put("error", true);
              jsonResponseObject.put("message", "This packet has been already sent to the employee "
                  + firstName + " " + lastName);
              jsonArray.add(jsonResponseObject);
            } else {
              String updateQuery = "update Employee set onboarding_packet = '" + packetId + "' " +
                  ", onboarding_status = 'Packet Sent' where employee_id =" + empId;
              String resultString = dbcrudUtil.createView(updateQuery);
              if (resultString.equals("success")) {
                sendOnboardingInsForCurrentEmp(empId);
                JsonObject jsonResponseObject = new JsonObject();
                jsonResponseObject.put("code", HttpStatus.SC_ACCEPTED);
                jsonResponseObject.put("success", true);
                jsonResponseObject.put("message", "Onboarding packet has been sent to the employee "
                    + firstName + " " + lastName);
                jsonArray.add(jsonResponseObject);
              } else {
                JsonObject successObject = new JsonObject();
                JsonObject jsonResponseObject = new JsonObject();
                successObject.put("message", "Data invalid");
                jsonResponseObject.put("code", HttpStatus.SC_BAD_REQUEST);
                jsonResponseObject.put("error", true);
                jsonResponseObject.put("message", "Data invalid");
                jsonArray.add(jsonResponseObject);
              }
            }
          } else {
            JsonObject jsonResponseObject = new JsonObject();
            jsonResponseObject.put("code", HttpStatus.SC_BAD_REQUEST);
            jsonResponseObject.put("error", true);
            jsonResponseObject.put("message", "Required params missing");
            routingContext.response()
                .setStatusCode(HttpStatus.SC_BAD_REQUEST)
                .putHeader("Content-Type", "application/json; charset=utf-8")
                .end(jsonResponseObject.toString());
          }
        }
        JsonObject dataObject = new JsonObject();
        dataObject.put("data", jsonArray);
        routingContext.response()
            .setStatusCode(HttpStatus.SC_OK)
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .end(dataObject.toString());

      }
    } catch (SQLException exception) {

      logger.error("Sql Exception while sending onboarding packet to employee");
      JsonObject jsonResponseObject = new JsonObject();
      errorObject.put("code", HttpStatus.SC_NOT_ACCEPTABLE);
      errorObject.put("message", "Invalid Data");
      errorObject.put("description", exception.getMessage());
      jsonResponseObject.put("error", errorObject);
      routingContext.response()
          .setStatusCode(HttpStatus.SC_NOT_ACCEPTABLE)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
    } catch (ClassNotFoundException e) {
      logger.error("Sql Exception while sending onboarding packet to employee");
      ResponseBuilder.writeInternalServerError(routingContext.response());
    }

  }

  public void getEmployeeOnboardingStatus(RoutingContext routingContext) {
    try {
      MultiMap params = routingContext.request().params();
      String status = params.get("status");
      int organizationId = 0;    // sadhana created these on 17/11/2017 --- for organic spec

      String limitValues = params.get("limit");
      String offsetValues = params.get("offset");

      if (params.get("organization_id") != null) {
        organizationId = Integer.parseInt(params.get("organization_id"));
      }

      StringBuilder selectFilter = new StringBuilder();
      StringBuilder limitFilter = new StringBuilder();

      if (limitValues != null && limitValues.length() > 0)
        limitFilter.append(" LIMIT " + limitValues);
      else if (offsetValues != null)
        limitFilter.append(" LIMIT 0");

      if (offsetValues != null && offsetValues.length() > 0) {
        if (limitFilter.length() == 0) {
          throw new IllegalArgumentException("Params for limit missing");
        }

        limitFilter.append(" OFFSET " + offsetValues);
      }

      if (status != null && status.length() > 0) {
        if (status.equalsIgnoreCase("pending")) {
          selectFilter.append(" and Employee.onboarding_status != 'completed' ");
        } else {
          selectFilter.append(" and Employee.onboarding_status = 'completed' ");
        }
      }
      if (organizationId > 0) {
        selectFilter.append(" and organization_id = " + organizationId);
      }


      new EmployeeHandler().getOnboardingDetails(selectFilter, limitFilter, routingContext);
    } catch (IllegalArgumentException e) {
      logger.error("IllegalArgumentException while getting onboarding status");

      ResponseBuilder.writeInternalServerError(routingContext.response());

    }
  }


  // sadhana done these for getting last employee data for particular Organization

  // it used to view the employee status pending details and completed details along with offerprocesscandidate matching data
  public void getOnboardingDetails(StringBuilder selectFilter, StringBuilder limitFilter, RoutingContext
      routingContext) {

    Connection connectionObject = null;

    try {

      PreparedStatement preparedStatement = null;
      JsonObject jsonSuccessObject = new JsonObject();
      jsonSuccessObject.put("code", HttpStatus.SC_OK);
      jsonSuccessObject.put("success", true);

      String queryString = "";
      String query = "";
      String countQueryString = "SELECT COUNT(*) FROM `Employee` WHERE `active_status` = 1 " +
          selectFilter;

      connectionObject = new MysqldbConf().getConnection();

      query = "select first_name,last_name,employee_id,user_id,created_date," +
          "onboarding_packet_date,onboarding_progress," +
          "onboarding_timestamp,Employee.personal_email,Employee.work_email,onboarding_status," +
          "hire_date,status,modified_date,user_id from Employee where active_status = '1' ";

      if (selectFilter.length() > 0) {
        queryString = query + selectFilter + " group by employee_id order by Employee.created_date desc " +
            "" + limitFilter;
      } else {
        queryString = query + " group by employee_id order by Employee.created_date desc " + limitFilter;
      }

      preparedStatement = connectionObject.prepareStatement(queryString);
      ResultSet resultSetObject = preparedStatement.executeQuery();
      JsonArray resultSetArray = new JsonArray();

      String tableName = "LoginDetails";
      String columnName = "date_time";
      String constraintString = "";


      while (resultSetObject.next()) {
        JsonObject dataObject = new JsonObject();

        String status = resultSetObject.getString("onboarding_status");

        constraintString = "login_id = " + resultSetObject.getInt("user_id");
        dataObject.put("first_name", resultSetObject.getString("first_name"));
        dataObject.put("last_name", resultSetObject.getString("last_name"));
        dataObject.put("employee_id", resultSetObject.getInt("employee_id"));
        dataObject.put("onboarding_packet_date",
            resultSetObject.getString("onboarding_packet_date"));
        dataObject.put("sent_date", resultSetObject.getString("created_date"));
        dataObject.put("start_date", resultSetObject.getString("hire_date"));
        dataObject.put("personal_email", resultSetObject.getString("personal_email"));
        dataObject.put("work_email", resultSetObject.getString("work_email"));
        dataObject.put("last_login", dbutil.getTablename(tableName, columnName, constraintString));
        dataObject.put("user_id", StringUtil.printValidString(
            resultSetObject.getString("user_id")));
        dataObject.put("onboarding_status", status);
        dataObject.put("onboarding_progress",
            resultSetObject.getInt("onboarding_progress"));

        if ("Packet Not Sent".equalsIgnoreCase(status)) {
          dataObject.put("onboarding_progress", "Packet Not Sent");
        }

        if ("Completed".equalsIgnoreCase(status)) {
          dataObject.put("completed_date", resultSetObject.getString("modified_date"));
        }

        resultSetArray.add(dataObject);
      }

      resultSetObject.close();
      preparedStatement.close();
      PreparedStatement countPreparedStatement = connectionObject.prepareStatement(countQueryString);
      ResultSet countResultSetObject = countPreparedStatement.executeQuery();
      long count = 0L;
      while (countResultSetObject.next()) {
        count = countResultSetObject.getLong(1);
      }

      countResultSetObject.close();
      countPreparedStatement.close();

      JsonObject dataObject = new JsonObject();
      dataObject.put("totalcount", count);
      dataObject.put("list", resultSetArray);

      jsonSuccessObject.put("data", dataObject);
      routingContext.response()
          .setStatusCode(HttpStatus.SC_OK) // 200
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonSuccessObject.toString());

    } catch (SQLException e) {

      logger.error("Sql Exception while getting onboarding status");

      JsonObject jsonResponseObject = new JsonObject();
      JsonObject jsonFailureObject = new JsonObject();
      jsonFailureObject.put("code", HttpStatus.SC_BAD_REQUEST);
      jsonFailureObject.put("message", "Wrong Field Name");
      jsonFailureObject.put("description", e.getMessage());
      jsonResponseObject.put("error", jsonFailureObject);

      routingContext.response()
          .setStatusCode(HttpStatus.SC_OK)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());

    } catch (ClassNotFoundException e) {
      logger.error("ClassNotFoundException while getting onboarding status");

      ResponseBuilder.writeInternalServerError(routingContext.response());
    } finally {
      if (connectionObject != null) {
        try {
          connectionObject.close();
        } catch (SQLException e) {
          logger.error("SQLException while getting onboarding status - close connection");
        }
      }
    }
  }

  // form name chk done by sadhana to know where the current progress is..
  public List<String> progressCheck(int id, int userId, Connection connectionObject)
      throws ApplicationUtilException {

    List<String> statusCheck = new ArrayList<String>();
    try {
      connectionObject = new MysqldbConf().getConnection();

      String query = "select form_name from OnboardingForm,OnboardingPacket,Employee " +
          " where packet_name = onboarding_packet and " +
          " OnboardingPacket.packet_id = OnboardingForm.packet_id and " +
          " organization_id = " + id +
          " and user_id = " + userId;
      PreparedStatement preparedStatement = connectionObject.prepareStatement(query);

      ResultSet rs = preparedStatement.executeQuery();
      String formName = "";

      while (rs.next()) {
        formName = rs.getString(1);

        statusCheck.add(formName);
      }
      rs.close();
      preparedStatement.close();

    } catch (SQLException e) {
      logger.error("Sql Exception while getting progresscheck in employee");

      throw new ApplicationUtilException("Sql Exception while getting progresscheck in employee");
    } catch (ClassNotFoundException e) {
      logger.error("ClassNotFoundException while getting progresscheck in employee");

      throw new ApplicationUtilException("ClassNotFoundException while getting progresscheck in employee");
    } finally {
      try {
        if (null != connectionObject && connectionObject.isClosed()) {
          connectionObject.close();
        }
      } catch (SQLException e) {
        logger.error(e.getMessage());
      }
    }
    return statusCheck;

  }


  // display post job details for filter in candidate module

  public void getLastEmployeeNo(RoutingContext routingContext) {

    try {
      MultiMap params = routingContext.request().params();

      JsonObject jsonSuccessObject = new JsonObject();
      jsonSuccessObject.put("code", HttpStatus.SC_OK); // 200
      jsonSuccessObject.put("success", true);

      int organizationId = 0;    // sadhana created these on 17/11/2017 --- for organic spec

      if (params.get("organization_id") != null) {
        organizationId = Integer.parseInt(params.get("organization_id"));
      }

      String employeeNo = dbutil.getTablename("Employee", "count(*)",
          "organization_id = " + organizationId);

      int id = Integer.parseInt(employeeNo);

      id = id + 1;

      String domain_name = dbutil.getTablename("Organization",
          "domain_name", "organization_id = " + organizationId);

      employeeNo = domain_name + "-" + id;
      JsonObject dataObject = new JsonObject();
      dataObject.put("employee_no", employeeNo);
      jsonSuccessObject.put("data", dataObject);
      routingContext.response()
          .setStatusCode(HttpStatus.SC_OK) // 200
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonSuccessObject.toString());


    } catch (IllegalArgumentException e) {

      logger.error("IllegalArgumentException while getting last employee id");

      ResponseBuilder.writeInternalServerError(routingContext.response());

      JsonObject jsonResponseObject = new JsonObject();
      JsonObject jsonFailureObject = new JsonObject();
      jsonFailureObject.put("code", HttpStatus.SC_BAD_REQUEST); // 400
      jsonFailureObject.put("message", "Wrong Field Name");
      jsonFailureObject.put("description", e.getMessage());
      jsonResponseObject.put("error", jsonFailureObject);

      routingContext.response()
          .setStatusCode(HttpStatus.SC_OK)  // 200
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());

    }
  }

  public void sendOnboardingInsForCurrentEmp(String employeeId) throws SQLException, ClassNotFoundException {
    Connection connection = new MysqldbConf().getConnection();
    String queryString = "select first_name,personal_email,work_email,user_id,organization_id " +
        "from Employee where employee_id = " + employeeId;
    PreparedStatement preparedStatement = connection.prepareStatement(queryString);
    ResultSet resultSet = preparedStatement.executeQuery();
    String firstName = "";
    String personalEmail = "";
    String workEmail = "";
    int userId = 0;
    int organizationId = 0;

    Mail mail = new Mail();
    while (resultSet.next()) {
      firstName = resultSet.getString("first_name");
      personalEmail = resultSet.getString("personal_email");
      workEmail = resultSet.getString("work_email");
      userId = resultSet.getInt("user_id");
      organizationId = resultSet.getInt("organization_id");
    }

    resultSet.close();
    preparedStatement.close();
    connection.close();
    String organizationName = dbutil.OrganizationName(organizationId, "hris_url");

    mail.onboardingMail(userId, firstName, personalEmail, organizationName, Vertx.vertx());
    mail.onboardingMail(userId, firstName, workEmail, organizationName, Vertx.vertx());

  }

  public void getFieldOption(RoutingContext routingContext) {

    try {
      Connection connectionObject = new MysqldbConf().getConnection();
      PreparedStatement preparedStatement = null;
      MultiMap params = routingContext.request().params();

      //  for organic spec
      int organizationid = Integer.parseInt(params.get("organization_id"));
      JsonObject jsonSuccessObject = new JsonObject();
      jsonSuccessObject.put("code", HttpStatus.SC_OK);
      jsonSuccessObject.put("success", true);
      JsonArray resultSetArray = new JsonArray();

      String fieldOption = "id,location";
      String jobTitle = "id,title";
      String department = "id,department";
      String paygroup = "paygroup_id,paygroup_name";
      String division = "division_id,division_name";
      String employmentstatus = "id,employmentType";

      JsonObject dataObject = new JsonObject();

      dataObject.put("loc", dbutil.getFieldOption(connectionObject, organizationid, fieldOption, "Location"));
      dataObject.put("jobTitle", dbutil.getFieldOption(connectionObject, organizationid, jobTitle, "Title"));
      dataObject.put("department", dbutil.getFieldOption(connectionObject, organizationid, department, "Department"));
      dataObject.put("paygroup", dbutil.getFieldOption(connectionObject, organizationid, paygroup, "PayGroup"));
      dataObject.put("division", dbutil.getFieldOption(connectionObject, organizationid, division, "Division"));
      dataObject.put("employmentstatus", dbutil.getFieldOption(connectionObject, organizationid, employmentstatus, "EmploymentType"));


      resultSetArray.add(dataObject);
      jsonSuccessObject.put("data", resultSetArray);

      routingContext.response()
          .setStatusCode(HttpStatus.SC_OK) // 200
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonSuccessObject.toString());
      connectionObject.close();

    } catch (SQLException e) {

      logger.error("Sql Exception while getting fieldoption in employee details");

      JsonObject jsonResponseObject = new JsonObject();
      JsonObject jsonFailureObject = new JsonObject();
      jsonFailureObject.put("code", HttpStatus.SC_BAD_REQUEST);  // 400
      jsonFailureObject.put("message", "Wrong Field Name");
      jsonFailureObject.put("description", e.getMessage());

      jsonResponseObject.put("error", jsonFailureObject);

      routingContext.response()
          .setStatusCode(HttpStatus.SC_OK)  // 200
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
      e.printStackTrace();

    } catch (ClassNotFoundException e) {
      logger.error("ClassNotFoundException while getting fieldoption in employee details");

      ResponseBuilder.writeInternalServerError(routingContext.response());
    }
  }

  /*   // uploading resume in post method
     public void getEmployeeProfileImage(RoutingContext routingContext) {
         try {
             Set<FileUpload> fileUploadSet = routingContext.fileUploads();
             for (FileUpload upload : fileUploadSet) {
                 //  System.out.println(upload.uploadedFileName());
                 if(!upload.contentType().startsWith("image/")){
                     routingContext.response().putHeader("content-type", "text/html").end("Please upload an Image.");
                 }else{
                     // Process your pdf file
                     candidateModel.setResume(upload.uploadedFileName());
                 }
                 routingContext.response()
                         .setStatusCode(202)
                         .putHeader("Content-Type", "application/json; charset=utf-8")
                         .end(candidateModel.resume );
             }
         } catch (Exception e) {
 //            e.printStackTrace();
             routingContext.response()
                     .setStatusCode(404)
                     .putHeader("Content-Type", "application/json; charset=utf-8")
                     .end("{\"status\": \"failure\",  \"message\":" + e.getMessage() + "}");
         }

     }
 */
  public void getStatusForEmployee(RoutingContext routingContext) {
    List<String> constraintList = new ArrayList<>();
    JsonObject jsonResponseObject = new JsonObject();
    JsonObject errorObject = new JsonObject();
    errorObject.put("code", HttpStatus.SC_NOT_ACCEPTABLE); // 406
    try {
      final String id = routingContext.request().getParam("employeeId");


      if (id.equals("") || id.equals(null))
        throw new IllegalArgumentException("Required params are missing");

      final Integer idAsInteger = Integer.valueOf(id);
      Connection connection = new MysqldbConf().getConnection();
      Map<String, String> formQueryParameters = new HashMap<>();
      formQueryParameters.put("fields", "status");
      formQueryParameters.put("table", "Employee");

      constraintList = new ArrayList<>();
      constraintList.add("employee_id = " + idAsInteger);

      formQueryParameters.put("constraint", DatabaseUtil.buildConstraintString(constraintList));
      QueryStringBuilder builder = new QueryStringBuilder();
      String formQueryString = builder.buildSelectQuery(formQueryParameters);

      ResultSet resultSet = DatabaseUtil.getResultSetForSelectQuery(connection, formQueryString);
      JsonObject dataObject = new JsonObject();
      while (resultSet.next()) {
        dataObject.put("onboarding_status", resultSet.getString("status"));
      }

      if (dataObject.getString("onboarding_status").equals("")
          || dataObject.getString("onboarding_status").equals(null)) {
        throw new IllegalArgumentException("Database connectivity error");
      }

      jsonResponseObject.put("code", HttpStatus.SC_OK); // 200
      jsonResponseObject.put("success", true);
      jsonResponseObject.put("data", dataObject);

      routingContext.response()
          .setStatusCode(HttpStatus.SC_OK) // 200
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
    } catch (SQLException exception) {

      logger.error("Sql Exception while adding onboarding status");

      errorObject.put("message", "Invalid Data");
      errorObject.put("description", exception.getMessage());
      jsonResponseObject.put("error", errorObject);
      routingContext.response()
          .setStatusCode(HttpStatus.SC_NOT_ACCEPTABLE) // 406
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
    } catch (ClassNotFoundException e) {

      logger.error("Sql Exception while adding onboarding status");

      ResponseBuilder.writeInternalServerError(routingContext.response());
    }
  }

  public void addEmployeeImage(RoutingContext routingContext) {
    Employee employeeModel = new Employee();
    MultiMap mapObject = (routingContext.request()).params();
    int id = Integer.parseInt(mapObject.get("employee_id"));
    try {
      Set<FileUpload> fileUploadSet = routingContext.fileUploads();
      for (FileUpload upload : fileUploadSet) {
        if (!upload.contentType().contains("image/png") &&
            !upload.contentType().contains("image/jpeg")) {
          routingContext.response().setStatusCode(org.apache.http.HttpStatus.SC_NOT_FOUND)
              .putHeader("Content-Type", "text/html")
              .end("You can only upload JPG/PNG file!");
        } else {
          BufferedImage originalImage = ImageIO.read(new File(upload.uploadedFileName()));
          if ((originalImage.getWidth() > 300 || originalImage.getHeight() > 300)) {
            throw new
                IllegalArgumentException(" Upload an image with valid " +
                "image size less than ( 300 px x 300 px )");
          }
          employeeModel.setProfile_image(upload.uploadedFileName());
          String name = upload.uploadedFileName().substring(13);
          String profileImages = "";

         /* JWTUtil imagePath = new JWTUtil();

          File source;
          String imagePathFolder = imagePath.getTestImagePath();

          source = new File(imagePathFolder + "/file-uploads/" + name);
         //File destination = new File(imagePathFolder + "/images/" + name);

         // Files.move(source.toPath(), destination.toPath());
          boolean isMoved = source.renameTo(new File(imagePathFolder + "/images/" + name));
          if (!isMoved) {
            throw new FileSystemException("Image upload failed");
          }*/

          profileImages = "file-uploads/" + upload.uploadedFileName().substring(13);
          employeeModel.getEmployee_id();

          String constraintString = " where employee_id = " + id;

          Map<String, String> updateQueryParameters = new HashMap<>();
          updateQueryParameters.put("table", "Employee");
          updateQueryParameters.put("key_value_pair", "profile_image = '" + profileImages + "' ");
          updateQueryParameters.put("constraint", constraintString);


          String updateString = buildQuery(updateQueryParameters, "UPDATE");
          int updatedRows = getUpdatedRows(updateString);
          if (updatedRows == 0) {
            throw new IllegalArgumentException("Image Upload Failed");
          }

          routingContext.response()
              .setStatusCode(org.apache.http.HttpStatus.SC_CREATED)  // 201
              .putHeader("Content-Type", "application/json; charset=utf-8")
              .end(profileImages);

        }
      }
    } /*catch (ApplicationUtilException e) {
      logger.error("Employee Image Upload Failed!");
      routingContext.response()
          .setStatusCode(HttpStatus.SC_NOT_FOUND) // 404
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(e.getMessage());
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } */ catch (IllegalArgumentException e) {
      //logger.error(e.getMessage());
      logger.error("Employee Image Upload Failed!");
      routingContext.response()
          .setStatusCode(HttpStatus.SC_NOT_FOUND) // 404
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(e.getMessage());
      ResponseBuilder.writeInternalServerError(routingContext.response());

    } catch (IOException e) {
      //logger.error(e.getMessage());
      logger.error("Employee Image Upload Failed!");
      routingContext.response()
          .setStatusCode(org.apache.http.HttpStatus.SC_NOT_FOUND)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(e.getMessage());
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (ClassNotFoundException e) {
      logger.error("ClassNotFoundException while Uploading Image!");
      routingContext.response()
          .setStatusCode(org.apache.http.HttpStatus.SC_NOT_FOUND)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(e.getMessage());
    } catch (SQLException e) {
      logger.error("SQLException while Uploading Image!");
      routingContext.response()
          .setStatusCode(org.apache.http.HttpStatus.SC_NOT_FOUND)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(e.getMessage());
    }
  }

  /**
   * Writes corresponding response based on the employee upload
   *
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 12/FEB/2018
   */

  public void uploadEmployeeDocument(RoutingContext routingContext) {
    MultiMap mapObject = (routingContext.request()).params();
    int id = Integer.parseInt(mapObject.get("organization_id"));
    int employeeId = Integer.parseInt(mapObject.get("employee_id"));
    String documentName = mapObject.get("document_name");
    String documentFilePath = mapObject.get("document_file_path");
    JsonObject jsonSuccessObject = new JsonObject();
    JsonObject jsonDataObject = new JsonObject();
    JsonObject jsonResponseObject = new JsonObject();
    JsonObject jsonErrorObject = new JsonObject();
    int insertRecords = 0;
    try {

      if (id == 0 || employeeId == 0
          || StringUtil.printValidString(documentName).trim().length() == 0
          || StringUtil.printValidString(documentFilePath).trim().length() == 0) {
        throw new NoSuchFieldException("Required params missing");
      }
      Connection connection = new MysqldbConf().getConnection();
      String existingQuery = "select 1 from EmployeeSignDocuments " +
          "where document_name = ? and employee_id = ?";
      PreparedStatement existingStatement = connection.prepareStatement(existingQuery);
      existingStatement.setString(1, documentName);
      existingStatement.setInt(2, employeeId);

      ResultSet existingSet = existingStatement.executeQuery();
      boolean isExisting = false;
      while (existingSet.next()) {
        isExisting = existingSet.getBoolean(1);
      }
      existingSet.close();
      existingStatement.close();

      String documentPdfName = "";
      documentPdfName = FileUploadHelper.writePdfStream("private_static/" + id + "/" + employeeId + "/",
          Base64.getMimeDecoder().decode(documentFilePath));

      documentFilePath = "private_static/" + id + "/" + employeeId + "/" + documentPdfName;


      if (isExisting) {
        String updateQuery = "update EmployeeSignDocuments set document_file = ? where employee_id = ? " +
            "and document_name = ?";
        PreparedStatement updateStatement = connection.prepareStatement(updateQuery);
        updateStatement.setString(1, documentFilePath);
        updateStatement.setInt(2, employeeId);
        updateStatement.setString(3, documentName);

        insertRecords = updateStatement.executeUpdate();
        updateStatement.close();

      } else {

        String uploadQuery = "insert into EmployeeSignDocuments (employee_id,document_name,document_file)" +
            "values (?,?,?)";


        PreparedStatement preparedStatement = connection.prepareStatement(uploadQuery);
        preparedStatement.setInt(1, employeeId);
        preparedStatement.setString(2, documentName);
        preparedStatement.setString(3, documentFilePath);

        insertRecords = preparedStatement.executeUpdate();
        preparedStatement.close();
      }
      connection.close();

      if (insertRecords > 0) {
        jsonDataObject.put("message", "Upload Successful");

        jsonSuccessObject.put("code", 200);
        jsonSuccessObject.put("success", true);
        jsonSuccessObject.put("data", jsonDataObject);


        routingContext.response()
            .setStatusCode(200)
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .end(jsonSuccessObject.toString());
      } else {
        jsonDataObject.put("message", "No changes available");

        jsonSuccessObject.put("code", 200);
        jsonSuccessObject.put("success", true);
        jsonSuccessObject.put("data", jsonDataObject);


        routingContext.response()
            .setStatusCode(200)
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .end(jsonSuccessObject.toString());
      }

    } catch (SQLException exception) {
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (ClassNotFoundException exception) {
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (NoSuchFieldException exception) {

      jsonErrorObject.put("code", 400);
      jsonErrorObject.put("message", "Bad Request");
      jsonErrorObject.put("description", exception.getMessage());
      jsonResponseObject.put("error", jsonErrorObject);

      routingContext.response()
          .setStatusCode(jsonErrorObject.getInteger("code"))
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
    }
  }

  public void uploadTaskDocuments(RoutingContext routingContext) {

    MultiMap mapObject = (routingContext.request()).params();
    int id = Integer.parseInt(mapObject.get("organization_id"));
    int employeeId = Integer.parseInt(mapObject.get("employee_id"));
    String taskName = mapObject.get("task_name");
    String taskDescription = mapObject.get("task_description");
    String taskFilePath = mapObject.get("task_file_path");
    JsonObject jsonSuccessObject = new JsonObject();
    JsonObject jsonDataObject = new JsonObject();
    JsonObject jsonResponseObject = new JsonObject();
    JsonObject jsonErrorObject = new JsonObject();
    int insertRecords = 0;
    try {

      if (id == 0 || employeeId == 0 || StringUtil.printValidString(taskName).trim().length() == 0) {
        throw new NoSuchFieldException("Required params missing");
      }

      Connection connection = new MysqldbConf().getConnection();
      String docRequiredQuery = "select OnboardingTask.doc_upload_required " +
          "from OnboardingTask,Employee where Employee.onboarding_packet = OnboardingTask.packet_id " +
          "and Employee.employee_id = ? and OnboardingTask.task_name = ?";
      PreparedStatement employeePreparedStatement = connection.prepareStatement(docRequiredQuery);
      employeePreparedStatement.setInt(1, employeeId);
      employeePreparedStatement.setString(2, taskName);
      ResultSet empResultSet = employeePreparedStatement.executeQuery();

      boolean docUploadRequired = false;
      while (empResultSet.next()) {
        docUploadRequired = empResultSet.getBoolean("doc_upload_required");
      }

      empResultSet.close();
      employeePreparedStatement.close();

      if (docUploadRequired) {
        if (taskFilePath.trim().length() == 0) {
          throw new NoSuchFieldException("Required params missing");
        }
      }

      String existingQuery = "select 1 from EmployeeTaskDocuments " +
          "where task_name = ? and employee_id = ?";
      PreparedStatement existingStatement = connection.prepareStatement(existingQuery);
      existingStatement.setString(1, taskName);
      existingStatement.setInt(2, employeeId);

      ResultSet existingSet = existingStatement.executeQuery();
      boolean isExisting = false;
      while (existingSet.next()) {
        isExisting = existingSet.getBoolean(1);
      }
      existingSet.close();
      existingStatement.close();

      if (isExisting) {
        String updateQuery = "update EmployeeTaskDocuments set task_file = ? where employee_id = ? " +
            "and task_name = ?";
        PreparedStatement updateStatement = connection.prepareStatement(updateQuery);
        updateStatement.setString(1, taskFilePath);
        updateStatement.setInt(2, employeeId);
        updateStatement.setString(3, taskName);

        insertRecords = updateStatement.executeUpdate();
        updateStatement.close();

      } else {

        String uploadQuery = "insert into EmployeeTaskDocuments (employee_id,task_name,task_description,task_file)" +
            "values (?,?,?,?)";


        PreparedStatement preparedStatement = connection.prepareStatement(uploadQuery);
        preparedStatement.setInt(1, employeeId);
        preparedStatement.setString(2, taskName);
        preparedStatement.setString(3, taskDescription);
        preparedStatement.setString(4, taskFilePath);

        insertRecords = preparedStatement.executeUpdate();
        preparedStatement.close();
      }
      connection.close();

      if (insertRecords > 0) {
        jsonDataObject.put("message", "Upload Successful");

        jsonSuccessObject.put("code", 201);
        jsonSuccessObject.put("success", true);
        jsonSuccessObject.put("data", jsonDataObject);

        routingContext.response()
            .setStatusCode(200)
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .end(jsonSuccessObject.toString());
      } else {
        jsonDataObject.put("message", "No changes available");

        jsonSuccessObject.put("code", 201);
        jsonSuccessObject.put("success", true);
        jsonSuccessObject.put("data", jsonDataObject);

        routingContext.response()
            .setStatusCode(200)
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .end(jsonSuccessObject.toString());
      }

    } catch (SQLException exception) {
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (ClassNotFoundException exception) {
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (NoSuchFieldException exception) {

      jsonErrorObject.put("code", 400);
      jsonErrorObject.put("message", exception.getMessage());
      jsonErrorObject.put("description", exception.getMessage());
      jsonResponseObject.put("error", jsonErrorObject);

      routingContext.response()
          .setStatusCode(jsonErrorObject.getInteger("code"))
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
    } catch (IllegalArgumentException exception) {
      jsonErrorObject.put("code", 400);
      jsonErrorObject.put("message", exception.getMessage());
      jsonErrorObject.put("description", exception.getMessage());
      jsonResponseObject.put("error", jsonErrorObject);

      routingContext.response()
          .setStatusCode(jsonErrorObject.getInteger("code"))
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
    }
  }

  public void uploadFilesForEmployee(RoutingContext routingContext) {
    MultiMap mapObject = (routingContext.request()).params();
    int id = Integer.parseInt(mapObject.get("organization_id"));
    int employeeId = Integer.parseInt(mapObject.get("employee_id"));
    try {
      String filePath = "";
      if (id == 0 || employeeId == 0) {
        throw new NoSuchFieldException("Required params missing");
      }
      Set<FileUpload> fileUploadSet = routingContext.fileUploads();
      for (FileUpload fileUpload : fileUploadSet) {
        FileTypeEnum fileTypeEnum = FileHelper.getFileType(fileUpload);
        String sourceFile = fileUpload.uploadedFileName();
        String destinationFile = fileUpload.uploadedFileName().split("/")[1] +
            fileTypeEnum.getExtension();
        // TODO this is a test target directory to be replaced later with actual logic
        String destinationFolderPath = "private_static/" + id + "/" + employeeId + "/";
        FileHelper.move(sourceFile, destinationFolderPath, destinationFile);
        filePath = destinationFolderPath + destinationFile;
      }

      routingContext.response()
          .setStatusCode(org.apache.http.HttpStatus.SC_CREATED)  // 201
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(filePath);

    } catch (NoSuchFieldException e) {
      logger.error(e.getMessage());
      ResponseHelper.writeInternalServerError(routingContext.response());
    }
  }

  public void viewUserList(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    String[] allowedParams = {"organization_id", "limit", "offset", "employee_id","search_filter"};
    String[] requiredParams = {"organization_id", "employee_id"};
    String[] userViewParmas = {"organization_id", "limit", "offset", "employee_id","search_filter"};
    List<Validator> validators = new ArrayList<>();

    Validator employeeViewValidator = new ValidatorImpl(userViewParmas, new HashMap<String, FieldOptions>() {{
      put("organization_id", FieldOptionsCommon.NUMBER);
      put("limit", FieldOptionsCommon.NUMBER);
      put("offset", FieldOptionsCommon.NUMBER);
      put("employee_id", FieldOptionsCommon.NUMBER);
      put("search_filter",FieldOptionsCommon.ANY_ALLOW_EMPTY);
    }});
    validators.add(employeeViewValidator);

    Employee employee = (Employee) routingContext.data().get("employee");

    if (employee.getAccess_level().equals(EmployeeBOEnum.EMPLOYEE_ACCESS)) {
      ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST, "You cannot consume service",
          "You cannot consume service", response);
      return;
    }

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }
    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
          JsonArray AllEmployees = employeeBO.getAllUsersList(params);
          if (AllEmployees.isEmpty())
            EmployeeHandler.writeArrayResponse(requestParams, httpServerResponse,
                dbHandle, AllEmployees, EmployeeBOEnum.TABLE_EMPTY);
          else
            EmployeeHandler.writeArrayResponse(requestParams, httpServerResponse,
                dbHandle, AllEmployees, EmployeeBOEnum.TABLE_VALUES);
        });
  }

  public void addUser(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"organization_id", "profile_image", "first_name", "last_name", "work_email", "primary_phone",
        "access_level", "department", "job_title", "city", "state", "country", "primary_region_code"};
    String[] requiredParams = {"organization_id", "first_name", "last_name", "work_email", "access_level"};
    String[] userAddParams = {"organization_id", "profile_image", "first_name", "last_name", "work_email", "primary_phone",
        "access_level", "department", "job_title", "city", "state", "country", "primary_region_code"};

    List<Validator> validators = new ArrayList<>();
    Validator employeeAddValidator = new ValidatorImpl(userAddParams,
        new HashMap<String, FieldOptions>() {{
          put("first_name", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 50));
          put("last_name", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 50));
          put("organization_id", FieldOptionsCommon.NUMBER);
          put("profile_image", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY,
              -1, 255));
          put("work_email", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 100));
          put("primary_phone", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY,
              -1, 20));
          put("access_level", new FieldOptionsImpl(FieldOptionsCommon.ANY,
              -1, 100));
          put("department", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY,
              -1, 100));
          put("job_title", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY,
              -1, 100));
          put("city", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY,
              -1, 30));
          put("state", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY,
              -1, 20));
          put("country", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 30));
          put("primary_region_code", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY,
              -1, 10));
    }});
    validators.add(employeeAddValidator);

    Employee employee = (Employee) routingContext.data().get("employee");

    if (employee.getAccess_level().equals(EmployeeBOEnum.EMPLOYEE_ACCESS.getDescription())) {
      ResponseHelper.writeErrorResponse(HttpStatus.SC_UNAUTHORIZED, "You cannot consume service",
          "You cannot consume service", response);
      return;
    }

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHandle dbHandle = DbHelper.getDBHandle(routingContext, false);
    if (null == dbHandle) {
      ResponseHelper.writeInternalServerError(response);
      return;
    }

    try {
      if (!StringUtil.printValidString(params.get("primary_phone")).equals("")) {
        boolean isValid = false;
        isValid = StringUtil.isValidPhoneNumber(params.get("primary_phone"), params.get("primary_region_code"));

        if (!isValid) {
          ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_BAD_REQUEST,
              "Please provide proper phone number",
              "Please provide proper phone number",
              routingContext.response());
          return;
        }
      }

      EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
      JsonObject dataobject = new JsonObject();

      if (params.get("profile_image") != null) {
        String filePath = " ";
        String file = params.get("profile_image");
        String base64Image = file.split(",")[1];
        String sourceFile = FileUploadHelper.writeImageStream(base64Image, util.randomString(2));
        if (null != sourceFile) {

          String destinationFolderPath = EndpointConstant.publicStaticImage + File.separator +
              routingContext.request().getParam("organization_id");
          FileHelper.move(sourceFile, destinationFolderPath, sourceFile);

          filePath = destinationFolderPath + File.separator + sourceFile;
          params.add("profile_image", filePath);
        } else {
          dataobject.put("message", "Upload image with 300*300 pixel size");
          EmployeeHandler.writeResponse(params, response,
              dbHandle, dataobject, EmployeeBOEnum.IMAGE_FORMAT_WRONG);
        }
      }
      int addId = 0;
      addId = employeeBO.addNewUser(params);

      String loginId = "" + addId;
      if (addId > 0) {
        HttpClient kongClient = KongServiceHelper.executeWithKongClient(routingContext.vertx());
        KongServiceHelper.addConsumer(kongClient, Long.parseLong("" + addId), clientResponse -> {
          if (clientResponse.statusCode() == KongHttpStatusCode.ADD.getStatusCode()) {
            clientResponse.bodyHandler(buffer -> {
              String consumerId = new JsonObject(buffer).getString("id");
              String accessRole = params.get("access_level");
              if (accessRole.equals("admin")) {
                accessRole = "Admin";
              } else {
                accessRole = "Employee";
              }
              Role role = null;
              boolean exception = false;
              try {
                new LoginBOImpl(dbHandle).updateConsumerId(consumerId, Long.parseLong(loginId));
                role = new RoleBOImpl(dbHandle).getByName(accessRole, Long.parseLong(params.get("organization_id")));
              } catch (SQLException e) {
                DbHelper.safeRollback(dbHandle);
                ResponseHelper.writeInternalServerError(routingContext.response());
                exception = true;
              } catch (DbHandleException e) {
                DbHelper.safeRollback(dbHandle);
                ResponseHelper.writeInternalServerError(routingContext.response());
                exception = true;
              } finally {
                if (exception) {
                  kongClient.close();
                  dbHandle.checkAndCloseConnection();
                }
              }
              KongServiceHelper.addConsumerRoles(kongClient, consumerId, "" + role.getId(), httpClientResponse -> {
                dataobject.put("message", "User added successfully");
                try {
                  EmployeeHandler.writeResponse(params, response,
                      dbHandle, dataobject, EmployeeBOEnum.EMPLOYEE_ADDED);
                  kongClient.close();
                  dbHandle.checkAndCloseConnection();
                } catch (SQLException e) {
                  kongClient.close();
                  DbHelper.safeRollback(dbHandle);
                  ResponseHelper.writeInternalServerError(response);
                } catch (DbHandleException e) {
                  kongClient.close();
                  DbHelper.safeRollback(dbHandle);
                  ResponseHelper.writeInternalServerError(response);
                }
              });
            });
          } else {
            DbHelper.safeRollback(dbHandle);
            dbHandle.checkAndCloseConnection();
            kongClient.close();
            ResponseHelper.writeInternalServerError(response);
          }
        });
      } else if (addId == -1) {
        dataobject.put("message", "User already exists");
        EmployeeHandler.writeResponse(params, response,
            dbHandle, dataobject, EmployeeBOEnum.USER_ALREADY_EXISTS);
        dbHandle.checkAndCloseConnection();

      } else {
        dataobject.put("message", "User added failed");
        EmployeeHandler.writeResponse(params, response,
            dbHandle, dataobject, EmployeeBOEnum.EMPLOYEE_ADD_FAILED);
        dbHandle.checkAndCloseConnection();
      }
    } catch (SQLException e) {
      logger.error(e.getMessage());
      ResponseHelper.writeInternalServerError(response);
      dbHandle.checkAndCloseConnection();
    } catch (DbHandleException e) {
      logger.error(e.getMessage());
      ResponseHelper.writeInternalServerError(response);
      dbHandle.checkAndCloseConnection();
    } catch (NumberParseException e) {
      logger.error(e.getMessage());
      ResponseHelper.writeInternalServerError(response);
      dbHandle.checkAndCloseConnection();
    }
  }

  public void deleteUser(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"employeeId", "organization_id"};
    String[] requiredParams = {"employeeId", "organization_id"};
    String[] userDeleteParams = {"employeeId", "organization_id"};

    List<Validator> validators = new ArrayList<>();
    Validator employeeDeleteValidator = new ValidatorImpl(userDeleteParams, new HashMap<String, FieldOptions>() {{
      put("employeeId", FieldOptionsCommon.NUMBER);
      put("organization_id", FieldOptionsCommon.NUMBER);

    }});
    validators.add(employeeDeleteValidator);

    Employee employee = (Employee) routingContext.data().get("employee");

    if (employee.getAccess_level().equals(EmployeeBOEnum.EMPLOYEE_ACCESS.getDescription())) {
      ResponseHelper.writeErrorResponse(HttpStatus.SC_UNAUTHORIZED, "You cannot consume service",
          "You cannot consume service", response);
      return;
    }

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
          int deletedCount = employeeBO.deleteUser(params);
          JsonObject dataObject = new JsonObject();
          if (deletedCount == 2) {
            dataObject.put("message", "Deleted successfully!");
            EmployeeHandler.writeResponse(requestParams, httpServerResponse,
                dbHandle, dataObject, EmployeeBOEnum.DELETE_SUCCESS);
          } else {
            dataObject.put("message", "Deleted failed");
            EmployeeHandler.writeResponse(requestParams, httpServerResponse,
                dbHandle, dataObject, EmployeeBOEnum.DELETE_FAILED);
          }
        });

  }

  public void editUser(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"employee_id", "organization_id", "profile_image", "first_name", "last_name", "work_email", "primary_phone",
        "access_level", "department", "job_title", "city", "state", "country", "primary_region_code"};
    String[] requiredParams = {"employee_id", "organization_id", "access_level", "work_email"};
    String[] userEditParams = {"employee_id", "organization_id", "profile_image", "first_name", "last_name", "work_email", "primary_phone",
        "access_level", "department", "job_title", "city", "state", "country", "primary_region_code"};

    List<Validator> validators = new ArrayList<>();


    Validator employeeEditValidator = new ValidatorImpl(userEditParams, new HashMap<String, FieldOptions>() {{

      put("first_name", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 50));
      put("last_name", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 50));
      put("employee_id", FieldOptionsCommon.NUMBER);
      put("organization_id", FieldOptionsCommon.NUMBER);
      put("limit", FieldOptionsCommon.ANY_ALLOW_EMPTY);
      put("offset", FieldOptionsCommon.ANY_ALLOW_EMPTY);
      put("profile_image", FieldOptionsCommon.ANY_ALLOW_EMPTY);
      put("work_email", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 100));
      put("primary_phone", FieldOptionsCommon.ANY_ALLOW_EMPTY);
      put("access_level", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 100));
      put("department", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 100));
      put("job_title", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 100));
      put("city", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 30));
      put("state", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 20));
      put("country", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 30));
      put("primary_region_code", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 10));
    }});

    validators.add(employeeEditValidator);

    Employee employeeModel = (Employee) routingContext.data().get("employee");

    if (employeeModel.getAccess_level().equals(EmployeeBOEnum.EMPLOYEE_ACCESS.getDescription())) {

      if (employeeModel.getEmployee_id() != Integer.valueOf(params.get("employee_id"))) {
        ResponseHelper.writeErrorResponse(HttpStatus.SC_UNAUTHORIZED, "You cannot consume service",
            "You cannot consume service", response);
        return;
      }

    }

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    if (!StringUtil.printValidString(params.get("primary_phone")).equals("")) {
      boolean isValid = false;
      try {
        isValid = StringUtil.isValidPhoneNumber(params.get("primary_phone"), params.get("primary_region_code"));
      } catch (NumberParseException e) {
        logger.error(e.getMessage());
        ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_BAD_REQUEST,
            e.getMessage(),
            e.getMessage(),
            routingContext.response());
      }

      if (!isValid) {
        ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_BAD_REQUEST,
            "Please provide proper phone number",
            "Please provide proper phone number",
            routingContext.response());
        return;
      }
    }
    DbHandle dbHandle = DbHelper.getDBHandle(routingContext, false);
    if (null == dbHandle) {
      ResponseHelper.writeInternalServerError(response);
      return;
    }
    EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
    LoginDAO loginDAO = new LoginDAOImpl(dbHandle);
    JsonObject dataobject = new JsonObject();
    long employeeId = 0;
    employeeId = Long.parseLong(params.get("employee_id"));
    int addId = 0;
    int userId = 0;
    String consumerId = "";
    try {
      addId = employeeBO.editUser(routingContext.vertx(), params);
      Employee employee = employeeBO.getEmployeeDetails(employeeId);
      userId = employee.getUser_id();
      consumerId = loginDAO.getConsumerId(userId);

      String loginId = "" + addId;
      if (addId > 0) {
        HttpClient kongClient = KongServiceHelper.executeWithKongClient(routingContext.vertx());
        String accessRole = params.get("access_level");
        if (accessRole.equals("admin")) {
          accessRole = "Admin";
        } else {
          accessRole = "Employee";
        }
        Role role = null;
        new LoginBOImpl(dbHandle).updateConsumerId(consumerId, Long.parseLong(loginId));
        role = new RoleBOImpl(dbHandle).getByName(accessRole, Long.parseLong(params.get("organization_id")));
        KongServiceHelper.editConsumerRoles(kongClient, consumerId, "" + role.getId(), httpClientResponse -> {
          dataobject.put("message", "User updated successfully");
          try {
            EmployeeHandler.writeResponse(params, response,
                dbHandle, dataobject, EmployeeBOEnum.UPDATE_SUCCESS);
          } catch (SQLException e) {
            DbHelper.safeRollback(dbHandle);
            ResponseHelper.writeInternalServerError(response);
          } catch (DbHandleException e) {
            DbHelper.safeRollback(dbHandle);
            ResponseHelper.writeInternalServerError(response);
          } finally {
            kongClient.close();
            dbHandle.checkAndCloseConnection();
          }
        });
      } else if (addId == -1) {
        dataobject.put("message", "User already exists");
        EmployeeHandler.writeResponse(params, response,
            dbHandle, dataobject, EmployeeBOEnum.USER_ALREADY_EXISTS);

      } else {
        dataobject.put("message", "No changes Found");
        EmployeeHandler.writeResponse(params, response,
            dbHandle, dataobject, EmployeeBOEnum.UPDATE_SUCCESS);
      }
    } catch (SQLException e) {
      DbHelper.safeRollback(dbHandle);
      ResponseHelper.writeInternalServerError(response);
      dbHandle.checkAndCloseConnection();
    } catch (DbHandleException e) {
      DbHelper.safeRollback(dbHandle);
      ResponseHelper.writeInternalServerError(response);
      dbHandle.checkAndCloseConnection();
    }
  }

  public void uploadProfileImage(RoutingContext routingContext) {

    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"employee_id", "organization_id", "image"};
    String[] requiredParams = {"employee_id", "organization_id"};
    String[] userDeleteParams = {"employee_id", "organization_id", "image"};

    List<Validator> validators = new ArrayList<>();
    Validator employeeImageValidator = new ValidatorImpl(userDeleteParams, new HashMap<String, FieldOptions>() {{
      put("employee_id", FieldOptionsCommon.NUMBER);
      put("organization_id", FieldOptionsCommon.NUMBER);
      put("image", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));

    }});
    validators.add(employeeImageValidator);

    Employee employeeModel = (Employee) routingContext.data().get("employee");

    if (employeeModel.getAccess_level().equals(EmployeeBOEnum.EMPLOYEE_ACCESS.getDescription())) {

      if (employeeModel.getEmployee_id() != Integer.valueOf(params.get("employee_id"))) {
        ResponseHelper.writeErrorResponse(HttpStatus.SC_UNAUTHORIZED, "You cannot consume service",
            "You cannot consume service", response);
        return;
      }

    }

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }
    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          Set<FileUpload> fileUploadSet = routingContext.fileUploads();
          for (FileUpload fileUpload : fileUploadSet) {

            FileTypeEnum fileTypeEnum = FileHelper.getImageFormatType(fileUpload);
            try {
              if (fileTypeEnum != null) {
                if (FileUploadHelper.isValidDimension(fileUpload.uploadedFileName(), 300, 300)) {
                  long organizationId = 0;
                  int employeeId = 0;
                  int uploadedCount = 0;
                  String imagePath = "";
                  organizationId = Long.parseLong(params.get("organization_id"));
                  employeeId = Integer.parseInt(params.get("employee_id"));
                  String sourceFile = fileUpload.uploadedFileName();
                  String destinationFile = fileUpload.uploadedFileName().split(File.separator)[1] +
                      fileTypeEnum.getExtension();
                  String destinationFolderPath = EndpointConstant.privateStaticRoot + File.separator +
                      organizationId + File.separator + employeeId;
                  FileHelper.move(sourceFile, destinationFolderPath, destinationFile);
                  imagePath = destinationFolderPath + File.separator + destinationFile;
                  EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
                  uploadedCount = employeeBO.uploadProfileImage(imagePath, organizationId, employeeId);
                  if (uploadedCount > 0) {
                    dbHandle.commit();
                    routingContext.response()
                        .setStatusCode(HttpStatus.SC_CREATED)  // 201
                        .putHeader("Content-Type", "application/json; charset=utf-8")
                        .end(destinationFolderPath + File.separator + destinationFile);
                  }

                } else {
                  ResponseHelper.writeStringErrorResponse(
                      "Please upload an image of size 300 X 300", response);
                }
              } else {
                ResponseHelper.writeStringErrorResponse(
                    "Please upload an image of JPG/PNG format", response);
              }
            } catch (IOException e) {
              ResponseHelper.writeStringErrorResponse(
                  "You can only upload PNG/JPEG images with 300x300px size", response);
            }
          }
        });
  }

  public String generateToken(Login loginObject) throws UnsupportedEncodingException {
    String generatedToken = "";
    //java.sql.Timestamp expiryTime = new Timestamp(System.currentTimeMillis() + 30 * 60 * 1000);
    String key = JWTUtil.getEncryptKey();
    Algorithm algorithm = Algorithm.HMAC256(key);
    generatedToken = JWT.create().withIssuer("auth0")
        .withClaim("organization_id", String.valueOf(loginObject.getOrganizationId()))
        .withClaim("user_id", String.valueOf(loginObject.getId()))
        .withClaim("user_type", loginObject.getUserType())
        .sign(algorithm);
    return generatedToken;
  }

  public void getDashBoardCount(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"employeeId"};
    String[] requiredParams = {"employeeId"};
    String[] userDeleteParams = {"employeeId"};

    List<Validator> validators = new ArrayList<>();
    Validator employeeImageValidator = new ValidatorImpl(userDeleteParams, new HashMap<String, FieldOptions>() {{
      put("employeeId", FieldOptionsCommon.NUMBER);
    }});
    validators.add(employeeImageValidator);

    Employee employeeModel = (Employee) routingContext.data().get("employee");

    if (employeeModel.getAccess_level().equals(EmployeeBOEnum.EMPLOYEE_ACCESS.getDescription())) {

      if (employeeModel.getEmployee_id() != Integer.valueOf(params.get("employeeId"))) {
        ResponseHelper.writeErrorResponse(HttpStatus.SC_UNAUTHORIZED, "You cannot consume service",
            "You cannot consume service", response);
        return;
      }

    } else {
      ResponseHelper.writeErrorResponse(HttpStatus.SC_UNAUTHORIZED, "You cannot consume service",
          "You cannot consume service", response);
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
          JsonObject dataObject = new JsonObject();
          dataObject = employeeBO.getDashBoardCount(requestParams);

          if (dataObject.getBoolean("dummy_data") == true) {
            dataObject.remove("dummy_data");
            ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
                "Count retrived", dataObject, true, httpServerResponse);
          } else {
            dataObject.remove("dummy_data");
            ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
                "Count retrived", dataObject, false, httpServerResponse);
          }
        });
  }
}