package com.auzmor.impl.handler;

import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.model.Tasks.ProjectModel;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.builder.ApiBuilder;
import com.auzmor.impl.model.Tasks.TaskModel;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.impl.util.DateUtil;
import com.auzmor.impl.util.ModelUtil;

import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

public class TaskHandler extends ApiBuilder {

    DateUtil dateUtil = new DateUtil();

    public void addtaskcontroller(RoutingContext context){
        TaskModel taskModel = new TaskModel();

        MultiMap params = context.request().params();
        try {
            for (String key : params.names()) {
                ModelUtil.setValues(key, params.get(key), taskModel);
            }

            Map<String, Boolean> paramMapper = new HashMap<>();

            paramMapper.put("task_title",true);
            paramMapper.put("assign_to",true);
            paramMapper.put("assign_by",true);
            paramMapper.put("due_date",true);
            paramMapper.put("time_due",true);
            paramMapper.put("project_id",false);
            paramMapper.put("recurrence",false);
            paramMapper.put("priority",false);
            paramMapper.put("description",false);
            paramMapper.put("organization_id",true);


            Date dNow = new Date();
            SimpleDateFormat date =
                    dateUtil.getDate();
            String created_date = date.format(dNow);

            Map<String, Object> insertionObjects = generateInsertionObject(params, paramMapper, taskModel);

            List<String> queryStringValueList = DatabaseUtil.prepareQueryStringValues(insertionObjects);
            String columnValueString = queryStringValueList.get(0);
            String entryValueString = queryStringValueList.get(1);

            columnValueString+= ",created_date";
            entryValueString+=", '"+created_date+"'";


            Map<String, String> insertQueryParameters = new HashMap<>();
            insertQueryParameters.put("table", "Tasks");
            insertQueryParameters.put("column_names", columnValueString);
            insertQueryParameters.put("column_values", entryValueString);

            String countchk = new DatabaseUtil().getTablename("Tasks","count(*)","task_title = '"+ taskModel.getTask_title() +"' and inactive_status = '0' and organization_id = '"+taskModel.organization_id+"'  ");

            int count = Integer.parseInt(countchk);
            if(count == 0) {
                String sqlQuery = buildQuery(insertQueryParameters, "INSERT");

                int id = insertRecord(sqlQuery);
                taskModel.task_id = id;

                JsonObject successObject = new JsonObject();
                successObject.put("id", taskModel.getTask_id());
                successObject.put("message", taskModel.getTask_title() + " are created successfully");

                getSuccessObject(successObject, context.response(), "POST");
            }else{
                throw new SQLException("Tasks Title already exists");
            }
        } catch (SQLException sqlexception){
            getErrorObject("Tasks Title's already exists ",sqlexception.getMessage(),context.response(),"POST");
        }
        catch (Exception e) {
            e.printStackTrace();
            String errorDescription = e.getMessage();
            getErrorObject("Invalid Data ",errorDescription,context.response(),"POST");
        }

        }


    public void updatetaskcontroller(RoutingContext routingContext) {

        String taskId = routingContext.request().getParam("task_id");
        MultiMap params = routingContext.request().params();
        JsonObject jsonResponseObject = new JsonObject();

        String updateString = "";

        try {
            Connection connection = new MysqldbConf().getConnection();

            if (params.names().contains("task_id")) {

                params.remove("task_id");
            }

            String updateStatement = generateUpdateStatement(params, new TaskModel());

            Date dNow = new Date();
            SimpleDateFormat date =
                    dateUtil.getDate();
            String modified_date = date.format(dNow);
            String constraintString ="";


            String name ="";

            updateStatement+=",modified_date = '"+modified_date+"' ";
           // String constraintString = "where task_id = " + taskId;
            if(taskId.contains(",")){
                constraintString = "where task_id in (" + taskId +") ";
                name = "Tasks Completed Successfully";
            }else{
                constraintString = "where task_id = " + taskId;

                DatabaseUtil dbutil = new DatabaseUtil();
                 name = dbutil.getTablename("Tasks","task_title","task_id = " + taskId);
               name = name+" updated Successfully!";
            }
            //System.out.println("updateStatement::"+ updateStatement);

            Map<String, String> updateQueryParameters = new HashMap<>();
            updateQueryParameters.put("table", "Tasks");
            updateQueryParameters.put("key_value_pair", updateStatement);
            updateQueryParameters.put("constraint", constraintString);

            updateString = buildQuery(updateQueryParameters, "UPDATE");

            int updatedRows = getUpdatedRows(updateString);
            if (updatedRows == 0) {
                name ="No Changes Detected";
            }
            connection.close();
            jsonResponseObject.put("message", name);
            getSuccessObject(jsonResponseObject, routingContext.response(), "PUT");

        } catch (Exception exception) {
            exception.printStackTrace();
            getErrorObject("Invalid Data", exception.getMessage(), routingContext.response(), "PUT");
        }
    }


    public void deletetaskcontroller(RoutingContext context) {

        MultiMap params = context.request().params();
        try {
            int task_id = Integer.valueOf(params.get("task_id"));

            String constraintString = "where task_id = " + task_id;

            Map<String,String> updateQueryParameters = new HashMap<>();
            updateQueryParameters.put("table","Tasks");
            updateQueryParameters.put("key_value_pair","inactive_status = 1");
            updateQueryParameters.put("constraint",constraintString);

            String deletePacketQuery = buildQuery(updateQueryParameters,"UPDATE");
            int deletedRecords = getUpdatedRows(deletePacketQuery);

            if (deletedRecords == 0) {
                throw new Exception("Invalid Request to delete Task");
            }
            DatabaseUtil dbutil = new DatabaseUtil();
            String name = dbutil.getTablename("Tasks","task_title","task_id = " + task_id);
            JsonObject dataObject = new JsonObject();
            dataObject.put("message","'" + name + "' Deleted Successfully");

            getSuccessObject(dataObject,context.response(),"DELETE");

        } catch (Exception deletePacketException) {
            deletePacketException.printStackTrace();
            getErrorObject("Deletion Error",deletePacketException.getMessage(),
                    context.response(),"DELETE");

        }

    }

    public void gettaskcontroller(RoutingContext context) {

        ResultSet resultSetObject = null;

        try {
            Connection connectionObject = new MysqldbConf().getConnection();
            HttpServerRequest request = context.request();

            String id = request.getParam("organization_id");

            String taskIdString = request.getParam("task_id");

            String assignIdString = request.getParam("assign_to");

            String status = request.getParam("status");

            String fieldString = request.getParam("fields");

            List<String> constraintList = new ArrayList<>();

            if (id != null) {
                constraintList.add("organization_id = " + id);
            }

            constraintList.add("inactive_status = 0");

            if (taskIdString != null) {
                constraintList.add("task_id = " + taskIdString);
            }

            if (assignIdString != null) {
                constraintList.add("assign_to = " + assignIdString);
            }

            if (status != null) {
                constraintList.add("task_status = '" + status+"' ");
            }


            String constraintString = DatabaseUtil.buildConstraintString(constraintList);

            Map<String, String> queryStringParameters = new HashMap<>();

            if(fieldString != null){
                fieldString+=",task_id";
                queryStringParameters.put("fields", fieldString);
            }else {
                queryStringParameters.put("fields", "*");
            }
            queryStringParameters.put("table", "Tasks");
            queryStringParameters.put("constraint", constraintString);

            String queryString = buildQuery(queryStringParameters,"SELECT");

            resultSetObject = getResultSetObject(connectionObject,queryString);

            JsonArray TaskArray = new JsonArray();


            Date dNow = new Date();
            SimpleDateFormat date =
                    dateUtil.getDate();
            String current_date = date.format(dNow);

            String columnName = "";
            String columnType = "";
            String due_date = "";
            String time = "";
            int task_id =0;
            int projectid =0;
            int assign_to = 0;
            int assign_by = 0;

            String name = StringUtil.printValidString(new DatabaseUtil().getTablename("Tasks","task_status","task_id ="+task_id));

            if(fieldString != null || assignIdString != null || status != null || id != null){
                while (resultSetObject.next()) {
                    JsonObject TaskComponentObject = new JsonObject();
                    ResultSetMetaData resultSetMetaData = resultSetObject.getMetaData();
                    for (int i = 1; i <= resultSetObject.getMetaData().getColumnCount(); i++) {
                        columnName = resultSetMetaData.getColumnName(i);
                        columnType = resultSetMetaData.getColumnTypeName(i);

                        if ("BIT".equalsIgnoreCase(columnType)) {
                            TaskComponentObject.put(columnName, resultSetObject.getBoolean(columnName));
                        } else if ("INT".equalsIgnoreCase(columnType)) {
                              task_id = resultSetObject.getInt("task_id");

                              projectid =resultSetObject.getInt("project_id");

                              assign_to =resultSetObject.getInt("assign_to");

                              assign_by =resultSetObject.getInt("assign_by");

                            TaskComponentObject.put(columnName, resultSetObject.getInt(columnName));
                        } else if ("VARCHAR".equalsIgnoreCase(columnType) || "BLOB".equalsIgnoreCase(columnType)) {
                            String columnValue = resultSetObject.getString(columnName);

                            if (columnName.equalsIgnoreCase("due_date")) {
                                if (!columnValue.equals("")) {
                                    due_date = columnValue;
                                }
                            }
                            if (columnName.equalsIgnoreCase("time_due")) {
                                if (!columnValue.equals("")) {
                                    time = columnValue;
                                }
                            }
                           if(columnName.equalsIgnoreCase("task_status")) {

                                String task_status_val = StringUtil.printValidString(columnValue);

                                if (!task_status_val.equals("OverDue")) {
                                    {
                                        String due_date_time = due_date + " " + time;
                                        String time_left = dateUtil.getDatetimeDifference(due_date_time);
                                        if (time_left.equalsIgnoreCase("OverDue")) {

                                            if (name.equals("") || name.length() == 0) {
                                                String constraint = "where task_id = " + task_id + " and task_status is null";
                                                Map<String, String> updateQueryParameters = new HashMap<>();
                                                updateQueryParameters.put("table", "Tasks");
                                                updateQueryParameters.put("key_value_pair", "task_status = 'OverDue' ");
                                                updateQueryParameters.put("constraint", constraint);

                                                String deletePacketQuery = buildQuery(updateQueryParameters, "UPDATE");
                                                int records = getUpdatedRows(deletePacketQuery);
                                            }
                                        }
                                        TaskComponentObject.put("time_left", time_left);
                                      }
                              }
                            }else{

                                String due_date_time = due_date + " " + time;
                                String time_left = dateUtil.getDatetimeDifference(due_date_time);
                                TaskComponentObject.put("time_left", time_left);
                            }

                            TaskComponentObject.put(columnName, columnValue);
                        }

                        TaskComponentObject.put("project_name",new DatabaseUtil().getTablename("Projects","project_name","project_id ="+projectid));

                        TaskComponentObject.put("assign_to",new DatabaseUtil().getUsername("Employee","first_name,last_name","employee_id ="+assign_to));

                          String userchk =  new DatabaseUtil().getTablename("Employee","user_id","employee_id ="+assign_to);

                            int useridchk = 0;
                            if(userchk.equals("")){
                                useridchk = 0;
                            }else{
                                useridchk = Integer.parseInt(userchk);
                            }

                           if(useridchk == assign_by) {
                               TaskComponentObject.put("assign_by", "Me");
                           }else {
                               TaskComponentObject.put("assign_by", new DatabaseUtil().getUsername("Login", "first_name,last_name", "id =" + assign_by));
                           }
                    }

                    TaskArray.add(TaskComponentObject);
                }

                getSuccessObject(TaskArray, context.response(), "GET");
            }else {

                JsonObject jsonDataObject = new JsonObject();

                while (resultSetObject.next()) {
                    ResultSetMetaData resultSetMetaData = resultSetObject.getMetaData();
                    for (int i = 1; i <= resultSetObject.getMetaData().getColumnCount(); i++) {
                        columnName = resultSetMetaData.getColumnName(i);
                        columnType = resultSetMetaData.getColumnTypeName(i);

                        if ("BIT".equalsIgnoreCase(columnType)) {
                            jsonDataObject.put(columnName, resultSetObject.getBoolean(columnName));
                        } else if ("INT".equalsIgnoreCase(columnType)) {
                            task_id = resultSetObject.getInt("task_id");
                            projectid =resultSetObject.getInt("project_id");

                            assign_to =resultSetObject.getInt("assign_to");

                            assign_by =resultSetObject.getInt("assign_by");

                            jsonDataObject.put(columnName, resultSetObject.getInt(columnName));
                        } else if ("VARCHAR".equalsIgnoreCase(columnType) || "BLOB".equalsIgnoreCase(columnType)) {
                            String columnValue = resultSetObject.getString(columnName);
                            if (columnName.equalsIgnoreCase("due_date")) {
                                if (!columnValue.equals("")) {
                                    due_date = columnValue;
                                }
                            }
                            if (columnName.equalsIgnoreCase("time_due")) {
                                if (!columnValue.equals("")) {
                                    time = columnValue;
                                }
                            }
                            String due_date_time = due_date + " " + time;
                            String time_left = dateUtil.getDatetimeDifference(due_date_time);

                            if (time_left.equalsIgnoreCase("OverDue")) {

                                if(name.equals("") || name.length()==0) {
                                    String constraint = "where task_id = " + task_id + " and task_status is null";
                                    Map<String, String> updateQueryParameters = new HashMap<>();
                                    updateQueryParameters.put("table", "Tasks");
                                    updateQueryParameters.put("key_value_pair", "task_status = 'OverDue' ");
                                    updateQueryParameters.put("constraint", constraint);

                                    String updatePacketQuery = buildQuery(updateQueryParameters, "UPDATE");
                                    int records = getUpdatedRows(updatePacketQuery);
                                }
                            }

                            jsonDataObject.put("time_left", time_left);

                            jsonDataObject.put(columnName, columnValue);
                        }

                        jsonDataObject.put("project_name",new DatabaseUtil().getTablename("Projects","project_name","project_id ="+projectid));

                        jsonDataObject.put("assign_to",new DatabaseUtil().getTablename("Employee","first_name,last_name","employee_id ="+assign_to));

                        String userchk =  new DatabaseUtil().getTablename("Employee","user_id","employee_id ="+assign_to);

                        int useridchk = 0;
                        if(userchk.equals("")){
                            useridchk = 0;
                        }else{
                            useridchk = Integer.parseInt(userchk);
                        }
                        if(useridchk == assign_by) {
                            jsonDataObject.put("assign_by", "Me");
                        }else {
                            jsonDataObject.put("assign_by", new DatabaseUtil().getUsername("Login", "first_name,last_name", "id =" + assign_by));
                        }

                    }
                }


                getSuccessObject(jsonDataObject, context.response(), "GET");
            }

            connectionObject.close();

        } catch (Exception e) {

            e.printStackTrace();

            String description = e.getMessage();

            getErrorObject("Bad Request",description,context.response(),"GET");

        }
    }



    // task projection creation

    public void addtaskprojectcontroller(RoutingContext context){
        ProjectModel projectModel = new ProjectModel();
        MultiMap params = context.request().params();
        try {
            for (String key : params.names()) {
                ModelUtil.setValues(key, params.get(key), projectModel);
            }

            Date dNow = new Date();
            SimpleDateFormat date =
                    dateUtil.getDate();
            projectModel.created_date =date.format(dNow);

            Map<String, Boolean> paramMapper = new HashMap<>();
            paramMapper.put("project_name",true);
            paramMapper.put("admin_id",true);
            paramMapper.put("organization_id",true);


            Map<String, Object> insertionObjects = generateInsertionObject(params, paramMapper, projectModel);

            List<String> queryStringValueList = DatabaseUtil.prepareQueryStringValues(insertionObjects);
            String columnValueString = queryStringValueList.get(0);
            String entryValueString = queryStringValueList.get(1);

            columnValueString+= ",created_date";
            entryValueString+=", '"+projectModel.getCreated_date()+"'";

            Map<String, String> insertQueryParameters = new HashMap<>();
            insertQueryParameters.put("table", "Projects");
            insertQueryParameters.put("column_names", columnValueString);
            insertQueryParameters.put("column_values", entryValueString);


            String countchk = new DatabaseUtil().getTablename("Projects","count(*)","project_name = '"+ projectModel.getProject_name()+"' and inactive_status = '0' and organization_id = '"+projectModel.organization_id+"'  ");

            int count = Integer.parseInt(countchk);
            if(count == 0) {
                String sqlQuery = buildQuery(insertQueryParameters, "INSERT");

                int id = insertRecord(sqlQuery);
                projectModel.project_id = id;

                JsonObject successObject = new JsonObject();
                successObject.put("id", projectModel.getProject_id());
                successObject.put("message", projectModel.getProject_name() + " created Successfully");
                getSuccessObject(successObject,context.response(),"POST");
            }else{
                throw new SQLException("Project Name already exists");
            }

        } catch (Exception e) {
            e.printStackTrace();
            String errorDescription = e.getMessage();
            getErrorObject("Project Name already exists",errorDescription,context.response(),"POST");
        }

    }

    public void updatetaskprojectcontroller(RoutingContext routingContext) {

        String projectId = routingContext.request().getParam("project_id");
        String OrgId = routingContext.request().getParam("organization_id");

        MultiMap params = routingContext.request().params();
        JsonObject jsonResponseObject = new JsonObject();
        String updateString = "";

        try {

            Connection connection = new MysqldbConf().getConnection();
            String countchk = "";
            String updateStatement = "";

            for (String key : params.names()) {
                String fieldType = ModelUtil.getFieldType(key, new ProjectModel());
                if ("boolean".equals(fieldType)) {
                    updateStatement += key + "=" + params.get(key) + ",";
                } else {
                    if(key.equals("project_name")){
                        countchk = new DatabaseUtil().getTablename("Projects","count(*)","project_name= '"+params.get(key) + "'  and organization_id ="+ OrgId);
                        int count = Integer.parseInt(countchk);
                        if(count== 0){
                            updateStatement += key + "='" + params.get(key) + "',";
                        }else{
                            throw new Exception("Project's already exists");
                        }
                    }else {
                        updateStatement += key + "='" + params.get(key) + "',";
                    }
                }
            }

           // String updateStatement = generateUpdateStatement(params, new ProjectModel());

            Date dNow = new Date();
            SimpleDateFormat date =
                    dateUtil.getDate();
            String modified_date = date.format(dNow);

            String constraintString ="";

            updateStatement+="modified_date = '"+modified_date+"' ";

           /* if(projectId.contains(",")){
                 constraintString = "where project_id in (" + projectId +") ";
            }else{*/
                 constraintString = "where project_id = " + projectId;
           /* }
*/
            Map<String, String> updateQueryParameters = new HashMap<>();
            updateQueryParameters.put("table", "Projects");
            updateQueryParameters.put("key_value_pair", updateStatement);
            updateQueryParameters.put("constraint", constraintString);


            updateString = buildQuery(updateQueryParameters, "UPDATE");

            int updatedRows = getUpdatedRows(updateString);
            if (updatedRows == 0) {
                throw new Exception("Invalid request to update Task");
            }

            connection.close();

            DatabaseUtil dbutil = new DatabaseUtil();
            String name = dbutil.getTablename("Projects","project_name","project_id = " + projectId);
            jsonResponseObject.put("message", " "+name+" updated Successfully! ");
            getSuccessObject(jsonResponseObject, routingContext.response(), "PUT");

        } catch (Exception exception) {
            exception.printStackTrace();
            getErrorObject("Invalid Data", exception.getMessage(), routingContext.response(), "PUT");
        }
    }


    public void deletetaskprojectcontroller(RoutingContext context) {

        MultiMap params = context.request().params();
        try {
            int project_id = Integer.valueOf(params.get("project_id"));
            int org_id = Integer.valueOf(params.get("organization_id"));

            String countchk = new DatabaseUtil().getTablename("Tasks","count(*)","project_id ="+project_id+" and organization_id ="+org_id);
            int count = Integer.parseInt(countchk);

            DatabaseUtil dbutil = new DatabaseUtil();
            String name = dbutil.getTablename("Projects", "project_name", "project_id = " + project_id);

            if(count == 0) {

                String constraintString = "where project_id = " + project_id;

                Map<String, String> updateQueryParameters = new HashMap<>();
                updateQueryParameters.put("table", "Projects");
                updateQueryParameters.put("key_value_pair", "inactive_status = 1");
                updateQueryParameters.put("constraint", constraintString);

                String deletePacketQuery = buildQuery(updateQueryParameters, "UPDATE");
                int deletedRecords = getUpdatedRows(deletePacketQuery);

                if (deletedRecords == 0) {
                    throw new Exception("Invalid Request to delete Task");
                }
                JsonObject dataObject = new JsonObject();
                dataObject.put("message", "'" + name + "' Projects Deleted");

                getSuccessObject(dataObject, context.response(), "DELETE");
            }else{
                throw new Exception( name + " Cannot delete is used already");
            }

        } catch (Exception deletePacketException) {
            deletePacketException.printStackTrace();
            getErrorObject("Deletion Error",deletePacketException.getMessage(),
                    context.response(),"DELETE");

        }

    }

    public void gettaskprojectcontroller(RoutingContext context) {

        ResultSet resultSetObject = null;

        try {
            Connection connectionObject = new MysqldbConf().getConnection();
            HttpServerRequest request = context.request();

            String id = request.getParam("organization_id");

            String projectIdString = request.getParam("project_id");

            String fieldString = request.getParam("fields");

            List<String> constraintList = new ArrayList<>();

            if (id != null) {
                constraintList.add("organization_id = " + id);
            }

            constraintList.add("inactive_status = 0");
            if (projectIdString != null) {
                constraintList.add("project_id = " + projectIdString);
            }

            String constraintString = DatabaseUtil.buildConstraintString(constraintList);

            Map<String, String> queryStringParameters = new HashMap<>();

            if(fieldString != null){
                fieldString+=",project_id";
                queryStringParameters.put("fields", fieldString);
            }else {
                queryStringParameters.put("fields", "*");
            }
            queryStringParameters.put("table", "Projects");
            queryStringParameters.put("constraint", constraintString);

            String queryString = buildQuery(queryStringParameters,"SELECT");

            resultSetObject = getResultSetObject(connectionObject,queryString);

            JsonArray TaskArray = new JsonArray();

            String columnName = "";
            String columnType = "";

            if(fieldString != null ){
                while (resultSetObject.next()) {
                    JsonObject TaskComponentObject = new JsonObject();
                    ResultSetMetaData resultSetMetaData = resultSetObject.getMetaData();
                    for (int i = 1; i <= resultSetObject.getMetaData().getColumnCount(); i++) {
                        columnName = resultSetMetaData.getColumnName(i);
                        columnType = resultSetMetaData.getColumnTypeName(i);

                        if ("BIT".equalsIgnoreCase(columnType)) {
                            TaskComponentObject.put(columnName, resultSetObject.getBoolean(columnName));
                        } else if ("INT".equalsIgnoreCase(columnType)) {
                            TaskComponentObject.put(columnName, resultSetObject.getInt(columnName));
                        } else if ("VARCHAR".equalsIgnoreCase(columnType) || "BLOB".equalsIgnoreCase(columnType)) {
                            String columnValue = resultSetObject.getString(columnName);
                            TaskComponentObject.put(columnName, columnValue);
                        }
                    }

                    TaskArray.add(TaskComponentObject);
                }

                getSuccessObject(TaskArray, context.response(), "GET");
            }else {

                JsonObject jsonDataObject = new JsonObject();

                while (resultSetObject.next()) {
                    ResultSetMetaData resultSetMetaData = resultSetObject.getMetaData();
                    for (int i = 1; i <= resultSetObject.getMetaData().getColumnCount(); i++) {
                        columnName = resultSetMetaData.getColumnName(i);
                        columnType = resultSetMetaData.getColumnTypeName(i);

                        if ("BIT".equalsIgnoreCase(columnType)) {
                            jsonDataObject.put(columnName, resultSetObject.getBoolean(columnName));
                        } else if ("INT".equalsIgnoreCase(columnType)) {
                            jsonDataObject.put(columnName, resultSetObject.getInt(columnName));
                        } else if ("VARCHAR".equalsIgnoreCase(columnType) || "BLOB".equalsIgnoreCase(columnType)) {
                            String columnValue = resultSetObject.getString(columnName);
                            jsonDataObject.put(columnName, columnValue);
                        }
                    }


                }


                getSuccessObject(jsonDataObject, context.response(), "GET");
            }

            connectionObject.close();

        } catch (Exception e) {

            e.printStackTrace();

            String description = e.getMessage();

            getErrorObject("Bad Request",description,context.response(),"GET");

        }
    }



    public void gettaskprojectlistcontroller(RoutingContext context) {

        ResultSet resultSetObject = null;

        try {
            Connection connectionObject = new MysqldbConf().getConnection();
            HttpServerRequest request = context.request();

            String id = request.getParam("organization_id");

            List<String> constraintList = new ArrayList<>();

            if (id != null) {
                constraintList.add("Projects.organization_id = " + id);

            }

            constraintList.add("inactive_status = 0");

            constraintList.add("admin_id = employee_id  group by project_name order by Projects.created_date desc");

            String constraintString = DatabaseUtil.buildConstraintString(constraintList);

            Map<String, String> queryStringParameters = new HashMap<>();

            queryStringParameters.put("fields", "project_id,project_name,admin_id,first_name,last_name");
            queryStringParameters.put("table", "Projects,Employee");
            queryStringParameters.put("constraint", constraintString);


            String queryString = buildQuery(queryStringParameters,"SELECT");

            /*String queryString = "SELECT project_name,admin_id,task_title,first_name from Projects,Tasks,Employee " +
                    " where project_id = Tasks.project_id and admin_id = employee_id and Projects.organization_id ="+id + " group by project_name";
*/
            resultSetObject = getResultSetObject(connectionObject, queryString);

            JsonArray TaskArray = new JsonArray();

            String columnName = "";
            String columnType = "";


            while (resultSetObject.next()) {
                JsonObject TaskComponentObject = new JsonObject();

                TaskComponentObject.put("project_name",resultSetObject.getString("project_name"));
                TaskComponentObject.put("project_id",resultSetObject.getInt("project_id"));
                TaskComponentObject.put("first_name",resultSetObject.getString("first_name"));
                TaskComponentObject.put("last_name",resultSetObject.getString("last_name"));
                TaskComponentObject.put("task",new DatabaseUtil().getJsonArrayOption(connectionObject,
                        "task_id,task_title","Tasks"," where project_id ="+resultSetObject.getInt("project_id") + " and organization_id =" +id));

                TaskComponentObject.put("count",new DatabaseUtil().getTablename("Tasks","count(*)","project_id ="+resultSetObject.getInt("project_id")));

                TaskArray.add(TaskComponentObject);
            }

            getSuccessObject(TaskArray, context.response(), "GET");

            connectionObject.close();

        } catch (Exception e) {

            e.printStackTrace();

            String description = e.getMessage();

            getErrorObject("Bad Request",description,context.response(),"GET");

        }
    }


    public void getfieldOption(RoutingContext context) {

        ResultSet resultSetObject = null;

        try {
            Connection connectionObject = new MysqldbConf().getConnection();
            HttpServerRequest request = context.request();

            String id = request.getParam("organization_id");
            List<String> constraintList = new ArrayList<>();

            if (id != null) {
                constraintList.add("organization_id = " + id);
            }

            constraintList.add("inactive_status = 0");

            String constraintString = DatabaseUtil.buildConstraintString(constraintList);

            Map<String, String> queryStringParameters = new HashMap<>();

            queryStringParameters.put("fields", "project_name,project_id");
            queryStringParameters.put("table", "Projects");
            queryStringParameters.put("constraint", constraintString);

            String queryString = buildQuery(queryStringParameters,"SELECT");

            resultSetObject = getResultSetObject(connectionObject,queryString);


            String columnName = "";
            String columnType = "";

            JsonArray fieldoption = new JsonArray();

            JsonObject FielddataObject = new JsonObject();

                while (resultSetObject.next()) {
                    JsonObject jsonDataObject = new JsonObject();
                    ResultSetMetaData resultSetMetaData = resultSetObject.getMetaData();
                    for (int i = 1; i <= resultSetObject.getMetaData().getColumnCount(); i++) {
                        columnName = resultSetMetaData.getColumnName(i);
                        columnType = resultSetMetaData.getColumnTypeName(i);

                        if ("BIT".equalsIgnoreCase(columnType)) {
                            jsonDataObject.put(columnName, resultSetObject.getBoolean(columnName));
                        } else if ("INT".equalsIgnoreCase(columnType)) {
                            jsonDataObject.put(columnName, resultSetObject.getInt(columnName));
                        } else if ("VARCHAR".equalsIgnoreCase(columnType) || "BLOB".equalsIgnoreCase(columnType)) {
                            String columnValue = resultSetObject.getString(columnName);
                            jsonDataObject.put(columnName, columnValue);
                        }
                    }

                fieldoption.add(jsonDataObject);
            }


            // priority and recurrence

            Map<String, String> queryStringParametersForPriority = new HashMap<>();

            String constraintStringForPriority = " where DropdownTasks.id = DropdownTasksName.dropdown_id and DropdownTasks.id= 1 and inactive_status = 0";

            queryStringParametersForPriority.put("fields", "dropdown_values,DropdownTasksName.id");
            queryStringParametersForPriority.put("table", "DropdownTasks,DropdownTasksName");
            queryStringParametersForPriority.put("constraint", constraintStringForPriority);

             queryString = buildQuery(queryStringParametersForPriority,"SELECT");

            resultSetObject = getResultSetObject(connectionObject,queryString);


            JsonArray priority = new JsonArray();

            while (resultSetObject.next()) {
                JsonObject jsonDataObject = new JsonObject();
                ResultSetMetaData resultSetMetaData = resultSetObject.getMetaData();
                for (int i = 1; i <= resultSetObject.getMetaData().getColumnCount(); i++) {
                    columnName = resultSetMetaData.getColumnName(i);
                    columnType = resultSetMetaData.getColumnTypeName(i);

                    if ("BIT".equalsIgnoreCase(columnType)) {
                        jsonDataObject.put(columnName, resultSetObject.getBoolean(columnName));
                    } else if ("INT".equalsIgnoreCase(columnType)) {
                        jsonDataObject.put(columnName, resultSetObject.getInt(columnName));
                    } else if ("VARCHAR".equalsIgnoreCase(columnType) || "BLOB".equalsIgnoreCase(columnType)) {
                        String columnValue = resultSetObject.getString(columnName);
                        jsonDataObject.put("name", columnValue);
                    }
                }

                priority.add(jsonDataObject);
            }


         /*
          sadhana commneted these as per instruction from vinoth ( create a DropdownTasks and handle it everything in that table itself)

          List<String> priorityArray = new ArrayList<>();
                    priorityArray.add("None");
                    priorityArray.add("High");
                    priorityArray.add("Medium");
                    priorityArray.add("Low");

                    for(int i=0;i< priorityArray.size();i++) {

                        JsonObject dataObject = new JsonObject();

                        dataObject.put("name", priorityArray.get(i).toString());
                        priority.add(dataObject);
                    }*/


            Map<String, String> queryStringParametersForRecurrence = new HashMap<>();

            String constraintStringForRecurrence = " where DropdownTasks.id = DropdownTasksName.dropdown_id and DropdownTasks.id= 2 and inactive_status = 0";

            queryStringParametersForRecurrence.put("fields", "dropdown_values,DropdownTasksName.id");
            queryStringParametersForRecurrence.put("table", "DropdownTasks,DropdownTasksName");
            queryStringParametersForRecurrence.put("constraint", constraintStringForRecurrence);

            queryString = buildQuery(queryStringParametersForRecurrence,"SELECT");

            resultSetObject = getResultSetObject(connectionObject,queryString);


            JsonArray recurrenceArray = new JsonArray();

            while (resultSetObject.next()) {
                JsonObject jsonDataObject = new JsonObject();
                ResultSetMetaData resultSetMetaData = resultSetObject.getMetaData();
                for (int i = 1; i <= resultSetObject.getMetaData().getColumnCount(); i++) {
                    columnName = resultSetMetaData.getColumnName(i);
                    columnType = resultSetMetaData.getColumnTypeName(i);

                    if ("BIT".equalsIgnoreCase(columnType)) {
                        jsonDataObject.put(columnName, resultSetObject.getBoolean(columnName));
                    } else if ("INT".equalsIgnoreCase(columnType)) {
                        jsonDataObject.put(columnName, resultSetObject.getInt(columnName));
                    } else if ("VARCHAR".equalsIgnoreCase(columnType) || "BLOB".equalsIgnoreCase(columnType)) {
                        String columnValue = resultSetObject.getString(columnName);
                        jsonDataObject.put("name", columnValue);
                    }
                }

                recurrenceArray.add(jsonDataObject);
            }



           /*

            List<String> recurrence = new ArrayList<>();
            recurrence.add("None");
            recurrence.add("Everyday");
            recurrence.add("Everyday-Weekdays");
            recurrence.add("Weekly");
            recurrence.add("Bi-Weekly");
            recurrence.add("Monthly");

            for(int i=0;i< recurrence.size();i++) {

                JsonObject dataObject1 = new JsonObject();

                dataObject1.put("name", recurrence.get(i).toString());

                recurrenceArray.add(dataObject1);

            }*/

            FielddataObject.put("Project",fieldoption);

            FielddataObject.put("Priority",priority);

            FielddataObject.put("Recurrence",recurrenceArray);

            getSuccessObject(FielddataObject, context.response(), "GET");
            connectionObject.close();

        } catch (Exception e) {

            e.printStackTrace();

            String description = e.getMessage();

            getErrorObject("Bad Request",description,context.response(),"GET");

        }
    }


}
