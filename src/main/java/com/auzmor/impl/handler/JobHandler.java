package com.auzmor.impl.handler;

import com.auzmor.bo.CandidateBO;
import com.auzmor.bo.JobBO;
import com.auzmor.bo.LoginBO;
import com.auzmor.dao.JobDAO;
import com.auzmor.impl.bo.CandidateBOImpl;
import com.auzmor.impl.bo.JobBOImpl;
import com.auzmor.impl.bo.LoginBOImpl;
import com.auzmor.impl.bo.RoleBOImpl;
import com.auzmor.impl.builder.ResponseBuilder;
import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.dao.EmployeeDAOImpl;
import com.auzmor.impl.dao.JobDAOImpl;
import com.auzmor.impl.dao.LoginDAOImpl;
import com.auzmor.impl.dao.OrganizationDAOImpl;
import com.auzmor.impl.enumarator.UserTypeEnum;
import com.auzmor.impl.enumarator.bo.JobBOEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.exception.util.ApplicationUtilException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.KongServiceHelper;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.Job.Collaborator;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.model.Login;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.Role;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.impl.util.DateUtil;
import com.auzmor.impl.util.Mail;
import com.auzmor.impl.util.ModelUtil;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.FieldOptions;
import com.auzmor.util.validator.Validator;
import com.mysql.cj.core.util.StringUtils;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.httpclient.HttpStatus;

import java.lang.reflect.Field;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

public class JobHandler {

  private static final Logger logger = LoggerFactory.getLogger(JobHandler.class);

  DateUtil dateUtil = new DateUtil();
  JobPostHandler postHandler = new JobPostHandler();
  JobUpdateHandler updateHandler = new JobUpdateHandler();
  JobDisplayHandler displayHandler = new JobDisplayHandler();
  private String consumerId;

  /**
   * addJob      This method acts as an entry point for posting a job
   *
   * @param routingContext the routing context
   */
  public void addJob(RoutingContext routingContext) {

    Job jobModel = new Job();
    MultiMap params = routingContext.request().params();
    Map<String, String> customQuestionMap = new HashMap<>();
    Map<String, String> collaboratorMap = new HashMap<>();

    Login login = (Login) routingContext.data().get("login");

    JsonObject jsonResponseObject = new JsonObject();
    JsonObject errorObject = new JsonObject();
    errorObject.put("code", HttpStatus.SC_NOT_ACCEPTABLE);
  
    String[] requiredParams = {"title", "location", "job_status", "job_description", "organization_id"};
    String[] allowedParams = {"title", "hiring_lead", "internal_job_code", "employment_type",
        "location", "city", "zip", "country", "state", "job_status", "department",
        "minimum_experience", "compensation", "job_description", "resume", "resume_required",
        "date_available", "date_available_required", "cover_letter", "cover_letter_required",
        "highest_education", "highest_education_required", "reference", "reference_required",
        "desired_salary", "desired_salary_required", "college", "college_required", "referred_by",
        "referred_by_required", "linkedin_url", "linkedin_url_required", "blog", "blog_required",
        "company_type_check", "company_type", "post_permission", "date_posted", "collab_resume",
        "collab_date_available", "collab_cover_letter", "collab_highest_education", "collab_reference",
        "collab_desired_salary", "collab_college", "collab_referred_by", "collab_linkedin_url",
        "collab_blog", "organization_id", "applicant", "screening", "interview", "offer", "hire",
        "job_desc_template", "not_hired", "currency_type", "custom_question", "custom_option1",
        "custom_option2", "custom_knockout", "custom_answer", "custom_question_required"};
    
    List<Validator> validators = new ArrayList<>();
    Validator jobValidator = new ValidatorImpl(allowedParams, jobFieldOptionsMap);
    validators.add(jobValidator);
    if (!ParamHelper.isParamsValid(routingContext.response(), params, allowedParams, requiredParams, validators)) {
      return;
    }

    try {

      boolean isCustomQuestion = false;
      boolean isCollaborator = false;
      boolean hasInvalidFields = false;

      DbHandle dbHandle = DbHelper.getDBHandle(routingContext, false);
      Employee employee = (login != null) ? new EmployeeDAOImpl(dbHandle).getEmployeeDetailsByLoginId(login.getId())
          : new Employee();
      params.set("job_creator_id", String.valueOf(employee.getEmployee_id()));
      dbHandle.checkAndCloseConnection();

      for (String key : params.names()) {
        isCollaborator = key.contains("collaborators");
        isCustomQuestion = key.contains("custom_");

        if (!isCustomQuestion && !isCollaborator) {

          ModelUtil.setValues(key, params.get(key), jobModel);

        } else {
          List<String> customQuestionParams = new ArrayList<>();
          customQuestionParams.add("custom_question");
          customQuestionParams.add("custom_answer");
          customQuestionParams.add("custom_option1");
          customQuestionParams.add("custom_option2");
          customQuestionParams.add("custom_knockout");
          customQuestionParams.add("custom_question_required");

          if (isCustomQuestion) {
            if (!customQuestionParams.contains(key)) {
              logger.error("Invalid Params (" + key + ") Entered for Job Post");
              throw new ApplicationUtilException("Invalid Params (" + key + ") Entered ");
            } else {
              customQuestionMap.put(key, params.get(key));
            }

          } else if (isCollaborator) {
            collaboratorMap.put(key, params.get(key));
            jobModel.setCollaborators(postHandler.getCollaborators(collaboratorMap));
          }
        }
      }

      if (StringUtil.printValidString(params.get("hiring_lead")).length() > 0) {
        dbHandle = DbHelper.getDBHandle(routingContext, false);
        boolean isHiringLeadValid = new JobBOImpl(dbHandle).isHiringLeadValid(
            Integer.parseInt(params.get("organization_id")), params.get("hiring_lead"));
        dbHandle.checkAndCloseConnection();
        if (!isHiringLeadValid) {
          ResponseBuilder.getErrorObject("Invalid Hiring Lead details Entered",
              "Invalid Hiring Lead details Entered",
              routingContext.response(), "POST");
          return;
        }

      }

      jobModel = postHandler.getHiringProcess(jobModel);
      if (customQuestionMap.size() > 0) {
        jobModel.setCustomQuestionModels(postHandler.getCustomQuestions(customQuestionMap));
      }

      Date jobPostDate = new Date();
      SimpleDateFormat jobPostDateFormat = dateUtil.getDate();
      String datePostedString = jobPostDateFormat.format(jobPostDate);

      jobModel.setDate_posted(datePostedString);

      if ("Published".equalsIgnoreCase(jobModel.getJob_status())) {
        jobModel.setJob_open_date(datePostedString);
      } else {
        jobModel.setJob_open_date("00/00/0000");
      }

      logger.info("Check the existence of Job Post");


      int jobId = 0;
      //if (!isJobPostAvailable) {
      if (!hasInvalidFields) {
        jobId = postHandler.addJobDetails(jobModel);

        jobModel.setId(jobId);

        JsonObject successObject = new JsonObject();
        successObject.put("id", jobModel.getId());
        String successMessage = "";

        if (!"Draft".equalsIgnoreCase(params.get("job_status"))) {
          successMessage = "Job " + params.get("job_status") + " successfully";
        } else {
          successMessage = "Job saved as Draft";
        }
        dbHandle = DbHelper.getDBHandle(routingContext, false);
        long adminid = 0;
        JobDAO jobDAO = new JobDAOImpl(dbHandle);
        Job job = jobDAO.getJobDetails(jobId);
        String title = job.getTitle();
        // adding this line to get the organization name in every mail
        String organizationName = ((Organization) routingContext.data().get("organization")).getOrganization_name();
        organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);
        LoginBO loginBO = new LoginBOImpl(dbHandle);
        long organization_id = job.getOrganization_id();
        List<HashMap<String, String>> adminList = loginBO.GetAdminList(organization_id);
        for (HashMap<String, String> map : adminList) {
          for (Map.Entry<String, String> mapEntry : map.entrySet()) {
            String key = mapEntry.getKey();
            String value = mapEntry.getValue();
            if (key.equals("id")) {
              adminid = Long.parseLong(value);
            }
            if (key.equals("email")) {
              mapEntry.getValue();
              String AdminEmail = value;
              String emailBody = "<html><body>Hi Admin,</br>" + jobModel.getTitle() + " " + successMessage + " <br/>" +
                  "Thank you,<br/>" + organizationName + " Team <br/>" +
                  "--------------------------------------------------------------\n<br/>" +
                  "This is system generated mail,please do not reply to this mail\n<br/>" +
                  "-------------------------------------------------------------- " +
                  "</body></html>";
              new Mail().sendMail(Vertx.vertx(),
                  emailBody,
                  AdminEmail, "Job Post-Admin");
            }
          }
        }
        dbHandle.checkAndCloseConnection();//by mounika
        String hiringLeadValue = StringUtil.printValidString(params.get("hiring_lead"));
        int hiringLead = StringUtil.isNumber(hiringLeadValue) ?
            Integer.parseInt(hiringLeadValue) : 0;
        if (hiringLead != 0) {
          dbHandle = DbHelper.getDBHandle(routingContext, false);
          employee = new EmployeeDAOImpl(dbHandle).getEmployeeDetails(hiringLead);
          int userID = employee.getUser_id();
          HttpClient kongClient = KongServiceHelper.executeWithKongClient(routingContext.vertx());
          String consumerId = "";
          Role role = null;
          String accessRole = "Hiring Lead";

          consumerId = new LoginDAOImpl(dbHandle).getConsumerId(userID);
          role = new RoleBOImpl(dbHandle).getByName(accessRole,
              Long.parseLong(params.get("organization_id")));
          KongServiceHelper.editConsumerRoles(kongClient, consumerId, "" + role.getId(),
              httpClientResponse -> {
                logger.info("Hiring Lead added successfully");
                kongClient.close();
              });
          JobBO jobBO = new JobBOImpl(dbHandle);
          jobBO.notifyNewHiringLeadViaEmail(hiringLead, jobId);

          dbHandle.checkAndCloseConnection();
        }
        successObject.put("message", successMessage);

        jsonResponseObject.put("code", HttpStatus.SC_CREATED);
        jsonResponseObject.put("success", true);
        jsonResponseObject.put("data", successObject);
        logger.info("Job Posted with job ID " + jobId);
        routingContext.response()
            .setStatusCode(HttpStatus.SC_CREATED)
            .putHeader("Content-Type", "application/json; charset=utf-8; id=" +
                jobModel.getId())
            .end(jsonResponseObject.toString());
      }
      /*} else {
        ResponseBuilder.getErrorObject("Job Post already exists",
            "Job Post already exists", routingContext.response(), "POST");
      }*/
    } catch (SQLException sqlException) {

      logger.error("SQL Exception in Post Job");
      ResponseBuilder.writeInternalServerError(routingContext.response());

    } catch (DbHandleException e) {

      logger.error("DbHandle Exception in Post Job");
      ResponseBuilder.writeInternalServerError(routingContext.response());

    } catch (ClassNotFoundException classNotFoundException) {

      logger.error("Class Not Found Exception in Post Job");
      ResponseBuilder.writeInternalServerError(routingContext.response());

    } catch (ApplicationUtilException appUtilException) {

      logger.error("Application Util Exception in Post Job");
      ResponseBuilder.writeInternalServerError(routingContext.response());

    } catch (NullPointerException nullPointerException) {

      errorObject.put("message", "Invalid Data");
      errorObject.put("description", nullPointerException.getMessage());

      jsonResponseObject.put("error", errorObject);

      logger.error("Null Pointer Exception in Post Job");
      routingContext.response()
          .setStatusCode(HttpStatus.SC_NOT_ACCEPTABLE)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());

    } catch (DecodeException decodeException) {
      errorObject.put("message", "Invalid Data");
      errorObject.put("description", decodeException.getMessage());

      jsonResponseObject.put("error", errorObject);

      logger.error("JSON Decode Exception in Post Job");

      routingContext.response()
          .setStatusCode(HttpStatus.SC_NOT_ACCEPTABLE)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
    }
  }

  /**
   * updateJobDetails Update Job Details
   *
   * @param routingContext
   * @author Pradeep Balasubramanian
   */
  public void updateJobDetails(RoutingContext routingContext) {

    String valueMapper = "";
    String creatorId = "";

    MultiMap params = (routingContext.request()).params();
    int id = Integer.valueOf(params.get("id"));
    UserTypeEnum userType = (UserTypeEnum) (routingContext.data().get("userType"));
    List<Role> roles = (List<Role>) routingContext.data().get("roles");
    List<String> rolesAssigned = new ArrayList<>();
    for (Role role : roles) {
      rolesAssigned.add(role.getName());
    }

    if (!rolesAssigned.contains("Admin") && !rolesAssigned.contains("Hiring Lead")) {
      DbHandle dbHandle = DbHelper.getDBHandle(routingContext, false);
      Employee employee = (Employee) routingContext.data().get("employee");
      JobBO jobBO = new JobBOImpl(dbHandle);
      try {
        Job jobDetails = jobBO.getJobDetails((long) id);
        if (Long.parseLong(jobDetails.getHiring_lead()) != employee.getUser_id()) {
          if (new Collaborator().isNotJobCollaborator(dbHandle, employee.getEmployee_id(), id)) {
            ResponseHelper.writeErrorResponse(HttpStatus.SC_FORBIDDEN, "cannot consume this service",
                "cannot consume this service", routingContext.response());
            return;
          }
        }
      } catch (SQLException e) {
        logger.error("SQL Exception in Job Update");
        ResponseBuilder.writeInternalServerError(routingContext.response());
        return;
      } catch (DbHandleException e) {
        logger.error("SQL Exception in Job Update");
        ResponseBuilder.writeInternalServerError(routingContext.response());
        return;
      } finally {
        dbHandle.checkAndCloseConnection();
      }
    }
    String jobStatus = params.get("job_status");
    boolean isValidJobStatusUpdate = true;
    if (StringUtil.printValidString(jobStatus).length() > 0) {
      DbHandle dbHandle = DbHelper.getDBHandle(routingContext, false);
      JobBO jobBO = new JobBOImpl(dbHandle);
      try {
        isValidJobStatusUpdate = jobBO.validateJobStatus(id, jobStatus);
        if (!isValidJobStatusUpdate) {
          logger.info("Job Status Validation is successful.");

          ResponseBuilder.getErrorObject("Invalid Job Status Update",
              "Invalid Job Status Update", routingContext.response(), "PUT");
          return;
        }
      } catch (SQLException e) {
        logger.error("SQL Exception in Job Update");
        ResponseBuilder.writeInternalServerError(routingContext.response());
      } catch (DbHandleException e) {
        logger.error("SQL Exception in Job Update");
        ResponseBuilder.writeInternalServerError(routingContext.response());
      } finally {
        dbHandle.checkAndCloseConnection();
      }
    }
  
    String[] requiredParams = {"id"};
    String[] allowedParams = {"id", "title", "hiring_lead", "internal_job_code", "employment_type",
        "location", "city", "zip", "country", "state", "job_status", "department",
        "minimum_experience", "compensation", "job_description", "resume", "resume_required",
        "date_available", "date_available_required", "cover_letter", "cover_letter_required",
        "highest_education", "highest_education_required", "reference", "reference_required",
        "desired_salary", "desired_salary_required", "college", "college_required", "referred_by",
        "referred_by_required", "linkedin_url", "linkedin_url_required", "blog", "blog_required",
        "company_type_check", "company_type", "post_permission", "date_posted", "collab_resume",
        "collab_date_available", "collab_cover_letter", "collab_highest_education", "collab_reference",
        "collab_desired_salary", "collab_college", "collab_referred_by", "collab_linkedin_url",
        "collab_blog", "organization_id", "applicant", "screening", "interview", "offer", "hire",
        "job_desc_template", "not_hired", "currency_type", "collaborators", "collab_email_update",
        "custom_question", "custom_option1", "custom_option2", "custom_knockout", "custom_answer",
        "custom_question_required", "creator_id"};
    List<Validator> validators = new ArrayList<>();
    Validator jobValidator = new ValidatorImpl(allowedParams, jobFieldOptionsMap);
    validators.add(jobValidator);
    
    if (!ParamHelper.isParamsValid(routingContext.response(), params, allowedParams,
                requiredParams, validators)) {
      return;
    }
    List<String> allowedParamList = Arrays.asList(allowedParams);

    boolean isHiringLeadChanged = false;
    if (params.names().contains("hiring_lead")) {
      if (StringUtil.isNumber(StringUtil.printValidString(params.get("hiring_lead")))) {
        String hiringLeadParam = StringUtil.printValidString(params.get("hiring_lead"));
        DbHandle dbHandle = DbHelper.getDBHandle(routingContext, false);
        JobBO jobBO = new JobBOImpl(dbHandle);
        try {
          Job jobItem = jobBO.getJobDetails(Long.valueOf(id));

          String existingHiringLead = (jobItem != null) ?
              StringUtil.printValidString(jobItem.getHiring_lead()) : "";
          if (!hiringLeadParam.equalsIgnoreCase(existingHiringLead)) {
            isHiringLeadChanged = true;
          }
          dbHandle.checkAndCloseConnection();
        } catch (SQLException e) {
          logger.error("SQL Exception in Job Update");
          ResponseBuilder.writeInternalServerError(routingContext.response());
        } catch (DbHandleException e) {
          logger.error("SQL Exception in Job Update");
          ResponseBuilder.writeInternalServerError(routingContext.response());
        }
      }
    }

    boolean isCustomQuestionChanged = false;
    boolean isCollaboratorChanged = false;
    Map<String, String> customQuestionMap = new HashMap<>();
    Map<String, String> collaboratorMap = new HashMap<>();
    if (params.names().contains("creator_id")) {
      creatorId = params.get("creator_id");
      params.remove("creator_id");
    }

    String collaboratorParam = "";
    if (params.names().contains("collaborators")) {
      isCollaboratorChanged = true;
      if (params.names().contains("collaborators")) {
        collaboratorParam = params.get("collaborators");
        collaboratorMap.put("collaborators", params.get("collaborators"));
        params.remove("collaborators");
      }
    }

    boolean containsInvalidParams = false;
    String fieldValuesForSelectQuery = "";
    int iterator = 0;
    for (String paramNames : params.names()) {
      if (!allowedParamList.contains(paramNames)) {
        containsInvalidParams = true;
        break;
      }
      if (!paramNames.contains("custom_")) {
        fieldValuesForSelectQuery += paramNames;
        if (iterator != params.names().size() - 1) {
          fieldValuesForSelectQuery += ",";
        }

      } else {
        isCustomQuestionChanged = true;
        customQuestionMap.put(paramNames, params.get(paramNames));
      }
      ++iterator;
    }

    if (containsInvalidParams) {
      logger.error("Invalid Params Entered");
      ResponseBuilder.getErrorObject("Invalid Params Entered",
          "Invalid Params Entered",
          routingContext.response(), "PUT");
      return;
    }

    Map<String, String> columnTypeMapper = new LinkedHashMap<>();
    String selectQuery = "select " + fieldValuesForSelectQuery + " from Job";

    DbHandle dbHandle = null;
    try {

      Connection connection = new MysqldbConf().getConnection();
      connection.setAutoCommit(false);
      Statement preparedStatement = null;
      preparedStatement = connection.createStatement();

      ResultSet resultSet = preparedStatement.executeQuery(selectQuery);
      ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

      for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
        columnTypeMapper.put(resultSetMetaData.getColumnName(i),
            resultSetMetaData.getColumnTypeName(i));
      }

      List<String> columnType = new ArrayList<>();
      List<String> columnValue = new ArrayList<>();

      for (String key : params.names()) {
        if (!key.contains("custom_")) {
          if ("BIT".equalsIgnoreCase(columnTypeMapper.get(key))) {
            valueMapper += key + "= ?,";
            columnType.add("boolean");
            columnValue.add(params.get(key));
          } else {
            String insertValue = params.get(key);
            if ("applicant".equals(key) || "interview".equals(key) ||
                "offer".equals(key) || "hire".equals(key)
                || "screening".equals(key)) {
              insertValue = StringUtil.convertJsonArrayToDBString(insertValue);
            }
            valueMapper += key + "= ?,";
            columnType.add("string");
            columnValue.add(insertValue);

          }
        }
      }

      String constraintString = " where id = " + id;
      SimpleDateFormat updateDateFormatter =
          dateUtil.getDate();
      valueMapper += "modified_date = ?";
      columnType.add("string");
      columnValue.add(updateDateFormatter.format(new Date()));
      if ("Published".equalsIgnoreCase(params.get("job_status"))) {
        valueMapper += ",job_open_date = ?";
        columnType.add("string");
        columnValue.add(updateDateFormatter.format(new Date()));
      }

      String queryString = "update Job set " + valueMapper + constraintString;

      PreparedStatement prepareUpdateStatement = connection.prepareStatement(queryString);

      for (int i = 0; i < columnType.size(); i++) {
        if ("boolean".equalsIgnoreCase(columnType.get(i))) {
          prepareUpdateStatement.setBoolean(i + 1, Boolean.valueOf(columnValue.get(i)));
        } else if ("string".equalsIgnoreCase(columnType.get(i))) {
          prepareUpdateStatement.setString(i + 1, columnValue.get(i));
        }
      }

      if (isCustomQuestionChanged) {
        updateHandler.updateCustomQuestions(id, connection, customQuestionMap);
      }
      if (isCollaboratorChanged) {

        dbHandle = DbHelper.getDBHandle(routingContext, false);
        try {
          JobBO jobBO = new JobBOImpl(dbHandle);
          List<String> newCollaborators = jobBO.updateCollaborators(id, collaboratorParam);
          int organizationId = 0;
          List<Integer> userIds = new ArrayList<>();
          for (String collaborator : newCollaborators) {
            Employee employee = new EmployeeDAOImpl(dbHandle).getEmployeeDetails(Long.parseLong(collaborator));
            userIds.add(employee.getUser_id());
            organizationId = employee.getOrganization_id();
          }
          if (newCollaborators.size() > 0) {
            HttpClient kongClient = KongServiceHelper.executeWithKongClient(routingContext.vertx());
            for (int userID : userIds) {
              Role role = null;
              String accessRole = "Collaborator";
              try {
                consumerId = new LoginDAOImpl(dbHandle).getConsumerId(userID);
                // new LoginBOImpl(dbHandle).updateConsumerId(consumerId, userID);
                role = new RoleBOImpl(dbHandle).getByName(accessRole,
                    (long) organizationId);
              } catch (SQLException e) {
                logger.error("SQL exception in Collaborator");
              } catch (DbHandleException e) {
                logger.error("Dbhandle exception in Collaborator");
              }

              KongServiceHelper.editConsumerRoles(kongClient, consumerId, "" + role.getId(),
                  httpClientResponse -> {
                    logger.info("Collaborator added successfully");
                    kongClient.close();
                  });
            }
          }
          dbHandle.commit();
          dbHandle.checkAndCloseConnection();
        } catch (SQLException | DbHandleException e) {
          dbHandle.rollback();
          dbHandle.checkAndCloseConnection();
        }

      }

      if (isHiringLeadChanged) {

        dbHandle = DbHelper.getDBHandle(routingContext, false);
        Employee employee = new EmployeeDAOImpl(dbHandle)
            .getEmployeeDetails(Long.valueOf(params.get("hiring_lead")));
        if (employee != null) {
          int userID = employee.getUser_id();
          HttpClient kongClient = KongServiceHelper.executeWithKongClient(routingContext.vertx());
          String consumerId = "";
          Role role = null;
          String accessRole = "Hiring Lead";

          consumerId = new LoginDAOImpl(dbHandle).getConsumerId(userID);
          role = new RoleBOImpl(dbHandle).getByName(accessRole, employee.getOrganization_id());
          KongServiceHelper.editConsumerRoles(kongClient, consumerId, "" + role.getId(),
              httpClientResponse -> {
                logger.info("Hiring Lead added successfully");
                kongClient.close();
              });

          new JobBOImpl(dbHandle).notifyNewHiringLeadViaEmail(
              Integer.valueOf(params.get("hiring_lead")), id);
          dbHandle.checkAndCloseConnection();
        }
      }

      prepareUpdateStatement.executeUpdate();

      JsonObject jsonSuccessObject = new JsonObject();
      jsonSuccessObject.put("code", HttpStatus.SC_ACCEPTED);
      jsonSuccessObject.put("success", true);
      String job_process = "";
      String successMessage = "Job Post Updated.";
      if (StringUtil.printValidString(jobStatus).length() > 0) {
        if (!"Draft".equalsIgnoreCase(StringUtil.printValidString(jobStatus))) {
          successMessage = "Job " + jobStatus + " successfully.";
          job_process = "Job " + jobStatus;
        } else {
          successMessage = "Job saved as Draft.";
          job_process = "Job saved as Draft.";

        }
      }

      if ("Closed".equalsIgnoreCase(StringUtil.printValidString(jobStatus))) {
        if (StringUtil.printValidString(creatorId).length() > 0) {
          dbHandle = DbHelper.getDBHandle(routingContext, false);
          JobBO jobBO = new JobBOImpl(dbHandle);
          String organizationName = ((Organization) routingContext.get("organization")).getOrganization_name();
          organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);
          jobBO.moveCandidatesToTalentPool(id, Integer.parseInt(creatorId), organizationName);
          dbHandle.commit();
          dbHandle.checkAndCloseConnection();

          successMessage += " Not Hired Candidates are moved to Talent Pool.";
          logger.info("Job (id=" + id + ") closed. Not Hired Candidates are moved to Talent Pool.");
          job_process = "Job Closed";
        }
      }

      dbHandle = DbHelper.getDBHandle(routingContext, false);
      JobDAO jobDAO = new JobDAOImpl(dbHandle);
      Job job = jobDAO.getJobDetails(id);
      String title = job.getTitle();
      // adding this line to get the organization name in every mail
      String organizationName = ((Organization) routingContext.data().get("organization")).getOrganization_name();
      organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);
      long adminid = 0;
      LoginBO loginBO = new LoginBOImpl(dbHandle);
      long organization_id = job.getOrganization_id();
      List<HashMap<String, String>> adminList = loginBO.GetAdminList(organization_id);
      dbHandle.checkAndCloseConnection();
      for (HashMap<String, String> map : adminList) {
        for (Map.Entry<String, String> mapEntry : map.entrySet()) {
          String key = mapEntry.getKey();
          String value = mapEntry.getValue();
          if (key.equals("id")) {
            adminid = Long.parseLong(value);
          }
          if (key.equals("email")) {
            mapEntry.getValue();
            String AdminEmail = value;
            String emailBody = "<html><body>Hi Admin,</br>" + title + " " + successMessage + " <br/>" +
                "Thank you,<br/>" + organizationName + " Team <br/>" +
                "--------------------------------------------------------------\n<br/>" +
                "This is system generated mail,please do not reply to this mail\n<br/>" +
                "-------------------------------------------------------------- " +
                "</body></html>";
            new Mail().sendMail(Vertx.vertx(),
                emailBody,
                AdminEmail, "Job Update Post-Admin");
          }
        }
      }
      jsonSuccessObject.put("data", new JsonObject().put("message", successMessage));

      connection.commit();
      connection.close();

      logger.info("Job (id=" + id + ") details updated ");
      routingContext.response()
          .setStatusCode(HttpStatus.SC_ACCEPTED)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonSuccessObject.toString());

    } catch (SQLException sqlException) {
      if (null != dbHandle) {
        dbHandle.checkAndCloseConnection();
      }
      logger.error("SQL Exception in Job Update");
      ResponseBuilder.writeInternalServerError(routingContext.response());

    } catch (DbHandleException e) {
      if (null != dbHandle) {
        dbHandle.checkAndCloseConnection();
      }
      logger.error("DbHandle Exception in Job Update");
      ResponseBuilder.writeInternalServerError(routingContext.response());

    } catch (ClassNotFoundException classNotFoundException) {
      if (null != dbHandle) {
        dbHandle.checkAndCloseConnection();
      }
      logger.error("Class Not Found Exception in Job Update");
      ResponseBuilder.writeInternalServerError(routingContext.response());

    } catch (DecodeException decodeException) {
      if (null != dbHandle) {
        dbHandle.checkAndCloseConnection();
      }
      logger.error("Decode Exception in Job Update");
      ResponseBuilder.getErrorObject("JSON Decode Exception", decodeException.getMessage(),
          routingContext.response(), "PUT");

    }
  }

  /**
   * deleteJobDetails - delete job details
   *
   * @param routingContext
   */
  public void deleteJobDetails(RoutingContext routingContext) {

    String id = routingContext.request().getParam("id");

    DbHandle dbHandle = null;
    try {
      int deletedRows = displayHandler.deleteJobPost(Integer.valueOf(id));
      if (deletedRows == 0) {
        logger.error("Invalid Job ID for delete job post ");
        throw new IllegalArgumentException("Invalid Job ID. No record with Job ID " + id + " found");
      }
      dbHandle = DbHelper.getDBHandle(routingContext, false);
      JobDAO jobDAO = new JobDAOImpl(dbHandle);
      Job job = jobDAO.getJobDetails(Integer.parseInt(id));
      String title = job.getTitle();
      // adding this line to get the organization name in every mail
      String organizationName = ((Organization) routingContext.data().get("organization")).getOrganization_name();
      organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);
      LoginBO loginBO = new LoginBOImpl(dbHandle);
      long organization_id = job.getOrganization_id();
      List<HashMap<String, String>> adminList = loginBO.GetAdminList(organization_id);
      for (HashMap<String, String> map : adminList) {
        for (Map.Entry<String, String> mapEntry : map.entrySet()) {
          String key = mapEntry.getKey();
          String value = mapEntry.getValue();
          if (key.equals("email")) {
            mapEntry.getValue();
            String AdminEmail = value;
            String emailBody = "<html><body>Hi Admin,</br>" + title + "  has been Closed  <br/>" +
                "Thank you,<br/>" + organizationName + " Team <br/>" +
                "--------------------------------------------------------------\n<br/>" +
                "This is system generated mail,please do not reply to this mail\n<br/>" +
                "-------------------------------------------------------------- " +
                "</body></html>";
            new Mail().sendMail(Vertx.vertx(),
                emailBody,
                AdminEmail, "Job Update Post-Admin");
          }
        }
      }
      JsonObject jsonDataObject = new JsonObject();
      jsonDataObject.put("message", "Job Post Deleted");
      dbHandle.checkAndCloseConnection();
      ResponseBuilder.getSuccessObject(jsonDataObject, routingContext.response(), "DELETE");

    } catch (SQLException sqlException) {

      if (null != dbHandle) {
        dbHandle.checkAndCloseConnection();
      }
      logger.error("SQL Exception in Delete Job");
      ResponseBuilder.writeInternalServerError(routingContext.response());

    } catch (ClassNotFoundException classNotFoundException) {

      if (null != dbHandle) {
        dbHandle.checkAndCloseConnection();
      }
      logger.error("Class Not Found Exception in Delete Job");
      ResponseBuilder.writeInternalServerError(routingContext.response());

    } catch (IllegalArgumentException e) {
      if (null != dbHandle) {
        dbHandle.checkAndCloseConnection();
      }
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (DbHandleException e) {
      if (null != dbHandle) {
        dbHandle.checkAndCloseConnection();
      }
      logger.error(e.getMessage());
      ResponseHelper.writeInternalServerError(routingContext.response());
    }
  }

  /**
   * getJobDetails - Get Job Details based on constraint
   *
   * @param routingContext
   * @author Pradeep Balasubramanian
   */
  public void getJobDetails(RoutingContext routingContext) {

    MultiMap mapObject = (routingContext.request()).params();

    String fieldString = mapObject.get("fields");
    String limit = mapObject.get("limit") == null ? "" : mapObject.get("limit");
    String offset = mapObject.get("offset") == null ? "" : mapObject.get("offset");
    String identityString = mapObject.get("id");
    String jobStatus = mapObject.get("job_status");
    String organizationId = mapObject.get("organization_id");
    String orderParameters = StringUtil.printValidString(mapObject.get("orderParams"));
    String candidateId = mapObject.get("candidate_id");
    String searchFilter = mapObject.get("searchFilter");

    int userId = 0;   // checking jobid is already applied or not while logging

    JsonObject jsonSuccessObject = new JsonObject();
    jsonSuccessObject.put("code", HttpStatus.SC_OK);
    jsonSuccessObject.put("success", true);
    DbHandle dbHandle = null;
    try {

      boolean isCollaborator = false;
      boolean isCustomQuestion = false;
      boolean requiresCandidateCount = false;
      String constraintString = " where organization_id = " +
          Integer.valueOf(organizationId) + " ";
      String limitFilterString = "";

      if (mapObject.get("user_id") != null) {
        userId = (int) ((Login) routingContext.data().get("login")).getId();
      }

      dbHandle = DbHelper.getDBHandle(routingContext, false);
      String value = "";
      if (jobStatus != null && jobStatus.length() > 0) {
        String jobStatusArray[] = jobStatus.split(",");
        if (jobStatusArray.length == 1) {
          value = "'" + jobStatus + "'";
        } else {
          for (int i = 0; i <= jobStatusArray.length - 1; i++) {
            value += "'" + jobStatusArray[i] + "'";
            if (i < jobStatusArray.length - 1) {
              value = value + ",";
            }

          }
        }
        constraintString += " and job_status IN( " + value + ")";
      }
      if (identityString != null && identityString.length() > 0) {
        constraintString = " where id = " + identityString + " and organization_id = " +
            Integer.valueOf(organizationId) + " ";
      }

      if ("".equals(limit) || "".equals(offset)) {
        if (limit.length() > 0) {
          limitFilterString += " LIMIT " + limit;
        }
        if (offset.length() > 0) {

          limitFilterString += " OFFSET " + offset;
        }

      } else {
        limitFilterString += " LIMIT " + limit + " OFFSET " + offset;
      }

      String queryString = "";

      List<String> fieldList = new LinkedList<>();

      if (fieldString != null && fieldString.length() > 0) {

        String[] fields = fieldString.split(",");

        for (String field : fields) {
          fieldList.add(field);
        }

        if (!fieldList.contains("id")) {
          fieldString = "id," + fieldString;
        }


        if (fieldList.contains("collaborators")) {
          fieldList.remove("collaborators");

          isCollaborator = true;

          fieldString = fieldString.replace("collaborators,", "");
          fieldString = fieldString.replace("collaborators", "");

          if (fieldString.endsWith(",")) {
            fieldString = fieldString.substring(0, fieldString.length() - 1);
          }
        }

        if (fieldList.contains("custom_question")) {
          fieldList.remove("custom_question");

          isCustomQuestion = true;

          fieldString = fieldString.replace("custom_question,", "");
          fieldString = fieldString.replace("custom_question", "");

          if (fieldString.endsWith(",")) {
            fieldString = fieldString.substring(0, fieldString.length() - 1);
          }
        }

        if (fieldList.contains("candidate_count")) {
          requiresCandidateCount = true;
          fieldList.remove("candidate_count");
          fieldString = fieldString.replace("candidate_count,", "");
          fieldString = fieldString.replace("candidate_count", "");

          if (fieldString.endsWith(",")) {
            fieldString = fieldString.substring(0, fieldString.length() - 1);
          }
        }
      }

      if (fieldString == null && identityString != null) {

        Field[] fields = Job.class.getDeclaredFields();

        StringBuilder fieldStringBuilder = new StringBuilder();
        for (int i = 0; i < fields.length - 2; i++) {
          fields[i].setAccessible(true);
          String fieldName = fields[i].getName();
          fieldStringBuilder.append(fieldName);
          if (i < fields.length - 3) {
            fieldStringBuilder.append(",");
          }
        }

        queryString = "SELECT " + fieldStringBuilder + " FROM Job " + constraintString;

        int jobID = Integer.parseInt(identityString);
        int jobCount = Integer.parseInt(new DatabaseUtil().
            getTablename("Job", "count(1)",
                "organization_id =" + organizationId));
        JsonObject jsonDataObject = new JsonObject();
        if (jobCount == 0 && jobID == 0) {
          jsonDataObject = getSampleData(organizationId);
          logger.info("Sample Data shown as Job Details");
        } else {
          jsonDataObject = displayHandler.getJobDetailsBasedOnId(routingContext, queryString);
        }

        Long organizationID = Long.valueOf(jsonDataObject.getInteger("organization_id"));
        Organization organization = new OrganizationDAOImpl(dbHandle).
            getOrganizationById(organizationID);
        logger.info("Organization Details fetched for showing HRIS URL");
        dbHandle.checkAndCloseConnection();

        if (organization != null) {
          String jobPostUrl = "https://" + organization.getHris_url() + "/careers/" +
              jsonDataObject.getInteger("id");
          jsonDataObject.put("job_url", jobPostUrl);
        }

        jsonSuccessObject.put("data", jsonDataObject);

        if (jsonDataObject.size() == 0) {
          jsonSuccessObject.put("no_data", true);
        } else {
          if (jobCount == 0) {
            jsonSuccessObject.put("no_data", true);
          }
          jsonSuccessObject.put("no_data", false);
        }

        routingContext.response()
            .setStatusCode(HttpStatus.SC_OK)
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .end(jsonSuccessObject.toString());

      } else if (candidateId != null && organizationId != null) {
        dbHandle = DbHelper.getDBHandle(routingContext, false);
        CandidateBO candidateBO = new CandidateBOImpl(dbHandle);
        long candidateID = Long.parseLong(candidateId);
        CandidateR candidate = candidateBO.getCandidate(dbHandle, candidateID);
        long candidateLoginId = candidate.getUserId();
        long organizationID = Long.parseLong(organizationId);

        JobBO jobBO = new JobBOImpl(dbHandle);
        List<HashMap<String, String>> resultSetArray = jobBO.
            getJobWithCandidateLoginId(candidateLoginId, organizationID);
        logger.info("Get Job Details for Candidate");
        JsonArray jobArray = new JsonArray();
        jobArray.add(resultSetArray);
        jsonSuccessObject.put("data", resultSetArray);
        if (resultSetArray.size() == 0) {
          jsonSuccessObject.put("no_data", true);
        } else {
          jsonSuccessObject.put("no_data", false);
        }
        dbHandle.checkAndCloseConnection();
        routingContext.response()
            .setStatusCode(HttpStatus.SC_OK)
            .putHeader("Content-Type", "application/json; charset=utf-8").end(jsonSuccessObject.toString());
      } else {
        String orderConstraintString = "";
        if (orderParameters.length() > 0) {
          orderConstraintString = " order by " + orderParameters + ",id desc";
        } else {
          orderConstraintString = " order by id desc ";
        }

        if (!StringUtils.isNullOrEmpty(searchFilter)) {
          constraintString += " and (UPPER(Job.title) like '%" + searchFilter + "%' " +
              "or UPPER(Job.location) like '%" + searchFilter + "%' )";
        }

        queryString = "SELECT " + fieldString + " FROM Job" + constraintString +
            orderConstraintString + limitFilterString;
        JsonArray availableJobStatusArray = new JsonArray();

        if (fieldString.contains("job_status")) {
          availableJobStatusArray = getAvailableJobStatus(constraintString);
          logger.info("Available Job Status");
        }

        JsonArray resultSetArray = displayHandler.getJobDetailsBasedOnConstraints(routingContext,
            queryString, fieldList, isCollaborator, isCustomQuestion, requiresCandidateCount,
            Integer.valueOf(organizationId), userId);

        String constraint = constraintString.replace("where", "");

        int count = Integer.parseInt(new DatabaseUtil().
            getTablename("Job", "count(1)", constraint));
        logger.info("Count of the number of jobs available in an Organization based on constraints");

        int data = Integer.parseInt(new DatabaseUtil().
            getTablename("Job", "count(1)",
                "organization_id =" + organizationId));
        logger.info("Count of the number of jobs available in an Organization");

        if (data == 0) {
          jsonSuccessObject.put("no_data", true);
          JsonObject jsonObject = getSampleData(organizationId);
          logger.info("Sample Data for newly Created Organization ");
          JsonObject filterObject = new JsonObject();
          for (String fields : fieldList) {
            filterObject.put(fields, jsonObject.getValue(fields));
          }
          if (mapObject.get("fields").contains("candidate_count")) {
            filterObject.put("candidate_count", jsonObject.getValue("candidate_count"));
          }
          resultSetArray.add(filterObject);
        } else {
          jsonSuccessObject.put("no_data", false);
        }

        jsonSuccessObject.put("data", resultSetArray);
        jsonSuccessObject.put("totalcount", count);

        if (fieldString.contains("job_status")) {
          jsonSuccessObject.put("available_job_status", availableJobStatusArray);
          if (jsonSuccessObject.getBoolean("no_data")) {
            jsonSuccessObject.put("available_job_status", new JsonArray().add("Published"));
          }
        }

        routingContext.response()
            .setStatusCode(HttpStatus.SC_OK)
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .end(jsonSuccessObject.toString());
      }

    } catch (SQLException sqlException) {
      logger.error("SQL Exception in Get Job Details");
      sqlException.printStackTrace();
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (DbHandleException e) {
      logger.error("DbHandle Exception in Get Job Details");
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (ClassNotFoundException classNotFoundException) {
      logger.error("Class Not Found Exception in Get Job Details");
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (IllegalArgumentException e) {
      logger.error("Illegal Argument Exception in Get Job details");
      if ("Invalid Job Display Request".equalsIgnoreCase(e.getMessage())) {
        ResponseBuilder.getErrorObject(e.getMessage(), "Bad Request",
            routingContext.response(), "GET");
      } else {
        ResponseBuilder.getErrorObject("Bad Request", e.getMessage(),
            routingContext.response(), "GET");
      }
    } catch (ParseException e) {
      logger.error("Date Format Exception in Get Job details");
      ResponseBuilder.getErrorObject("Bad Request", e.getMessage(),
          routingContext.response(), "GET");
    } finally {
      if (null != dbHandle) {
        dbHandle.checkAndCloseConnection();
      }
    }
  }

  /**
   * @param constraintString
   * @return
   * @throws SQLException
   * @author Pradeep balasubramanian
   */
  private JsonArray getAvailableJobStatus(String constraintString) throws SQLException {
    JobBO jobBO = new JobBOImpl();
    return jobBO.getAvailableJobStatus(constraintString);
  }

  /**
   * getJobBasedOnFilters  Get Job Details based on filters
   * contains {@link #selectAllDetailsBasedOnFilter(StringBuilder, StringBuilder, RoutingContext)}
   *
   * @param routingContext the routing context
   * @author sadhana
   */
  public void getJobBasedOnFilters(RoutingContext routingContext) {

    try {
      MultiMap params = routingContext.request().params();
      String locationValues = params.get("location");
      String jobTitleValues = params.get("title");
      String experienceValues = params.get("minimum_experience");
      String employmentTypeValues = params.get("employment_type");
      String department = params.get("department");

      String organizationId = params.get("organization_id");

      String limitValues = params.get("limit");
      String offsetValues = params.get("offset");

      StringBuilder selectFilter = new StringBuilder();
      StringBuilder limitFilter = new StringBuilder();

      if (limitValues != null && limitValues.length() > 0) {
        limitFilter.append(" LIMIT " + limitValues);
      }

      if (offsetValues != null && offsetValues.length() > 0) {
        if (limitFilter.length() == 0) {
          throw new IllegalArgumentException("Params for limit missing");
        }

        limitFilter.append(" OFFSET " + offsetValues);
      }

      if (locationValues != null && locationValues.length() > 0) {
        if (locationValues.contains(",")) {
          locationValues = locationValues.replace(",", " ', '");
        }
        selectFilter.append(" and location in ('" + locationValues + "')");
      }

      if (jobTitleValues != null && jobTitleValues.length() > 0) {
        if (jobTitleValues.contains(",")) {
          jobTitleValues = jobTitleValues.replace(",", "','");
        }
        selectFilter.append(" and title in ('" + jobTitleValues + "')");
      }

      if (employmentTypeValues != null && employmentTypeValues.length() > 0) {
        if (employmentTypeValues.contains(",")) {
          employmentTypeValues = employmentTypeValues.replace(",", "','");
        }
        selectFilter.append(" and employment_type in ('" + employmentTypeValues + "')");
      }

      if (experienceValues != null && experienceValues.length() > 0) {
        if (experienceValues.contains(",")) {
          experienceValues = experienceValues.replace(",", "','");
        }
        selectFilter.append(" and minimum_experience in ('" + experienceValues + "')");
      }

      if (department != null && department.length() > 0) {
        if (department.contains(",")) {
          department = department.replace(",", "','");
        }
        selectFilter.append(" and department in ('" + department + "')");
      }

      if (organizationId != null && organizationId.length() > 0) {
        selectFilter.append(" and organization_id = " + Integer.valueOf(organizationId) + " ");
      }

      int jobCount = Integer.parseInt(new DatabaseUtil().
          getTablename("Job", "count(1)",
              "organization_id =" + organizationId));

      if (jobCount == 0) {
        JsonObject jsonSuccessObject = new JsonObject();
        jsonSuccessObject.put("code", HttpStatus.SC_OK);
        jsonSuccessObject.put("success", true);
        JsonArray dataArray = new JsonArray();
        JsonObject dataObject = new JsonObject();
        dataObject.put("title", "Software Engineer");
        dataObject.put("id", 1);
        dataObject.put("employment_type", "");
        dataObject.put("location", "California");
        dataObject.put("hiring_lead", "1");
        dataObject.put("state", "CA");
        dataObject.put("city", "San Francisco");
        dataObject.put("department", "Information Technology");
        dataObject.put("minimum_experience", "7 years");
        dataArray.add(dataObject);

        jsonSuccessObject.put("data", dataArray);

        logger.info("Sample Data Added for New Organization");

        routingContext.response()
            .setStatusCode(HttpStatus.SC_OK)
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .end(jsonSuccessObject.toString());

      } else {
        selectAllDetailsBasedOnFilter(selectFilter, limitFilter, routingContext);
      }
    } catch (SQLException sqlException) {

      logger.error("SQL Exception in Job Filter");
      ResponseBuilder.writeInternalServerError(routingContext.response());

    } catch (ClassNotFoundException classNotFoundException) {

      logger.error("Class Not Found Exception in Job Filter");
      ResponseBuilder.writeInternalServerError(routingContext.response());

    } catch (IllegalArgumentException jobFilterException) {

      jobFilterException.printStackTrace();
      logger.error("Illegal Argument Exception in Job Filter");

      routingContext.response()
          .setStatusCode(HttpStatus.SC_BAD_REQUEST)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end("{\"status\": \"failure\",  \"message\":\"" + jobFilterException.getMessage() + "\"}");
    }
  }

  /**
   * selectAllDetailsBasedonFilter Called from getJobBasedOnFilters method
   *
   * @param selectFilter   contains the list of filter elements in a string format
   * @param limitFilter    represents the number of elements to be filtered and index from which
   *                       the elements need to be filtered
   * @param routingContext the routing context
   * @throws Exception
   * @see #getJobBasedOnFilters(RoutingContext)
   * A java method used for selecting the job details based on Filter
   */
  public void selectAllDetailsBasedOnFilter(StringBuilder selectFilter,
                                            StringBuilder limitFilter,
                                            RoutingContext routingContext)
      throws ClassNotFoundException, SQLException {

    JsonObject jsonSuccessObject = new JsonObject();
    jsonSuccessObject.put("code", HttpStatus.SC_OK);
    jsonSuccessObject.put("success", true);
    Connection connectionObject = new MysqldbConf().getConnection();
    PreparedStatement preparedStatement = null;
    ResultSet resultSetObject = null;
    ResultSet candidateResultSet = null;
    try {
      String queryString = "SELECT title,id,employment_type,location,hiring_lead,state,city," +
          " department,minimum_experience FROM Job " +
          " WHERE job_status ='Published' ";

      if (selectFilter.length() == 0) {
        queryString = queryString + limitFilter;
      } else {
        queryString = queryString + selectFilter + " " + limitFilter;
      }

      preparedStatement = connectionObject.prepareStatement(queryString);
      resultSetObject = preparedStatement.executeQuery();

      String columnName = "";
      String columnType = "";

      JsonArray resultSetArray = new JsonArray();
      while (resultSetObject.next()) {
        JsonObject jsonDataObject = new JsonObject();
        ResultSetMetaData resultSetMetaData = resultSetObject.getMetaData();
        for (int i = 1; i <= resultSetObject.getMetaData().getColumnCount(); i++) {
          columnName = resultSetMetaData.getColumnName(i);
          columnType = resultSetMetaData.getColumnTypeName(i);
          if ("BIT".equalsIgnoreCase(columnType)) {
            jsonDataObject.put(columnName, resultSetObject.getBoolean(columnName));
          } else if ("INT".equalsIgnoreCase(columnType)) {
            jsonDataObject.put(columnName, resultSetObject.getInt(columnName));
          } else if ("VARCHAR".equalsIgnoreCase(columnType) ||
              "BLOB".equalsIgnoreCase(columnType)) {
            String columnValue = resultSetObject.getString(columnName);
            jsonDataObject.put(columnName, columnValue);
          } else if ("DATE".equalsIgnoreCase(columnType)) {
            jsonDataObject.put(columnName, resultSetObject.getString(columnName));
          }
          //checking applied job for  a particular candidate
          logger.info("checking applied job for  a particular candidate");
          if (routingContext.data().get("login") != null) {
            int userId = (int) ((Login) routingContext.data().get("login")).getId();
            int organizationId = ((Organization) routingContext.data().get("organization")).getOrganization_id();
            if (userId != 0) {
              jsonDataObject.put("appiled_job", false);
              String candidateCountQueryString = "select job_id from Candidate where user_id = ? and job_id = ?" +
                  " and organization_id = ? and active_status = 1";
              preparedStatement = connectionObject.prepareStatement(candidateCountQueryString);
              preparedStatement.setInt(1, userId);
              preparedStatement.setInt(2, resultSetObject.getInt("id"));
              preparedStatement.setInt(3, organizationId);
              candidateResultSet = preparedStatement.executeQuery();
              while (candidateResultSet.next()) {
                jsonDataObject.put("appiled_job", true);
              }
            }
          }
        }
        resultSetArray.add(jsonDataObject);
      }
      if (null != candidateResultSet)
        candidateResultSet.close();
      preparedStatement.close();
      jsonSuccessObject.put("data", resultSetArray);

      logger.info("Data Displayed for Job Filter ");

      routingContext.response()
          .setStatusCode(HttpStatus.SC_OK)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonSuccessObject.toString());
    } finally {
      if (null != connectionObject)
        connectionObject.close();
    }
  }

  /**
   * archiveJobBasedOnId - Archive Job based on ID
   *
   * @paramcontext
   */
  public void archiveJobBasedOnId(RoutingContext routingContext) {

    String id = routingContext.request().getParam("id");
    Connection connectionObject = null;
    try {
      connectionObject = new MysqldbConf().getConnection();
      connectionObject.setAutoCommit(false);

      String[] archiveIds = (id.split(","));

      String queryString = "update Job set job_archive_status = ? where id = ?";

      PreparedStatement preparedStatement = connectionObject.prepareStatement(queryString);
      for (int i = 0; i < archiveIds.length; i++) {
        preparedStatement.setInt(1, 1);
        preparedStatement.setInt(2, Integer.valueOf(archiveIds[i]));
        preparedStatement.addBatch();
      }

      int[] updatedRows = preparedStatement.executeBatch();
      logger.info("Archive Jobs based on Job ID");

      connectionObject.commit();
      connectionObject.close();

      for (int rowsUpdated : updatedRows) {
        if (rowsUpdated == 0) {
          throw new IllegalArgumentException("Invalid Request to archive Job Post");
        }
      }

      JsonObject jsonSuccessObject = new JsonObject();
      jsonSuccessObject.put("message", "Job Post archived successfully");

      ResponseBuilder.getSuccessObject(jsonSuccessObject,
          routingContext.response(),
          "PUT");

    } catch (IllegalArgumentException illegalArgumentException) {

      logger.error("Illegal Argument Exception in Job Archive");
      ResponseBuilder.getErrorObject("Unable to Archive Job Post",
          illegalArgumentException.getMessage(), routingContext.response(), "PUT");
    } catch (SQLException sqlException) {
      try {
        connectionObject.rollback();
      } catch (SQLException rollbackException) {
        logger.error("SQL Exception in Job Archive - Rollback");
      }
      logger.error("SQL Exception in Job Archive");
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (ClassNotFoundException classNotFoundException) {
      logger.error("Class Not Found Exception in Job Archive");
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } finally {
      DbUtils.closeQuietly(connectionObject);
    }
  }

  /**
   * getCollaboratorList - get collaborator list based on job id
   *
   * @param routingContext
   */
  public void getCollaboratorList(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"id", "organization_id"};
    String[] requiredParams = {"id", "organization_id"};
    String[] titleParams = {"id", "organization_id"};

    List<Validator> validators = new ArrayList<>();
    Map<String, FieldOptions> fieldOptionsMap = new HashMap<String, FieldOptions>() {{
      put("organization_id", FieldOptionsCommon.NUMBER);
      put("id", FieldOptionsCommon.NUMBER);
    }};
    Validator titleValidator = new ValidatorImpl(titleParams, fieldOptionsMap);
    validators.add(titleValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          UserTypeEnum userType = (UserTypeEnum) (routingContext.data().get("userType"));

          Job job = null;
          JobBO jobBO = new JobBOImpl(dbHandle);
          if (userType != UserTypeEnum.ADMIN) {
            Employee employee = (Employee) routingContext.data().get("employee");
            try {
              long jobId = Long.parseLong(params.get("job_id"));
              job = jobBO.getJobDetails((long) jobId);
              if (Long.parseLong(job.getHiring_lead()) != employee.getEmployee_id()) {
                ResponseHelper.writeErrorResponse(org.apache.commons.httpclient.HttpStatus.SC_FORBIDDEN, "cannot consume this service",
                    "cannot consume this service", routingContext.response());
                return;
              }
            } catch (SQLException e) {
              logger.error("SQL Exception in Job Update");
              ResponseBuilder.writeInternalServerError(routingContext.response());
              return;
            } catch (DbHandleException e) {
              logger.error("SQL Exception in Job Update");
              ResponseBuilder.writeInternalServerError(routingContext.response());
              return;
            }
          }
          if (null == job) {
            job = new Job();
            job.setId(Integer.valueOf(requestParams.get("id")));
            job.setOrganization_id(Integer.valueOf(requestParams.get("organization_id")));
          }
          ReturnObject<JobBOEnum, JsonObject> boResponse = jobBO.getCollaboratorList(job);
          logger.info("Collaborators available for a job is displayed ");
          switch (boResponse.getStatusEnum()) {
            case COLLOBARATOR_LIST_RETREIVED:
              dbHandle.commit();
              int collaboratorsCount = boResponse.getData().getInteger("collaborators_count");
              boolean isNoDataAvailable = (collaboratorsCount > 0) ? false : true;
              ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
                  "", boResponse.getData(), isNoDataAvailable, httpServerResponse);
              break;
            case INVALID_JOB_ID:
              dbHandle.rollback();
              ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_BAD_REQUEST,
                  "Invalid Job ID", "Invalid Job ID",
                  httpServerResponse);
              break;
            default:
              dbHandle.rollback();
              ResponseHelper.writeInternalServerError(httpServerResponse);
              break;
          }
        });
  }

  private JsonObject getSampleData(String organizationId) {
    JsonObject jsonObject = new JsonObject();
    jsonObject.put("id", 0);
    jsonObject.put("title", "Software Engineer");
    jsonObject.put("candidate_count", 1);
    JsonObject hiringleadObject = new JsonObject();
    hiringleadObject.put("employee_id", 1);
    hiringleadObject.put("first_name", "Paul");
    hiringleadObject.put("last_name", "Wilson");
    hiringleadObject.put("profile_image",
        "static/img/null/83fcd877-cbfe-40d9-ab2c-6c83fe426cc7.jpeg");
    jsonObject.put("hiring_lead", hiringleadObject);
    jsonObject.put("internal_job_code", "");
    jsonObject.put("employment_type", "");
    jsonObject.put("location", "California");
    jsonObject.put("city", "San Francisco");
    jsonObject.put("zip", "");
    jsonObject.put("country", "United States");
    jsonObject.put("state", "CA");
    jsonObject.put("job_status", "Published");
    jsonObject.put("department", "Information Technology");
    jsonObject.put("minimum_experience", "7 years");
    jsonObject.put("compensation", "");
    jsonObject.put("job_desc_template", "Software Engineer");
    jsonObject.put("job_open_date", "01-Jan-2018");
    jsonObject.put("job_description", "<h4>Job brief</h4><p>We are looking for an Android Developer who \" +\n" +
        "        \"possesses a passion for pushing mobile technologies to the limits.</p> <p>This Android \" +\n" +
        "        \"app developer will work with our team of talented engineers to design and build the next \" +\n" +
        "        \"generation of our mobile applications.</p><p>Android programming works closely with other \" +\n" +
        "        \"app development and technical teams.</p><h4>Responsibilities</h4><ul><li>Design and build \" +\n" +
        "        \"advanced applications for the Android platform.</li><li>Collaborate with cross-functional \" +\n" +
        "        \" to define, design, and ship new features.</li><li>Work with outside data sources and APIs.\" +\n" +
        "        \"Unit-test code for robustness, including edge cases, usability, and general reliability.\" +\n" +
        "        \"</li><li>Work on bug fixing and improving application performance.</li><li>Continuously \" +\n" +
        "        \"discover, evaluate, and implement new technologies to maximize development efficiency.\" +\n" +
        "        \"</li></ul><h4>Requirements</h4><p>BS/MS degree in Computer Science, Engineering or a \" +\n" +
        "        \"related subject.</p><p>Proven software development experience and Android skills \" +\n" +
        "        \"development.</p><p>Proven working experience in Android app development.</p><p>Have \" +\n" +
        "        \"published at least one original Android app.</p><p>Experience with Android SDK.</p><p>\" +\n" +
        "        \"Experience working with remote data via REST and JSON.</p><p>Experience with third-party \" +\n" +
        "        \"libraries and APIs.Working knowledge of the general mobile landscape, architectures, \" +\n" +
        "        \"trends, and emerging technologies.Solid understanding of the full mobile development \" +\n" +
        "        \"life cycle.</p>");
    jsonObject.put("resume", true);
    jsonObject.put("resume_required", true);
    jsonObject.put("date_available", false);
    jsonObject.put("date_available_required", false);
    jsonObject.put("cover_letter", false);
    jsonObject.put("cover_letter_required", false);
    jsonObject.put("highest_education", false);
    jsonObject.put("highest_education_required", false);
    jsonObject.put("reference", false);
    jsonObject.put("reference_required", false);
    jsonObject.put("desired_salary", false);
    jsonObject.put("desired_salary_required", false);
    jsonObject.put("college", false);
    jsonObject.put("college_required", false);
    jsonObject.put("referred_by", false);
    jsonObject.put("referred_by_required", false);
    jsonObject.put("linkedin_url", false);
    jsonObject.put("linkedin_url_required", false);
    jsonObject.put("blog", false);
    jsonObject.put("blog_required", false);
    jsonObject.put("company_type_check", true);
    jsonObject.put("company_type", "");
    jsonObject.put("post_permission", true);
    jsonObject.put("modified_date", "01-Jan-2018");
    jsonObject.put("date_posted", "01-Jan-2018");
    jsonObject.put("organization_id", Integer.parseInt(organizationId));
    jsonObject.put("collab_resume", false);
    jsonObject.put("collab_date_available", false);
    jsonObject.put("collab_cover_letter", false);
    jsonObject.put("collab_resume", false);
    jsonObject.put("collab_date_available", false);
    jsonObject.put("collab_cover_letter", false);
    jsonObject.put("collab_highest_education", false);
    jsonObject.put("collab_reference", false);
    jsonObject.put("collab_desired_salary", false);
    jsonObject.put("collab_college", false);
    jsonObject.put("collab_referred_by", false);
    jsonObject.put("collab_linkedin_url", false);
    jsonObject.put("collab_blog", false);
    jsonObject.put("applicant", new JsonArray().add(new JsonObject().put("applicant", "New")));
    jsonObject.put("applicantstatus", new JsonArray());
    jsonObject.put("applicantCount", 0);
    jsonObject.put("screening", new JsonArray().add(new JsonObject().put("screening", "Screening")));
    jsonObject.put("screeningstatus", new JsonArray());
    jsonObject.put("screeningCount", 0);
    jsonObject.put("interview", new JsonArray().add(new JsonObject()
        .put("interview", "Schedule Interview")));
    jsonObject.put("interviewstatus", new JsonArray());
    jsonObject.put("interviewCount", 0);
    jsonObject.put("offer", new JsonArray().add(new JsonObject().put("offer", "Offer Extended")));
    jsonObject.put("offerstatus", new JsonArray());
    jsonObject.put("offerCount", 0);
    jsonObject.put("hire", new JsonArray().add(new JsonObject().put("hire", "Hire Candidate")));
    jsonObject.put("hirestatus", new JsonArray());
    jsonObject.put("hireCount", 0);
    JsonArray hireArray = new JsonArray();
    hireArray.add(new JsonObject().put("not_hired", "Archive to Talent Pool"));
    hireArray.add(new JsonObject().put("not_hired", "Declined offer"));
    hireArray.add(new JsonObject().put("not_hired", "Not Qualified / Knockout"));
    hireArray.add(new JsonObject().put("not_hired", "Over Qualified"));
    hireArray.add(new JsonObject().put("not_hired", "Took another position"));
    hireArray.add(new JsonObject().put("not_hired", "Blacklisted"));
    jsonObject.put("not_hired", hireArray);
    jsonObject.put("not_hiredstatus", new JsonArray());
    jsonObject.put("not_hiredCount", 0);
    jsonObject.put("job_active_status", true);
    JsonObject customQuestionObject = new JsonObject();
    customQuestionObject.put("custom_question_id", 1);
    customQuestionObject.put("custom_question", "Does the job provides overtime benefits ? ");
    customQuestionObject.put("custom_answer", "Yes");
    customQuestionObject.put("custom_knockout", false);
    customQuestionObject.put("custom_option1", "");
    customQuestionObject.put("custom_option2", "");
    customQuestionObject.put("custom_question_required", true);

    jsonObject.put("custom_question", new JsonArray().add(customQuestionObject));
    jsonObject.put("custom_questions", new JsonArray().add(customQuestionObject));
    jsonObject.put("collaborators", new JsonArray().add(
        new JsonObject().put("collaborator", 1).put("collab_email_update", false)
    ));

    return jsonObject;

  }

  public void getJobForEmployee(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"employeeId"};
    String[] requiredParams = {"employeeId"};
    String[] titleParams = {"employeeId"};

    List<Validator> validators = new ArrayList<>();
    Map<String, FieldOptions> fieldOptionsMap = new HashMap<String, FieldOptions>() {{
      put("employeeId", FieldOptionsCommon.NUMBER);
    }};
    Validator titleValidator = new ValidatorImpl(titleParams, fieldOptionsMap);
    validators.add(titleValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    String format = "dd-MMM-yyyy";

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          JobBO jobBO = new JobBOImpl(dbHandle);
          long employeeId = StringUtil.printValidString(requestParams.get("employeeId")) == "" ? 0 :
              Long.valueOf(requestParams.get("employeeId"));
          try {
            JsonArray resultArray = jobBO.getJobDetailsForEmployee(employeeId, format);
            if (resultArray.size() == 0) {
              ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
                  "no_data is true", resultArray, httpServerResponse);
            } else {
              ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
                  "no_data is false", resultArray, httpServerResponse);
            }
          } catch (ParseException e) {
            logger.error("Parse Exception occurs " + e.getMessage());
            ResponseHelper.writeInternalServerError(httpServerResponse);
          }
        });
  }

  /**
   * @param routingContext
   */
  public void cloneJob(RoutingContext routingContext) {

    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"id", "organizationId"};
    String[] requiredParams = {"id", "organizationId"};
    String[] locationParams = {"id", "organizationId"};

    Map<String, FieldOptions> cloneJobMap = new HashMap<String, FieldOptions>() {{
      put("organizationId", FieldOptionsCommon.NUMBER);
      put("id", FieldOptionsCommon.NUMBER);

    }};
    List<Validator> validators = new ArrayList<>();
    Validator cloneJobValidator = new ValidatorImpl(locationParams, cloneJobMap);
    validators.add(cloneJobValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          JobBO jobBO = new JobBOImpl();
          Job job = new Job();
          job.setOrganization_id(Integer.parseInt(params.get("organizationId")));
          job.setId(Integer.parseInt(params.get("id")));

          ReturnObject<JobBOEnum, JsonObject> boResponse = jobBO.cloneJob(dbHandle, job);
          switch (boResponse.getStatusEnum()) {

            case JOB_POST_SUCCESS:
              dbHandle.commit();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
                  "", boResponse.getData(), httpServerResponse);
              break;

            case JOB_POST_INVALID:
              dbHandle.rollback();
              ResponseHelper.writeErrorResponse(HttpStatus.SC_NOT_ACCEPTABLE,
                  "Job Clone Failed", "Job Clone Failed", httpServerResponse);
              break;

            case INVALID_JOB_ID:
              dbHandle.rollback();
              ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
                  "Invalid Job ID", "Invalid Job ID",
                  httpServerResponse);
              break;

            default:
              dbHandle.rollback();
              ResponseHelper.writeInternalServerError(httpServerResponse);
              break;
          }
        });

  }
  
  private static Map<String, FieldOptions> jobFieldOptionsMap = new HashMap<String, FieldOptions>() {{
        put("id", FieldOptionsCommon.NUMBER);
        put("title", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 100));
        put("location", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 100));
        put("job_status", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 20));
        put("job_description", FieldOptionsCommon.ANY);
        put("job_desc_template", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 150));
        put("internal_job_code", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 10));
        put("employment_type", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 50));
        put("city", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 50));
        put("state", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 100));
        put("zip", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 10));
        put("country", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 100));
        put("department", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 100));
        put("minimum_experience", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 100));
        put("compensation", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 50));
        put("hiring_lead", FieldOptionsCommon.NUMBER_ALLOW_EMPTY);
        put("company_type", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 100));
        put("organization_id", FieldOptionsCommon.NUMBER);
        put("applicant", FieldOptionsCommon.ANY);
        put("screening", FieldOptionsCommon.ANY);
        put("interview", FieldOptionsCommon.ANY);
        put("offer", FieldOptionsCommon.ANY);
        put("hire", FieldOptionsCommon.ANY);
        put("not_hired", FieldOptionsCommon.ANY);
        put("resume", FieldOptionsCommon.BOOLEAN);
        put("resume_required", FieldOptionsCommon.BOOLEAN);
        put("date_available", FieldOptionsCommon.BOOLEAN);
        put("date_available_required", FieldOptionsCommon.BOOLEAN);
        put("cover_letter", FieldOptionsCommon.BOOLEAN);
        put("cover_letter_required", FieldOptionsCommon.BOOLEAN);
        put("highest_education", FieldOptionsCommon.BOOLEAN);
        put("highest_education_required", FieldOptionsCommon.BOOLEAN);
        put("reference", FieldOptionsCommon.BOOLEAN);
        put("reference_required", FieldOptionsCommon.BOOLEAN);
        put("desired_salary", FieldOptionsCommon.BOOLEAN);
        put("desired_salary_required", FieldOptionsCommon.BOOLEAN);
        put("college", FieldOptionsCommon.BOOLEAN);
        put("college_required", FieldOptionsCommon.BOOLEAN);
        put("referred_by", FieldOptionsCommon.BOOLEAN);
        put("referred_by_required", FieldOptionsCommon.BOOLEAN);
        put("linkedin_url", FieldOptionsCommon.BOOLEAN);
        put("linkedin_url_required", FieldOptionsCommon.BOOLEAN);
        put("blog", FieldOptionsCommon.BOOLEAN);
        put("blog_required", FieldOptionsCommon.BOOLEAN);
        put("company_type_check", FieldOptionsCommon.BOOLEAN);
        put("post_permission", FieldOptionsCommon.BOOLEAN);
        put("collab_resume", FieldOptionsCommon.BOOLEAN);
        put("collab_date_available", FieldOptionsCommon.BOOLEAN);
        put("collab_cover_letter", FieldOptionsCommon.BOOLEAN);
        put("collab_highest_education", FieldOptionsCommon.BOOLEAN);
        put("collab_reference", FieldOptionsCommon.BOOLEAN);
        put("collab_desired_salary", FieldOptionsCommon.BOOLEAN);
        put("collab_college", FieldOptionsCommon.BOOLEAN);
        put("collab_referred_by", FieldOptionsCommon.BOOLEAN);
        put("collab_linkedin_url", FieldOptionsCommon.BOOLEAN);
        put("collab_blog", FieldOptionsCommon.BOOLEAN);
        put("custom_question", FieldOptionsCommon.ANY);
        put("custom_answer", FieldOptionsCommon.ANY);
        put("custom_option1", FieldOptionsCommon.ANY);
        put("custom_option2", FieldOptionsCommon.ANY);
        put("custom_knockout", FieldOptionsCommon.ANY);
        put("collaborators", FieldOptionsCommon.ANY);
        put("custom_question_required", FieldOptionsCommon.ANY);
        put("creator_id", FieldOptionsCommon.NUMBER_ALLOW_EMPTY);
        put("currency_type", new FieldOptionsImpl(FieldOptionsCommon.ALPHABETSWITHSPACE, -1, 3));
    
        }};
}

