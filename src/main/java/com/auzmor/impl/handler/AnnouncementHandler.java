package com.auzmor.impl.handler;

import com.auzmor.impl.model.AnnouncementModel;
import com.auzmor.impl.util.DBCRUDUtil;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.impl.util.ModelUtil;

import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class AnnouncementHandler {
    /**
     * Gets list of announcements
     * @author Pradeep Sudhakaran
     * @lastModifiedDate 09-11-2017
     * @param routingContext
     */
    public void addAnnouncement(RoutingContext routingContext){
        AnnouncementModel announcementModel = new AnnouncementModel();
        MultiMap params = routingContext.request().params();
        Map<String, Object> insertionObjects = new LinkedHashMap<>();
        JsonObject jsonResponseObject = new JsonObject();
        JsonObject errorObject = new JsonObject();
        errorObject.put("code", 406);
        DBCRUDUtil dbcrudUtil = new DBCRUDUtil();
        int announcement_id = 0;
        try{
            for(String key : params.names()) {
                try {
                        ModelUtil.setValues(key, params.get(key), announcementModel);
                } catch (Exception e) {
                    e.printStackTrace();
                    }
                }
                insertionObjects.put("announcement_title",announcementModel.getAnnouncement_title());
                insertionObjects.put("announcement_details",announcementModel.getAnnouncement_details());
                insertionObjects.put("start_date",announcementModel.getStart_date());
                insertionObjects.put("end_date",announcementModel.getEnd_date());
                insertionObjects.put("audience",announcementModel.getAudience());
                insertionObjects.put("specific_audience",announcementModel.getSpecific_audience());
                insertionObjects.put("organization_id",announcementModel.getOrganization_id());
                insertionObjects.put("active_status","true");

                announcement_id = dbcrudUtil.insertIntoTable(insertionObjects,"Announcement");

                if(announcement_id==0){
                    errorObject.put("message", "Bad Request");
                    errorObject.put("description", "Invalid Data");

                    jsonResponseObject.put("error", errorObject);

                    routingContext.response()
                            .setStatusCode(406)
                            .putHeader("Content-Type", "application/json; charset=utf-8")
                            .end(jsonResponseObject.toString());
                }

                JsonObject successObject = new JsonObject();
                successObject.put("announcement_id",announcement_id);
                successObject.put("announcement_title",announcementModel.getAnnouncement_title());
                successObject.put("announcement_details",announcementModel.getAnnouncement_details());
                successObject.put("start_date",announcementModel.getStart_date());
                successObject.put("end_date",announcementModel.getEnd_date());
                successObject.put("audience",announcementModel.getAudience());
                successObject.put("specific_audience",announcementModel.getSpecific_audience());
                successObject.put("organization_id",announcementModel.getOrganization_id());
                successObject.put("active_status","true");
                jsonResponseObject.put("code",201);
                jsonResponseObject.put("success",true);
                jsonResponseObject.put("data",successObject);

                routingContext.response()
                        .setStatusCode(202)
                        .putHeader("Content-Type", "application/json; charset=utf-8 id=" + announcement_id)
                        .end(jsonResponseObject.toString());

        }catch (Exception exception){
            exception.printStackTrace();
            errorObject.put("message","Invalid Data");
            errorObject.put("description",exception.getMessage());
            jsonResponseObject.put("error",errorObject);
            routingContext.response()

                    .setStatusCode(406)

                    .putHeader("Content-Type", "application/json; charset=utf-8")

                    .end(jsonResponseObject.toString());
        }
    }


    /**
     * Gets list of announcements
     * @author Pradeep Sudhakaran
     * @lastModifiedDate 09-11-2017
     * @param routingContext
     */
    public void getAllAnnouncements(RoutingContext routingContext){
        MultiMap mapObject = (routingContext.request()).params();
        DBCRUDUtil dbcrudUtil = new DBCRUDUtil();
        String fieldString = mapObject.get("fields");
        String limit = mapObject.get("limit")==null?"":mapObject.get("limit");
        String offset = mapObject.get("offset")==null?"":mapObject.get("offset");
        String id = mapObject.get("organization_id")==null?"":mapObject.get("organization_id");
        String whereString = "WHERE active_status=";
        String whereParams = "true";

        JsonObject jsonSuccessObject = new JsonObject();
        jsonSuccessObject.put("code", 200);
        jsonSuccessObject.put("success", true);

        try{
            fieldString = fieldString +",announcement_id";

            String constraintString = "";
            if ("" .equals(limit) || "" .equals(offset)) {
                if (limit.length() > 0) {
                    constraintString = " LIMIT " + limit;
                }
                if (offset.length() > 0) {
                    constraintString = " OFFSET " + offset;
                }

            } else {
                constraintString = " LIMIT " + limit + " OFFSET " + offset;
            }

            String[] fields = fieldString.split(",");

            List<String> fieldList = new LinkedList<>();
            for (String field : fields) {
                fieldList.add(field);
            }
            String queryString = "";
            queryString = "SELECT " + fieldString + " FROM  Announcement where organization_id="+id+" and active_status = 'true' order by announcement_id desc "+constraintString;
            System.out.println(queryString);

            JsonArray resultArray = dbcrudUtil.selectFieldsWithLimit(queryString,fieldList);

            jsonSuccessObject.put("data", resultArray);

            routingContext.response()
                    .setStatusCode(200)
                    .putHeader("Content-Type", "application/json; charset=utf-8")
                    .end(jsonSuccessObject.toString());

        } catch (Exception exception) {
            exception.printStackTrace();
            JsonObject jsonResponseObject = new JsonObject();

            JsonObject jsonFailureObject = new JsonObject();
            jsonFailureObject.put("code", 400);
            jsonFailureObject.put("message", "Wrong Field Name");
            jsonFailureObject.put("description", exception.getMessage());

            jsonResponseObject.put("error", jsonFailureObject);

            routingContext.response()
                    .setStatusCode(400)
                    .putHeader("Content-Type", "application/json; charset=utf-8")
                    .end(jsonResponseObject.toString());
        }
    }

    /**
     * Deletes particular announcement
     * @author Pradeep Sudhakaran
     * @lastModifiedDate 08-11-2017
     * @param routingContext
     */
    public void deleteAnnouncement(RoutingContext routingContext){
        String id = routingContext.request().getParam("id");

        DatabaseUtil databaseUtil = new DatabaseUtil();
        String setFields = "active_status = 'false'";
        String whereField = "announcement_id";

        JsonObject jsonSuccessObject = new JsonObject();
        JsonObject jsonResponseObject = new JsonObject();
        JsonObject jsonFailureObject = new JsonObject();
        DBCRUDUtil dbcrudUtil = new DBCRUDUtil();
        int deleteRows = 0;
        try {
            deleteRows = databaseUtil.deletePost("Announcement", setFields, whereField, Integer.valueOf(id));

            if (deleteRows == 0) {
                jsonFailureObject.put("code", 400);
                jsonFailureObject.put("message", "Deletion Error");
                jsonFailureObject.put("description", "Invalid Announcement ID. No record with Announcement ID " + id + " found");

                jsonResponseObject.put("error", jsonFailureObject);

                routingContext.response()
                        .setStatusCode(400)
                        .putHeader("Content-Type", "application/json; charset=utf-8")
                        .end(jsonResponseObject.toString());
            }

            String getTitleQuery = "select announcement_title from Announcement where announcement_id = "+id;

            String announcement_title = dbcrudUtil.returnValueForQuery(getTitleQuery);
            jsonSuccessObject.put("code", 200);
            jsonSuccessObject.put("success", true);
            jsonSuccessObject.put("message", announcement_title+" Deleted Successfully");

            routingContext.response()
                    .setStatusCode(200)
                    .putHeader("Content-Type", "application/json; charset=utf-8")
                    .end(jsonSuccessObject.toString());

        }catch (Exception exception){
            exception.printStackTrace();
            jsonFailureObject.put("code", 400);
            jsonFailureObject.put("message", "Deletion Error");
            jsonFailureObject.put("description", exception.getMessage());

            jsonResponseObject.put("error", jsonFailureObject);

            routingContext.response()
                    .setStatusCode(400)
                    .putHeader("Content-Type", "application/json; charset=utf-8")
                    .end(jsonResponseObject.toString());
        }
    }

    /**
     * Gets particular announcement details
     * @author Pradeep Sudhakaran
     * @lastModifiedDate 15-11-2017
     * @param routingContext
     */
    public void getAnnouncementId(RoutingContext routingContext){
        String id = routingContext.request().getParam("id");
        DatabaseUtil databaseUtil = new DatabaseUtil();
        JsonObject jsonSuccessObject = new JsonObject();
        JsonObject jsonResponseObject = new JsonObject();
        JsonObject jsonFailureObject = new JsonObject();
        try{
            String selectFields = "announcement_id,announcement_title,announcement_details,start_date,end_date,audience,specific_audience,organization_id,active_status";
            String whereFields = "announcement_id = "+id;
            JsonObject jsonObject = new JsonObject();

            jsonObject = databaseUtil.getSelectFields(selectFields,"Announcement",whereFields);

            jsonSuccessObject.put("code", 200);
            jsonSuccessObject.put("success", true);
            jsonSuccessObject.put("data", jsonObject);

            routingContext.response()
                    .setStatusCode(200)
                    .putHeader("Content-Type", "application/json; charset=utf-8")
                    .end(jsonSuccessObject.toString());


        }catch(Exception exception){
            exception.printStackTrace();
            jsonFailureObject.put("code", 400);
            jsonFailureObject.put("message", "Deletion Error");
            jsonFailureObject.put("description", exception.getMessage());

            jsonResponseObject.put("error", jsonFailureObject);

            routingContext.response()
                    .setStatusCode(400)
                    .putHeader("Content-Type", "application/json; charset=utf-8")
                    .end(jsonResponseObject.toString());
        }
    }

    /**
     * Gets particular announcement
     * @author Pradeep Sudhakaran
     * @lastModifiedDate 15-11-2017
     * @param routingContext
     */
    public void updateAnnouncement(RoutingContext routingContext){
        MultiMap params = routingContext.request().params();
        DatabaseUtil databaseUtil =  new DatabaseUtil();
        JsonObject jsonSuccessObject = new JsonObject();
        JsonObject jsonResponseObject = new JsonObject();
        JsonObject jsonFailureObject = new JsonObject();
        String valueMapper="";
        try{

            String id = routingContext.request().getParam("announcementId");
            params.remove("announcementId");
            params.set("announcement_id", id);
            for(String key:params.names()){
                if(!key.equals("announcement_id")) {
                    if (key.equals("organization_id"))
                        valueMapper += key + "=" + params.get(key) + ",";
                    else
                        valueMapper += key + "= '" + params.get(key) + "',";
                }
            }
            StringBuilder builder = new StringBuilder(valueMapper);
            builder.deleteCharAt(valueMapper.length()-1);
            valueMapper = builder.toString();

            int updatedRows = 0;

            updatedRows = databaseUtil.deletePost("Announcement",valueMapper,
                "announcement_id",Integer.valueOf(id));

            if(updatedRows == 0){
                jsonFailureObject.put("code", 400);
                jsonFailureObject.put("message", "Deletion Error");
                jsonFailureObject.put("description", "No such record found");

                jsonResponseObject.put("error", jsonFailureObject);

                routingContext.response()
                        .setStatusCode(400)
                        .putHeader("Content-Type", "application/json; charset=utf-8")
                        .end(jsonResponseObject.toString());
            }

            jsonSuccessObject.put("message","Announcement Updated Successfully");

            jsonResponseObject.put("code", 202);
            jsonResponseObject.put("success", true);
            jsonResponseObject.put("data", jsonSuccessObject);

            routingContext.response()
                    .setStatusCode(202)
                    .putHeader("Content-Type", "application/json; charset=utf-8")
                    .end(jsonResponseObject.toString());


        }catch (Exception exception){
            exception.printStackTrace();
            jsonFailureObject.put("code", 400);
            jsonFailureObject.put("message", "Deletion Error");
            jsonFailureObject.put("description", exception.getMessage());

            jsonResponseObject.put("error", jsonFailureObject);

            routingContext.response()
                    .setStatusCode(400)
                    .putHeader("Content-Type", "application/json; charset=utf-8")
                    .end(jsonResponseObject.toString());
        }

    }
}
