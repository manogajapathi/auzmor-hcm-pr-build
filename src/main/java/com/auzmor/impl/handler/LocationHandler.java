package com.auzmor.impl.handler;

import com.auzmor.bo.LocationBO;
import com.auzmor.impl.bo.LocationBOImpl;
import com.auzmor.impl.enumarator.LocationEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.Invite;
import com.auzmor.impl.model.LocationModel;
import com.auzmor.impl.util.db.DbHandleImpl;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.Validator;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LocationHandler {
  private static Logger logger = LoggerFactory.getLogger(LocationHandler.class);

  /**
   * Adding a new location in a organization
   * @param routingContext
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  public static void addLocation(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    LocationModel locationModel = new LocationModel();

    String[] allowedParams = {"location", "organization_id","company_address_line1",
        "company_address_line2","country","city","zip","state","active_status"};
    String[] requiredParams = {"location", "organization_id"};
    String[] locationParams = {"location", "organization_id","company_address_line1",
        "company_address_line2","country","city","zip","state","active_status"};

    List<Validator> validators = new ArrayList<>();
    Validator locationValidator = new ValidatorImpl(locationParams, LocationModel.getValidatorMap());
    validators.add(locationValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          LocationBO locationBO = new LocationBOImpl();
          locationModel.setLocation(requestParams.get("location"));
          locationModel.setOrganizationId(Integer.valueOf(requestParams.get("organization_id")));
          locationModel.setCompanyAddressLine1(requestParams.get("company_address_line1"));
          locationModel.setCompanyAddressLine2(requestParams.get("company_address_line2"));
          locationModel.setCity(requestParams.get("city"));
          locationModel.setState(requestParams.get("state"));
          locationModel.setCountry(requestParams.get("country"));
          locationModel.setZip(requestParams.get("zip"));

          ReturnObject<LocationEnum, JsonObject> boResponse = locationBO.addLocation(dbHandle,locationModel);
          switch (boResponse.getStatusEnum()) {
            case ADDITION_SUCCESS:
              dbHandle.commit();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  "", boResponse.getData(), httpServerResponse);
              break;
            case ADDITION_FAILED:
              dbHandle.rollback();
              ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
                  "Unable to find location", "Unable to find location",
                  httpServerResponse);
              break;
            case EXISTS_ALREADY:
              dbHandle.rollback();
              ResponseHelper.writeErrorResponse(HttpStatus.SC_CONFLICT,
                  params.get("location") + " already exists",
                  params.get("location") + " already exists",
                  httpServerResponse);
              break;
            default:
              dbHandle.rollback();
              ResponseHelper.writeInternalServerError(httpServerResponse);
              break;
          }
        });
  }

  /**
   * Get particular details of location
   * @param routingContext
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  public static void getLocation(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    LocationModel locationModel = new LocationModel();

    String[] allowedParams = {"id"};
    String[] requiredParams = {"id"};
    String[] locationParams = {"id"};

    List<Validator> validators = new ArrayList<>();
    Validator locationValidator = new ValidatorImpl(locationParams, LocationModel.getValidatorMap());
    validators.add(locationValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          locationModel.setId(Integer.valueOf(requestParams.get("id")));
          LocationBO locationBO = new LocationBOImpl();
          ReturnObject<LocationEnum, JsonObject> boResponse = locationBO.getLocation(dbHandle,locationModel);
          LocationHandler.writeResponse(requestParams,httpServerResponse,dbHandle,boResponse);
        });

  }

  /**
   * Update a particular location
   * @param routingContext
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  public static void updateLocation(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    LocationModel locationModel = new LocationModel();

    String[] allowedParams = {"id","location","company_address_line1","company_address_line2"
        ,"country","city","zip","state", "organization_id", "active_status"};
    String[] requiredParams = {"id"};
    String[] locationParams = {"id","location", "organization_id","company_address_line1",
        "company_address_line2","country","city","zip","state", "active_status"};

    List<Validator> validators = new ArrayList<>();
    Validator locationValidator = new ValidatorImpl(locationParams, LocationModel.getValidatorMap());
    validators.add(locationValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          LocationBO locationBO = new LocationBOImpl();
          ReturnObject<LocationEnum, JsonObject> boResponse = locationBO.updateLocation(dbHandle,requestParams);
          LocationHandler.writeResponse(requestParams,httpServerResponse,dbHandle,boResponse);
        });
  }

  /**
   * Delete a particular location
   * @param routingContext
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  public static void deleteLocation(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    LocationModel locationModel = new LocationModel();

    String[] allowedParams = {"id"};
    String[] requiredParams = {"id"};
    String[] locationParams = {"id"};

    List<Validator> validators = new ArrayList<>();
    Validator locationValidator = new ValidatorImpl(locationParams, LocationModel.getValidatorMap());
    validators.add(locationValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          LocationBO locationBO = new LocationBOImpl();
          locationModel.setId(Integer.valueOf(requestParams.get("id")));
          ReturnObject<LocationEnum, JsonObject> boResponse = locationBO.deleteLocation(dbHandle,locationModel);
          LocationHandler.writeResponse(requestParams,httpServerResponse,dbHandle,boResponse);
        });

  }

  /**
   * Get all locations in a particular organization
   * @param routingContext
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  public static void getAllLocations(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    LocationModel locationModel = new LocationModel();

    String[] allowedParams = {"organization_id"};
    String[] requiredParams = {"organization_id"};
    String[] locationParams = {"organization_id"};

    List<Validator> validators = new ArrayList<>();
    Validator locationValidator = new ValidatorImpl(locationParams, LocationModel.getValidatorMap());
    validators.add(locationValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }


    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          LocationBO locationBO = new LocationBOImpl();
          locationModel.setOrganizationId(Integer.valueOf(requestParams.get("organization_id")));
          ReturnObject<LocationEnum, JsonArray> boResponse = locationBO.getAllLocations(dbHandle,locationModel);
          switch (boResponse.getStatusEnum()) {
            case VALUE_SUCCESS:
              dbHandle.commit();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  "", boResponse.getData(), httpServerResponse);
              break;
            case NO_SUCH_VALUE:
              dbHandle.rollback();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  "no data is true", new JsonArray(),
                  httpServerResponse);
              break;
            default:
              dbHandle.rollback();
              ResponseHelper.writeInternalServerError(httpServerResponse);
              break;
          }
        });
  }

  /**
   * Writing correct response according to enum values
   * @param params
   * @param httpServerResponse
   * @param dbHandle
   * @param boResponse
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  public static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   ReturnObject<LocationEnum, JsonObject> boResponse)
      throws SQLException, DbHandleException {

    JsonObject dataObject = new JsonObject();
    dataObject.put("message", boResponse.getStatusEnum().getDescription());
    switch (boResponse.getStatusEnum()) {
      case EXISTS_ALREADY:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            params.get("location") + " already exists",
            params.get("location") + " already exists",
            httpServerResponse);
        break;
      case ADDITION_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Location added successfully", dataObject, httpServerResponse);
        break;
      case VALUE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "", boResponse.getData(), httpServerResponse);
        break;
      case ADDITION_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
      case UPDATE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Location updated successfully", dataObject, httpServerResponse);
        break;
      case NO_CHANGES_MADE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "No changes made", dataObject, httpServerResponse);
        break;
      case NO_SUCH_VALUE:
        dbHandle.rollback();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Unable to find location", dataObject,
            httpServerResponse);
        break;
      case DELETE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Location deleted successfully", dataObject, httpServerResponse);
        break;
      case CANNOT_DELETE:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Location cannot be deleted, Please reassign Employees",
            "Location cannot be deleted, Please reassign Employees",
            httpServerResponse);
        break;
      case RESTRICTED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Published Location cannot be changed",
            "Published Location cannot be changed",
            httpServerResponse);
        break;
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }
}