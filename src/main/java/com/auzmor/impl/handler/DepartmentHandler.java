package com.auzmor.impl.handler;

import com.auzmor.bo.DepartmentBO;
import com.auzmor.impl.bo.DepartmentBOImpl;
import com.auzmor.impl.enumarator.DepartmentEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.DepartmentModel;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.Validator;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DepartmentHandler {
  private static Logger logger = LoggerFactory.getLogger(DepartmentHandler.class);

  /**
   * Adding a new department in a organization
   * @param routingContext
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  public static void addDepartment(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    DepartmentModel departmentModel = new DepartmentModel();

    String[] allowedParams = {"department", "organization_id", "active_status"};
    String[] requiredParams = {"department", "organization_id"};
    String[] departmentParams = {"department", "organization_id", "active_status"};

    List<Validator> validators = new ArrayList<>();
    Validator departmentValidator = new ValidatorImpl(departmentParams, DepartmentModel.getValidatorMap());
    validators.add(departmentValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          DepartmentBO departmentBO = new DepartmentBOImpl(dbHandle);
          departmentModel.setDepartment(requestParams.get("department"));
          departmentModel.setOrganizationId(Integer.valueOf(requestParams.get("organization_id")));

          ReturnObject<DepartmentEnum, JsonObject> boResponse = 
              departmentBO.addDepartment(dbHandle,departmentModel);
          DepartmentHandler.writeResponse(requestParams,httpServerResponse,dbHandle,boResponse);
        });
  }

  /**
   * Get particular details of department
   * @param routingContext
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  public static void getDepartment(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    DepartmentModel departmentModel = new DepartmentModel();

    String[] allowedParams = {"id"};
    String[] requiredParams = {"id"};
    String[] departmentParams = {"id"};

    List<Validator> validators = new ArrayList<>();
    Validator departmentValidator = new ValidatorImpl(departmentParams, DepartmentModel.getValidatorMap());
    validators.add(departmentValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          departmentModel.setId(Integer.valueOf(requestParams.get("id")));
          DepartmentBO departmentBO = new DepartmentBOImpl(dbHandle);
          ReturnObject<DepartmentEnum, JsonObject> boResponse = 
              departmentBO.getDepartment(dbHandle, departmentModel);
          DepartmentHandler.writeResponse(requestParams,httpServerResponse,dbHandle,boResponse);
        });

  }

  /**
   * Update a particular department
   * @param routingContext
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  public static void updateDepartment(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    DepartmentModel departmentModel = new DepartmentModel();

    String[] allowedParams = {"id", "department", "organization_id", "active_status"};
    String[] requiredParams = {"id", "department"};
    String[] departmentParams = {"id", "department", "organization_id", "active_status"};

    List<Validator> validators = new ArrayList<>();
    Validator departmentValidator = new ValidatorImpl(departmentParams, DepartmentModel.getValidatorMap());
    validators.add(departmentValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          DepartmentBO departmentBO = new DepartmentBOImpl(dbHandle);
          ReturnObject<DepartmentEnum, JsonObject> boResponse = 
              departmentBO.updateDepartment(dbHandle,requestParams);
          DepartmentHandler.writeResponse(requestParams,httpServerResponse,dbHandle,boResponse);
        });

  }

  /**
   * Delete a particular department
   * @param routingContext
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  public static void deleteDepartment(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    DepartmentModel departmentModel = new DepartmentModel();

    String[] allowedParams = {"id"};
    String[] requiredParams = {"id"};
    String[] departmentParams = {"id"};

    List<Validator> validators = new ArrayList<>();
    Validator departmentValidator = new ValidatorImpl(departmentParams, DepartmentModel.getValidatorMap());
    validators.add(departmentValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          DepartmentBO departmentBO = new DepartmentBOImpl(dbHandle);
          departmentModel.setId(Integer.valueOf(requestParams.get("id")));
          ReturnObject<DepartmentEnum, JsonObject> boResponse = 
              departmentBO.deleteDepartment(dbHandle,departmentModel);
          DepartmentHandler.writeResponse(requestParams,httpServerResponse,dbHandle,boResponse);
        });

  }

  /**
   * Get all departments in a particular organization
   * @param routingContext
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  public static void getAllDepartments(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    DepartmentModel departmentModel = new DepartmentModel();

    String[] allowedParams = {"organization_id"};
    String[] requiredParams = {"organization_id"};
    String[] departmentParams = {"organization_id"};

    List<Validator> validators = new ArrayList<>();
    Validator departmentValidator = new ValidatorImpl(departmentParams, DepartmentModel.getValidatorMap());
    validators.add(departmentValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          DepartmentBO departmentBO = new DepartmentBOImpl(dbHandle);
          departmentModel.setOrganizationId(Integer.valueOf(requestParams.get("organization_id")));
          ReturnObject<DepartmentEnum, JsonArray> boResponse = 
              departmentBO.getAllDepartments(dbHandle,departmentModel);
          switch (boResponse.getStatusEnum()) {
            case VALUE_SUCCESS:
              dbHandle.commit();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  "", boResponse.getData(), httpServerResponse);
              break;
            case NO_SUCH_VALUE:
              dbHandle.commit();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  "no data is true", new JsonArray(),
                  httpServerResponse);
              break;
            default:
              dbHandle.rollback();
              ResponseHelper.writeInternalServerError(httpServerResponse);
              break;
          }
        });
  }

  /**
   * Writing correct response according to enum values
   * @param params
   * @param httpServerResponse
   * @param dbHandle
   * @param boResponse
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  public static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   ReturnObject<DepartmentEnum, JsonObject> boResponse)
      throws SQLException, DbHandleException {

    JsonObject dataObject = new JsonObject();
    dataObject.put("message", boResponse.getStatusEnum().getDescription());
    switch (boResponse.getStatusEnum()) {
      case EXISTS_ALREADY:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            params.get("department") + " already exists",
            params.get("department") + " already exists",
            httpServerResponse);
        break;
      case ADDITION_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Department added successfully", dataObject, httpServerResponse);
        break;
      case VALUE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Department added successfully", boResponse.getData(), httpServerResponse);
        break;
      case ADDITION_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
      case UPDATE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Department updated successfully", dataObject, httpServerResponse);
        break;
      case NO_CHANGES_MADE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "No changes made", dataObject, httpServerResponse);
        break;
      case NO_SUCH_VALUE:
        dbHandle.rollback();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Unable to find Department", dataObject,
            httpServerResponse);
        break;
      case DELETE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Department deleted successfully", dataObject, httpServerResponse);
        break;
      case CANNOT_DELETE:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Department cannot be deleted, Please reassign Employees",
            "Department cannot be deleted, Please reassign Employees",
            httpServerResponse);
        break;
      case RESTRICTED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Published Department Cannot be Changed",
            "Published Department Cannot be Changed",
            httpServerResponse);
        break;
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }
}
