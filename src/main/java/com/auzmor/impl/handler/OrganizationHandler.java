package com.auzmor.impl.handler;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auzmor.bo.OrganizationBO;
import com.auzmor.impl.bo.LoginBOImpl;
import com.auzmor.impl.bo.OrganizationBOImpl;
import com.auzmor.impl.builder.ApiBuilder;
import com.auzmor.impl.builder.ResponseBuilder;
import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.constant.file.endpoint.EndpointConstant;
import com.auzmor.impl.constant.route.PathConstants;
import com.auzmor.impl.dao.OrganizationDAOImpl;
import com.auzmor.impl.enumarator.bo.OrganizationBOEnum;
import com.auzmor.impl.enumarator.file.FileTypeEnum;
import com.auzmor.impl.enumarator.kong.KongHttpStatusCode;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.exception.util.ApplicationUtilException;
import com.auzmor.impl.helper.*;
import com.auzmor.impl.model.Api;
import com.auzmor.impl.model.Login;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.model.onboarding.OnboardingDocument;
import com.auzmor.impl.model.onboarding.OnboardingForm;
import com.auzmor.impl.util.JWTUtil;
import com.auzmor.impl.util.Mail;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.Validator;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

public class OrganizationHandler extends ApiBuilder {


  static final String alphaNumericSet = "0123456789" + "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
      + "abcdefghijklmnopqrstuvwxyz";
  private static final Logger logger = LoggerFactory.getLogger(OrganizationHandler.class);
  static SecureRandom secureRandom = new SecureRandom();
  public Map<Integer, Organization> organizationData = new LinkedHashMap<Integer, Organization>();
  DatabaseUtil dbUtil = new DatabaseUtil();

  /**
   * @param routingContext
   * @author Charles Sam Dilip
   * @lastModificationDate 21/MAR/2018
   */
  public static void checkAvailability(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    String[] allowedParams = {"domain"};
    String[] requiredParams = {"domain"};

    List<Validator> validators = new ArrayList<>();
    Validator inviteValidator = new ValidatorImpl(requiredParams, Organization.getValidatorMap());
    validators.add(inviteValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }
    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          OrganizationBO organizationBO = new OrganizationBOImpl(dbHandle);
          JsonObject jsonObject = new JsonObject();
          jsonObject.put("domainAvailable", organizationBO.isDomainAvailable(params.get("domain")));
          OrganizationHandler.writeResponse(params, response, dbHandle, jsonObject,
              OrganizationBOEnum.DOMAIN_AVAILABLE_CHECK);
        });
  }

  /**
   * @param routingContext
   * @author Charles Sam Dilip
   * @lastModificationDate 21/MAR/2018
   */
  public static void updateDomains(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    String[] allowedParams = {"auzmorDomain", "careerDomain", "auzmorDomainCrt", "careerDomainCrt",
        "auzmorDomainCrtKey", "careerDomainCrtKey", "organizationId"};
    String[] requiredParams = {"ats_domain", "organizationId"};


    List<Validator> validators = new ArrayList<>();
    Validator inviteValidator = new ValidatorImpl(allowedParams, Organization.getValidatorMap());
    validators.add(inviteValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }
    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          OrganizationBOEnum organizationBOEnum = new OrganizationBOImpl(dbHandle)
              .updateDomains(params.get("auzmorDomain"), params.get("careerDomain"),
                  Long.parseLong(params.get("organizationId")), params.get("auzmorDomainCrtKey"), params.get("auzmorDomainCrt"),
                  params.get("careerDomainCrtKey"), params.get("careerDomainCrt"));
          OrganizationHandler.writeResponse(params, response, dbHandle,
              null, organizationBOEnum);
        });
  }

  /**
   * @param routingContext
   * @author Charles Sam Dilip
   * @lastModifiedDate 08/MAR/2018
   */
  public static void getOrganizationUrlAndLogo(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    String[] allowedParams = {"id"};
    String[] requiredParams = {"id"};
    List<Validator> validators = new ArrayList<>();
    Validator organizationValidator = new ValidatorImpl(allowedParams, Organization.getValidatorMap());
    validators.add(organizationValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          Organization organization = new OrganizationBOImpl(dbHandle)
              .getOrganizationDetails(Long.parseLong(params.get("id")));
          JsonObject jsonObject = new JsonObject();
          jsonObject.put("url", organization.getHris_url());
          jsonObject.put("logo_url", organization.getLogo());
          OrganizationHandler.writeResponse(params, response, dbHandle,
              jsonObject, OrganizationBOEnum.SUCCESSFUL);
        });
  }

  public static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle, JsonObject jsonObject,
                                   OrganizationBOEnum organizationBOEnum)
      throws SQLException, DbHandleException {

    switch (organizationBOEnum) {
      // Success Response
      case DOMAIN_AVAILABLE_CHECK:
      case SUCCESSFUL:
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK, organizationBOEnum.getMessage(),
            jsonObject, httpServerResponse);
        break;
      case DOMAINS_UPDATE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED, organizationBOEnum.getMessage(),
            jsonObject, httpServerResponse);
        break;
      case DOMAIN_ALREADY_EXISTS:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_CONFLICT, organizationBOEnum.getMessage(),
            "Domain name already exists!!", httpServerResponse);
        break;

      // Error Response
      case DOMAIN_NOT_AVAILABLE:
      case ATS_DOMAIN_NOT_AVAILABLE:
      case CAREER_DOMAIN_NOT_AVAILABLE:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_CONFLICT, organizationBOEnum.getMessage(),
            organizationBOEnum.getDescription(), httpServerResponse);
        break;
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }

  /**
   * addOrganization - Creating new Organization
   *
   * @param routingContext
   * @method POST
   */

  public void addOrganization(RoutingContext routingContext) {

    int organizationId;
    MultiMap params = routingContext.request().params();
    JsonObject jsonResponseObject = new JsonObject();
    JsonObject errorObject = new JsonObject();
    errorObject.put("code", HttpStatus.SC_NOT_ACCEPTABLE);
    Organization organizationModel = new Organization();
    try {
      for (String key : params.names()) {
        if (key.equals("hris_url")) {
          Pattern pattern = Pattern.compile("[^\\.]+\\.[^\\.]+\\.[^\\.]{2,}");
          Matcher matcher = pattern.matcher(params.get(key));
          if (!matcher.matches()) {
            ResponseHelper.writeErrorResponse(400, "Not a valid domain name",
                "Domain name not satisfied the requiremnt", routingContext.response());
            return;
          }
        } else if (!key.equals("region_code")) {
          organizationModel.set(key, params.get(key));
        }
      }

      if (StringUtil.printValidString(organizationModel.getHris_url()).equals("")) {
        String url = organizationModel.getDomain_name().toLowerCase();
        String urlName = JWTUtil.getUrlPath();
        url = url.trim() + urlName.trim();
        organizationModel.setHris_url(url);
      } else if (!organizationModel.getHris_url().contains(".auzmor.com")) {
        throw new IllegalArgumentException("Organization's URL must end with .auzmor.com");
      }

      if (StringUtil.printValidString(params.get("region_code")).equals("")
          || StringUtil.printValidString(organizationModel.getPhone_no()).equals("")) {
        throw new NoSuchFieldException("Required params missing");
      }

      boolean isValidPhone = StringUtil.isValidPhoneNumber(organizationModel.getPhone_no(),
          params.get("region_code"));

      if (!isValidPhone) {
        JsonObject jsonFailureObject = new JsonObject();
        jsonFailureObject.put("code", HttpStatus.SC_BAD_REQUEST);
        jsonFailureObject.put("message", "Please provide proper phone number");
        jsonFailureObject.put("description", "Please provide proper phone number");
        jsonResponseObject.put("error", jsonFailureObject);
        routingContext.response()
            .setStatusCode(HttpStatus.SC_BAD_REQUEST)
            .putHeader("Content-Type", "application/json; charset=utf-8; id="
                + organizationModel.getOrganization_id())
            .end(jsonResponseObject.toString());
        return;
      }

      organizationModel.setRegion_code(params.get("region_code"));
      int id = -1;
      organizationId = this.addOrganizationDetails(organizationModel);
      organizationModel.setOrganization_id(organizationId);

      if (organizationId > 0) {
        String ownerEmail = organizationModel.getAccount_owner_email();
        String pwd = this.randomString(8);
        Login login = new Login();
        login.setFirstName(organizationModel.getFirst_name());
        login.setLastName(organizationModel.getLast_name());
        login.setEmail(ownerEmail.toLowerCase().trim());
        login.setUserType("admin");
        login.setPassword(pwd);
        login.setOrganizationId(organizationId);
        login.setKeyValue(UUID.randomUUID().toString());
        login.setHashValue(UUID.randomUUID().toString());
        DbHandle dbHandle = null;
        try {
          dbHandle = DbHelper.getDBHandle(routingContext, false);
          id = new LoginBOImpl(dbHandle).addNewUser(login);
          login.setId(id);
          dbHandle.commit();

        } catch (DbHandleException e) {
          DbHelper.safeRollback(dbHandle);
          logger.error("Unable to add user");
          ResponseHelper.writeInternalServerError(routingContext.response());
          return;
        } finally {
          if (null != dbHandle) {
            dbHandle.checkAndCloseConnection();
          }
        }
      //  String name = organizationModel.getFirst_name().trim() + " " + organizationModel.getLast_name();
        Mail mailObject = new Mail();
        String encryptedString;

        try {
          encryptedString = generateToken(login);
        } catch (UnsupportedEncodingException e) {
          ResponseHelper.writeInternalServerError(routingContext.response());
          return;
        }

        login.setKeyValue(UUID.randomUUID().toString());

        mailObject.sendOrganizationDetails(Vertx.vertx(), login.getFirstName(),
            encryptedString, login.getEmail(), organizationModel.hris_url);

        Employee employee = new Employee();
        employee.setFirst_name(organizationModel.getFirst_name());
        employee.setLast_name(organizationModel.getLast_name());
        employee.setPersonal_email(organizationModel.getAccount_owner_email());
        employee.setWork_email(organizationModel.getAccount_owner_email());
        employee.setOrganization_id(organizationId);
        employee.setPrimary_phone(organizationModel.getPhone_no());
        employee.setPrimary_region_code(organizationModel.getRegion_code());
        employee.setWork_phone(organizationModel.getPhone_no());
        employee.setAccess_level("admin");
        employee.setOnboarding_status("Packet Not Sent");
        employee.setOnboarding_packet_date("-");
        employee.setOnboarding_packet("0");
        employee.setUser_id(id);
        new EmployeeHandler().addEmployeeDetails(employee);

        addOnboardingPacket(organizationId, id);
        addTaskProjectController(id, organizationId);
        addEmploymentType(organizationId);
      //  addJobDescription(organizationId);
        addHighestEducation(organizationId);
        addMinimumExperience(organizationId);
        JsonObject successObject = new JsonObject();

        //new OrganizationBOImpl(null).addNewAtsDomain(organizationModel.getHris_url(), organizationId);
        String url = "https://" + organizationModel.getHris_url();
        successObject.put("hris_url", url);
        jsonResponseObject.put("code", HttpStatus.SC_CREATED);
        jsonResponseObject.put("success", true);
        jsonResponseObject.put("data", successObject);

        // Adding default Roles to organization
        Connection connectionObject = new MysqldbConf().getConnection();
        PreparedStatement preparedStatement = connectionObject.prepareStatement("INSERT INTO `Role` (`name`, `organization_id`) " +
            "VALUES (?, ?)", RETURN_GENERATED_KEYS);
        for (String role : PathConstants.DEFAULT_ROLES) {
          preparedStatement.setString(1, role);
          preparedStatement.setLong(2, organizationId);
          preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
        ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
        //long[] ids = new long[PathConstants.DEFAULT_ROLES.length];
        Map<String, String> rolesMap = new HashMap<>();
        for (String role : PathConstants.DEFAULT_ROLES) {
          generatedKeys.next();
          rolesMap.put(role, "" + generatedKeys.getLong(1));
        }
        generatedKeys.close();
        preparedStatement.close();

        // Anonymous role
        rolesMap.put("Anonymous", "" + 0);
        Statement statement = connectionObject.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT `name`, `acl_plugin_id` From `Api`");
        Map<String, Api> apisMap = new HashMap<>();
        while (resultSet.next()) {
          Api api = new Api();
          api.setName(resultSet.getString(1));
          api.setAclId(resultSet.getString(2));
          apisMap.put(api.getName(), api);
        }
        resultSet.close();
        statement.close();
        connectionObject.close();

        // Adding the admin role and anonymous role
        String adminId = "" + id;
        AtomicInteger count = new AtomicInteger(PathConstants.APIS.size());
        HttpClient kongClient = KongServiceHelper.executeWithKongClient(routingContext.vertx());
        KongServiceHelper.addConsumer(kongClient, Long.parseLong(adminId), clientResponse -> {
          if (clientResponse.statusCode() == KongHttpStatusCode.ADD.getStatusCode()) {
            clientResponse.bodyHandler(buffer -> {
              String consumerId = new JsonObject(buffer).getString("id");
              try {
                Connection connection = new MysqldbConf().getConnection();
                PreparedStatement preparedStatementUpdate = connection.prepareStatement("UPDATE `Login` SET `consumer_id` = ? WHERE id = ?");
                preparedStatementUpdate.setString(1, consumerId);
                preparedStatementUpdate.setLong(2, Long.parseLong(adminId));
                preparedStatementUpdate.executeUpdate();
                preparedStatementUpdate.close();
                connection.close();
              } catch (SQLException e) {
                kongClient.close();
                ResponseHelper.writeInternalServerError(routingContext.response());
              } catch (ClassNotFoundException e) {
                kongClient.close();
                ResponseHelper.writeInternalServerError(routingContext.response());
              }
              KongServiceHelper.addConsumerRoles(kongClient, consumerId, rolesMap.get("Admin"), httpClientResponse -> {
                if (httpClientResponse.statusCode() == KongHttpStatusCode.ADD.getStatusCode()) {
                  for (Map.Entry<String, Object> entry : PathConstants.APIS.getMap().entrySet()) {
                    Map apiEntry = (Map) entry.getValue();
                    List<String> whitelisted = new ArrayList<>();
                    boolean isAnonymousEnabled = ((List<String>) apiEntry.get("roles")).contains("Anonymous");
                    boolean isOnlyAnonymous = ((List<String>) apiEntry.get("roles")).contains("Only Anonymous");
                    if (isOnlyAnonymous) {
                      whitelisted.add(rolesMap.get("Anonymous"));
                    } else if (isAnonymousEnabled) {
                      for (Map.Entry<String, String> roleEntry : rolesMap.entrySet()) {
                        whitelisted.add(roleEntry.getValue());
                      }
                    } else {
                      for (String role : ((List<String>) apiEntry.get("roles"))) {
                        whitelisted.add(rolesMap.get(role));
                      }
                    }
                    KongServiceHelper.editWhitelistRolesAcl(kongClient, apisMap.get(entry.getKey()).getAclId(), whitelisted, httpClientAclResponse -> {
                      if (httpClientAclResponse.statusCode() == KongHttpStatusCode.EDIT.getStatusCode()) {
                        if (count.decrementAndGet() == 0) {
                          kongClient.close();
                          routingContext.response()
                              .setStatusCode(HttpStatus.SC_CREATED)
                              .putHeader("Content-Type", "application/json; charset=utf-8; id="
                                  + organizationModel.getOrganization_id())
                              .end(jsonResponseObject.toString());
                        }
                      }
                    });
                  }
                }
              });
            });
          }
        });
      } else {
        ResponseHelper.writeErrorResponse(HttpStatus.SC_CONFLICT, "Domain name already Exists",
            "Domain name already Exists", routingContext.response());
      }

      routingContext.response()
          .setStatusCode(HttpStatus.SC_CREATED)
          .putHeader("Content-Type", "application/json; charset=utf-8; id="
              + organizationModel.getOrganization_id())
          .end(jsonResponseObject.toString());

    } catch (ClassNotFoundException e) {
      logger.error(e.getMessage());
      ResponseHelper.writeInternalServerError(routingContext.response());
    } catch (IllegalArgumentException e) {
      logger.error(e.getMessage());
      ResponseHelper.writeInternalServerError(routingContext.response());
    } catch (NoSuchFieldException e) {
      logger.error(e.getMessage());
      JsonObject jsonFailureObject = new JsonObject();
      jsonFailureObject.put("code", HttpStatus.SC_NOT_ACCEPTABLE);
      jsonFailureObject.put("message", e.getMessage());
      jsonFailureObject.put("description", e.getMessage());
      jsonResponseObject.put("error", jsonFailureObject);
    } catch (ParseException e) {
      logger.error(e.getMessage());
      ResponseHelper.writeInternalServerError(routingContext.response());
    } catch (SQLException e) {
      logger.error(e.getMessage());
      ResponseHelper.writeInternalServerError(routingContext.response());
    } catch (ApplicationUtilException e) {
      logger.error("Application Util Exception while adding Organization");
    } catch (NumberParseException e) {
      logger.error(e.getMessage());
      ResponseHelper.writeInternalServerError(routingContext.response());
    }
  }

  /**
   * addOnboardingPacket - Method used to add default packets for new Organization
   *
   * @param organizationId
   * @param creatorId
   * @throws SQLException
   * @throws ClassNotFoundException
   */

  public void addOnboardingPacket(int organizationId, int creatorId)
      throws SQLException, ClassNotFoundException {

    Connection connection = new MysqldbConf().getConnection();
    ResultSet resultSet = null;
    String sqlQuery = "insert into OnboardingPacket(packet_name,organization_id,creator_id,is_default) values(?,?,?,?)";
    PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, RETURN_GENERATED_KEYS);
    preparedStatement.setString(1, "Employee Onboarding");
    preparedStatement.setInt(2, organizationId);
    preparedStatement.setInt(3, creatorId);
    preparedStatement.setBoolean(4, true);
    preparedStatement.executeUpdate();
    resultSet = preparedStatement.getGeneratedKeys();
    int generatedPacketId = 0;
    if (resultSet.next()) {
      generatedPacketId = resultSet.getInt(1);
    }
    resultSet.close();
    preparedStatement.close();
    List<String> formNames = new ArrayList<>();

    sqlQuery = "select form_name from AuzmorForms";
    resultSet = DatabaseUtil.getResultSetForSelectQuery(connection, sqlQuery);
    while (resultSet.next()) {
      formNames.add(resultSet.getString("form_name"));
    }
    List<Boolean> formRequired = new ArrayList<>();
    for (int i = 0; i < formNames.size(); i++) {
      if (i == 0 || i == formNames.size() - 1) {
        formRequired.add(false);
      } else {
        formRequired.add(true);
      }

      OnboardingForm onboardingForm = new OnboardingForm();
      onboardingForm.setForm_name(formNames.get(i));
      onboardingForm.setForm_required(formRequired.get(i));
      onboardingForm.setPacket_id(generatedPacketId);
      sqlQuery = "insert into OnboardingForm(form_required,packet_id,form_name) values(?,?,?)";
      preparedStatement = connection.prepareStatement(sqlQuery);
      preparedStatement.setBoolean(1, onboardingForm.isForm_required());
      preparedStatement.setInt(2, onboardingForm.getPacket_id());
      preparedStatement.setString(3, onboardingForm.getForm_name());
      preparedStatement.executeUpdate();
      preparedStatement.close();
    }

    sqlQuery = "select document_name,document_path from AuzmorDocuments";
    resultSet = DatabaseUtil.getResultSetForSelectQuery(connection, sqlQuery);
    while (resultSet.next()) {
      OnboardingDocument onboardingDocument = new OnboardingDocument();
      onboardingDocument.setDocument_name(resultSet.getString("document_name"));
      onboardingDocument.setDocument_path(resultSet.getString("document_path"));
      onboardingDocument.setPacket_id(generatedPacketId);
      sqlQuery = "insert into OnboardingDocuments(document_name,packet_id,document_path,viewing_reqd) values(?,?,?,?)";
      preparedStatement = connection.prepareStatement(sqlQuery);
      preparedStatement.setString(1, onboardingDocument.getDocument_name());
      preparedStatement.setInt(2, onboardingDocument.getPacket_id());
      preparedStatement.setString(3, onboardingDocument.getDocument_path());
      preparedStatement.setBoolean(4, true);
      preparedStatement.executeUpdate();
      preparedStatement.close();
    }
    resultSet.close();
    connection.close();
  }

  String randomString(int len) {
    StringBuilder builder = new StringBuilder(len);
    for (int i = 0; i < len; i++)
      builder.append(alphaNumericSet.charAt(secureRandom.nextInt(alphaNumericSet.length())));
    return builder.toString();
  }

  /**
   * addTaskProjectController - Method used to add default task for new organization
   *
   * @param admin
   * @param organizationId
   */


  public void addTaskProjectController(int admin, int organizationId)
      throws SQLException, ClassNotFoundException {
    Connection connection = new MysqldbConf().getConnection();
    Date dNow = new Date();
    SimpleDateFormat jobPostDateFormat =
        new SimpleDateFormat("MM/dd/yyyy");
    String candidateCreateDateString = jobPostDateFormat.format(dNow);
    String sqlQuery = "insert into Projects(project_name,organization_id,admin_id,created_date) values(?,?,?,?)";
    PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
    preparedStatement.setString(1, "None");
    preparedStatement.setInt(2, organizationId);
    preparedStatement.setInt(3, admin);
    preparedStatement.setString(4, candidateCreateDateString);
    preparedStatement.executeUpdate();
    preparedStatement.close();
  }

  public void addEmploymentType(int organizationId) throws SQLException, ClassNotFoundException {
    // Add Employee Type in Employment Table
    Connection connection = new MysqldbConf().getConnection();

    // For Part-Time Job
    String sqlQuery = "insert into EmploymentType(employmentType,active_status,organization_id) values(?,?,?)";
    PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
    preparedStatement.setString(1, "Part-Time");
    preparedStatement.setString(2, "true");
    preparedStatement.setInt(3, organizationId);
    preparedStatement.executeUpdate();
    preparedStatement.close();

    // For Full-Time Job
    sqlQuery = "insert into EmploymentType(employmentType,active_status,organization_id) values(?,?,?)";
    preparedStatement = connection.prepareStatement(sqlQuery);
    preparedStatement.setString(1, "Full-Time");
    preparedStatement.setString(2, "true");
    preparedStatement.setInt(3, organizationId);
    preparedStatement.executeUpdate();
    preparedStatement.close();

    // For Contract Job
    sqlQuery = "insert into EmploymentType(employmentType,active_status,organization_id) values(?,?,?)";
    preparedStatement = connection.prepareStatement(sqlQuery);
    preparedStatement.setString(1, "Contractor - W2");
    preparedStatement.setString(2, "true");
    preparedStatement.setInt(3, organizationId);
    preparedStatement.executeUpdate();
    preparedStatement.close();

    // For Contractor - 1099 Job
    sqlQuery = "insert into EmploymentType(employmentType,active_status,organization_id) values(?,?,?)";
    preparedStatement = connection.prepareStatement(sqlQuery);
    preparedStatement.setString(1, "Contractor - 1099");
    preparedStatement.setString(2, "true");
    preparedStatement.setInt(3, organizationId);
    preparedStatement.executeUpdate();
    preparedStatement.close();
    connection.close();
  }

  public void addHighestEducation(int organizationId) throws SQLException, ClassNotFoundException {
    Connection connection = new MysqldbConf().getConnection();
    String insertQuery = "insert into Highest_Education (highest_education,organization_id) " +
        " values (?,?)";
    PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
    List<String> highestEducationList = new ArrayList<>();
    highestEducationList.add("GED or Equivalent");
    highestEducationList.add("High School");
    highestEducationList.add("Some College");
    highestEducationList.add("Associate");
    highestEducationList.add("Bachelors");
    highestEducationList.add("Masters");
    highestEducationList.add("Doctorate");

    for (String highestEducation : highestEducationList) {
      preparedStatement.setString(1, highestEducation);
      preparedStatement.setLong(2, (long) organizationId);
      int result = 0;
      result = preparedStatement.executeUpdate();

      if (result == 0) {
        throw new SQLException("Highest Education Record not inserted");
      }
    }

    preparedStatement.close();
    if (connection != null || !connection.isClosed()) {
      connection.close();
    }

  }

  public void addMinimumExperience(int organizationId) throws SQLException, ClassNotFoundException {
    Connection connection = new MysqldbConf().getConnection();
    String insertQuery = "insert into Minimum_Experience (minimum_experience,organization_id) " +
        " values (?,?)";
    PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
    List<String> minimumExperienceList = new ArrayList<>();
    minimumExperienceList.add("Entry-level");
    minimumExperienceList.add("Mid-level");
    minimumExperienceList.add("Experienced");
    minimumExperienceList.add("Manager/Supervisor");
    minimumExperienceList.add("Senior Manager/Supervisor");
    minimumExperienceList.add("Executive");
    minimumExperienceList.add("Senior Executive");

    for (String minimumExperience : minimumExperienceList) {
      preparedStatement.setString(1, minimumExperience);
      preparedStatement.setLong(2, (long) organizationId);
      int result = 0;
      result = preparedStatement.executeUpdate();

      if (result == 0) {
        throw new SQLException("Minimum Experience Record not inserted");
      }
    }

    preparedStatement.close();
    if (connection != null || !connection.isClosed()) {
      connection.close();
    }

  }

  /**
   * getOrganizationStatus - a method to check registered organization
   *
   * @param routingContext
   * @method GET
   */

  public void getOrganizationStatus(RoutingContext routingContext) {

    int organizationEntries = 0;
    int id = 0;
    String imageUrl = "";
    String logo = "";
    String favicon = "";

    MultiMap params = routingContext.request().params();
    String organizationName = params.get("organization_name");
    JsonObject jsonSuccessObject = new JsonObject();
    jsonSuccessObject.put("code", HttpStatus.SC_OK);
    jsonSuccessObject.put("success", true);
    String queryString = "";
    Connection connectionObject = null;
    PreparedStatement preparedStatement = null;
    ResultSet resultSetObject = null;

    try {
      if (organizationName == null || organizationName.length() == 0) {
        throw new IllegalArgumentException("Organization '" + organizationName + "' " +
            "is not registered");
      }
      connectionObject = new MysqldbConf().getConnection();
      String urlName = JWTUtil.getUrlPath();
      queryString = "SELECT organization_id,logo_image,logo,favicon FROM Organization  " +
          "where hris_url = ? ";
      preparedStatement = connectionObject.prepareStatement(queryString);
      preparedStatement.setString(1, organizationName.trim() + urlName.trim());
      resultSetObject = preparedStatement.executeQuery();

      while (resultSetObject.next()) {
        id = resultSetObject.getInt("organization_id");
        imageUrl = resultSetObject.getString("logo_image");
        logo = resultSetObject.getString("logo");
        favicon = resultSetObject.getString("favicon");
        ++organizationEntries;
      }

      if (organizationEntries != 1) {
        throw new IllegalArgumentException("Organization '" + organizationName + "' " +
            "is not registered");
      } else {
        JsonObject dataObject = new JsonObject();
        dataObject.put("organization_id", id);
        dataObject.put("image_url", logo);
        dataObject.put("favicon", favicon);
        jsonSuccessObject.put("data", dataObject);
        routingContext.response()
            .setStatusCode(HttpStatus.SC_OK)
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .end(jsonSuccessObject.toString());
      }
      resultSetObject.close();
      preparedStatement.close();
      connectionObject.close();
    } catch (SQLException e) {
      logger.error("Sql Exception while updating Organization");
      JsonObject jsonResponseObject = new JsonObject();
      JsonObject jsonFailureObject = new JsonObject();
      jsonFailureObject.put("code", HttpStatus.SC_NOT_FOUND);
      jsonFailureObject.put("description", e.getMessage());
      jsonResponseObject.put("error", jsonFailureObject);
      routingContext.response()
          .setStatusCode(HttpStatus.SC_CONFLICT)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonFailureObject.toString());
    } catch (ClassNotFoundException e) {
      logger.error("ClassNotFoundException while updating Organization");
      JsonObject jsonResponseObject = new JsonObject();
      JsonObject jsonFailureObject = new JsonObject();
      jsonFailureObject.put("code", HttpStatus.SC_NOT_FOUND);
      jsonFailureObject.put("description", e.getMessage());
      jsonResponseObject.put("error", jsonFailureObject);
      routingContext.response()
          .setStatusCode(HttpStatus.SC_CONFLICT)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonFailureObject.toString());
    } catch (IllegalArgumentException e) {
      logger.error("IllegalArgumentException while updating Organization");
      JsonObject jsonResponseObject = new JsonObject();
      JsonObject jsonFailureObject = new JsonObject();
      jsonFailureObject.put("code", HttpStatus.SC_NOT_FOUND);
      jsonFailureObject.put("description", e.getMessage());
      jsonResponseObject.put("error", jsonFailureObject);
      routingContext.response()
          .setStatusCode(HttpStatus.SC_CONFLICT)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonFailureObject.toString());
    } catch (ApplicationUtilException e) {
      logger.error("IllegalArgumentException while updating Organization");
    }
  }

  public int addOrganizationDetails(Organization organizationModel)
      throws SQLException, ClassNotFoundException, ParseException {

    Connection connectionObject = new MysqldbConf().getConnection();
    int organizationId = 0;
    Map<String, Object> insertionObjects = new LinkedHashMap<>();
    Date dNow = new Date();
    SimpleDateFormat jobPostDateFormat =
        new SimpleDateFormat("MM/dd/yyyy");
    String candidateCreateDateString = jobPostDateFormat.format(dNow);
    organizationModel.setCreated_date(candidateCreateDateString);
    organizationModel.setLogo("static/default/default-logo.png");
    organizationModel.setFavicon("static/default/favicon.png");

    if (StringUtil.printValidString(organizationModel.getOrganization_name()).equals("")) {
      organizationModel.setOrganization_name(organizationModel.getDomain_name());
    }


    if (StringUtils.defaultString(organizationModel.getDefault_country()).equals("")) {
      organizationModel.setDefault_country("United States");
    }

    StringUtil.checkRequiredFields(insertionObjects, "domain_name",
        organizationModel.getDomain_name());
    StringUtil.checkRequiredFields(insertionObjects, "first_name",
        organizationModel.getFirst_name());
    StringUtil.checkRequiredFields(insertionObjects, "last_name",
        organizationModel.getLast_name());
    StringUtil.checkRequiredFields(insertionObjects, "account_owner_email",
        organizationModel.getAccount_owner_email());
    StringUtil.checkRequiredFields(insertionObjects, "phone_no",
        organizationModel.getPhone_no());
    StringUtil.checkRequiredFields(insertionObjects, "region_code",
        organizationModel.getRegion_code());
    StringUtil.checkRequiredFields(insertionObjects, "hris_url",
        organizationModel.getHris_url());
    StringUtil.checkRequiredFields(insertionObjects, "organization_name",
        organizationModel.getOrganization_name());
    String sqlQueryForOrganization = "insert into Organization(domain_name,first_name,last_name,account_owner_email,phone_no,region_code,hris_url," +
        "file_size,company_address_line1,city,state,zip_code,default_country,billing_credit_card,billing_expiry_date," +
        "billing_cvv,billing_address_line1,billing_city,billing_state,billing_zip_code,billing_country,billing_contact_person1," +
        "company_address_line2,billing_address_line2,billing_contact_person2,billing_contact_person3,ein,created_date,logo,favicon,organization_name)" +
        " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
    PreparedStatement preparedStatement = connectionObject.prepareStatement(sqlQueryForOrganization, RETURN_GENERATED_KEYS);
    preparedStatement.setString(1, organizationModel.getDomain_name());
    preparedStatement.setString(2, organizationModel.getFirst_name());
    preparedStatement.setString(3, organizationModel.getLast_name());
    preparedStatement.setString(4, organizationModel.getAccount_owner_email());
    preparedStatement.setString(5, organizationModel.getPhone_no());
    preparedStatement.setString(6, organizationModel.getRegion_code());
    preparedStatement.setString(7, organizationModel.getHris_url());
    preparedStatement.setString(8, StringUtil.printValidString(organizationModel.getFile_size()));
    preparedStatement.setString(9, StringUtil.printValidString(organizationModel.getCompany_address_line1()));
    preparedStatement.setString(10, StringUtil.printValidString(organizationModel.getCity()));
    preparedStatement.setString(11, StringUtil.printValidString(organizationModel.getState()));
    preparedStatement.setString(12, StringUtil.printValidString(Integer.toString(organizationModel.getZip_code())));
    preparedStatement.setString(13, StringUtil.printValidString(organizationModel.getDefault_country()));
    preparedStatement.setString(14, StringUtil.printValidString(organizationModel.getBilling_credit_card()));
    preparedStatement.setString(15, StringUtil.printValidString(organizationModel.getBilling_expiry_date()));
    preparedStatement.setString(16, StringUtil.printValidString(Integer.toString(organizationModel.getBilling_cvv())));
    preparedStatement.setString(17, StringUtil.printValidString(organizationModel.getBilling_address_line1()));
    preparedStatement.setString(18, StringUtil.printValidString(organizationModel.getBilling_city()));
    preparedStatement.setString(19, StringUtil.printValidString(organizationModel.getBilling_state()));
    preparedStatement.setString(20, StringUtil.printValidString(Integer.toString(organizationModel.getBilling_zip_code())));
    preparedStatement.setString(21, StringUtil.printValidString(organizationModel.getBilling_country()));
    preparedStatement.setString(22, StringUtil.printValidString(organizationModel.getBilling_contact_person1()));
    preparedStatement.setString(23, StringUtil.printValidString(organizationModel.getCompany_address_line2()));
    preparedStatement.setString(24, StringUtil.printValidString(organizationModel.getBilling_address_line2()));
    preparedStatement.setString(25, StringUtil.printValidString(organizationModel.getBilling_contact_person2()));
    preparedStatement.setString(26, StringUtil.printValidString(organizationModel.getBilling_contact_person3()));
    preparedStatement.setString(27, StringUtil.printValidString(organizationModel.getEin()));
    preparedStatement.setString(28, StringUtil.printValidString(organizationModel.getCreated_date()));
    preparedStatement.setString(29, StringUtil.printValidString(organizationModel.getLogo()));
    preparedStatement.setString(30, StringUtil.printValidString(organizationModel.getFavicon()));
    preparedStatement.setString(31, organizationModel.getOrganization_name());

    String selectSQL = "select count(1) from Organization where hris_url= ?";
    PreparedStatement preparedStatementCount = connectionObject.prepareStatement(selectSQL);
    preparedStatementCount.setString(1, organizationModel.getHris_url());
    ResultSet rs = preparedStatementCount.executeQuery();
    int countDomain = -1;
    while (rs.next()) {
      countDomain = rs.getInt(1);
    }

    rs.close();
    preparedStatementCount.close();
    if (countDomain == 0) {
      preparedStatement.executeUpdate();
      ResultSet resultSet = preparedStatement.getGeneratedKeys();
      if (resultSet.next()) {
        organizationId = resultSet.getInt(1);
      }
      resultSet.close();
      preparedStatement.close();
      if (organizationId > 0) {
        int count = 0;
        String sqlQueryForPlanDetails = "insert into PlanDetails" +
            "(plan_name,plan_description,plan_version_id,organization_id) " +
            " values(?,?,?,?)";
        preparedStatement = connectionObject.prepareStatement(sqlQueryForPlanDetails, RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, "Trail");
        preparedStatement.setString(2, "Trail Version Only For 30 Days");
        preparedStatement.setInt(3, 1);
        preparedStatement.setInt(4, organizationId);
        preparedStatement.executeUpdate();
        ResultSet resultSetPlan = preparedStatement.getGeneratedKeys();
        while (resultSetPlan.next()) {
          count = resultSetPlan.getInt(1);
        }
        resultSetPlan.close();
        preparedStatement.close();
        if (count == 0) {
          throw new IllegalArgumentException("Trail Plan for Organization is not valid!");
        }
      }
    }

    connectionObject.close();
    return organizationId;
  }

  /**
   * updateOrganization - method to change Organization's fields
   *
   * @param routingContext
   */

  public void updateOrganization(RoutingContext routingContext) {

    try {

      JsonObject jsonResponseObject = new JsonObject();
      JsonObject errorObject = new JsonObject();
      errorObject.put("code", HttpStatus.SC_NOT_ACCEPTABLE);

      MultiMap params = (routingContext.request()).params();
      String auzmorDomain = params.get("auzmor_domain");
      String careerDomain = params.get("career_domain");
      String auzmorKeyFile = params.get("auzmor_key_file");
      String auzmorCrtFile = params.get("auzmor_crt_file");
      String careerKeyFile = params.get("career_key_file");
      String careerCrtFile = params.get("career_crt_file");
      if (null != auzmorDomain) {
        params.remove("auzmor_domain");
      }
      if (null != careerDomain) {
        params.remove("career_domain");
      }
      if (null != auzmorKeyFile) {
        params.remove("auzmor_key_file");
      }
      if (null != auzmorCrtFile) {
        params.remove("auzmor_crt_file");
      }
      if (null != careerKeyFile) {
        params.remove("career_key_file");
      }
      if (null != careerCrtFile) {
        params.remove("career_crt_file");
      }
      long id = Long.parseLong(routingContext.request().getParam("organizationId"));

      if (!StringUtil.printValidString(params.get("region_code")).equals("")
          && !StringUtil.printValidString(params.get("phone_no")).equals("")) {


        boolean isValidPhone = StringUtil.isValidPhoneNumber(params.get("phone_no"),
            params.get("region_code"));

        if (!isValidPhone) {
          JsonObject jsonFailureObject = new JsonObject();
          jsonFailureObject.put("code", HttpStatus.SC_BAD_REQUEST);
          jsonFailureObject.put("message", "Please provide proper phone number");
          jsonFailureObject.put("description", "Please provide proper phone number");
          jsonResponseObject.put("error", jsonFailureObject);
          routingContext.response()
              .setStatusCode(HttpStatus.SC_BAD_REQUEST)
              .putHeader("Content-Type", "application/json; charset=utf-8; id=")
              .end(jsonResponseObject.toString());
        }
      }

      Connection connectionObject = new MysqldbConf().getConnection();

      SimpleDateFormat updateDateFormatter = new SimpleDateFormat("MM/dd/YYYY");
      StringBuilder parameterizedSql = new StringBuilder("UPDATE `Organization` SET `organization_name` = ?, ");
      parameterizedSql.append("`default_country` = ?, `phone_no` = ?, `region_code` = ?, `date_format` = ?, ");
      parameterizedSql.append("`timezone_format` = ?, `number_format` = ?, `logo` = ?, `default_currency` = ?, ");
      parameterizedSql.append("`name_format` = ?, `modified_date` = ? WHERE `organization_id` = ?");
      PreparedStatement preparedStatementUpdate = connectionObject.prepareStatement(parameterizedSql.toString());
      preparedStatementUpdate.setString(1, StringUtil.printValidString(params.get("organization_name")));
      preparedStatementUpdate.setString(2, StringUtil.printValidString(params.get("default_country")));
      preparedStatementUpdate.setString(3, StringUtil.printValidString(params.get("phone_no")));
      preparedStatementUpdate.setString(4, StringUtil.printValidString(params.get("region_code")));
      preparedStatementUpdate.setString(5, StringUtil.printValidString(params.get("date_format")));
      preparedStatementUpdate.setString(6, StringUtil.printValidString(params.get("timezone_format")));
      preparedStatementUpdate.setString(7, StringUtil.printValidString(params.get("number_format")));
      preparedStatementUpdate.setString(8, StringUtil.printValidString(params.get("logo")));
      preparedStatementUpdate.setString(9, StringUtil.printValidString(params.get("default_currency")));
      preparedStatementUpdate.setString(10, StringUtil.printValidString(params.get("name_format")));
      preparedStatementUpdate.setString(11, updateDateFormatter.format(new Date()));
      preparedStatementUpdate.setLong(12, id);

      preparedStatementUpdate.execute();

      JsonObject dataObject = new JsonObject();
      dataObject.put("message", "Organization Updated Successfully!");

      JsonObject jsonSuccessObject = new JsonObject();
      jsonSuccessObject.put("code", HttpStatus.SC_ACCEPTED);
      jsonSuccessObject.put("success", true);
      jsonSuccessObject.put("data", dataObject);
      routingContext.response()
          .setStatusCode(HttpStatus.SC_ACCEPTED)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonSuccessObject.toString());

      preparedStatementUpdate.close();
      connectionObject.close();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
      logger.error("Sql Exception while updating Organization");
      JsonObject jsonResponseObject = new JsonObject();
      JsonObject jsonFailureObject = new JsonObject();
      jsonFailureObject.put("code", HttpStatus.SC_NOT_ACCEPTABLE);
      jsonFailureObject.put("message", "Wrong Field Name");
      jsonFailureObject.put("description", e.getMessage());

      jsonResponseObject.put("error", jsonFailureObject);
      routingContext.response()
          .setStatusCode(HttpStatus.SC_NOT_ACCEPTABLE)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
    } catch (SQLException e) {
      e.printStackTrace();
      logger.error("Sql Exception while updating Organization");
      JsonObject jsonResponseObject = new JsonObject();
      JsonObject jsonFailureObject = new JsonObject();
      jsonFailureObject.put("code", HttpStatus.SC_NOT_ACCEPTABLE);
      jsonFailureObject.put("message", "Wrong Field Name");
      jsonFailureObject.put("description", e.getMessage());

      jsonResponseObject.put("error", jsonFailureObject);
      routingContext.response()
          .setStatusCode(HttpStatus.SC_NOT_ACCEPTABLE)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
    } catch (NumberParseException e) {
      logger.error(e.getMessage());
      JsonObject jsonResponseObject = new JsonObject();
      JsonObject jsonFailureObject = new JsonObject();
      jsonFailureObject.put("code", HttpStatus.SC_NOT_ACCEPTABLE);
      jsonFailureObject.put("message", e.getMessage());
      jsonFailureObject.put("description", e.getMessage());

      jsonResponseObject.put("error", jsonFailureObject);
      routingContext.response()
          .setStatusCode(HttpStatus.SC_NOT_ACCEPTABLE)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());
    }
  }

  /**
   * deleteOrganization - method to delete the exisiting Organization
   *
   * @param routingContext
   */

  public void deleteOrganization(RoutingContext routingContext) {
    String id = routingContext.request().getParam("id");
    if (id == null) {
      routingContext.response().setStatusCode(HttpStatus.SC_BAD_REQUEST).
          end("Organization is not registered");
    } else {
      Integer idAsInteger = Integer.valueOf(id);
      organizationData.remove(idAsInteger);
      routingContext.response().setStatusCode(HttpStatus.SC_OK)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(Json.encodePrettily(organizationData));
    }
  }

  /**
   * getAllOrganization - method to get all registered Organizations
   *
   * @param routingContext
   */
  /*public void getAllOrganization(RoutingContext routingContext) {
    routingContext.response()
        .putHeader("Content-Type", "application/json; charset=utf-8")
        .end(Json.encodePrettily(organizationData.values()));
  }*/

  /**
   * getOrganizationDetails -  Display the details about the Organization
   *
   * @param routingContext
   */

  public void getOrganizationDetails(RoutingContext routingContext) {
    JsonObject jsonResponseObject = new JsonObject();
    jsonResponseObject.put("code", HttpStatus.SC_OK);
    jsonResponseObject.put("success", true);

    Organization organization = (Organization) routingContext.data().get("organization");

    MultiMap params = routingContext.request().params();
    String fields = params.get("fields");
    JsonObject jsonObject = new JsonObject();
    if (null == fields) {
      jsonObject.put("organization_name", organization.getOrganization_name());
      jsonObject.put("organization_id", organization.getOrganization_id());
      jsonObject.put("first_name", organization.getFirst_name());
      jsonObject.put("default_country", organization.getDefault_country());
      jsonObject.put("region_code", organization.getRegion_code());
      jsonObject.put("phone_no", organization.getPhone_no());
      jsonObject.put("default_currency", organization.getDefault_currency());
      jsonObject.put("timezone_format", organization.getTimezone_format());
      jsonObject.put("date_format", organization.getDate_format());
      jsonObject.put("number_format", organization.getNumber_format());
      jsonObject.put("name_format", organization.getName_format());

      jsonObject.put("logo", organization.getLogo());
      jsonObject.put("favicon", organization.getFavicon());
    } else {
      for (String field : fields.split(",")) {
        if (field.equals("default_country")) {
          jsonObject.put("default_country", organization.getDefault_country());
        }
      }
    }
    jsonResponseObject.put("data", jsonObject);
    routingContext.response().setStatusCode(HttpStatus.SC_OK).putHeader("Content-Type",
        "application/json; charset=utf-8").end(jsonResponseObject.toString());
  }

  /**
   * getEmployeeActiveDetails - Checking the active and inactive status of the Employee
   *
   * @param connectionObject
   * @param id
   * @param activecount
   * @param organizationid
   * @return
   */

  public int getEmployeeActiveDetails(Connection connectionObject, int id,
                                      int activecount, int organizationid) {
    int count = 0;
    try {
      PreparedStatement preparedStatement = null;
      String queryString = "";
      queryString = "SELECT count(*) FROM Employee where active_status=? and organization_id = ?";
      preparedStatement = connectionObject.prepareStatement(queryString);
      preparedStatement.setInt(1, activecount);
      preparedStatement.setInt(2, organizationid);
      ResultSet resultSetObject = preparedStatement.executeQuery();
      while (resultSetObject.next()) {
        count = resultSetObject.getInt("count(*)");
      }
      resultSetObject.close();
      preparedStatement.close();
      return count;
    } catch (SQLException e) {
      logger.error("Sql Exception while getting employee data");
    }
    return count;
  }

  /**
   * addOrganizationImageFavicon - method to add logo and favicon in an Organization
   *
   * @param routingContext
   */

  public void addOrganizationImageFavicon(RoutingContext routingContext) {
    try {
      Connection connectionObject = new MysqldbConf().getConnection();
      MultiMap mapObject = (routingContext.request()).params();
      int id = Integer.parseInt(mapObject.get("organization_id"));
      Set<FileUpload> fileUploadSet = routingContext.fileUploads();
      for (FileUpload fileUpload : fileUploadSet) {
        FileTypeEnum fileTypeEnum = FileHelper.getFavImageFormatType(fileUpload);
        if (null != fileTypeEnum) {
          if (FileUploadHelper.isValidDimension(fileUpload.uploadedFileName(), 32, 32)) {
            String sourceFile = fileUpload.uploadedFileName();
            String destinationFile = fileUpload.uploadedFileName().split(File.separator)[1] +
                fileTypeEnum.getExtension();
            String destinationFolderPath = EndpointConstant.publicStaticImage + File.separator +
                routingContext.request().getParam("organization_id");
            FileHelper.move(sourceFile, destinationFolderPath, destinationFile);
            String logoImages = destinationFolderPath + File.separator + destinationFile;
            String updateString = "update Organization set favicon = ?  where organization_id = ?";
            PreparedStatement preparedStatement = connectionObject.prepareStatement(updateString);
            preparedStatement.setString(1, logoImages);
            preparedStatement.setInt(2, id);
            int updatedRows = 0;
            try {
              updatedRows = preparedStatement.executeUpdate();
            } catch (SQLException e) {
              ResponseHelper.writeInternalServerError(routingContext.response());
            } finally {
              preparedStatement.close();
              connectionObject.close();
            }
            if (updatedRows == 0) {
              throw new IllegalArgumentException("Image Upload Failed");
            }
            routingContext.response()
                .setStatusCode(HttpStatus.SC_CREATED)  // 201
                .putHeader("Content-Type", "application/json; charset=utf-8")
                .end(destinationFolderPath + File.separator + destinationFile);
          } else {
            ResponseHelper.writeStringErrorResponse(
                "Please upload an image of size 32 X 32px ", routingContext.response());
          }
        } else {
          ResponseHelper.writeStringErrorResponse(
              "Please upload an image of PNG/GIF format", routingContext.response());
        }
      }
    } catch (IllegalArgumentException e) {
      logger.error("Favicon Image Upload Failed!");
      routingContext.response()
          .setStatusCode(org.apache.commons.httpclient.HttpStatus.SC_NOT_FOUND) // 404
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end("Favicon Image Upload Failed! please upload only PNG/GIF Format of size (32x32)");
    } catch (IOException e) {
      routingContext.response()
          .setStatusCode(org.apache.commons.httpclient.HttpStatus.SC_NOT_FOUND) // 404
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end("Favicon Image Upload Failed! please upload only PNG/GIF Format of size (32x32)");
    } catch (SQLException e) {
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  /**
   * addOrganizationImage - Adding image to the Oraganization
   *
   * @param routingContext
   */
  public void addOrganizationImage(RoutingContext routingContext) {
    try {
      Connection connectionObject = new MysqldbConf().getConnection();
      MultiMap mapObject = (routingContext.request()).params();
      int id = Integer.parseInt(mapObject.get("organization_id"));
      Set<FileUpload> fileUploadSet = routingContext.fileUploads();
      for (FileUpload fileUpload : fileUploadSet) {
        FileTypeEnum fileTypeEnum = FileHelper.getImageFormatType(fileUpload);
        if (null != fileTypeEnum) {
          if (FileUploadHelper.isValidDimension(fileUpload.uploadedFileName(), 360, 162)) {
            String sourceFile = fileUpload.uploadedFileName();
            String destinationFile = fileUpload.uploadedFileName().split(File.separator)[1] +
                fileTypeEnum.getExtension();
            String destinationFolderPath = EndpointConstant.publicStaticImage + File.separator +
                routingContext.request().getParam("organization_id");
            FileHelper.move(sourceFile, destinationFolderPath, destinationFile);
            String logoImages = destinationFolderPath + File.separator + destinationFile;
            String updateString = "update Organization set logo = ?  where organization_id = ?";
            PreparedStatement preparedStatement = connectionObject.prepareStatement(updateString);
            preparedStatement.setString(1, logoImages);
            preparedStatement.setInt(2, id);

            int updatedRows = preparedStatement.executeUpdate();
            if (updatedRows == 0) {
              throw new IllegalArgumentException("Image Upload Failed");
            }
            preparedStatement.close();
            connectionObject.close();
            routingContext.response()
                .setStatusCode(HttpStatus.SC_CREATED)  // 201
                .putHeader("Content-Type", "application/json; charset=utf-8")
                .end(destinationFolderPath + File.separator + destinationFile);
          } else {
            ResponseHelper.writeStringErrorResponse(
                "Please upload an image of size 360 X 162px", routingContext.response());
          }
        } else {
          ResponseHelper.writeStringErrorResponse(
              "Please upload an image of JPG/PNG format", routingContext.response());
        }
      }
    } catch (IllegalArgumentException e) {
      logger.error("Logo Image Upload Failed!");
      routingContext.response()
          .setStatusCode(org.apache.commons.httpclient.HttpStatus.SC_NOT_FOUND) // 404
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(e.getMessage());
    } catch (IOException e) {
      logger.error("Logo Image Upload Failed!");
      routingContext.response()
          .setStatusCode(HttpStatus.SC_NOT_FOUND)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(e.getMessage());
    } catch (SQLException e) {
      logger.error("Logo Image Upload Failed!");
      routingContext.response()
          .setStatusCode(HttpStatus.SC_NOT_FOUND)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(e.getMessage());
    } catch (ClassNotFoundException e) {
      logger.error("Logo Image Upload Failed!");
      routingContext.response()
          .setStatusCode(HttpStatus.SC_NOT_FOUND)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(e.getMessage());
    }
  }

  /**
   * addOrganizationSettingOptions - Initial setting of the Organization
   *
   * @param routingContext
   */

  public void addOrganizationSettingOptions(RoutingContext routingContext) {
    try {
      Connection connectionObject = new MysqldbConf().getConnection();
      String countryOption = "id,country_name";
      String dateOption = "id,dateformat_type";
      String numberOption = "id,numberformat";
      String displayOption = "id,nameformat";
      JsonObject dataObject = new JsonObject();
      String country_name = "";
      JsonArray resultSetArray = new JsonArray();
      country_name = routingContext.request().params().get("country_name");

      if (country_name == null) {
        dataObject.put("country",
            dbUtil.getFieldOption(connectionObject, 0, countryOption, "Country"));
        dataObject.put("date_format",
            dbUtil.getFieldOption(connectionObject, 0, dateOption, "DateFormat"));
        dataObject.put("number_format",
            dbUtil.getFieldOption(connectionObject, 0, numberOption, "NumberFormat"));
        dataObject.put("display_name_format",
            dbUtil.getFieldOption(connectionObject, 0, displayOption, "DisplayNameFormat"));
      } else {
        // for initial currency details
        JsonArray currencyArray = new JsonArray();
        String currency = "";
        String currencyCode = "";
        String initialCurrencySQL = "select currency_name,currency_code from Country where country_name= ?";
        PreparedStatement preparedStatement = connectionObject.prepareStatement(initialCurrencySQL);
        preparedStatement.setString(1, country_name.trim());
        ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet.isBeforeFirst()) {
          while (resultSet.next()) {
            JsonObject currencyObject = new JsonObject();
            currency = resultSet.getString("currency_name") + "" +
                "(" + resultSet.getString("currency_code") + ")";
            currencyCode = resultSet.getString("currency_code");
            currencyObject.put("currency_code", currencyCode);
            currencyObject.put("currency_name", currency);
            currencyArray.add(currencyObject);
          }
        }else{
          JsonObject currencyObject = new JsonObject();
          currencyObject.put("currency_code", currencyCode);
          currencyObject.put("currency_name", currency);
          currencyArray.add(currencyObject);
        }
        resultSet.close();
        preparedStatement.close();
        dataObject.put("initial_currency",currencyArray);

        //for total currency details
        JsonArray totalCurrencyArray = new JsonArray();
        String totalCurrency = "";
        String totalCurrencyCode = "";
        String totalCurrencySQL = "select currency_name,currency_code from Country where country_name != ?";
        preparedStatement = connectionObject.prepareStatement(totalCurrencySQL);
        preparedStatement.setString(1, country_name.trim());
        resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
          JsonObject totalCurrencyObject = new JsonObject();
          totalCurrency = resultSet.getString("currency_name") + "" +
              "(" + resultSet.getString("currency_code") + ")";
          totalCurrencyCode = resultSet.getString("currency_code");
          totalCurrencyObject.put("currency_code",totalCurrencyCode);
          totalCurrencyObject.put("currency_name",totalCurrency);
          totalCurrencyArray.add(totalCurrencyObject);
        }
        resultSet.close();
        preparedStatement.close();
        dataObject.put("total_currency",totalCurrencyArray);

       // for initial timezone details
        JsonArray initialTimeZoneArray = new JsonArray();
        String intialTimeZoneQuery = "select timezone_name,gmt_offset from TimeZones where country_name  like  ? ";
        preparedStatement = connectionObject.prepareStatement(intialTimeZoneQuery);
        preparedStatement.setString(1, country_name.trim() + "%");
        resultSet = preparedStatement.executeQuery();
        if(resultSet.isBeforeFirst()) {
          while (resultSet.next()) {
            JsonObject initialTimeZoneObject = new JsonObject();
            initialTimeZoneObject.put("name", resultSet.getString(1) + "" +
                " " + "(" + resultSet.getString(2) + ")");
            initialTimeZoneArray.add(initialTimeZoneObject);
          }
        } else {
          JsonObject initialTimeZoneObject = new JsonObject();
          initialTimeZoneObject.put("name"," ");
          initialTimeZoneArray.add(initialTimeZoneObject);
        }
        resultSet.close();
        preparedStatement.close();
        dataObject.put("initial_timezone",initialTimeZoneArray);

        //for total timezoneDetails
        JsonArray totalTimeZoneArray = new JsonArray();
        String totalTimeZoneQuery = "select timezone_name,gmt_offset from TimeZones where country_name  not like  ? ";
        preparedStatement = connectionObject.prepareStatement(totalTimeZoneQuery);
        preparedStatement.setString(1, country_name.trim() + "%");
        resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
          JsonObject totalTimeZoneObject = new JsonObject();
          totalTimeZoneObject.put("name", resultSet.getString(1) + "" +
              " " + "(" + resultSet.getString(2) + ")");
          totalTimeZoneArray.add(totalTimeZoneObject);
        }
        resultSet.close();
        preparedStatement.close();
        dataObject.put("total_timezone",totalTimeZoneArray);

        //for mobile number region code
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        JsonArray totalCallingCode = new JsonArray();
        JsonArray initialCallingCode = new JsonArray();
        for (Integer callingCode : phoneNumberUtil.getSupportedCallingCodes()) {
          JsonObject callingObject = new JsonObject();
          callingObject.put("calling_code", "+" + callingCode);
          String regionCode = phoneNumberUtil.getRegionCodeForCountryCode(callingCode);
          callingObject.put("region_code", regionCode);
          Locale locale = new Locale("", regionCode);
          callingObject.put("country_name", locale.getDisplayCountry());
          if (callingObject.getString("country_name").equals(country_name)) {
            initialCallingCode.add(callingObject);
          } else {
            totalCallingCode.add(callingObject);
          }
        }
          if(initialCallingCode.size() <= 0){
            JsonObject initialCallingObject = new JsonObject();
            initialCallingObject.put("calling_code", " ");
            initialCallingObject.put("region_code", " ");
            initialCallingObject.put("country_name"," ");
            initialCallingCode.add(initialCallingObject);
          }

        dataObject.put("initial_callingcode",initialCallingCode);
        dataObject.put("total_callingcode", totalCallingCode);
      }
      resultSetArray.add(dataObject);
      getSuccessObject(resultSetArray, routingContext.response(), "GET");
      connectionObject.close();
    } catch (SQLException e) {
      logger.error("Sql Exception while updating Organization");
      JsonObject jsonResponseObject = new JsonObject();
      JsonObject jsonFailureObject = new JsonObject();
      jsonFailureObject.put("code", HttpStatus.SC_CONFLICT);
      jsonFailureObject.put("message", "Invalid Data");
      jsonFailureObject.put("description", e.getMessage());
      jsonResponseObject.put("error", jsonFailureObject);
    } catch (ClassNotFoundException e) {
      logger.error("ClassNotFoundException while updating Organization");
      JsonObject jsonResponseObject = new JsonObject();
      JsonObject jsonFailureObject = new JsonObject();
      jsonFailureObject.put("code", HttpStatus.SC_CONFLICT);
      jsonFailureObject.put("message", "Invalid Data");
      jsonFailureObject.put("description", e.getMessage());
      jsonResponseObject.put("error", jsonFailureObject);
    } catch (IllegalArgumentException e) {
      logger.error("IllegalArgumentException while updating Organization");
      JsonObject jsonResponseObject = new JsonObject();
      JsonObject jsonFailureObject = new JsonObject();
      jsonFailureObject.put("code", HttpStatus.SC_CONFLICT);
      jsonFailureObject.put("message", "Invalid Data");
      jsonFailureObject.put("description", e.getMessage());
      jsonResponseObject.put("error", jsonFailureObject);
      ResponseBuilder.writeInternalServerError(routingContext.response());
    }
  }

  /**
   * To get number of interview process,candidates,jobs,employees for dashboard display
   *
   * @param routingContext
   * @author Pradeep Sudhakaran
   */
  public void getOrganizationCount(RoutingContext routingContext) {
    String queryCount = "";
    Connection connection = null;
    int jobCurrentCount = 0;
    int jobPreviousCount = 0;
    int jobTotalCount = 0;
    int interviewPreviousCount = 0;
    int interviewCurrentCount = 0;
    int candidatePreviousCount = 0;
    int candidateCurrentCount = 0;
    int candidateTotalCount = 0;
    int offerExtendedPreviousCount = 0;
    int offerAcceptedPreviousCount = 0;
    int offerExtendedCurrentCount = 0;
    int offerAcceptedCurrentCount = 0;
    String interviewStatus = "";
    String candidateStatus = "";
    String jobStatus = "";
    String offerStatus = "";
    DbHandle dbHandler = null;
    Organization organization = null;
    try {
      connection = new MysqldbConf().getConnection();
      dbHandler = DbHelper.getDBHandle(routingContext, false);

      organization = new OrganizationDAOImpl(dbHandler).
          getOrganizationById(Long.parseLong(routingContext.request().params().get("organization_id")));
      boolean doesOrganizationExists = (organization != null);
      int id = Integer.parseInt(routingContext.request().params().get("organization_id"));
      String selectSQL = "select count(1) from Job where organization_id= ?";
      PreparedStatement preparedStatement1 = connection.prepareStatement(selectSQL);
      preparedStatement1.setInt(1, id);
      ResultSet rs = preparedStatement1.executeQuery();
      int jobCount = 0;
      while (rs.next()) {
        jobCount = rs.getInt(1);
      }

      if (jobCount == 0 && doesOrganizationExists) {
        JsonObject dataObject = new JsonObject();
        dataObject.put("interview_current_count", 0);
        dataObject.put("interview_difference_count", 0);
        dataObject.put("interview_difference", "equal");
        dataObject.put("candidate_total_count", 1);
        dataObject.put("candidate_difference_count", 1);
        dataObject.put("candidate_difference", "increasing");
        dataObject.put("job_total_count", 1);
        dataObject.put("job_difference_count", 1);
        dataObject.put("job_difference", "increasing");
        dataObject.put("offer_current_percentage", 0.0);
        dataObject.put("offer_difference", "equal");
        JsonObject jsonSuccessObject = new JsonObject();
        jsonSuccessObject.put("code", HttpStatus.SC_OK);
        jsonSuccessObject.put("success", true);
        jsonSuccessObject.put("data", dataObject);
        jsonSuccessObject.put("no_data", false);

        routingContext.response()
            .setStatusCode(HttpStatus.SC_OK)
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .end(jsonSuccessObject.toString());

        return;
      }
    } catch (SQLException e) {
      DbHelper.safeRollback(dbHandler);
      logger.error(e.getMessage());
      ResponseHelper.writeInternalServerError(routingContext.response());
      return;
    } catch (DbHandleException e) {
      DbHelper.safeRollback(dbHandler);
      logger.error(e.getMessage());
      ResponseHelper.writeInternalServerError(routingContext.response());
      return;
    } catch (ClassNotFoundException e) {
      logger.error(e.getMessage());
      DbHelper.safeRollback(dbHandler);
      ResponseHelper.writeInternalServerError(routingContext.response());
      return;
    } finally {
      if (null != dbHandler) {
        dbHandler.checkAndCloseConnection();
      }
    }


    try {
      Calendar calendar = Calendar.getInstance();
      int month = calendar.get(Calendar.MONTH) + 1;
      int year = calendar.get(Calendar.YEAR);

      int previousMonth = 0;
      int previousYear = 0;

      if (month == 1) {
        previousMonth = 12;
        previousYear = year - 1;
      } else {
        previousMonth = month - 1;
        previousYear = year;
      }
      String monthString = "";
      String previousMonthString = "";
      if (month < 10) {
        monthString = "0" + month;
        previousMonthString = "0" + previousMonth;
      } else if (month == 10) {
        monthString = String.valueOf(month);
        previousMonthString = "0" + previousMonth;
      } else {
        monthString = String.valueOf(month);
        previousMonthString = String.valueOf(previousMonth);
      }

      String currentFromDate = year + "-" + monthString + "-01";
      String currentToDate = year + "-" + monthString + "-31";
      String previousFromDate = year + "-" + previousMonthString + "-01";
      String previousToDate = year + "-" + previousMonthString + "-31";

      final String id = routingContext.request().getParam("organization_id");
      if (id.length() == 0) {
        throw new NoSuchFieldException("Required parameters missing");
      }

      int organizationId = Integer.valueOf(id);
      queryCount = "select interview.interview_previous_count,interview.interview_current_count," +
          "candidate.candidate_current_count,candidate.candidate_previous_count," +
          "candidate.candidate_total_count," +
          "job.job_previous_count,job.job_current_count,job.job_total_count " +
          "from (select count(case when InterviewProcessCandidate.interview_date >= ? " +
          " and InterviewProcessCandidate.interview_date <= ? then 1 else null end) " +
          "as interview_previous_count," +
          "count(case when InterviewProcessCandidate.interview_date >= ? " +
          " and InterviewProcessCandidate.interview_date <= ? then 1 else null end) " +
          "as interview_current_count " +
          " from InterviewProcessCandidate,Candidate " +
          "where InterviewProcessCandidate.candidate_id =Candidate.candidate_id " +
          "and Candidate.organization_id = ? and InterviewProcessCandidate.active_status = 1 " +
          "and Candidate.active_status = 1) interview," +
          "(select count(case when Candidate.created >= ? " +
          " and  Candidate.created <= ? then 1 else null end) " +
          "as candidate_previous_count," +
          "count(case when Candidate.created >= ? " +
          " and  Candidate.created <= ? then 1 else null end) " +
          "as candidate_current_count," +
          " count(Candidate.candidate_id) as candidate_total_count " +
          " from Candidate where organization_id = ? and active_status = 1 and status != 'Not Hired'" +
        //  " and status != 'Hire') candidate," +
          " ) candidate," +
          "(select count(case when STR_TO_DATE(Job.date_posted, '%m/%d/%Y' ) >= ? " +
          "and STR_TO_DATE(Job.date_posted, '%m/%d/%Y' ) <= ? then id else null end) as job_previous_count," +
          "count(case when STR_TO_DATE(Job.date_posted, '%m/%d/%Y' ) >= ? " +
          "and STR_TO_DATE(Job.date_posted, '%m/%d/%Y' ) <= ? then id else null end) as job_current_count, " +
          "count(case when job_status = 'Published' then 1 else null end) as job_total_count" +
          " from Job where organization_id =? and job_active_status = 1 and job_archive_status = 0) job";

      connection = new MysqldbConf().getConnection();
      PreparedStatement preparedStatement = connection.prepareStatement(queryCount);
      preparedStatement.setString(1, previousFromDate);
      preparedStatement.setString(2, previousToDate);
      preparedStatement.setString(3, currentFromDate);
      preparedStatement.setString(4, currentToDate);
      preparedStatement.setInt(5, organizationId);
      preparedStatement.setString(6, previousFromDate);
      preparedStatement.setString(7, previousToDate);
      preparedStatement.setString(8, currentFromDate);
      preparedStatement.setString(9, currentToDate);
      preparedStatement.setInt(10, organizationId);
      preparedStatement.setString(11, previousFromDate);
      preparedStatement.setString(12, previousToDate);
      preparedStatement.setString(13, currentFromDate);
      preparedStatement.setString(14, currentToDate);
      preparedStatement.setInt(15, organizationId);

      ResultSet resultSet = preparedStatement.executeQuery();

      while (resultSet.next()) {
        interviewPreviousCount = resultSet.getInt("interview_previous_count");
        interviewCurrentCount = resultSet.getInt("interview_current_count");
        candidateCurrentCount = resultSet.getInt("candidate_current_count");
        candidatePreviousCount = resultSet.getInt("candidate_previous_count");
        candidateTotalCount = resultSet.getInt("candidate_total_count");
        jobPreviousCount = resultSet.getInt("job.job_previous_count");
        jobCurrentCount = resultSet.getInt("job_current_count");
        jobTotalCount = resultSet.getInt("job_total_count");
      }

      String offerQueryCount = "select offer.offer_extended_current_count," +
          "offer.offer_accepted_current_count,offer.offer_extended_previous_count," +
          "offer.offer_accepted_previous_count " +
          "from (select count(case when OfferProcessCandidate.created >= ? " +
          "and OfferProcessCandidate.created <= ? then 1 else null end) " +
          "as offer_extended_current_count, " +
          "count(case when OfferProcessCandidate.created >= ? " +
          "and OfferProcessCandidate.created <= ? " +
          "and OfferProcessCandidate.status= 'accept' then 1 else null end) " +
          "as offer_accepted_current_count," +
          "count(case when OfferProcessCandidate.created >= ?" +
          " and OfferProcessCandidate.created <= ? then 1 else null end) " +
          "as offer_extended_previous_count,count(case when OfferProcessCandidate.created >= ? " +
          "and OfferProcessCandidate.created <= ? " +
          "and OfferProcessCandidate.status= 'accept' then 1 else null end) " +
          "as offer_accepted_previous_count from OfferProcessCandidate,Candidate " +
          "where Candidate.candidate_id = OfferProcessCandidate.candidate_id and Candidate.organization_id = ?) offer";

      PreparedStatement offerPrepareStatement = connection.prepareStatement(offerQueryCount);
      offerPrepareStatement.setString(1, (year) + "-01-01");
      offerPrepareStatement.setString(2, (year) + "-12-31");
      offerPrepareStatement.setString(3, (year) + "-01-01");
      offerPrepareStatement.setString(4, (year) + "-12-31");
      offerPrepareStatement.setString(5, (year - 1) + "-01-01");
      offerPrepareStatement.setString(6, (year - 1) + "-12-31");
      offerPrepareStatement.setString(7, (year - 1) + "-01-01");
      offerPrepareStatement.setString(8, (year - 1) + "-12-31");
      offerPrepareStatement.setInt(9, organizationId);

      ResultSet offerResultSet = offerPrepareStatement.executeQuery();
      while (offerResultSet.next()) {
        offerExtendedCurrentCount = offerResultSet.getInt("offer_extended_current_count");
        offerAcceptedCurrentCount = offerResultSet.getInt("offer_accepted_current_count");
        offerExtendedPreviousCount = offerResultSet.getInt("offer_extended_previous_count");
        offerAcceptedPreviousCount = offerResultSet.getInt("offer_accepted_previous_count");
      }

      int interviewDifference = interviewCurrentCount - interviewPreviousCount;
      int candidateDifference = candidateCurrentCount - candidatePreviousCount;
      int jobDifference = jobCurrentCount - jobPreviousCount;

      float offerCurrentPercentage = 0;
      float offerPreviousPercentage = 0;

      if (offerAcceptedCurrentCount > 0 && offerExtendedCurrentCount > 0) {
        offerCurrentPercentage = ((float) offerAcceptedCurrentCount
            / (float) offerExtendedCurrentCount) * 100;
      }

      if (offerAcceptedPreviousCount > 0 && offerExtendedPreviousCount > 0) {
        offerPreviousPercentage = ((float) offerAcceptedPreviousCount
            / (float) offerExtendedPreviousCount) * 100;
      }

      DecimalFormat decimalFormat = new DecimalFormat("###.##");

      float offerDifference = offerCurrentPercentage - offerPreviousPercentage;

      if (interviewDifference < 0) {
        interviewStatus = "decreasing";
      } else if (interviewDifference == 0) {
        interviewStatus = "equal";
      } else {
        interviewStatus = "increasing";
      }

      String candidateString = "";
      if (candidateDifference < 0) {
        candidateStatus = "decreasing";
        candidateString = String.valueOf(candidateDifference);
      } else if (candidateDifference == 0) {
        candidateStatus = "equal";
        candidateStatus = String.valueOf(candidateDifference);
      } else {
        candidateStatus = "increasing";
      }

      if (jobDifference < 0) {
        jobStatus = "decreasing";
      } else if (jobDifference == 0) {
        jobStatus = "equal";
      } else {
        jobStatus = "increasing";
      }

      if (offerDifference < 0.0) {
        offerStatus = "decreasing";
      } else if (offerDifference == 0.0) {
        offerStatus = "equal";
      } else {
        offerStatus = "increasing";
      }

      if (null != preparedStatement && preparedStatement.isClosed()) {
        preparedStatement.close();
      }

      if (null != resultSet && resultSet.isClosed()) {
        resultSet.close();
      }

      if (null != offerResultSet && offerResultSet.isClosed()) {
        offerResultSet.close();
      }

      if (null != offerPrepareStatement && offerPrepareStatement.isClosed()) {
        offerPrepareStatement.close();
      }

      if (null != connection && connection.isClosed()) {
        connection.close();
      }

      JsonObject jsonDataObject = new JsonObject();
      JsonObject jsonSuccessObject = new JsonObject();
      jsonDataObject.put("interview_current_count", interviewCurrentCount);
      jsonDataObject.put("interview_difference_count", interviewDifference);
      jsonDataObject.put("interview_difference", interviewStatus);
      jsonDataObject.put("candidate_total_count", candidateTotalCount);
      jsonDataObject.put("candidate_difference_count", candidateDifference);
      jsonDataObject.put("candidate_difference", candidateStatus);
      jsonDataObject.put("job_total_count", jobTotalCount);
      jsonDataObject.put("job_difference_count", jobDifference);
      jsonDataObject.put("job_difference", jobStatus);

      boolean isDataAvailable = ((interviewCurrentCount == 0) && (interviewDifference == 0) &&
          ("equal".equals(interviewStatus)) && (candidateTotalCount == 0) &&
          (candidateDifference == 0) && ("0".equals(candidateStatus)) &&
          (jobTotalCount == 0) && (jobDifference == 0) && ("equal".equals(jobStatus)));


      if (offerCurrentPercentage == 0.0) {
        jsonDataObject.put("offer_current_percentage", offerCurrentPercentage);
        jsonDataObject.put("offer_current_percentage", offerPreviousPercentage);
      } else {
        jsonDataObject.put("offer_current_percentage", decimalFormat.format(offerCurrentPercentage));
        jsonDataObject.put("offer_previous_percentage", decimalFormat.format(offerPreviousPercentage));
      }
      jsonDataObject.put("offer_difference", offerStatus);
      jsonSuccessObject.put("code", HttpStatus.SC_OK);
      jsonSuccessObject.put("success", true);
      jsonSuccessObject.put("data", jsonDataObject);
      jsonSuccessObject.put("no_data", (isDataAvailable));

      routingContext.response()
          .setStatusCode(HttpStatus.SC_OK)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonSuccessObject.toString());

    } catch (NoSuchFieldException exception) {
      logger.error(exception.getMessage());
      JsonObject jsonResponseObject = new JsonObject();
      JsonObject jsonFailureObject = new JsonObject();
      jsonFailureObject.put("code", HttpStatus.SC_BAD_REQUEST);
      jsonFailureObject.put("message", "Bad Request");
      jsonFailureObject.put("description", exception.getMessage());
      jsonResponseObject.put("error", jsonFailureObject);

      routingContext.response()
          .setStatusCode(HttpStatus.SC_BAD_REQUEST)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonResponseObject.toString());

    } catch (SQLException exception) {
      logger.error(exception.getMessage());
      ResponseBuilder.writeInternalServerError(routingContext.response());
    } catch (ClassNotFoundException exception) {
      logger.error(exception.getMessage());
      ResponseBuilder.writeInternalServerError(routingContext.response());
    }
  }

  //deprecated
  /*public void addJobDescription(int organizationId) throws SQLException, ClassNotFoundException {
    Connection connection = new MysqldbConf().getConnection();
    String insertQuery = "insert into JobDescription (jobdesctype,jobdescdetails,organization_id) " +
        "values (?,?,?)";
    String jobDetails = "<h4>Job brief</h4><p>We are looking for an Android Developer who " +
        "possesses a passion for pushing mobile technologies to the limits.</p> <p>This Android " +
        "app developer will work with our team of talented engineers to design and build the next " +
        "generation of our mobile applications.</p><p>Android programming works closely with other " +
        "app development and technical teams.</p><h4>Responsibilities</h4><ul><li>Design and build " +
        "advanced applications for the Android platform.</li><li>Collaborate with cross-functional " +
        " to define, design, and ship new features.</li><li>Work with outside data sources and APIs." +
        "Unit-test code for robustness, including edge cases, usability, and general reliability." +
        "</li><li>Work on bug fixing and improving application performance.</li><li>Continuously " +
        "discover, evaluate, and implement new technologies to maximize development efficiency." +
        "</li></ul><h4>Requirements</h4><p>BS/MS degree in Computer Science, Engineering or a " +
        "related subject.</p><p>Proven software development experience and Android skills " +
        "development.</p><p>Proven working experience in Android app development.</p><p>Have " +
        "published at least one original Android app.</p><p>Experience with Android SDK.</p><p>" +
        "Experience working with remote data via REST and JSON.</p><p>Experience with third-party " +
        "libraries and APIs.Working knowledge of the general mobile landscape, architectures, " +
        "trends, and emerging technologies.Solid understanding of the full mobile development " +
        "life cycle.</p>";
    String jobType = "Android Developer (Sample)";

    PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
    preparedStatement.setString(1, jobType);
    preparedStatement.setString(2, jobDetails);
    preparedStatement.setInt(3, organizationId);

    int jobDescId = preparedStatement.executeUpdate();

    preparedStatement.close();
    if (jobDescId == 0) {
      throw new SQLException("Record not inserted");
    }
    connection.close();
  }*/

  public void uploadOrganizationKeyCertFile(RoutingContext routingContext) {
    try {
      Set<FileUpload> fileUploadSet = routingContext.fileUploads();
      for (FileUpload fileUpload : fileUploadSet) {
        FileTypeEnum fileTypeEnum = FileHelper.getFileType(fileUpload);
        long fileSize = fileUpload.size() / 1024 / 1024;

        if (fileTypeEnum != null && FileUploadHelper.isSupportedKeyCrtFormat(fileTypeEnum)) {
          if (fileSize < 10) {
            try {
              String sourceFile = fileUpload.uploadedFileName();
              String destinationFile = fileUpload.uploadedFileName().split(File.separator)[1] +
                  fileTypeEnum.getExtension();
              String destinationFolderPath = EndpointConstant.publicStaticImage + File.separator +
                  routingContext.request().getParam("organization_id");

              FileHelper.move(sourceFile, destinationFolderPath, destinationFile);
              routingContext.response()
                  .setStatusCode(HttpStatus.SC_CREATED)  // 201
                  .putHeader("Content-Type", "application/json; charset=utf-8")
                  .end(destinationFolderPath + File.separator + destinationFile);
            } catch (Exception e) {
              routingContext.response()
                  .setStatusCode(HttpStatus.SC_NOT_ACCEPTABLE)  // 201
                  .putHeader("Content-Type", "application/json; charset=utf-8")
                  .end("Large File Size, Attach file with less Size!");
              logger.error("Large File Size,Attach file with less Size!");
            }
          } else {
            routingContext.response()
                .setStatusCode(HttpStatus.SC_NOT_ACCEPTABLE)  // 201
                .putHeader("Content-Type", "application/json; charset=utf-8")
                .end("Large File Size, Attach file with less Size!");
            logger.error("Large File Size,Attach file with less Size!");
          }
        } else {
          routingContext.response()
              .setStatusCode(HttpStatus.SC_NOT_ACCEPTABLE)  // 201
              .putHeader("Content-Type", "application/json; charset=utf-8")
              .end("You can only upload KEYSTORE file!");
          logger.error("You can only upload KEYSTORE file!");
        }
      }
    } catch (IllegalArgumentException e) {
      routingContext.response()
          .setStatusCode(HttpStatus.SC_NOT_ACCEPTABLE)  // 201
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end("You can only upload KEYSTORE file!");
      logger.error("You can only upload KEYSTORE file!");
    }
  }

  public void uploadOrganizationCertFile(RoutingContext routingContext) {
    try {
      Set<FileUpload> fileUploadSet = routingContext.fileUploads();
      for (FileUpload fileUpload : fileUploadSet) {
        FileTypeEnum fileTypeEnum = FileHelper.getFileType(fileUpload);
        long fileSize = fileUpload.size() / 1024 / 1024;

        if (fileTypeEnum != null && FileUploadHelper.isSupportedCrtFormat(fileTypeEnum)) {
          if (fileSize < 10) {
            try {
              String sourceFile = fileUpload.uploadedFileName();
              String destinationFile = fileUpload.uploadedFileName().split(File.separator)[1] +
                  fileTypeEnum.getExtension();
              String destinationFolderPath = EndpointConstant.publicStaticImage + File.separator +
                  routingContext.request().getParam("organizationId");

              FileHelper.move(sourceFile, destinationFolderPath, destinationFile);
              routingContext.response()
                  .setStatusCode(HttpStatus.SC_CREATED)  // 201
                  .putHeader("Content-Type", "application/json; charset=utf-8")
                  .end(destinationFolderPath + File.separator + destinationFile);
            } catch (Exception e) {
              routingContext.response()
                  .setStatusCode(HttpStatus.SC_NOT_ACCEPTABLE)  // 201
                  .putHeader("Content-Type", "application/json; charset=utf-8")
                  .end("Large File Size, Attach file with less Size");
            }
          } else {
            routingContext.response()
                .setStatusCode(HttpStatus.SC_NOT_ACCEPTABLE)  // 201
                .putHeader("Content-Type", "application/json; charset=utf-8")
                .end("Large File Size, Attach file with less Size");
          /*  ResponseHelper.
                writeStringErrorResponse("Large File Size, Attach file with less Size"
                    , routingContext.response());*/
            logger.error("Large File Size Attach file with less Size!");
          }
        } else {
          routingContext.response()
              .setStatusCode(HttpStatus.SC_NOT_ACCEPTABLE)  // 201
              .putHeader("Content-Type", "application/json; charset=utf-8")
              .end("You can only upload CERTIFICATE file!");
          logger.error("You can only upload CERTIFICATE file!");
        }
      }
    } catch (IllegalArgumentException e) {
      routingContext.response()
          .setStatusCode(HttpStatus.SC_NOT_ACCEPTABLE)  // 201
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end("You can only upload CERTIFICATE file!");
      logger.error("You can only upload CERTIFICATE file!");
    }
  }

  public String generateToken(Login loginObject) throws UnsupportedEncodingException {
    String generatedToken = "";
    java.sql.Timestamp expiryTime = new Timestamp(System.currentTimeMillis() + 30 * 60 * 1000);
    String key = JWTUtil.getEncryptKey();
    Algorithm algorithm = Algorithm.HMAC256(key);
    generatedToken = JWT.create().withIssuer("auth0").withExpiresAt(expiryTime)
        .withClaim("organization_id", String.valueOf(loginObject.getOrganizationId()))
        .withClaim("user_id", String.valueOf(loginObject.getId()))
        .withClaim("user_type", loginObject.getUserType())
        .sign(algorithm);
    return generatedToken;
  }

}
