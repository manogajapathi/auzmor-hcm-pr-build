package com.auzmor.impl.handler;

import com.auzmor.bo.FieldOptionsBO;
import com.auzmor.impl.bo.FieldOptionsBOImpl;
import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.enumarator.FieldOptionsEnum;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.FieldOptions.DivisionModel;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.validator.FieldOptions;
import com.auzmor.util.validator.Validator;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FieldOptionsHandler {
  private static final Logger logger = LoggerFactory.getLogger(FieldOptionsHandler.class);
  public static void getAllFieldOptions(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    DivisionModel divisionModel = new DivisionModel();

    String[] allowedParams = {"organization_id","fields"};
    String[] requiredParams = {"organization_id","fields"};
    String[] fieldOptionsParams = {"organization_id","fields"};
    
    List<Validator> validators = new ArrayList<>();
    Validator fieldValidator = new ValidatorImpl(fieldOptionsParams, new HashMap<String, FieldOptions>() {{
      put("fields", FieldOptionsCommon.ANY);
      put("organization_id", FieldOptionsCommon.NUMBER);
    }});
    validators.add(fieldValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          FieldOptionsBO fieldOptionsBO = new FieldOptionsBOImpl();
          ReturnObject<FieldOptionsEnum, JsonArray> boResponse =
              fieldOptionsBO.getFieldOptionValues(dbHandle,requestParams);
          switch (boResponse.getStatusEnum()) {
            case VALUE_SUCCESS:
              dbHandle.commit();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  "", boResponse.getData(), httpServerResponse);
              break;
            case NO_SUCH_VALUE:
              dbHandle.commit();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  "No tasks assigned",boResponse.getData() , httpServerResponse);
              break;
            default:
              dbHandle.rollback();
              ResponseHelper.writeInternalServerError(httpServerResponse);
              break;
          }
        });
  }
}
