package com.auzmor.impl.handler;

import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.model.Job.Collaborator;
import com.auzmor.impl.model.Job.CustomQuestion;
import com.auzmor.impl.model.Job.Job;

import com.auzmor.impl.util.StringUtil;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.commons.dbutils.DbUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

import java.util.*;

public class JobPostHandler {

  /**
   * getHiringProcess - Post Hiring Process Details for a Job
   *
   * @param jobModel
   * @return Job Object
   * @throws Exception
   * @author Pradeep Balasubramanian
   * modified by ${date}
   */

  private static final Logger logger = LoggerFactory.getLogger(JobPostHandler.class);

  public Job getHiringProcess(Job jobModel) throws DecodeException {
    String applicantDBValue = StringUtil.convertJsonArrayToDBString(jobModel.getApplicant());
    String interviewDBValue = StringUtil.convertJsonArrayToDBString(jobModel.getInterview());
    String offerDBValue = StringUtil.convertJsonArrayToDBString(jobModel.getOffer());
    String hireDBValue = StringUtil.convertJsonArrayToDBString(jobModel.getHire());
    String notHiredDBValue = StringUtil.convertJsonArrayToDBString(jobModel.getNot_hired());
    String screeningDBValue = StringUtil.convertJsonArrayToDBString(jobModel.getScreening());

    if (applicantDBValue != null && applicantDBValue.length() > 0) {
      jobModel.setApplicant("New ::" + applicantDBValue);
    } else {
      jobModel.setApplicant("New");
    }

    if(screeningDBValue != null && screeningDBValue.length() > 0) {
      jobModel.setScreening("Screening::" + screeningDBValue);
    } else {
      jobModel.setScreening("Screening");
    }

    if (interviewDBValue != null && interviewDBValue.length() > 0) {
      jobModel.setInterview("Schedule Interview::" + interviewDBValue);
    } else {
      jobModel.setInterview("Schedule Interview");
    }

    if (offerDBValue != null && offerDBValue.length() > 0) {
      jobModel.setOffer("Offer Extended::" + offerDBValue);
    } else {
      jobModel.setOffer("Offer Extended");
    }

    if (hireDBValue != null && hireDBValue.length() > 0) {
      jobModel.setHire("Hire Candidate::" + hireDBValue);
    } else {
      jobModel.setHire("Hire Candidate");
    }

    if(StringUtil.printValidString(notHiredDBValue).equals("") && notHiredDBValue.length() == 0){
      jobModel.setNot_hired("Archive to Talent Pool::Declined offer::" +
          "Not Qualified / Knockout::Over Qualified::Took another position::Blacklisted");
    }

    logger.info("Hiring Lead values added to Job Model");

    return jobModel;
  }


  public List<CustomQuestion> getCustomQuestions(Map<String, String> customQuestionMap) throws DecodeException {
    String customQuestionObjectString = customQuestionMap.get("custom_question");
    String customAnswerObjectString = customQuestionMap.get("custom_answer");
    String customKnockoutObjectString = customQuestionMap.get("custom_knockout");
    String customOption1ObjectString = customQuestionMap.get("custom_option1");
    String customOption2ObjectString = customQuestionMap.get("custom_option2");
    String customQuestionRequired = customQuestionMap.get("custom_question_required");


    JsonArray customQuestionArray = new JsonArray(customQuestionObjectString);
    JsonArray customAnswerArray = new JsonArray(customAnswerObjectString);
    JsonArray customKnockoutArray = new JsonArray(customKnockoutObjectString);
    JsonArray customOption1Array = new JsonArray(customOption1ObjectString);
    JsonArray customOption2Array = new JsonArray(customOption2ObjectString);
    JsonArray customQuestionRqdArray = new JsonArray(customQuestionRequired);

    List<CustomQuestion> customQuestions = new ArrayList<>();
    for (int i = 0; i < customQuestionArray.size(); i++) {
      CustomQuestion customQuestionObject = new CustomQuestion();
      customQuestionObject.setCustomQuestion(customQuestionArray.getString(i));
      customQuestionObject.setCustomAnswer(customAnswerArray.getString(i));
      customQuestionObject.setCustomKnockout(customKnockoutArray.getBoolean(i));
      customQuestionObject.setCustomOption1(customOption1Array.getString(i));
      customQuestionObject.setCustomOption2(customOption2Array.getString(i));
      customQuestionObject.setCustomQuestionRequired(customQuestionRqdArray.getBoolean(i));
      customQuestions.add(customQuestionObject);
    }

    return customQuestions;

  }

  /**
   * getCollaborators - Get Collaborators for Job Insertion
   *
   * @param collaboratorMap
   * @return List<Collaborator>
   * @author Pradeep Balasubramanian
   */
  public List<Collaborator> getCollaborators(Map<String, String> collaboratorMap) {
    String collaboratorObjectString = collaboratorMap.get("collaborators");
    List<Collaborator> collaborators = new ArrayList<>();
    JsonArray collaboratorArray = new JsonArray(collaboratorObjectString);
    for (int i = 0; i < collaboratorArray.size(); i++) {
      String collaborator = collaboratorArray.getString(i);
      Collaborator collaboratorObject = new Collaborator();
      collaboratorObject.setCollaborator(collaborator);
      collaborators.add(collaboratorObject);
    }

    return collaborators;
  }

  /**
   * addJobDetails - Add Job Items required to be posted
   *
   * @param jobModel
   * @return integer
   * @throws Exception
   * @author Pradeep Balasubramanian
   * Modified at ${date}
   */

  public int addJobDetails(Job jobModel)
      throws NullPointerException,SQLException,ClassNotFoundException {

    Connection connectionObject = null;
    try {
      connectionObject = new MysqldbConf().getConnection();
      connectionObject.setAutoCommit(false);

      String state = StringUtil.printValidString(jobModel.getState());

      if (state != "" || state.length() > 0) {
        if (state.contains("'")) {
          state = state.replace("'", "");
        }
      }

      Map<String, Object> insertionObjects = new LinkedHashMap<>();
      StringUtil.checkRequiredFields(insertionObjects, "title", jobModel.getTitle());
      StringUtil.checkRequiredFields(insertionObjects, "location", jobModel.getLocation());
      StringUtil.checkRequiredFields(insertionObjects, "job_status", jobModel.getJob_status());
      StringUtil.checkRequiredFields(insertionObjects, "job_description", jobModel.getJob_description());
      insertionObjects.put("job_creator_id", jobModel.getJob_creator_id());
      insertionObjects.put("internal_job_code", StringUtil.printValidString(jobModel.getInternal_job_code()));
      insertionObjects.put("department", StringUtil.printValidString(jobModel.getDepartment()));
      insertionObjects.put("minimum_experience", StringUtil.printValidString(jobModel.getMinimum_experience()));
      insertionObjects.put("compensation", StringUtil.printValidString(jobModel.getCompensation()));
      insertionObjects.put("job_desc_template", StringUtil.printValidString(jobModel.getJob_desc_template()));
      insertionObjects.put("hiring_lead", StringUtil.printValidString(jobModel.getHiring_lead()));
      insertionObjects.put("employment_type", StringUtil.printValidString(jobModel.getEmployment_type()));
      insertionObjects.put("city", StringUtil.printValidString(jobModel.getCity()));
      insertionObjects.put("state", StringUtil.printValidString(state));
      insertionObjects.put("zip", StringUtil.printValidString(jobModel.getZip()));
      insertionObjects.put("country", StringUtil.printValidString(jobModel.getCountry()));
      insertionObjects.put("resume", true);
      insertionObjects.put("resume_required", true);
      insertionObjects.put("date_available", jobModel.isDate_available());
      insertionObjects.put("date_available_required", jobModel.isDate_available_required());
      insertionObjects.put("cover_letter", jobModel.isCover_letter());
      insertionObjects.put("cover_letter_required", jobModel.isCover_letter_required());
      insertionObjects.put("highest_education", jobModel.isHighestEducation());
      insertionObjects.put("highest_education_required", jobModel.isHighest_education_required());
      insertionObjects.put("reference", jobModel.isReference());
      insertionObjects.put("reference_required", jobModel.isReference_required());
      insertionObjects.put("desired_salary", jobModel.isDesired_salary());
      insertionObjects.put("desired_salary_required", jobModel.isDesired_salary_required());
      insertionObjects.put("college", jobModel.isCollege());
      insertionObjects.put("college_required", jobModel.isCollege_required());
      insertionObjects.put("referred_by", jobModel.isReferred_by());
      insertionObjects.put("referred_by_required", jobModel.isReferred_by_required());
      insertionObjects.put("linkedin_url", jobModel.isLinkedin_url());
      insertionObjects.put("linkedin_url_required", jobModel.isLinkedin_url_required());
      insertionObjects.put("blog", jobModel.isBlog());
      insertionObjects.put("blog_required", jobModel.isBlog_required());
      insertionObjects.put("company_type_check", jobModel.isCompany_type_check());
      insertionObjects.put("company_type", StringUtil.printValidString(jobModel.getCompany_type()));
      insertionObjects.put("post_permission", jobModel.isPost_permission());
      insertionObjects.put("date_posted", jobModel.getDate_posted());
      insertionObjects.put("job_open_date", jobModel.getJob_open_date());
      insertionObjects.put("organization_id", jobModel.getOrganization_id());
      insertionObjects.put("interview", jobModel.getInterview());
      insertionObjects.put("offer", jobModel.getOffer());
      insertionObjects.put("hire", jobModel.getHire());
      insertionObjects.put("screening", jobModel.getScreening());
      insertionObjects.put("applicant", jobModel.getApplicant());
      insertionObjects.put("not_hired", jobModel.getNot_hired());
      insertionObjects.put("currency_type", StringUtil.printValidString(jobModel.getCurrency_type()));
      insertionObjects.put("collab_resume", jobModel.isCollab_resume());
      insertionObjects.put("collab_date_available", jobModel.isCollab_date_available());
      insertionObjects.put("collab_cover_letter", jobModel.isCollab_cover_letter());
      insertionObjects.put("collab_highest_education", jobModel.isCollab_highest_education());
      insertionObjects.put("collab_reference", jobModel.isCollab_reference());
      insertionObjects.put("collab_desired_salary", jobModel.isCollab_desired_salary());
      insertionObjects.put("collab_college", jobModel.isCollab_college());
      insertionObjects.put("collab_referred_by", jobModel.isCollab_referred_by());
      insertionObjects.put("collab_linkedin_url", jobModel.isCollab_linkedin_url());
      insertionObjects.put("collab_blog", jobModel.isCollab_blog());
//      insertionObjects.put("job_active_status", jobModel.isJob_active_status());

      String columnValueString = "";
      String entryValueString = "";

      for (Map.Entry<String, Object> columnEntries : insertionObjects.entrySet()) {
        columnValueString += columnEntries.getKey() + ",";
        entryValueString += "?" + ",";
      }
      columnValueString = columnValueString.substring(0, columnValueString.length() - 1);
      entryValueString = entryValueString.substring(0, entryValueString.length() - 1);

      String sqlQueryForJob = "insert into Job(" + columnValueString + ") " +
          "values(" + entryValueString + ")";

      PreparedStatement preparedStatement = connectionObject.prepareStatement(sqlQueryForJob,
          Statement.RETURN_GENERATED_KEYS);

      int statementIterator = 0;
      for (Map.Entry<String, Object> columnEntries : insertionObjects.entrySet()) {
        Object insertionObject = columnEntries.getValue();
        if (insertionObject instanceof Boolean) {
          preparedStatement.setBoolean(++statementIterator,
              Boolean.valueOf(insertionObject.toString()));
        } else if (insertionObject instanceof Integer) {
          preparedStatement.setInt(++statementIterator,
              Integer.valueOf(insertionObject.toString()));
        } else {
          preparedStatement.setString(++statementIterator, insertionObject.toString());
        }
      }

      preparedStatement.executeUpdate();

      ResultSet rs = preparedStatement.getGeneratedKeys();

      int jobId = 0;

      if (rs.next()) {
        jobId = rs.getInt(1);
      }
      rs.close();
      preparedStatement.close();

      logger.info("Job ID " + jobId + " generated. ");

      List<CustomQuestion> customQuestionList = jobModel.getCustomQuestionModels();
      List<Collaborator> collaboratorList = jobModel.getCollaborators();

      if (customQuestionList != null && customQuestionList.size() > 0) {
        for (int j = 0; j < customQuestionList.size(); j++) {
          CustomQuestion customQuestion = customQuestionList.get(j);
          PreparedStatement customQuestionPreparedStatement = generatePreparedStatement(
              customQuestion,jobId,connectionObject);
          customQuestionPreparedStatement.executeUpdate();
          customQuestionPreparedStatement.close();
        }
        logger.info("Custom Questions added to Job. ");
      }

      if (collaboratorList != null && collaboratorList.size() > 0) {
        for (int k = 0; k < collaboratorList.size(); k++) {
          Collaborator collaborator = collaboratorList.get(k);
          PreparedStatement collaboratorStatement = generatePreparedStatement(
              collaborator,jobId,connectionObject);
          collaboratorStatement.executeUpdate();
          collaboratorStatement.close();
        }
        logger.info("Collaborators added to Job.");
      }
      connectionObject.commit();
      connectionObject.close();
      return jobId;

    } finally {
      if(connectionObject != null) {
        DbUtils.closeQuietly(connectionObject);
      }
    }
  }

  /**
   * generatePreparedStatement - Generate SQL Statement for adding collaborator details
   *
   * @param collaborator
   * @param jobId
   * @return String
   * @throws Exception
   * @author Pradeep Balasubramanian
   * <p>
   * Modified at ${date}
   */

  private PreparedStatement generatePreparedStatement(Collaborator collaborator,
                                                      int jobId,
                                                      Connection connection)
      throws SQLException {

    Map<String, Object> insertionObjects = new LinkedHashMap<>();
    insertionObjects.put("job_id", jobId);
    insertionObjects.put("collaborator", collaborator.getCollaborator());

    String columnValueString = "";
    String entryValueString = "";
    String appender = "";
    for (Map.Entry<String, Object> columnEntries : insertionObjects.entrySet()) {

      columnValueString += columnEntries.getKey() + ",";
      entryValueString += "?,";

    }

    entryValueString = entryValueString.substring(0, entryValueString.length() - 1);
    columnValueString = columnValueString.substring(0, columnValueString.length() - 1);


    String collaboratorQuery = "insert into Collaborators(" + columnValueString + ") values(" + entryValueString + ");";
    PreparedStatement preparedStatement = connection.prepareStatement(collaboratorQuery);
    int statementIterator = 0;
    for (Map.Entry<String, Object> columnEntries : insertionObjects.entrySet()) {
      Object insertionObject = columnEntries.getValue();
      if (insertionObject instanceof Boolean) {
        preparedStatement.setBoolean(++statementIterator,
            Boolean.valueOf(insertionObject.toString()));
      } else if (insertionObject instanceof Integer) {
        preparedStatement.setInt(++statementIterator,
            Integer.valueOf(insertionObject.toString()));
      } else {
        preparedStatement.setString(++statementIterator, insertionObject.toString());
      }
    }
    return preparedStatement;

  }

  /**
   * generatePreparedStatement - Create SQL Query for adding Custom Question Details
   *
   * @param customQuestion
   * @param jobId
   * @return java.lang.String
   * @throws Exception
   * @author Pradeep Balasubramanian
   */
  private PreparedStatement generatePreparedStatement(CustomQuestion customQuestion,
                                                      int jobId,
                                                      Connection connection)
      throws SQLException {
    Map<String, Object> insertionObjects = new LinkedHashMap<>();
    insertionObjects.put("job_id", jobId);
    insertionObjects.put("custom_question", customQuestion.getCustomQuestion());
    insertionObjects.put("custom_option1", customQuestion.getCustomOption1());
    insertionObjects.put("custom_option2", customQuestion.getCustomOption2());
    insertionObjects.put("custom_knockout", customQuestion.isCustomKnockout());
    insertionObjects.put("custom_answer", customQuestion.getCustomAnswer());
    insertionObjects.put("custom_question_required", customQuestion.getCustomQuestionRequired());


    String columnValueString = "";
    String entryValueString = "";
    String appender = "";
    for (Map.Entry<String, Object> columnEntries : insertionObjects.entrySet()) {
      columnValueString += columnEntries.getKey() + ",";
      entryValueString +=  "?,";
    }

    entryValueString = entryValueString.substring(0, entryValueString.length() - 1);
    columnValueString = columnValueString.substring(0, columnValueString.length() - 1);


    String sqlQueryForCustomQuestions = "insert into CustomQuestionJob(" + columnValueString + ")" +
        " values(" + entryValueString + ")";
    PreparedStatement preparedStatement = connection.prepareStatement(sqlQueryForCustomQuestions);
    int statementIterator = 0;
    for (Map.Entry<String, Object> columnEntries : insertionObjects.entrySet()) {
      Object insertionObject = columnEntries.getValue();
      if (insertionObject instanceof Boolean) {
        preparedStatement.setBoolean(++statementIterator,
            Boolean.valueOf(insertionObject.toString()));
      } else if (insertionObject instanceof Integer) {
        preparedStatement.setInt(++statementIterator,
            Integer.valueOf(insertionObject.toString()));
      } else {
        preparedStatement.setString(++statementIterator, insertionObject.toString());
      }
    }
    return preparedStatement;
  }
}