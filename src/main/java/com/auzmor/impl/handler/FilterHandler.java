package com.auzmor.impl.handler;

import com.auzmor.impl.builder.ResponseBuilder;
import com.auzmor.impl.configuration.MysqldbConf;

import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * FilterHandler - A class which contains API Implementation for the Search Filters
 */
public class FilterHandler {

  private static final Logger logger = LoggerFactory.getLogger(FilterHandler.class);

  /**
   * getFilterList -  Method for filter on Candidate and Talent Pool
   *
   * @param routingContext
   */
  public void getFilterList(RoutingContext routingContext) {

    Connection connectionObject = null;
    try {
      connectionObject = new MysqldbConf().getConnection();
      PreparedStatement preparedStatement;
      MultiMap params = routingContext.request().params();
      int organizationId = Integer.parseInt(params.get("organization_id"));
      String status = params.get("status");

      JsonObject jsonSuccessObject = new JsonObject();
      jsonSuccessObject.put("code", HttpStatus.SC_OK);
      jsonSuccessObject.put("success", true);

      String jobTitle;
      String jobStatus;
      String location;
      String candidateStatus;
      String hiringLeadId;
      String rating;
      String source;
      String minExp;
      String queryString;

      if (status != null && status.equals("Not Hired")) {
        // Filter for Talent Pool
        queryString = "select" +
            "(SELECT group_concat(DISTINCT job_title) from Candidate,Job,HiringprocessCandidate" +
            " where Candidate.job_id= Job.id and " +
            "HiringprocessCandidate.candidate_id=Candidate.candidate_id " +
            "and (Candidate.status != 'Hire Candidate' or Candidate.status is null) " +
            "and Job.job_status != 'filled' and Candidate.active_status = '0' " +
            "and job_active_status ='1' " +
            "  and Job.organization_id = Candidate.organization_id " +
            "and Job.organization_id=" + organizationId + " " +
            "and HiringprocessCandidate.status='" + status + "') as jobTitle," +
            "  (SELECT group_concat(DISTINCT Candidate.source) " +
            "from Candidate,Job,HiringprocessCandidate " +
            "where Candidate.job_id= Job.id and " +
            "HiringprocessCandidate.candidate_id=Candidate.candidate_id and " +
            "(Candidate.status != 'Hire Candidate' or Candidate.status is null)" +
            "  and Job.job_status != 'filled' and Candidate.active_status = '0' " +
            "and job_active_status ='1' " +
            "  and Job.organization_id = Candidate.organization_id " +
            "and Job.organization_id=" + organizationId +
            " and HiringprocessCandidate.status='" + status + "') as source," +
            "  (SELECT group_concat(DISTINCT Job.minimum_experience) " +
            "from Candidate,Job,HiringprocessCandidate where Candidate.job_id= Job.id " +
            "and HiringprocessCandidate.candidate_id=Candidate.candidate_id and " +
            "(Candidate.status != 'Hire Candidate' or Candidate.status is null)" +
            "  and Job.job_status != 'filled' and Candidate.active_status = '0'" +
            " and job_active_status ='1' " +
            "  and Job.organization_id = Candidate.organization_id and " +
            "Job.organization_id=" + organizationId + " and " +
            "HiringprocessCandidate.status='" + status + "' and " +
            "Candidate.minimum_experience != '' ) as minExp," +
            "  (SELECT group_concat(DISTINCT Candidate.rating) " +
            "from Candidate,Job,HiringprocessCandidate " +
            "where Candidate.job_id= Job.id and " +
            "HiringprocessCandidate.candidate_id=Candidate.candidate_id and" +
            " (Candidate.status != 'Hire Candidate' or Candidate.status is null)" +
            "  and Job.job_status != 'filled' and " +
            "Candidate.active_status = '0' and job_active_status ='1' " +
            "  and Job.organization_id = Candidate.organization_id and " +
            "Job.organization_id=" + organizationId + " and " +
            "HiringprocessCandidate.status='" + status + "') as rating";
      } else {
        // Filter for Candidate
        queryString = "select" +
            "(SELECT group_concat(DISTINCT Job.title) from Candidate,Job " +
            "where Candidate.job_id= Job.id and " +
            "(Candidate.status != 'Hire Candidate' or Candidate.status is null) " +
            "  and Job.job_status != 'filled' and " +
            "Candidate.active_status = '0' and job_active_status ='1' " +
            "  and Job.organization_id = Candidate.organization_id and " +
            "Job.organization_id=" + organizationId + ") as jobTitle," +
            "  (SELECT group_concat(DISTINCT Job.job_status) from Candidate,Job " +
            "where Candidate.job_id= Job.id and " +
            "(Candidate.status != 'Hire Candidate' or Candidate.status is null)" +
            "  and Job.job_status != 'filled' and " +
            "Candidate.active_status = '0' and job_active_status ='1' " +
            "  and Job.organization_id = Candidate.organization_id and " +
            "Job.organization_id=" + organizationId + ") as jobStatus," +
            "  (SELECT group_concat(DISTINCT Job.location) from Candidate,Job " +
            "where Candidate.job_id= Job.id and " +
            "(Candidate.status != 'Hire Candidate' or Candidate.status is null)" +
            "  and Job.job_status != 'filled' and Candidate.active_status = '0' " +
            "and job_active_status ='1' and Job.organization_id = Candidate.organization_id " +
            "and Job.organization_id=" + organizationId + ") as location," +
            "  (SELECT group_concat(DISTINCT Candidate.status) from Candidate,Job " +
            "where Candidate.job_id= Job.id and " +
            "(Candidate.status != 'Hire Candidate' or Candidate.status is null)" +
            "  and Job.job_status != 'filled' and Candidate.active_status = '0' " +
            "and job_active_status ='1' " +
            "  and Job.organization_id = Candidate.organization_id and " +
            "Job.organization_id=" + organizationId + " ) as candidateStatus," +

            //   "(SELECT group_concat(DISTINCT employee_id, Employee.first_name,Employee.last_name)
            // FROM Job,Employee,Candidate where Employee.organization_id = " + organizationid + " and
            // employee_id = hiring_lead and Candidate.job_id= Job.id and (Candidate.status != 'Hire Candidate' or " +
           // "Candidate.status is null)" + "  and Job.job_status != 'filled' and " +
           // "Candidate.active_status = '0' and job_active_status ='1' " +
            // "  and Job.organization_id = Candidate.organization_id and " +
           // "Job.organization_id=" + organizationId + " and Job.hiring_lead != '' ) as hiringlead ,\n" +
            "  (SELECT group_concat(DISTINCT Job.hiring_lead) from Candidate,Job " +
            "where Candidate.job_id= Job.id and (Candidate.status != 'Hire Candidate' or " +
            "Candidate.status is null)" + "  and Job.job_status != 'filled' and " +
            "Candidate.active_status = '0' and job_active_status ='1' " +
            "  and Job.organization_id = Candidate.organization_id and " +
            "Job.organization_id=" + organizationId + " and Job.hiring_lead != '' ) as hiringLead,"+

            "  (SELECT group_concat(DISTINCT rating) from Candidate,Job " +
            "where Candidate.job_id= Job.id and (Candidate.status != 'Hire Candidate' " +
            "or Candidate.status is null)" + "  and Job.job_status != 'filled' and " +
            "Candidate.active_status = '0' and job_active_status ='1' " +
            "  and Job.organization_id = Candidate.organization_id and " +
            "Job.organization_id=" + organizationId + ") as rating";
      }

      JsonObject dataObject = new JsonObject();

      preparedStatement = connectionObject.prepareStatement(queryString);
      ResultSet resultSetObject = preparedStatement.executeQuery();
      JsonArray resultSetArray = new JsonArray();

      while (resultSetObject.next()) {

        if (status != null) {
          jobTitle = resultSetObject.getString("jobTitle");
          source = resultSetObject.getString("source");
          minExp = resultSetObject.getString("minExp");
          rating = resultSetObject.getString("rating");

          dataObject.put("jobTitle", jobTitle);
          dataObject.put("source", source);
          dataObject.put("minExp", minExp);
          dataObject.put("rating", rating);

        } else {
          jobTitle = resultSetObject.getString("jobTitle");
          jobStatus = resultSetObject.getString("jobStatus");
          location = resultSetObject.getString("location");
          candidateStatus = resultSetObject.getString("candidateStatus");
          hiringLeadId = resultSetObject.getString("hiringLead");
          rating = resultSetObject.getString("rating");


          dataObject.put("jobTitle", jobTitle);
          dataObject.put("jobStatus", jobStatus);
          dataObject.put("location", location);
          dataObject.put("candidateStatus", candidateStatus);
          dataObject.put("hiringLead",getHiringLeadDetails(hiringLeadId,connectionObject));
          dataObject.put("rating", rating);
        }
      }

      resultSetArray.add(dataObject);
      jsonSuccessObject.put("data", resultSetArray);
      routingContext.response()
          .setStatusCode(HttpStatus.SC_OK)
          .putHeader("Content-Type", "application/json; charset=utf-8")
          .end(jsonSuccessObject.toString());
      resultSetObject.close();
      preparedStatement.close();
      connectionObject.close();
    } catch(SQLException sqlException){

      try {
        connectionObject.close();
      } catch (SQLException e) {
        logger.error("Connection Object Closed");
      }

      logger.error("SQL Exception in Filter Handler");
      logger.error(sqlException.getMessage());

      ResponseBuilder.writeInternalServerError(routingContext.response());

    } catch(ClassNotFoundException classNotFoundException){

      logger.error("Class Not Found Exception in Filter Handler");

      ResponseBuilder.writeInternalServerError(routingContext.response());
    }
  }

  /**
   * getHiringLeadName - getHiringLeadName based on Hiring Lead Id
   * @param hiringLeadId
   * @param connection
   * @return
   * @throws SQLException
   */
  private String getHiringLeadName(String hiringLeadId,Connection connection)
      throws SQLException {

    String query = "select first_name,last_name from Employee where employee_id = ?";
    String firstName = "",lastName = "";
    PreparedStatement preparedStatement = connection.prepareStatement(query);
    preparedStatement.setInt(1,Integer.valueOf(hiringLeadId));

    ResultSet resultSet = preparedStatement.executeQuery();
    while(resultSet.next()){
      firstName = resultSet.getString("first_name");
      lastName = resultSet.getString("last_name");
    }

    resultSet.close();
    preparedStatement.close();

    return firstName + " " + lastName;
  }

  private JsonArray getHiringLeadDetails(String hiringLeadId,Connection connection)
      throws SQLException {

    String firstName = "",lastName = "";

    String[] splitter = hiringLeadId.split(",");
    String constraintQuery = "";
    for(int i=0;i<splitter.length;i++) {
      constraintQuery += "?";
      if(i<splitter.length-1){
        constraintQuery += ",";
      }
    }
    String query = "select employee_id,first_name,last_name from Employee ";
    String constraint = " where employee_id in ("+constraintQuery+")";

    PreparedStatement preparedStatement = connection.prepareStatement(query + constraint);
    for(int i=1;i<=splitter.length;i++){
      preparedStatement.setString(i,splitter[i-1]);
    }

    ResultSet resultSet = preparedStatement.executeQuery();
    JsonArray resultArray = new JsonArray();
    while(resultSet.next()){
      JsonObject jsonObject = new JsonObject();
      jsonObject.put("employee_id",resultSet.getInt("employee_id"));
      jsonObject.put("firstName",resultSet.getString("first_name"));
      jsonObject.put("lastName",resultSet.getString("last_name"));

      resultArray.add(jsonObject);
    }

    resultSet.close();
    preparedStatement.close();

    return resultArray;
  }
}