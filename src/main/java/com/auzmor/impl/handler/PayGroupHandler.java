package com.auzmor.impl.handler;


import com.auzmor.bo.PayGroupBO;
import com.auzmor.impl.bo.PayGroupBOImpl;
import com.auzmor.impl.enumarator.PayGroupEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.PayGroupModel;

import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.Validator;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PayGroupHandler {

  private static final Logger logger = LoggerFactory.getLogger(PayGroupHandler.class);

  /**
   * Get details of pay groups
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  public static void getAllPayGroup(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    PayGroupModel payGroupModel = new PayGroupModel();

    String[] allowedParams = {"organizationId"};
    String[] requiredParams = {"organizationId"};
    String[] payGroupParams = {"organizationId"};

    List<Validator> validators = new ArrayList<>();
    Validator payGroupValidator = new ValidatorImpl(payGroupParams, PayGroupModel.getValidatorMap());
    validators.add(payGroupValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          PayGroupBO payGroupBO = new PayGroupBOImpl(dbHandle);
          payGroupModel.setOrganizationId(Integer.valueOf(requestParams.get("organizationId")));
          ReturnObject<PayGroupEnum, JsonArray> boResponse =
              payGroupBO.getAllPayGroups(dbHandle,payGroupModel);
          switch (boResponse.getStatusEnum()) {
            case VALUE_SUCCESS:
              dbHandle.commit();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  "", boResponse.getData(), httpServerResponse);
              break;
            case NO_SUCH_VALUE:
              dbHandle.rollback();
              ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
                  "Unable to find Division", "Unable to find Division",
                  httpServerResponse);
              break;
            default:
              dbHandle.rollback();
              ResponseHelper.writeInternalServerError(httpServerResponse);
              break;
          }
        });
  }

  /**
   * add new pay group
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  public static void addPayGroup(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    PayGroupModel payGroupModel = new PayGroupModel();

    String[] allowedParams = {"paygroup_name", "organization_id", "active_status"};
    String[] requiredParams = {"paygroup_name", "organization_id"};
    String[] payGroupParams = {"paygroup_name", "organization_id", "active_status"};

    List<Validator> validators = new ArrayList<>();
    Validator payGroupValidator = new ValidatorImpl(payGroupParams, PayGroupModel.getValidatorMap());
    validators.add(payGroupValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          PayGroupBO payGroupBO = new PayGroupBOImpl(dbHandle);
          payGroupModel.setPaygroupName(requestParams.get("paygroup_name"));
          payGroupModel.setOrganizationId(Integer.valueOf(requestParams.get("organization_id")));

          ReturnObject<PayGroupEnum, JsonObject> boResponse =
              payGroupBO.addPayGroup(dbHandle,payGroupModel);
          PayGroupHandler.writeResponse(requestParams,httpServerResponse,dbHandle,boResponse);
        });
  }

  /**
   * Update details of pay group
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  public static void updatePayGroup(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    PayGroupModel payGroupModel = new PayGroupModel();

    String[] allowedParams = {"paygroup_name", "paygroupId", "organization_id", "active_status"};
    String[] requiredParams = {"paygroup_name", "paygroupId", "organization_id", "active_status"};
    String[] payGroupParams = {"paygroup_name", "paygroupId", "organization_id", "active_status"};

    List<Validator> validators = new ArrayList<>();
    Validator payGroupValidator = new ValidatorImpl(payGroupParams, PayGroupModel.getValidatorMap());
    validators.add(payGroupValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          PayGroupBO payGroupBO = new PayGroupBOImpl(dbHandle);
          ReturnObject<PayGroupEnum, JsonObject> boResponse =
              payGroupBO.updatePayGroup(dbHandle,requestParams);
          PayGroupHandler.writeResponse(requestParams,httpServerResponse,dbHandle,boResponse);
        });
  }

  /**
   * get details of particular pay group
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  public static void getPayGroup(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    PayGroupModel payGroupModel = new PayGroupModel();

    String[] allowedParams = {"paygroupId"};
    String[] requiredParams = {"paygroupId"};
    String[] payGroupParams = {"paygroupId"};

    List<Validator> validators = new ArrayList<>();
    Validator payGroupValidator = new ValidatorImpl(payGroupParams, PayGroupModel.getValidatorMap());
    validators.add(payGroupValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          PayGroupBO payGroupBO = new PayGroupBOImpl(dbHandle);
          payGroupModel.setPaygroupId(Integer.valueOf(requestParams.get("paygroupId")));
          ReturnObject<PayGroupEnum, JsonObject> boResponse =
              payGroupBO.getPayGroup(dbHandle,payGroupModel);
          PayGroupHandler.writeResponse(requestParams,httpServerResponse,dbHandle,boResponse);
        });
  }

  /**
   * Delete details of pay group
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  public static void deletePayGroup(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    PayGroupModel payGroupModel = new PayGroupModel();

    String[] allowedParams = {"paygroupId"};
    String[] requiredParams = {"paygroupId"};
    String[] payGroupParams = {"paygroupId"};

    List<Validator> validators = new ArrayList<>();
    Validator payGroupValidator = new ValidatorImpl(payGroupParams, PayGroupModel.getValidatorMap());
    validators.add(payGroupValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          PayGroupBO payGroupBO = new PayGroupBOImpl(dbHandle);
          payGroupModel.setPaygroupId(Integer.valueOf(requestParams.get("paygroupId")));
          ReturnObject<PayGroupEnum, JsonObject> boResponse =
              payGroupBO.deletePayGroup(dbHandle,payGroupModel);
          PayGroupHandler.writeResponse(requestParams,httpServerResponse,dbHandle,boResponse);
        });
  }

  /**
   * Writing correct response according to enum values
   * @param params
   * @param httpServerResponse
   * @param dbHandle
   * @param boResponse
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 14/FEB/2018
   */
  public static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   ReturnObject<PayGroupEnum, JsonObject> boResponse)
      throws SQLException, DbHandleException {
    JsonObject dataObject = new JsonObject();
    dataObject.put("message", boResponse.getStatusEnum().getDescription());

    switch (boResponse.getStatusEnum()) {
      case EXISTS_ALREADY:
        dbHandle.rollback();
        logger.error("This pay group name :"+boResponse);
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            params.get("paygroup_name") + " already exists",
            params.get("paygroup_name") + " already exists",
            httpServerResponse);
        break;
      case ADDITION_SUCCESS:
        dbHandle.commit();
        logger.info("Pay group added Successfully :"+boResponse);
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Pay Group added successfully", dataObject, httpServerResponse);
        break;
      case VALUE_SUCCESS:
        dbHandle.commit();
        logger.info("Pay group retrieved successfully :"+boResponse);
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "", boResponse.getData(), httpServerResponse);
        break;
      case ADDITION_FAILED:
        dbHandle.rollback();
        logger.error("could not able to add pay group :"+boResponse);
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Unable to add Pay Group", "Pay Group cannot be added",
            httpServerResponse);
        break;
      case UPDATE_SUCCESS:
        dbHandle.commit();
        logger.info("pay group updated successfully :"+boResponse);
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Pay Group updated successfully", dataObject, httpServerResponse);
        break;
      case NO_SUCH_VALUE:
        dbHandle.rollback();
        logger.error("This pay group does not exist :"+boResponse);
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Unable to find Pay Group", dataObject,
            httpServerResponse);
        break;
      case CANNOT_DELETE:
        dbHandle.rollback();
        logger.error("Could not able to delete pay group :"+boResponse);
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Unable to delete Pay Group", "Unable to delete Pay Group",
            httpServerResponse);
        break;
      case DELETE_SUCCESS:
        dbHandle.commit();
        logger.info("pay group deleted successfully :"+boResponse);
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Pay Group deleted successfully", dataObject, httpServerResponse);
        break;
      default:
        dbHandle.rollback();
        logger.error(""+boResponse);
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }
}
