package com.auzmor.impl.handler;

import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonArray;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JobUpdateHandler {

  /**
   * updateCustomQuestions Update Custom Question items in Job
   *
   * @param id
   * @param connectionObject
   * @param customQuestionsMap
   * @throws Exception
   * @author Pradeep Balasubramanian
   * Modified at ${date}
   */
  public void updateCustomQuestions(int id, Connection connectionObject,
                                    Map<String, String> customQuestionsMap)
      throws DecodeException, SQLException {

    Statement customQuestionStatement = connectionObject.createStatement();

    String deleteCustomQueryString = "delete from CustomQuestionJob where job_id=" + id;
    customQuestionStatement.executeUpdate(deleteCustomQueryString);


    String fieldString = null;
    String valueString = null;
    JsonArray customQuestionArray = new JsonArray((customQuestionsMap.get("custom_question")));

    for (int i = 0; i < customQuestionArray.size(); i++) {
      fieldString = "";
      valueString = "";
      List<String> entryType = new ArrayList<String>();
      List<String> entryValue = new ArrayList<>();

      for (Map.Entry<String, String> mapEntries : customQuestionsMap.entrySet()) {
        fieldString += mapEntries.getKey() + ",";
        if ("custom_knockout".equals(mapEntries.getKey()) ||
              "custom_question_required".equals(mapEntries.getKey())) {
          valueString += "?,";
          entryType.add("boolean");
          entryValue.add(String.valueOf(new JsonArray(mapEntries.getValue()).getBoolean(i)));
        } else {
          valueString += "?,";
          entryType.add("string");
          entryValue.add(new JsonArray(mapEntries.getValue()).getString(i));
        }
      }

      String customQuestionUpdateQuery = "insert into CustomQuestionJob(" + fieldString + "job_id) " +
          "values(" + valueString + id + ")";

      PreparedStatement preparedStatement = connectionObject
          .prepareStatement(customQuestionUpdateQuery);
      for(int j = 0; j < entryType.size(); j++) {
        if("boolean".equalsIgnoreCase(entryType.get(j))) {
          preparedStatement.setBoolean(j+1, Boolean.valueOf(entryValue.get(j)));
        } else if("string".equalsIgnoreCase(entryType.get(j))) {
          preparedStatement.setString(j+1, entryValue.get(j));
        }
      }

      preparedStatement.executeUpdate();

    }
  }

}
