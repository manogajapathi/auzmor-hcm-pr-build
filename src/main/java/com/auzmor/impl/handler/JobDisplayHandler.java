package com.auzmor.impl.handler;

import com.auzmor.bo.JobBO;
import com.auzmor.impl.bo.JobBOImpl;
import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.Job.Collaborator;
import com.auzmor.impl.model.Job.CustomQuestion;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.util.DateUtil;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.dbutils.DbUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.text.ParseException;
import java.util.List;

public class JobDisplayHandler {

  private static final Logger logger = LoggerFactory.getLogger(JobDisplayHandler.class);

  public int deleteJobPost(int id) throws ClassNotFoundException, SQLException {
    Connection connectionObject = null;
    int deletedRows = 0;
    try {
      connectionObject = new MysqldbConf().getConnection();
      connectionObject.setAutoCommit(false);

      String queryString = "update Job set job_status = ? where id = ?";

      PreparedStatement preparedStatement = connectionObject.prepareStatement(queryString);
      preparedStatement.setString(1,"Closed");
      preparedStatement.setInt(2,id);


      deletedRows = preparedStatement.executeUpdate();

      connectionObject.commit();
      connectionObject.close();
    } finally {
      DbUtils.closeQuietly(connectionObject);
    }
    return deletedRows;
  }

  public JsonObject getJobDetailsBasedOnId(RoutingContext routingContext, String query)
      throws IllegalArgumentException, SQLException, ClassNotFoundException, ParseException {
    int recordCount = 0;
    JsonObject jsonDataObject = new JsonObject();
    CustomQuestion customQuestionObject = new CustomQuestion();
    Collaborator collaboratorObject = new Collaborator();
    Connection connectionObject = new MysqldbConf().getConnection();
    PreparedStatement preparedStatement = connectionObject.prepareStatement(query);
    ResultSet resultSetObject = preparedStatement.executeQuery();
    DbHandle dbHandle = null;
    dbHandle = DbHelper.getDBHandle(routingContext, false);

    String columnName = "";
    String columnType = "";
    long jobId = 0;
    long organizationId = 0;
    while (resultSetObject.next()) {
      ResultSetMetaData resultSetMetaData = resultSetObject.getMetaData();

      JobBO jobBO = new JobBOImpl(dbHandle);

      for (int i = 1; i <= resultSetObject.getMetaData().getColumnCount(); i++) {
        columnName = resultSetMetaData.getColumnName(i);
        columnType = resultSetMetaData.getColumnTypeName(i);

        if ("BIT".equalsIgnoreCase(columnType)) {
          jsonDataObject.put(columnName, resultSetObject.getBoolean(columnName));
        } else if ("INT".equalsIgnoreCase(columnType)) {

          if (columnName.equals("id")) {
            jobId = resultSetObject.getInt(columnName);
          }
          if (columnName.equals("organization_id")) {
            organizationId = resultSetObject.getInt(columnName);
          }

          jsonDataObject.put(columnName, resultSetObject.getInt(columnName));
        } else if ("VARCHAR".equalsIgnoreCase(columnType) || "BLOB".equalsIgnoreCase(columnType)) {
          String columnValue = resultSetObject.getString(columnName);
          jsonDataObject.put(columnName, columnValue);

          if ("applicant".equals(columnName) || "interview".equals(columnName)
              || "offer".equals(columnName) || "hire".equals(columnName)
              || "screening".equals(columnName) || "not_hired".equals(columnName)) {


            JsonArray jsonObject = null;
            try {
              jsonObject = jobBO.getJobStatus(columnName, jobId, organizationId);
            } catch (DbHandleException e) {
              e.printStackTrace();
            }
            jsonDataObject.put(columnName + "status", jsonObject);
            jsonDataObject.put(columnName + "Count", jsonObject.size());

            jsonDataObject.put(columnName,
                StringUtil.convertDBToJsonString(columnValue, columnName));

          } else if ("hiring_lead".equals(columnName)) {
            if (StringUtil.isNumber(jsonDataObject.getString("hiring_lead"))) {
              jsonDataObject.put(columnName,
                  getHiringLeadName(jsonDataObject.getString("hiring_lead"), connectionObject));
            } else {
              jsonDataObject.put(columnName, "-");
            }
          }
          if (columnName.equalsIgnoreCase("date_posted") ||
              columnName.equalsIgnoreCase("job_open_date")) {
            if (!columnValue.contains("00")) {
              String date = new DateUtil().getDateFormat(routingContext, StringUtil
                  .printValidString(columnValue));
              jsonDataObject.put(columnName, date);
            }
          }

          if (columnName.equalsIgnoreCase("currency_type")) {

            Organization organization = ((Organization) routingContext.data().get("organization"));
            String currencyValue = "";
            if(organization != null) {
              currencyValue = StringUtil.printValidString(organization.getDefault_currency());
            }

            if(currencyValue.length() == 0) {
              currencyValue = "USD";
            }

            String currencyResponseObject = "";
            boolean doesCurrencyTypeExists = StringUtil.printValidString(
                resultSetObject.getString(columnName)).length() > 0;
            currencyResponseObject = doesCurrencyTypeExists == true ?
                resultSetObject.getString(columnName) : currencyValue;
            jsonDataObject.put(columnName, currencyResponseObject);
          }

        } else if ("DATE".equalsIgnoreCase(columnType)) {
          jsonDataObject.put(columnName, resultSetObject.getString(columnName));
        }
      }

      int id = resultSetObject.getInt("id");

      jsonDataObject.put("custom_questions",
          customQuestionObject.getCustomQuestionArray(connectionObject, id));
      jsonDataObject.put("collaborators",
          collaboratorObject.getCollaboratorArray(connectionObject, id));
      ++recordCount;
    }

    resultSetObject.close();
    preparedStatement.close();
    dbHandle.checkAndCloseConnection();
    connectionObject.close();


    if(recordCount == 0) {
      throw new IllegalArgumentException("Invalid Job Display Request");
    }

    return jsonDataObject;
  }

  /**
   * getJobDetailsBasedOnConstraints - get job details based on constraints
   *
   * @param query
   * @param fieldList
   * @param isCollaborator
   * @param isCustomQuestion
   * @param requiresCandidateCount
   * @param organizationId
   * @return
   * @throws Exception
   */
  public JsonArray getJobDetailsBasedOnConstraints(RoutingContext routingContext, String query,
                                                   List<String> fieldList,
                                                   boolean isCollaborator, boolean isCustomQuestion,
                                                   boolean requiresCandidateCount,
                                                   int organizationId, int userId)
      throws ClassNotFoundException, SQLException, ParseException {
    int recordCount = 0;
    Connection connectionObject = new MysqldbConf().getConnection();
    PreparedStatement preparedStatement = connectionObject.prepareStatement(query);
    ResultSet resultSetObject = preparedStatement.executeQuery();
    JsonArray resultSetArray = new JsonArray();
    ResultSetMetaData resultSetMetaData = resultSetObject.getMetaData();
    Collaborator collaborator = new Collaborator();
    CustomQuestion customQuestion = new CustomQuestion();

    while (resultSetObject.next()) {
      JsonObject dataObject = new JsonObject();

      for (String field : fieldList) {
        String columnType = resultSetMetaData.getColumnTypeName(resultSetObject.findColumn(field));
        if ("BIT".equalsIgnoreCase(columnType)) {
          dataObject.put(field, resultSetObject.getBoolean(field));
        } else if ("INT".equalsIgnoreCase(columnType)) {
          dataObject.put(field, resultSetObject.getInt(field));
        } else {
          /*if ("applicant".equalsIgnoreCase(field) || "interview".equalsIgnoreCase(field)
              || "offer".equalsIgnoreCase(field) || "hire".equalsIgnoreCase(field)
              || "screening".equalsIgnoreCase(field) || "not_hired".equalsIgnoreCase(field)) {

            dataObject.put(field,
                StringUtil.convertDBToJsonString(resultSetObject.getString(field), field));

          } else*/
          if (field.equalsIgnoreCase("hiring_lead")) {
            if (StringUtil.isNumber(resultSetObject.getString(field))) {
              dataObject.put(field,
                  getHiringLeadName(resultSetObject.getString(field), connectionObject));
            } else {
              dataObject.put(field, "-");
            }
          } else {
            dataObject.put(field, resultSetObject.getString(field));
          }

          if (field.equalsIgnoreCase("date_posted")) {
            String date = new DateUtil().getDateFormat(routingContext,
                StringUtil.printValidString(resultSetObject.getString("date_posted")));
            dataObject.put(field, date);
          }

          if (field.equalsIgnoreCase("currency_type")) {

            Organization organization = ((Organization) routingContext.data().get("organization"));
            String currencyValue = "";
            if(organization != null) {
              currencyValue = StringUtil.printValidString(organization.getDefault_currency());
            }

            if(currencyValue.length() == 0) {
              currencyValue = "USD";
            }

            String currencyResponseObject = "";
            boolean doesCurrencyTypeExists = StringUtil.printValidString(
                resultSetObject.getString(field)).length() > 0;
            currencyResponseObject = doesCurrencyTypeExists == true ?
                resultSetObject.getString(field) : currencyValue;
            dataObject.put(field, currencyResponseObject);
          }

          if (field.equalsIgnoreCase("job_open_date")) {
            if (!resultSetObject.getString("job_open_date").contains("00")) {
              String date = new DateUtil().getDateFormat(routingContext,
                  StringUtil.printValidString(resultSetObject.getString("job_open_date")));
              dataObject.put(field, date);
            }
          }
        }
      }

      if (isCollaborator) {
        JsonArray collaboratorArray = collaborator.getCollaboratorArray(connectionObject,
            resultSetObject.getInt("id"));
        dataObject.put("collaborators", collaboratorArray);
      }

      if (isCustomQuestion) {
        JsonArray customQuestionsArray = customQuestion.getCustomQuestionArray(connectionObject,
            resultSetObject.getInt("id"));
        dataObject.put("custom_question", customQuestionsArray);
      }

      if (requiresCandidateCount) {
        String candidateCountQueryString = "select count(1) as count from Candidate where job_id = "
            + resultSetObject.getInt("id")
            + " and active_status = 1 and organization_id = " + organizationId;
        Statement candidateQueryStatement = connectionObject.createStatement();
        ResultSet candidateResultSet = candidateQueryStatement.executeQuery(candidateCountQueryString);
        while (candidateResultSet.next()) {
          dataObject.put("candidate_count", candidateResultSet.getString("count"));
        }
        logger.info("Get the number of candidates applied for Job");
      }

      if (userId != 0) {
        int i = 0;
        String candidateCountQueryString = "select job_id from Candidate where user_id = "
            + userId
            + " and job_id = " + resultSetObject.getInt("id") +
            " and organization_id = " + organizationId +" and active_status = 1";
        Statement candidateQueryStatement = connectionObject.createStatement();
        ResultSet candidateResultSet = candidateQueryStatement.executeQuery(candidateCountQueryString);
        while (candidateResultSet.next()) {
          i = 1;
          dataObject.put("appiled_job", true);
        }
        if (i == 0) {
          dataObject.put("appiled_job", false);
        }
      }
      resultSetArray.add(dataObject);
      ++recordCount;
    }

    connectionObject.close();
    return resultSetArray;

  }


  /**
   * getHiringLeadName - getHiringLeadName based on Hiring Lead Id
   *
   * @param hiringLeadId
   * @param connection
   * @return
   * @throws SQLException
   */
  private JsonObject getHiringLeadName(String hiringLeadId, Connection connection)
      throws SQLException {

    String query = "select first_name,last_name,profile_image from Employee where employee_id = ?";
    String firstName = "", lastName = "";
    String profileImage = "";
    PreparedStatement preparedStatement = connection.prepareStatement(query);
    preparedStatement.setInt(1, Integer.valueOf(hiringLeadId));

    ResultSet resultSet = preparedStatement.executeQuery();
    while (resultSet.next()) {
      firstName = resultSet.getString("first_name");
      lastName = resultSet.getString("last_name");
      profileImage = resultSet.getString("profile_image");
    }

    JsonObject jsonObject = new JsonObject();
    jsonObject.put("employee_id", Integer.valueOf(hiringLeadId));
    jsonObject.put("first_name", firstName);
    jsonObject.put("last_name", lastName);
    jsonObject.put("profile_image", profileImage);

    resultSet.close();
    preparedStatement.close();

    return jsonObject;
  }

}
