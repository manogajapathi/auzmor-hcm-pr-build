package com.auzmor.impl.handler;

import com.auzmor.bo.PTOCommentBO;
import com.auzmor.impl.bo.PTOCommentsBOImpl;
import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.enumarator.bo.PTOCommentsEnum;
import com.auzmor.impl.enumarator.file.FileTypeEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.*;
import com.auzmor.impl.model.PTO.PTOComments;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.FieldOptions;
import com.auzmor.util.validator.Validator;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class PTOCommentsHandler {
  private static final Logger logger = LoggerFactory.getLogger(PTOCommentsHandler.class);

  /**
   * Add new comment for a particular pto request
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  public static void addComment(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    PTOComments ptoComments = new PTOComments();

    String[] allowedParams = {"pto_id", "creator_id", "comment_description",
        "pto_comments_file_path"};
    String[] requiredParams = {"pto_id", "creator_id", "comment_description"};
    String[] ptoParams = {"pto_id", "creator_id", "comment_description","pto_comments_file_path"};

    List<Validator> validators = new ArrayList<>();
    Validator employeePTOValidator = new ValidatorImpl(ptoParams, ptoComments.getValidatorMap());
    validators.add(employeePTOValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          PTOCommentBO ptoCommentBO = new PTOCommentsBOImpl(dbHandle);
          ReturnObject<PTOCommentsEnum, JsonObject> boResponse =
              ptoCommentBO.addComment(dbHandle, requestParams);
          PTOCommentsHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  /**
   * Get all comments for PTO Request
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  public static void getAllComments(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    PTOComments ptoComments = new PTOComments();

    String[] allowedParams = {"pto_id"};
    String[] requiredParams = {"pto_id"};
    String[] ptoParams = {"pto_id"};

    List<Validator> validators = new ArrayList<>();
    Validator employeePTOValidator = new ValidatorImpl(ptoParams, ptoComments.getValidatorMap());
    validators.add(employeePTOValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
      PTOCommentBO ptoCommentBO = new PTOCommentsBOImpl(dbHandle);
          try {
            ReturnObject<PTOCommentsEnum, JsonArray> boResponse =
                ptoCommentBO.getAllComments(dbHandle, requestParams);
            switch (boResponse.getStatusEnum()) {
              case VALUE_SUCCESS:
                dbHandle.commit();
                ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                    "no_data is false", boResponse.getData(), httpServerResponse);
                break;
              case NO_SUCH_VALUE:
                dbHandle.commit();
                ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                    "no_data is true", boResponse.getData(), httpServerResponse);
                break;
              default:
                dbHandle.rollback();
                ResponseHelper.writeInternalServerError(httpServerResponse);
                break;
            }
          } catch (ParseException e) {
            logger.error(e.getMessage());
            ResponseHelper.writeInternalServerError(httpServerResponse);
          }
        });
  }

  /**
   * Upload attachment for PTO Comments
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  public static void uploadDocuments(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = routingContext.request().params();

    String[] allowedParams = {"pto_id","organization_id","employee_id"};
    String[] requiredParams = {"pto_id","organization_id","employee_id"};
    String[] taskParams = {"pto_id", "organization_id","employee_id"};

    List<Validator> validators = new ArrayList<>();
    Validator fieldValidator = new ValidatorImpl(taskParams, new HashMap<String, FieldOptions>() {{
      put("pto_id", FieldOptionsCommon.NUMBER);
      put("organization_id", FieldOptionsCommon.NUMBER);
      put("employee_id", FieldOptionsCommon.NUMBER);
    }});
    validators.add(fieldValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    int organizationId = Integer.valueOf(params.get("organization_id"));
    int ptoId = Integer.valueOf(params.get("pto_id"));
    int employeeId = Integer.valueOf(params.get("employee_id"));
    String taskDocumentPath = "";

    Set<FileUpload> fileUploadSet = routingContext.fileUploads();
    for (FileUpload fileUpload : fileUploadSet) {
      FileTypeEnum fileTypeEnum = FileHelper.getFileType(fileUpload);
      String sourceFile = fileUpload.uploadedFileName();
      String destinationFile = fileUpload.uploadedFileName().split("/")[1] +
            fileTypeEnum.getExtension();
      String destinationFolderPath = "private_static/" + organizationId +"/"+ employeeId +
            "/pto_comments_doc/" + ptoId + "/";
      FileHelper.move(sourceFile, destinationFolderPath, destinationFile);
      taskDocumentPath = destinationFolderPath + destinationFile;
    }

    routingContext.response()
        .setStatusCode(org.apache.http.HttpStatus.SC_CREATED)  // 201
        .putHeader("Content-Type", "application/json; charset=utf-8")
        .end(taskDocumentPath);
  }

  /**
   * Delete comment
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  public static void deleteComment(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    PTOComments ptoComments = new PTOComments();

    String[] allowedParams = {"comment_id", "creator_id"};
    String[] requiredParams = {"comment_id", "creator_id"};
    String[] ptoParams = {"comment_id", "creator_id"};

    List<Validator> validators = new ArrayList<>();
    Validator fieldValidator = new ValidatorImpl(ptoParams, new HashMap<String, FieldOptions>() {{
      put("comment_id", FieldOptionsCommon.NUMBER);
      put("creator_id", FieldOptionsCommon.NUMBER);
    }});
    validators.add(fieldValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
      PTOCommentBO ptoCommentBO  = new PTOCommentsBOImpl(dbHandle);
      ReturnObject<PTOCommentsEnum, JsonObject> boResponse =
          ptoCommentBO.deleteComment(dbHandle, requestParams);
      PTOCommentsHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  /**
   * Update comment details
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  public static void updateComment(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    PTOComments ptoComments = new PTOComments();

    String[] allowedParams = {"id", "pto_id", "creator_id", "comment_description",
        "pto_comments_file_path"};
    String[] requiredParams = {"id", "pto_id", "creator_id", "comment_description"};
    String[] ptoParams = {"id", "pto_id", "creator_id", "comment_description","pto_comments_file_path"};

    List<Validator> validators = new ArrayList<>();
    Validator employeePTOValidator = new ValidatorImpl(ptoParams, ptoComments.getValidatorMap());
    validators.add(employeePTOValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
      PTOCommentBO ptoCommentBO = new PTOCommentsBOImpl(dbHandle);
      ReturnObject<PTOCommentsEnum, JsonObject> boResponse =
              ptoCommentBO.updateComment(dbHandle, requestParams);
      PTOCommentsHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  /**
   * Writes response according to PTOCommentsEnum
   * @param params
   * @param httpServerResponse
   * @param dbHandle
   * @param boResponse
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  public static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   ReturnObject<PTOCommentsEnum, JsonObject> boResponse)
      throws SQLException, DbHandleException {
    JsonObject dataObject = new JsonObject();
    dataObject.put("message", boResponse.getStatusEnum().getDescription());
    switch (boResponse.getStatusEnum()) {
      case EXISTS_ALREADY:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            boResponse.getStatusEnum().getDescription(),
            boResponse.getStatusEnum().getDescription(),
            httpServerResponse);
        break;
      case ADDITION_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
      case ADDITION_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Comment Created Successfully", dataObject, httpServerResponse);
        break;
      case DELETE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Comment Deleted Successfully", dataObject, httpServerResponse);
        break;
      case CANNOT_DELETE:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            boResponse.getStatusEnum().getDescription(),
            boResponse.getStatusEnum().getDescription(),
            httpServerResponse);
        break;
      case NO_ACCESS:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            boResponse.getStatusEnum().getDescription(),
            boResponse.getStatusEnum().getDescription(),
            httpServerResponse);
        break;
      case UPDATE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Comment updated Successfully", dataObject, httpServerResponse);
        break;
      case NO_SUCH_VALUE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "No such comments present", dataObject, httpServerResponse);
        break;
      case NO_UPDATE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "No new changes", dataObject, httpServerResponse);
        break;
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }
}
