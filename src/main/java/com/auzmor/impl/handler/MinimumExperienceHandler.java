package com.auzmor.impl.handler;

import com.auzmor.bo.MinimumExperienceBO;
import com.auzmor.impl.bo.MinimumExperienceBOImpl;
import com.auzmor.impl.enumarator.bo.MinimumExperienceEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.FieldOptions.MinimumExperienceModel;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.Validator;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MinimumExperienceHandler {
  private static Logger logger = LoggerFactory.getLogger(MinimumExperienceHandler.class);

  public static void addMinimumExperience(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    MinimumExperienceModel minimumExperienceModel = new MinimumExperienceModel();

    String[] allowedParams = {"minimum_experience", "organization_id", "active_status"};
    String[] requiredParams = {"minimum_experience", "organization_id"};
    String[] titleParams = {"minimum_experience", "organization_id", "active_status"};

    List<Validator> validators = new ArrayList<>();
    Validator titleValidator = new ValidatorImpl(titleParams, MinimumExperienceModel.getValidatorMap());
    validators.add(titleValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          MinimumExperienceBO minimumExperienceBO = new MinimumExperienceBOImpl();
          ReturnObject<MinimumExperienceEnum, JsonObject> boResponse =
              minimumExperienceBO.addMinimumExperience(dbHandle, requestParams);
          MinimumExperienceHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  public static void getMinimumExperience(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    MinimumExperienceModel minimumExperienceModel = new MinimumExperienceModel();
    
    String[] allowedParams = {"id"};
    String[] requiredParams = {"id"};
    String[] titleParams = {"id"};

    List<Validator> validators = new ArrayList<>();
    Validator titleValidator = new ValidatorImpl(titleParams, MinimumExperienceModel.getValidatorMap());
    validators.add(titleValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          MinimumExperienceBO minimumExperienceBO = new MinimumExperienceBOImpl();
          ReturnObject<MinimumExperienceEnum, JsonObject> boResponse =
              minimumExperienceBO.getMinimumExperience(dbHandle, requestParams);
          MinimumExperienceHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  public static void getAllMinimumExperiences(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    MinimumExperienceModel minimumExperienceModel = new MinimumExperienceModel();

    String[] allowedParams = {"organization_id"};
    String[] requiredParams = {"organization_id"};
    String[] titleParams = {"organization_id"};

    List<Validator> validators = new ArrayList<>();
    Validator titleValidator = new ValidatorImpl(titleParams, MinimumExperienceModel.getValidatorMap());
    validators.add(titleValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          MinimumExperienceBO minimumExperienceBO = new MinimumExperienceBOImpl();
          ReturnObject<MinimumExperienceEnum, JsonArray> boResponse =
              minimumExperienceBO.getAllMinimumExperience(dbHandle, requestParams);
          switch (boResponse.getStatusEnum()) {
            case VALUE_SUCCESS:
              dbHandle.commit();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  "no_data is false", boResponse.getData(), httpServerResponse);
              break;
            case NO_SUCH_VALUE:
              dbHandle.rollback();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  "no_data is true", boResponse.getData(), httpServerResponse);
              break;
            default:
              dbHandle.rollback();
              ResponseHelper.writeInternalServerError(httpServerResponse);
              break;
          }
        });
  }

  public static void updateMinimumExperience(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    MinimumExperienceModel minimumExperienceModel = new MinimumExperienceModel();

    String[] allowedParams = {"id", "minimum_experience", "organization_id", "active_status"};
    String[] requiredParams = {"id", "minimum_experience"};
    String[] titleParams = {"id", "minimum_experience", "organization_id", "active_status"};

    List<Validator> validators = new ArrayList<>();
    Validator titleValidator = new ValidatorImpl(titleParams, MinimumExperienceModel.getValidatorMap());
    validators.add(titleValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          MinimumExperienceBO minimumExperienceBO = new MinimumExperienceBOImpl();
          ReturnObject<MinimumExperienceEnum, JsonObject> boResponse =
              minimumExperienceBO.updateMinimumExperience(dbHandle, requestParams);
          MinimumExperienceHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  public static void deleteMinimumExperience(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    MinimumExperienceModel minimumExperienceModel = new MinimumExperienceModel();

    String[] allowedParams = {"id"};
    String[] requiredParams = {"id"};
    String[] titleParams = {"id"};

    List<Validator> validators = new ArrayList<>();
    Validator titleValidator = new ValidatorImpl(titleParams, MinimumExperienceModel.getValidatorMap());
    validators.add(titleValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          MinimumExperienceBO minimumExperienceBO = new MinimumExperienceBOImpl();
          ReturnObject<MinimumExperienceEnum, JsonObject> boResponse =
              minimumExperienceBO.deleteMinimumExperience(dbHandle, requestParams);
          MinimumExperienceHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  public static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   ReturnObject<MinimumExperienceEnum, JsonObject> boResponse)
      throws SQLException, DbHandleException {

    JsonObject dataObject = new JsonObject();
    dataObject.put("message", boResponse.getStatusEnum().getDescription());
    switch (boResponse.getStatusEnum()) {
      case EXISTS_ALREADY:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            params.get("minimum_experience") + " already exists",
            params.get("minimum_experience") + " already exists",
            httpServerResponse);
        break;
      case ADDITION_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Minimum Experience added successfully", dataObject, httpServerResponse);
        break;
      case VALUE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Minimum Experience retrieved successfully", boResponse.getData(), httpServerResponse);
        break;
      case ADDITION_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
      case UPDATE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Minimum Experience updated successfully", dataObject, httpServerResponse);
        break;
      case NO_CHANGES_MADE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "No changes made", dataObject, httpServerResponse);
        break;
      case NO_SUCH_VALUE:
        dbHandle.rollback();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_BAD_REQUEST,
            "Unable to find Minimum Experience", dataObject,
            httpServerResponse);
        break;
      case CANNOT_DELETE:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Minimum Experience cannot be deleted, Please reassign Employees",
            "Minimum Experience cannot be deleted, Please reassign Employees",
            httpServerResponse);
        break;
      case DELETE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Minimum Experience deleted successfully", dataObject, httpServerResponse);
        break;
      case RESTRICTED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Published Minimum Experience cannot be changed",
            "Published Minimum Experience cannot be changed",
            httpServerResponse);
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }
  
}
