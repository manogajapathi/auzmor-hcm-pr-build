package com.auzmor.impl.handler;

import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.impl.builder.ApiBuilder;

import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.sql.Connection;
import java.sql.ResultSet;

public class AccessHandler extends ApiBuilder {
    public void getAllAccess(RoutingContext routingContext){
        MultiMap params = routingContext.request().params();
        DatabaseUtil databaseUtil = new DatabaseUtil();
        try{
            Connection connectionObject = new MysqldbConf().getConnection();
            String role = params.get("role_name");
            int org_id = Integer.parseInt(params.get("organization_id"));

            String roleQuery = "select entity_id from Role_Assignment where role_id in (select role_id from Roles where role_name = '"+role+"' and organization_id = "+org_id+")";

            ResultSet resultSetAccess = getResultSetObject(connectionObject,roleQuery);

            String entity_id = "";
            while (resultSetAccess.next()){
                entity_id = resultSetAccess.getString("entity_id");
            }

            String field_name = "entity_method,entity_api";
            String table_name = "Entity_List";
            String constriant = "where id in ("+entity_id+")";

            JsonArray dataArray = databaseUtil.getJsonArrayOption(connectionObject,field_name,table_name,constriant);

            connectionObject.close();

            getSuccessObject(dataArray,routingContext.response(),"GET");

        }catch (Exception e){
            e.printStackTrace();
            String errorDescription = e.getMessage();
            getErrorObject("Invalid Data",errorDescription,routingContext.response(),"GET");
        }
    }

    public void getCheckPermissionAllowed(RoutingContext routingContext) {
        MultiMap params = routingContext.request().params();
        try{
            Connection connectionObject = new MysqldbConf().getConnection();
            JsonObject dataObject = new JsonObject();
            //String role = params.get("role_name");
            int user_id = Integer.parseInt(params.get("user_id"));
            String entity_api = params.get("entity_api");
            String entity_method = params.get("entity_method");
            int org_id = Integer.parseInt(params.get("organization_id"));
            // first role_id,entity_class,entity_method.
            // select role_id from Role_Assignment where entity_id in ( select id from Entity_List where entity_class = 'Holiday' and entity_method='addHoliday') and user_id = '54' and organization_id ='1' and inactive_status = 0;

            // getting entity id from ENtity List
            String entity_id = new DatabaseUtil().getTablename("Entity_List","id","entity_api = '"+entity_api.trim()+"' and entity_method = '"+entity_method+"'");

            String access_type = "select role_id from Role_Assignment where entity_id like '%"+entity_id+"%' and user_id = "+user_id+" and organization_id ="+org_id+" and inactive_status = 0";

            System.out.println(access_type);
            ResultSet resultSetAccess = getResultSetObject(connectionObject,access_type);
            boolean accesscontrol = false;
            while (resultSetAccess.next()){
             if(resultSetAccess.getInt("role_id") > 0){
                 accesscontrol = true;
             }else{
                 accesscontrol= false;
             }
            }

            connectionObject.close();

            dataObject.put("access_control",accesscontrol);

            getSuccessObject(dataObject,routingContext.response(),"GET");

        }catch (Exception e){
            e.printStackTrace();
            String errorDescription = e.getMessage();
            getErrorObject("Invalid Data",errorDescription,routingContext.response(),"GET");
        }
    }
}
