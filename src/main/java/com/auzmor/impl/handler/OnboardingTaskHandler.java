package com.auzmor.impl.handler;

import com.auzmor.bo.OnboardingTaskBO;
import com.auzmor.impl.bo.OnboardingTaskBOImpl;
import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.enumarator.bo.OnboardingTaskEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.onboarding.OnboardingTask;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.FieldOptions;
import com.auzmor.util.validator.Validator;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OnboardingTaskHandler {
  /**
   * Add a new task
   * @param context
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 08/MAR/2018
   */
  public static void addTask(RoutingContext context) {
    HttpServerRequest request = context.request();
    HttpServerResponse response = context.response();
    MultiMap params = request.params();
    OnboardingTask onboardingTask = new OnboardingTask();

    String[] allowedParams = {"packet_id","task_name", "task_description", "task_file_path", "allow_doc_upload",
        "viewing_required", "doc_upload_required"};
    String[] requiredParams = {"packet_id","task_name"};
    String[] taskParams = {"packet_id", "task_name", "task_description", "task_file_path", "allow_doc_upload",
        "viewing_required", "doc_upload_required"};

    List<Validator> validators = new ArrayList<>();
    Validator formValidator = new ValidatorImpl(taskParams, onboardingTask.getValidatorMap());
    validators.add(formValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(context, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          OnboardingTaskBO onboardingTaskBO = new OnboardingTaskBOImpl();
          ReturnObject<OnboardingTaskEnum, JsonObject> boResponse =
              onboardingTaskBO.addTask(dbHandle, requestParams);
          OnboardingTaskHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  /**
   * updateOnboardingTask - Update Onboarding Task
   *
   * @param routingContext
   * @author Pradeep Balasubramanian
   */
  public static void updateOnboardingTask(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    OnboardingTask onboardingTask = new OnboardingTask();

    String[] allowedParams = {"taskId", "packet_id","task_name", "task_description", "task_file_path", "allow_doc_upload",
        "viewing_required", "doc_upload_required"};
    String[] requiredParams = {"taskId", "packet_id","task_name"};
    String[] taskParams = {"taskId", "packet_id", "task_name", "task_description", "task_file_path", "allow_doc_upload",
        "viewing_required", "doc_upload_required"};

    List<Validator> validators = new ArrayList<>();
    Validator formValidator = new ValidatorImpl(taskParams, onboardingTask.getValidatorMap());
    validators.add(formValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          OnboardingTaskBO onboardingTaskBO = new OnboardingTaskBOImpl();
          ReturnObject<OnboardingTaskEnum, JsonObject> boResponse =
              onboardingTaskBO.updateTask(dbHandle, requestParams);
          OnboardingTaskHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  /**
   * deleteOnboardingTask : Delete Onboarding Task
   *
   * @param context
   * @author Pradeep Balasubramanian
   */
  public static void deleteOnboardingTask(RoutingContext context) {
    HttpServerRequest request = context.request();
    HttpServerResponse response = context.response();
    MultiMap params = context.request().params();

    String[] allowedParams = {"taskId"};
    String[] requiredParams = {"taskId"};
    String[] taskParams = {"taskId"};

    List<Validator> validators = new ArrayList<>();
    Validator fieldValidator = new ValidatorImpl(taskParams, new HashMap<String, FieldOptions>() {{
      put("taskId", FieldOptionsCommon.NUMBER);
    }});
    validators.add(fieldValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(context, false,
        (httpServerResponse, requestParams, dbHandle) -> {
        OnboardingTaskBO onboardingTaskBO = new OnboardingTaskBOImpl();
          ReturnObject<OnboardingTaskEnum, JsonObject> boResponse =
              onboardingTaskBO.deleteTask(dbHandle, requestParams);
          OnboardingTaskHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }


  /**
   * Write response based on OnboardingTaskEnum values
   * @param params
   * @param httpServerResponse
   * @param dbHandle
   * @param boResponse
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 08/MAR/2018
   */
  public static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   ReturnObject<OnboardingTaskEnum, JsonObject> boResponse)
      throws SQLException, DbHandleException {
    switch (boResponse.getStatusEnum()) {
      case ADD_FAILURE:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
      case UPDATE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_ACCEPTED,
            "Task details updated successfully", boResponse.getData(), httpServerResponse);
        break;
      case ADD_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Task details added successfully", boResponse.getData(), httpServerResponse);
        break;
      case VALUE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "Details retrived", boResponse.getData(), httpServerResponse);
        break;
      case NO_SUCH_VALUE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "No Task details present", new JsonObject(), httpServerResponse);
        break;
      case UPDATE_FAILURE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "No values to be updated", new JsonObject(), httpServerResponse);
        break;
      case ALREADY_EXISTS:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_BAD_REQUEST,
            "Task Already exists",
            "Task Already exists",
            httpServerResponse);
        break;
      case DELETE_FAILURE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "No values to be deleted", new JsonObject(), httpServerResponse);
        break;
      case DELETE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Task deleted successfully", boResponse.getData(), httpServerResponse);
        break;
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }
}
