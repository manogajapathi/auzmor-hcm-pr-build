package com.auzmor.impl.handler;

import com.auzmor.bo.HighestEducationBO;
import com.auzmor.impl.bo.HighestEducationBOImpl;
import com.auzmor.impl.enumarator.bo.HighestEducationEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.FieldOptions.HighestEducationModel;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.Validator;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HighestEducationHandler {
  private static Logger logger = LoggerFactory.getLogger(HighestEducationHandler.class);

  public static void addHighestEducation(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    HighestEducationModel highestEducationModel = new HighestEducationModel();

    String[] allowedParams = {"highest_education", "organization_id", "active_status"};
    String[] requiredParams = {"highest_education", "organization_id"};
    String[] titleParams = {"highest_education", "organization_id", "active_status"};

    List<Validator> validators = new ArrayList<>();
    Validator titleValidator = new ValidatorImpl(titleParams, HighestEducationModel.getValidatorMap());
    validators.add(titleValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          HighestEducationBO highestEducationBO = new HighestEducationBOImpl();
          ReturnObject<HighestEducationEnum, JsonObject> boResponse =
              highestEducationBO.addHighestEducation(dbHandle, requestParams);
          HighestEducationHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  public static void getHighestEducation(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    HighestEducationModel highestEducationModel = new HighestEducationModel();

    String[] allowedParams = {"id"};
    String[] requiredParams = {"id"};
    String[] titleParams = {"id"};

    List<Validator> validators = new ArrayList<>();
    Validator titleValidator = new ValidatorImpl(titleParams, HighestEducationModel.getValidatorMap());
    validators.add(titleValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          HighestEducationBO highestEducationBO = new HighestEducationBOImpl();
          ReturnObject<HighestEducationEnum, JsonObject> boResponse =
              highestEducationBO.getHighestEducation(dbHandle, requestParams);
          HighestEducationHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  public static void getAllHighestEducations(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    HighestEducationModel highestEducationModel = new HighestEducationModel();

    String[] allowedParams = {"organization_id"};
    String[] requiredParams = {"organization_id"};
    String[] titleParams = {"organization_id"};

    List<Validator> validators = new ArrayList<>();
    Validator titleValidator = new ValidatorImpl(titleParams, HighestEducationModel.getValidatorMap());
    validators.add(titleValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
         HighestEducationBO highestEducationBO = new HighestEducationBOImpl();
         ReturnObject<HighestEducationEnum, JsonArray> boResponse =
             highestEducationBO.getAllHighestEducation(dbHandle, requestParams);
          switch (boResponse.getStatusEnum()) {
            case VALUE_SUCCESS:
              dbHandle.commit();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  "no_data is false", boResponse.getData(), httpServerResponse);
              break;
            case NO_SUCH_VALUE:
              dbHandle.rollback();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  "no_data is true", boResponse.getData(), httpServerResponse);
              break;
            default:
              dbHandle.rollback();
              ResponseHelper.writeInternalServerError(httpServerResponse);
              break;
          }
        });
  }

  public static void updateHighestEducation(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    HighestEducationModel highestEducationModel = new HighestEducationModel();

    String[] allowedParams = {"id", "highest_education", "organization_id", "active_status"};
    String[] requiredParams = {"id", "highest_education"};
    String[] titleParams = {"id", "highest_education", "organization_id", "active_status"};

    List<Validator> validators = new ArrayList<>();
    Validator titleValidator = new ValidatorImpl(titleParams, HighestEducationModel.getValidatorMap());
    validators.add(titleValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
        HighestEducationBO highestEducationBO = new HighestEducationBOImpl();
        ReturnObject<HighestEducationEnum, JsonObject> boResponse =
              highestEducationBO.updateHighestEducation(dbHandle, requestParams);
          HighestEducationHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  public static void deleteHighestEducation(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    HighestEducationModel highestEducationModel = new HighestEducationModel();

    String[] allowedParams = {"id"};
    String[] requiredParams = {"id"};
    String[] titleParams = {"id"};

    List<Validator> validators = new ArrayList<>();
    Validator titleValidator = new ValidatorImpl(titleParams, HighestEducationModel.getValidatorMap());
    validators.add(titleValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
        HighestEducationBO highestEducationBO = new HighestEducationBOImpl();
        ReturnObject<HighestEducationEnum, JsonObject> boResponse =
            highestEducationBO.deleteHighestEducation(dbHandle, requestParams);
        HighestEducationHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  public static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   ReturnObject<HighestEducationEnum, JsonObject> boResponse)
      throws SQLException, DbHandleException {

    JsonObject dataObject = new JsonObject();
    dataObject.put("message", boResponse.getStatusEnum().getDescription());
    switch (boResponse.getStatusEnum()) {
      case EXISTS_ALREADY:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            params.get("highest_education") + " already exists",
            params.get("highest_education") + " already exists",
            httpServerResponse);
        break;
      case ADDITION_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Highest Education added successfully", dataObject, httpServerResponse);
        break;
      case VALUE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Highest Education retrieved successfully", boResponse.getData(), httpServerResponse);
        break;
      case ADDITION_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
      case UPDATE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Highest Education updated successfully", dataObject, httpServerResponse);
        break;
      case NO_CHANGES_MADE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "No changes made", dataObject, httpServerResponse);
        break;
      case NO_SUCH_VALUE:
        dbHandle.rollback();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_BAD_REQUEST,
            "Unable to find Highest Education", dataObject,
            httpServerResponse);
        break;
      case CANNOT_DELETE:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Highest Education cannot be deleted, Please reassign Employees",
            "Highest Education cannot be deleted, Please reassign Employees",
            httpServerResponse);
        break;
      case DELETE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Highest Education deleted successfully", dataObject, httpServerResponse);
        break;
      case RESTRICTED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Highest Education already used in candidates",
            "Highest Education already used in candidates",
            httpServerResponse);
        break;
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }
}
