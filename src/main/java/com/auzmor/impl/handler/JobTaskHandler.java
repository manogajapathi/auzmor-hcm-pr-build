package com.auzmor.impl.handler;

import com.auzmor.bo.JobBO;
import com.auzmor.bo.JobTaskBO;
import com.auzmor.impl.bo.JobBOImpl;
import com.auzmor.impl.bo.JobTaskBOImpl;
import com.auzmor.impl.builder.ResponseBuilder;
import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.enumarator.TitleEnum;
import com.auzmor.impl.enumarator.UserTypeEnum;
import com.auzmor.impl.enumarator.bo.JobTaskBOEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.Job.Collaborator;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.model.Job.JobTaskModel;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.FieldOptions;
import com.auzmor.util.validator.Validator;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class JobTaskHandler {
  private static final Logger logger = LoggerFactory.getLogger(JobTaskHandler.class);

  /**
   * Add a new task to employee/collaborator for a particular job based on job_id
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 02/MAR/2018
   */
  public static void addTaskForJob(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    JobTaskModel jobTaskModel = new JobTaskModel();

    String[] allowedParams = {"job_id", "employee_id", "task_name", "candidate_id",
        "all_flag","complete_within", "due_date", "creator_id"};
    String[] requiredParams = {"job_id", "employee_id", "task_name", "creator_id"};
    String[] taskParams = {"job_id", "employee_id", "task_name", "candidate_id",
        "all_flag","complete_within", "due_date", "creator_id"};

    List<Validator> validators = new ArrayList<>();
    Validator taskValidator = new ValidatorImpl(taskParams, JobTaskModel.getValidatorMap());
    validators.add(taskValidator);



    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          JobTaskBO jobTaskBO = new JobTaskBOImpl();
          UserTypeEnum userType = (UserTypeEnum) (routingContext.data().get("userType"));

          if (userType != UserTypeEnum.ADMIN) {
            Employee employee = (Employee) routingContext.data().get("employee");
            JobBO jobBO = new JobBOImpl(dbHandle);
            try {
              long jobId = Long.parseLong(params.get("job_id"));
              Job jobDetails = jobBO.getJobDetails((long) jobId);
              if (Long.parseLong(jobDetails.getHiring_lead()) != employee.getEmployee_id()) {
                ResponseHelper.writeErrorResponse(org.apache.commons.httpclient.HttpStatus.SC_FORBIDDEN, "cannot consume this service",
                    "cannot consume this service", routingContext.response());
                return;
              }
            } catch (SQLException e) {
              logger.error("SQL Exception in Job Update");
              ResponseBuilder.writeInternalServerError(routingContext.response());
              return;
            } catch (DbHandleException e) {
              logger.error("SQL Exception in Job Update");
              ResponseBuilder.writeInternalServerError(routingContext.response());
              return;
            }
          }
          try {
            ReturnObject<JobTaskBOEnum, JsonObject> boResponse =
                jobTaskBO.addJobTask(dbHandle, requestParams);
            JobTaskHandler.writeResponse(params,httpServerResponse,dbHandle,boResponse);
          } catch (ParseException e) {
            logger.error("Parse Exception occurs "+e.getMessage());
            ResponseHelper.writeInternalServerError(httpServerResponse);
          }
        });
  }

  /**
   * Get all tasks assign to an employee/collaborator based on employee_id
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 02/MAR/2018
   */
  public static void getTasksForEmployee(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();

    String[] allowedParams = {"employee_id"};
    String[] requiredParams = {"employee_id"};
    String[] taskParams = {"employee_id"};

    List<Validator> validators = new ArrayList<>();
    Validator taskValidator = new ValidatorImpl(taskParams, JobTaskModel.getValidatorMap());
    validators.add(taskValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
        JobTaskBO jobTaskBO = new JobTaskBOImpl();
          try {
            Employee employee = (Employee) routingContext.data().get("employee");
            ReturnObject<JobTaskBOEnum, JsonArray> boResponse =
                jobTaskBO.getTasksForEmployee(dbHandle, employee.getEmployee_id());
            switch (boResponse.getStatusEnum()) {
              case VALUE_SUCCESS:
                dbHandle.commit();
                ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                    "no_data is false", boResponse.getData(), httpServerResponse);
                break;
              case NO_VALUES:
                dbHandle.commit();
                ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                    "no_data is true", boResponse.getData(), httpServerResponse);
                break;
              default:
                dbHandle.rollback();
                ResponseHelper.writeInternalServerError(httpServerResponse);
                break;
            }
          } catch (ParseException e) {
            logger.error(e.getMessage());
            ResponseHelper.writeInternalServerError(response);
          }
        });

  }

  /**
   * To mark complete flag once employee/collaborator completes the task
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 02/MAR/2018
   */
  public static void completeTasks(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    JobTaskModel jobTaskModel = new JobTaskModel();

    String[] allowedParams = {"task_array"};
    String[] requiredParams = {"task_array"};
    String[] taskParams = {"task_array"};

    List<Validator> validators = new ArrayList<>();
    Validator fieldValidator = new ValidatorImpl(taskParams, new HashMap<String, FieldOptions>() {{
      put("task_array", FieldOptionsCommon.ANY);
    }});
    validators.add(fieldValidator);

    if (StringUtil.printValidString(params.get("task_array")).trim().length() == 0) {
      ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST, "Please select atleast " +
          "one task to complete","Please select atleast one task to complete", response);
    }

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
         Employee employee = (Employee) routingContext.data().get("employee");
         JobTaskBO jobTaskBO = new JobTaskBOImpl();
         List<Long> jobTasks = jobTaskBO.getTaskIdByEmployeeId(dbHandle, employee.getEmployee_id());
         for (String taskId : params.get("task_array").split(",")) {
           if (!jobTasks.contains(Long.parseLong(taskId))) {
             ResponseHelper.writeErrorResponse(org.apache.commons.httpclient.HttpStatus.SC_FORBIDDEN, "cannot consume this service",
                 "cannot consume this service", routingContext.response());
             return;
           }
         }
          ReturnObject<JobTaskBOEnum, JsonObject> boResponse =
              jobTaskBO.completeTask(dbHandle, requestParams);
          JobTaskHandler.writeResponse(params,httpServerResponse,dbHandle,boResponse);
        });
  }

  /**
   * To write responses based on JobTaskBOEnum values
   * @param params
   * @param httpServerResponse
   * @param dbHandle
   * @param boResponse
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 02/MAR/2018
   */
  public static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   ReturnObject<JobTaskBOEnum, JsonObject> boResponse)
      throws SQLException, DbHandleException {
    JsonObject dataObject = new JsonObject();
    dataObject.put("message", boResponse.getStatusEnum().getDescription());
    switch (boResponse.getStatusEnum()) {
      case ALREADY_EXISTS:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Task Already assigned to employee",
            "Task Already assigned to employee",
            httpServerResponse);
        break;
      case TASK_ADDED:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Task Created Successfully", dataObject, httpServerResponse);
        break;
      case TASK_ADD_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Task cannot be added",
            "Task cannot be added",
            httpServerResponse);
        break;
      case FAILED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Failed due to server issue",
            "Task cannot be added",
            httpServerResponse);
        break;
      case VALUE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Task Details Successfully", boResponse.getData(), httpServerResponse);
        break;
      case MISSING_DUE:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Any one of the completion parameter to be selected",
            "Any one of the completion parameter to be selected",
            httpServerResponse);
        break;
      case MISSING_COMPLETE_WITHIN:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Complete within cannot be null",
            "Complete within cannot be null",
            httpServerResponse);
        break;
      case UPDATE_TASK_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Task completed Successfully", boResponse.getData(), httpServerResponse);
        break;
      case NO_VALUES:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Task completed Successfully", boResponse.getData(), httpServerResponse);
        break;
      case CURRENT_CANDIDATES:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Task completed Successfully", dataObject, httpServerResponse);
        break;
      case FUTURE_DATE:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Due date should be in future",
            "Due date should be in future",
            httpServerResponse);
        break;
      /*case IS_COLLABORATOR:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_OK,
            "Collaborator can not add task",
            "Collaborator is not having access to add task",
            httpServerResponse);
        break;*/
      default:
        dbHandle.commit();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }
}
