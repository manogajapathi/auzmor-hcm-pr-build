package com.auzmor.impl.handler;

import com.auzmor.bo.OnboardingDocumentBO;
import com.auzmor.impl.bo.OnboardingDocumentBOImpl;
import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.enumarator.bo.OnboardingDocumentEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.onboarding.OnboardingDocument;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.FieldOptions;
import com.auzmor.util.validator.Validator;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.dbutils.DbUtils;
import org.apache.http.HttpStatus;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OnboardingDocumentHandler {
  /**
   * addDocument - A Post Method REST API Implementation for adding document
   *
   * @param context
   * @author Pradeep Balasubramanian
   * @lastModifiedDate Nov 8, 2017
   */
  public static void addDocument(RoutingContext context) {
    HttpServerRequest request = context.request();
    HttpServerResponse response = context.response();
    MultiMap params = context.request().params();
    OnboardingDocument onboardingDocument = new OnboardingDocument();

    String[] allowedParams = {"packet_id", "organization_id", "document_name", "document_file_path",
        "viewing_reqd", "signing_required", "page", "x_coordinate", "y_coordinate"};
    String[] requiredParams = {"packet_id", "organization_id", "document_name"};
    String[] documentParams = {"packet_id", "organization_id", "document_name", "document_file_path",
        "viewing_reqd", "signing_required", "page", "x_coordinate", "y_coordinate"};

    List<Validator> validators = new ArrayList<>();
    Validator formValidator = new ValidatorImpl(documentParams, onboardingDocument.getValidatorMap());
    validators.add(formValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(context, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          OnboardingDocumentBO onboardingDocumentBO = new OnboardingDocumentBOImpl();
          ReturnObject<OnboardingDocumentEnum, JsonObject> boResponse =
              onboardingDocumentBO.addDocument(dbHandle, requestParams);
          OnboardingDocumentHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  /**
   * deleteOnboardingDocument : delete onboarding document from a packet
   *
   * @param context
   * @author Pradeep Balasubramanian
   */
  public static void deleteOnboardingDocument(RoutingContext context) {
    HttpServerRequest request = context.request();
    HttpServerResponse response = context.response();
    MultiMap params = context.request().params();

    String[] allowedParams = {"documentId"};
    String[] requiredParams = {"documentId"};
    String[] taskParams = {"documentId"};

    List<Validator> validators = new ArrayList<>();
    Validator fieldValidator = new ValidatorImpl(taskParams, new HashMap<String, FieldOptions>() {{
      put("documentId", FieldOptionsCommon.NUMBER);
    }});
    validators.add(fieldValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(context, false,
        (httpServerResponse, requestParams, dbHandle) -> {
        OnboardingDocumentBO onboardingDocumentBO = new OnboardingDocumentBOImpl();
        ReturnObject<OnboardingDocumentEnum, JsonObject> boResponse =
              onboardingDocumentBO.deleteDocument(dbHandle, requestParams);
        OnboardingDocumentHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  /**
   * updateOnboardingDocument update Onboarding Document
   *
   * @param routingContext
   * @author Pradeep Balasubramanian
   */
  public static void updateOnboardingDocument(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = routingContext.request().params();
    OnboardingDocument onboardingDocument = new OnboardingDocument();

    String[] allowedParams = {"documentId","packet_id", "organization_id", "document_name", "document_file_path",
        "viewing_reqd", "signing_required", "page", "x_coordinate", "y_coordinate"};
    String[] requiredParams = {"documentId","packet_id", "organization_id", "document_name"};
    String[] documentParams = {"documentId","packet_id", "organization_id", "document_name", "document_file_path",
        "viewing_reqd", "signing_required", "page", "x_coordinate", "y_coordinate"};

    List<Validator> validators = new ArrayList<>();
    Validator formValidator = new ValidatorImpl(documentParams, onboardingDocument.getValidatorMap());
    validators.add(formValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          OnboardingDocumentBO onboardingDocumentBO = new OnboardingDocumentBOImpl();
          ReturnObject<OnboardingDocumentEnum, JsonObject> boResponse =
              onboardingDocumentBO.updateDocument(dbHandle, requestParams);
          OnboardingDocumentHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  /**
   * Write response based on OnboardingDocumentEnum values
   * @param params
   * @param httpServerResponse
   * @param dbHandle
   * @param boResponse
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 08/MAR/2018
   */
  public static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   ReturnObject<OnboardingDocumentEnum, JsonObject> boResponse)
      throws SQLException, DbHandleException {
    switch (boResponse.getStatusEnum()) {
      case ADD_FAILURE:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
      case UPDATE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_ACCEPTED,
            "Document details updated successfully", boResponse.getData(), httpServerResponse);
        break;
      case ADD_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Document details added successfully", boResponse.getData(), httpServerResponse);
        break;
      case VALUE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "Details retrived", boResponse.getData(), httpServerResponse);
        break;
      case NO_SUCH_VALUE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "No Document details present", boResponse.getData(), httpServerResponse);
        break;
      case UPDATE_FAILURE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "No values to be updated", boResponse.getData(), httpServerResponse);
        break;
      case ALREADY_EXISTS:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(org.apache.http.HttpStatus.SC_BAD_REQUEST,
            "Document Already exists",
            "Document Already exists",
            httpServerResponse);
        break;
      case DELETE_FAILURE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(org.apache.http.HttpStatus.SC_OK,
            "No document to be deleted", boResponse.getData(), httpServerResponse);
        break;
      case DELETE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Document deleted successfully", boResponse.getData(), httpServerResponse);
        break;
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }
}
