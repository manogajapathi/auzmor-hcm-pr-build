package com.auzmor.impl.handler;

import com.auzmor.bo.DivisionBO;
import com.auzmor.impl.bo.DivisionBOImpl;
import com.auzmor.impl.enumarator.DivisionEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.FieldOptions.DivisionModel;

import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.Validator;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DivisionHandler {
  private static final Logger logger = LoggerFactory.getLogger(DivisionHandler.class);

  /**
   * Add a new division for field options
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  public static void addDivision(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    DivisionModel divisionModel = new DivisionModel();

    String[] allowedParams = {"division_name", "organization_id", "active_status"};
    String[] requiredParams = {"division_name", "organization_id"};
    String[] divisionParams = {"division_name", "organization_id", "active_status"};

    List<Validator> validators = new ArrayList<>();
    Validator divisionValidator = new ValidatorImpl(divisionParams, DivisionModel.getValidatorMap());
    validators.add(divisionValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          DivisionBO divisionBO = new DivisionBOImpl();
          divisionModel.setDivisionName(requestParams.get("division_name"));
          divisionModel.setOrganizationId(Integer.valueOf(requestParams.get("organization_id")));

          ReturnObject<DivisionEnum, JsonObject> boResponse =
              divisionBO.addDivision(dbHandle,divisionModel);
          DivisionHandler.writeResponse(requestParams,httpServerResponse,dbHandle,boResponse);
        });
  }

  /**
   * Gets particular division
   *
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */

  public static void updateDivision(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    DivisionModel divisionModel = new DivisionModel();

    String[] allowedParams = {"division_name", "divisionId", "organization_id", "active_status"};
    String[] requiredParams = {"division_name", "divisionId"};
    String[] divisionParams = {"division_name", "divisionId", "organization_id", "active_status"};

    List<Validator> validators = new ArrayList<>();
    Validator divisionValidator = new ValidatorImpl(divisionParams, DivisionModel.getValidatorMap());
    validators.add(divisionValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          DivisionBO divisionBO = new DivisionBOImpl();
          ReturnObject<DivisionEnum, JsonObject> boResponse =
              divisionBO.updateDivision(dbHandle,requestParams);
          DivisionHandler.writeResponse(requestParams,httpServerResponse,dbHandle,boResponse);
        });
  }

  /**
   * Get job title details for particular id
   * @param routingContext
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 12/FEB/2018
   *
   */
  public static void getDivision(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    DivisionModel divisionModel = new DivisionModel();

    String[] allowedParams = {"divisionId"};
    String[] requiredParams = {"divisionId"};
    String[] divisionParams = {"divisionId"};

    List<Validator> validators = new ArrayList<>();
    Validator titleValidator = new ValidatorImpl(divisionParams, DivisionModel.getValidatorMap());
    validators.add(titleValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          divisionModel.setDivisionId(Integer.valueOf(requestParams.get("divisionId")));
          DivisionBO divisionBO = new DivisionBOImpl();
          ReturnObject<DivisionEnum, JsonObject> boResponse = divisionBO.getDivision(dbHandle,divisionModel);
          DivisionHandler.writeResponse(requestParams,httpServerResponse,dbHandle,boResponse);
        });

  }

  /**
   * Gets details of Divisions
   *
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */

  public static void getAllDivisions(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    DivisionModel divisionModel = new DivisionModel();

    String[] allowedParams = {"organizationId"};
    String[] requiredParams = {"organizationId"};
    String[] divisionParams = {"organizationId"};

    List<Validator> validators = new ArrayList<>();
    Validator divisionValidator = new ValidatorImpl(divisionParams, DivisionModel.getValidatorMap());
    validators.add(divisionValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          DivisionBO divisionBO = new DivisionBOImpl();
          divisionModel.setOrganizationId(Integer.valueOf(requestParams.get("organizationId")));
          ReturnObject<DivisionEnum, JsonArray> boResponse =
              divisionBO.getAllDivisions(dbHandle,divisionModel);
          switch (boResponse.getStatusEnum()) {
            case VALUE_SUCCESS:
              dbHandle.commit();
              ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
                  "", boResponse.getData(), httpServerResponse);
              break;
            case NO_SUCH_VALUE:
              dbHandle.rollback();
              ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
                  "Unable to find Division", "Unable to find Division",
                  httpServerResponse);
              break;
            default:
              dbHandle.rollback();
              ResponseHelper.writeInternalServerError(httpServerResponse);
              break;
          }
        });
  }

  /**
   * Delete particular division
   *
   * @param routingContext
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */

  public static void deleteDivision(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = request.params();
    DivisionModel divisionModel = new DivisionModel();

    String[] allowedParams = {"divisionId"};
    String[] requiredParams = {"divisionId"};
    String[] divisionParams = {"divisionId"};

    List<Validator> validators = new ArrayList<>();
    Validator divisionValidator = new ValidatorImpl(divisionParams, DivisionModel.getValidatorMap());
    validators.add(divisionValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          DivisionBO divisionBO = new DivisionBOImpl();
          divisionModel.setDivisionId(Integer.valueOf(requestParams.get("divisionId")));
          ReturnObject<DivisionEnum, JsonObject> boResponse =
              divisionBO.deleteDivision(dbHandle,divisionModel);
          DivisionHandler.writeResponse(requestParams,response,dbHandle,boResponse);
        });
  }

  /**
   * Writing correct response according to enum values
   * @param params
   * @param httpServerResponse
   * @param dbHandle
   * @param boResponse
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */

  public static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   ReturnObject<DivisionEnum, JsonObject> boResponse)
      throws SQLException, DbHandleException {

    JsonObject dataObject = new JsonObject();
    dataObject.put("message", boResponse.getStatusEnum().getDescription());
    switch (boResponse.getStatusEnum()) {
      case EXISTS_ALREADY:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            params.get("division_name") + " already exists",
            params.get("division_name") + " already exists",
            httpServerResponse);
        break;
      case ADDITION_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Division added successfully", dataObject, httpServerResponse);
        break;
      case VALUE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Division added successfully", boResponse.getData(), httpServerResponse);
        break;
      case ADDITION_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
      case UPDATE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Division updated successfully", dataObject, httpServerResponse);
        break;
      case NO_SUCH_VALUE:
        dbHandle.rollback();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Unable to find Division", dataObject,
            httpServerResponse);
        break;
      case CANNOT_DELETE:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Unable to add Division", "Division cannot be added",
            httpServerResponse);
        break;
      case DELETE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Division deleted successfully", dataObject, httpServerResponse);
        break;
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;

    }
  }
}
