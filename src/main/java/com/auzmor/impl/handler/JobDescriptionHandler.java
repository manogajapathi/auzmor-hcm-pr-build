package com.auzmor.impl.handler;

import com.auzmor.bo.JobDescriptionBO;
import com.auzmor.impl.bo.JobDescriptionBOImpl;
import com.auzmor.impl.builder.ResponseBuilder;
import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.enumarator.LocationEnum;
import com.auzmor.impl.enumarator.bo.JobDescriptionEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.exception.util.ApplicationUtilException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.ParamHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.Job.JobTaskModel;
import com.auzmor.impl.model.JobDescriptionModel;
import com.auzmor.impl.util.ModelUtil;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.validator.ValidatorImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.validator.Validator;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class JobDescriptionHandler {
  private static final Logger logger = LoggerFactory.getLogger(JobDescriptionHandler.class);
  private Map<Integer, JobDescriptionModel> jobDescData = new LinkedHashMap<Integer, JobDescriptionModel>();

  JobDescriptionModel jobDesc = new JobDescriptionModel();

  public static void getJobDescId(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = routingContext.request().params();
    JobDescriptionModel jobDescModel = new JobDescriptionModel();

    String[] allowedParams = {"id"};
    String[] requiredParams = {"id"};
    String[] taskParams = {"id"};

    List<Validator> validators = new ArrayList<>();
    Validator taskValidator = new ValidatorImpl(taskParams, JobDescriptionModel.getValidatorMap());
    validators.add(taskValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
         JobDescriptionBO jobDescriptionBO = new JobDescriptionBOImpl();
         ReturnObject<JobDescriptionEnum, JsonObject> boResponse =
             jobDescriptionBO.getJobDescription(dbHandle, requestParams);
         JobDescriptionHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  public static void getAllJobDescription(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = routingContext.request().params();
    JobDescriptionModel jobDescModel = new JobDescriptionModel();

    String[] allowedParams = {"organization_id"};
    String[] requiredParams = {"organization_id"};
    String[] taskParams = {"organization_id"};

    List<Validator> validators = new ArrayList<>();
    Validator taskValidator = new ValidatorImpl(taskParams, JobDescriptionModel.getValidatorMap());
    validators.add(taskValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
         JobDescriptionBO jobDescriptionBO = new JobDescriptionBOImpl();
         ReturnObject<JobDescriptionEnum, JsonArray> boResponse =
          jobDescriptionBO.getAllJobDescriptions(dbHandle, requestParams);
         switch (boResponse.getStatusEnum()) {
           case VALUE_SUCCESS:
            dbHandle.commit();
            ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
              "no_data is false", boResponse.getData(), httpServerResponse);
            break;
           case NO_SUCH_VALUE:
            dbHandle.rollback();
            ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
              "no_data is true", new JsonArray(),
              httpServerResponse);
            break;
           default:
            dbHandle.rollback();
            ResponseHelper.writeInternalServerError(httpServerResponse);
            break;
         }
        });
  }

  public static void addJobDescription(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = routingContext.request().params();
    JobDescriptionModel jobDescModel = new JobDescriptionModel();

    String[] allowedParams = {"jobdesctype", "jobdescdetails", "organization_id"};
    String[] requiredParams = {"jobdesctype", "jobdescdetails", "organization_id"};
    String[] taskParams = {"jobdesctype", "jobdescdetails", "organization_id"};

    List<Validator> validators = new ArrayList<>();
    Validator taskValidator = new ValidatorImpl(taskParams, JobDescriptionModel.getValidatorMap());
    validators.add(taskValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          JobDescriptionBO jobDescriptionBO = new JobDescriptionBOImpl();
          ReturnObject<JobDescriptionEnum, JsonObject> boResponse =
              jobDescriptionBO.addJobDescription(dbHandle, requestParams);
          JobDescriptionHandler.writeResponse(requestParams,httpServerResponse,dbHandle,boResponse);
        });
  }

  public static void updateJobDesc(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = routingContext.request().params();
    JobDescriptionModel jobDescModel = new JobDescriptionModel();

    String[] allowedParams = {"id", "jobdesctype", "jobdescdetails", "organization_id"};
    String[] requiredParams = {"id", "jobdesctype"};
    String[] taskParams = {"id", "jobdesctype", "jobdescdetails", "organization_id"};

    List<Validator> validators = new ArrayList<>();
    Validator taskValidator = new ValidatorImpl(taskParams, JobDescriptionModel.getValidatorMap());
    validators.add(taskValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          JobDescriptionBO jobDescriptionBO = new JobDescriptionBOImpl();
          ReturnObject<JobDescriptionEnum, JsonObject> boResponse =
              jobDescriptionBO.updateJobDescription(dbHandle, requestParams);
          JobDescriptionHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  public static void deleteJobDesc(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    MultiMap params = routingContext.request().params();
    JobDescriptionModel jobDescModel = new JobDescriptionModel();

    String[] allowedParams = {"id"};
    String[] requiredParams = {"id"};
    String[] taskParams = {"id"};

    List<Validator> validators = new ArrayList<>();
    Validator taskValidator = new ValidatorImpl(taskParams, JobDescriptionModel.getValidatorMap());
    validators.add(taskValidator);

    if (!ParamHelper.isParamsValid(response, params, allowedParams, requiredParams, validators)) {
      return;
    }

    DbHelper.safeExecuteDbHandleMethod(routingContext, false,
        (httpServerResponse, requestParams, dbHandle) -> {
          JobDescriptionBO jobDescriptionBO = new JobDescriptionBOImpl();
          ReturnObject<JobDescriptionEnum, JsonObject> boResponse =
              jobDescriptionBO.deleteJobDescription(dbHandle, requestParams);
          JobDescriptionHandler.writeResponse(requestParams, httpServerResponse, dbHandle, boResponse);
        });
  }

  public static void writeResponse(MultiMap params, HttpServerResponse httpServerResponse,
                                   DbHandle dbHandle,
                                   ReturnObject<JobDescriptionEnum, JsonObject> boResponse)
      throws SQLException, DbHandleException {

    JsonObject dataObject = new JsonObject();
    dataObject.put("message", boResponse.getStatusEnum().getDescription());
    switch (boResponse.getStatusEnum()) {
      case EXISTS_ALREADY:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            params.get("jobdesctype") + " already exists",
            params.get("jobdesctype") + " already exists",
            httpServerResponse);
        break;
      case ADDITION_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_CREATED,
            "Description added successfully", dataObject, httpServerResponse);
        break;
      case VALUE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "", boResponse.getData(), httpServerResponse);
        break;
      case ADDITION_FAILED:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
      case UPDATE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "Description updated successfully", dataObject, httpServerResponse);
        break;
      case NO_CHANGES_MADE:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_ACCEPTED,
            "No changes made", dataObject, httpServerResponse);
        break;
      case NO_SUCH_VALUE:
        dbHandle.rollback();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Unable to find Description", dataObject,
            httpServerResponse);
        break;
      case DELETE_SUCCESS:
        dbHandle.commit();
        ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK,
            "Description deleted successfully", dataObject, httpServerResponse);
        break;
      case CANNOT_DELETE:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Location cannot be deleted, Please reassign Employees",
            "Location cannot be deleted, Please reassign Employees",
            httpServerResponse);
        break;
      case RESTRICTED:
        dbHandle.rollback();
        ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST,
            "Published Job Description cannot be changed",
            "Published Job Description cannot be changed",
            httpServerResponse);
        break;
      default:
        dbHandle.rollback();
        ResponseHelper.writeInternalServerError(httpServerResponse);
        break;
    }
  }
}
