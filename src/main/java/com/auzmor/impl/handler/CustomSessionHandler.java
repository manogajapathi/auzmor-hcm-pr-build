package com.auzmor.impl.handler;

import com.auth0.jwt.JWT;
import com.auzmor.impl.bo.CandidateBOImpl;
import com.auzmor.impl.bo.EmployeeBOImpl;
import com.auzmor.impl.bo.LoginBOImpl;
import com.auzmor.impl.bo.OrganizationBOImpl;
import com.auzmor.impl.dao.RoleDAOImpl;
import com.auzmor.impl.enumarator.UserTypeEnum;
import com.auzmor.impl.enumarator.kong.KongHttpStatusCode;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.KongServiceHelper;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.helper.ServiceDiscoveryHelper;
import com.auzmor.impl.model.Login;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.Role;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.util.db.DbHandle;

import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.Cookie;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.ServiceReference;
import org.apache.http.HttpStatus;
import org.apache.logging.log4j.ThreadContext;
import org.slf4j.MDC;

import javax.jws.soap.SOAPBinding;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class CustomSessionHandler {
  private static final Logger logger = LoggerFactory.getLogger(CustomSessionHandler.class);

  /**
   *
   * @param routingContext
   */
  public static void accessInit(RoutingContext routingContext) {
    logger.info("Initializing the thread for the request");
    MultiMap headers = routingContext.request().headers();
    setLoggerThreadValues(headers);
    DbHandle dbHandle = DbHelper.getDBHandle(routingContext, false);
    try {
      if (setRoutingValuesAndRoute(routingContext, dbHandle)) {
        routingContext.next();
      }
    } catch (SQLException e) {
      ResponseHelper.writeInternalServerError(routingContext.response());
    } catch (DbHandleException e) {
      ResponseHelper.writeInternalServerError(routingContext.response());
    } finally {
      dbHandle.checkAndCloseConnection();
    }
  }

  /**
   *
   * @param routingContext
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 21/MAR/2018
   */
  public static void logout(RoutingContext routingContext) {
    logger.info("Logging out user");
    Cookie cookie = routingContext.getCookie("auth-token");
    JWT jwt = JWT.decode(cookie.getValue());
    HttpClient httpClient = KongServiceHelper.executeWithKongClient(routingContext.vertx());
    logger.info("Delete credentials from kong JWT");
    KongServiceHelper.deleteCredential(httpClient, routingContext.request().headers().get("X-Consumer-ID"),
        jwt.getIssuer(), httpClientResponse -> {
          if (KongHttpStatusCode.DELETE.getStatusCode() == httpClientResponse.statusCode()) {
            logger.info("Delete cookie");
            Cookie expireCookie = Cookie.cookie("auth-token", ""  ).setDomain(
                routingContext.request().getHeader("X-Real-Host") != null ?
                    routingContext.request().getHeader("X-Real-Host") : "")
                .setPath("/").setMaxAge(0);
            if (!Vertx.currentContext().config().getString("app.env").equals("dev")) {
              expireCookie.setSecure(true);
            }
            routingContext.addCookie(expireCookie);
            ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK, "Logged out successfully",
                "", routingContext.response());
          } else if (HttpStatus.SC_NO_CONTENT == httpClientResponse.statusCode()) {
            ResponseHelper.writeErrorResponse(HttpStatus.SC_NO_CONTENT, "Not a valid session",
                "", routingContext.response());
          } else {
            ResponseHelper.writeInternalServerError(routingContext.response());
          }
        });

  }

  private static void setLoggerThreadValues(MultiMap headers) {
    MDC.put("request_id", "R:" + UUID.randomUUID().toString());
    MDC.put("consumer_id", "U:" + headers.get("X-Consumer-ID"));
    MDC.put("real_ip", "IP:" + headers.get("X-Real-IP"));
  }

  /**
   *
   * @param routingContext
   * @param dbHandle
   * @throws SQLException
   * @throws DbHandleException
   */
  private static boolean setRoutingValuesAndRoute(RoutingContext routingContext, DbHandle dbHandle)
      throws SQLException, DbHandleException {
    MultiMap headers =  routingContext.request().headers();
    Organization organization = new OrganizationBOImpl(dbHandle)
        .getOrganizationWithDomainName(headers.get("X-Real-Host"));
    Login login = null;
    Employee employee = null;
    // CandidateR candidate = null;
    boolean isNonAnonymous = false;
    if (null == organization) {
      logger.info("Unable to recognize the host");
      ResponseHelper.writeErrorResponse(HttpStatus.SC_BAD_REQUEST, "Not a valid url",
          "Unable to trace the host in the request", routingContext.response());
      return false;
    }

    if (null == headers.get("X-Anonymous-Consumer") ||
        !headers.get("X-Anonymous-Consumer").equals("true")) {
      login = new LoginBOImpl(dbHandle).getLoginById(Long.parseLong(headers.get("X-Consumer-Custom-ID")));
      routingContext.data().put("userType", getUserTypeEnum(login.getUserType()));
      isNonAnonymous = true;
    }

    if (isNonAnonymous && login.getOrganizationId() != organization.getOrganization_id()) {
      logger.info("Unable to recognize the host");
      ResponseHelper.writeErrorResponse(HttpStatus.SC_UNAUTHORIZED, "Not Authorized",
          "Not authorized to access", routingContext.response());
      return false;
    }

    List<Role> roles = null;
    if (isNonAnonymous) {
      /*if (login.getUserType().equals("candidate")) {
         candidate = new CandidateBOImpl(dbHandle).getCandidateByLoginId(login.getId());
      } else { */
      if (!login.getUserType().equals("candidate")) {
        employee = new EmployeeBOImpl(dbHandle).getEmployeeDetailsByLoginId(login.getId());
      }
      roles = new RoleDAOImpl(dbHandle).getRoles(Arrays.stream(headers
          .get("X-Consumer-Groups").split(",")).map(role -> Long.parseLong(role.trim()))
          .collect(Collectors.toList()));
    }

    routingContext.data().put("organization", organization);
    routingContext.data().put("login", login);
    //routingContext.data().put("candidate", candidate);
    routingContext.data().put("employee", employee);
    routingContext.data().put("roles", roles);
    routingContext.data().put("isAnonymous", !isNonAnonymous);

    return true;
  }


  private static UserTypeEnum getUserTypeEnum(String userType) {
    if (userType.equals(UserTypeEnum.ADMIN.getUserTypeValue())) {
      return UserTypeEnum.ADMIN;
    }
    if (userType.equals(UserTypeEnum.EMPLOYEE.getUserTypeValue())) {
      return UserTypeEnum.EMPLOYEE;
    }
    return UserTypeEnum.CANDIDATE;
  }
}
