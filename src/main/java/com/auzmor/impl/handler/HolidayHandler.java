package com.auzmor.impl.handler;

import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.model.Holiday.HolidayModel;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.impl.util.ModelUtil;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.builder.ApiBuilder;

import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.RoutingContext;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HolidayHandler extends ApiBuilder{

    private static final Logger logger = LoggerFactory.getLogger(HolidayHandler.class);
    public void addHoliday(RoutingContext routingContext){
        HolidayModel holidayModel = new HolidayModel();
        MultiMap params = routingContext.request().params();
        String departmentParam = "";
        String locationParam = "";
        String divisionParam = "";
        String payGroupParam = "";
        try{
            Connection connectionObject = new MysqldbConf().getConnection();
            for (String key : params.names()) {
                if (key.contains("department")) {
                    departmentParam = params.get(key);
                }if (key.contains("location")) {
                    locationParam = params.get(key);
                }if (key.contains("division")) {
                    divisionParam = params.get(key);
                }if (key.contains("pay_group")) {
                    payGroupParam = params.get(key);
                }else {
                    ModelUtil.setValues(key, params.get(key), holidayModel);
                }
            }

            if(holidayModel.getDate().equals("")||holidayModel.getDate().equals(null)){
                throw new Exception("Required params missing");
            }

            if (params.get("assign_to").contains("Specific group of people")){
                if (StringUtil.printValidString(params.get("location")).equals("")
                        && StringUtil.printValidString(params.get("department")).equals("")
                        && StringUtil.printValidString(params.get("division")).equals("")
                        && StringUtil.printValidString(params.get("pay_group")).equals("")){

                    throw new Exception("Please select any group");
                }
            }

            String departmentString = "";
            String divisionString = "";
            String locationString = "";
            String payGroupString = "";
            if (!StringUtil.printValidString(departmentParam).equals("{}")
                    && !StringUtil.printValidString(departmentParam).equals("")) {
                departmentString = calculateFromArray(departmentParam);
                holidayModel.setDepartment(departmentString);
                params.add("department",departmentString);
            }else{
                departmentString = "";
                holidayModel.setDepartment(departmentString);
                params.add("department",departmentString);
            }

            if (!StringUtil.printValidString(locationParam).equals("{}")
                    && !StringUtil.printValidString(locationParam).equals("")) {
                locationString = calculateFromArray(locationParam);
                holidayModel.setLocation(locationString);
                params.add("location",locationString);
            }else{
                locationString = "";
                holidayModel.setLocation(locationString);
                params.add("department",departmentString);
            }

            if (!StringUtil.printValidString(divisionParam).equals("{}")
                    && !StringUtil.printValidString(divisionParam).equals("")) {
                divisionString = calculateFromArray(divisionParam);
                holidayModel.setDivision(divisionString);
                params.add("division",divisionString);
            }else{
                divisionString = "";
                holidayModel.setDivision(divisionString);
                params.add("department",departmentString);
            }

            if (!StringUtil.printValidString(payGroupParam).equals("{}")
                    && !StringUtil.printValidString(payGroupParam).equals("")) {
                payGroupString = calculateFromArray(payGroupParam);
                holidayModel.setPay_group(payGroupString);
                params.add("pay_group",payGroupString);
            }else{
                payGroupString = "";
                holidayModel.setPay_group(payGroupString);
                params.add("department",departmentString);
            }

            List<String> constraintList = new ArrayList<>();

            constraintList.add("inactive_status = 0");
            constraintList.add("holiday_name = '"+holidayModel.getHoliday_name()+"'");
            constraintList.add("list_id = "+holidayModel.getList_id());
            constraintList.add("organization_id = "+holidayModel.getOrganization_id());

            String constraintString = DatabaseUtil.buildConstraintString(constraintList);

            Map<String, String> queryStringParameters = new HashMap<>();
            queryStringParameters.put("fields","holiday_id");
            queryStringParameters.put("table","Holiday");
            queryStringParameters.put("constraint", constraintString);

            String holidayListQueryString = buildQuery(queryStringParameters,"SELECT");

            ResultSet resultSetHoliday = getResultSetObject(connectionObject,holidayListQueryString);

            int existingId = 0;
            while (resultSetHoliday.next()){
                existingId = resultSetHoliday.getInt("holiday_id");
            }

            if(existingId!=0){
                throw new Exception("Holiday Name already exists");
            }

            constraintList = new ArrayList<>();

            constraintList.add("inactive_status = 0");
            constraintList.add("date = '"+holidayModel.getDate()+"'");
            constraintList.add("list_id = "+holidayModel.getList_id());
            constraintList.add("organization_id = "+holidayModel.getOrganization_id());

            String constraintStringForDate = DatabaseUtil.buildConstraintString(constraintList);

            queryStringParameters = new HashMap<>();
            queryStringParameters.put("fields","holiday_id");
            queryStringParameters.put("table","Holiday");
            queryStringParameters.put("constraint", constraintStringForDate);

            holidayListQueryString = buildQuery(queryStringParameters,"SELECT");

            resultSetHoliday = getResultSetObject(connectionObject,holidayListQueryString);

            existingId = 0;
            while (resultSetHoliday.next()){
                existingId = resultSetHoliday.getInt("holiday_id");
            }

            if(existingId!=0){
                throw new Exception("Holiday Date already exists");
            }



            Map<String, Boolean> paramMapper = new HashMap<>();

            paramMapper.put("holiday_name",true);
            paramMapper.put("date",true);
            paramMapper.put("list_id",true);
            paramMapper.put("organization_id",true);
            paramMapper.put("to_date",false);
            paramMapper.put("assign_to",false);
            paramMapper.put("division",false);
            paramMapper.put("department",false);
            paramMapper.put("pay_group",false);
            paramMapper.put("location",false);
           // paramMapper.put("employees",false);
            paramMapper.put("paid_flag",false);

            Map<String, Object> insertionObjects = generateInsertionObject(params, paramMapper, holidayModel);

            List<String> queryStringValueList = DatabaseUtil.prepareQueryStringValues(insertionObjects);
            String columnValueString = queryStringValueList.get(0);
            String entryValueString = queryStringValueList.get(1);

            Map<String, String> insertQueryParameters = new HashMap<>();
            insertQueryParameters.put("table", "Holiday");
            insertQueryParameters.put("column_names", columnValueString);
            insertQueryParameters.put("column_values", entryValueString);

            String sqlQuery = buildQuery(insertQueryParameters, "INSERT");

            int id = insertRecord(sqlQuery);
            holidayModel.setHoliday_id(id);

            JsonObject successObject = new JsonObject();
            successObject.put("holiday_id", id);
            successObject.put("list_id", holidayModel.getList_id());
            successObject.put("holiday_name", holidayModel.getHoliday_name());
            successObject.put("date", holidayModel.getDate());
            successObject.put("to_date", holidayModel.getTo_date());
            successObject.put("organization_id", holidayModel.getOrganization_id());
            successObject.put("assign_to", holidayModel.getAssign_to());

            JsonArray tempArray;
            if (!(StringUtil.printValidString(holidayModel.getLocation()).equals(""))) {
                String locationFromDB = holidayModel.getLocation();
                String[] locations = locationFromDB.split(",");

                tempArray = new JsonArray();
                for (String location : locations) {
                    tempArray.add(location);
                }

                successObject.put("location", tempArray);
            }else{
                successObject.put("location",new JsonArray());
            }

            if (!(StringUtil.printValidString(holidayModel.getDepartment()).equals(""))) {
                String departmentFromDB = holidayModel.getDepartment();
                String[] departments = departmentFromDB.split(",");

                tempArray = new JsonArray();
                for (String department : departments) {
                    tempArray.add(department);
                }

                successObject.put("department", tempArray);
            }else{
                successObject.put("department",new JsonArray());
            }

            if (!(StringUtil.printValidString(holidayModel.getDivision()).equals(""))) {
                String divisionFromDB = holidayModel.getDivision();
                String[] divisions = divisionFromDB.split(",");

                tempArray = new JsonArray();
                for (String division : divisions) {
                    tempArray.add(division);
                }

                successObject.put("division", tempArray);
            }else{
                successObject.put("division",new JsonArray());
            }

            if (!(StringUtil.printValidString(holidayModel.getPay_group()).equals(""))) {
                String[] payGroups = holidayModel.getPay_group().split(",");
                tempArray = new JsonArray();
                for (String payGroup : payGroups) {
                    tempArray.add(payGroup);
                }

                successObject.put("pay_group", tempArray);
            }else{
                successObject.put("pay_group",new JsonArray());
            }

            /*successObject.put("division", holidayModel.getDivision());
            successObject.put("department", holidayModel.getDepartment());
            successObject.put("location", holidayModel.getLocation());
            successObject.put("pay_group", holidayModel.getPay_group());*/
           // successObject.put("employees", holidayModel.getEmployees());
            successObject.put("paid_flag", holidayModel.isPaid_flag());

            connectionObject.close();
            getSuccessObject(successObject,routingContext.response(),"POST");


        }catch (Exception exception){
            logger.error(exception.getMessage());
            exception.printStackTrace();
            String errorDescription = exception.getMessage();
            getErrorObject("Invalid Data",errorDescription,routingContext.response(),"POST");
        }

    }

    public void getAllHolidays(RoutingContext routingContext){
        try{
            Connection connectionObject = new MysqldbConf().getConnection();
            HttpServerRequest request = routingContext.request();

            //String id = request.getParam("organization_id");

            String holidayIdString = request.getParam("list_id");

            List<String> constraintList = new ArrayList<>();

            if(holidayIdString.equals("")|| holidayIdString.equals(null)){
                throw new Exception("Required params missing");
            }

            /*if (id != null || !(id.equals(""))) {
                constraintList.add("organization_id = " + id);
            }*/

            constraintList.add("inactive_status = 0");

            if(holidayIdString.equals(""))
                throw new Exception("Required Params missing");

            if (holidayIdString != null || !(holidayIdString.equals(""))) {
                constraintList.add("list_id = " + holidayIdString);
            }else{
                throw new Exception("Required Params missing");
            }

            String constraintString = DatabaseUtil.buildConstraintString(constraintList);
            Map<String, String> queryStringParameters = new HashMap<>();
            queryStringParameters.put("fields","holiday_id,holiday_name,date,to_date,assign_to,location,department," +
                    "division,pay_group,paid_flag");
            queryStringParameters.put("table","Holiday");
            queryStringParameters.put("constraint", constraintString);

            String holidaysQuery = buildQuery(queryStringParameters,"SELECT");

            ResultSet resultSetObject = getResultSetObject(connectionObject,holidaysQuery);

            JsonArray HolidayArray = new JsonArray();
            String columnName = "";
            String columnType = "";

            while (resultSetObject.next()) {
                JsonObject jsonDataObject = new JsonObject();
                ResultSetMetaData resultSetMetaData = resultSetObject.getMetaData();
                for (int i = 1; i <= resultSetObject.getMetaData().getColumnCount(); i++) {
                    columnName = resultSetMetaData.getColumnName(i);
                    columnType = resultSetMetaData.getColumnTypeName(i);

                    if ("BIT".equalsIgnoreCase(columnType)) {
                        jsonDataObject.put(columnName, resultSetObject.getBoolean(columnName));
                    } else if ("INT".equalsIgnoreCase(columnType)) {
                        jsonDataObject.put(columnName, resultSetObject.getInt(columnName));
                    } else if ("VARCHAR".equalsIgnoreCase(columnType) || "BLOB".equalsIgnoreCase(columnType)) {
                        String columnValue = resultSetObject.getString(columnName);
                        jsonDataObject.put(columnName, columnValue);
                    } else if ("Date".equalsIgnoreCase(columnType)) {
                        jsonDataObject.put(columnName, resultSetObject.getString(columnName));
                    }
                }

                JsonArray tempArray ;
                if (!(StringUtil.printValidString(jsonDataObject.getString("location")).equals(""))) {
                    String locationFromDB = jsonDataObject.getString("location");
                    String[] locations = locationFromDB.split(",");

                    tempArray = new JsonArray();
                    for (String location : locations) {
                        tempArray.add(location);
                    }

                    jsonDataObject.put("location", tempArray);
                }else{
                    jsonDataObject.put("location", new JsonArray());
                }


                if (!(StringUtil.printValidString(jsonDataObject.getString("department")).equals(""))) {
                    String departmentFromDB = jsonDataObject.getString("department");
                    String[] departments = departmentFromDB.split(",");

                    tempArray = new JsonArray();
                    for (String department : departments) {
                        tempArray.add(department);
                    }

                    jsonDataObject.put("department", tempArray);
                }else{
                    jsonDataObject.put("department", new JsonArray());
                }


                if (!(StringUtil.printValidString(jsonDataObject.getString("division")).equals(""))) {
                    String divisionFromDB = jsonDataObject.getString("division");
                    String[] divisions = divisionFromDB.split(",");

                    tempArray = new JsonArray();
                    for (String division : divisions) {
                        tempArray.add(division);
                    }

                    jsonDataObject.put("division", tempArray);
                }else{
                    jsonDataObject.put("division", new JsonArray());
                }

                if (!(StringUtil.printValidString(jsonDataObject.getString("pay_group")).equals(""))) {
                    String[] payGroups = jsonDataObject.getString("pay_group").split(",");
                    tempArray = new JsonArray();
                    for (String payGroup : payGroups) {
                        tempArray.add(payGroup);
                    }

                    jsonDataObject.put("pay_group", tempArray);
                }else{
                    jsonDataObject.put("pay_group", new JsonArray());
                }

                /*JsonArray employeeArray = new JsonArray();
                if(!(jsonDataObject.getString("employees").equals(""))){
                    String listOfEmployees = jsonDataObject.getString("employees");

                    String[] employees = listOfEmployees.split(",");
                    for(String employee:employees){
                        queryStringParameters = new HashMap<>();
                        queryStringParameters.put("fields","first_name,last_name");
                        queryStringParameters.put("table","Employee");
                        queryStringParameters.put("constraint", "where employee_id = "+employee);

                        String employeeString = buildQuery(queryStringParameters,"SELECT");

                        ResultSet employeeResult = getResultSetObject(connectionObject,holidaysQuery);
                        JsonObject employeeObject = new JsonObject();

                        while (employeeResult.next()) {
                            employeeObject.put("first_name",employeeResult.getString(1));
                            employeeObject.put("last_name",employeeResult.getString(2));
                            employeeObject.put("employee_id",employee);
                        }
                        employeeArray.add(employeeObject);
                    }
                }*//*
                jsonDataObject.put("employees",employeeArray);*/

                HolidayArray.add(jsonDataObject);
            }

            connectionObject.close();

            getSuccessObject(HolidayArray,routingContext.response(),"GET");

        }catch(Exception exception){
            exception.printStackTrace();
            String errorDescription = exception.getMessage();
            getErrorObject("Invalid Data",errorDescription,routingContext.response(),"GET");
        }
    }

    public void getHoliday(RoutingContext routingContext){
        try{
            Connection connectionObject = new MysqldbConf().getConnection();
            HttpServerRequest request = routingContext.request();

            String id = request.getParam("organization_id");

            String holidayId = request.getParam("holiday_id");

            List<String> constraintList = new ArrayList<>();

            if (id != null) {
                constraintList.add("organization_id = " + id);
            }

            constraintList.add("inactive_status = 0");
            System.out.println("Holiday::'"+holidayId+"'");
            if(holidayId=="")
                throw new Exception("Required Params missing");
            if (holidayId != null || holidayId!="") {
                constraintList.add("holiday_id = " + holidayId);
            }else{
                throw new Exception("Required Params missing");
            }

            String constraintString = DatabaseUtil.buildConstraintString(constraintList);

            Map<String, String> queryStringParameters = new HashMap<>();
            queryStringParameters.put("fields","holiday_id,holiday_name,date,to_date,assign_to,location,department," +
                    "division,pay_group,paid_flag");
            queryStringParameters.put("table","Holiday");
            queryStringParameters.put("constraint", constraintString);

            String holidayListQueryString = buildQuery(queryStringParameters,"SELECT");

            String columnName = "";
            String columnType = "";

            ResultSet resultSetHoliday = getResultSetObject(connectionObject,holidayListQueryString);

            JsonObject resultObject = new JsonObject();

            while (resultSetHoliday.next()){
                ResultSetMetaData resultSetMetaData = resultSetHoliday.getMetaData();
                for (int i = 1; i <= resultSetHoliday.getMetaData().getColumnCount(); i++) {
                    columnName = resultSetMetaData.getColumnName(i);
                    columnType = resultSetMetaData.getColumnTypeName(i);

                    if ("BIT".equalsIgnoreCase(columnType)) {
                        resultObject.put(columnName, resultSetHoliday.getBoolean(columnName));
                    } else if ("INT".equalsIgnoreCase(columnType)) {
                        resultObject.put(columnName, resultSetHoliday.getInt(columnName));
                    } else if ("VARCHAR".equalsIgnoreCase(columnType) || "BLOB".equalsIgnoreCase(columnType)) {
                        String columnValue = resultSetHoliday.getString(columnName);
                        resultObject.put(columnName, columnValue);
                    } else if ("Date".equalsIgnoreCase(columnType)) {
                        resultObject.put(columnName, resultSetHoliday.getString(columnName));
                    }
                }
                JsonArray tempArray;

                if (!(resultObject.getString("location").equals(""))) {
                    String locationFromDB = resultObject.getString("location");
                    String[] locations = locationFromDB.split(",");

                    tempArray = new JsonArray();
                    for (String location : locations) {
                        tempArray.add(location);
                    }

                    resultObject.put("location", tempArray);
                }else{
                    resultObject.put("location", new JsonArray());
                }

                if (!(StringUtil.printValidString(resultObject.getString("department")).equals(""))) {
                    String departmentFromDB = resultObject.getString("department");
                    String[] departments = departmentFromDB.split(",");

                    tempArray = new JsonArray();
                    for (String department : departments) {
                        tempArray.add(department);
                    }

                    resultObject.put("department", tempArray);
                }else{
                    resultObject.put("department", new JsonArray());
                }

                if (!(StringUtil.printValidString(resultObject.getString("division")).equals(""))) {
                    String divisionFromDB = resultObject.getString("division");
                    String[] divisions = divisionFromDB.split(",");

                    tempArray = new JsonArray();
                    for (String division : divisions) {
                        tempArray.add(division);
                    }

                    resultObject.put("division", tempArray);
                }else{
                    resultObject.put("division", new JsonArray());
                }

                if (!(StringUtil.printValidString(resultObject.getString("pay_group")).equals(""))) {
                    String[] payGroups = resultObject.getString("pay_group").split(",");
                    tempArray = new JsonArray();
                    for (String payGroup : payGroups) {
                        tempArray.add(payGroup);
                    }

                    resultObject.put("pay_group", tempArray);
                }else{
                    resultObject.put("pay_group", new JsonArray());
                }

                /*JsonArray employeeArray = new JsonArray();
                if(!(resultObject.getString("employees").equals(""))){
                    String listOfEmployees = resultObject.getString("employees");

                    String[] employees = listOfEmployees.split(",");
                    for(String employee:employees){
                        queryStringParameters = new HashMap<>();
                        queryStringParameters.put("fields","first_name,last_name");
                        queryStringParameters.put("table","Employee");
                        queryStringParameters.put("constraint", "where employee_id = "+employee);

                        String employeeString = buildQuery(queryStringParameters,"SELECT");

                        ResultSet employeeResult = getResultSetObject(connectionObject,employeeString);
                        JsonObject employeeObject = new JsonObject();

                        while (employeeResult.next()) {
                            employeeObject.put("first_name",employeeResult.getString(1));
                            employeeObject.put("last_name",employeeResult.getString(2));
                            employeeObject.put("employee_id",employee);
                        }
                        employeeArray.add(employeeObject);
                    }
                }
                resultObject.put("employees",employeeArray);*/
            }
            connectionObject.close();

            getSuccessObject(resultObject,routingContext.response(),"GET");

        }catch (Exception exception){
            exception.printStackTrace();
            String errorDescription = exception.getMessage();
            getErrorObject("Invalid Data",errorDescription,routingContext.response(),"GET");
        }
    }

    public void updateHoliday(RoutingContext routingContext){
        String holiday_id = routingContext.request().getParam("holiday_id");
        MultiMap params = routingContext.request().params();
        JsonObject jsonResponseObject = new JsonObject();

        String updateString = "";
        String departmentParam = "";
        String locationParam = "";
        String divisionParam = "";
        String payGroupParam = "";
        try{
            Connection connection = new MysqldbConf().getConnection();

            for (String key : params.names()) {
                if (key.contains("department")) {
                    departmentParam = params.get(key);
                }else if (key.contains("location")) {
                    locationParam = params.get(key);
                }else if (key.contains("division")) {
                    divisionParam = params.get(key);
                }else if (key.contains("pay_group")) {
                    payGroupParam = params.get(key);
                }
            }

            String departmentString = "";
            String divisionString = "";
            String locationString = "";
            String payGroupString = "";

            System.out.println(!StringUtil.printValidString(departmentParam).equals("{}") || !StringUtil.printValidString(departmentParam).equals(""));
            if (!StringUtil.printValidString(departmentParam).equals("{}")
                    && !StringUtil.printValidString(departmentParam).equals("")) {
                departmentString = calculateFromArray(departmentParam);
                params.add("department",departmentString);
            }else{
                departmentString = "";
                params.add("department",departmentString);
            }

            if (!StringUtil.printValidString(locationParam).equals("{}")
                    && !StringUtil.printValidString(locationParam).equals("")) {
                locationString = calculateFromArray(locationParam);
                params.add("location",locationString);
            }else{
                locationString = "";
                params.add("location",locationString);
            }

            if (!StringUtil.printValidString(divisionParam).equals("{}")
                    && !StringUtil.printValidString(divisionParam).equals("")) {
                divisionString = calculateFromArray(divisionParam);
                params.add("division",divisionString);
            }else{
                divisionString = "";
                params.add("division",divisionString);
            }

            if (!StringUtil.printValidString(payGroupParam).equals("{}")
                    && !StringUtil.printValidString(payGroupParam).equals("")) {
                payGroupString = calculateFromArray(payGroupParam);
                params.add("pay_group",payGroupString);
            }else{
                payGroupString = "";
                params.add("pay_group",payGroupString);
            }

            String updateStatement = generateUpdateStatement(params, new HolidayModel());

            if(params.get("holiday_name").equals("")|| params.get("holiday_name").equals(null)){
                throw new Exception("Required params missing");
            }

            if (params.get("assign_to").contains("Specific group of people")){
                if (StringUtil.printValidString(params.get("location")).equals("")
                        && StringUtil.printValidString(params.get("department")).equals("")
                        && StringUtil.printValidString(params.get("division")).equals("")
                        && StringUtil.printValidString(params.get("pay_group")).equals("")){

                    throw new Exception("Please select any group");
                }
            }


            String constraintString = "where holiday_id = " + holiday_id;

            Map<String, String> updateQueryParameters = new HashMap<>();
            updateQueryParameters.put("table", "Holiday");
            updateQueryParameters.put("key_value_pair", updateStatement);
            updateQueryParameters.put("constraint", constraintString);


            updateString = buildQuery(updateQueryParameters, "UPDATE");

            int updatedRows = getUpdatedRows(updateString);
            if (updatedRows == 0) {
                throw new Exception("Invalid request to update Holiday");
            }



            DatabaseUtil dbutil = new DatabaseUtil();
            String name = dbutil.getTablename("Holiday","concat(holiday_name,'|',list_id)","holiday_id = " + holiday_id);
            String[] splitter = name.split("\\|");
            String holiday_name = splitter[0];
            String list_id = splitter[1];
            jsonResponseObject.put("message", " "+holiday_name+" updated Successfully! ");
            getSuccessObject(jsonResponseObject, routingContext.response(), "PUT");

            checkCompletedStatus(connection,list_id);
            connection.close();


        }catch(Exception exception){
            exception.printStackTrace();
            String errorDescription = exception.getMessage();
            getErrorObject("Invalid Data",errorDescription,routingContext.response(),"PUT");
        }
    }

    public void deleteHoliday(RoutingContext routingContext){
        MultiMap params = routingContext.request().params();
        try{
            int holiday_id = Integer.valueOf(params.get("holiday_id"));

            String constraintString = "where holiday_id = " + holiday_id;

            Map<String,String> updateQueryParameters = new HashMap<>();
            updateQueryParameters.put("table","Holiday");
            updateQueryParameters.put("key_value_pair","inactive_status = 1");
            updateQueryParameters.put("constraint",constraintString);

            String deletePacketQuery = buildQuery(updateQueryParameters,"UPDATE");

            int deletedRecords = getUpdatedRows(deletePacketQuery);

            if (deletedRecords == 0) {
                throw new Exception("Invalid Request to delete List");
            }
            DatabaseUtil dbutil = new DatabaseUtil();
            String name = dbutil.getTablename("Holiday","holiday_name","holiday_id = " + holiday_id);
            JsonObject dataObject = new JsonObject();
            dataObject.put("message","'" + name + "' Deleted");

            getSuccessObject(dataObject,routingContext.response(),"DELETE");

        }catch (Exception exception){
            exception.printStackTrace();
            String errorDescription = exception.getMessage();
            getErrorObject("Invalid Data",errorDescription,routingContext.response(),"DELETE");
        }
    }

    public void checkCompletedStatus(Connection connectionObject,String list_id) throws  Exception{

            List<String> constraintList = new ArrayList<>();

            constraintList.add("list_id = "+list_id);

            String constraintString = DatabaseUtil.buildConstraintString(constraintList);

            Map<String, String> queryStringParameters = new HashMap<>();
            queryStringParameters.put("fields","holiday_name,date");
            queryStringParameters.put("table","Holiday");
            queryStringParameters.put("constraint", constraintString);

            String holidayListQueryString = buildQuery(queryStringParameters,"SELECT");

            ResultSet resultSet = getResultSetObject(connectionObject,holidayListQueryString);
            int nullCount=0;
            while (resultSet.next()){
                if(resultSet.getString("date").equals(null)||
                        resultSet.getString("date").equals(""))
                    nullCount++;
            }

            if(nullCount==0){
                constraintString = "where list_id = " + list_id;

                Map<String, String> updateQueryParameters = new HashMap<>();
                updateQueryParameters.put("table", "HolidayList");
                updateQueryParameters.put("key_value_pair", "completed_status=0");
                updateQueryParameters.put("constraint", constraintString);


                String updateString = buildQuery(updateQueryParameters, "UPDATE");

                int updatedRows = getUpdatedRows(updateString);
                if (updatedRows == 0) {
                    throw new Exception("Invalid request to update Holiday");
                }
            }



    }

    public String calculateFromArray(String stringToBeSplitted){
        JsonObject splitObject = new JsonObject(stringToBeSplitted);
        JsonArray tempList = splitObject.getJsonArray("list");
        String returnString = "";

        for (int i=0;i<tempList.size();i++) {
            returnString = returnString + tempList.getString(i) + ",";
        }

        StringBuilder returnBuilder = new StringBuilder(returnString);
        returnBuilder.deleteCharAt(returnBuilder.length()-1);
        returnString = returnBuilder.toString();

        return returnString;
    }
}
