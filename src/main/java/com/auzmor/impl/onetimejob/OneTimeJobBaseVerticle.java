package com.auzmor.impl.onetimejob;

import com.auzmor.impl.builder.DbBuilder;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.helper.KongServiceHelper;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpClient;
import io.vertx.servicediscovery.ServiceReference;

public abstract class OneTimeJobBaseVerticle extends AbstractVerticle {

  private DbHandle dbHandle;
  private HttpClient kongClient;
  private HttpClient elasticSearchClient;

  public DbHandle getDbHandle() {
    return dbHandle;
  }

  public OneTimeJobBaseVerticle setDbHandle(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
    return this;
  }

  public HttpClient getKongClient() {
    return kongClient;
  }

  public OneTimeJobBaseVerticle setKongClient(HttpClient httpClient) {
    this.kongClient = httpClient;
    return this;
  }

  public HttpClient getElasticSearchClient() {
    return elasticSearchClient;
  }

  public OneTimeJobBaseVerticle setElasticSearchClient(HttpClient elasticSearchClient) {
    this.elasticSearchClient = elasticSearchClient;
    return this;
  }

  private void closeAllConnection()  {
    this.getDbHandle().checkAndCloseConnection();
    this.kongClient.close();
    //this.elasticSearchClient.close();
  }


  protected void succeeded(Future<Void> fut) {
    DbHelper.safeCommit(this.getDbHandle());
    closeAllConnection();
    fut.complete();
  }

  protected void failed(Future<Void> fut) {
    DbHelper.safeRollback(this.getDbHandle());
    closeAllConnection();
    fut.fail("Not succeeded");
  }

  public void start(Future<Void> fut) {
    DbBuilder.initializeAtsSqlDb();
    DbHandle dbHandle = DbHelper.getDBHandle(vertx, true);
    if (null == dbHandle) {
      fut.fail("Unable to establish db connection");
    }
    this.setDbHandle(dbHandle);
    this.setKongClient(KongServiceHelper.executeWithKongClient(vertx));
    startJob(fut);
  }

  public abstract void startJob(Future<Void> fut);
}
