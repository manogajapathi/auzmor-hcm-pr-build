package com.auzmor.impl.onetimejob.job;

import com.auzmor.impl.bo.RoleBOImpl;
import com.auzmor.impl.dao.EmployeeDAOImpl;
import com.auzmor.impl.dao.LoginDAOImpl;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.KongServiceHelper;
import com.auzmor.impl.model.Role;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.onetimejob.OneTimeJobBaseVerticle;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.Future;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.http.HttpStatus;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Job20180510 extends OneTimeJobBaseVerticle {
  private static Logger logger = LoggerFactory.getLogger(Job20180510.class);

  private int apisCount;

  private void addRolesToEmployees(Future<Void> fut)
      throws SQLException, DbHandleException {
    String query = "select distinct hiring_lead from Job where job_active_status = 1 " +
    "and hiring_lead != ? and job_status != ?";
    PreparedStatement preparedStatement = super.getDbHandle().createPreparedStatement(query);
    preparedStatement.setString(1, "");
    preparedStatement.setString(2, "Closed");

    ResultSet resultSet = preparedStatement.executeQuery();

    List<String> employeeIds = new ArrayList<>();
    while (resultSet.next()) {
      employeeIds.add(resultSet.getString(1));
    }

    for(String empId : employeeIds) {
      int hiringLead = StringUtil.isNumber(empId) ?
          Integer.parseInt(empId) : 0;
      if(hiringLead != 0) {
        DbHandle dbHandle = super.getDbHandle();
        Employee employee = new EmployeeDAOImpl(dbHandle).getEmployeeDetails(hiringLead);
        if(employee != null) {
          int userID = employee.getUser_id();
          String consumerId = "";
          Role role = null;
          String accessRole = "Hiring Lead";

          consumerId = new LoginDAOImpl(dbHandle).getConsumerId(userID);
          role = new RoleBOImpl(dbHandle).getByName(accessRole,
              employee.getOrganization_id());
          KongServiceHelper.editConsumerRoles(super.getKongClient(), consumerId, "" + role.getId(),
              httpClientResponse -> {

                if(httpClientResponse.statusCode() == HttpStatus.SC_CREATED) {
                  logger.info("Hiring Lead with user id " + userID + " added successfully");
                }
              });
        }
      }
    }
    addRolesToApis(fut);
  }


  private void addRolesToApis(Future<Void> fut) throws SQLException, DbHandleException {

    List<String> whiteListedCollaborators = new ArrayList<>();
    String roleQuery = "select id from Role where name = ?";
    PreparedStatement collabPreparedStatement = super.getDbHandle()
        .createPreparedStatement(roleQuery);
    collabPreparedStatement.setString(1, "Collaborator");
    ResultSet collabResultSet = collabPreparedStatement.executeQuery();
    while(collabResultSet.next()) {
      whiteListedCollaborators.add(collabResultSet.getString(1));
    }

    String aclQuery = "select acl_plugin_id from Api where name = ?";

    PreparedStatement aclPreparedStatement = super.getDbHandle().createPreparedStatement(aclQuery);
    aclPreparedStatement.setString(1, "Update_InterviewProcess");

    ResultSet aclResultSet = aclPreparedStatement.executeQuery();
    while(aclResultSet.next()) {
      String aclPluginId = aclResultSet.getString(1);
      KongServiceHelper.editWhitelistRolesAcl(super.getKongClient(), aclPluginId, whiteListedCollaborators,
        httpClientAclResponse -> {
          if(httpClientAclResponse.statusCode() == HttpStatus.SC_OK) {
            logger.info("Collaborator Role added to Api : Update_InterviewProcess");
          }
        });
    }


    List<String> apis = new ArrayList<>();
    apis.add("Create_InterviewProcess");
    apis.add("Update_InterviewProcess");
    apis.add("Preview_InterviewProcess_List");
    apis.add("Get_Job_Tasks");
    apis.add("Add_Job_Task");
    apis.add("Complete_Job_Tasks");
    apis.add("Get_Job_Collaborator_List");
    apis.add("Create_SendMail");
    apis.add("Multiple_Attachment");
    apis.add("Create_Candidate_Offer");
    apis.add("OfferProcess_Attachment");
    apis.add("OfferProcess_Update");
    apis.add("Create_Candidate_Status");
    apis.add("Create_Comments");
    apis.add("Delete_Comments");
    apis.add("Update_Comments_Details");


    roleQuery = "select id from Role where name = ?";
    PreparedStatement statement = super.getDbHandle().createPreparedStatement(roleQuery);
    statement.setString(1, "Hiring Lead");

    ResultSet resultSet = statement.executeQuery();
    List<String> whitelisted = new ArrayList<>();
    while(resultSet.next()) {
      whitelisted.add(resultSet.getString(1));
    }

    apisCount = apis.size();

    for(String api : apis) {
      String query = "select acl_plugin_id from Api where name = ?";
      PreparedStatement preparedStatement = super.getDbHandle().createPreparedStatement(query);
      preparedStatement.setString(1, api);

      resultSet = preparedStatement.executeQuery();
      while(resultSet.next()) {
        String aclPluginId = resultSet.getString(1);

        KongServiceHelper.editWhitelistRolesAcl(super.getKongClient(), aclPluginId, whitelisted,
            httpClientAclResponse -> {

              if(httpClientAclResponse.statusCode() == HttpStatus.SC_OK) {
                --apisCount;
                logger.info("Hiring Lead Role added to Api : " + api);
                if(apisCount == 0) {
                  super.succeeded(fut);
                }
              }
            });
      }
    }
  }

  @Override
  public void startJob(Future<Void> fut) {
    try {
      addRolesToEmployees(fut);
    } catch (SQLException e) {
      super.failed(fut);
    } catch (DbHandleException e) {
      super.failed(fut);
    }
  }
}
