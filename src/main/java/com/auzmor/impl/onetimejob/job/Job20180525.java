package com.auzmor.impl.onetimejob.job;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.KongServiceHelper;
import com.auzmor.impl.onetimejob.OneTimeJobBaseVerticle;
import io.vertx.core.Future;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Job20180525 extends OneTimeJobBaseVerticle {

  private static Logger logger = LoggerFactory.getLogger(Job20180511.class);

  private void addAccessControl(Future<Void> fut) throws SQLException, DbHandleException {
    String apiName = "Applicant_Status_Report";
    List<String> whiteListedCollaborators = new ArrayList<>();
    String roleQuery = "select id from Role where name in (?,?)";
    PreparedStatement collabPreparedStatement = super.getDbHandle()
        .createPreparedStatement(roleQuery);
    collabPreparedStatement.setString(1, "Collaborator");
    collabPreparedStatement.setString(2, "Hiring Lead");
    ResultSet collabResultSet = collabPreparedStatement.executeQuery();
    while (collabResultSet.next()) {
      whiteListedCollaborators.add(collabResultSet.getString(1));
    }


    String aclQuery = "select acl_plugin_id from Api where name = ?";
    PreparedStatement aclPreparedStatement = super.getDbHandle().createPreparedStatement(aclQuery);
    aclPreparedStatement.setString(1, apiName);
    ResultSet aclResultSet = aclPreparedStatement.executeQuery();
    while (aclResultSet.next()) {
      String aclPluginId = aclResultSet.getString(1);
      KongServiceHelper.editWhitelistRolesAcl(super.getKongClient(), aclPluginId, whiteListedCollaborators,
          httpClientAclResponse -> {
            if (httpClientAclResponse.statusCode() == HttpStatus.SC_OK) {
              logger.info("Employee Role added to Api : Applicant_Status_Report");
              super.succeeded(fut);
            }
          });
    }
  }

  @Override
  public void startJob(Future<Void> fut) {
    try {
      addAccessControl(fut);
    } catch (SQLException e) {
      super.failed(fut);
    } catch (DbHandleException e) {
      super.failed(fut);
    }

  }
}
