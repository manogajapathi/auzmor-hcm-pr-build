package com.auzmor.impl.onetimejob.job;

import com.auzmor.impl.constant.route.PathConstants;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.KongServiceHelper;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.onetimejob.OneTimeJobBaseVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.http.HttpStatus;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Job20180503 extends OneTimeJobBaseVerticle {
  private static Logger logger = LoggerFactory.getLogger(Job20180503.class);
  private int consumersCount;
  private int apisCount;
  private int userTypesCount;
  private String anonymousConsumerId;
  private String anonymousRoleId;
  private Map<Long, Map<String, Long>> rolesMap;
  private Map<String, List<String>> userRolesMap;
  private Map<String, Long> entitiesMap;

  private class UserDetail{
    private int id;
    private String userType;
    private long organizationId;
    private long collaboratorId;

    public long getCollaboratorId() {
      return collaboratorId;
    }

    public void setCollaboratorId(long collaboratorId) {
      this.collaboratorId = collaboratorId;
    }

    public long getOrganizationId() {
      return organizationId;
    }

    public void setOrganizationId(long organizationId) {
      this.organizationId = organizationId;
    }

    public int getId() {
      return id;
    }

    public void setId(int id) {
      this.id = id;
    }

    public String getUserType() {
      return userType;
    }

    public void setUserType(String userType) {
      this.userType = userType;
    }
  }


  private void addUserRoles(Future<Void> fut) {
    for (String key : userRolesMap.keySet()) {
      List<String> userTypes = userRolesMap.get(key);
      for (String userType: userTypes) {
        KongServiceHelper.addConsumerRoles(super.getKongClient(), key, userType, httpClientResponse -> {
          if (httpClientResponse.statusCode() == HttpStatus.SC_CREATED) {
            --this.userTypesCount;
            if (this.userTypesCount == 0) {
              super.succeeded(fut);
            }
          } else {
            super.failed(fut);
          }
        });
      }

    }
  }

  private void addConsumers(Future<Void> fut) throws SQLException, DbHandleException {
    List<UserDetail> userDetails = super.getDbHandle().getSerializedObject("SELECT t1.id, t1.user_type, t1.organization_id, " +
        "t3.collaborator FROM `Login` t1 LEFT JOIN (`Employee` t2) ON (t1.id = t2.user_id) " +
        "LEFT JOIN (Select distinct collaborator  from `Collaborators` where active_status = 1) t3 ON (t2.employee_id = t3.collaborator) " +
        "ORDER BY t1.id", resultSet -> {
      int retrieveIndex = 0;
      UserDetail userDetail = new UserDetail();
      userDetail.setId(resultSet.getInt(++retrieveIndex));
      userDetail.setUserType(resultSet.getString(++retrieveIndex));
      userDetail.setOrganizationId(resultSet.getLong(++retrieveIndex));
      userDetail.setCollaboratorId(resultSet.getLong(++retrieveIndex));
      return userDetail;
    });

    this.consumersCount = userDetails.size();
    this.userTypesCount = 0;
    this.userRolesMap = new HashMap<>();
    for (UserDetail userDetail: userDetails) {
      KongServiceHelper.addConsumer(super.getKongClient(), userDetail.getId(),
          httpClientResponse -> {
            if (httpClientResponse.statusCode() == HttpStatus.SC_CREATED) {
              httpClientResponse.bodyHandler(buffer -> {
                String consumerId = new JsonObject(buffer).getString("id");
                List<String> userTypes = new ArrayList<>();
                if (userDetail.getUserType().equals("candidate")) {
                  userTypes.add("" + this.rolesMap.get(userDetail.getOrganizationId()).get("Candidate"));
                }
                else {
                  if (userDetail.getUserType().startsWith("employee")) {
                    userTypes.add("" + this.rolesMap.get(userDetail.getOrganizationId()).get("Employee"));
                  } else if (userDetail.getUserType().equals("admin")) {
                    userTypes.add("" + this.rolesMap.get(userDetail.getOrganizationId()).get("Admin"));
                  }
                  if (userDetail.getCollaboratorId() > 0) {
                    userTypes.add("" + this.rolesMap.get(userDetail.getOrganizationId()).get("Collaborator"));
                  }
                }
                this.userTypesCount += userTypes.size();
                this.userRolesMap.put(consumerId, userTypes);
                try {
                  PreparedStatement consumerPreparedStatement = super.getDbHandle().createPreparedStatement("UPDATE `Login` SET `consumer_id` = ? WHERE id = ?");
                  consumerPreparedStatement.setString(1, consumerId);
                  consumerPreparedStatement.setLong(2, userDetail.getId());
                  consumerPreparedStatement.execute();
                  consumerPreparedStatement.close();
                  --this.consumersCount;
                  if (this.consumersCount == 0) {
                    addUserRoles(fut);
                  }
                } catch (SQLException e) {
                  super.failed(fut);
                } catch (DbHandleException e) {
                  super.failed(fut);
                }
              });
            } else {
              super.failed(fut);
            }

          });
    }

  }

  private void addApis(Future<Void> fut) throws SQLException, DbHandleException {
    this.apisCount = PathConstants.APIS.getMap().keySet().size();
    for (Map.Entry<String, Object> api : PathConstants.APIS.getMap().entrySet()) {
      Map apiMap = (Map)api.getValue();
      KongServiceHelper.addApi(super.getKongClient(), api.getKey(), Arrays.asList((String)apiMap.get("pattern")),
          (List<String>) apiMap.get("methods"),
          "http://localhost:8080/", false, httpClientResponse -> {
            if (httpClientResponse.statusCode() == HttpStatus.SC_CREATED) {
              httpClientResponse.bodyHandler(buffer -> {
                String apiId = new JsonObject(buffer).getString("id");

                boolean isAnonymousEnabled = ((List<String>) apiMap.get("roles")).contains("Anonymous");
                boolean isOnlyAnonymous = ((List<String>) apiMap.get("roles")).contains("Only Anonymous");
                KongServiceHelper.addPlugin(super.getKongClient(), apiId, "jwt",
                    (isAnonymousEnabled || isOnlyAnonymous) ? this.anonymousConsumerId : null,
                    null, httpClientPluginResponse -> {
                      if (httpClientPluginResponse.statusCode() == HttpStatus.SC_CREATED) {
                        httpClientPluginResponse.bodyHandler(pluginBuffer -> {
                          String jwtId = new JsonObject(pluginBuffer).getString("id");

                          List<String> aclRoles = new ArrayList<>();
                          if (isOnlyAnonymous) {
                            aclRoles = Arrays.asList(this.anonymousRoleId);
                          } else if (isAnonymousEnabled) {
                            aclRoles.add("" + 0);
                            for (long organizationId : this.rolesMap.keySet()) {
                              for (String organizationRole : this.rolesMap.get(organizationId).keySet()) {
                                aclRoles.add("" + this.rolesMap.get(organizationId).get(organizationRole));
                              }
                            }
                          } else {
                            for (String role : (List<String>) apiMap.get("roles")) {
                              for (long organizationId : this.rolesMap.keySet()) {
                                for (String organizationRole : this.rolesMap.get(organizationId).keySet()) {
                                  if (isAnonymousEnabled || organizationRole.equals(role)) {
                                    aclRoles.add("" + this.rolesMap.get(organizationId).get(role));
                                    break;
                                  }
                                }
                              }
                            }
                          }

                          KongServiceHelper.addPlugin(super.getKongClient(), apiId, "acl", null,
                              aclRoles, httpClientACLResponse -> {
                                if (httpClientACLResponse.statusCode() == HttpStatus.SC_CREATED) {
                                  httpClientACLResponse.bodyHandler(aclBuffer -> {
                                    String aclId = new JsonObject(aclBuffer).getString("id");
                                    try {
                                      PreparedStatement preparedStatement = super.getDbHandle().createPreparedStatement(
                                          "INSERT INTO `Api` (`name`, `http_method`, `url_pattern`, `entity_id`, `api_id`, `jwt_plugin_id`, `acl_plugin_id`) " +
                                              "VALUES (?, ?, ?, ?, ?, ?, ?)");
                                      preparedStatement.setString(1, api.getKey());
                                      preparedStatement.setString(2, ((List<String>) apiMap.get("methods")).get(0));
                                      preparedStatement.setString(3, (String)apiMap.get("pattern"));
                                      preparedStatement.setLong(4, this.entitiesMap.get((String)apiMap.get("entity")));
                                      preparedStatement.setString(5, apiId);
                                      preparedStatement.setString(6, jwtId);
                                      preparedStatement.setString(7, aclId);
                                      preparedStatement.execute();
                                      preparedStatement.close();
                                      --this.apisCount;
                                      if (0 == this.apisCount) {
                                        addConsumers(fut);
                                      }
                                    } catch (SQLException e) {
                                      super.failed(fut);
                                    } catch (DbHandleException e) {
                                      super.failed(fut);
                                    }
                                  });
                                }
                              });
                        });
                      } else {
                        super.failed(fut);
                      }
                    });
              });
            } else {
              super.failed(fut);
            }
          });
    }

  }

  private void addRoles(Future<Void> fut)
      throws SQLException, DbHandleException {
    List<Organization> organizations = super.getDbHandle().getSerializedObject("SELECT `organization_id` " +
        "FROM `Organization`", resultSet -> {
      int retrieveIndex = 0;
      Organization organization = new Organization();
      organization.setOrganization_id(resultSet.getInt(++retrieveIndex));
      return organization;
    });

    this.anonymousRoleId = "" + 0;
    this.rolesMap = new HashMap<>();
    String[] roles = PathConstants.DEFAULT_ROLES;
    PreparedStatement preparedStatement = super.getDbHandle().createPreparedStatement(
        "INSERT INTO `Role` (`name`, `organization_id`) " +
            "VALUES (?, ?)", true);

    for (Organization organization : organizations) {
      for (String role : roles) {
        preparedStatement.setString(1, role);
        preparedStatement.setLong(2, organization.getOrganization_id());
        preparedStatement.addBatch();
      }

      preparedStatement.executeBatch();
      ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
      List<Long> ids = new ArrayList<>();
      while (generatedKeys.next()) {
        ids.add(generatedKeys.getLong(1));
      }
      generatedKeys.close();
      Map<String, Long> map = new HashMap<>();
      int idx = 0;
      for (String role : roles) {
        map.put(role, (long) ids.get(idx++));
      }
      this.rolesMap.put((long) organization.getOrganization_id(), map);
    }

    KongServiceHelper.addConsumer(super.getKongClient(), 0,
        httpClientResponse -> {
          if (httpClientResponse.statusCode() == HttpStatus.SC_CREATED) {
            httpClientResponse.bodyHandler(buffer -> {
              this.anonymousConsumerId = new JsonObject(buffer).getString("id");
              KongServiceHelper.addConsumerRoles(super.getKongClient(), this.anonymousConsumerId, this.anonymousRoleId, httpRoles -> {
                if (httpClientResponse.statusCode() == HttpStatus.SC_CREATED) {
                  try {
                    addApis(fut);
                  } catch (SQLException e) {
                    super.failed(fut);
                  } catch (DbHandleException e) {
                    super.failed(fut);
                  }
                } else {
                  super.failed(fut);
                }
              });
            });
          }
        });
  }

  /**
   * @throws SQLException
   * @throws DbHandleException
   * @author Charles Sam Dilip
   * @lastModifiedDate 23/MAR/2018
   */
  private void addEntites(Future<Void> fut) throws SQLException, DbHandleException {
    String[] entities = {"Announcement", "Candidate", "Candidate  OfferProcess", "Candidate Comments",
        "Candidate HiringProcess", "Candidate InterviewProcess", "Candidate SendMail",
        "Candidate SendMail Attachment", "Email Template", "Employee", "Field Options", "Holiday",
        "Invite", "Job", "Job Tasks", "Login", "Newsfeed", "None", "Onboarding", "Organization",
        "Projects", "PTO Request", "Reports", "Tasks", "Phone Number", "Currency"};
    PreparedStatement preparedStatement = super.getDbHandle()
        .createPreparedStatement("INSERT INTO `Entity` (`name`) VALUES (?)", true);
    for (String entity : entities) {
      preparedStatement.setString(1, entity);
      preparedStatement.addBatch();
    }
    preparedStatement.executeBatch();
    ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
    List<Long> ids = new ArrayList<>();
    while (generatedKeys.next()) {
      ids.add(generatedKeys.getLong(1));
    }
    this.entitiesMap = new HashMap<String, Long>();
    int idx = 0;
    for (String entity : entities) {
      this.entitiesMap.put(entity, ids.get(idx++));
    }
    addRoles(fut);
  }

  @Override
  public void startJob(Future<Void> fut) {
    try {
      addEntites(fut);
    } catch (SQLException e) {
      super.failed(fut);
    } catch (DbHandleException e) {
      super.failed(fut);
    }
  }
}