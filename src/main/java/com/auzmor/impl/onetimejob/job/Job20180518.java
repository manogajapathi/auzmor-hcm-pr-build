package com.auzmor.impl.onetimejob.job;

import com.auzmor.impl.constant.route.PathConstants;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.KongServiceHelper;
import com.auzmor.impl.onetimejob.OneTimeJobBaseVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Job20180518 extends OneTimeJobBaseVerticle {

  private static Logger logger = LoggerFactory.getLogger(Job20180518.class);

  private void addCountrywithStateRegionCode(Future<Void> fut) {
    String key = "Country_With_Region_Code";
    Map<String, Object> api = (Map<String, Object>) PathConstants.APIS.getMap().get(key);
    KongServiceHelper.addApi(super.getKongClient(), key, Arrays.asList((String) api.get("pattern")),
        (List<String>) api.get("methods"), "http://localhost:8080/", false, httpClientResponse -> {
          if (httpClientResponse.statusCode() == HttpStatus.SC_CREATED) {
            httpClientResponse.bodyHandler(buffer -> {
              String apiId = new JsonObject(buffer).getString("id");

              KongServiceHelper.addPlugin(super.getKongClient(), apiId, "jwt", null,
                  null, httpClientPluginResponse -> {
                    if (httpClientPluginResponse.statusCode() == HttpStatus.SC_CREATED) {
                      httpClientPluginResponse.bodyHandler(pluginBuffer -> {
                        String jwtId = new JsonObject(pluginBuffer).getString("id");

                        List<String> aclRoles = new ArrayList<>();
                        try {
                          PreparedStatement preparedStatementCandidate = super.getDbHandle().createPreparedStatement("SELECT `id` FROM `Role`");
                          ResultSet resultSetCandidate = preparedStatementCandidate.executeQuery();
                          while (resultSetCandidate.next()) {
                            aclRoles.add(resultSetCandidate.getLong(1) + "");
                          }
                        } catch (DbHandleException e) {
                          e.printStackTrace();
                        } catch (SQLException e) {
                          e.printStackTrace();
                        }
                        KongServiceHelper.addPlugin(super.getKongClient(), apiId, "acl", null,
                            aclRoles, httpClientACLResponse -> {
                              if (httpClientACLResponse.statusCode() == HttpStatus.SC_CREATED) {
                                httpClientACLResponse.bodyHandler(aclBuffer -> {
                                  String aclId = new JsonObject(aclBuffer).getString("id");
                                  try {
                                    PreparedStatement preparedStatement = super.getDbHandle().createPreparedStatement(
                                        "INSERT INTO `Api` (`name`, `http_method`, `url_pattern`, `entity_id`, `api_id`, `jwt_plugin_id`, `acl_plugin_id`) " +
                                            "VALUES (?, ?, ?, ?, ?, ?, ?)");
                                    PreparedStatement preparedStatementId = super.getDbHandle().createPreparedStatement("SELECT `id` FROM `Entity` WHERE `name` = 'Candidate'");
                                    ResultSet resultSet = preparedStatementId.executeQuery();
                                    resultSet.next();
                                    preparedStatement.setString(1, key);
                                    preparedStatement.setString(2, "GET");
                                    preparedStatement.setString(3, (String) api.get("pattern"));
                                    preparedStatement.setLong(4, resultSet.getLong(1));
                                    preparedStatement.setString(5, apiId);
                                    preparedStatement.setString(6, jwtId);
                                    preparedStatement.setString(7, aclId);
                                    preparedStatement.execute();
                                    preparedStatement.close();
                                    super.succeeded(fut);
                                  } catch (SQLException e) {
                                    super.failed(fut);
                                  } catch (DbHandleException e) {
                                    super.failed(fut);
                                  }
                                });
                              }
                            });
                      });
                    } else {
                      super.failed(fut);
                    }
                  });
            });
          } else {
            super.failed(fut);
          }
        });
  }

  @Override
  public void startJob(Future<Void> fut) {
    addCountrywithStateRegionCode(fut);
  }
}
