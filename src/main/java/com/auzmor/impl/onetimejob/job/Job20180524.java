package com.auzmor.impl.onetimejob.job;

import com.auzmor.impl.constant.route.PathConstants;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.exception.db.*;
import com.auzmor.impl.helper.KongServiceHelper;
import com.auzmor.impl.onetimejob.OneTimeJobBaseVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


public class Job20180524 extends OneTimeJobBaseVerticle {

  private static Logger logger = LoggerFactory.getLogger(Job20180524.class);

  private void addAnonymousRoleToGetOrganization(Future<Void> fut)
      throws SQLException, DbHandleException {
    String apiName = "Get_Organization_Details";
    List<String> whiteListedCollaborators = new ArrayList<>();
    whiteListedCollaborators.add("0");
    String roleQuery = "select id from Role";
    PreparedStatement collabPreparedStatement = super.getDbHandle()
        .createPreparedStatement(roleQuery);
    ResultSet collabResultSet = collabPreparedStatement.executeQuery();
    while (collabResultSet.next()) {
      whiteListedCollaborators.add(collabResultSet.getString(1));
    }

    String aclQuery = "select acl_plugin_id from Api where name = ?";
    PreparedStatement aclPreparedStatement = super.getDbHandle().createPreparedStatement(aclQuery);
    aclPreparedStatement.setString(1, apiName);
    ResultSet aclResultSet = aclPreparedStatement.executeQuery();
    while (aclResultSet.next()) {
      String aclPluginId = aclResultSet.getString(1);
      KongServiceHelper.editWhitelistRolesAcl(super.getKongClient(), aclPluginId, whiteListedCollaborators,
          httpClientAclResponse -> {
            if (httpClientAclResponse.statusCode() == HttpStatus.SC_OK) {
              logger.info("Anonymous Role added to Api : Get_Organization_Details");
              super.succeeded(fut);
            }
          });
    }
  }

  @Override
  public void startJob(Future<Void> fut) {
    try {
      addAnonymousRoleToGetOrganization(fut) ;
    } catch (SQLException e) {
      e.printStackTrace();
    } catch (DbHandleException e) {
      e.printStackTrace();
    }
  }
}
