package com.auzmor.impl.onetimejob.job;

import com.auzmor.impl.constant.route.PathConstants;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.KongServiceHelper;
import com.auzmor.impl.onetimejob.OneTimeJobBaseVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Job20180605 extends OneTimeJobBaseVerticle {
  private static Logger logger = LoggerFactory.getLogger(Job20180605.class);
  private int apisCount;

  /* private void addRolesToApis(Future<Void> fut) throws SQLException, DbHandleException {
    
    List<String> whiteListedCollaborators = new ArrayList<>();
    String roleQuery = "select id from Role where name = ?";
    PreparedStatement collabPreparedStatement = super.getDbHandle()
        .createPreparedStatement(roleQuery);
    collabPreparedStatement.setString(1, "Employee");
    ResultSet collabResultSet = collabPreparedStatement.executeQuery();
    while(collabResultSet.next()) {
      whiteListedCollaborators.add(collabResultSet.getString(1));
    }
    
    List<String> apis = new ArrayList<>();
    apis.add("Get_Job_List_Employee");
    apis.add("Preview_Candidate");
    apis.add("Preview_InterviewProcess_List");
    
    apisCount = apis.size();
  
    StringBuilder aclQuery = new StringBuilder("select acl_plugin_id from Api where name in (");
    for(int apiIterator = 0; apiIterator < apisCount; apiIterator++) {
      aclQuery.append("?");
      if(apiIterator != apisCount - 1) {
        aclQuery.append(",");
      }
    }
    aclQuery.append(")");
  
    PreparedStatement aclPreparedStatement = super.getDbHandle().createPreparedStatement(aclQuery.toString());
    for(int apiIterator = 1; apiIterator <= apisCount; apiIterator++) {
      aclPreparedStatement.setString(apiIterator, apis.get(apiIterator - 1));
    }
  
    ResultSet aclResultSet = aclPreparedStatement.executeQuery();
    while (aclResultSet.next()) {
      String aclPluginId = aclResultSet.getString(1);
      KongServiceHelper.editWhitelistRolesAcl(super.getKongClient(), aclPluginId, whiteListedCollaborators,
          httpClientAclResponse -> {
            if (httpClientAclResponse.statusCode() == HttpStatus.SC_OK) {
              --apisCount;
              logger.info("Employee Role added to Apis ");
              if (apisCount == 0) {
                super.succeeded(fut);
              }
            }
          });
    }
  } */

  private void addGetLoggedInUserRoles(Future<Void> fut) {
    String apiKey = "Get_Logged_In_User_Roles";
    Map<String, Object> api = (Map<String, Object>) PathConstants.APIS.getMap().get(apiKey);
    KongServiceHelper.addApi(super.getKongClient(), apiKey, Arrays.asList((String) api.get("pattern")),
        (List<String>) api.get("methods"), "http://localhost:8080/", false, httpClientResponse -> {
          if (httpClientResponse.statusCode() == HttpStatus.SC_CREATED) {
            httpClientResponse.bodyHandler(buffer -> {
              String apiId = new JsonObject(buffer).getString("id");

              KongServiceHelper.addPlugin(super.getKongClient(), apiId, "jwt", null,
                  null, httpClientPluginResponse -> {
                    if (httpClientPluginResponse.statusCode() == HttpStatus.SC_CREATED) {
                      httpClientPluginResponse.bodyHandler(pluginBuffer -> {
                        String jwtId = new JsonObject(pluginBuffer).getString("id");

                        List<String> aclRoles = new ArrayList<>();
                        try {
                          PreparedStatement preparedStatementCandidate = super.getDbHandle().createPreparedStatement("SELECT `id` FROM `Role`");
                          ResultSet resultSetCandidate = preparedStatementCandidate.executeQuery();
                          while (resultSetCandidate.next()) {
                            aclRoles.add(resultSetCandidate.getLong(1) + "");
                          }
                        } catch (DbHandleException e) {
                          e.printStackTrace();
                        } catch (SQLException e) {
                          e.printStackTrace();
                        }
                        KongServiceHelper.addPlugin(super.getKongClient(), apiId, "acl", null,
                            aclRoles, httpClientACLResponse -> {
                              if (httpClientACLResponse.statusCode() == HttpStatus.SC_CREATED) {
                                httpClientACLResponse.bodyHandler(aclBuffer -> {
                                  String aclId = new JsonObject(aclBuffer).getString("id");
                                  try {
                                    PreparedStatement preparedStatement = super.getDbHandle().createPreparedStatement(
                                        "INSERT INTO `Api` (`name`, `http_method`, `url_pattern`, `entity_id`, `api_id`, `jwt_plugin_id`, `acl_plugin_id`) " +
                                            "VALUES (?, ?, ?, ?, ?, ?, ?)");
                                    PreparedStatement preparedStatementId = super.getDbHandle().createPreparedStatement("SELECT `id` FROM `Entity` WHERE `name` = 'Login'");
                                    ResultSet resultSet = preparedStatementId.executeQuery();
                                    resultSet.next();
                                    preparedStatement.setString(1, apiKey);
                                    preparedStatement.setString(2, "GET");
                                    preparedStatement.setString(3, (String) api.get("pattern"));
                                    preparedStatement.setLong(4, resultSet.getLong(1));
                                    preparedStatement.setString(5, apiId);
                                    preparedStatement.setString(6, jwtId);
                                    preparedStatement.setString(7, aclId);
                                    preparedStatement.execute();
                                    preparedStatement.close();
                                    //addRolesToApis(fut);
                                    super.succeeded(fut);
                                  } catch (SQLException e) {
                                    super.failed(fut);
                                  } catch (DbHandleException e) {
                                    super.failed(fut);
                                  }
                                });
                              }
                            });
                      });
                    } else {
                      super.failed(fut);
                    }
                  });
            });
          } else {
            super.failed(fut);
          }
        });
  }
  
  @Override
  public void startJob(Future<Void> fut) {
    addGetLoggedInUserRoles(fut);
  }
}
