package com.auzmor.impl.exception.db;

public class DbHandleException extends Exception {

  public DbHandleException(String message) {
    super(message);
  }

}
