package com.auzmor.impl.exception.util;

public class ApplicationUtilException extends Exception {
  public ApplicationUtilException(String message) {
    super(message);
  }
}
