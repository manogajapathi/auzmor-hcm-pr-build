package com.auzmor.impl.exception.http;

public class HttpException extends Exception{
  public HttpException(String message) {
    super(message);
  }
}
