package com.auzmor.impl.util;

import com.auzmor.impl.exception.util.ApplicationUtilException;
import com.auzmor.impl.model.Organization;
import io.vertx.ext.web.RoutingContext;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class DateUtil {
  public SimpleDateFormat getDate() {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
    return simpleDateFormat;
  }

  public SimpleDateFormat getDateTime() {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    return simpleDateFormat;
  }


  public String getDatetimeDifference(String date) throws ApplicationUtilException {

    Date dNow = new Date();
    SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
    String currentDate = df.format(dNow);
    String dateCalculation = "";
    //HH converts hour in 24 hours format (0-23), day calculation
    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm a");

    Date todayDate = null;
    Date futureDate = null;

    try {
      todayDate = format.parse(currentDate);
      futureDate = format.parse(date);

      //in milliseconds
      long diff = futureDate.getTime() - todayDate.getTime();

      long diffSeconds = diff / 1000 % 60;
      long diffMinutes = diff / (60 * 1000) % 60;
      long diffHours = diff / (60 * 60 * 1000) % 24;
      long diffDays = diff / (24 * 60 * 60 * 1000);

           /* System.out.print(diffDays + " days, ");
            System.out.print(diffHours + " hours, ");
            System.out.print(diffMinutes + " minutes, ");
            System.out.print(diffSeconds + " seconds.");*/

      if (diff < 0) {
        dateCalculation = "OverDue";
      } else {

        if (diffDays == 0) {
          dateCalculation = Long.toString(diffHours) + " hours " + Long.toString(diffMinutes) + "minutes";
        } else if (diffHours == 0) {
          dateCalculation = Long.toString(diffMinutes) + "minutes";
        } else if (diffMinutes == 0) {
          dateCalculation = Long.toString(diffSeconds) + "seconds";
        } else {
          dateCalculation = Long.toString(diffDays) + "days " +
              Long.toString(diffHours) + " hours " + Long.toString(diffMinutes) + "minutes";
        }
      }

    } catch (ParseException e) {
      throw new ApplicationUtilException("DateFormat Exception");
    }

    return dateCalculation;
  }

  public String getDateFormat(RoutingContext routingContext, String date) throws ParseException {


    String format = ((Organization) routingContext.data().get("organization")).getDate_format();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format != null ? format : "dd-MMM-yyyy");
    SimpleDateFormat DateFormat = new SimpleDateFormat("MM/dd/yyyy");
    Date dateFormat = DateFormat.parse(date);
    String dateFormatValue = simpleDateFormat.format(dateFormat);

    return dateFormatValue;
  }

  public String getDateFormatDB(RoutingContext routingContext, String date) throws ParseException {

    String format = ((Organization) routingContext.data().get("organization")).getDate_format();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format != null ? format : "dd-MMM-yyyy");
    SimpleDateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Date dateFormat = DateFormat.parse(date);
    String dateFormatValue = simpleDateFormat.format(dateFormat);

    return dateFormatValue;
  }

  public String getDateFormatDB(String format, String date) throws ParseException {

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format != null ? format : "dd-MMM-yyyy");
    SimpleDateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Date dateFormat = DateFormat.parse(date);
    String dateFormatValue = simpleDateFormat.format(dateFormat);

    return dateFormatValue;
  }

  public String getDateFormatWithSeconds(RoutingContext routingContext, String date) throws ParseException {

    String format = ((Organization) routingContext.data().get("organization")).getDate_format();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format != null ? format : "dd-MMM-yyyy");
    SimpleDateFormat DateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
    Date dateFormat = DateFormat.parse(date);
    String dateFormatValue = simpleDateFormat.format(dateFormat);

    return dateFormatValue;
  }

  public String getDateFormat(String format, String date) throws ParseException {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format != null ? format : "dd-MMM-yyyy");
    SimpleDateFormat DateFormat = new SimpleDateFormat("MM/dd/yyyy");
    Date dateFormat = DateFormat.parse(date);
    String dateFormatValue = simpleDateFormat.format(dateFormat);

    return dateFormatValue;
  }

  public String getDateFormatWithSeconds(String format, String date) throws ParseException {

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format != null ? format : "dd-MMM-yyyy");
    SimpleDateFormat DateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
    Date dateFormat = DateFormat.parse(date);
    String dateFormatValue = simpleDateFormat.format(dateFormat);

    return dateFormatValue;
  }

  /*public String getDateFormat(String date) throws ParseException {

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
    SimpleDateFormat DateFormat = new SimpleDateFormat("MM/dd/yyyy");
    Date dateFormat = DateFormat.parse(date);
    String dateFormatValue = simpleDateFormat.format(dateFormat);

    return dateFormatValue;
  }*/

  /*public String getDateFormatWithSeconds(String date) throws ParseException {

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
    SimpleDateFormat DateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
    Date dateFormat = DateFormat.parse(date);
    String dateFormatValue = simpleDateFormat.format(dateFormat);

    return dateFormatValue;
  }*/

  public String convertDateintoString(Date date) throws ParseException{
    String dateFormat = this.getDate().format(date);
    return  dateFormat;
  }

  public String convertDateTimeintoString(Date date) throws ParseException{
    String dateFormat = this.getDateTime().format(date);
    return  dateFormat;
  }

  public long DateDifferenceBetweenTwoDates(String date) throws ParseException{
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    Date date1 = new Date();
    Date date2 = sdf.parse(date);

    long diff = date1.getTime() - date2.getTime();

    // using TimeUnit class from java.util.concurrent package
    long getDaysDiff = TimeUnit.MILLISECONDS.toDays(diff);

    return getDaysDiff;
  }
  
  public String convertFormat(String dateTime, String fromFormat, String toFormat) throws ParseException {
    DateFormat fromFormatter = new SimpleDateFormat(fromFormat);
    DateFormat toFormatter = new SimpleDateFormat(toFormat);
    return toFormatter.format(fromFormatter.parse(dateTime));
  }

  public Long getTimeInMillis(String dateTime, String format) throws ParseException {
    DateFormat dateFormatter = new SimpleDateFormat(format);
    return dateFormatter.parse(dateTime).getTime();
  }

  public Long getTimeInUTC(Long time, String actualTimeZone) {
    TimeZone fromTimeZone = TimeZone.getTimeZone(actualTimeZone);
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(time);
    calendar.setTimeZone(fromTimeZone);
    calendar.add(Calendar.MILLISECOND, fromTimeZone.getRawOffset() * -1);
    if (fromTimeZone.inDaylightTime(calendar.getTime())) {
      calendar.add(Calendar.MILLISECOND, calendar.getTimeZone().getDSTSavings() * -1);
    }
    return calendar.getTimeInMillis();
  }

  public String getDateFormatWithSeconds(String format, long timeInMillis) throws ParseException {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format == null ? "MM/dd/yyyy hh:mm a" :
        format);
    String dateFormatValue = simpleDateFormat.format(new Date(timeInMillis));

    return dateFormatValue;
  }
}