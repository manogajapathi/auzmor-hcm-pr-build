package com.auzmor.impl.util.bo;

import com.auzmor.util.bo.ReturnObject;

public class ReturnObjectImpl<E, T> implements ReturnObject {
  private E statusEnum;
  private T data;

  public ReturnObjectImpl(E statusEnum, T data) {
    this.statusEnum = statusEnum;
    this.data = data;
  }

  public E getStatusEnum() {
    return statusEnum;
  }

  public T getData() {
    return data;
  }

}
