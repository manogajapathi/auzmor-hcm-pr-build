package com.auzmor.impl.util;

import com.auzmor.impl.configuration.MysqldbConf;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.commons.dbutils.DbUtils;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class DBCRUDUtil {

  PreparedStatement preparedStatement = null;

  public Connection getConnection() {
    Connection connection = null;
    MysqldbConf mysqldbConf = new MysqldbConf();
    try {
      connection = mysqldbConf.getConnection();
    } catch (ClassNotFoundException exception) {
      exception.printStackTrace();
    } catch (SQLException sqlException) {
      sqlException.printStackTrace();
    }
    return connection;
  }

  public int insertIntoTable(Map<String, Object> insertionObjects, String tableName) {
    Connection connection = null;
    connection = getConnection();
    ResultSet resultSet = null;
    String insertQuery = "";
    String columnValueString = "";
    String entryValueString = "";
    String appender = "";
    int division_id = 0;
    try {
      for (Map.Entry<String, Object> columnEntries : insertionObjects.entrySet()) {

      columnValueString += columnEntries.getKey() + ",";
        Object insertObject = columnEntries.getValue();
        if (insertObject instanceof Boolean) {
          appender = insertObject.toString();
        } else {
          appender = "'" + columnEntries.getValue() + "'";
        }
        entryValueString += appender + ",";
      }
      entryValueString = entryValueString.substring(0, entryValueString.length() - 1);

      columnValueString = columnValueString.substring(0, columnValueString.length() - 1);insertQuery = "insert into " + tableName + " (" + columnValueString + ") values (" + entryValueString + ");";

      Statement preparedStatement = connection.createStatement();
      preparedStatement.executeUpdate(insertQuery, Statement.RETURN_GENERATED_KEYS);
      resultSet = preparedStatement.getGeneratedKeys();

      if (resultSet.next()) {
        division_id = resultSet.getInt(1);
      }

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      DbUtils.closeQuietly(connection);
    }
    return division_id;
  }

  /**
  *Generic select query method
  * @param queryString
  * @param fieldList
  * @return
  * @author Pradeep Sudhakaran
  * @lastModifiedDate 07-11-2017
  */
  public JsonArray selectFieldsWithLimit(String queryString, List<String> fieldList) {
    JsonArray resultSetArray = new JsonArray();
    Connection connection = null;
    PreparedStatement preparedStatement = null;

    try {
      connection = getConnection();
      preparedStatement = connection.prepareStatement(queryString);
      ResultSet resultSetObject = preparedStatement.executeQuery();

      while (resultSetObject.next()) {

        JsonObject dataObject = new JsonObject();
        for (String field : fieldList) {
          dataObject.put(field, resultSetObject.getString(field));
        }

        resultSetArray.add(dataObject);
      }

    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      DbUtils.closeQuietly(connection);
    }
    return resultSetArray;
  }

  public List<String> selectFieldsForList(String queryString, String field) {
    PreparedStatement preparedStatement = null;
    List<String> listOfStrings = new LinkedList<>();
    Connection connection = null;
    try {
      connection = getConnection();
      preparedStatement = connection.prepareStatement(queryString);
      ResultSet resultSetObject = preparedStatement.executeQuery();
      while (resultSetObject.next()) {
        String supervisor = "";
        supervisor = resultSetObject.getString(field);
        listOfStrings.add(supervisor);
      }
      connection.close();
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      DbUtils.closeQuietly(connection);
    }
    return listOfStrings;
  }

  public String createView(String queryString) {

    PreparedStatement preparedStatement = null;
    String returnString = "";
    Connection connection = null;
    try {
      connection = getConnection();
      preparedStatement = connection.prepareStatement(queryString);
      int updateCount = preparedStatement.executeUpdate();

      if (updateCount != 0) {
        returnString = "success";
      } else {
        returnString = "failed";
      }

    } catch (Exception exception) {
      exception.printStackTrace();
      returnString = "failed";
    } finally {
      DbUtils.closeQuietly(connection);
    }
    return returnString;
  }

  public String returnValueForQuery(String queryString) {
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    String returnString = "";
    Connection connection = null;
    try {
      connection = getConnection();
      preparedStatement = connection.prepareStatement(queryString);
      resultSet = preparedStatement.executeQuery();

      while (resultSet.next()) {
        returnString = resultSet.getString(1);
      }

      connection.close();
    } catch (Exception exception) {
      exception.printStackTrace();
      returnString = "failed";
    } finally {
      DbUtils.closeQuietly(connection);
    }
    return returnString;
  }
}
