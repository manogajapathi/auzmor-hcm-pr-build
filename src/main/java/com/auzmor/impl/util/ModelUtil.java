package com.auzmor.impl.util;

import com.auzmor.impl.exception.util.ApplicationUtilException;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

public class ModelUtil {
  public static void setValues(String name, String value, Object object)
      throws ApplicationUtilException {
    try {
      Method method = null;
      Field field = object.getClass().getDeclaredField(name);
      String fieldName = field.getName();
      Class thisClass = object.getClass();

      String methodName = "set" + Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1);
      if ("int".equalsIgnoreCase(field.getType().toString())) {
        method = thisClass.getMethod(methodName, Integer.TYPE);
        int integerType = Integer.valueOf(value);
        method.invoke(object, integerType);
      } else if ("boolean".equalsIgnoreCase(field.getType().toString())) {
        method = thisClass.getMethod(methodName, Boolean.TYPE);
        method.invoke(object, new Boolean(value));
      } else {
        method = thisClass.getMethod(methodName, String.class);
        method.invoke(object, value);
      }

    } catch (NoSuchFieldException exception) {
      throw new ApplicationUtilException("No such method");
    } catch (NoSuchMethodException exception) {
      throw new ApplicationUtilException("No such field");
    } catch (IllegalAccessException exception) {
      throw new ApplicationUtilException("Illegal Access Exception");
    } catch (InvocationTargetException exception) {
      throw new ApplicationUtilException("Invocation Target Exception");
    }
}
  public static String getFieldType(String name, Object object) throws Exception {
    Field field = object.getClass().getDeclaredField(name);
    return field.getType().toString();
  }

  public static List<String> getFieldNames(Object object) throws Exception {
    Field[] fields = object.getClass().getDeclaredFields();
    List<String> fieldNames = new LinkedList<String>();
    for (int i = 0; i < fields.length; i++) {
      fieldNames.add(fields[i].getName());
    }
    return fieldNames;
  }

  public static Object getFieldValue(String fieldName, Object object) throws Exception {
    String methodName = "";
    if ("boolean".equals(getFieldType(fieldName, object))) {
      methodName = "is" + Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1);
    } else {
      methodName = "get" + Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1);
    }
    Method fieldGetter = object.getClass().getMethod(methodName);
    return fieldGetter.invoke(object) == null ? "" : fieldGetter.invoke(object);
  }

}
