package com.auzmor.impl.util.db;

import com.auzmor.impl.configuration.MysqldbConf;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.commons.dbutils.DbUtils;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DatabaseUtil {
  /**
   * insertRecord insert record in database using JDBC
   *
   * @param query
   * @return
   * @throws Exception
   */
  public static int insertRecord(String query) throws SQLException, ClassNotFoundException {
    Connection connectionObject = null;
    Statement preparedStatement = null;
    ResultSet resultSet = null;
    int generatedId = 0;
    try {
      connectionObject = new MysqldbConf().getConnection();
      preparedStatement = connectionObject.createStatement();
      preparedStatement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
      resultSet = preparedStatement.getGeneratedKeys();

      if (resultSet.next()) {
        generatedId = resultSet.getInt(1);
      }
    } finally {
      DbUtils.closeQuietly(connectionObject, preparedStatement, resultSet);
    }
    return generatedId;
  }

  /**
   * updateRecord delete record in database using JDBC
   *
   * @param query
   * @return
   * @throws Exception
   */
  public static int updateRecord(String query) throws SQLException, ClassNotFoundException {
    Connection connectionObject = null;
    Statement statement = null;
    int recordsUpdated = 0;
    try {
      connectionObject = new MysqldbConf().getConnection();
      statement = connectionObject.createStatement();
      recordsUpdated = statement.executeUpdate(query);
    } finally {
      DbUtils.closeQuietly(connectionObject);
    }

    return recordsUpdated;
  }

  /**
   * buildConstraintString build a constraint string for 'where' clause
   *
   * @param constraintList
   * @return
   */
  public static String buildConstraintString(List<String> constraintList) {
    String constraintString = "";
    if (constraintList.size() > 0) {
      constraintString += " where " + constraintList.get(0) + " ";
    }
    if (constraintList.size() > 1) {
      for (int i = 1; i < constraintList.size(); i++) {
        constraintString += " and " + constraintList.get(i) + " ";
      }
    }

    return constraintString;
  }

  /**
   * getResultSetForSelectQuery returns resultset based on query
   *
   * @param connectionObject
   * @param queryString
   * @return
   */
  public static ResultSet getResultSetForSelectQuery(Connection connectionObject, String queryString)
      throws SQLException {
    PreparedStatement preparedStatement = connectionObject.prepareStatement(queryString);
    return preparedStatement.executeQuery();
  }


  public static ResultSet getResultSetForSelectQueryStmt(Connection connectionObject, String queryString)
      throws SQLException {
    Statement preparedStatement = connectionObject.createStatement();
    return preparedStatement.executeQuery(queryString);
  }


  /**
   * prepareQueryStringValues : prepare query values for column name and column values
   *
   * @param insertionObjects
   * @return
   */
  public static List<String> prepareQueryStringValues(Map<String, Object> insertionObjects) {

    String columnValueString = "";
    String entryValueString = "";
    String appender = "";
    List<String> valueList = new ArrayList<>();

    for (Map.Entry<String, Object> columnEntries : insertionObjects.entrySet()) {

      columnValueString += columnEntries.getKey() + ",";
      Object insertObject = columnEntries.getValue();
      if (insertObject instanceof Boolean) {
        appender = insertObject.toString();
      } else {
        appender = "'" + columnEntries.getValue() + "'";
      }
      entryValueString += appender + ",";

    }
    entryValueString = entryValueString.substring(0, entryValueString.length() - 1);
    columnValueString = columnValueString.substring(0, columnValueString.length() - 1);

    valueList.add(columnValueString);
    valueList.add(entryValueString);

    return valueList;
  }

  public int deletePost(String tableName, String setFields, String constraintField, int id)
      throws SQLException,
      ClassNotFoundException {
    Connection connectionObject = new MysqldbConf().getConnection();

    Statement preparedStatement = connectionObject.createStatement();

    String queryString = "update "+ tableName +
        " set " + setFields + " where " + constraintField + " = " + id;

    int deletedRows = preparedStatement.executeUpdate(queryString);
    connectionObject.close();

    return deletedRows;
  }

  public JsonObject getSelectFields(String selectFields, String tableName, String whereFields)
      throws Exception {
    JsonObject resultObject = new JsonObject();
    String queryString = "SELECT " + selectFields + " FROM " + tableName + " WHERE " + whereFields;
    String columnName = "";
    String columnType = "";
    int recordCount = 0;

    Connection connection = new MysqldbConf().getConnection();
    PreparedStatement preparedStatement = connection.prepareStatement(queryString);
    ResultSet resultSetObject = preparedStatement.executeQuery();

    while (resultSetObject.next()) {
      ResultSetMetaData resultSetMetaData = resultSetObject.getMetaData();
      for (int i = 1; i <= resultSetObject.getMetaData().getColumnCount(); i++) {

        columnName = resultSetMetaData.getColumnName(i);
        columnType = resultSetMetaData.getColumnTypeName(i);

        if ("BIT".equalsIgnoreCase(columnType)) {
          resultObject.put(columnName, resultSetObject.getBoolean(columnName));
        } else if ("INT".equalsIgnoreCase(columnType)) {
          resultObject.put(columnName, resultSetObject.getInt(columnName));
        } else if ("VARCHAR".equalsIgnoreCase(columnType) || "BLOB".equalsIgnoreCase(columnType)) {
          String columnValue = resultSetObject.getString(columnName);
          resultObject.put(columnName, columnValue);
        } else if ("DATE".equalsIgnoreCase(columnType)) {
          resultObject.put(columnName, resultSetObject.getString(columnName));
        }

        ++recordCount;
      }
    }

    if (recordCount == 0) {
      throw new Exception("No records to display");
    }
    return resultObject;
  }
  // sadhana created these for checking Organization name

  public String OrganizationName(int id, String fieldname)
      throws SQLException, ClassNotFoundException {
    Connection connectionObject = new MysqldbConf().getConnection();
    String query = "Select " + fieldname + " from Organization " +
        " where organization_id = " + id;
    Statement preparedStatement = null;

    preparedStatement = connectionObject.createStatement();
    ResultSet rs = preparedStatement.executeQuery(query);
    String name = "";
    while (rs.next()) {
      name = rs.getString(1);
    }
    connectionObject.close();
    return name;
  }

  public String getTablename(String tablename, String fieldname, String constraint) {
    String name = "";
    Connection connectionObject = null;
    Statement preparedStatement = null;
    try {

      try {
        connectionObject = new MysqldbConf().getConnection();
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }

      String query = "Select " + fieldname + " from " + tablename + " where " + constraint + " ";
      preparedStatement = connectionObject.createStatement();

      ResultSet rs = preparedStatement.executeQuery(query);

      while (rs.next()) {
        name = rs.getString(1);
      }
      connectionObject.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return name;
  }

  // getting interview process details for candidate id only
  public JsonArray getFieldOption(Connection connectionObject, int id, String fieldname, String tablename)
      throws SQLException,ClassNotFoundException {
    Statement preparedStatement = null;
    JsonArray FieldArray = new JsonArray();
    String constraintString = "";

    if (id > 0) {
      constraintString = "where organization_id = " + id;
    }

    String queryString = "Select " + fieldname + " from " + tablename + "  " + constraintString;
    preparedStatement = connectionObject.createStatement();
    ResultSet resultSetObject = preparedStatement.executeQuery(queryString);
    int i = 0;
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    while (resultSetObject.next()) {
      i = 1;
      JsonObject dataObject = new JsonObject();
      dataObject.put("id", resultSetObject.getString(1));
      dataObject.put("name", resultSetObject.getString(2));
      FieldArray.add(dataObject);
    }

    return FieldArray;
  }

  public JsonArray getFieldOptionWithCount(Connection connectionObject, int id, String fieldName,
                                           String tableName, String countColumn)
      throws SQLException {
    PreparedStatement preparedStatement = null;
    JsonArray FieldArray = new JsonArray();
    String constraintString = "";

    if (id > 0) {
      constraintString = "where organization_id = ? and active_status=?";
    }

    String queryString = "Select " + fieldName + " from " + tableName + "  " + constraintString;
    String queryCount = "select count(" + countColumn + ") as employee_count from Employee " +
        "where " + countColumn + " = '";

    preparedStatement = connectionObject.prepareStatement(queryString);
    preparedStatement.setInt(1,id);
    preparedStatement.setString(2,"true");

    ResultSet resultSetObject = preparedStatement.executeQuery();

    while (resultSetObject.next()) {
      JsonObject dataObject = new JsonObject();
      dataObject.put("id", resultSetObject.getString(1));
      dataObject.put("name", resultSetObject.getString(2));


      PreparedStatement preStatement = connectionObject.prepareStatement
          (queryCount + resultSetObject.getString(2) +
          "' and organization_id = ? and active_status = ?");
      preStatement.setInt(1,id);
      preStatement.setInt(2,0);
      ResultSet result = preStatement.executeQuery();
      while (result.next()) {
        dataObject.put("employee_count", result.getInt(1));
      }
      FieldArray.add(dataObject);
      }
      return FieldArray;
    }


  public JsonArray getJsonArrayOption(Connection connectionObject,
                                      String fieldname,
                                      String tablename,
                                      String constraintString)
      throws Exception {
    Statement preparedStatement = null;
    JsonArray FieldArray = new JsonArray();

    String queryString = "Select " + fieldname + " from " + tablename + "  " + constraintString;

    preparedStatement = connectionObject.createStatement();
    ResultSet resultSetObject = preparedStatement.executeQuery(queryString);
    int i = 0;
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    while (resultSetObject.next()) {
      ResultSetMetaData rsmd = resultSetObject.getMetaData();
      i = 1;
      JsonObject dataObject = new JsonObject();
      dataObject.put(rsmd.getColumnName(1), resultSetObject.getString(1));
      dataObject.put(rsmd.getColumnName(2), resultSetObject.getString(2));
      FieldArray.add(dataObject);
    }
    // connectionObject.close();
    return FieldArray;
  }

  public String getUsername(String tablename, String fieldname, String constraint) {
    String name = "";
    Connection connectionObject = null;
    Statement preparedStatement = null;
    try {

      try {
        connectionObject = new MysqldbConf().getConnection();
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
      String query = "Select " + fieldname + " from " + tablename + " where " + constraint + " ";

      preparedStatement = connectionObject.createStatement();
      ResultSet resultSet = preparedStatement.executeQuery(query);
      while (resultSet.next()) {
        name = resultSet.getString(1);
        name = name + " " + resultSet.getString(2);
      }
      connectionObject.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return name;
  }


}
