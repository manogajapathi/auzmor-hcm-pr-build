package com.auzmor.impl.util;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;

public class ResponseUtil {
    public static void writeSuccessObject(int code, Object dataObject, HttpServerResponse response){
        JsonObject jsonResponseObject = new JsonObject();
        jsonResponseObject.put("code", code);
        jsonResponseObject.put("success", true);
        jsonResponseObject.put("data", dataObject);

        response
            .setStatusCode(code)
            .putHeader("Content-Type", "application/json; charset=utf-8;")
            .end(jsonResponseObject.toString());


    }

    public static void writeErrorObject(int code,String message,String description,HttpServerResponse response){
        JsonObject jsonResponseObject = new JsonObject();
        JsonObject errorObject = new JsonObject();
        errorObject.put("code",code);
        errorObject.put("message", message);
        errorObject.put("description", description);

        jsonResponseObject.put("error", errorObject);

        response
            .setStatusCode(code)
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .end(jsonResponseObject.toString());
    }
}
