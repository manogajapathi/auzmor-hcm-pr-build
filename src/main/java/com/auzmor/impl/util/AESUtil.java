package com.auzmor.impl.util;

import java.security.MessageDigest;
import javax.crypto.spec.SecretKeySpec;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

public class AESUtil {
  public static String getSecretKey() {

    String key = "";
    try {
      JsonObject vertxConfig = Vertx.currentContext().config();
      key = vertxConfig.getString("encrypt.passwordEncryptKey");
    } catch (Exception e) {
      e.printStackTrace();
    }
    return key;
  }

  public static String getAESEncyptString(String strToEncrypt) {
    SecretKeySpec secretKey;
    byte[] byteKey;
    MessageDigest messageDigest;
    String key = "";
    String aesString = "";
    try {
            /*key = getSecretKey();
            byteKey = key.getBytes("UTF-8");
            *//*messageDigest = MessageDigest.getInstance("SHA-1");
            byteKey = messageDigest.digest(byteKey);*//*
            byteKey = Arrays.copyOf(byteKey,32);
            secretKey = new SecretKeySpec(byteKey,"AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            aesString = Base64.encodeBase64String(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));*/
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return strToEncrypt;
  }
}