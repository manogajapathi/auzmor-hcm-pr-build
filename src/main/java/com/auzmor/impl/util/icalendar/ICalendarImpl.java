package com.auzmor.impl.util.icalendar;

import com.auzmor.util.icalendar.ICalendar;
import com.auzmor.util.icalendar.component.ICalendarEvent;
import com.auzmor.util.icalendar.component.ICalendarTimeZone;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

public class ICalendarImpl implements ICalendar {
  private static final String BEGIN = "BEGIN:VCALENDAR";
  private static final String END = "END:VCALENDAR";
  private static final String PRODID = "PRODID:-//Auzmor HCM //ATS";
  private static final String VERSION = "VERSION:2.0";
  private static final String ENDLINE = "\n";
  private String calScale;
  private List<ICalendarEvent> iCalendarEvents;
  private ICalendarTimeZone iCalendarTimeZone;
  
  public ICalendarImpl() {
    this.iCalendarEvents = new ArrayList<>();
    this.calScale = "GREGORIAN";
  }
  
  public ICalendar addEvent(ICalendarEvent iCalendarEvent) {
    this.iCalendarEvents.add(iCalendarEvent);
    return this;
  }
  
  public ICalendar setTimeZone(ICalendarTimeZone iCalendarTimeZone) {
    this.iCalendarTimeZone = iCalendarTimeZone;
    return this;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(BEGIN).append(ENDLINE).append(PRODID).append(ENDLINE).append(VERSION)
        .append(ENDLINE).append("CALSCALE:").append(this.calScale).append(ENDLINE);
    stringBuilder.append(iCalendarTimeZone.toString()).append(ENDLINE);
    for (ICalendarEvent calendarEvent : iCalendarEvents) {
      stringBuilder.append(calendarEvent.toString()).append(ENDLINE);
    }
    stringBuilder.append(END);
    
    return stringBuilder.toString();
  }
}
