package com.auzmor.impl.util.icalendar.component;

import com.auzmor.util.icalendar.component.ICalendarTimeZone;

public class ICalendarTimeZoneImpl implements ICalendarTimeZone {
  private static final String BEGIN = "BEGIN:VTIMEZONE";
  private static final String END = "END:VTIMEZONE";
  private static final String ENDLINE = "\n";
  private String tzId;
  
  public ICalendarTimeZoneImpl() {
    this.tzId = "Etc/UTC";
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(BEGIN).append(ENDLINE).append("TZID:").append(this.tzId).append(ENDLINE)
        .append(END);
    return stringBuilder.toString();
  }
}
