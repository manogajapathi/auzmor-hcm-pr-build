package com.auzmor.impl.util.icalendar.component;

import com.auzmor.impl.util.DateUtil;
import com.auzmor.util.icalendar.component.ICalendarEvent;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;

public class ICalendarEventImpl implements ICalendarEvent {
  private static final String BEGIN = "BEGIN:VEVENT";
  private static final String END = "END:VEVENT";
  private static final String ENDLINE = "\n";
  private static final String toDateFormat = "yyyyMMdd";
  private static final String toTimeFormat = "HHmmss";
  private String organizer;
  private String status;
  private String eventClass;
  private String startDate;
  private String endDate;
  private String startTime;
  private String endTime;
  private String summary;
  private String description;
  private String location;
  private String uid;
  private String tzId;
  
  public ICalendarEventImpl() {
    this.uid = UUID.randomUUID().toString() + "@auzmor.com";
    this.eventClass = "PUBLIC";
    this.status = "CONFIRMED";
    this.summary = null;
    this.description = null;
    this.location = null;
    this.tzId = "Etc/UTC";
  }

  public ICalendarEvent setTzId(String tzId) {
    this.tzId = tzId;
    return this;
  }

  public ICalendarEvent setStartDate(String startDate, String format) throws ParseException {
    this.startDate = convertDTFormat(startDate, format, toDateFormat);
    return this;

  }
  public ICalendarEvent setEndDate(String endDate, String format) throws ParseException {
    this.endDate = convertDTFormat(endDate, format, toDateFormat);
    return this;
  }
  public ICalendarEvent setStartTime(String startTime, String format) throws ParseException {
    this.startTime = convertDTFormat(startTime, format, toTimeFormat);
    return this;
  }
  public ICalendarEvent setEndTime(String endTime, String format) throws ParseException {
    this.endTime = convertDTFormat(endTime, format, toTimeFormat);;
    return this;
  }
  public ICalendarEvent setSummary(String summary) {
    this.summary = summary;
    return this;
  }
  public ICalendarEvent setDescription(String description) {
    this.description = description;
    return this;
  }
  public ICalendarEvent setLocation(String location) {
    this.location = location;
    return this;
  }
  public ICalendarEvent setOrganizer(String organizer) {
    this.organizer = organizer;
    return this;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(BEGIN).append(ENDLINE).append("UID:").append(uid).append(ENDLINE)
        .append("CLASS:").append(eventClass).append(ENDLINE).append("STATUS:").append(this.status).append(ENDLINE)
        .append("DTSTART;TZID=").append(this.tzId).append(":").append(this.startDate).append("T").append(this.startTime).append(ENDLINE)
        .append("DTEND;TZID=").append(this.tzId).append(":").append(this.endDate).append("T").append(this.endTime).append(ENDLINE)
        .append("CREATED:").append(new SimpleDateFormat(toDateFormat).format(Calendar.getInstance().getTime()))
        .append("T").append(new SimpleDateFormat(toTimeFormat).format(Calendar.getInstance().getTime())).append("Z")
        .append(ENDLINE);
    
    if (null != this.summary) {
      stringBuilder.append("SUMMARY:").append(this.summary).append(ENDLINE);
    }
    if (null != this.description) {
      stringBuilder.append("DESCRIPTION:").append(this.description).append(ENDLINE);
    }
    if (null != this.location) {
      stringBuilder.append("LOCATION:").append(this.location).append(ENDLINE);
    }
    if (null != this.organizer) {
      stringBuilder.append("ORGANIZER;CN=").append(this.organizer).append(":MAILTO:no-reply@auzmor.com").append(ENDLINE);
    }
    stringBuilder.append(END);
    
    return stringBuilder.toString();
  }
  
  
  private static String convertDTFormat(String dateTime, String fromFormat, String toFormat) throws ParseException {
    if (null == fromFormat) {
      return dateTime;
    }
    return new DateUtil().convertFormat(dateTime, fromFormat, toFormat);
  }
}
