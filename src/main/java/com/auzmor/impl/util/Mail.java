package com.auzmor.impl.util;

import com.auzmor.impl.enumarator.file.FileTypeEnum;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.impl.models.CandidateR.InterviewProcessCandidate;
import com.auzmor.impl.models.CandidateR.OfferProcessCandidate;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.ext.mail.*;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Mail {

  // final String serverHost = "email-smtp.us-east-1.amazonaws.com";
  final int port = 587;
  final String serverHost = "smtp.gmail.com";
  // Aws Config method smtp method
  // region US EAST
  //String userName = "AKIAIBY6M2LVSHREFSAA";
  // String password = "AmCVeQFhV3M3a7EUxb1bDal+SCfquO83m+xh/BEErIdf";   // authenication key
  String userName = "jira@auzmor.com";
  String password = "Jira@123";
  String fromMail = "no-reply@auzmor.com";
  List<MailAttachment> list = new ArrayList<MailAttachment>();

  public MailConfig getMailConfig() {
    MailConfig config = new MailConfig();
    config.setHostname(serverHost);
    config.setPort(port);
    config.setStarttls(StartTLSOptions.REQUIRED);
    config.setUsername(userName);
    config.setPassword(password);
    return config;
  }

  public String mail(InterviewProcessCandidate interviewProcessModel, String mail,
                     Vertx vertx) throws FileNotFoundException {
    try {

      /*MailClient mailClient = MailClient.createNonShared(vertx, getMailConfig());

      MailMessage message = new MailMessage();
      message.setFrom(fromMail);
      message.setTo(mail);
      message.setSubject(interviewProcessModel.getSubject_name());
      message.setHtml(interviewProcessModel.getInterview_mail_description());*/
     /* try {
        if (interviewProcessModel.getInterview_attachment_name().contains("file-uploads")) {
          /*if (interviewProcessModel.getInterview_attachment_name().contains(",")) {
            String attachment_name = interviewProcessModel.getInterview_attachment_name();
            List<String> myList = new ArrayList<String>(Arrays.asList(attachment_name.split(",")));
            for (int i = 0; i < myList.size(); i++) {

              Buffer image = vertx.fileSystem().readFileBlocking(myList.get(i));

              list.add(new MailAttachment()
                  .setData(image)
                  .setName("Interview Details")
                  .setContentType("application/*"));
            }
            message.setAttachment(list);
          } else {*/
        /*    Buffer image = vertx.fileSystem().readFileBlocking(interviewProcessModel.getInterview_attachment_name());
            MailAttachment attachment = new MailAttachment();
            attachment.setContentType("application/*");
            attachment.setName("Interview Details");
            attachment.setData(image);
            message.setAttachment(attachment);*/
      /*}*/
        /*}
      } catch (Exception e) {
        e.printStackTrace();
      }

      mailClient.sendMail(message, result -> {
        if (result.succeeded()) {
          System.out.println("mail sent");
        } else {
          result.cause().printStackTrace();
        }
      });*/
      sendMail(vertx, interviewProcessModel.getInterviewMailDescription(),
          mail, interviewProcessModel.getSubjectName());
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "success";
  }

  public String mailDetails(Vertx vertx, String mailDescription, String toAddress,
                            String subjectName, String attachmentName) throws FileNotFoundException {
    try {

      MailClient mailClient = MailClient.createNonShared(vertx, getMailConfig());

      MailMessage message = new MailMessage();
      message.setFrom(fromMail);
      message.setTo(toAddress);
      message.setSubject(subjectName);
      message.setHtml(mailDescription);
      try {

        if (attachmentName.contains(",")) {
          List<String> myList = new ArrayList<String>(Arrays.asList(attachmentName.split(",")));
          for (int i = 0; i < myList.size(); i++) {
            Buffer image = vertx.fileSystem().readFileBlocking(myList.get(i));
            String extension = myList.get(i).split("\\.")[1];
            String contentType = "";
            if (extension.equals("pdf")) {
              contentType = FileTypeEnum.PDF.getExtension();
            } else if (extension.equals("doc")) {
              contentType = FileTypeEnum.DOC.getExtension();
            } else if (extension.equals("docx")) {
              contentType = FileTypeEnum.DOCX.getExtension();
            } else if (extension.equals("png")) {
              contentType = FileTypeEnum.PNG.getExtension();
            } else if (extension.equals("ppt")) {
              contentType = FileTypeEnum.PPT.getExtension();
            } else if (extension.equals("jpeg")) {
              contentType = FileTypeEnum.JPEG.getExtension();
            } else if (extension.equals("jpg")) {
              contentType = FileTypeEnum.JPG.getExtension();
            } else if (extension.equals("xls")) {
              contentType = FileTypeEnum.XLS.getExtension();
            } else if (extension.equals("xlsx")) {
              contentType = FileTypeEnum.XLSX.getExtension();
            }
            list.add(new MailAttachment()
                .setData(image)
                .setName("MailAttachment." + extension)
                .setContentType("application/" + contentType));
          }
          message.setAttachment(list);
        } else {
          if (!StringUtil.printValidString(attachmentName).equals("")) {
            Buffer image = vertx.fileSystem().readFileBlocking(attachmentName);
            MailAttachment attachment = new MailAttachment();
            String extension = attachmentName.split("\\.")[1];
            String contentType = "";
            if (extension.equals("pdf")) {
              contentType = FileTypeEnum.PDF.getExtension();
            } else if (extension.equals("doc")) {
              contentType = FileTypeEnum.DOC.getExtension();
            } else if (extension.equals("docx")) {
              contentType = FileTypeEnum.DOCX.getExtension();
            } else if (extension.equals("png")) {
              contentType = FileTypeEnum.PNG.getExtension();
            } else if (extension.equals("ppt")) {
              contentType = FileTypeEnum.PPT.getExtension();
            } else if (extension.equals("jpeg")) {
              contentType = FileTypeEnum.JPEG.getExtension();
            } else if (extension.equals("jpg")) {
              contentType = FileTypeEnum.JPG.getExtension();
            } else if (extension.equals("xls")) {
              contentType = FileTypeEnum.XLS.getExtension();
            } else if (extension.equals("xlsx")) {
              contentType = FileTypeEnum.XLSX.getExtension();
            }

            attachment.setContentType("application/" + contentType);
            attachment.setName("MailAttachment." + extension);
            attachment.setData(image);
            message.setAttachment(attachment);
          }
        }

      } catch (Exception e) {
        e.printStackTrace();
      }

      mailClient.sendMail(message, result -> {
        if (result.succeeded()) {
          System.out.println("mail sent");
        } else {
          result.cause().printStackTrace();
        }
      });

    } catch (Exception e) {
      e.printStackTrace();
    }
    return "success";
  }

  public String offerLetter(String subjectName, String mail, String name, OfferProcessCandidate offerProcessModel,
                            String orgName, String url, Vertx vertx, long loginId, long candidateId) {
    try {
      MailClient mailClient = MailClient.createNonShared(vertx, getMailConfig());
      orgName = orgName.substring(0, 1).toUpperCase() + orgName.substring(1);

      MailMessage message = new MailMessage();
      message.setFrom(fromMail);
      message.setTo(mail);
      message.setSubject(subjectName);
      message.setHtml("Hello " + name + " <br><br> " +
          orgName + " would like to extend a job offer to you for the  position of " + offerProcessModel.getOfferPosition() + "." +
          orgName + "  would like to offer you a pay rate of [" + offerProcessModel.getCurrencyType() + "." + offerProcessModel.getOfferPayRate() + "]" +
          " which would be effective on your first day of work on " + offerProcessModel.getOfferStartDate() + " at our " +
          offerProcessModel.getOfferJobLocation() + " location. <br/>" +
          " Please review any attachments/details and <a href='https://" + url + "/careers/" + loginId + "/details/" + candidateId + "'>" +
          " click here</a> to respond to your offer. <br/> " +
          " " + offerProcessModel.getOfferContractDetails() + " <br>" +
          "We look forward to hearing from you! <br><br>" +
          "Thank you, <br/>" +
          "" + orgName + " Team <br/>" +
          "--------------------------------------------------------------\n<br/>" +
          "This is system generated mail,please do not reply to this mail\n<br/>" +
          "-------------------------------------------------------------- ");

      try {
        // for sending mutiple attachments in mail
        if (offerProcessModel.getOfferAttachmentName().contains(",")) {
          String attachment_name = offerProcessModel.getOfferAttachmentName();
          List<String> myList = new ArrayList<String>(Arrays.asList(attachment_name.split(",")));
          for (int i = 0; i < myList.size(); i++) {
            String extension = myList.get(i).split("\\.")[1];
            String contentType = "";
            if (extension.equals("pdf")) {
              contentType = FileTypeEnum.PDF.getExtension();
            } else if (extension.equals("doc")) {
              contentType = FileTypeEnum.DOC.getExtension();
            } else if (extension.equals("docx")) {
              contentType = FileTypeEnum.DOCX.getExtension();
            }
            Buffer image = vertx.fileSystem().readFileBlocking(myList.get(i));
            list.add(new MailAttachment()
                .setData(image)
                .setName("OfferLetter." + extension)
                .setContentType("application/" + contentType));

          }
          message.setAttachment(list);
        } else {
          Buffer image = vertx.fileSystem().readFileBlocking(offerProcessModel.getOfferAttachmentName());
          MailAttachment attachment = new MailAttachment();
          String extension = offerProcessModel.getOfferAttachmentName().split("\\.")[1];
          String contentType = "";
          if (extension.equals("pdf")) {
            contentType = FileTypeEnum.PDF.getExtension();
          } else if (extension.equals("doc")) {
            contentType = FileTypeEnum.DOC.getExtension();
          } else if (extension.equals("docx")) {
            contentType = FileTypeEnum.DOCX.getExtension();
          }
          attachment.setContentType("application/" + contentType);
          attachment.setName("OfferLetter." + extension);
          attachment.setData(image);
          message.setAttachment(attachment);

        }
      } catch (Exception e) {
        e.printStackTrace();
      }
      mailClient.sendMail(message, result -> {
        if (result.succeeded()) {
          System.out.println("mail sent");
          // mailmessage = "mail sent";
        } else {
          result.cause().printStackTrace();
        }
      });

    } catch (Exception e) {
      e.printStackTrace();
    }
    return "success";
  }

  public String registeredWithOnboarding(Vertx vertx, String firstName, String encryptedString, String toEmailAddress,
                                         String organizationName, String hrisUrl) {
    try {
      organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);
      String body = "<html><body>Hello <b>" + firstName + "</b>, <br/> " + "<p>Welcome to " + organizationName +
          "!  We are excited to have you join our team.<br>" +
          "Below is a link where you will be able to set your password to login and begin the " +
          "onboarding process. Please reach out to an administrator if you have any questions or " +
          "issues related to the onboarding process. Please click the link provided below for Setting Password for Your " +
          "Organization <br/>" + "<br/><a href='https://" + hrisUrl + "/backend/login/checkReset/"
          + encryptedString + "'>Set Your Password</a><br/> Thank you," +
          "<br/>" + organizationName + " Team\n<br/>" +
          "--------------------------------------------------------------\n<br/>" +
          "This is a system generated mail.\n<br/>" +
          "Please do not reply to this mail\n<br/>" +
          "-------------------------------------------------------------- " +
          "</body></html>";

      String subject = "Onboarding Instructions";

      sendMail(vertx, body, toEmailAddress, subject);

    } catch (Exception e) {
      e.printStackTrace();
    }
    return "success";
  }

  public String registeredWithoutOnboarding(Vertx vertx, String firstName, String encryptedString, String toEmailAddress,
                                            String organizationName, String hrisUrl) {
    try {
      organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);
      String body = "<html><body>Hello <b>" + firstName + "</b>, <br/> " + "<p>Welcome to " + organizationName +
          "!  We are excited to have you join our team.<br>" +
          "Below is a link where you will be able to set your password to login, Please reach " +
          "out to an administrator if you have any questions or " +
          "issues . Please click the link provided below for Setting Password for Your " +
          "Organization <br/>" + "<br/><a href='https://" + hrisUrl + "/backend/login/checkReset/"
          + encryptedString + "'>Set Your Password</a><br/> Thank you," +
          "<br/>" + organizationName + " Team\n<br/>" +
          "--------------------------------------------------------------\n<br/>" +
          "This is a system generated mail.\n<br/>" +
          "Please do not reply to this mail\n<br/>" +
          "-------------------------------------------------------------- " +
          "</body></html>";

      String subject = "Login Details";
      sendMail(vertx, body, toEmailAddress, subject);

    } catch (Exception e) {
      e.printStackTrace();
    }
    return "success";
  }

  public String onboardingMail(int id, String firstname, String email, String organization_name, Vertx vertx) {
    try {
      // Create and build a new MailMessage object
      String body = "<html><body>Hello <b>" + firstname + "</b>, <br> " +
          "Below is a link where you will be able to login and begin the onboarding process." +
          "Please reach out to an administrator if you have any questions or issues related to the onboarding process. " +
          " <br><a href= '" + organization_name + "'>please click the link to login</a> <br>" +
          "Thank you and happy onboarding! <br>" +
          "--------------------------------------------------------------\n<br/>" +
          "This is system generated mail,please do not reply to this mail\n<br/>" +
          "-------------------------------------------------------------- " +
          "</body></html>";

      String toEmailAddress = email;

      String subject = "Onboarding Instructions";

      sendMail(vertx, body, toEmailAddress, subject);

    } catch (Exception e) {
      e.printStackTrace();
    }
    return "success";
  }

  public String resetMail(Vertx vertx, String firstName, String encryptedString, String toEmailAddress,
                          String organizationName, String logo, String hrisUrl) {
    try {
      String api_url = JWTUtil.getApiUrl();
      organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);

      String body = "<html><img src=\"https://" + hrisUrl + "/backend/" + logo + "\" alt=\"logo\"><body>" +
          organizationName + "<br/><br/>" +
          "Hello <b>" + firstName + "</b>, <br/> "
          + "<p>Please click the link provided below for resetting your password<br/>" +
          "<br/><a href='https://" + hrisUrl + "/backend/login/checkReset/" + encryptedString + "'>Reset Password</a><br/>" +
          "<br/>Link expires in 30 minutes<br/> Thank you," +
          "<br/>" + organizationName + " Team\n<br/>" +
          "--------------------------------------------------------------\n<br/>" +
          "This is a system generated mail.\n<br/>" +
          "Please do not reply to this mail\n<br/>" +
          "-------------------------------------------------------------- " +
          "</body></html>";

      String subject = "Reset Password";
      sendMail(vertx, body, toEmailAddress, subject);

    } catch (Exception e) {
      e.printStackTrace();
    }
    return "success";
  }


  public String LoginDetails(String username, String pwd, String firstName, String toEmailAddress,
                             String url, String organizationName, Vertx vertx) {
    try {
      organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);
      String body = "<html><body>Hello <b>" + firstName + "</b>, <br> <br>" +
          "<p>Welcome to " + organizationName + "!  We are excited to have you join our team.<br> " +
          "Below is a link where you will be able to login. " +
          "Please reach out to an administrator if you have any questions or issues related to our product. \n" +
          "\n <br/><a href= 'https://" + url + "'>please click the link to login</a> <br>" +
          "User ID: " + username + "\n<br>" +
          "Temporary Password: " + pwd + "\n<br><br><br>" +
          "Thank You,\n<br/>" +
          "" + organizationName + " Team\n<br>" +
          "--------------------------------------------------------------\n<br/>" +
          "This is a system generated mail.\n<br/>" +
          "Please do not reply to this mail\n<br/>" +
          "-------------------------------------------------------------- " +
          "</body></html>";

      String subject = "Login Details";
      sendMail(vertx, body, toEmailAddress, subject);

    } catch (Exception e) {
      e.printStackTrace();
    }
    return "success";
  }


  public String LoginDetailsWithToken(String username, String token, String firstName, String toEmailAddress,
                                      String url, String organizationName, Vertx vertx) {
    try {
      organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);
      String body = "<html><body>Dear <b>" + firstName + "</b>, <br> <br>" +
          "<p> Greetings from " + organizationName + "!! </p> <br/><br/>" +
          "<p>Welcome you to " + organizationName + "!  This is to inform you that your registration was successful." +
          " You can now complete your application process. " +
          "Please find below your login credentials to continue on to the job site. <br/> " +
          " Click on this link to continue:  \n<br><br>" +
          " User ID: " + username + " \n<br> " +
          "<br/><a href='https://" + url + "/backend/login/checkReset/" + token + "'> please click the link to set password</a><br/>" +
          " Kindly reach out to our support team if you face issues.<br/><br/>" +
          "Thank You,\n<br/>" +
          "" + organizationName + " Team\n<br>" +
          "--------------------------------------------------------------\n<br/>" +
          "This is a system generated mail.\n<br/>" +
          "Please do not reply to this mail\n<br/>" +
          "-------------------------------------------------------------- " +
          "</body></html>";

      String subject = "Registration Confirmation of job application";

      sendMail(vertx, body, toEmailAddress, subject);

    } catch (Exception e) {
      e.printStackTrace();
    }
    return "success";
  }


  public String sendOrganizationDetails(Vertx vertx, String firstName, String encryptedString, String toEmailAddress,
                                        String hrisUrl) {
    try {
     // organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);
      String body = "<html><body> Hello <b>" + firstName + "</b>, <br/> "
          + "<p>Please click the link provided below for Setting Password for Your " +
          "Organization <br/>" + "<br/><a href='https://" + hrisUrl + "/backend/login/checkReset/"
          + encryptedString + "'>set Your Organization Password</a><br/>" + "<br/>Link expires in" +
          " 30 minutes<br/>" +
          "<br/>Auzmor Team\n<br/>" + "<a href=\"mailto:support@auzmor.com\">support@auzmor.com</a>\n<br>" +
          "--------------------------------------------------------------\n<br/>" +
          "This is a system generated mail.\n<br/>" +
          "Please do not reply to this mail\n<br/>" +
          "-------------------------------------------------------------- " +
          "</body></html>";

      String subject = "Organization Created";
      sendMail(vertx, body, toEmailAddress, subject);

    } catch (Exception e) {
      e.printStackTrace();
    }
    return "success";
  }

  public String sendMailwithCc(Vertx vertx, String body, String fromAddress, String toAddress,
                               String cc_address, String subject) {
    MailConfig config = getMailConfig();
    MailClient mailClient = MailClient.createNonShared(vertx, config);
    MailMessage message = new MailMessage();
    message.setFrom(fromAddress);
    message.setTo(toAddress);
    message.setCc(cc_address);
    message.setSubject(subject);
    message.setHtml(body);
    mailClient.sendMail(message, result -> {
      if (result.succeeded()) {
        result.toString();
      } else {
        result.cause().printStackTrace();
      }
    });
    return "success";
  }

  public String sendMail(Vertx vertx, String body, String toAddress, String subject) {
    MailConfig config = getMailConfig();
    MailClient mailClient = MailClient.createNonShared(vertx, config);
    MailMessage message = new MailMessage();
    message.setFrom(fromMail);
    message.setTo(toAddress);

    message.setSubject(subject);
    message.setHtml(body);
    mailClient.sendMail(message, result -> {
      if (result.succeeded()) {
        result.toString();
      } else {
        result.cause().printStackTrace();
      }
    });
    return "success";
  }

  public String sendMailWithAttachment(Vertx vertx, String body, String toAddress, String subject,
                                       List<MailAttachment> mailAttachmentJsons) {
    MailConfig config = getMailConfig();
    MailClient mailClient = MailClient.createNonShared(vertx, config);
    MailMessage message = new MailMessage();
    message.setFrom(fromMail);
    message.setTo(toAddress);
    if (null != mailAttachmentJsons && mailAttachmentJsons.size() > 0) {
      List<MailAttachment> mailAttachments = new ArrayList<>();
      for (MailAttachment mailAttachmentJson : mailAttachmentJsons) {
        mailAttachments.add(mailAttachmentJson);
      }
      message.setAttachment(mailAttachments);
    }
    message.setSubject(subject);
    message.setHtml(body);
    mailClient.sendMail(message, result -> {
      if (result.succeeded()) {
        result.toString();
      } else {
        result.cause().printStackTrace();
      }
    });
    return "success";
  }

  public String sendCompleteTaskMail(String AdminEmail, Employee employee, CandidateR candidateR,
                                     String taskName, Vertx vertx, String organizationName) {
    try {
      organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);
      String body = "";
      if (null != candidateR) {
        body = "<html><body>Dear Admin</b>, <br> <br>" +
            "<p> Job task ( " + taskName + ") which was assigned to " + employee.getFirst_name() + " " + employee.getLast_name() +
            "has been completed for candidate ( " + candidateR.getFirstName() + " " + candidateR.getLastName() + ")" +
            " Kindly reach out to our support team if you face issues.<br/><br/>" +
            "Thank You,\n<br/>" +
            " " + organizationName + " Team\n<br>" +
            "--------------------------------------------------------------\n<br/>" +
            "This is a system generated mail.\n<br/>" +
            "Please do not reply to this mail\n<br/>" +
            "-------------------------------------------------------------- " +
            "</body></html>";
      } else {
        body = "<html><body>Dear Admin</b>, <br> <br>" +
            "<p> Job task ( " + taskName + ") which was assigned to " + employee.getFirst_name() + " " + employee.getLast_name() +
            "has been completed." +
            " Kindly reach out to our support team if you face issues.<br/><br/>" +
            "Thank You!\n<br/>" +
            " " + organizationName + " Team\n<br>" +
            "--------------------------------------------------------------\n<br/>" +
            "This is a system generated mail.\n<br/>" +
            "Please do not reply to this mail\n<br/>" +
            "-------------------------------------------------------------- " +
            "</body></html>";
      }
      String subject = "Task Update Confirmation of job application";

      sendMail(vertx, body, AdminEmail, subject);

    } catch (Exception e) {
      e.printStackTrace();
    }
    return "success";
  }

  public String sendAddTaskMail(String AdminEmail, Employee employee, CandidateR candidateR,
                                String JobName, Vertx vertx, String organizationName) {
    try {
      organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);
      String body = "<html><body>Dear Admin </br>" +
          "<p> Task Added  to " + employee.getFirst_name() + " " + employee.getLast_name() + " For the Job " + JobName + "</p></br>" +
          " Kindly reach out to our support team if you face issues.<br/><br/>" +
          "Thank You,\n<br/>" +
          " " + organizationName + " Team\n<br>" +
          "--------------------------------------------------------------\n<br/>" +
          "This is a system generated mail.\n<br/>" +
          "Please do not reply to this mail\n<br/>" +
          "-------------------------------------------------------------- " +
          "</body></html>";

      String subject = "Task Added Confirmation of job application";
      sendMail(vertx, body, AdminEmail, subject);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "success";
  }

}
