package com.auzmor.impl.util.validator;

import com.auzmor.util.validator.FieldOptions;
import com.auzmor.util.validator.Validator;
import edu.emory.mathcs.backport.java.util.Arrays;

import java.util.List;
import java.util.Map;

public class ValidatorImpl implements Validator {
  private List<String> params;
  Map<String, FieldOptions> validationMap;

  public ValidatorImpl(String[] params, Map<String, FieldOptions> validationMap) {
    this.setParams(Arrays.asList(params));
    this.setValidationMap(validationMap);
  }

  @Override
  public List<String> getParams() {
    return params;
  }

  private void setParams(List<String> params) {
    this.params = params;
  }

  @Override
  public Map<String, FieldOptions> getValidationMap() {
    return validationMap;
  }

  private void setValidationMap(Map<String, FieldOptions> validationMap) {
    this.validationMap = validationMap;
  }
}