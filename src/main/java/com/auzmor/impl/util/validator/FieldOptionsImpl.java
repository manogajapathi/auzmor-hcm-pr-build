package com.auzmor.impl.util.validator;

import com.auzmor.util.validator.FieldOptions;

import java.util.regex.Pattern;

  public class FieldOptionsImpl implements FieldOptions {
    private Pattern expectedPattern;
    private boolean allowEmpty;
    private boolean lengthCheck;
    private boolean htmlEscape;
    private long maxLength;
    private long minLength;

    public FieldOptionsImpl(String expectedPattern) {
      this.setExpectedPattern(Pattern.compile(expectedPattern));
      this.setAllowEmpty(false);
      this.setHtmlEscape(false);
    }

    public FieldOptionsImpl(String expectedPattern, boolean allowEmpty) {
      this.setExpectedPattern(Pattern.compile(expectedPattern));
      this.setAllowEmpty(allowEmpty);
      this.setHtmlEscape(false);
    }

    public FieldOptionsImpl(String expectedPattern, boolean allowEmpty, long minLength,
                            long maxLength) {
      this.setExpectedPattern(Pattern.compile(expectedPattern));
      this.setAllowEmpty(allowEmpty);
      this.setLengthCheck(true);
      this.setMinLength(minLength);
      this.setMaxLength(maxLength);
      this.setHtmlEscape(false);
    }

    public FieldOptionsImpl(FieldOptions fieldOptions, long minLength,
                            long maxLength) {
      this.setExpectedPattern(fieldOptions.getExpectedPattern());
      this.setAllowEmpty(fieldOptions.doAllowEmpty());
      this.setLengthCheck(true);
      this.setMinLength(minLength);
      this.setMaxLength(maxLength);
      this.setHtmlEscape(fieldOptions.doHtmlEscape());
    }

    @Override
    public Pattern getExpectedPattern() {
      return expectedPattern;
    }

    private void setExpectedPattern(Pattern expectedPattern) {
      this.expectedPattern = expectedPattern;
    }

    @Override
    public boolean doAllowEmpty() {
      return allowEmpty;
    }

    private void setAllowEmpty(boolean allowEmpty) {
      this.allowEmpty = allowEmpty;
    }

    @Override
    public boolean doLengthCheck() {
      return lengthCheck;
    }

    private void setLengthCheck(boolean lengthCheck) {
      this.lengthCheck = lengthCheck;
    }

    @Override
    public long getMaxLength() {
      return maxLength;
    }

    private void setMaxLength(long maxLength) {
      this.maxLength = maxLength;
    }

    @Override
    public long getMinLength() {
      return minLength;
    }

    private void setMinLength(long minLength) {
      this.minLength = minLength;
    }

    @Override
    public boolean doHtmlEscape() {
      return htmlEscape;
    }

    @Override
    public FieldOptions setHtmlEscape(boolean htmlEscape) {
      this.htmlEscape = htmlEscape;
      return this;
    }
}
