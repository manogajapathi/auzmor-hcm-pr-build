package com.auzmor.impl.util;

import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.model.*;
import com.auzmor.impl.model.Job.Collaborator;

import com.auzmor.impl.model.Job.CustomQuestion;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import io.vertx.core.MultiMap;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StrSubstitutor;

import java.lang.reflect.Field;
import java.security.SecureRandom;
import java.sql.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {

  int i = 0, length = 0;
  boolean isCustomQuestion = false;
  boolean isCollaborator = false;

  static final String randomBuilderString = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
      "abcdefghijklmnopqrstuvwxyz";
  static SecureRandom randomItemGenerator = new SecureRandom();

  public StringBuilder getUpdateStatement(MultiMap params) throws SQLException,ClassNotFoundException {

    String fieldValuesForSelectQuery = "";
    int iterator = 0;
    StringBuilder fieldString = new StringBuilder();
    i = 0;
    length = 0;
    length = params.size();
    for (String paramNames : params.names()) {
      fieldValuesForSelectQuery += paramNames;
      if (iterator != params.names().size() - 1) {
        fieldValuesForSelectQuery += ",";
      }
      ++iterator;
    }

    Map<String, String> columnTypeMapper = new LinkedHashMap<>();
    String selectQuery = "select " + fieldValuesForSelectQuery + " from Employee";

    Connection connectionObject = new MysqldbConf().getConnection();
    Statement preparedStatement = null;
    preparedStatement = connectionObject.createStatement();

    ResultSet resultSet = preparedStatement.executeQuery(selectQuery);
    ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

    for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
      columnTypeMapper.put(resultSetMetaData.getColumnName(i),
          resultSetMetaData.getColumnTypeName(i));
    }

    connectionObject.close();

    for (String key : params.names()) {
      if ("BIT".equalsIgnoreCase(columnTypeMapper.get(key))) {
        fieldString.append(key + "=" + params.get(key));
      } else {
        if (params.get(key).equalsIgnoreCase("Employment Information")
            && key.equalsIgnoreCase("status")) {
          key = "status";
          fieldString.append(key + "='Viewed'");

        } else {
          fieldString.append(key + "='" + params.get(key) + "'");
        }
      }
      if (i < length - 1) {
        fieldString.append(",");
      }
      i++;
    }
    return fieldString;
  }

  public String[] joincustomQuestionList(RoutingContext routingContext,
                                         CustomQuestion customQuestionModel)
      throws NoSuchFieldException {
    MultiMap params = routingContext.request().params();
    StringBuilder fieldString = new StringBuilder();
    StringBuilder valueString = new StringBuilder();
    i = 0;
    length = 0;
    length = params.size();
    for (String key : params.names()) {
      if (isCustomQuestion) {
        Field field = customQuestionModel.getClass().getField(key);
        fieldString.append(key);
        if (field.getType().toString().equals("boolean")) {
          valueString.append(params.get(key));
        } else {
          valueString.append("'");
          valueString.append(params.get(key));
          valueString.append("'");
        }

        if (i < length - 1) {
          fieldString.append(",");
          valueString.append(",");
        }

      }
      i++;
    }
    String[] returnStrings = {fieldString.toString(), valueString.toString()};
    return returnStrings;
  }

  public String[] joinCollaboratorList(RoutingContext routingContext, Collaborator collaboratorModel)
      throws NoSuchFieldException {
    MultiMap params = routingContext.request().params();
    StringBuilder fieldString = new StringBuilder();
    StringBuilder valueString = new StringBuilder();
    i = 0;
    length = 0;
    length = params.size();
    for (String key : params.names()) {
      if (isCollaborator) {
        Field field = collaboratorModel.getClass().getField(key);
        fieldString.append(key);
        if (field.getType().toString().equals("boolean")) {
          valueString.append(params.get(key));
        } else {
          valueString.append("'");
          valueString.append(params.get(key));
          valueString.append("'");
        }

        if (i < length - 1) {
          fieldString.append(",");
          valueString.append(",");
        }
      }
      i++;
    }
    String[] returnStrings = {fieldString.toString(), valueString.toString()};
    return returnStrings;
  }

  /*public String[] joinCandidateList(RoutingContext routingContext, CandidateModel candidateModel)
      throws NoSuchFieldException {
    MultiMap params = routingContext.request().params();
    StringBuilder fieldString = new StringBuilder();
    StringBuilder valueString = new StringBuilder();
    i = 0;
    length = 0;
    length = params.size();
    for (String key : params.names()) {
      Field field = candidateModel.getClass().getField(key);
      fieldString.append(key);

      if (field.getType().toString().equals("boolean")) {
        valueString.append(params.get(key));
      } else {
        valueString.append("'");
        valueString.append(params.get(key));
        valueString.append("'");
      }

      if (i < length - 1) {
        fieldString.append(",");
        valueString.append(",");
      }

      i++;
    }
    String[] returnStrings = {fieldString.toString(), valueString.toString()};
    return returnStrings;
  }*/

  public String[] joinRegisterList(RoutingContext routingContext, UserLoginModel userLoginModel)
      throws NoSuchFieldException {
    MultiMap params = routingContext.request().params();
    StringBuilder fieldString = new StringBuilder();
    StringBuilder valueString = new StringBuilder();
    i = 0;
    length = 0;
    length = params.size();
    for (String key : params.names()) {
      Field field = userLoginModel.getClass().getField(key);
      fieldString.append(key);
      if (field.getType().toString().equals("boolean")) {
        valueString.append(params.get(key));
      } else {
        valueString.append("'");
        valueString.append(params.get(key));
        valueString.append("'");
      }

      if (i < length - 1) {
        fieldString.append(",");
        valueString.append(",");
      }

      i++;
    }
    String[] returnStrings = {fieldString.toString(), valueString.toString()};
    return returnStrings;
  }

  public String[] joinTitleList(RoutingContext routingContext, TitleModel titleModel)
      throws NoSuchFieldException {
    MultiMap params = routingContext.request().params();
    StringBuilder fieldString = new StringBuilder();
    StringBuilder valueString = new StringBuilder();
    i = 0;
    length = 0;
    length = params.size();
    for (String key : params.names()) {
      Field field = titleModel.getClass().getField(key);
      fieldString.append(key);
      if (params.get(key).equals("")) {
        routingContext.response()
            .setStatusCode(400)
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .end("{\"status\": \"failure\"}");
      } else {
        if (field.getType().toString().equals("boolean") ||
            field.getType().toString().equals("int")) {
          valueString.append(params.get(key));
        } else {
          valueString.append("'");
          valueString.append(params.get(key));
          valueString.append("'");
        }


        if (i < length - 1) {
          fieldString.append(",");
          valueString.append(",");
        }
      }

      i++;
    }
    String[] returnStrings = {fieldString.toString(), valueString.toString()};
    return returnStrings;
  }

  public String[] joinDepartmentList(RoutingContext routingContext, DepartmentModel departmentModel)
      throws NoSuchFieldException {
    MultiMap params = routingContext.request().params();
    StringBuilder fieldString = new StringBuilder();
    StringBuilder valueString = new StringBuilder();
    i = 0;
    length = 0;
    length = params.size();
    for (String key : params.names()) {
      Field field = departmentModel.getClass().getDeclaredField(key);
      fieldString.append(key);
      if (params.get(key).equals("")) {
        routingContext.response()
            .setStatusCode(400)
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .end("{\"status\": \"failure\"}");
      } else {
        if (field.getType().toString().equals("boolean") ||
            field.getType().toString().equals("int")) {
          valueString.append(params.get(key));
        } else {
          valueString.append("'");
          valueString.append(params.get(key));
          valueString.append("'");
        }


        if (i < length - 1) {
          fieldString.append(",");
          valueString.append(",");
        }
      }

      i++;
    }
    String[] returnStrings = {fieldString.toString(), valueString.toString()};
    return returnStrings;
  }

  public String[] joinLocationList(RoutingContext routingContext, LocationModel locationModel)
      throws NoSuchFieldException {
    MultiMap params = routingContext.request().params();
    StringBuilder fieldString = new StringBuilder();
    StringBuilder valueString = new StringBuilder();
    i = 0;
    length = 0;
    length = params.size();
    for (String key : params.names()) {
      Field field = locationModel.getClass().getField(key);
      fieldString.append(key);
      if (params.get(key).equals("")) {
        routingContext.response()
            .setStatusCode(400)
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .end("{\"status\": \"failure\"}");
      } else {
        if (field.getType().toString().equals("boolean") ||
            field.getType().toString().equals("int")) {
          valueString.append(params.get(key));
        } else {
          valueString.append("'");
          valueString.append(params.get(key));
          valueString.append("'");
        }


        if (i < length - 1) {
          fieldString.append(",");
          valueString.append(",");
        }
      }

      i++;
    }
    String[] returnStrings = {fieldString.toString(), valueString.toString()};
    return returnStrings;
  }

  public String[] joinEmploymentList(RoutingContext routingContext, EmploymentModel employmentModel)
      throws NoSuchFieldException {
    MultiMap params = routingContext.request().params();
    StringBuilder fieldString = new StringBuilder();
    StringBuilder valueString = new StringBuilder();
    i = 0;
    length = 0;
    length = params.size();
    for (String key : params.names()) {
      Field field = employmentModel.getClass().getField(key);
      fieldString.append(key);

      if (params.get(key).equals("")) {
        routingContext.response()
            .setStatusCode(400)
            .putHeader("Content-Type", "application/json; charset=utf-8")
            .end("{\"status\": \"failure\"}");
      } else {
        if (field.getType().toString().equals("boolean") ||
            field.getType().toString().equals("int")) {
          valueString.append(params.get(key));
        } else {
          valueString.append("'");
          valueString.append(params.get(key));
          valueString.append("'");
        }


        if (i < length - 1) {
          fieldString.append(",");
          valueString.append(",");
        }
      }

      i++;
    }
    String[] returnStrings = {fieldString.toString(), valueString.toString()};
    return returnStrings;
  }

  public StringBuilder updateLoginList(RoutingContext routingContext, UserLoginModel userLoginModel)
      throws NoSuchFieldException {
    MultiMap params = routingContext.request().params();
    StringBuilder fieldString = new StringBuilder();
    i = 0;
    length = 0;
    length = params.size();
    for (String key : params.names()) {
      Field field = userLoginModel.getClass().getField(key);
      if (field.getType().toString().equals("boolean")) {
        fieldString.append(key + "=" + params.get(key));
      } else {
        fieldString.append(key + "='" + params.get(key) + "'");
      }
      if (i < length - 1) {
        fieldString.append(",");
      }
      i++;
    }
    System.out.println(fieldString);
    return fieldString;
  }



  public StringBuilder updateTitleList(RoutingContext routingContext, TitleModel titleModel)
      throws NoSuchFieldException {
    MultiMap params = routingContext.request().params();
    StringBuilder fieldString = new StringBuilder();
    i = 0;
    length = 0;
    length = params.size();
    for (String key : params.names()) {
      Field field = titleModel.getClass().getField(key);
      if (field.getType().toString().equals("boolean") ||
          field.getType().toString().equals("int")) {
        fieldString.append(key + "=" + params.get(key));
      } else {
        fieldString.append(key + "='" + params.get(key) + "'");
      }
      if (i < length - 1) {
        fieldString.append(",");
      }
      i++;
    }
    System.out.println(fieldString);
    return fieldString;
  }

  public StringBuilder updateDepartmentList(RoutingContext routingContext,
                                            DepartmentModel departmentModel)
      throws NoSuchFieldException {
    MultiMap params = routingContext.request().params();
    StringBuilder fieldString = new StringBuilder();
    i = 0;
    length = 0;
    length = params.size();
    String valueMapper = "";
    for (String key : params.names()) {
      if (!key.equals("id")) {
        if (key.equals("organization_id"))
          valueMapper += key + "=" + params.get(key) + ",";
        else
          valueMapper += key + "= '" + params.get(key) + "',";
      }
    }
    StringBuilder builder = new StringBuilder(valueMapper);
    builder.deleteCharAt(valueMapper.length() - 1);
    valueMapper = builder.toString();

    return builder;
  }

  public StringBuilder updateLocationList(RoutingContext routingContext,
                                          LocationModel locationModel)
      throws NoSuchFieldException {
    MultiMap params = routingContext.request().params();
    StringBuilder fieldString = new StringBuilder();
    i = 0;
    length = 0;
    length = params.size();
    for (String key : params.names()) {
      Field field = locationModel.getClass().getField(key);
      if (field.getType().toString().equals("boolean") ||
          field.getType().toString().equals("int")) {
        fieldString.append(key + "=" + params.get(key));
      } else {
        fieldString.append(key + "='" + params.get(key) + "'");
      }
      if (i < length - 1) {
        fieldString.append(",");
      }
      i++;
    }
    System.out.println(fieldString);
    return fieldString;
  }


  public String randomString(int len) {
    StringBuilder builder = new StringBuilder(len);
    for (int i = 0; i < len; i++)
      builder.append(randomBuilderString.charAt(
          randomItemGenerator.nextInt(randomBuilderString.length())));
    return builder.toString();
  }


  public StringBuilder updateEmploymentList(RoutingContext routingContext,
                                            EmploymentModel employmentModel)
      throws NoSuchFieldException {
    MultiMap params = routingContext.request().params();
    StringBuilder fieldString = new StringBuilder();
    i = 0;
    length = 0;
    length = params.size();
    for (String key : params.names()) {
      Field field = employmentModel.getClass().getField(key);
      if (field.getType().toString().equals("boolean") ||
          field.getType().toString().equals("int")) {
        fieldString.append(key + "=" + params.get(key));
      } else {
        fieldString.append(key + "='" + params.get(key) + "'");
      }
      if (i < length - 1) {
        fieldString.append(",");
      }
      i++;
    }
    System.out.println(fieldString);
    return fieldString;
  }


  public static String convertJsonToDBString(String jsonString, String key) {
    if (jsonString != null && jsonString.length() > 0) {
      JsonArray jsonArray = new JsonArray(jsonString);
      String databaseString = "";
      for (int i = 0; i < jsonArray.size(); i++) {
        JsonObject object = jsonArray.getJsonObject(i);
        String item = object.getString(key);
        databaseString += item;
        if (i < jsonArray.size() - 1) {
          databaseString += "::";
        }
      }

      return databaseString;
    } else {
      return "";
    }
  }

  public static String convertJsonArrayToDBString(String jsonString) throws DecodeException {
    if (jsonString != null && jsonString.length() > 0) {
      JsonArray jsonArray = new JsonArray(jsonString);
      String databaseString = "";
      for (int i = 0; i < jsonArray.size(); i++) {
        String object = jsonArray.getString(i);
        databaseString += object.toString();
        if (i < jsonArray.size() - 1) {
          databaseString += "::";
        }
      }
      return databaseString;
    } else {
      return "";
    }
  }


  public static JsonArray convertDBToJsonString(String databaseColumnValue, String key) {
    if (databaseColumnValue != null && databaseColumnValue.length() > 0) {
      JsonArray array = new JsonArray();
      String stringArray[] = databaseColumnValue.split("::");
      for (int k = 0; k < stringArray.length; k++) {
        JsonObject object = new JsonObject();
        object.put(key, stringArray[k]);
        array.add(object);
      }
      return array;
    } else {
      return new JsonArray();
    }
  }

  public static JsonArray convertDBToJsonString(String databaseColumnValue) {
    if (databaseColumnValue != null && databaseColumnValue.length() > 0) {
      JsonArray array = new JsonArray();
      String stringArray[] = databaseColumnValue.split("::");
      for (int k = 0; k < stringArray.length; k++) {
        array.add(stringArray[k]);
      }
      return array;
    } else {
      return new JsonArray();
    }
  }

  public static String printValidString(String string) {
    if (string == null)
      return "";
    return string;
  }

  public static void checkRequiredFields(Map<String,Object> insertionObject,
                                         String key, String string) throws NullPointerException{
    if(string!=null && string.length()>0){
      insertionObject.put(key,string);
    } else {
      throw new NullPointerException("Required field " + key + " is missing or invalid");
    }
  }


  /**
   * substituteString - substitute set of template text with required string values
   *
   * @param originalString    The original string
   * @param substitutionItems The list of values which are to be substituted
   */
  public static String substituteString(String originalString, Map<String, String> substitutionItems) {

    StrSubstitutor stringSubstitutor = new StrSubstitutor(substitutionItems);

    originalString = originalString.replace("(", "${").replace(")", "}");

    return stringSubstitutor.replace(originalString);

  }

  /**
   *
   * @param source
   * @return
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 13/FEB/2018
   */
  public static String escapeHtml(String source) {
    return StringUtils.replaceEach(source, new String[]{"&", "<", ">", "\"", "'", "/"},
        new String[]{"&amp;", "&lt;", "&gt;", "&quot;", "&#x27;", "&#x2F;"});
  }

  /**
   *
   * @param source
   * @return
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 9/MARCH/2018
   */
  public static String unEscapeHtml(String source) {
    return StringUtils.replaceEach(source,
        new String[]{"&amp;", "&lt;", "&gt;", "&quot;", "&#x27;", "&#x2F;"},
        new String[]{"&", "<", ">", "\"", "'", "/"});
  }

  /**
   *
   * @param string
   * @return
   *
   * @author Pradeep Balasubramanian
   * @lastModifiedDate 22/MARCH/2018
   */
  public static boolean isNumber(String string) {
    try {
      int number = Integer.parseInt(string);
      return true;
    } catch(NumberFormatException numberFormatException) {
      return false;
    }
  }

  public static boolean isValidPhoneNumber(String phoneNumber, String regionCode)
      throws NumberParseException {
    if (printValidString(phoneNumber).equals("") && printValidString(regionCode).equals("")) {
      return false;
    }
    boolean isValid = false;
    String regex = "^[0-9]+$";

    Pattern patternObject = Pattern.compile(regex);

    Matcher matcher = patternObject.matcher(phoneNumber);

    if (matcher.matches()) {
      isValid = true;
    }

    /*PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
    Phonenumber.PhoneNumber phoneNumberObject = phoneNumberUtil.parseAndKeepRawInput(phoneNumber, regionCode);
    boolean isValid = phoneNumberUtil.isValidNumberForRegion(phoneNumberObject, regionCode);*/
    return isValid;
  }

}