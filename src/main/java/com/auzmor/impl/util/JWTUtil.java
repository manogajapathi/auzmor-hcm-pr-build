package com.auzmor.impl.util;

import com.auzmor.impl.exception.util.ApplicationUtilException;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

public class JWTUtil {
  public static String getEncryptKey() {
    String key = "";
    try {
      JsonObject vertxConfig = Vertx.currentContext().config();
      key = vertxConfig.getString("encrypt.tokenEncryptKey");
    } catch (Exception e) {
      e.printStackTrace();
    }
    return key;
  }

  public static String getApiUrl() {
    String key = "";
    try {
      JsonObject vertxConfig = Vertx.currentContext().config();
      key = vertxConfig.getString("api_url");
    } catch (Exception e) {
      e.printStackTrace();
    }
    return key;
  }

  public static String getTestImagePath() throws ApplicationUtilException {
    String key = "";
    try {
      JsonObject vertxConfig = Vertx.currentContext().config();
      key = vertxConfig.getString("image.path");
    } catch (IllegalArgumentException e) {
      throw new ApplicationUtilException("Upload Image Failed");
    }
    return key;
  }

  public static String getProfileImagePath() {
    String key = "";
    try {
      JsonObject vertxConfig = Vertx.currentContext().config();
      key = vertxConfig.getString("profile_image.path");
    } catch (IllegalArgumentException e) {
      e.printStackTrace();
    }
    return key;
  }

  public static String getUrlPath() throws ApplicationUtilException {
    String key = "";
    try {
      JsonObject vertxConfig = Vertx.currentContext().config();
      key = vertxConfig.getString("url.name");
    } catch (IllegalArgumentException e) {
      throw new ApplicationUtilException("Url Path Not Found");
    }
    return key;
  }

  public static String getAccountId() throws ApplicationUtilException {
    String key = "";
    try {
      JsonObject vertxConfig = Vertx.currentContext().config();
      key = vertxConfig.getString("account.sid");
    } catch (IllegalArgumentException e) {
      throw new ApplicationUtilException("Account Id is not registered");
    }
    return key;
  }
  public static String getAccountToken() throws ApplicationUtilException {
    String key = "";
    try {
      JsonObject vertxConfig = Vertx.currentContext().config();
      key = vertxConfig.getString("auth:token");
    } catch (IllegalArgumentException e) {
      throw new ApplicationUtilException("Token are invaild");
    }
    return key;
  }
}