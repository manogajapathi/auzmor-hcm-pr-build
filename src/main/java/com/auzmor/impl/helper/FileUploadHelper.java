package com.auzmor.impl.helper;

import com.auzmor.impl.enumarator.file.FileTypeEnum;

import java.awt.*;
import java.io.ByteArrayInputStream;
import io.vertx.ext.web.FileUpload;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.UUID;


public class FileUploadHelper {


  /**
   * @param fileUpload
   * @return
   * @throws IOException
   * @author Charles Sam Dilip
   * @lastModifiedDate 12/FEB/2018
   */
  public static boolean isValidOrganizationLogo(FileUpload fileUpload, FileTypeEnum fileTypeEnum)
      throws IOException {
    FileTypeEnum[] allowedContentTypes = {FileTypeEnum.PNG, FileTypeEnum.JPEG};
    int width = 360;
    int height = 162;

    if (isValidContent(fileUpload, fileTypeEnum, allowedContentTypes, width, height)) {
      return true;
    }

    return false;
  }

  /**
   * @param fileUpload
   * @return
   * @throws IOException
   * @author Charles Sam Dilip
   * @lastModifiedDate 12/FEB/2018
   */
  public static boolean isValidOrganizationFavico(FileUpload fileUpload, FileTypeEnum fileTypeEnum)
      throws IOException {
   // FileTypeEnum[] allowedContentTypes = {FileTypeEnum.PNG,FileTypeEnum.GIF,FileTypeEnum.ICO};
    FileTypeEnum[] allowedContentTypes = {FileTypeEnum.PNG,FileTypeEnum.GIF};
    int width = 32;
    int height = 32;

    if (isValidContent(fileUpload, fileTypeEnum, allowedContentTypes, width, height)) {
      return true;
    }
    return false;
  }

  /**
   * @param fileTypeEnum
   * @return
   * @author Charles Sam Dilip
   * @lastModifiedDate 25/FEB/2018
   */
  public static boolean isSupportedDocFormat(FileTypeEnum fileTypeEnum) {
    FileTypeEnum[] allowedContentTypes = {FileTypeEnum.PDF, FileTypeEnum.DOC, FileTypeEnum.DOCX};
    if (isValidContent(fileTypeEnum, allowedContentTypes)) {
      return true;
    }

    return false;
  }

  /**
   * @author sadhanaVenkatesan
   * @param fileTypeEnum
   * @return
   */
  public static boolean isSupportedPdfFormat(FileTypeEnum fileTypeEnum) {
    FileTypeEnum[] allowedContentTypes = {FileTypeEnum.PDF};
    if (isValidContent(fileTypeEnum, allowedContentTypes)) {
      return true;
    }

    return false;
  }

  /**
   * @author sadhanaVenkatesan
   * @param fileTypeEnum
   * @return
   */
  public static boolean isSupportedKeyCrtFormat(FileTypeEnum fileTypeEnum) {
    FileTypeEnum[] allowedContentTypes = {FileTypeEnum.KEY};
    if (isValidContent(fileTypeEnum, allowedContentTypes)) {
      return true;
    }
    return false;
  }

  /**
   * @author sadhanaVenkatesan
   * @param fileTypeEnum
   * @return
   */
  public static boolean isSupportedCrtFormat(FileTypeEnum fileTypeEnum) {
    FileTypeEnum[] allowedContentTypes = {FileTypeEnum.CRT};
    if (isValidContent(fileTypeEnum, allowedContentTypes)) {
      return true;
    }
    return false;
  }
  /**
   * @param fileUpload
   * @param fileTypeEnum
   * @return
   * @throws IOException
   * @author SadhanaVenkatesan
   **/

  public static boolean isValidProfileImage(FileUpload fileUpload, FileTypeEnum fileTypeEnum)
      throws IOException {
    FileTypeEnum[] allowedContentTypes = {FileTypeEnum.PNG, FileTypeEnum.JPEG};
    int width = 300;
    int height = 300;

    if (isValidContent(fileUpload, fileTypeEnum, allowedContentTypes, width, height)) {
      return true;
    }

    return false;
  }

  /**
   * @param fileTypeEnum
   * @param allowedContentTypes
   * @return
   * @throws IOException
   * @author Charles Sam Dilip
   * @lastModifiedDate 25/FEB/2018
   */

  private static boolean isValidContent(FileTypeEnum fileTypeEnum,
                                        FileTypeEnum[] allowedContentTypes) {
    if (isContentSupported(fileTypeEnum, allowedContentTypes)) {
      return true;
    }

    return false;
  }

  /**
   * @param fileUpload
   * @param fileTypeEnum
   * @param allowedContentTypes
   * @param width
   * @param height
   * @return
   * @throws IOException
   * @author Charles Sam Dilip
   * @lastModifiedDate 25/FEB/2018
   */
  private static boolean isValidContent(FileUpload fileUpload, FileTypeEnum fileTypeEnum,
                                        FileTypeEnum[] allowedContentTypes,
                                        int width, int height) throws IOException {
    if (isContentSupported(fileTypeEnum, allowedContentTypes) &&
        isValidDimension(fileUpload.uploadedFileName(), width, height)) {
      return true;
    }
    return false;
  }

  /**
   * @param filePath
   * @param width
   * @param height
   * @return
   * @throws IOException
   * @author Charles Sam Dilip
   * @lastModifiedDate 12/FEB/2018
   */
  public static boolean isValidDimension(String filePath, int width, int height)
      throws IOException {
    BufferedImage image = ImageIO.read(new File(filePath));
    if (image.getWidth() > width || image.getHeight() > height) {
      return false;
    }
    return true;
  }

  /**
   * @param fileTypeEnum
   * @param supportedContentTypes
   * @return
   * @author Charles Sam Dilip
   * @lastModifiedDate 25/FEB/2018
   */
  private static boolean isContentSupported(FileTypeEnum fileTypeEnum,
                                            FileTypeEnum[] supportedContentTypes) {
    for (FileTypeEnum docType : supportedContentTypes) {
      if (fileTypeEnum.getContentType().equalsIgnoreCase(docType.getContentType())) {
        return true;
      }
    }
    return false;

    //throw new IllegalArgumentException("Content type " + fileTypeEnum.getContentType()
      //  + " is not supported");
  }

  /**
   * @param destinationPath
   * @param dataStream
   * @return
   * @author Charles Sam Dilip
   * @lastModifiedDate 25/FEB/2018
   */
  public static String writePdfStream(String destinationPath, byte[] dataStream) {
    String pdfFile = UUID.randomUUID().toString() + FileTypeEnum.PDF.getExtension();
    FileHelper.write(pdfFile, destinationPath, dataStream);
    return pdfFile;
  }

  /**
   *
   * @param input
   * @param random
   * @return
   *
   * @author Kanniga
   * @lastModifiedBy Charles Sam Dilip
   * @lastModifiedDate 23/MAR/2018
   */
  public static String writeImageStream(String input, String random) {
    File outputFile;
    try {
      Base64.Decoder decoder = Base64.getDecoder();
      byte[] imageByte = decoder.decode(input);
      ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
      BufferedImage image = ImageIO.read(bis);
      if (image.getWidth() <= 300 && image.getHeight() <= 300) {
        outputFile = new File("image" + random + ".png");
        ImageIO.write(image, "png", outputFile);
      } else {
        return null;
      }

    } catch (IOException e) {
      return null;
    }
    return outputFile.toString();
  }
}


