package com.auzmor.impl.helper;

import com.auzmor.impl.exception.util.ApplicationUtilException;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.util.validator.FieldOptions;
import io.vertx.core.MultiMap;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.regex.Matcher;

public class ValidationHelper {
  /**
   *
   * @return
   * @author Charles Sam Dilip
   * @lastModifiedDate 13/FEB/2018
   */
  public static MultiMap validate(MultiMap params, Map<String, FieldOptions> paramValidator)
      throws ApplicationUtilException {

    for (Map.Entry<String, String> paramPair : params) {
      FieldOptions fieldOptions = paramValidator.get(paramPair.getKey());
      String value = paramPair.getValue();
      if (null != value) {
        value = value.trim();
        Matcher matcher = fieldOptions.getExpectedPattern().matcher(value);
        if (StringUtils.isAllBlank(value)) {
          if (!fieldOptions.doAllowEmpty()) {
            throw new ApplicationUtilException("Validation failed on the parameter " +
                paramPair.getKey());
          } else {
            continue;
          }
        }

        if (!matcher.matches()) {
          throw new ApplicationUtilException(paramPair.getKey()+" does not match the " +
              "accepted criteria");
        }

        if (fieldOptions.doLengthCheck()) {
          if (-1 <= fieldOptions.getMinLength() && value.length() < fieldOptions.getMinLength()) {
            throw new ApplicationUtilException("Validation failed on the parameter " +
                paramPair.getKey());
          }
          if (-1 <= fieldOptions.getMaxLength() && value.length() > fieldOptions.getMaxLength()) {
            throw new ApplicationUtilException("Validation failed on the parameter " +
                paramPair.getKey());
          }
        }

        if (fieldOptions.doHtmlEscape()) {
          value = StringUtil.escapeHtml(value);
        }
        // TODO rest of the pre processing
        params.set(paramPair.getKey(), value);
      } else if (!fieldOptions.doAllowEmpty()) {
        throw new ApplicationUtilException("Validation failed on the parameter " +
            paramPair.getKey());
      }
    }

    return params;
  }
}