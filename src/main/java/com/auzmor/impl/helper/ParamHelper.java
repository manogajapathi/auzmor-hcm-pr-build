package com.auzmor.impl.helper;

    import com.auzmor.impl.exception.util.ApplicationUtilException;
    import com.auzmor.util.validator.FieldOptions;
    import com.auzmor.util.validator.Validator;
    import edu.emory.mathcs.backport.java.util.Arrays;
    import io.vertx.core.MultiMap;
    import io.vertx.core.http.HttpServerResponse;
    import org.apache.commons.lang3.StringUtils;

    import java.util.HashMap;
    import java.util.List;
    import java.util.Map;

public class ParamHelper {

  public static boolean isParamsValid(HttpServerResponse response, MultiMap params, String[] allowedParams,
                                      String[] requiredParams, List<Validator> validators) {
    if (!isParamsAllowed(params, Arrays.asList(allowedParams))) {
      ResponseHelper.writeContainsInvalidParams(response);
      return false;
    }

    if (null != validators) {
      Map<String, FieldOptions> paramFieldOptionsMap = null;
      try {
        paramFieldOptionsMap = mapParamFieldOptions(validators);
      } catch (ApplicationUtilException applicationException) {
        ResponseHelper.writeInternalServerError(response);
        return false;
      }
      try {
        params = ValidationHelper.validate(params, paramFieldOptionsMap);
      } catch(ApplicationUtilException applicationException){
        ResponseHelper.writeContainsMisMatchParams(response, applicationException.getMessage());
        return false;
      }
    }
    if (null != requiredParams) {
      if (!hasAllRequiredParams(params, Arrays.asList(requiredParams))) {
        ResponseHelper.writeRequiredParamsMissing(response);
        return false;
      }
    }

    return true;
  }

  private static Map<String, FieldOptions> mapParamFieldOptions(List<Validator> validators)
      throws ApplicationUtilException {
    Map<String, FieldOptions> paramValidatorMap = new HashMap<>();
    for (Validator validator : validators) {
      for (String param : validator.getParams()) {
        if (null != paramValidatorMap.get(param)) {
          throw new ApplicationUtilException("Param is mapped to two validators");
        }
        paramValidatorMap.put(param, validator.getValidationMap().get(param));
      }
    }

    return paramValidatorMap;
  }

  /**
   *
   * @returns true if only
   * @author Charles Sam Dilip
   * @lastModifiedDate 13/FEB/2018
   */
  private static boolean isParamsAllowed(MultiMap params, List<String> allowedParams) {

    for (Map.Entry<String, String> paramPair : params) {
      if (!allowedParams.contains(paramPair.getKey())) {
        return false;
      }
    }

    return true;
  }

  /**
   *
   * @return
   * @author Charles Sam Dilip
   * @lastModifiedDate 13/FEB/2018
   */
  private static boolean hasAllRequiredParams(MultiMap params,
                                              List<String> requiredParams) {
    Map<String, Boolean> matchedParams = new HashMap<>();
    int matchedParamsCount = 0;

    for (Map.Entry<String, String> paramPair : params) {
      if (requiredParams.contains(paramPair.getKey())) {
        if (!StringUtils.isNotEmpty(paramPair.getValue())) {
          return false;
        }
        if (null == matchedParams.get(paramPair.getKey())) {
          matchedParams.put(paramPair.getKey(), true);
          ++matchedParamsCount;
        }
      }
    }

    return matchedParamsCount == requiredParams.size() ? true : false;
  }
}