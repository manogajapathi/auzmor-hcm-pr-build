package com.auzmor.impl.helper;

import com.auzmor.impl.enumarator.file.FileTypeEnum;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileSystem;
import io.vertx.ext.web.FileUpload;

import java.io.File;

public class FileHelper {
  /**
   * @param targetFile
   * @author Charles Sam Dilip
   * @lastModifiedDate 10/MAY/2018
   */
  public static void delete(String targetFile) {
    if (isFileExists(targetFile)) {
      Vertx.vertx().fileSystem().deleteBlocking(targetFile);
    }
  }
  /**
   * @param direcotryPath
   * @param filePath
   * @author Charles Sam Dilip
   * @lastModifiedDate 03/MAR/2018
   */
  public static void safeDelete(String direcotryPath, String filePath) {
    String targetFile = direcotryPath + File.separator + filePath;

    if (isFileExists(targetFile)) {
      Vertx.vertx().fileSystem().deleteBlocking(targetFile);
    }
  }

  /**
   * @param sourceFilePath
   * @param destinationFolderPath
   * @param destinationFilePath
   * @return
   * @author Charles Sam Dilip
   * @lastModifiedDate 03/MAR/2018
   */
  public static void move(String sourceFilePath, String destinationFolderPath,
                          String destinationFilePath) {
    FileSystem fileSystem = Vertx.vertx().fileSystem();
    if (!isFileExists(destinationFolderPath)) {
      fileSystem.mkdirsBlocking(destinationFolderPath);
    }

    fileSystem.moveBlocking(sourceFilePath, destinationFolderPath + File.separator + destinationFilePath);
  }

  /**
   * @param destinationPath
   * @param destinationFolderPath
   * @param dataStream
   * @author Charles Sam Dilip
   * @lastModifiedDate 03/MAR/2018
   */
  public static void write(String destinationPath, String destinationFolderPath, byte[] dataStream) {
    FileSystem fileSystem = Vertx.vertx().fileSystem();
    if (!isFileExists(destinationFolderPath)) {
      fileSystem.mkdirsBlocking(destinationFolderPath);
    }
    fileSystem.writeFileBlocking(destinationFolderPath + File.separator + destinationPath,
        Buffer.buffer(dataStream));
  }
  /**
   * @param fileUpload
   * @return
   * @author Kanniga Natarajan
   * @lastModifiedDate 13/APR/2018
   */
  public static FileTypeEnum getImageFormatType(FileUpload fileUpload) {
    if (fileUpload.contentType().equals(FileTypeEnum.JPEG.getContentType())) {
      return FileTypeEnum.JPEG;
    } else if (fileUpload.contentType().equals(FileTypeEnum.PNG.getContentType())) {
      return FileTypeEnum.PNG;
    }
    return null;
  }
  /**
   * @param fileUpload
   * @return
   * @author Kanniga Natarajan
   * @lastModifiedDate 13/APR/2018
   */
  public static FileTypeEnum getFavImageFormatType(FileUpload fileUpload) {
    if (fileUpload.contentType().equals(FileTypeEnum.PNG.getContentType())) {
      return FileTypeEnum.PNG;
    } else if (fileUpload.contentType().equals(FileTypeEnum.GIF.getContentType())) {
      return FileTypeEnum.GIF;
    }
    return null;
  }
  /**
   * @param fileUpload
   * @return
   * @author Charles Sam Dilip
   * @lastModifiedDate 03/MAR/2018
   */
  public static FileTypeEnum getFileType(FileUpload fileUpload) {
    if (fileUpload.contentType().equals(FileTypeEnum.DOCX.getContentType())) {
      return FileTypeEnum.DOCX;
    } else if (fileUpload.contentType().equals(FileTypeEnum.DOC.getContentType())) {
      return FileTypeEnum.DOC;
    } else if (fileUpload.contentType().equals(FileTypeEnum.PDF.getContentType())) {
      return FileTypeEnum.PDF;
    } else if (fileUpload.contentType().equals(FileTypeEnum.JPEG.getContentType())) {
      return FileTypeEnum.JPEG;
    } else if (fileUpload.contentType().equals(FileTypeEnum.PNG.getContentType())) {
      return FileTypeEnum.PNG;
    } else if (fileUpload.contentType().equals(FileTypeEnum.PNG.getContentType())) {
      return FileTypeEnum.PNG;
    } else if (fileUpload.contentType().equals(FileTypeEnum.MP4.getContentType())) {
      return FileTypeEnum.MP4;
    } else if (fileUpload.contentType().equals(FileTypeEnum.GIF.getContentType())) {
      return FileTypeEnum.GIF;
    } else if (fileUpload.contentType().equals(FileTypeEnum.ICO.getContentType())) {
      return FileTypeEnum.ICO;
    }else if (fileUpload.contentType().equals(FileTypeEnum.CRT.getContentType())) {
      return FileTypeEnum.CRT;
    }else if (fileUpload.contentType().equals(FileTypeEnum.KEY.getContentType())) {
      return FileTypeEnum.KEY;
    }
    return null;
  }
  /**
   * @param fileUpload
   * @return
   * @author Charles Sam Dilip
   * @lastModifiedDate 17/APR/2018 by Kanniga Natarajan
   */
  public static FileTypeEnum getFileTypeFoDocUpload(FileUpload fileUpload) {
    if (fileUpload.contentType().equals(FileTypeEnum.DOCX.getContentType())) {
      return FileTypeEnum.DOCX;
    } else if (fileUpload.contentType().equals(FileTypeEnum.DOC.getContentType())) {
      return FileTypeEnum.DOC;
    } else if (fileUpload.contentType().equals(FileTypeEnum.PDF.getContentType())) {
      return FileTypeEnum.PDF;
    } else if (fileUpload.contentType().equals(FileTypeEnum.JPEG.getContentType())) {
      return FileTypeEnum.JPEG;
    } else if (fileUpload.contentType().equals(FileTypeEnum.PNG.getContentType())) {
      return FileTypeEnum.PNG;
    } else if (fileUpload.contentType().equals(FileTypeEnum.JPG.getContentType())) {
      return FileTypeEnum.JPG;
    } else if (fileUpload.contentType().equals(FileTypeEnum.GIF.getContentType())) {
      return FileTypeEnum.GIF;
    }else if (fileUpload.contentType().equals(FileTypeEnum.XLS.getContentType())) {
      return FileTypeEnum.XLS;
    }else if(fileUpload.contentType().equals(FileTypeEnum.XLSX.getContentType())){
      return FileTypeEnum.XLSX;
    }else if(fileUpload.contentType().equals(FileTypeEnum.PPT.getContentType())){
      return FileTypeEnum.PPT;
    }else if(fileUpload.contentType().equals(FileTypeEnum.TXT.getContentType())){
      return FileTypeEnum.TXT;
    }
    return null;
  }
  /**
   * @param filePath
   * @return
   * @author Charles Sam Dilip
   * @lastModifiedDate 03/MAR/2018
   */
  public static boolean isFileExists(String filePath) {
    return Vertx.vertx().fileSystem().existsBlocking(filePath);
  }
  /* This method is used to check wheather the file exists in the directory
   *  @param filePath, fileName
   */
  public static boolean isFileNameExists(String filePath,String fileName){
    File name = new File (filePath,fileName);
    if(name.exists())
      return true;
    else
      return false;
  }
}

