package com.auzmor.impl.helper;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auzmor.impl.model.Login;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.util.StringUtil;
import io.vertx.core.Vertx;
import io.vertx.ext.web.Cookie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class JwtHelper {
  private static Logger logger = LoggerFactory.getLogger(JwtHelper.class);
  public static Cookie generateCookie(String key, String secret, String domain)
      throws UnsupportedEncodingException {
    long dayInSeconds = 24 * 60 * 60;
    Date expirationDate = new Date(System.currentTimeMillis() + dayInSeconds * 1000);
    JWTCreator.Builder jwtBuilder = JWT.create().withIssuer(key).withExpiresAt(expirationDate);
    Cookie cookie = Cookie.cookie("auth-token", jwtBuilder.sign(Algorithm.HMAC256(secret)))
        .setDomain(domain).setPath("/").setMaxAge(dayInSeconds).setHttpOnly(true);
    if (!Vertx.currentContext().config().getString("app.env").equals("dev")) {
      cookie.setSecure(true);
    }
    return cookie;
  }

  public static String getUserInfoJwt(String key, String secret, long jobCount, Employee employee,
                                      List<String> roles, Login login, Organization organization)
      throws UnsupportedEncodingException {
    logger.info("Generating the kong based JWT");
    long dayInSeconds = 24 * 60 * 60;
    Date expirationDate = new Date(System.currentTimeMillis() + dayInSeconds * 1000);
    JWTCreator.Builder jwtBuilder = JWT.create().withIssuer(key).withExpiresAt(expirationDate)
        .withClaim("user_type", login.getUserType())
        .withClaim("organization_id", organization.getOrganization_id())
        .withClaim("email", login.getEmail())
        .withClaim("first_name", login.getFirstName())
        .withClaim("last_name", login.getLastName())
        .withClaim("organization_name", organization.getOrganization_name())
        .withClaim("organization_url", organization.getHris_url())
        .withClaim("image_url", organization.getLogo())
        .withClaim("favicon",  StringUtil.printValidString(organization.getFavicon()))
        .withClaim("jobCountIsNull", jobCount > 0 ? false : true)
        .withClaim("user_roles", String.join(",", roles));
    long dateDiff = Calendar.getInstance().getTime().getTime() - organization.getCreated().getTime();

    float days = (dateDiff / (1000 * dayInSeconds));
    jwtBuilder.withClaim("organizationIsExpired", days > 31.0 ? true : false);
    if (null != organization.getCareer_url()) {
      jwtBuilder = jwtBuilder.withClaim("career_url", organization.getCareer_url());
    }
    if (login.getUserType().startsWith("employee") || login.getUserType().equals("admin")) {
      jwtBuilder = jwtBuilder.withClaim("user_id", (double)employee.getEmployee_id())
          .withClaim("profile_image", StringUtil.printValidString(employee.getProfile_image()));
    } else {
      jwtBuilder = jwtBuilder.withClaim("user_id", (double)login.getId())
          .withClaim("profile_image", "");
    }
    return jwtBuilder.sign(Algorithm.HMAC256(secret));
  }
}
