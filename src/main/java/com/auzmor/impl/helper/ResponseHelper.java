package com.auzmor.impl.helper;

import com.auzmor.impl.constant.response.DescriptionConstant;
import com.auzmor.impl.constant.response.MessageConstant;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;

public class ResponseHelper {
  /**
   * @param response
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */
  public static void writeContainsInvalidParams(HttpServerResponse response) {
    writeErrorResponse(HttpStatus.SC_BAD_REQUEST, MessageConstant.UNALLOWED_FIELD_PRESENT,
        DescriptionConstant.UNALLOWED_FIELD_PRESENT, response);
  }

  /**
   * @param response
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */
  public static void writeRequiredParamsMissing(HttpServerResponse response) {
    writeErrorResponse(HttpStatus.SC_UNPROCESSABLE_ENTITY,
        MessageConstant.REQUIRED_FIELDS_MISSING,
        DescriptionConstant.REQUIRED_FIELDS_MISSING, response);
  }

  /**
   * @param response
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */
  public static void writeInternalServerError(HttpServerResponse response) {
    writeErrorResponse(HttpStatus.SC_INTERNAL_SERVER_ERROR, "Internal Server Error",
        "Internal Server Error", response);
  }

  /**
   * @param response
   * @param message
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 02/MAR/2018
   */
  public static void writeContainsMisMatchParams(HttpServerResponse response, String message) {
    writeErrorResponse(HttpStatus.SC_BAD_REQUEST, message, message, response);
  }

  /**
   * @param code
   * @param message
   * @param dataObject
   * @param response
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */
  public static void writeSuccessResponse(int code, String message, JsonObject dataObject,
                                          HttpServerResponse response) {

    JsonObject jsonResponseObject = new JsonObject();
    jsonResponseObject.put("code", code);
    jsonResponseObject.put("success", true);
    if (null == dataObject || dataObject.containsKey("")) {
      dataObject = new JsonObject();
      dataObject.put("message", message);
    }
    jsonResponseObject.put("data", dataObject);

    writeResponse(code, jsonResponseObject, response);
  }

  /**
   * @param code
   * @param message
   * @param dataObject
   * @param response
   * @author Kanniga Natarajan
   * @lastModifiedDate 23/APR/2018
   */
  public static void writeErrorResponse(int code, String message, JsonObject dataObject,
                                        HttpServerResponse response) {

    JsonObject jsonResponseObject = new JsonObject();
    jsonResponseObject.put("code", code);
    jsonResponseObject.put("success", true);
    if (null == dataObject || dataObject.containsKey("")) {
      dataObject = new JsonObject();
      dataObject.put("message", message);
    }
    jsonResponseObject.put("data", dataObject);

    writeResponse(code, jsonResponseObject, response);
  }


  /**
   * @param code
   * @param message
   * @param dataObject
   * @param isNoDataAvailable
   * @param response
   * @author Pradeep Balasubramanian
   * @lastModifiedDate 13/APR/2018
   */
  public static void writeSuccessResponse(int code, String message, JsonObject dataObject,
                                          boolean isNoDataAvailable, HttpServerResponse response) {
    JsonObject jsonResponseObject = new JsonObject();
    jsonResponseObject.put("code", code);
    jsonResponseObject.put("success", true);
    if (null == dataObject || dataObject.containsKey("")) {
      dataObject = new JsonObject();
      dataObject.put("message", message);
    }
    jsonResponseObject.put("data", dataObject);
    jsonResponseObject.put("no_data", isNoDataAvailable);

    writeResponse(code, jsonResponseObject, response);
  }

  public static void writeSuccessResponse(int code, String message, JsonArray dataObject,
                                          HttpServerResponse response) {

    JsonObject jsonResponseObject = new JsonObject();
    jsonResponseObject.put("code", code);
    jsonResponseObject.put("success", true);
    jsonResponseObject.put("data", dataObject);

    if (StringUtils.isNotEmpty(message)) {
      if (message.equals("no_data is false")) {
        jsonResponseObject.put("no_data", false);
      } else if (message.equals("no_data is true")) {
        jsonResponseObject.put("no_data", true);
      }
    }

    writeResponse(code, jsonResponseObject, response);
  }

  public static void writeSuccessResponse(int code, String message, String description,
                                          HttpServerResponse response) {
    JsonObject jsonObject = new JsonObject();
    JsonObject jsonResponseObject = new JsonObject();
    jsonObject.put("code", code);
    jsonObject.put("message", message);
    jsonObject.put("description", description);
    jsonResponseObject.put("success", jsonObject);
    writeResponse(code, jsonResponseObject, response);
  }

  /**
   * @author Sadhana Venkatesan
   */

  public static void writeArraySuccessResponse(int code, String message, JsonArray dataObject,
                                               HttpServerResponse response) {
    JsonObject jsonResponseObject = new JsonObject();
    jsonResponseObject.put("code", code);
    jsonResponseObject.put("success", true);
    if (message.equals("no_data is false"))
      jsonResponseObject.put("no_data", false);
    else if (message.equals("no_data is true"))
      jsonResponseObject.put("no_data", true);
    jsonResponseObject.put("data", dataObject);
    writeResponse(code, jsonResponseObject, response);
  }

  /**
   * @param code
   * @param dataObject
   * @param response
   * @author sadhanaVenkatesan
   */
  public static void writeSuccessNoDataResponse(int code, JsonObject dataObject,
                                                HttpServerResponse response) {
    writeResponse(code, dataObject, response);
  }


  /**
   * @param code
   * @param message
   * @param description
   * @param response
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */

  public static void writeErrorResponse(int code, String message, String description,
                                        HttpServerResponse response) {
    JsonObject jsonObject = new JsonObject();
    jsonObject.put("code", code);
    jsonObject.put("message", message);
    jsonObject.put("description", description);

    JsonObject jsonResponseObject = new JsonObject();
    jsonResponseObject.put("error", jsonObject);
    writeResponse(code, jsonResponseObject, response);
  }

  /**
   * @param code
   * @param jsonResponseObject
   * @param response
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */


  private static void writeResponse(int code, JsonObject jsonResponseObject,
                                    HttpServerResponse response) {
    response
        .setStatusCode(code)
        .putHeader("Content-Type", "application/json; charset=utf-8;")
        .end(jsonResponseObject.toString());
  }

  /**
   * @author Gowtham
   * @lastModifiedDate 05/APR/2018
   */
  public static void writeSuccessResponse(JsonObject dataObject,
                                          HttpServerResponse response, boolean status, int count) {
    JsonObject jsonResponseObject = new JsonObject();
    jsonResponseObject.put("code", 200);
    jsonResponseObject.put("success", true);
    dataObject.remove("no_data");
    dataObject.remove("totalcount");
    jsonResponseObject.put("data", dataObject);
    jsonResponseObject.put("no_data", status);
    jsonResponseObject.put("totalCount", count);
    writeResponse(200, jsonResponseObject, response);
  }

  /**
   * @author Gowtham
   * @lastModifiedDate 05/APR/2018
   */
  public static void writeStringErrorResponse(String dataObject,
                                              HttpServerResponse response) {
    response
        .setStatusCode(406)
        .putHeader("Content-Type", "application/json; charset=utf-8;")
        .end(dataObject);
  }
}


