package com.auzmor.impl.helper;

import com.auzmor.impl.constant.http.HttpContentTypeConstat;
import com.auzmor.impl.constant.kong.KongConstant;
import com.auzmor.impl.enumarator.kong.KongEndpointEnum;

import com.auzmor.impl.enumarator.kong.KongHttpStatusCode;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KongServiceHelper {
  private static final Logger logger = LoggerFactory.getLogger(KongServiceHelper.class);

  /**
   *
   * @param httpClient
   * @param responseHandler
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/MAR/2018
   */
  public static void getApis(HttpClient httpClient, Handler<HttpClientResponse> responseHandler) {
    HttpServiceHelper.get(httpClient, KongEndpointEnum.API.getEndpointUrl(), null,
        responseHandler);
  }

  /**
   *
   * @param httpClient
   * @param responseHandler
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/MAR/2018
   */
  public static void getApi(HttpClient httpClient, String apiId, Handler<HttpClientResponse> responseHandler) {
    HttpServiceHelper.get(httpClient, KongEndpointEnum.API.getEndpointUrl() + apiId,
        null, responseHandler);
  }

  /**
   *
   * @param httpClient
   * @param apiName
   * @param uris
   * @param upstreamUrl
   * @param stripUri
   * @param responseHandler
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/MAR/2018
   */
  public static void addApi(HttpClient httpClient, String apiName, List<String> uris, List<String> methods,
                            String upstreamUrl, Boolean stripUri, Handler<HttpClientResponse> responseHandler) {
    Map<String, String> headers = new HashMap<String, String>(){{
      put(HttpHeaders.CONTENT_TYPE.toString(),
          HttpContentTypeConstat.APPLICATION_JSON);
    }};
    JsonObject jsonObject = new JsonObject();
    jsonObject.put("name", apiName);
    JsonArray uriJsonArray = new JsonArray();
    for (String method : methods) {
      uriJsonArray.add(method);
    }
    jsonObject.put("methods", uriJsonArray);
    JsonArray methodJsonArray = new JsonArray();
    for (String uri : uris) {
      methodJsonArray.add(uri);
    }
    jsonObject.put("uris", methodJsonArray);
    jsonObject.put("upstream_url", upstreamUrl);
    jsonObject.put("strip_uri", null == stripUri ? true : stripUri);

    HttpServiceHelper.add(httpClient, KongEndpointEnum.API.getEndpointUrl(), headers,
        jsonObject, true, responseHandler);
  }


  /**
   *
   * @param httpClient
   * @param apiId
   * @param name
   * @param uris
   * @param upstreamUrl
   * @param stripUri
   * @param responseHandler
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/MAR/2018
   */
  public static void editApi(HttpClient httpClient, String apiId, String name, String[] uris, String upstreamUrl,
                             Boolean stripUri,  Handler<HttpClientResponse> responseHandler) {
    Map<String, String> headers = new HashMap<String, String>(){{
      put(HttpHeaders.CONTENT_TYPE.toString(),
          HttpContentTypeConstat.APPLICATION_JSON);
    }};
    JsonObject jsonObject = new JsonObject();
    if (null != name) {
      jsonObject.put("name", name);
    }
    if (uris.length > 0) {
      JsonArray jsonArray = new JsonArray();
      for (String uri : uris) {
        jsonArray.add(uri);
      }
      jsonObject.put("uris", jsonArray);
    }
    if (null != upstreamUrl) {
      jsonObject.put("upstream_url", upstreamUrl);
    }
    jsonObject.put("strip_uri", null == stripUri ? true : stripUri);

    HttpServiceHelper.edit(httpClient, KongEndpointEnum.API.getEndpointUrl() + "/" +
        apiId, headers, jsonObject, HttpMethod.PATCH, true, responseHandler);
  }


  /**
   *
   * @param httpClient
   * @param apiId
   * @param responseHandler
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/MAR/2018
   */
  public static void deleteApi(HttpClient httpClient, String apiId,
                               Handler<HttpClientResponse> responseHandler) {
    HttpServiceHelper.delete(httpClient, KongEndpointEnum.API.getEndpointUrl() + "/" +
        apiId,null, responseHandler);
  }


  /**
   *
   * @param httpClient
   * @param responseHandler
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/MAR/2018
   */
  public static void getConsumers(HttpClient httpClient, Handler<HttpClientResponse> responseHandler) {
    HttpServiceHelper.get(httpClient, KongEndpointEnum.CONSUMER.getEndpointUrl(),
        null, responseHandler);
  }


  /**
   *
   * @param httpClient
   * @param id
   * @param responseHandler
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/MAR/2018
   */
  public static void addConsumer(HttpClient httpClient,  long id,
                                 Handler<HttpClientResponse> responseHandler) {
    Map<String, String> headers = new HashMap<String, String>() {{
      put(HttpHeaders.CONTENT_TYPE.toString(),
          HttpContentTypeConstat.APPLICATION_JSON);
    }};
    JsonObject jsonObject = new JsonObject();
    jsonObject.put("custom_id",  "" + id);

    HttpServiceHelper.add(httpClient, KongEndpointEnum.CONSUMER.getEndpointUrl(), headers,
        jsonObject, true, responseHandler);
  }

  /**
   *
   * @param httpClient
   * @param consumerId
   * @param name
   * @param organizationId
   * @param responseHandler
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/MAR/2018
   */
  public static void editConsumer(HttpClient httpClient, String consumerId, String name,long organizationId,
                                  Handler<HttpClientResponse> responseHandler) {
    Map<String, String> headers = new HashMap<String, String>(){{
      put(HttpHeaders.CONTENT_TYPE.toString(),
          HttpContentTypeConstat.APPLICATION_JSON);
    }};
    JsonObject jsonObject = new JsonObject();
    jsonObject.put("username", name);
    jsonObject.put("custom_id", organizationId);

    HttpServiceHelper.edit(httpClient, KongEndpointEnum.CONSUMER.getEndpointUrl() + "/" +
        consumerId, headers, jsonObject, HttpMethod.PATCH, true, responseHandler);
  }

  /**
   *
   * @param httpClient
   * @param consumerId
   * @param responseHandler
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/MAR/2018
   */
  public static void deleteConsumer(HttpClient httpClient, String consumerId,
                                    Handler<HttpClientResponse> responseHandler) {
    HttpServiceHelper.delete(httpClient, KongEndpointEnum.API.getEndpointUrl() + "/" +
        consumerId, null, responseHandler);
  }

  /**
   *
   * @param httpClient
   * @param consumerId
   * @param role
   * @param responseHandler
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 25/MAR/2018
   */
  public static void addConsumerRoles(HttpClient httpClient, String consumerId, String role,
                                      Handler<HttpClientResponse> responseHandler) {
    Map<String, String> headers = new HashMap<String, String>() {{
      put(HttpHeaders.CONTENT_TYPE.toString(),
          HttpContentTypeConstat.APPLICATION_JSON);
    }};
    JsonObject jsonObject = new JsonObject();
    jsonObject.put("group", role);

    HttpServiceHelper.add(httpClient, KongEndpointEnum.CONSUMER.getEndpointUrl() + consumerId + "/" +
        KongConstant.ACL, headers,
        jsonObject, true, responseHandler);
  }

  public static void editConsumerRoles(HttpClient httpClient, String consumerId, String role,
                                      Handler<HttpClientResponse> responseHandler) {
    Map<String, String> headers = new HashMap<String, String>() {{
      put(HttpHeaders.CONTENT_TYPE.toString(),
          HttpContentTypeConstat.APPLICATION_JSON);
    }};
    JsonObject jsonObject = new JsonObject();
    jsonObject.put("group", role);

    HttpServiceHelper.edit(httpClient, KongEndpointEnum.CONSUMER.getEndpointUrl() + consumerId + "/" +
            KongConstant.ACL, headers,
        jsonObject, HttpMethod.PUT,true, responseHandler);
  }

  public static void getConsumerRoles(HttpClient httpClient, String consumerId,
                                       Handler<HttpClientResponse> responseHandler) {
    Map<String, String> headers = new HashMap<String, String>() {{
      put(HttpHeaders.CONTENT_TYPE.toString(),
          HttpContentTypeConstat.APPLICATION_JSON);
    }};

    HttpServiceHelper.get(httpClient, KongEndpointEnum.CONSUMER.getEndpointUrl() + consumerId + "/" +
            KongConstant.ACL, headers, responseHandler);
  }

  public static void deleteConsumerRole(HttpClient httpClient, String consumerId, String role,
                                       Handler<HttpClientResponse> responseHandler) {
    Map<String, String> headers = new HashMap<String, String>() {{
      put(HttpHeaders.CONTENT_TYPE.toString(),
          HttpContentTypeConstat.APPLICATION_JSON);
    }};
    
    HttpServiceHelper.delete(httpClient, KongEndpointEnum.CONSUMER.getEndpointUrl() + consumerId + "/" +
            KongConstant.ACL + "/" + role, headers, responseHandler);
  }

  /**
   *
   * @param httpClient
   * @param apiId
   * @param pluginName
   * @param anonymousId
   * @param roles
   * @param responseHandler
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/MAR/2018
   */
  public static void addPlugin(HttpClient httpClient, String apiId, String pluginName, String anonymousId,
                              List<String> roles, Handler<HttpClientResponse> responseHandler) {
    Map<String, String> headers = new HashMap<String, String>(){{
      put(HttpHeaders.CONTENT_TYPE.toString(),
          HttpContentTypeConstat.APPLICATION_JSON);
    }};
    JsonObject jsonObject = new JsonObject();
    jsonObject.put("name", pluginName);
    if (pluginName.equals("jwt")) {
      jsonObject.put("config.uri_param_names", new JsonArray());
      jsonObject.put("config.cookie_names", "auth-token");
      jsonObject.put("config.claims_to_verify", new JsonArray(Arrays.asList("exp")));
      if (null != anonymousId) {
        jsonObject.put("config.anonymous", anonymousId);
      }
    }
    if (pluginName.equals("acl")) {
      if (null != roles) {
        jsonObject.put("config.whitelist", new JsonArray(roles));
      } else {
        System.out.println(apiId + " " + roles.toString());
      }
    }
    HttpServiceHelper.add(httpClient, KongEndpointEnum.API.getEndpointUrl() + apiId + "/" +
        KongConstant.PLUGIN, headers, jsonObject, true, responseHandler);
  }

  public static void editPlugin(HttpClient httpClient, String pluginId, String pluginName,
                               List<String> roles, Handler<HttpClientResponse> responseHandler) {
    Map<String, String> headers = new HashMap<String, String>(){{
      put(HttpHeaders.CONTENT_TYPE.toString(),
          HttpContentTypeConstat.APPLICATION_JSON);
    }};
    JsonObject jsonObject = new JsonObject();
    jsonObject.put("name", pluginName);
    if (pluginName.equals("acl")) {
      jsonObject.put("config.whitelist", String.join(",", roles));
    }
    HttpServiceHelper.edit(httpClient, KongEndpointEnum.PLUGIN.getEndpointUrl() + pluginId +
        KongConstant.PLUGIN, headers, jsonObject, HttpMethod.PATCH, true, responseHandler);
  }

  /**
   *
   * @param httpClient
   * @param apiId
   * @param responseHandler
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/MAR/2018
   */
  public static void getApiPlugins(HttpClient httpClient, String apiId,
                                   Handler<HttpClientResponse> responseHandler) {
    HttpServiceHelper.get(httpClient, KongEndpointEnum.API.getEndpointUrl() + apiId +
        KongConstant.PLUGIN, null, responseHandler);
  }

  /**
   *
   * @param httpClient
   * @param pluginId
   * @param responseHandler
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/MAR/2018
   */
  public static void getPlugins(HttpClient httpClient, String pluginId,
                                   Handler<HttpClientResponse> responseHandler) {
    HttpServiceHelper.get(httpClient, KongEndpointEnum.PLUGIN.getEndpointUrl() +
        pluginId + KongConstant.PLUGIN, null, responseHandler);
  }

  /**
   *
   * @param httpClient
   * @param responseHandler
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/MAR/2018
   */
  public static void getPlugin(HttpClient httpClient,
                               Handler<HttpClientResponse> responseHandler) {
    HttpServiceHelper.get(httpClient, KongEndpointEnum.PLUGIN.getEndpointUrl(),
        null, responseHandler);
  }

  /**
   *
   * @param httpClient
   * @param consumerId
   * @param key
   * @param secret
   * @param responseHandler
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/MAR/2018
   */
  public static void createCredential(HttpClient httpClient, String consumerId, String key, String secret,
                                      Handler<HttpClientResponse> responseHandler) {
    Map<String, String> headers = new HashMap<String, String>(){{
      put(HttpHeaders.CONTENT_TYPE.toString(),
          HttpContentTypeConstat.APPLICATION_JSON);
    }};
    JsonObject jsonObject = new JsonObject();
    jsonObject.put("key", key);
    jsonObject.put("secret", secret);
    HttpServiceHelper.add(httpClient,KongEndpointEnum.CONSUMER.getEndpointUrl() +
        consumerId + "/" + KongConstant.JWT, headers, jsonObject, true, responseHandler);
  }

  /**
   *
   * @param httpClient
   * @param consumerId
   * @param key
   * @param responseHandler
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/MAR/2018
   */
  public static void deleteCredential(HttpClient httpClient, String consumerId, String key,
                                      Handler<HttpClientResponse> responseHandler) {
    HttpServiceHelper.delete(httpClient,KongEndpointEnum.CONSUMER.getEndpointUrl() + consumerId +
        "/"  +  KongConstant.JWT  + "/" + key, null, responseHandler);
  }

  /**
   *
   * @param httpClient
   * @param consumerId
   * @param responseHandler
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/MAR/2018
   */
  public static void getCredentials(HttpClient httpClient, String consumerId,
                                    Handler<HttpClientResponse> responseHandler) {
    HttpServiceHelper.get(httpClient,KongEndpointEnum.CONSUMER.getEndpointUrl() + consumerId +
            "/" + KongConstant.JWT, null, responseHandler);
  }

  /**
   *
   * @param httpClient
   * @param pluginId
   * @param whiteListedRoles
   * @param responseHandler
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/MAR/2018
   */
  public static void editWhitelistRolesAcl(HttpClient httpClient, String pluginId,
                                  List<String> whiteListedRoles, Handler<HttpClientResponse> responseHandler) {
    Map<String, String> headers = new HashMap<String, String>(){{
      put(HttpHeaders.CONTENT_TYPE.toString(),
          HttpContentTypeConstat.APPLICATION_JSON);
    }};
    HttpServiceHelper.get(httpClient,KongEndpointEnum.PLUGIN.getEndpointUrl() +
        pluginId, headers, httpClientResponse -> {
      if (httpClientResponse.statusCode() == KongHttpStatusCode.GET.getStatusCode()) {
        httpClientResponse.bodyHandler(buffer -> {
          JsonObject responseJsonObject = new JsonObject(buffer);
          whiteListedRoles.addAll(responseJsonObject.getJsonObject("config").getJsonArray("whitelist").getList());
          JsonObject jsonObject = new JsonObject();
          jsonObject.put("config.whitelist", String.join(",", whiteListedRoles));
          HttpServiceHelper.edit(httpClient, KongEndpointEnum.PLUGIN.getEndpointUrl() +
              pluginId, headers, jsonObject, HttpMethod.PATCH, true, responseHandler);
        });
      }
    });
  }

  public static HttpClient executeWithKongClient(Vertx vertx) {
    HttpClientOptions httpClientOptions = new HttpClientOptions().setDefaultHost("localhost")
        .setDefaultPort(8001);
    return vertx.createHttpClient(httpClientOptions);
  }
}
