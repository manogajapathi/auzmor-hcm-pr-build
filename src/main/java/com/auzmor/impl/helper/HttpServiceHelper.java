package com.auzmor.impl.helper;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.ServiceDiscoveryOptions;
import io.vertx.servicediscovery.ServiceReference;

import java.util.Map;

public class HttpServiceHelper {

  /**
   * @param httpClient
   * @param uri
   * @param headers
   * @param responseHandler
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/MAR/2018
   */
  public static void get(HttpClient httpClient, String uri, Map<String, String> headers,
                         Handler<HttpClientResponse> responseHandler) {
    HttpClientRequest request = httpClient.get(uri, responseHandler);
    request = setRequestHeaders(headers, request);
    request.sendHead().end();
  }

  /**
   * @param httpClient
   * @param uri
   * @param headers
   * @param jsonObject
   * @param chunked
   * @param responseHandler
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/MAR/2018
   */
  public static void add(HttpClient httpClient, String uri, Map<String, String> headers,
                         JsonObject jsonObject, boolean chunked,
                         Handler<HttpClientResponse> responseHandler) {
      HttpClientRequest request = httpClient.post(uri, responseHandler).setChunked(chunked);
      request = setRequestHeaders(headers, request);

      request.sendHead();
      if (jsonObject == null) {
        request.end();
      } else {
        request.end(jsonObject.toBuffer());
      }
  }

  /**
   *
   * @param httpClient
   * @param uri
   * @param headers
   * @param jsonObject
   * @param method
   * @param chunked
   * @param responseHandler
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/MAR/2018
   */
  public static void edit(HttpClient httpClient, String uri, Map<String, String> headers,
                          JsonObject jsonObject, HttpMethod method, boolean chunked,
                          Handler<HttpClientResponse> responseHandler) {
      HttpClientRequest request = null;
      if (HttpMethod.PUT == method) {
        request = httpClient.put(uri, responseHandler).setChunked(chunked);
      } else if (HttpMethod.PATCH == method) {
        request = httpClient.request(HttpMethod.PATCH, uri, responseHandler).setChunked(chunked);
      }

      request = setRequestHeaders(headers, request);
      request.sendHead();
      if (jsonObject == null) {
        request.end();
      } else {
        request.end(jsonObject.toBuffer());
      }
  }

  /**
   * @param httpClient
   * @param uri
   * @param headers
   * @param responseHandler
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/MAR/2018
   */
  public static void delete(HttpClient httpClient, String uri, Map<String, String> headers,
                            Handler<HttpClientResponse> responseHandler) {
    HttpClientRequest request = httpClient.delete(uri, responseHandler);
    request = setRequestHeaders(headers, request);
    request.sendHead().end();
  }

  /**
   * @param headers
   * @param request
   * @return
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/MAR/2018
   */
  private static HttpClientRequest setRequestHeaders(Map<String, String> headers,
                                                     HttpClientRequest request) {
    if (null != headers && headers.size() > 0) {
      for (Map.Entry<String, String> header : headers.entrySet()) {
        request.putHeader(header.getKey(), header.getValue());
      }
    }
    return request;
  }

}
