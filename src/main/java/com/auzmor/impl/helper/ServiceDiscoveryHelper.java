package com.auzmor.impl.helper;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.ServiceDiscoveryOptions;

import java.io.FileNotFoundException;

public class ServiceDiscoveryHelper {
  private static final Logger logger = LoggerFactory.getLogger(ServiceDiscoveryHelper.class);
  public static ServiceDiscovery getDiscoveryObject(Vertx vertx) {
    JsonObject servicesConfig = null;
    try {
      servicesConfig = ConfigurationJsonHelper.getConf("services.json");
    } catch (FileNotFoundException fileNotFoundException) {
      logger.error("Services configuration file is missing");
      return null;
    }
    ServiceDiscovery discovery = ServiceDiscovery.create(vertx,
        new ServiceDiscoveryOptions().setBackendConfiguration(new JsonObject()
            .put("connection", servicesConfig.getString("ats.services.zookeeper.connection"))
            .put("ephemeral", true)
            .put("guaranteed", true)
            .put("basePath", servicesConfig.getString("ats.services.zookeeper.basePath"))));
    return discovery;
  }
}
