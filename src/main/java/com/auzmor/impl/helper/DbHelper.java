package com.auzmor.impl.helper;

import com.auzmor.impl.db.DbConnectionManager;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.util.db.DbHandleImpl;
import com.auzmor.util.db.DbExecuteHandler;
import com.auzmor.util.db.DbHandle;
import com.auzmor.util.db.Executor;
import com.auzmor.util.db.serialier.ResultSetSerializer;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.ServiceDiscoveryOptions;
import io.vertx.servicediscovery.ServiceReference;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Helper for the Database
 *
 * @author: Charles Sam Dilip
 * @lastModifiedDate: 31/JAN/2018
 */
public class DbHelper {
  private static final Logger logger = LoggerFactory.getLogger(DbHelper.class);

  /**
   * Feteches the required connection from pool or a new dedicated
   *
   * @param dedicated
   * @return Connection to the db
   * @throws SQLException
   *
   * @author: Charles Sam Dilip
   * @Modified: 31/JAN/2018
   */
  @Deprecated
  public static Connection getConnection(boolean dedicated) throws SQLException {
    DbConnectionManager dbConnectionManager = DbConnectionManager.getInstance();
    if (dedicated) {
      return dbConnectionManager.getDedicatedConnection();
    } else {
      return dbConnectionManager.getConnectionFromPool();
    }
  }

  /**
   * Captures the data from the data and serialize it using the resultSetSerializer
   *
   * @param resultSet
   * @param resultSetSerializer
   * @param <T>
   * @return
   * @throws SQLException on result set failure
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */
  @Deprecated
  public static <T> List<T> getSerializedObject(ResultSet resultSet,
                                                ResultSetSerializer<T> resultSetSerializer)
      throws SQLException {

    List<T> serializedObjectList = new ArrayList<>(resultSet.getFetchSize());

    // Serializing the executed fetched data
    while (resultSet.next()) {
      T object = resultSetSerializer.serialize(resultSet); // Uses the serializer method
      serializedObjectList.add(object);
    }

    return serializedObjectList;
  }

  /**
   *
   * @param routingContext
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */
  /**
   *
   * @param routingContext
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */
  public static void safeExecuteDbHandleMethod(RoutingContext routingContext,
                                               boolean dedicated, Executor executor) {
    HttpServerResponse httpServerResponse = routingContext.response();
    MultiMap requstParams = routingContext.request().params();
    // TODO get user
    boolean exceptionOccured = false;
    DbHandle dbHandle = new DbHandleImpl();

    try {
      dbHandle.checkAndEstablishConnection(dedicated);
      executor.method(httpServerResponse, requstParams, dbHandle);
    } catch (DbHandleException dbHandleException) {
      exceptionOccured = true;
      logger.error(dbHandleException.getMessage());
      ResponseHelper.writeInternalServerError(httpServerResponse);
    } catch (SQLException sqlException) {
      sqlException.printStackTrace();
      exceptionOccured = true;
      logger.error("SQL Exception: " + sqlException.getMessage());
      ResponseHelper.writeInternalServerError(httpServerResponse);
    } catch(Exception exception) {
      exceptionOccured = true;
      logger.error("Uncaught ErrorDB Helper to rollback");
    } finally {
      if (exceptionOccured) {
        safeRollback(dbHandle);
      }
      dbHandle.checkAndCloseConnection();
    }
  }


  public static void safeExecuteDbHandleMethod(Vertx vertx,
                                               boolean dedicated, DbExecuteHandler<DbHandle> dbExecuteHandler) {
    // TODO get user
    boolean exceptionOccured = false;
    DbHandle dbHandle = new DbHandleImpl();

    try {
      dbHandle.checkAndEstablishConnection(dedicated);
      dbExecuteHandler.handle(dbHandle);
    } catch (DbHandleException dbHandleException) {
      exceptionOccured = true;
      logger.error(dbHandleException.getMessage());
    } catch (SQLException sqlException) {
      sqlException.printStackTrace();
      exceptionOccured = true;
      logger.error("SQL Exception: " + sqlException.getMessage());
    }catch(Exception exception) {
      exceptionOccured = true;
      logger.error("Uncaught ErrorDB Helper to rollback");
    } finally {
      if (exceptionOccured) {
        safeRollback(dbHandle);
      }
      dbHandle.checkAndCloseConnection();
    }
  }
  /**
   *
   * @param routingContext
   * @param dedicated
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/FEB/2018
   */
  /*public static void safeExecuteDbHandleMethod(RoutingContext routingContext, boolean dedicated,
                                               DbExecuteHandler<DbHandle> dbExecuteHandler) {
    HttpServerResponse httpServerResponse = routingContext.response();
    ServiceDiscovery discovery = ServiceDiscoveryHelper.getDiscoveryObject(routingContext.vertx());
    if (null == discovery) {
      ResponseHelper.writeInternalServerError(httpServerResponse);
      return;
    }

    discovery.getRecord(record -> record.getName().equals("ats.mysql"), ar-> {
      boolean exceptionOccured = false;
      if (ar.succeeded() && null != ar.result()) {
        ServiceReference reference = discovery.getReference(ar.result());
        DbHandle dbHandle = reference.getAs(DbHandle.class);

        try {
          dbHandle.checkAndEstablishConnection(dedicated);
          dbExecuteHandler.handle(dbHandle);
        } catch (DbHandleException dbHandleException) {
          exceptionOccured = true;
          logger.error(dbHandleException.getMessage());
          ResponseHelper.writeInternalServerError(httpServerResponse);
        } catch (SQLException sqlException) {
          sqlException.printStackTrace();
          exceptionOccured = true;
          logger.error("SQL Exception: " + sqlException.getMessage());
          ResponseHelper.writeInternalServerError(httpServerResponse);
        }catch(Exception exception) {
          exceptionOccured = true;
          logger.error("Uncaught ErrorDB Helper to rollback");
          throw exception;
        } finally {
          if (exceptionOccured) {
            safeRollback(dbHandle);
          }
          //dbHandle.checkAndCloseConnection();
          reference.release();
        }
      } else {
        logger.fatal("Service not found");
        ar.cause().printStackTrace();
      }
    });

  }*/

  public static DbHandle getDBHandle(RoutingContext routingContext, boolean dedicated) {
    /*HttpServerResponse httpServerResponse = routingContext.response();
    ServiceDiscovery discovery = ServiceDiscoveryHelper.getDiscoveryObject(routingContext.vertx());
    if (null == discovery) {
      ResponseHelper.writeInternalServerError(httpServerResponse);
      return;
    }

    discovery.getRecord(record -> record.getName().equals("ats.mysql"), ar -> {
      if (ar.succeeded() && null != ar.result()) {
        ServiceReference reference = discovery.getReference(ar.result());
        try {
          dbExecuteHandler.handle(reference);
        } catch (SQLException sqlException) {
          logger.error("SQL Exception: " + sqlException.getMessage());
          ResponseHelper.writeInternalServerError(httpServerResponse);
        } catch (DbHandleException dbHandleException) {
          logger.error("SQL Exception: " + dbHandleException.getMessage());
          ResponseHelper.writeInternalServerError(httpServerResponse);
        }
      }
    });*/
    HttpServerResponse httpServerResponse = routingContext.response();
    DbHandle dbHandle = new DbHandleImpl();
    try {
      dbHandle.checkAndEstablishConnection(false);
    } catch (DbHandleException dbHandleException) {
      logger.error(dbHandleException.getMessage());
      ResponseHelper.writeInternalServerError(httpServerResponse);
      dbHandle = null;
    } catch (SQLException sqlException) {
      logger.error("SQL Exception: " + sqlException.getMessage());
      ResponseHelper.writeInternalServerError(httpServerResponse);
      dbHandle = null;
    } finally {
      return dbHandle;
    }
  }

  public static DbHandle getDBHandle(Vertx vertx, boolean dedicated) {
    /*HttpServerResponse httpServerResponse = routingContext.response();
    ServiceDiscovery discovery = ServiceDiscoveryHelper.getDiscoveryObject(routingContext.vertx());
    if (null == discovery) {
      ResponseHelper.writeInternalServerError(httpServerResponse);
      return;
    }

    discovery.getRecord(record -> record.getName().equals("ats.mysql"), ar -> {
      if (ar.succeeded() && null != ar.result()) {
        ServiceReference reference = discovery.getReference(ar.result());
        try {
          dbExecuteHandler.handle(reference);
        } catch (SQLException sqlException) {
          logger.error("SQL Exception: " + sqlException.getMessage());
          ResponseHelper.writeInternalServerError(httpServerResponse);
        } catch (DbHandleException dbHandleException) {
          logger.error("SQL Exception: " + dbHandleException.getMessage());
          ResponseHelper.writeInternalServerError(httpServerResponse);
        }
      }
    });*/
    DbHandle dbHandle = new DbHandleImpl();

    try {
      dbHandle.checkAndEstablishConnection(false);
    } catch (DbHandleException dbHandleException) {
      logger.error(dbHandleException.getMessage());
      dbHandle = null;
    } catch (SQLException sqlException) {
      logger.error("SQL Exception: " + sqlException.getMessage());
      dbHandle = null;
    } finally {
      return dbHandle;
    }
  }
  /**
   *
   * @param vertx
   * @param dedicated
   * @param dbExecuteHandler
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/FEB/2018
   */
  public static void safeExecuteDbHandleMethod(Vertx vertx, boolean dedicated, boolean autoclose,
                                               DbExecuteHandler<DbHandle> dbExecuteHandler) {
    boolean exceptionOccured = false;

    DbHandle dbHandle = DbHelper.getDBHandle(vertx, dedicated);
        try {
          dbHandle.checkAndEstablishConnection(dedicated);
          dbExecuteHandler.handle(dbHandle);
        } catch (DbHandleException dbHandleException) {
          exceptionOccured = true;
          logger.error(dbHandleException.getMessage());
        } catch (SQLException sqlException) {
          sqlException.printStackTrace();
          exceptionOccured = true;
          logger.error("SQL Exception: " + sqlException.getMessage());
        }catch(Exception exception) {
          exceptionOccured = true;
          logger.error("Uncaught ErrorDB Helper to rollback");
        } finally {
          if (exceptionOccured) {
            safeRollback(dbHandle);
          }
          if (autoclose) {
            dbHandle.checkAndCloseConnection();
          }
        }
        return;
  }




  /**
   * Used to rollback on a failed transaction safely
   *
   * @param dbHandle
   * @author Charles Sam Dilip
   * @lastModifiedDate 07/FEB/2018
   */
  public static void safeCommit(DbHandle dbHandle) {
    try {
      dbHandle.commit();
    } catch (SQLException sqlException) {
      sqlException.printStackTrace();
      logger.error("Rollback failled");
    } catch (DbHandleException dbHandleException) {
      logger.error("Not a valid connection exists in rollback");
    }
  }

  /**
   * Used to rollback on a failed transaction safely
   *
   * @param dbHandle
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */
  public static void safeRollback(DbHandle dbHandle) {
    try {
      dbHandle.rollback();
    } catch (SQLException sqlException) {
      logger.error("Rollback failled");
    } catch (DbHandleException dbHandleException) {
      logger.error("Not a valid connection exists in rollback");
    }
  }

  /**
   * Execute update queries for sql string
   *
   * @param dbHandle
   * @param sql
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */
  public static int executeUpdate(DbHandle dbHandle, String sql)
      throws SQLException, DbHandleException {

    int result = dbHandle.executeUpdate(sql);
    return result;
  }

  /**
   * Executes and returns for the boolean query for sql string
   *
   * @param dbHandle
   * @param sql
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */
  public static boolean getBoolean(DbHandle dbHandle, String sql)
      throws SQLException, DbHandleException {

    List<Boolean> resultList = dbHandle.getSerializedObject(sql, resultSet -> {
      return resultSet.getBoolean(1);
    });

    return resultList.size() > 0 ? resultList.get(1) : false;
  }


  /**
   * Execute update queries for PreparedStatement
   *
   * @param dbHandle
   * @param preparedStatement
   * @return
   * @throws SQLException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */
  public static int executeUpdate(DbHandle dbHandle, PreparedStatement preparedStatement,
                                  boolean retriveAutoGeneratedKey)
      throws SQLException, DbHandleException {

    int result = preparedStatement.executeUpdate();
    if (retriveAutoGeneratedKey) {
      ResultSet resultSet = preparedStatement.getGeneratedKeys();
      while(resultSet.next()) {
        // TODO to handle long
        result = resultSet.getInt(1);
      }
    }
    dbHandle.closePreparedStatement(preparedStatement);
    return result;
  }

  /**
   * Executes and returns for the boolean query for Prepared Statement
   *
   * @param dbHandle
   * @param preparedStatement
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */
  public static boolean getBoolean(DbHandle dbHandle, PreparedStatement preparedStatement)
      throws SQLException, DbHandleException {

    List<Boolean> resultList = dbHandle.getSerializedObject(preparedStatement, resultSet -> {
      return resultSet.getBoolean(1);
    });

    dbHandle.closePreparedStatement(preparedStatement);
    return resultList.size() > 0 ? resultList.get(0) : false;
  }

  /**
   * Executes and returns for the boolean query for Prepared Statement
   *
   * @param dbHandle
   * @param preparedStatement
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 07/FEB/2018
   */
  public static <E> E getObject(DbHandle dbHandle, PreparedStatement preparedStatement,
                                ResultSetSerializer<E> resultSetSerializer)
      throws SQLException, DbHandleException {

    List<E> resultList = dbHandle.getSerializedObject(preparedStatement, resultSetSerializer);

    dbHandle.closePreparedStatement(preparedStatement);
    return resultList.size() > 0 ? resultList.get(0) : null;
  }

  /**
   * Executes and returns for the boolean query for Prepared Statement
   *
   * @param dbHandle
   * @param preparedStatement
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 14/FEB/2018
   */
  public static <E> List<E> getObjectList(DbHandle dbHandle, PreparedStatement preparedStatement,
                                          ResultSetSerializer<E> resultSetSerializer)
      throws SQLException, DbHandleException {

    List<E> resultList = dbHandle.getSerializedObject(preparedStatement, resultSetSerializer);

    dbHandle.closePreparedStatement(preparedStatement);
    return resultList;
  }
}