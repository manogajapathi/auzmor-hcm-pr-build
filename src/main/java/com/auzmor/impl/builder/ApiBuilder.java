package com.auzmor.impl.builder;

import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.impl.util.ModelUtil;
import com.auzmor.impl.util.StringUtil;

import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerResponse;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * {@link ApiBuilder} Generalised class which helps in creating CRUD APIs
 */
public class ApiBuilder {


    /**
     * buildQuery - The method helps in building a database query based on the type of queries (select, insert, update)
     * @param queryParameters
     * @param queryType includes SELECT, INSERT AND UPDATE
     * @return queryString
     */
    protected String buildQuery(Map<String,String> queryParameters,String queryType){
        if("SELECT".equals(queryType)) {
            return QueryStringBuilder.buildSelectQuery(queryParameters);
        } else if("INSERT".equals(queryType)){
            return QueryStringBuilder.buildInsertQuery(queryParameters);
        } else if("UPDATE".equalsIgnoreCase(queryType)){
            return QueryStringBuilder.buildUpdateQuery(queryParameters);
        }
        return "";
    }


    /**
     * getResultSetObject - The method returns a resultset object for a query string parameter
     * @param connection
     * @param queryString
     * @return
     * @throws SQLException
     */
    protected ResultSet getResultSetObject(Connection connection, String queryString) throws SQLException {

        return DatabaseUtil.getResultSetForSelectQuery(connection,queryString);
    }

    /**
     * getSuccessObject - Generate a JSON Response for a valid request
     * @param dataObject
     * @param response
     * @param methodType
     */
    public void getSuccessObject(Object dataObject, HttpServerResponse response,String methodType){

        ResponseBuilder.getSuccessObject(dataObject,response,methodType);

    }

    /**
     * getErrorObject - Generate a JSON Response for invalid request
     * @param message
     * @param description
     * @param response
     * @param methodType
     */
    public void getErrorObject(String message, String description, HttpServerResponse response,String methodType){
        ResponseBuilder.getErrorObject(message,description,response,methodType);
    }

    /**
     * insertRecord - Insert Record in a database table
     * @param query
     * @return
     * @throws Exception
     */
    public int insertRecord(String query) throws Exception {
        return DatabaseUtil.insertRecord(query);
    }

    /**
     * getUpdatedRows - Update Record in a database table
     * @param query
     * @return
     * @throws Exception
     */
    public int getUpdatedRows(String query) throws SQLException, ClassNotFoundException {
        return DatabaseUtil.updateRecord(query);
    }

    /**
     * generateInsertionObject - Generate insertion object for request parameters
     * @param params
     * @param fieldMapper
     * @param object
     * @return
     * @throws Exception
     */
    public Map<String,Object> generateInsertionObject(MultiMap params,Map<String,Boolean> fieldMapper,Object object) throws Exception {
        for (String key : params.names()) {
            ModelUtil.setValues(key, params.get(key), object);
        }

        Map<String, Object> insertionObjects = new LinkedHashMap<>();

        for (Map.Entry<String, Boolean> entry : fieldMapper.entrySet()) {
            Object value = ModelUtil.getFieldValue(entry.getKey(),object);
            String type = ModelUtil.getFieldType(entry.getKey(),object);
            if(entry.getValue()){
                if("int".equals(type)){
                    if((int)(value)==0) {
                        throw new Exception("Required Field '" + entry.getKey() + "' is missing");
                    }
                } else {
                    if ("".equals(value)) {
                        throw new Exception("Required Field '" + entry.getKey() + "' is missing");
                    }
                }
                StringUtil.checkRequiredFields(insertionObjects,entry.getKey(),String.valueOf(value));
            } else {
                insertionObjects.put(entry.getKey(),ModelUtil.getFieldValue(entry.getKey(),object));
            }

        }
        return insertionObjects;
    }

    /**
     * generateUpdateStatement Generate Update Statement based on request parameters
     * @param params
     * @param object
     * @return
     * @throws Exception
     */
    public String generateUpdateStatement(MultiMap params,Object object) throws Exception {
        String updateStatement = "";
        for (String key : params.names()) {
            String fieldType = ModelUtil.getFieldType(key, object);
            if ("boolean".equals(fieldType)) {
                updateStatement += key + "=" + params.get(key) + ",";
            } else {
                updateStatement += key + "='" + params.get(key) + "',";
            }
        }
        return updateStatement.substring(0,updateStatement.length()-1);
    }
}
