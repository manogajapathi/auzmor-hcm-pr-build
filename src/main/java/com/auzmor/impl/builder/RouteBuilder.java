package com.auzmor.impl.builder;

import com.auzmor.impl.constant.file.endpoint.EndpointConstant;
import com.auzmor.impl.constant.file.upload.UploadConstant;
import com.auzmor.impl.controller.*;
import com.auzmor.impl.handler.Candidate.*;
import com.auzmor.impl.handler.*;
import com.auzmor.impl.helper.ResponseHelper;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Currency;
import java.util.List;

public class RouteBuilder {
  private static final Logger logger = LoggerFactory.getLogger(RouteBuilder.class);

  /**
   * This method adds all the routers of the verticle
   *
   * @param router
   */
  public static void addRouteHandlers(Router router) {
    addPreRouteHandlers(router);
    addStaticRouteHandlers(router);
    addRequestRouteHandlers(router);
  }

  /**
   * This method adds the global exception handler for routes
   *
   * @param router
   */
  public static void addExceptionHandlers(Router router) {
    // catches the route exception and returns ISE
    router.route().failureHandler(ctx -> {
      if (null != ctx.failure().getCause()) {
        logger.error("Route Failure", ctx.failure().getCause());
      } else {
        logger.error("Route Failure", ctx.failure());
      }

      ResponseBuilder.writeInternalServerError(ctx.response());
    });

    // catches any exception in failure handler when the response fails
    router.exceptionHandler(throwable -> {
      if (null != throwable.getCause()) {
        logger.error("Uncaught Failure", throwable.getCause());
      } else {
        logger.error("Uncaught Failure", throwable);
      }
    });
  }

  /**
   * Logs the stack trace in case of uncaught exception only
   *
   * @param throwable
   */
  private static void logRouteFailure(Throwable throwable) {
    logger.error("Fatal Error", throwable);
  }

  /**
   * This method adds the necessary pre-processing actions before the actual handler
   *
   * @param router
   */
  private static void addPreRouteHandlers(Router router) {
    // Cors Handler
    router.route().handler(CorsHandler.create("*")
        .allowedMethod(HttpMethod.GET)
        .allowedMethod(HttpMethod.POST)
        .allowedMethod(HttpMethod.PUT)
        .allowedMethod(HttpMethod.DELETE)
        .allowedMethod(HttpMethod.OPTIONS)
        .allowedHeader("application/x-www-form-urlencoded")
        .allowedHeader("Content-Type")
        .allowedHeader("X-Requested-With")
        .allowedHeader("Authorization")
        //.allowCredentials(true)
        .allowedHeader("Access-Control-Allow-Method")
        .allowedHeader("Access-Control-Allow-Origin")
        .allowedHeader("Access-Control-Allow-Credentials"));

    router.route().handler(CookieHandler.create());
    router.route().handler(CustomSessionHandler::accessInit);
  }

  /**
   * This method adds the static route handlers and static responses
   *
   * @param router
   */
  private static void addStaticRouteHandlers(Router router) {
    // Bind "/" to our hello message.
    router.get("/").handler(routingContext -> {
      routingContext.response()
          .putHeader("Content-Type", "text/html")
          .end("<h4>VertxAPI</h4>");
    });

    // Static resources
    router.get("/images/*").handler(StaticHandler.create("images"));
    router.get("/file-uploads/*").handler(StaticHandler.create("file-uploads"));
    router.get("/static/*").handler(StaticHandler.create(EndpointConstant.publicStaticRoot));
    router.get("/private_static/*").handler(StaticHandler.create(EndpointConstant.privateStaticRoot));
    router.get("/private_static_upload/*").handler(StaticHandler.create(EndpointConstant.privateStaticUploadRoot));

    router.get("/currency").consumes("").produces("application/json").handler(routingContext -> {
      List<String> currencies = new ArrayList<>();
      for (Currency currency : Currency.getAvailableCurrencies()) {
        currencies.add(currency.getCurrencyCode());
      }
      Collections.sort(currencies);
      ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK, "list of available currencies",
          new JsonArray(currencies), routingContext.response());
    });
  }

  /**
   * This method adds the custom app route handlers
   *
   * @param router
   */
  private static void addRequestRouteHandlers(Router router) {
    new AccessController(router);

    new AnnouncementController(router);

    // new CandidateController(router);
    // new CandidateHiringProcess(router);
    // new CandidateInterviewProcess(router);
    // new CandidateOfferProcessController(router);

    new EmployeeController(router);

    new FilterController(router);

    new JobController(router);

    new LoginController(router);

    new OrganizationController(router);
    new HolidayController(router);

    //new EmailTemplateController(router);
    new ReportController(router);

    new TaskController(router);     // sadhana added these task controller on 13/12/2017
    new DemoController(router);
    addInviteRoutes(router);
    addLocationRoutes(router);
    addTitleRoutes(router);
    addFieldOptionsRoutes(router);
    addDepartmentRoutes(router);
    addCandidateRoutes(router);
    addInterviewRoutes(router);
    addOfferRoutes(router);
    addHiringRoutes(router);
    addCommentsRoutes(router);
    addJobTaskRoutes(router);
    addEmailTemplateRoutes(router);
    addPacketRoutes(router);
    sendBulkMail(router);
    addNewsfeedRoutes(router);
    addReportRoutes(router);
    addPTOTasks(router);
    addSessionRoutes(router);
    getStatesRoutes(router);
    addJobDescriptionRoutes(router);
    addHighestEducationRoutes(router);
    addCallingCodeRoute(router);
    addHRISRoutes(router);
    addMinimumExperienceRoutes(router);
  }

  private static void addCallingCodeRoute(Router router) {
    router.get("/callingcode/").pathRegex("/callingcode/")
        .produces("application/json")
        .handler(CountryStateHandler::getCallingCode);
  }

  private static void getStatesRoutes(Router router) {
    router.get("/country_withstates/:countryId")
        .pathRegex("/country_withstates/(?<countryId>\\d+)")
        .produces("application/json")
        .handler(CountryStateHandler::getStatesNameswithCountryId);
    router.get("/country_withstates/:country_region_code")
        .produces("application/json")
        .handler(CountryStateHandler::getStatesNamesWithCountryRegionCode);
    router.get("/country/list")
        .produces("application/json")
        .handler(CountryStateHandler::getCountriesList);
  }

  private static void addInviteRoutes(Router router) {
    router.route("/invite*").consumes("application/x-www-form-urlencoded")
        .produces("application/json")
        .handler(BodyHandler.create().setDeleteUploadedFilesOnEnd(true));
    router.post("/invite/employee").handler(InviteHandler::inviteNewEmployee);
    router.post("/invite/token/status").handler(InviteHandler::validateTokenStatus);
    router.post("/invite/employee/add").handler(InviteHandler::addInvitedEmployee);
  }

  private static void addCandidateRoutes(Router router) {
    router.get("/candidate/list").handler(CandidateHandlerR::getCandidateList);   // candiate list using Job and Candidate
    router.get("/candidate/filter").produces("application/json")
        .handler(CandidateHandlerR::getPostJobDetails); // get the post details

    router.get("/candidate").produces("application/json").handler(CandidateHandlerR::getCandidate);     // using Candidate id
    router.get("/candidate/talentpool").produces("application/json")
        .handler(CandidateHandlerR::talentPoolList);
    router.get("/candidate/list/status").produces("application/json")
        .handler(CandidateHandlerR::getCandidateListBasedOnStatus);

    /* to fetch the availabe resumes for an candidate*/
    router.get("/candidate/resumes/:userid").produces("application/json").
        pathRegex("/candidate/resumes/(?<userid>\\d+)").handler(CandidateHandlerR::getAllResumes);
    router.delete("/candidate/document/:filePath").pathRegex("/candidate/document/(?<filePath>.*)")
        .handler(CandidateHandlerR::deleteDocument);
    router.route("/candidate/update*").consumes("application/x-www-form-urlencoded")
        .produces("application/json").
        handler(BodyHandler.create().setDeleteUploadedFilesOnEnd(true));

    router.put("/candidate/update/:candidateId")
        .pathRegex("/candidate/update/(?<candidateId>\\d+)").
        handler(CandidateHandlerR::updateCandidate); // edit rating, move and forward candidate

    router.route("/candidate/*").consumes("application/x-www-form-urlencoded")
        .produces("application/json").
        handler(BodyHandler.create().setDeleteUploadedFilesOnEnd(true));
    router.put("/candidate/:candidateId")
        .pathRegex("/candidate/(?<candidateId>\\d+)")
        .handler(CandidateHandlerR::updateCandidateId);

    router.delete("/candidate/:id")
        .pathRegex("/candidate/(?<id>\\d+)")
        .handler(CandidateHandlerR::deleteApplicant);

    router.post("/candidate/resume*").consumes("multipart/form-data")
        .produces("application/json")
        .handler(BodyHandler
            .create(UploadConstant.UPLOAD_DIRECTORY).setMergeFormAttributes(true));
    router.post("/candidate/resume").handler(CandidateHandlerR::addCandidateResume);

    router.post("/candidate/coverletter*").consumes("multipart/form-data")
        .produces("application/json")
        .handler(BodyHandler
            .create(UploadConstant.UPLOAD_DIRECTORY).setMergeFormAttributes(true));
    router.post("/candidate/coverletter").handler(CandidateHandlerR::addCandidateCoverLetter);

    router.post("/candidate/profile/image*").consumes("multipart/form-data")
        .produces("application/json")
        .handler(BodyHandler
            .create(UploadConstant.UPLOAD_DIRECTORY).setMergeFormAttributes(true));
    router.post("/candidate/profile/image").handler(CandidateHandlerR::addCandidateProfileImage);

    router.route("/candidate/create*").consumes("application/x-www-form-urlencoded")
        .produces("application/json").handler(BodyHandler.create().setDeleteUploadedFilesOnEnd(true));
    router.post("/candidate/create").handler(CandidateHandlerR::addCandidate);

  }

  private static void addHiringRoutes(Router router) {
    router.route("/candidate/hiringprocess*").consumes("application/x-www-form-urlencoded")
        .produces("application/json").handler(BodyHandler.create().setDeleteUploadedFilesOnEnd(true));
    router.post("/candidate/hiringprocess/create").
        handler(CandidateHiringProcessHandlerR::addHiringProcessData);
  }

  private static void addCommentsRoutes(Router router) {
    router.delete("/candidate/comments/delete").produces("application/json").
        handler(CandidateCommentsHandlerR::deleteCommentsDetails);
    router.route("/candidate/comments*").consumes("application/x-www-form-urlencoded")
        .produces("application/json")
        .handler(BodyHandler.create().setDeleteUploadedFilesOnEnd(true));
    router.post("/candidate/comments/create").
        handler(CandidateCommentsHandlerR::addComments);
    router.put("/candidate/comments/update").
        handler(CandidateCommentsHandlerR::updateCommentsDetails);
  }

  private static void addInterviewRoutes(Router router) {
    router.get("/candidate/interviewprocess/list").produces("application/json").
        handler(CandidateInterviewProcessHandlerR::getInterviewProcessDetails);

    router.route("/candidate/interviewprocess/*").consumes("application/x-www-form-urlencoded").
        produces("application/json").handler(BodyHandler.create().setDeleteUploadedFilesOnEnd(true));
    router.post("/candidate/interviewprocess/create").
        handler(CandidateInterviewProcessHandlerR::addInterviewProcess);

    router.put("/candidate/interviewprocess/:candidateId").
        pathRegex("/candidate/interviewprocess/(?<candidateId>\\d+)").
        handler(CandidateInterviewProcessHandlerR::updateInterviewProcess);
  }

  private static void sendBulkMail(Router router) {

    router.route("/candidate/sendmail*").
        handler(BodyHandler.create().setDeleteUploadedFilesOnEnd(true));

    router.post("/candidate/sendmail").
        handler(MailHandlerR::SendBulkMail);

    router.post("/candidate/attachment").
        handler(BodyHandler
            .create(UploadConstant.UPLOAD_DIRECTORY).setMergeFormAttributes(true));
    router.post("/candidate/attachment").consumes("multipart/form-data")
        .produces("application/json").handler(MailHandlerR::mailAttachment);

  }

  private static void addOfferRoutes(Router router) {

    router.route("/candidate/offerprocess*").consumes("application/x-www-form-urlencoded")
        .produces("application/json")
        .handler(BodyHandler.create().setDeleteUploadedFilesOnEnd(true));
    router.post("/candidate/offerprocess/create").handler(
        CandidateOfferProcessHandlerR::addOfferProcess);
    router.put("/candidate/offerprocess/:candidateId").
        pathRegex("/candidate/offerprocess/(?<candidateId>\\d+)").
        handler(CandidateOfferProcessHandlerR::updateOfferProcess);

    router.post("/candidate/offerprocess/attachment").consumes("multipart/form-data")
        .produces("application/json").
        handler(BodyHandler
            .create(UploadConstant.UPLOAD_DIRECTORY).setMergeFormAttributes(true));
    router.post("/candidate/offerprocess/attachment").
        handler(CandidateOfferProcessHandlerR::addOfferAttachment);
  }

  /**
   * this method adds static route handler for location requests
   *
   * @param router
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  private static void addLocationRoutes(Router router) {

    router.get("/location/list").produces("application/json")
        .handler(LocationHandler::getAllLocations);
    router.get("/location/:id").pathRegex("/location/(?<id>\\d+)").produces("application/json")
        .handler(LocationHandler::getLocation);
    router.delete("/location/:id").pathRegex("/location/(?<id>\\d+)")
        .produces("application/json")
        .handler(LocationHandler::deleteLocation);

    router.route("/location*").consumes("application/x-www-form-urlencoded")
        .produces("application/json")
        .handler(BodyHandler.create().setDeleteUploadedFilesOnEnd(true));
    router.post("/location/create").produces("application/json")
        .handler(LocationHandler::addLocation);
    router.put("/location/:id").pathRegex("/location/(?<id>\\d+)").produces("application/json")
        .handler(LocationHandler::updateLocation);
  }

  /**
   * this method adds static route handler for job title requests
   *
   * @param router
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  private static void addTitleRoutes(Router router) {

    router.get("/title/list").produces("application/json")
        .handler(TitleHandler::getAllTitles);
    router.get("/title/:id").pathRegex("/title/(?<id>\\d+)")
        .produces("application/json")
        .handler(TitleHandler::getTitle);
    router.delete("/title/:id").pathRegex("/title/(?<id>\\d+)")
        .produces("application/json")
        .handler(TitleHandler::deleteTitle);

    router.route("/title*").consumes("application/x-www-form-urlencoded")
        .produces("application/json")
        .handler(BodyHandler.create().setDeleteUploadedFilesOnEnd(true));
    router.post("/title/create").produces("application/json")
        .handler(TitleHandler::addTitle);
    router.put("/title/:id").pathRegex("/title/(?<id>\\d+)")
        .produces("application/json").handler(TitleHandler::updateTitle);
  }

  /**
   * this method adds static route handler for department requests
   *
   * @param router
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  private static void addDepartmentRoutes(Router router) {
    router.get("/department/list").produces("application/json")
        .handler(DepartmentHandler::getAllDepartments);
    router.get("/department/:id").pathRegex("/department/(?<id>\\d+)")
        .produces("application/json")
        .handler(DepartmentHandler::getDepartment);
    router.delete("/department/:id").pathRegex("/department/(?<id>\\d+)")
        .produces("application/json").handler(DepartmentHandler::deleteDepartment);

    router.route("/department*").consumes("application/x-www-form-urlencoded")
        .produces("application/json")
        .handler(BodyHandler.create().setDeleteUploadedFilesOnEnd(true));
    router.post("/department/create").produces("application/json")
        .handler(DepartmentHandler::addDepartment);
    router.put("/department/:id").pathRegex("/department/(?<id>\\d+)")
        .produces("application/json").handler(DepartmentHandler::updateDepartment);
  }

  /**
   * this method adds static route handler for division, paygroup and field options requests
   *
   * @param router
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15/FEB/2018
   */
  private static void addFieldOptionsRoutes(Router router) {
    router.get("/fieldOptions/list").produces("application/json")
        .handler(FieldOptionsHandler::getAllFieldOptions);
    router.route("/fieldOptions*").consumes("application/x-www-form-urlencoded")
        .produces("application/json")
        .handler(BodyHandler.create().setDeleteUploadedFilesOnEnd(true));
    router.get("/division/list/:organizationId").pathRegex("/division/list/(?<organizationId>\\d+)")
        .produces("application/json")
        .handler(DivisionHandler::getAllDivisions);
    router.get("/division/:divisionId").pathRegex("/division/(?<divisionId>\\d+)")
        .produces("application/json")
        .handler(DivisionHandler::getDivision);
    router.delete("/division/:divisionId").pathRegex("/division/(?<divisionId>\\d+)")
        .produces("application/json")
        .handler(DivisionHandler::deleteDivision);
    router.route("/division*").consumes("application/x-www-form-urlencoded").produces("application/json")
        .handler(BodyHandler.create());
    router.post("/division/create").produces("application/json")
        .handler(DivisionHandler::addDivision);
    router.put("/division/:divisionId").pathRegex("/division/(?<divisionId>\\d+)")
        .produces("application/json")
        .handler(DivisionHandler::updateDivision);
    router.get("/paygroup/list/:organizationId").pathRegex("/paygroup/list/(?<organizationId>\\d+)")
        .produces("application/json")
        .handler(PayGroupHandler::getAllPayGroup);
    router.get("/paygroup/:paygroupId").pathRegex("/paygroup/(?<paygroupId>\\d+)")
        .produces("application/json")
        .handler(PayGroupHandler::getPayGroup);
    router.delete("/paygroup/:paygroupId").pathRegex("/paygroup/(?<paygroupId>\\d+)")
        .produces("application/json")
        .handler(PayGroupHandler::deletePayGroup);
    router.route("/paygroup*").consumes("application/x-www-form-urlencoded")
        .produces("application/json")
        .handler(BodyHandler.create());
    router.post("/paygroup/create").produces("application/json")
        .handler(PayGroupHandler::addPayGroup);
    router.put("/paygroup/:paygroupId").pathRegex("/paygroup/(?<paygroupId>\\d+)")
        .produces("application/json")
        .handler(PayGroupHandler::updatePayGroup);
    router.get("/employmentType/list").produces("application/json")
        .handler(EmploymentTypeHandler::getAllEmploymentTypes);
    router.get("/employmentType/:id").pathRegex("/employmentType/(?<id>\\d+)")
        .produces("application/json")
        .handler(EmploymentTypeHandler::getEmploymentType);
    router.delete("/employmentType/:id").pathRegex("/employmentType/(?<id>\\d+)")
        .produces("application/json")
        .handler(EmploymentTypeHandler::deleteEmploymentType);
    router.route("/employmentType*").consumes("application/x-www-form-urlencoded")
        .produces("application/json")
        .handler(BodyHandler.create());
    router.post("/employmentType/create").produces("application/json")
        .handler(EmploymentTypeHandler::addEmploymentType);
    router.put("/employmentType/:id").pathRegex("/employmentType/(?<id>\\d+)")
        .produces("application/json")
        .handler(EmploymentTypeHandler::updateEmploymentType);
  }

  /**
   * this method adds request routes for job tasks
   *
   * @param router
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/MAR/2018
   */
  private static void addJobTaskRoutes(Router router) {
    router.get("/jobTask").handler(JobTaskHandler::getTasksForEmployee);
    router.route("/jobTask*").handler(BodyHandler.create());
    router.post("/jobTask/create").handler(JobTaskHandler::addTaskForJob);
    router.put("/jobTask").handler(JobTaskHandler::completeTasks);
  }

  private static void addEmailTemplateRoutes(Router router) {
    router.get("/email_templates/variables").handler(EmailTemplateHandler::getTemplateVariable);
    router.get("/email_templates/:templateId").pathRegex("/email_templates/(?<templateId>\\d+)")
        .handler(EmailTemplateHandler::getTemplate);
    router.get("/email_templates/all/:organizationId").pathRegex("/email_templates/all/(?<organizationId>\\d+)")
        .handler(EmailTemplateHandler::getTemplates);
    router.get("/email_templates/:templateId/preview").pathRegex("/email_templates/(?<templateId>\\d+)/preview")
        .handler(EmailTemplateHandler::getEmailPreviews);
    router.get("/email_templates/:templateId/send").pathRegex("/email_templates/(?<templateId>\\d+)/send")
        .handler(EmailTemplateHandler::sendEmails);
    router.delete("/email_templates/:templateId").pathRegex("/email_templates/(?<templateId>\\d+)")
        .handler(EmailTemplateHandler::deleteTemplate);

    router.route("/email_templates*").consumes("application/x-www-form-urlencoded").produces("application/json")
        .handler(BodyHandler.create().setDeleteUploadedFilesOnEnd(true));
    router.post("/email_templates").handler(EmailTemplateHandler::addTemplate);
    router.put("/email_templates/:templateId").pathRegex("/email_templates/(?<templateId>\\d+)")
        .handler(EmailTemplateHandler::updateTemplate);
  }

  private static void addNewsfeedRoutes(Router router) {
    router.get("/newsfeed/admin/").handler(NewsFeedHandler::getFeedsForAdmin);
  }

  /**
   * This method adds onboarding routes
   *
   * @param router
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/MAR/2018
   */
  private static void addPacketRoutes(Router router) {
    OnboardingPacketHandler packetHandler = new OnboardingPacketHandler();
    router.route("/onboardingPacket*")
        .handler(BodyHandler.create());
    router.route("/onboardingPacket/welcome*").handler(BodyHandler.create());
    router.route("/onboardingPacket/mapAddress*").handler(BodyHandler.create());

    router.put("/onboardingPacket/welcome/video/:organizationId")
        .pathRegex("/onboardingPacket/welcome/video/(?<organizationId>\\d+)")
        .produces("application/json").handler(OnboardingWelcomeHandler::updateWelcomeScreen);

    router.post("/onboardingPacket/welcome/video")
        .handler(BodyHandler.create().setMergeFormAttributes(true));
    router.post("/onboardingPacket/welcome/video/:organizationId")
        .pathRegex("/onboardingPacket/welcome/video/(?<organizationId>\\d+)")
        .produces("application/json")
        .handler(OnboardingWelcomeHandler::uploadVideo);
    router.get("/onboardingPacket/welcome/map").handler(OnboardingWelcomeHandler::getMapFrame);
    router.put("/onboardingPacket/mapAddress/").handler(OnboardingWelcomeHandler::appMapAddress);

    router.post("/onboardingPacket/add").produces("application/json")
        .handler(OnboardingPacketHandler::addPacket);
    router.post("/onboardingPacket/form").produces("application/json")
        .handler(OnboardingFormHandler::addForm);

    router.post("/onboardingPacket/task/uploadReferralDoc/*")
        .consumes("multipart/form-data").produces("multipart/form-data")
        .handler(BodyHandler.create(UploadConstant.UPLOAD_DIRECTORY).setMergeFormAttributes(true));

    router.post("/onboardingPacket/task/uploadReferralDoc").produces("multipart/form-data")
        .handler(OnboardingPacketHandler::uploadReferralDocForTask);
    router.post("/onboardingPacket/task").handler(OnboardingTaskHandler::addTask);

    router.post("/onboardingPacket/document").produces("application/json")
        .handler(OnboardingDocumentHandler::addDocument);

    router.get("/onboardingPacket").produces("application/json")
        .handler(OnboardingPacketHandler::getOnboardingPackets);

    // send remainder mail for employee done by sadhana on 09/11/2017
    router.post("/onboardingPacket/sendremainder").produces("application/json")
        .handler(OnboardingPacketHandler::SendRemainderMail);

    router.post("/onboardingPacket/document/upload").consumes("multipart/form-data")
        .produces("multipart/form-data")
        .handler(BodyHandler.create().setMergeFormAttributes(true));
    router.post("/onboardingPacket/document/upload")
        .produces("multipart/form-data").handler(OnboardingPacketHandler::uploadDocuments);

    router.delete("/onboardingPacket/:packetId")
        .pathRegex("/onboardingPacket/(?<packetId>\\d+)")
        .produces("application/json")
        .handler(OnboardingPacketHandler::deleteOnboardingPacket);
    router.delete("/onboardingPacket/form/:formId")
        .pathRegex("/onboardingPacket/form/(?<formId>\\d+)")
        .produces("application/json")
        .handler(OnboardingFormHandler::deleteOnboardingForm);
    router.delete("/onboardingPacket/task/:taskId")
        .pathRegex("/onboardingPacket/task/(?<taskId>\\d+)")
        .produces("application/json")
        .handler(OnboardingTaskHandler::deleteOnboardingTask);
    router.delete("/onboardingPacket/document/:documentId")
        .pathRegex("/onboardingPacket/document/(?<documentId>\\d+)")
        .produces("application/json")
        .handler(OnboardingDocumentHandler::deleteOnboardingDocument);

    router.put("/onboardingPacket/send/:employeeId").produces("application/json")
        .handler(OnboardingPacketHandler::sendOnboardingPacket);

    router.put("/onboardingPacket/:packetId")
        .pathRegex("/onboardingPacket/(?<packetId>\\d+)")
        .produces("application/json")
        .handler(OnboardingPacketHandler::updateOnboardingPacket);
    router.put("/onboardingPacket/form/:formId")
        .pathRegex("/onboardingPacket/form/(?<formId>\\d+)")
        .produces("application/json").handler(OnboardingFormHandler::updateOnboardingForm);
    router.put("/onboardingPacket/task/:taskId")
        .pathRegex("/onboardingPacket/task/(?<taskId>\\d+)")
        .produces("application/json").handler(OnboardingTaskHandler::updateOnboardingTask);
    router.put("/onboardingPacket/document/:documentId")
        .pathRegex("/onboardingPacket/document/(?<documentId>\\d+)")
        .produces("application/json")
        .handler(OnboardingDocumentHandler::updateOnboardingDocument);

    router.get("/onboardingPacket/getPacketForEmployee").produces("application/json")
        .handler(packetHandler::getPacketForEmployee);

    router.get("/onboardingPacket/forms/auzmor").produces("application/json")
        .handler(OnboardingFormHandler::getAuzmorForms);
    router.get("/onboardingPacket/documents/auzmor").produces("application/json")
        .handler(packetHandler::getAuzmorDocuments);

    router.get("/onboardingPacket/documents/in-house").handler(packetHandler::getInhouseDocuments);
  }

  private static void addReportRoutes(Router router) {
    router.get("/report/applicantstatus").handler(ReportHandler::getCandidateStatusReports);
    router.get("/report/jobposting").handler(ReportHandler::getJobPostReports);

  }

  private static void addPTOTasks(Router router) {
    router.get("/ptoRequests").produces("application/json")
        .handler(EmployeePTOHandler::allRequests);
    router.get("/ptoRequests/used").produces("application/json")
        .handler(EmployeePTOHandler::allUsedRequests);
    router.get("/ptoAvailableHours").produces("application/json")
        .handler(EmployeePTOHandler::getAvailableHours);
    router.get("/upcomingTimeOff").produces("application/json")
        .handler(EmployeePTOHandler::getUpcomingTimeOff);
    router.get("/ptoRequests/comments").produces("application/json")
        .handler(PTOCommentsHandler::getAllComments);
    router.delete("/ptoComments")
        .produces("application/json")
        .handler(PTOCommentsHandler::deleteComment);
    router.route("/ptoRequests/create*").consumes("application/x-www-form-urlencoded")
        .produces("application/json").handler(BodyHandler.create());
    router.post("/ptoRequests/create").handler(EmployeePTOHandler::addRequest);
    router.route("/ptoComments/create*").consumes("application/x-www-form-urlencoded")
        .produces("application/json").handler(BodyHandler.create());
    router.post("/ptoComments/create").handler(PTOCommentsHandler::addComment);

    router.route("/ptoComments*").consumes("application/x-www-form-urlencoded")
        .produces("application/json").handler(BodyHandler.create());
    router.put("/ptoComments/:id").pathRegex("/ptoComments/(?<id>\\d+)")
        .handler(PTOCommentsHandler::updateComment);

    router.post("/ptoComments/upload").consumes("multipart/form-data")
        .produces("multipart/form-data")
        .handler(BodyHandler.create().setMergeFormAttributes(true));
    router.post("/ptoComments/upload")
        .produces("multipart/form-data").handler(PTOCommentsHandler::uploadDocuments);
  }

  private static void addSessionRoutes(Router router) {
    router.post("/session/logout").pathRegex("/session/logout").handler(CustomSessionHandler::logout);
  }

  private static void addJobDescriptionRoutes(Router router) {
    router.get("/jobdesc").handler(JobDescriptionHandler::getAllJobDescription);
    router.get("/jobdesc/:id").pathRegex("/jobdesc/(?<id>\\d+)")
        .handler(JobDescriptionHandler::getJobDescId);
    router.delete("/jobdesc/:id").pathRegex("/jobdesc/(?<id>\\d+)")
        .handler(JobDescriptionHandler::deleteJobDesc);
    router.route("/jobdesc*").consumes("application/x-www-form-urlencoded")
        .produces("application/json").handler(BodyHandler.create());
    router.post("/jobdesc/create").handler(JobDescriptionHandler::addJobDescription);
    router.put("/jobdesc/:id").pathRegex("/jobdesc/(?<id>\\d+)")
        .handler(JobDescriptionHandler::updateJobDesc);
  }

  private static void addHighestEducationRoutes(Router router) {
    router.get("/highesteducation/list").handler(HighestEducationHandler::getAllHighestEducations);
    router.get("/highesteducation/:id").pathRegex("/highesteducation/(?<id>\\d+)")
        .handler(HighestEducationHandler::getHighestEducation);
    router.delete("/highesteducation/:id").pathRegex("/highesteducation/(?<id>\\d+)")
        .handler(HighestEducationHandler::deleteHighestEducation);
    router.route("/highesteducation*").consumes("application/x-www-form-urlencoded")
        .produces("application/json").handler(BodyHandler.create());
    router.post("/highesteducation/create").handler(HighestEducationHandler::addHighestEducation);
    router.put("/highesteducation/:id").pathRegex("/highesteducation/(?<id>\\d+)")
        .handler(HighestEducationHandler::updateHighestEducation);
  }

  /**
   * this method adds request routes for HRIS tasks
   *
   * @param router
   * @author Kanniga Natarajan
   * @lastModifiedDate 21/APR/2018
   */
  private static void addHRISRoutes(Router router){
    router.route("/invite*").consumes("application/x-www-form-urlencoded")
        .produces("application/json")
        .handler(BodyHandler.create().setDeleteUploadedFilesOnEnd(true));
    router.post("/invite/hris/employee").handler(InviteHandler::inviteNewEmployee);

  }

  private static void addMinimumExperienceRoutes(Router router) {
    router.get("/minimumexperience/list").handler(MinimumExperienceHandler::getAllMinimumExperiences);
    router.get("/minimumexperience/:id").pathRegex("/minimumexperience/(?<id>\\d+)")
        .handler(MinimumExperienceHandler::getMinimumExperience);
    router.delete("/minimumexperience/:id").pathRegex("/minimumexperience/(?<id>\\d+)")
        .handler(MinimumExperienceHandler::deleteMinimumExperience);
    router.route("/minimumexperience*").consumes("application/x-www-form-urlencoded")
        .produces("application/json").handler(BodyHandler.create());
    router.post("/minimumexperience/create").handler(MinimumExperienceHandler::addMinimumExperience);
    router.put("/minimumexperience/:id").pathRegex("/minimumexperience/(?<id>\\d+)")
        .handler(MinimumExperienceHandler::updateMinimumExperience);
  }
}