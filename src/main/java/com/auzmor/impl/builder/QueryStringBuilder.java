package com.auzmor.impl.builder;

import com.auzmor.impl.util.StringUtil;

import java.util.Map;

/**
 * {@link QueryStringBuilder} A class used to build SQL Query based on Query Parameters
 */
public class QueryStringBuilder {


    /**
     * buildSelectQuery - Build a select query based on Query Parameters
     * @param queryParameters
     * @return
     */
    public static String buildSelectQuery(Map<String,String> queryParameters){
        String fieldString = StringUtil.printValidString(queryParameters.get("fields"));
        String tableName = StringUtil.printValidString(queryParameters.get("table"));
        String constraintString = StringUtil.printValidString(queryParameters.get("constraint"));
        String groupParams = StringUtil.printValidString(queryParameters.get("groupParams"));
        String orderParams = StringUtil.printValidString(queryParameters.get("orderParams"));
        String limitParams = StringUtil.printValidString(queryParameters.get("limitParams"));

        String queryString = "select " + fieldString + " from " + tableName + " " + constraintString + " "
                + groupParams + " " + orderParams  + " " + limitParams;
        return queryString;

    }

    /**
     * buildInsertQuery - Build Insertion Query based on Query Parameters
     * @param queryParameters
     * @return
     */
    public static String buildInsertQuery(Map<String,String> queryParameters){
        String tableName = StringUtil.printValidString(queryParameters.get("table"));
        String columnNameString = StringUtil.printValidString(queryParameters.get("column_names"));
        String columnValueString = StringUtil.printValidString(queryParameters.get("column_values"));

        String queryString = "insert into " + tableName + "("+columnNameString+") " +
                                "values("+columnValueString+")";
        return queryString;
    }

    /**
     * buildUpdateQuery - Build Updation Query based on Query Parameters
     * @param queryParameters
     * @return
     */
    public static String buildUpdateQuery(Map<String,String> queryParameters){
        String tableName = StringUtil.printValidString(queryParameters.get("table"));
        String updateStatement = StringUtil.printValidString(queryParameters.get("key_value_pair"));
        String constraintString = StringUtil.printValidString(queryParameters.get("constraint"));

        String queryString = "update " + tableName + " set " + updateStatement + " " + constraintString;

        return queryString;
    }
}
