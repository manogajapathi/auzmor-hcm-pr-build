package com.auzmor.impl.builder;

import com.auzmor.impl.util.ResponseUtil;

import io.vertx.core.http.HttpServerResponse;
import org.apache.commons.httpclient.HttpStatus;

/**
 * {@link ResponseBuilder} A Helper class used to write Response Objects
 */
public class ResponseBuilder {

  /**
   * getSuccessObject - A method used to write Success Response Object
   *
   * @param dataObject
   * @param response
   * @param methodType
   */
  public static void getSuccessObject(Object dataObject, HttpServerResponse response,
                                      String methodType) {

    if ("GET".equals(methodType)) {
      ResponseUtil.writeSuccessObject(HttpStatus.SC_OK, dataObject, response);
    } else if ("POST".equals(methodType)) {
      ResponseUtil.writeSuccessObject(HttpStatus.SC_CREATED, dataObject, response);
    } else if ("PUT".equals(methodType)) {
      ResponseUtil.writeSuccessObject(HttpStatus.SC_ACCEPTED, dataObject, response);
    } else if ("DELETE".equals(methodType)) {
      ResponseUtil.writeSuccessObject(HttpStatus.SC_OK, dataObject, response);
    }

  }

  /**
   * getErrorObject - A method used to write Failure Response Object
   *
   * @param message
   * @param description
   * @param response
   * @param methodType
   */
  public static void getErrorObject(String message, String description, HttpServerResponse response,
                                    String methodType) {

    if ("GET".equals(methodType)) {
      ResponseUtil.writeErrorObject(HttpStatus.SC_BAD_REQUEST, message, description, response);
    } else if ("POST".equals(methodType)) {
      ResponseUtil.writeErrorObject(HttpStatus.SC_NOT_ACCEPTABLE, message, description, response);
    } else if ("PUT".equals(methodType)) {
      ResponseUtil.writeErrorObject(HttpStatus.SC_NOT_ACCEPTABLE, message, description, response);
    } else if ("DELETE".equals(methodType)) {
      ResponseUtil.writeErrorObject(HttpStatus.SC_BAD_REQUEST, message, description, response);
    }

  }

  public static void writeInternalServerError(HttpServerResponse response) {

    ResponseUtil.writeErrorObject(HttpStatus.SC_INTERNAL_SERVER_ERROR, "Internal Server Error",
        "Internal Server Error", response);

  }
}
