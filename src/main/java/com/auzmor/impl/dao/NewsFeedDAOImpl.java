package com.auzmor.impl.dao;

import com.auzmor.dao.NewsFeedDAO;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;

import java.io.IOException;

public class NewsFeedDAOImpl implements NewsFeedDAO {

  @Override
  public JsonArray getAnnouncementDetails(RestClient restClient, int organizationId)
      throws IOException {
    String endPoint = "";
    endPoint = "/announcement/_search?q=organization_id:" + organizationId;
    Response response = restClient.performRequest("GET", endPoint);

    String announcementJsonString = EntityUtils.toString(response.getEntity());

    JsonObject jsonObject = new JsonObject(announcementJsonString);
    JsonArray hitsArray = jsonObject.getJsonObject("hits").getJsonArray("hits");
    JsonArray sourceArray = new JsonArray();

    for (int i = 0; i < hitsArray.size(); i++) {
      JsonObject object = hitsArray.getJsonObject(i).getJsonObject("_source");
      sourceArray.add(object);
    }
    return sourceArray;
  }

  @Override
  public JsonArray getOnboardingDetails(RestClient restClient, int organizationId)
      throws IOException {
    String endPoint = "/onboarding/_search?q=organization_id:" + organizationId;
    Response response = restClient.performRequest("GET", endPoint);
    JsonObject jsonObject = new JsonObject(EntityUtils.toString(response.getEntity()));
    JsonArray hitsArray = jsonObject.getJsonObject("hits").getJsonArray("hits");

    JsonArray sourceArray = new JsonArray();

    for (int i = 0; i < hitsArray.size(); i++) {
      JsonObject object = hitsArray.getJsonObject(i).getJsonObject("_source");
      sourceArray.add(object);
    }
    return sourceArray;
  }

  @Override
  public JsonArray getJobTasksDetails(RestClient restClient, int organizationId) throws IOException {
    String endPoint = "/jobtasks/_search?q=organization_id:" + organizationId;
    Response response = restClient.performRequest("GET", endPoint);
    JsonObject jsonObject = new JsonObject(EntityUtils.toString(response.getEntity()));
    JsonArray hitsArray = jsonObject.getJsonObject("hits").getJsonArray("hits");

    JsonArray sourceArray = new JsonArray();

    for (int i = 0; i < hitsArray.size(); i++) {
      JsonObject object = hitsArray.getJsonObject(i).getJsonObject("_source");
      sourceArray.add(object);
    }
    return sourceArray;
  }
}
