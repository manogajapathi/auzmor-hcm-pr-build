package com.auzmor.impl.dao;

import com.auzmor.dao.OnboardingWelcomeDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.onboarding.OnboardingPacket;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonObject;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OnboardingWelcomeDAOImpl implements OnboardingWelcomeDAO{
  private DbHandle dbHandle;

  public OnboardingWelcomeDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  /**
   * Update Welcome details based on organization_id
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 01/MAR/2018
   */
  @Override
  public int updateWelcomeScreen (MultiMap params) throws SQLException, DbHandleException {
    StringBuilder stringBuilder = new StringBuilder();
    int i = 1;
    int length = params.size();
    List<String> listOfParams = new ArrayList<String>();
    for(String key : params.names()) {
      if(!key.equals("organizationId")) {
        stringBuilder.append(key + " = ?");
        listOfParams.add(params.get(key));
      }


      if (i < length && i != 1) {
        stringBuilder.append(",");
      }
      i++;
    }

    String parameterizedSql = "update OnboardingPacket set " + stringBuilder + " where organization_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    int columnIndex = 1;
    for(int index = 0; index < listOfParams.size(); index++) {
      preparedStatement.setString(columnIndex, listOfParams.get(index));
      columnIndex++;
    }
    preparedStatement.setInt(listOfParams.size() + 1,
        Integer.valueOf(params.get("organizationId")));

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  /**
   * Fetch welcome details for organization
   * @param onboardingPacket
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 01/MAR/2018
   */
  @Override
  public OnboardingPacket getWelcomeDetails(OnboardingPacket onboardingPacket)
      throws SQLException, DbHandleException {
    String parameterizedSql = "select welcome_header,welcome_description,welcome_video," +
        "latitude,longitude,organization_id from OnboardingPacket where organization_id= ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setInt(1, onboardingPacket.getOrganization_id());

    return DbHelper.getObject(dbHandle, preparedStatement , resultSet -> {
      onboardingPacket.setWelcome_header(resultSet.getString("welcome_header"));
      onboardingPacket.setWelcome_description(resultSet.getString("welcome_description"));
      onboardingPacket.setWelcome_video(resultSet.getString("welcome_video"));
      onboardingPacket.setOrganization_id(resultSet.getInt("organization_id"));
      onboardingPacket.setLatitude(resultSet.getDouble("latitude"));
      onboardingPacket.setLongitude(resultSet.getDouble("longitude"));
      return onboardingPacket;
    });
  }

  /**
   * Update lat and long for map address based on organization_id
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 01/MAR/2018
   */
  @Override
  public boolean updateLatLongValues(MultiMap params) throws SQLException, DbHandleException {
    String updateQuery = "update OnboardingPacket set latitude = ? , longitude = ? " +
        "where organization_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(updateQuery);
    preparedStatement.setDouble(1, Double.valueOf(params.get("latitude")));
    preparedStatement.setDouble(2, Double.valueOf(params.get("longitude")));
    preparedStatement.setInt(3, Integer.valueOf(params.get("organization_id")));

    return DbHelper.getBoolean(this.dbHandle, preparedStatement);
  }
}