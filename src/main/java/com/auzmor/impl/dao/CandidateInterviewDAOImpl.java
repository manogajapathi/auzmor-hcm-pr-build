package com.auzmor.impl.dao;

import com.auzmor.dao.CandidateInterviewDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.models.CandidateR.InterviewProcessCandidate;
import com.auzmor.impl.util.DateUtil;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.util.db.DbHandle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;


public class CandidateInterviewDAOImpl implements CandidateInterviewDAO {
  private DbHandle dbHandle;

  private static Logger logger = LoggerFactory.getLogger(CandidateInterviewDAOImpl.class);

  public CandidateInterviewDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  /**
   * @param interviewProcessCandidate
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author SadhanaVenkatesan
   */

  @Override
  public int create(InterviewProcessCandidate interviewProcessCandidate)
      throws SQLException, DbHandleException {

    StringBuilder parameterizedSql = new StringBuilder("INSERT INTO InterviewProcessCandidate");
    parameterizedSql.append("(interview_date, interview_time,interview_duration,interview_type,");
    parameterizedSql.append("type,interview_details,interview_location,subject_name,candidate_id,");
    parameterizedSql.append("admin_name,interview_mail_description,user_id,collaborator_id,interview_end_time,sub_status )");
    parameterizedSql.append("VALUES (?, ?, ?,?,?,?,?,?,?,?,?,?,?,?,? )");

    int index = 0;

    java.sql.Date sqlDate = new java.sql.Date(interviewProcessCandidate.getInterviewDate().getTime());
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql
        .toString(), true);
    preparedStatement.setDate(++index, sqlDate);
    preparedStatement.setString(++index, interviewProcessCandidate.getInterviewTime());
    preparedStatement.setString(++index, interviewProcessCandidate.getInterviewDuration());
    preparedStatement.setString(++index, interviewProcessCandidate.getInterviewType());
    preparedStatement.setString(++index, interviewProcessCandidate.getType());
    preparedStatement.setString(++index, interviewProcessCandidate.getInterviewDetails());
    preparedStatement.setString(++index, interviewProcessCandidate.getInterviewLocation());
    preparedStatement.setString(++index, interviewProcessCandidate.getSubjectName());
    preparedStatement.setLong(++index, interviewProcessCandidate.getCandidateId());
    preparedStatement.setString(++index, interviewProcessCandidate.getAdminName());
    preparedStatement.setString(++index, interviewProcessCandidate.getInterviewMailDescription());
    preparedStatement.setLong(++index, interviewProcessCandidate.getUserId());
    preparedStatement.setLong(++index, interviewProcessCandidate.getCollaboratorId());
    preparedStatement.setString(++index, interviewProcessCandidate.getInterviewEndTime());
    preparedStatement.setString(++index, interviewProcessCandidate.getSubStatus());

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, true);
  }

  @Override
  public int updateActiveStatus(long candidateId) throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder("update InterviewProcessCandidate set active_status = 0 ");
    query.append("where candidate_id = ? and active_status= 1");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, candidateId);
    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  @Override
  public InterviewProcessCandidate getInterviewProcessDetails(long candidateId)
      throws SQLException, DbHandleException {

    String query = "select interview_date,interview_time,interview_duration,interview_type," +
        " interview_details,interview_location,subject_name,admin_name," +
        " interview_mail_description,interview_cancel_reason,status,created,modified,user_id," +
        " interview_end_time,collaborator_id,is_completed,id " +
        " from InterviewProcessCandidate where candidate_id = ? " +
        " order by id desc limit 1";

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query);
    preparedStatement.setLong(++index, candidateId);
    return DbHelper.getObject(this.dbHandle, preparedStatement, resultSet -> {
      InterviewProcessCandidate interviewProcessCandidate = new InterviewProcessCandidate();

      interviewProcessCandidate.setInterviewDate(resultSet.getDate(1));
      interviewProcessCandidate.setInterviewTime(resultSet.getString(2));
      interviewProcessCandidate.setInterviewDuration(resultSet.getString(3));
      interviewProcessCandidate.setInterviewType(resultSet.getString(4));
      interviewProcessCandidate.setInterviewDetails(resultSet.getString(5));
      interviewProcessCandidate.setInterviewLocation(resultSet.getString(6));
      interviewProcessCandidate.setSubjectName(resultSet.getString(7));
      interviewProcessCandidate.setAdminName(resultSet.getString(8));
      interviewProcessCandidate.setInterviewMailDescription(resultSet.getString(9));
      interviewProcessCandidate.setInterviewCancelReason(resultSet.getString(10));
      interviewProcessCandidate.setStatus(resultSet.getString(11));
      interviewProcessCandidate.setCreated(resultSet.getDate(12));
      interviewProcessCandidate.setModified(resultSet.getDate(13));
      interviewProcessCandidate.setUserId(resultSet.getLong(14));
      interviewProcessCandidate.setInterviewEndTime(resultSet.getString(15));
      interviewProcessCandidate.setCollaboratorId(resultSet.getInt(16));
      interviewProcessCandidate.setCompleted(resultSet.getBoolean(17));
      interviewProcessCandidate.setId((long) resultSet.getInt(18));
      logger.info("Interview Process Details fetched Successfully");
      return interviewProcessCandidate;
    });

  }

  @Override
  public int updateCancelStatus(String cancelReason, String status, long candidateId, long userId)
      throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder(" update InterviewProcessCandidate set ");
    query.append(" interview_cancel_reason = ?,status = ? ,user_id = ? ");
   /* if(status.equalsIgnoreCase("Admin Rejected")){
      query.append(" ,is_completed = 1 ");
    }*/
    query.append(" where candidate_id = ? and active_status= 1 ");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setString(++index, cancelReason);
    preparedStatement.setString(++index, status);
    preparedStatement.setLong(++index, userId);
    preparedStatement.setLong(++index, candidateId);
    logger.info("Cancel Status updated Successfully");
    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  @Override
  public int updateStatus(String status, long candidateId) throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder(" update InterviewProcessCandidate set status = ?");
    query.append(" where candidate_id = ? and active_status= 1 ");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setString(++index, status);
    preparedStatement.setLong(++index, candidateId);
    logger.info("Updated Status Successfully");
    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  @Override
  public int updateInterviewDetails(InterviewProcessCandidate interviewProcessCandidate)
      throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder();
    if (!StringUtil.printValidString(interviewProcessCandidate.getInterviewDetails()).equals("") &&
        interviewProcessCandidate.getInterviewType().equalsIgnoreCase("Web")) {

      query.append("update InterviewProcessCandidate set interview_date= ?,interview_time=?,");
      query.append(" interview_duration=?,interview_type=?,interview_details=?,interview_mail_description = ?,");
      query.append(" type=?,subject_name=?,admin_name=?,user_id = ?,interview_end_time=?,collaborator_id=?,status=? ");
      query.append(" where candidate_id = ? and active_status= 1 and sub_status = ? ");
    } else {
      query.append("update InterviewProcessCandidate set interview_date= ?,interview_time=?,");
      query.append(" interview_duration=?,interview_type=?,interview_location=?,interview_mail_description = ?,");
      query.append(" type=?,subject_name=?,admin_name=?,user_id = ?,interview_end_time=?,collaborator_id=?,status=? ");
      query.append(" where candidate_id = ? and active_status= 1 and sub_status = ?");
    }

    int index = 0;
    java.sql.Date sqlDate = new java.sql.Date(interviewProcessCandidate.getInterviewDate().getTime());

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());

    preparedStatement.setDate(++index, sqlDate);
    preparedStatement.setString(++index, interviewProcessCandidate.getInterviewTime());
    preparedStatement.setString(++index, interviewProcessCandidate.getInterviewDuration());
    preparedStatement.setString(++index, interviewProcessCandidate.getInterviewType());

    if (!StringUtil.printValidString(interviewProcessCandidate.getInterviewDetails()).equals("") &&
        interviewProcessCandidate.getInterviewType().equalsIgnoreCase("Web")) {
      preparedStatement.setString(++index, interviewProcessCandidate.getInterviewDetails());
    } else {
      preparedStatement.setString(++index, interviewProcessCandidate.getInterviewLocation());
    }
    preparedStatement.setString(++index, interviewProcessCandidate.getInterviewMailDescription());
    preparedStatement.setString(++index, interviewProcessCandidate.getType());
    preparedStatement.setString(++index, interviewProcessCandidate.getSubjectName());
    preparedStatement.setString(++index, interviewProcessCandidate.getAdminName());
    preparedStatement.setLong(++index, interviewProcessCandidate.getUserId());
    preparedStatement.setString(++index, interviewProcessCandidate.getInterviewEndTime());
    preparedStatement.setLong(++index, interviewProcessCandidate.getCollaboratorId());
    preparedStatement.setString(++index, StringUtil.printValidString(interviewProcessCandidate.getStatus()));
    preparedStatement.setLong(++index, interviewProcessCandidate.getCandidateId());
    preparedStatement.setString(++index, interviewProcessCandidate.getSubStatus());
    logger.info("Interview Details updated Successfully");

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  @Override
  public List<HashMap<String, String>> getListOfInterviewProcess(String dateFormat,
                                                                 StringBuilder fieldsName,
                                                                 long organizationId,
                                                                 int limit, int offset,
                                                                 List<String> fieldValue, long collaboratorId)
      throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder();
    query.append("select " + fieldsName.toString() + " ");
    query.append(" InterviewProcessCandidate.candidate_id as candidate_id,Candidate.job_id, ");
    query.append(" Any_Value(InterviewProcessCandidate.sub_status) as sub_status, ");
    query.append(" Any_Value(InterviewProcessCandidate.collaborator_id) as collaborator_id ");
    query.append(" FROM InterviewProcessCandidate,Candidate,Job ");
    query.append(" where InterviewProcessCandidate.candidate_id = Candidate.candidate_id ");
    query.append(" and InterviewProcessCandidate.interview_date != '' and Job.id = Candidate.job_id and ");
    query.append(" Candidate.status ='Interview' and  Candidate.active_status = 1 and  ");
    //  query.append(" HiringprocessCandidate.candidate_id = Candidate.candidate_id ");
    //  query.append(" and HiringprocessCandidate.status = Candidate.status ");
    query.append(" InterviewProcessCandidate.is_completed = 0 ");
    query.append(" and ((InterviewProcessCandidate.status = 'accept') or (InterviewProcessCandidate.status IS NULL) or ");
    query.append(" (InterviewProcessCandidate.status = '')) ");
    query.append(" and InterviewProcessCandidate.interview_cancel_reason is null ");
    query.append(" and InterviewProcessCandidate.active_status = 1 and Candidate.organization_id = ? ");
    query.append(" and str_to_date(concat(interview_date,' ',interview_time), '%Y-%m-%d %H:%i')  > current_timestamp() ");

    if (collaboratorId > 0 && (limit > 0 || offset > 0)) {
      query.append(" and InterviewProcessCandidate.collaborator_id = ? group by candidate_id ");
      query.append(" order by interview_date,interview_time desc limit ? offset ? ");
    } else if ((limit == 0 || offset == 0) && collaboratorId > 0) {
      query.append(" and InterviewProcessCandidate.collaborator_id = ? group by candidate_id ");
      query.append(" order by interview_date,interview_time");
    } else if (limit > 0 || offset > 0) {
      query.append(" group by candidate_id ");
      query.append(" order by interview_date,interview_time desc limit ? offset ? ");

    } else {
      query.append(" group by candidate_id ");
      query.append(" order by interview_date,interview_time desc ");
    }
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());

    preparedStatement.setLong(++index, organizationId);
    if (collaboratorId > 0 && (limit > 0 || offset > 0)) {
      preparedStatement.setLong(++index, collaboratorId);
      preparedStatement.setInt(++index, limit);
      preparedStatement.setInt(++index, offset);
    } else if ((limit == 0 || offset == 0) && collaboratorId > 0) {
      preparedStatement.setLong(++index, collaboratorId);
    } else if (limit > 0 || offset > 0) {
      preparedStatement.setInt(++index, limit);
      preparedStatement.setInt(++index, offset);
    }

    return DbHelper.getObjectList(this.dbHandle, preparedStatement, resultSet -> {
      HashMap<String, String> interviewDetailsList = new HashMap<>();

      for (int i = 0; i < fieldValue.size(); i++) {

        if (fieldValue.get(i).toString().equals("interview_date")) {
          String date = new DateUtil().getDate().format(resultSet.getDate("interview_date"));
          try {
            interviewDetailsList.put("interview_date", new DateUtil().getDateFormat(dateFormat, date));
          } catch (ParseException e) {
            e.printStackTrace();
          }
        } else if (fieldValue.get(i).toString().equals("interview_time")) {
          interviewDetailsList.put("interview_time", resultSet.getString("interview_time"));
        } else if (fieldValue.get(i).toString().equals("interview_end_time")) {
          interviewDetailsList.put("interview_end_time", resultSet.getString("interview_end_time"));
        } else if (fieldValue.get(i).toString().equals("interview_duration")) {
          interviewDetailsList.put("interview_duration", resultSet.getString("interview_duration"));
        } else if (fieldValue.get(i).toString().equals("interview_location")) {
          interviewDetailsList.put("interview_location", resultSet.getString("interview_location"));
        } else if (fieldValue.get(i).toString().equals("first_name")) {
          interviewDetailsList.put("first_name", resultSet.getString("first_name"));
        } else if (fieldValue.get(i).toString().equals("last_name")) {
          interviewDetailsList.put("last_name", resultSet.getString("last_name"));
        } else if (fieldValue.get(i).toString().equals("job_title")) {
          interviewDetailsList.put("job_title", resultSet.getString("job_title"));
        } else if (fieldValue.get(i).toString().equals("id")) {
          interviewDetailsList.put("interview_id", resultSet.getString("id"));
        }
        interviewDetailsList.put("candidate_id", resultSet.getString("candidate_id"));
        interviewDetailsList.put("collaborator_id", resultSet.getString("collaborator_id"));
        interviewDetailsList.put("job_id", resultSet.getString("job_id"));
        interviewDetailsList.put("sub_status", resultSet.getString("sub_status"));
      }
      logger.info("Interview Details fetched Successfully");
      return interviewDetailsList;
    });
  }

  @Override
  public List<HashMap<String, String>> getInterviewMailList(long candidateId)
      throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder("select subject_name,admin_name,user_id,");
    query.append("interview_mail_description,created,modified ");
    query.append(" from InterviewProcessCandidate");
    query.append(" where candidate_id= ?  and interview_mail_description != '' ");
    query.append("  and active_status =1  order by created desc");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.
        createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, candidateId);
    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      HashMap<String, String> getMailList = new HashMap<>();
      int retrieveIndex = 0;
      getMailList.put("subject_name", resultSet.getString(++retrieveIndex));
      getMailList.put("admin_name", resultSet.getString(++retrieveIndex));
      getMailList.put("user_id", resultSet.getString(++retrieveIndex));
      getMailList.put("mail_description", resultSet.getString(++retrieveIndex));

      String date = resultSet.getString(++retrieveIndex);
      String dateFormat = date.substring(0, 10);
      String timeFormat = date.substring(11, 16);
      String createdDate = dateFormat + " at " + timeFormat;

      getMailList.put("created_date", createdDate);
      logger.info("Interview Mail List fetched Successfully");
      return getMailList;
    });
  }

  @Override
  public int updateCompletedStatus(long candidateId) throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder(" update InterviewProcessCandidate set is_completed = 1 ");
    query.append(" where candidate_id = ? and active_status= 1 ");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, candidateId);
    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  @Override
  public int updateCompletedStatus(long candidateId, String subStatus) throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder(" update InterviewProcessCandidate set is_completed = 1 ");
    query.append(" where candidate_id = ? and active_status= 1 and sub_status = ? ");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, candidateId);
    preparedStatement.setString(++index, subStatus);
    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  @Override
  public boolean checkStatus(String subStatus) throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder("Select is_completed from InterviewProcessCandidate");
    query.append(" where sub_status = ? ");
    query.append(" and active_status = 1 order by id desc limit 1");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.
        toString());
    preparedStatement.setString(++index, subStatus);
    logger.info("Status Check fetched Successfully!");
    return DbHelper.getBoolean(this.dbHandle, preparedStatement);
  }

}
