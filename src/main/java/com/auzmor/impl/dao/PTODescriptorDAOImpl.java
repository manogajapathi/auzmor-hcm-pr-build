package com.auzmor.impl.dao;

import com.auzmor.dao.PTODescriptorDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.PTO.PTODescriptor;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.json.JsonObject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class PTODescriptorDAOImpl implements PTODescriptorDAO {
  private DbHandle dbHandle;

  public PTODescriptorDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  /**
   * Fetch maximum available hours based on employee_id and time_off_type
   * @param ptoDescriptor
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  @Override
  public JsonObject getPTOMaxHours(PTODescriptor ptoDescriptor) throws SQLException, DbHandleException {
    JsonObject dataObject = new JsonObject();
    String sqlQuery = "select id,max_hours from PTO_Descriptor where employee_id = ?" +
        " and time_off_type = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(sqlQuery);
    preparedStatement.setLong(1, ptoDescriptor.getEmployeeId());
    preparedStatement.setString(2, ptoDescriptor.getTimeOffType());

    ResultSet resultSet = preparedStatement.executeQuery();
    while (resultSet.next()) {
      dataObject.put("id", resultSet.getLong("id"));
      dataObject.put("max_hours", resultSet.getDouble("max_hours"));
    }
    if (resultSet!=null && !resultSet.isClosed()) {
      resultSet.close();
    }
    if (preparedStatement!=null && !preparedStatement.isClosed()) {
      preparedStatement.close();
    }
    
    return dataObject;
  }

  /**
   * Fetch maximum hours based on employee_id
   * @param ptoDescriptor
   * @return
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public List<JsonObject> getAllHours(PTODescriptor ptoDescriptor) throws SQLException, DbHandleException {
    String parameterizedSql = "select id,max_hours,time_off_type from PTO_Descriptor " +
        "where employee_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setLong(1, ptoDescriptor.getEmployeeId());

    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      JsonObject jsonObject = new JsonObject();
      jsonObject.put("id", resultSet.getLong("id"));
      jsonObject.put("max_hours", resultSet.getDouble("max_hours"));
      jsonObject.put("time_off_type", resultSet.getString("time_off_type"));
      return jsonObject;
    });
  }
}
