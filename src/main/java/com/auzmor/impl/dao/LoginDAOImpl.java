package com.auzmor.impl.dao;

import com.auzmor.dao.LoginDAO;
import com.auzmor.impl.enumarator.bo.LoginBOEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.Login;
import com.auzmor.impl.util.Mail;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class LoginDAOImpl implements LoginDAO {
  private static final Logger logger = LoggerFactory.getLogger(LoginDAOImpl.class);
  private DbHandle dbHandle;

  public LoginDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  @Override
  public int create(Login login) throws SQLException, DbHandleException {
    logger.info("Create a login in db");
    StringBuilder parameterizedSql = new StringBuilder("INSERT INTO `Login` (`email`, `password`, ");
    parameterizedSql.append("`first_name`, `last_name`, `user_type`, ");
    parameterizedSql.append("`organization_id`, ");
    if (null != login.getKeyValue()) {
      parameterizedSql.append("`key_value`, ");
    }
    if (null != login.getHashValue()) {
      parameterizedSql.append("`hash_value`, ");
    }
    parameterizedSql.append("`created_date`) VALUES(?, SHA1(?), ?, ?, ?, ?, ");
    if (null != login.getKeyValue()) {
      parameterizedSql.append("?, ");
    }
    if (null != login.getHashValue()) {
      parameterizedSql.append("?, ");
    }
    parameterizedSql.append("? )");

    DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql
        .toString(), true);
    preparedStatement.setString(++index, login.getEmail());
    preparedStatement.setString(++index, login.getPassword());
    preparedStatement.setString(++index, login.getFirstName());
    preparedStatement.setString(++index, login.getLastName());
    preparedStatement.setString(++index, login.getUserType());
    preparedStatement.setLong(++index, login.getOrganizationId());

    if (null != login.getKeyValue()) {
      preparedStatement.setString(++index, login.getKeyValue());
    }
    if (null != login.getHashValue()) {
      preparedStatement.setString(++index, login.getHashValue());
    }

    preparedStatement.setString(++index, dateFormat.format(Calendar.getInstance().getTime()));


    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, true);
  }

  @Override
  public int createWithToken(Login login) throws SQLException, DbHandleException {
    logger.info("Create a login with token in db");
    StringBuilder parameterizedSql = new StringBuilder("INSERT INTO `Login` (`email`, `password`, ");
    parameterizedSql.append("`first_name`, `last_name`, `user_type`, ");
    parameterizedSql.append("`organization_id`, `created_date`,`key_value`, `hash_value` ) VALUES(?, SHA1(?), ?, ?, ?, ?, ?, ?, ? )");

    DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql
        .toString(), true);
    preparedStatement.setString(++index, login.getEmail());
    preparedStatement.setString(++index, login.getPassword());
    preparedStatement.setString(++index, login.getFirstName());
    preparedStatement.setString(++index, login.getLastName());
    preparedStatement.setString(++index, login.getUserType());
    preparedStatement.setLong(++index, login.getOrganizationId());
    preparedStatement.setString(++index, dateFormat.format(Calendar.getInstance().getTime()));
    preparedStatement.setString(++index, login.getKeyValue());
    preparedStatement.setString(++index, login.getHashValue());

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, true);
  }

  @Override
  public void updateConsumerId(String consumerId, long loginId) throws SQLException, DbHandleException {
    logger.info("Update consumer id to login  in db");
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(
        "UPDATE `Login` SET `consumer_id` = ? WHERE id = ?");
    int index = 0;
    preparedStatement.setString(++index, consumerId);
    preparedStatement.setLong(++index, loginId);

    DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  @Override
  public String getConsumerId(int userId) throws SQLException, DbHandleException {

    logger.info("Get Consumer id from db");
    String consumerId = "";
    String queryString = "SELECT consumer_id from Login where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(queryString);
    preparedStatement.setLong(1, userId);

    ResultSet resultSet = preparedStatement.executeQuery();
    while (resultSet.next()) {
      consumerId = resultSet.getString("consumer_id");
    }
    return consumerId;
  }

  // TODO convert int id to long
  @Override
  @Deprecated
  public boolean isLoginAlreadyExists(String email, int organizationId) throws SQLException,
      DbHandleException {
    logger.info("Check login already exists");
    String parameterizedSql = "SELECT 1 FROM `Login` WHERE `organization_id` = ? AND `email` = ?";

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setString(++index, email);
    // TODO convert int id to long
    preparedStatement.setInt(++index, organizationId);

    return DbHelper.getBoolean(this.dbHandle, preparedStatement);
  }

  // All the id's will be updated soon
  @Override
  public boolean isLoginAlreadyExists(String email, Long organizationId) throws SQLException,
      DbHandleException {
    logger.info("Check login already exists");
    String parameterizedSql = "SELECT 1 FROM `Login` WHERE `organization_id` = ? AND `email` = ?";

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setLong(++index, organizationId);
    preparedStatement.setString(++index, email);

    return DbHelper.getBoolean(this.dbHandle, preparedStatement);
  }

  @Override
  public Login isValidCredentialAndGetLogin(String email, String password, Long organizationId)
      throws SQLException, DbHandleException {
    StringBuilder parameterizedSql = new StringBuilder("SELECT `id`, `first_name`, `last_name`, ");
    parameterizedSql.append("`email`, `consumer_id`, `user_type`  FROM `Login` ");
    parameterizedSql.append("WHERE `organization_id` = ? AND `email` = ? AND `password` = SHA1(?) order by id DESC ");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql
        .toString());
    preparedStatement.setLong(++index, organizationId);
    preparedStatement.setString(++index, email);
    preparedStatement.setString(++index, password);
    return DbHelper.getObject(this.dbHandle, preparedStatement, resultSet -> {
      int retrieveIndex = 0;
      Login login = new Login();

      login.setId(resultSet.getInt(++retrieveIndex));
      login.setFirstName(resultSet.getString(++retrieveIndex));
      login.setLastName(resultSet.getString(++retrieveIndex));
      login.setEmail(resultSet.getString(++retrieveIndex));
      login.setConsumerId(resultSet.getString(++retrieveIndex));
      login.setUserType(resultSet.getString(++retrieveIndex));
      return login;
    });
  }

  // All the id's will be updated soon
  @Override
  public Login getLoginDetails(long loginId) throws SQLException, DbHandleException {

    logger.info("Get Login details from db");
    StringBuilder query = new StringBuilder("Select first_name,last_name,organization_id");
    query.append(" from Login where id = ? and active_status =1 ");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    // TODO conver int id to long
    preparedStatement.setLong(++index, loginId);

    return DbHelper.getObject(this.dbHandle, preparedStatement, resultSet -> {
      int retrieveIndex = 0;
      Login login = new Login();
      login.setFirstName(resultSet.getString(++retrieveIndex));
      login.setLastName(resultSet.getString(++retrieveIndex));
      login.setOrganizationId(resultSet.getInt(++retrieveIndex));
      return login;
    });

  }

  @Override

  public boolean isOldPasswordCorrect(String oldPassword, int loginId) throws SQLException, DbHandleException {

    logger.info("Check old password from db");
    String parameterizedSql = "SELECT 1 FROM `Login` WHERE `id` = ? AND `password` = SHA1(?)";
    int index = 0;

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setInt(++index, loginId);
    // TODO conver int id to long
    preparedStatement.setString(++index, oldPassword);

    return DbHelper.getBoolean(this.dbHandle, preparedStatement);

  }


  @Override
  public int updatePassword(String changePassword, int loginId) throws SQLException, DbHandleException {

    logger.info("updating the password for the login id=" + loginId);
    String parameterizedSql = "Update Login set password = sha1(?) where id = ?";

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    // TODO conver int id to long
    preparedStatement.setString(++index, changePassword);
    preparedStatement.setInt(++index, loginId);

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  public LoginBOEnum approveEmployeeSignup(MultiMap params) throws SQLException, DbHandleException {

    logger.info("Approve employee signup in db");
    int userId = Integer.valueOf(params.get("user_id"));
    String packetId = params.get("onboarding_packet");
    java.util.Date currentDate = new java.util.Date();
    SimpleDateFormat currentDateFormatter = new SimpleDateFormat("MM/dd/yyyy");
    String todayDate = currentDateFormatter.format(currentDate);
    String queryString = "";
    String updateStringSuccess = "";
    String successString1 = " ";
    String toEmailAddress = "";
    String firstName = "";
    String companyUrl = " ";
    String organizationName = " ";
    String onboardingStatus = " ";
    String userType = " ";
    Mail mail = new Mail();
    if (!(params.isEmpty())) {
      logger.info("Params is empty");
      queryString = "SELECT Login.email,Login.first_name,Employee.employee_id," +
          "Employee.organization_id,Organization.hris_url,Organization.organization_name FROM " +
          "Login INNER JOIN Employee ON Login.id = Employee.user_id INNER JOIN " +
          "Organization ON Organization.Organization_id = Employee.Organization_id where " +
          "Login.id = ?";
      PreparedStatement preStatement = this.dbHandle.createPreparedStatement(queryString);
      preStatement.setInt(1, userId);
      ResultSet resultSetObject = preStatement.executeQuery();
      while (resultSetObject.next()) {
        toEmailAddress = resultSetObject.getString("email");
        firstName = resultSetObject.getString("first_name");
        companyUrl = resultSetObject.getString("hris_url");
        organizationName = resultSetObject.getString("organization_name");
      }
      if ((("true").equals(params.get("approve_status"))) &&
          ("true").equals(params.get("approve_onboardingstatus"))) {
        logger.info("Approved status and onboarding status");
        updateStringSuccess = "UPDATE Login,Employee SET Login.user_type = 'employee',Login." +
            "active_status = 1,Employee.status= 'Active', Employee.onboarding_status = " +
            "'Packet Sent' ,Employee.onboarding_packet_date = ?,Employee.active_status =  1 ," +
            "Employee.onboarding_packet = ?,Employee.access_level ='employee' where Login.id = ? and  " +
            "Employee.user_id = ? ";
        PreparedStatement preStatement1 = this.dbHandle.createPreparedStatement(updateStringSuccess);
        preStatement1.setString(1, todayDate);
        preStatement1.setString(2, packetId);
        preStatement1.setInt(3, userId);
        preStatement1.setInt(4, userId);
        preStatement1.executeUpdate();
        dbHandle.commit();
        // Create and build a new MailMessage object
        String successMailBody = "<html><body>Hello <b>" + firstName + "</b>, <br> " +
            "<p>We are glad to inform you that your access request for " + organizationName + " " +
            "is approved.  We are excited to have you join our team.<br>" +
            "We have already sent your onboarding packets. Please begin the onboarding process.." +
            "Please reach out to an administrator if you have any questions or issues related to " +
            "the onboarding process. " +
            " <br><a href= 'https://" + companyUrl + "'>please click the link to login</a> <br>" +
            "<br>" +
            "Thank you,\n<br/>" +
            organizationName + " Team\n<br>" +
            "<a href=\"mailto:support@auzmor.com\">support@auzmor.com</a>\n<br>" +
            "--------------------------------------------------------------\n<br/>" +
            "This is system generated mail,please do not reply to this mail\n<br/>" +
            "-------------------------------------------------------------- " +
            "</body></html>";

        String subject = "Access Request Accepted";
        mail.sendMail(Vertx.vertx(), successMailBody, toEmailAddress, subject);
      } else if (("true").equals(params.get("approve_status")) &&
          ("false").equals(params.get("approve_onboardingstatus"))) {
        logger.info("Approved and onboarding status pending");
        String updateStringSuccess1 = "UPDATE Login,Employee SET Login.user_type = 'employee'," +
            "Login.active_status = 1,Employee.status= 'Active', Employee.onboarding_status = " +
            "'Packet Not Sent',Employee.active_status = 1,Employee.access_level ='employee'" +
            " where Login.id = ? and Employee.user_id = ?";
        PreparedStatement preStatement1 = this.dbHandle.createPreparedStatement(updateStringSuccess1);
        preStatement1.setInt(1, userId);
        preStatement1.setInt(2, userId);
        preStatement1.executeUpdate();
        dbHandle.commit();
        String successMailBody1 = "<html><body>Hello <b>" + firstName + "</b>, <br> " +
            "<p>We are glad to inform you that your access request for " + organizationName
            + " is approved. Click here to login....." +
            " <br><a href= 'https://" + companyUrl + "'>please click the link to login</a> <br>" +
            "<br>" +
            "Thank You!\n<br/>" +
            organizationName + "\n<br>" +
            "<a href=\"mailto:support@auzmor.com\">support@auzmor.com</a>\n<br>" +
            "--------------------------------------------------------------\n<br/>" +
            "This is system generated mail,please do not reply to this mail\n<br/>" +
            "-------------------------------------------------------------- " +
            "</body></html>";

        String subject = "Access Request Accepted";
        mail.sendMail(Vertx.vertx(), successMailBody1, toEmailAddress, subject);

      } else {
        updateStringSuccess = "UPDATE Login,Employee SET Login.user_type = 'reject',Login." +
            "active_status = 0,Employee.status= 'Rejected',Employee.active_status = 0 where " +
            "Login.id = ? and Employee.user_id = ?";
        PreparedStatement preStatement2 = this.dbHandle.createPreparedStatement(updateStringSuccess);
        preStatement2.setInt(1, userId);
        preStatement2.setInt(2, userId);
        int count = DbHelper.executeUpdate(this.dbHandle, preStatement2, false);
        dbHandle.commit();
        String failureMailBody = "<html><body>Hello <b>" + firstName + "</b>, <br> " +
            "<p>We are regret to inform you that your access request for " + organizationName +
            " " + " has been rejected by administrator. Kinldy contact administrator" +
            " for further queries<br>" +

            "Thank You!\n<br/>" +
            organizationName + "\n<br>" +
            "<a href=\"mailto:support@auzmor.com\">support@auzmor.com</a>\n<br>" +
            "--------------------------------------------------------------\n<br/>" +
            "This is system generated mail,please do not reply to this mail\n<br/>" +
            "-------------------------------------------------------------- " +
            "</body></html>";
        String subject = "Access Request Rejected";
        mail.sendMail(Vertx.vertx(), failureMailBody, toEmailAddress, subject);

      }
    }
    successString1 = "SELECT Login.user_type,Employee.onboarding_status FROM Login INNER JOIN " +
        "Employee ON Login.id = Employee.user_id where Login.id= ? ";
    PreparedStatement preStatement3 = this.dbHandle.createPreparedStatement(successString1);
    preStatement3.setInt(1, userId);
    ResultSet resultSetObject1 = preStatement3.executeQuery();

    while (resultSetObject1.next()) {
      onboardingStatus = resultSetObject1.getString("onboarding_status");
      userType = resultSetObject1.getString("user_type");
    }
    if (("Packet Sent").equals(onboardingStatus) && (("employee").equals(userType)))
      return LoginBOEnum.EMPLOYEE_APPROVED_PACKETSENT;
    else if (("Packet Not Sent").equals(onboardingStatus) && (("employee").equals(userType)))
      return LoginBOEnum.EMPLOYEE_APPROVED;
    else if (("reject").equals(userType))
      return LoginBOEnum.EMPLOYEE_REJECTED;
    else
      return LoginBOEnum.INVALID_DATA;

  }

  @Override
  public Login getLoginDetails(String email, int organizationId) throws SQLException, DbHandleException {
    logger.info("Get Login Details");
    String selectQuery = "select id,first_name,last_name,organization_id,user_type from Login " +
        "where email = ? and organization_id = ? and active_status = 1";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setString(1, email);
    preparedStatement.setInt(2, organizationId);

    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      Login login = new Login();
      login.setId(resultSet.getLong(1));
      login.setFirstName(resultSet.getString(2));
      login.setLastName(resultSet.getString(3));
      login.setOrganizationId(resultSet.getInt(4));
      login.setUserType(resultSet.getString(5));
      return login;
    });
  }

  @Override
  public int updateKeyValue(Login login) throws SQLException, DbHandleException {
    logger.info("Update Key Value for Login");
    String updateQuery = "update Login set key_value = ?, hash_value = ? where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(updateQuery);
    preparedStatement.setString(1, login.getKeyValue());
    preparedStatement.setString(2, login.getHashValue());
    preparedStatement.setLong(3, login.getId());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  @Override
  public Login getKeyValues(Login login) throws SQLException, DbHandleException {
    logger.info("Update Key Values for Login");

    String selectQuery = "select key_value,hash_value,user_type from Login where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setLong(1, login.getId());

    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      Login loginObject = new Login();
      loginObject.setKeyValue(resultSet.getString(1));
      loginObject.setHashValue(resultSet.getString(2));
      loginObject.setUserType(resultSet.getString(3));
      return loginObject;
    });
  }

  @Override
  public int updatePasswordWithKeyValue(Login login) throws SQLException, DbHandleException {
    logger.info("Update password Key Value for Login");

    String updateQuery = "update Login set password = sha1(?) where id = ? and key_value = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(updateQuery);
    preparedStatement.setString(1, login.getPassword());
    preparedStatement.setLong(2, login.getId());
    preparedStatement.setString(3, login.getKeyValue());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  @Override
  public boolean candidateLoginCheck(Login login) throws SQLException, DbHandleException {
    logger.info("checking email id is" + login.getEmail());
    String parameterizedSql = "SELECT 1 FROM `Login` WHERE `organization_id` = ? AND `email` = ? ";

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setLong(++index, login.getOrganizationId());
    preparedStatement.setString(++index, login.getEmail());
    return DbHelper.getBoolean(this.dbHandle, preparedStatement);
  }

  @Override
  public long getLoginIdWithKeyValue(Login login) throws SQLException, DbHandleException {
    logger.info("Get login with key value");
    String selectQuery = "select id from Login where key_value = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setString(1, login.getKeyValue());

    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      long loginId = 0;
      loginId = resultSet.getLong(1);
      return loginId;
    });
  }

  @Override
  public Login getLoginUserDetails(long loginId) throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder("Select first_name,last_name,organization_id,user_type ");
    query.append(",consumer_id,email from Login where id = ? and active_status =1 ");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    // TODO conver int id to long
    preparedStatement.setLong(++index, loginId);

    return DbHelper.getObject(this.dbHandle, preparedStatement, resultSet -> {
      int retrieveIndex = 0;
      Login login = new Login();
      login.setId(loginId);
      login.setFirstName(resultSet.getString(++retrieveIndex));
      login.setLastName(resultSet.getString(++retrieveIndex));
      login.setOrganizationId(resultSet.getInt(++retrieveIndex));
      login.setUserType(resultSet.getString(++retrieveIndex));
      login.setConsumerId(resultSet.getString(++retrieveIndex));
      login.setEmail(resultSet.getString(++retrieveIndex));
      return login;
    });
  }

  @Override
  public int updateHashValue(Login login) throws SQLException, DbHandleException {
    String updateQuery = "update Login set hash_value = NULL where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(updateQuery);
    preparedStatement.setLong(1, login.getId());
    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  @Override
  public List<HashMap<String, String>> GetAdminList(Long organization_id) throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder("select id,organization_id ,email");
    query.append(" from Login where user_type='admin' and active_status=1 and organization_id=?;");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.
        createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, organization_id);
    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      HashMap<String, String> getMailList = new HashMap<>();
      int retrieveIndex = 0;
      getMailList.put("id", resultSet.getString(++retrieveIndex));
      getMailList.put("organization_id", resultSet.getString(++retrieveIndex));
      getMailList.put("email", resultSet.getString(++retrieveIndex));
      logger.info("Admin Details List fetched Successfully");
      return getMailList;
    });
  }
}

