package com.auzmor.impl.dao;

import com.auzmor.dao.EmploymentTypeDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.EmploymentModel;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EmploymentTypeDAOImpl implements EmploymentTypeDAO {
  private DbHandle dbHandle;

  public EmploymentTypeDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  /**
   * Insert into EmploymentType table. Rows employmentType, organization_id are mandatory
   * @param employmentModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13-02-2018
   */
  @Override
  public int insert(EmploymentModel employmentModel) throws SQLException,DbHandleException {
    String insertQuery = "insert into EmploymentType (employmentType,organization_id,active_status) " +
        "values (?,?,?)";

    PreparedStatement preparedStatement =
        this.dbHandle.createPreparedStatement(insertQuery, true);
    preparedStatement.setString(1, employmentModel.getEmploymentType());
    preparedStatement.setInt(2, employmentModel.getOrganizationId());
    preparedStatement.setString(3, "true");


    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, true);
  }

  /**
   * To check if employment type already exists before insert
   * @param employmentModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13-02-2018
   */
  @Override
  public boolean isEmploymentTypeExistsInOrganization(EmploymentModel employmentModel)
      throws SQLException,DbHandleException{
    String parameterizedSql  = "select 1 from EmploymentType where employmentType = ? and organization_id = ?" +
        "  and active_status = 'true'";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setString(1, employmentModel.getEmploymentType());
    preparedStatement.setInt(2, employmentModel.getOrganizationId());

    return DbHelper.getBoolean(this.dbHandle, preparedStatement);
  }

  /**
   * Fetch employment details based on primary id
   * @param employmentModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13-02-2018
   */
  @Override
  public EmploymentModel getEmploymentTypeDetails(EmploymentModel employmentModel)
      throws SQLException,DbHandleException {
    String parameterizedSql = "select id,employmentType,organization_id from EmploymentType where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setInt(1, employmentModel.getId());

    return DbHelper.getObject(this.dbHandle, preparedStatement, resultSet -> {
      employmentModel.setId(resultSet.getInt(1));
      employmentModel.setEmploymentType(resultSet.getString(2));
      employmentModel.setOrganizationId(resultSet.getInt(3));
      return employmentModel;
    });
  }

  /**
   * Fetch all employment types in an organization
   * @param employmentModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13-02-2018
   */
  @Override
  public List<EmploymentModel> getAllEmploymentTypes(EmploymentModel employmentModel)
      throws SQLException,DbHandleException {
    String parameterizedSql = "select id,employmentType,organization_id from EmploymentType " +
        "where organization_id = ? " +
        "and active_status = 'true'";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setInt(1, employmentModel.getOrganizationId());

    List<EmploymentModel> resultList = this.dbHandle.getSerializedObject(preparedStatement, resultSet -> {
      EmploymentModel resultModel = new EmploymentModel();
      resultModel.setId(resultSet.getInt(1));
      resultModel.setEmploymentType(resultSet.getString(2));
      resultModel.setOrganizationId(resultSet.getInt(3));
      return resultModel;
    });
    return resultList;
  }

  /**
   * Update employment type details based on id
   * @param requestParams
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13-02-2018
   */
  @Override
  public int updateEmploymentTypeDetails(MultiMap requestParams) throws SQLException,
      DbHandleException {
    /*StringBuilder stringBuilder = new StringBuilder();
    int i = 1;
    int length = requestParams.size();
    List<String> listOfParams = new ArrayList<String>();
    for(String key : requestParams.names()) {
      if(!key.equals("id")) {
        stringBuilder.append(key + " = ?");
        listOfParams.add(requestParams.get(key));
      }


      if (i < length && i != 1) {
        stringBuilder.append(",");
      }
      i++;
    }*/

    int columnIndex = 1;
    String parameterizedSql = "update EmploymentType set employmentType = ? where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setString(columnIndex,
        requestParams.get("employmentType"));

    preparedStatement.setInt(columnIndex + 1,
        Integer.valueOf(requestParams.get("id")));

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  /**
   * Delete employment type details based on id
   * @param employmentModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13-02-2018
   */
  @Override
  public int deleteEmploymentType(EmploymentModel employmentModel) throws SQLException,
      DbHandleException {
    String paramaterizedSql = "update EmploymentType set active_status = 'false' where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(paramaterizedSql);
    preparedStatement.setInt(1, employmentModel.getId());

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }
}
