package com.auzmor.impl.dao;

import com.auzmor.dao.HighestEducationDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.FieldOptions.HighestEducationModel;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.json.JsonObject;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class HighestEducationDAOImpl implements HighestEducationDAO {

  private DbHandle dbHandle;

  public HighestEducationDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  public int insert(HighestEducationModel highestEducationModel) throws SQLException, DbHandleException {
    String insertQuery = "insert into Highest_Education (highest_education,organization_id) " +
        "values (?,?)";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(insertQuery, true);
    preparedStatement.setString(1, highestEducationModel.getHighestEducation());
    preparedStatement.setLong(2, highestEducationModel.getOrganizationId());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, true);
  }

  public boolean isAlreadyHighestEducation(HighestEducationModel highestEducationModel)
      throws SQLException, DbHandleException {
    String selectQuery = "select 1 from Highest_Education where highest_education = ? and " +
        "organization_id = ? and active_status = 1";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setString(1, highestEducationModel.getHighestEducation());
    preparedStatement.setLong(2, highestEducationModel.getOrganizationId());

    return DbHelper.getBoolean(dbHandle, preparedStatement);
  }

  public JsonObject getHighestEducation(HighestEducationModel highestEducationModel)
      throws SQLException, DbHandleException {
    String selectQuery = "select id,highest_education,organization_id from Highest_Education " +
        "where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setLong(1, highestEducationModel.getId());

    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      JsonObject dataObject = new JsonObject();
      dataObject.put("id", resultSet.getLong(1));
      dataObject.put("highest_education", resultSet.getString(2));
      dataObject.put("organization_id", resultSet.getLong(3));
      return dataObject;
    });
  }

  public List<JsonObject> getAllHighestEducation(HighestEducationModel highestEducationModel)
      throws SQLException, DbHandleException {
    String selectQuery = "select id,highest_education,organization_id from Highest_Education " +
        "where organization_id = ? and active_status = 1";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setLong(1, highestEducationModel.getOrganizationId());

    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      JsonObject dataObject = new JsonObject();
      dataObject.put("id", resultSet.getLong(1));
      dataObject.put("highest_education", resultSet.getString(2));
      dataObject.put("organization_id", resultSet.getLong(3));
      return dataObject;
    });
  }

  public int update(HighestEducationModel highestEducationModel)
      throws SQLException, DbHandleException {
    String updateQuery = "update Highest_Education set highest_education =? where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(updateQuery);
    preparedStatement.setString(1, highestEducationModel.getHighestEducation());
    preparedStatement.setLong(2, highestEducationModel.getId());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  public int delete(HighestEducationModel highestEducationModel)
      throws SQLException, DbHandleException {
    String updateQuery = "update Highest_Education set active_status = 0 where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(updateQuery);
    preparedStatement.setLong(1, highestEducationModel.getId());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }
}
