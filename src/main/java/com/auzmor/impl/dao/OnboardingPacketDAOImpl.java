package com.auzmor.impl.dao;

import com.auzmor.dao.OnboardingPacketDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.onboarding.OnboardingPacket;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OnboardingPacketDAOImpl implements OnboardingPacketDAO{
  private DbHandle dbHandle;

  public OnboardingPacketDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  /**
   * To check already existing packet
   * @param onboardingPacket
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate Mar 8,2018
   */
  @Override
  public boolean isAlreadyExists(OnboardingPacket onboardingPacket) throws SQLException, DbHandleException {
    String parameterizedSql = "select 1 from OnboardingPacket where packet_name = ?" +
        " and organization_id = ? and inactive_status = 0";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setString(1, onboardingPacket.getPacket_name());
    preparedStatement.setInt(2, onboardingPacket.getOrganization_id());

    return DbHelper.getBoolean(dbHandle, preparedStatement);
  }

  /**
   * To add a packet
   * @param onboardingPacket
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate Mar 8,2018
   */
  @Override
  public int addPacket(OnboardingPacket onboardingPacket) throws SQLException, DbHandleException {
    String insertQuery = "insert into OnboardingPacket (packet_name, organization_id, creator_id," +
        " welcome_header, welcome_description, welcome_video, latitude, longitude) " +
        "values (?,?,?,?,?,?,?,?)";
    int index = 0;
    PreparedStatement preparedStatement =
        this.dbHandle.createPreparedStatement(insertQuery, true);
    preparedStatement.setString(++index, onboardingPacket.getPacket_name());
    preparedStatement.setInt(++index, onboardingPacket.getOrganization_id());
    preparedStatement.setInt(++index, onboardingPacket.getCreator_id());
    preparedStatement.setString(++index, onboardingPacket.getWelcome_header());
    preparedStatement.setString(++index, onboardingPacket.getWelcome_description());
    preparedStatement.setString(++index, onboardingPacket.getWelcome_video());
    preparedStatement.setDouble(++index, onboardingPacket.getLatitude());
    preparedStatement.setDouble(++index, onboardingPacket.getLongitude());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, true);
  }

  /**
   * To get packet details
   * @param onboardingPacket
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate Mar 8,2018
   */
  @Override
  public JsonArray getPackets(OnboardingPacket onboardingPacket)
      throws SQLException, DbHandleException {
    JsonArray resultArray = new JsonArray();
    String parameterizedSql = "select packet_id,packet_name,is_default from " +
        "OnboardingPacket where organization_id = ? " +
        "and packet_id = if(? > 0,? ,packet_id) and inactive_status = 0";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setInt(1, onboardingPacket.getOrganization_id());
    preparedStatement.setInt(2, onboardingPacket.getPacket_id());
    preparedStatement.setInt(3, onboardingPacket.getPacket_id());

    ResultSet resultSet = preparedStatement.executeQuery();
    while (resultSet.next()) {
      JsonObject jsonObject = new JsonObject();
      jsonObject.put("id", resultSet.getInt("packet_id"));
      jsonObject.put("name", resultSet.getString("packet_name"));
      jsonObject.put("is_default", resultSet.getBoolean("is_default"));
      resultArray.add(jsonObject);
    }

    if (null != resultSet && !resultSet.isClosed()) {
      resultSet.close();
    }

    return resultArray;
  }

  /**
   * Delete onboarding packet
   * @param onboardingPacket
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 01/MAR/2018
   */
  @Override
  public int deletePacket(OnboardingPacket onboardingPacket) throws SQLException, DbHandleException {
    String deleteQuery = "update OnboardingPacket set inactive_status = 1 where packet_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(deleteQuery);
    preparedStatement.setInt(1, onboardingPacket.getPacket_id());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  /**
   * Update packet details
   * @param requestParams
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 01/MAR/2018
   */
  @Override
  public int updatePacket(MultiMap requestParams) throws SQLException, DbHandleException {
    StringBuilder stringBuilder = new StringBuilder();
    int i = 1;
    int length = requestParams.size();
    List<String> listOfParams = new ArrayList<String>();
    for(String key : requestParams.names()) {
      if(!key.equals("packetId")) {
        stringBuilder.append(key + " = ?");
        if(i <= length-2)
          stringBuilder.append(",");
        listOfParams.add(requestParams.get(key));
        i++;
      }
    }

    int columnIndex = 1;
    String parameterizedSql = "update OnboardingPacket set " + stringBuilder + " where packet_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    for(int index = 0; index < listOfParams.size(); index++) {
      preparedStatement.setString(columnIndex, listOfParams.get(index));
      columnIndex++;
    }
    preparedStatement.setInt(listOfParams.size() + 1,
        Integer.valueOf(requestParams.get("packetId")));

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }
}
