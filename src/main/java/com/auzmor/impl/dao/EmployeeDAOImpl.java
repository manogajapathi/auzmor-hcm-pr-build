package com.auzmor.impl.dao;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auzmor.dao.EmployeeDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.handler.CountryStateHandler;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.Login;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.util.JWTUtil;
import com.auzmor.impl.util.Mail;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.util.db.DbHandle;
import com.mysql.cj.core.util.StringUtils;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class EmployeeDAOImpl implements EmployeeDAO {
  private static final Logger logger = LoggerFactory.getLogger(EmployeeDAOImpl.class);
  StringUtil util = new StringUtil();
  DatabaseUtil dbutil = new DatabaseUtil();
  private DbHandle dbHandle;

  public EmployeeDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  @Override
  public int create(Employee employee) throws SQLException, DbHandleException {
    logger.info("Create a Employee in DB");
    StringBuilder parameterizedSql = new StringBuilder("INSERT INTO `Employee`(`first_name`, ");
    parameterizedSql.append("`last_name`, `personal_email`, `work_email`, `job_title`, `manager`, ");
    parameterizedSql.append("`location`, `pay_rate`, `pay_type`, `employment_status`, `primary_phone`");
    parameterizedSql.append(", `hire_date`, `user_id`, `status`, `organization_id`,`onboarding_status`,`access_level`) VALUES (?, ?,");
    parameterizedSql.append(" ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql
        .toString(), true);
    preparedStatement.setString(++index, employee.getFirst_name());
    preparedStatement.setString(++index, employee.getLast_name());
    preparedStatement.setString(++index, employee.getPersonal_email());
    preparedStatement.setString(++index, employee.getWork_email());
    preparedStatement.setString(++index, employee.getJob_title());
    preparedStatement.setString(++index, employee.getManager());
    preparedStatement.setString(++index, employee.getLocation());
    preparedStatement.setString(++index, employee.getPay_rate());
    preparedStatement.setString(++index, employee.getPay_type());
    preparedStatement.setString(++index, employee.getEmployment_status());
    preparedStatement.setString(++index, employee.getPrimary_phone());
    preparedStatement.setString(++index, employee.getHire_date());
    preparedStatement.setInt(++index, employee.getUser_id());
    preparedStatement.setString(++index, employee.getStatus());
    preparedStatement.setLong(++index, employee.getOrganization_id());
    preparedStatement.setString(++index, employee.getOnboarding_status());
    preparedStatement.setString(++index, employee.getAccess_level());

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, true);
  }

  /**
   * Check for employee with private email.
   *
   * @param email
   * @param organizationId
   * @return
   * @throws DbHandleException
   * @throws SQLException
   */
  @Override
  public boolean isEmployeeExistsInOrganization(String email, Long organizationId)
      throws DbHandleException, SQLException {

    logger.info("Checking an Employee in organization");
    StringBuilder parameterizedSql = new StringBuilder("SELECT 1 FROM `Employee` ");
    parameterizedSql.append("WHERE (`personal_email` = ? OR `work_email` = ?) AND ");
    parameterizedSql.append("`organization_id` = ? and active_status = 1");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql
        .toString());
    preparedStatement.setString(++index, email);
    preparedStatement.setString(++index, email);
    preparedStatement.setLong(++index, organizationId);

    return DbHelper.getBoolean(this.dbHandle, preparedStatement);
  }

  @Override
  public boolean isEmployeeExistsInOrganization(long employeeId, Long organizationId)
      throws DbHandleException, SQLException {

    logger.info("Checking an Employee in organization");
    StringBuilder parameterizedSql = new StringBuilder("SELECT 1 FROM `Employee` ");
    parameterizedSql.append("WHERE employee_id = ? AND ");
    parameterizedSql.append("`organization_id` = ? and active_status = 1");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql
        .toString());
    preparedStatement.setLong(++index, employeeId);
    preparedStatement.setLong(++index, organizationId);

    return DbHelper.getBoolean(this.dbHandle, preparedStatement);
  }

  @Override
  public JsonArray getAllEmployees(MultiMap params) throws SQLException, DbHandleException {

    logger.info("Get all employee for organization from DB");
    String sorterString = params.get("sorter");
    String searchString = params.get("search_string");
    int limit = Integer.valueOf(params.get("limit"));
    int offset = Integer.valueOf(params.get("offset"));
    int organizationId = Integer.valueOf(params.get("organization_id"));
    JsonArray resultArray = new JsonArray();
    String fieldString = params.get("fields");
    String[] fields = fieldString.split(",");

    List<String> fieldList = new LinkedList<>();
    for (String field : fields) {
      fieldList.add(field);
    }

    if (!fieldString.contains("employee_id")) {
      fieldString = "employee_id," + fieldString;
    }

    String queryString = "select " + fieldString + " from Employee where LEFT(first_name,1) in (?," +
        " ?) and concat(first_name,' ',last_name) like '%?%' and active_status = 1 " +
        " and organization_id = ? limit ? offset ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(queryString);
    preparedStatement.setString(1, sorterString.toUpperCase());
    preparedStatement.setString(2, sorterString.toLowerCase());
    preparedStatement.setString(3, searchString);
    preparedStatement.setInt(4, organizationId);
    preparedStatement.setInt(5, limit);
    preparedStatement.setInt(6, offset);

    ResultSet resultSetObject = preparedStatement.executeQuery();
    while (resultSetObject.next()) {
      JsonObject dataObject = new JsonObject();
      for (String field : fieldList) {
        dataObject.put(field, resultSetObject.getString(field));
      }
      resultArray.add(dataObject);
    }

    return resultArray;
  }

  /**
   * To check if any employee is assigned order any fieldOption
   *
   * @param fieldOptionColumn field option column name to be checked as in Employee table
   * @param fieldOptionValue  value for the corresponding field option
   * @param organizationId    organization specific
   * @return
   * @throws DbHandleException
   * @throws SQLException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15/FEB/2018
   */
  @Override
  public boolean isFieldOptionPresent(String fieldOptionColumn, String fieldOptionValue,
                                      int organizationId)
      throws DbHandleException, SQLException {

    logger.info("Check is any field options present in DB");
    String parameterizedSql = "select 1 from Employee where " + fieldOptionColumn + " = ? " +
        "and organization_id = ? and active_status = 1";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setString(1, fieldOptionValue);
    preparedStatement.setInt(2, organizationId);

    return DbHelper.getBoolean(dbHandle, preparedStatement);
  }

  /**
   * To retrieve field options details along with count of employees assigned under each field option
   *
   * @param fieldRequired  fields requried from field option table
   * @param tableName      table for field option to be retrieved
   * @param fieldColumn    column name of field option for fetching count as in Employee table
   * @param organizationId organization specific
   * @return
   * @throws DbHandleException
   * @throws SQLException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15/FEB/2018
   */
  @Override
  public JsonArray getEmployeeCountForFieldOptions(String fieldRequired, String tableName,
                                                   String fieldColumn, int organizationId)
      throws DbHandleException, SQLException {
    logger.info("Get employee count from the organization");
    JsonArray FieldArray = new JsonArray();
    StringBuilder fieldOptionsQuery = new StringBuilder();
    fieldOptionsQuery.append("select ");
    fieldOptionsQuery.append(fieldRequired);
    fieldOptionsQuery.append(" from ");
    fieldOptionsQuery.append(tableName);
    fieldOptionsQuery.append(" where organization_id = ? and active_status = ?");
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(fieldOptionsQuery.toString());
    preparedStatement.setInt(1, organizationId);
    preparedStatement.setString(2, "true");

    ResultSet resultSet = preparedStatement.executeQuery();
    while (resultSet.next()) {
      JsonObject dataObject = new JsonObject();
      dataObject.put("id", resultSet.getString(1));
      dataObject.put("name", resultSet.getString(2));
      if (StringUtil.printValidString(tableName).equals("Location")) {
        dataObject.put("company_address_line1", resultSet.getString(3));
      }

      StringBuilder parameterizedSql = new StringBuilder();
      parameterizedSql.append("select count(");
      parameterizedSql.append(fieldColumn);
      parameterizedSql.append(") from Employee where ");
      parameterizedSql.append(fieldColumn);
      parameterizedSql.append(" = ? and active_status = 1 and organization_id = ?");
      PreparedStatement preStatement = this.dbHandle.createPreparedStatement(parameterizedSql.toString());
      preStatement.setString(1, resultSet.getString(2));
      preStatement.setInt(2, organizationId);
      ResultSet result = preStatement.executeQuery();
      while (result.next()) {
        dataObject.put("employee_count", result.getInt(1));
      }
      FieldArray.add(dataObject);
    }
    return FieldArray;
  }

  public Employee getEmployeeDetails(long employeeId) throws SQLException, DbHandleException {
    logger.info("getting the employee details for id=" + employeeId);

    StringBuilder query = new StringBuilder("Select profile_image,first_name,last_name," +
        "organization_id,onboarding_packet, `user_id`,work_email,personal_email, `timezone_format`");
    query.append(" from Employee where employee_id = ? and active_status =1 ");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, employeeId);
    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      int retrieveIndex = 0;
      Employee employee = new Employee();
      employee.setProfile_image(StringUtil.printValidString(resultSet.getString(++retrieveIndex)));
      employee.setFirst_name(resultSet.getString(++retrieveIndex));
      employee.setLast_name(resultSet.getString(++retrieveIndex));
      employee.setOrganization_id(resultSet.getInt(++retrieveIndex));
      employee.setOnboarding_packet(resultSet.getString(++retrieveIndex));
      employee.setUser_id(resultSet.getInt(++retrieveIndex));
      employee.setWork_email(resultSet.getString(++retrieveIndex));
      employee.setPersonal_email(resultSet.getString(++retrieveIndex));
      employee.setTimeZoneFormat(resultSet.getString(++retrieveIndex));
      // TODO fill the values

      return employee;
    });

  }

  public Employee getEmployeeDetailsByLoginId(long loginId) throws SQLException, DbHandleException {

    logger.info("Get Employee details with login id");
    StringBuilder query = new StringBuilder("SELECT `employee_id`, `profile_image`, `first_name`, `last_name`, `organization_id` ");
    query.append(", `access_level` ");
    query.append(" FROM `Employee` WHERE `user_id` = ? AND `active_status` = TRUE");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, loginId);

    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      int retrieveIndex = 0;
      Employee employee = new Employee();
      employee.setEmployee_id(resultSet.getInt(++retrieveIndex));
      employee.setProfile_image(StringUtil.printValidString(resultSet.getString(++retrieveIndex)));
      employee.setFirst_name(resultSet.getString(++retrieveIndex));
      employee.setLast_name(resultSet.getString(++retrieveIndex));
      employee.setOrganization_id(resultSet.getInt(++retrieveIndex));
      employee.setAccess_level(resultSet.getString(++retrieveIndex));
      employee.setUser_id((int) loginId);
      // TODO fill the values

      return employee;
    });

  }

  public int getUserIdForEmployee(int employeeId) throws SQLException, DbHandleException {
    logger.info("getting the user id for employee id=" + employeeId);
    String parameterizedSql = "select user_id from Employee where employee_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setInt(1, employeeId);

    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      int loginId = 0;
      loginId = resultSet.getInt(1);
      return loginId;
    });
  }

  @Override
  public int updateOrganizationDetails(int organizationId, String workEmail) throws SQLException, DbHandleException {
    logger.info("Update organziation details from db");

    String updateQuery = "update Organization set account_owner_email = ? where organization_id = ?";
    PreparedStatement preStatement = this.dbHandle.createPreparedStatement(updateQuery);
    preStatement.setString(1, workEmail);
    preStatement.setInt(2, organizationId);

    int count = DbHelper.executeUpdate(this.dbHandle, preStatement, false);
    return count;
  }


  public int deleteUser(int organizationId, int employeeId) throws SQLException, DbHandleException {

    logger.info("Delete a user from DB");
    int userId = this.getUserId(employeeId);

    String updateQuery = "update Login,Employee set Login.active_status = 0,Employee.active_status" +
        " = 0,Employee.status = 'Deleted' where Employee.user_id = ? and Login.id = ? and " +
        "Employee.organization_id = ? and Login.organization_id = ? ";
    PreparedStatement preStatement = this.dbHandle.createPreparedStatement(updateQuery);
    preStatement.setInt(1, userId);
    preStatement.setInt(2, userId);
    preStatement.setInt(3, organizationId);
    preStatement.setInt(4, organizationId);

    int count = DbHelper.executeUpdate(this.dbHandle, preStatement, false);
    return count;

  }

  public JsonArray getAllUsersList(MultiMap params) throws SQLException, DbHandleException {
    logger.info("Get all user details from DB");
    String limit = params.get("limit");
    String offset = params.get("offset");
    String searchFilter = params.get("search_filter");
    long adminEmployeeId = Long.parseLong(params.get("employee_id"));
    long organizationId = Long.parseLong(params.get("organization_id"));
    JsonArray resultArray = new JsonArray();
    StringBuilder limitFilter = new StringBuilder();

    if (limit != null && limit.length() > 0) {
      limitFilter.append(" LIMIT " + limit);
    } else if (offset != null) {
      limitFilter.append(" LIMIT 0");
    }
    if (offset != null && offset.length() > 0) {
      limitFilter.append(" OFFSET " + offset);
    }
    String selectString = "";
    if (!StringUtils.isNullOrEmpty(searchFilter)) {
      searchFilter = searchFilter.toUpperCase();
      selectString = " and (UPPER(Employee.first_name) like ? or " +
          "UPPER(Employee.last_name) like ? or UPPER(CONCAT(Employee.first_name, ' ', " +
          "Employee.last_name)) like ?)";
    }

    String queryString = "select Employee.employee_id,Employee.first_name,Employee.last_name,Employee.work_email," +
        "Employee.primary_phone,Employee.secondary_phone,Employee.work_phone,Employee.access_level,Employee.department," +
        "Employee.job_title,Employee.city,Employee.state,Employee.country,Employee.user_id,Employee.profile_image," +
        "Employee.onboarding_status,Employee.primary_region_code,Employee.secondary_region_code," +
        "Employee.work_region_code from Employee inner join Login on Employee.user_id = Login.id where " +
        "Employee.active_status = 1 and Employee.organization_id = ? and " +
        "Login.active_status = 1 " + selectString + " order by Employee.employee_id = ? DESC, " +
        "Employee.onboarding_status = 'Self Registered' DESC, " +
        "Employee.employee_id DESC " + limitFilter;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(queryString);
    preparedStatement.setLong(1, organizationId);
    if (!selectString.isEmpty()) {
      preparedStatement.setString(2, "%"+searchFilter+"%");
      preparedStatement.setString(3, "%"+searchFilter+"%");
      preparedStatement.setString(4, "%"+searchFilter+"%");
      preparedStatement.setLong(5, adminEmployeeId);
    } else {
      preparedStatement.setLong(2, adminEmployeeId);
    }
    ResultSet resultSetObject = preparedStatement.executeQuery();
    while (resultSetObject.next()) {
      JsonObject dataObject = new JsonObject();
      dataObject.put("user_id", resultSetObject.getInt("user_id"));
      dataObject.put("employee_id", resultSetObject.getInt("employee_id"));
      dataObject.put("first_name", resultSetObject.getString("first_name"));
      dataObject.put("last_name", resultSetObject.getString("last_name"));
      dataObject.put("work_email", resultSetObject.getString("work_email"));
      dataObject.put("primary_phone", resultSetObject.getString("primary_phone"));
      dataObject.put("secondary_phone", resultSetObject.getString("primary_phone"));
      dataObject.put("work_phone", resultSetObject.getString("work_phone"));
      dataObject.put("access_level", resultSetObject.getString("access_level"));
      dataObject.put("department", resultSetObject.getString("department"));
      dataObject.put("job_title", resultSetObject.getString("job_title"));
      dataObject.put("city", resultSetObject.getString("city"));
      dataObject.put("country", resultSetObject.getString("country"));
      dataObject.put("profile_image", resultSetObject.getString("profile_image"));
      dataObject.put("onboarding_status", resultSetObject.getString("onboarding_status"));
      dataObject.put("primary_region_code", resultSetObject.getString("primary_region_code"));
      dataObject.put("secondary_region_code", resultSetObject.getString("secondary_region_code"));
      dataObject.put("work_region_code", resultSetObject.getString("work_region_code"));
      if ((resultSetObject.getString("state") != null) && (!(resultSetObject.getString("state").isEmpty()))) {
        dataObject.put("state", CountryStateHandler.getStateName(resultSetObject.getString("state")));
      } else {
        dataObject.put("state", " ");
      }
      resultArray.add(dataObject);
    }
    return resultArray;
  }

  public int editUser(MultiMap params) throws SQLException, DbHandleException {

    logger.info("Edit user in db");
    int organizationId = Integer.valueOf(params.get("organization_id"));
    int employeeId = Integer.valueOf(params.get("employee_id"));
    String firstName = params.get("first_name");
    String lastName = params.get("last_name");
    String profileImage = params.get("profile_image");
    String workEmail = params.get("work_email");
    String primaryPhone = params.get("primary_phone");
    String accessLevel = params.get("access_level");
    String department = params.get("department");
    String jobTitle = params.get("job_title");
    String city = params.get("city");
    String state = params.get("state");
    String country = params.get("country");
    String regionCode = params.get("primary_region_code");
    int userId = this.getUserId(employeeId);

    String updateQuery = "update Login,Employee set Employee.first_name = ?,Employee.last_name = ?," +
        " Employee.profile_image = ?,Employee.work_email = ?,Employee.primary_phone = ?,Employee.access_level = ?," +
        "Employee.department = ?,Employee.job_title = ?,Employee.city = ?," +
        "Employee.state = ?,Employee.country = ? ,Login.first_name = ?,Login.last_name = ?," +
        "Login.user_type = ?, Login.email = ?, Employee.primary_region_code = ? where Employee.user_id = ? and Login.id = ? " +
        "and Employee.organization_id = ? and Login.organization_id = ?";

    PreparedStatement preStatement = this.dbHandle.createPreparedStatement(updateQuery);
    preStatement.setString(1, firstName);
    preStatement.setString(2, lastName);
    preStatement.setString(3, profileImage);
    preStatement.setString(4, workEmail);
    preStatement.setString(5, primaryPhone);
    preStatement.setString(6, accessLevel);
    preStatement.setString(7, department);
    preStatement.setString(8, jobTitle);
    preStatement.setString(9, city);
    preStatement.setString(10, state);
    preStatement.setString(11, country);
    preStatement.setString(12, firstName);
    preStatement.setString(13, lastName);
    preStatement.setString(14, accessLevel);
    preStatement.setString(15, workEmail);
    preStatement.setString(16, regionCode);
    preStatement.setInt(17, userId);
    preStatement.setInt(18, userId);
    preStatement.setInt(19, organizationId);
    preStatement.setInt(20, organizationId);

    return DbHelper.executeUpdate(this.dbHandle, preStatement, false);
  }


  public int addNewUser(MultiMap params) throws SQLException, DbHandleException {

    logger.info("Add new user in db");
    int organizationId = Integer.valueOf(params.get("organization_id"));
    String firstName = params.get("first_name");
    String lastName = params.get("last_name");
    String workEmail = params.get("work_email");
    String primaryPhone = params.get("primary_phone");
    String accessLevel = params.get("access_level");
    String department = params.get("department");
    String jobTitle = params.get("job_title");
    String city = params.get("city");
    String state = params.get("state");
    String country = params.get("country");
    String profileImage = params.get("profile_image");
    String manager = " ";
    String payRate = " ";
    String location = " ";
    String payType = " ";
    String personalEmail = " ";
    String hireDate = " ";
    String status = "Active";
    String onboardingStatus = "Packet Not Sent";
    int onboardingPacketId = 0;
    int userId = 0;
    String regionCode = params.get("primary_region_code");

    if (!(this.isEmployeeExistsInOrganization(workEmail, (long) organizationId))) {

      String organizationQueryString = "select organization_name,hris_url from Organization where " +
          "organization_id = ?";
      PreparedStatement preStatement2 = this.dbHandle.createPreparedStatement(organizationQueryString);
      preStatement2.setInt(1, organizationId);

      String organizationName = " ";
      String organizationUrl = " ";

      ResultSet resultSetObject = preStatement2.executeQuery();
      while (resultSetObject.next()) {
        organizationUrl = resultSetObject.getString("hris_url");
        organizationName = resultSetObject.getString("organization_name");
      }
      String password = util.randomString(8);
      String key = UUID.randomUUID().toString();
      String hash = UUID.randomUUID().toString();
      String updateLoginQuery = "INSERT INTO Login(first_name,last_name,user_type,email," +
          "organization_id,password,key_value, hash_value) VALUES(?,?,?,?,?,sha(?),?, ?)";
      PreparedStatement preStatement4 = this.dbHandle.createPreparedStatement(updateLoginQuery);
      preStatement4.setString(1, firstName);
      preStatement4.setString(2, lastName);
      preStatement4.setString(3, accessLevel);
      preStatement4.setString(4, workEmail);
      preStatement4.setInt(5, organizationId);
      preStatement4.setString(6, password);
      preStatement4.setString(7, key);
      preStatement4.setString(8, hash);

      preStatement4.executeUpdate();

      String idRetrieveQuery = "SELECT id from Login where email = ?";

      PreparedStatement preStatement5 = this.dbHandle.createPreparedStatement(idRetrieveQuery);
      preStatement5.setString(1, workEmail);

      ResultSet resultSetObject1 = preStatement5.executeQuery();
      while (resultSetObject1.next()) {
        userId = resultSetObject1.getInt("id");
      }

      String updateEmployeeQuery = "INSERT INTO Employee (first_name,last_name," +
          "work_email,primary_phone,access_level,department,job_title,city,state,onboarding_packet,country," +
          "organization_id,personal_email,hire_date,manager,pay_rate,location,pay_type,status," +
          "user_id,onboarding_status,profile_image,primary_region_code) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?" +
          ",?,?,?)";

      PreparedStatement preStatement6 = this.dbHandle.createPreparedStatement(updateEmployeeQuery, true);
      preStatement6.setString(1, firstName);
      preStatement6.setString(2, lastName);
      preStatement6.setString(3, workEmail);
      preStatement6.setString(4, primaryPhone);
      preStatement6.setString(5, accessLevel);
      preStatement6.setString(6, department);
      preStatement6.setString(7, jobTitle);
      preStatement6.setString(8, city);
      preStatement6.setString(9, state);
      preStatement6.setInt(10, onboardingPacketId);
      preStatement6.setString(11, country);
      preStatement6.setInt(12, organizationId);
      preStatement6.setString(13, personalEmail);
      preStatement6.setString(14, hireDate);
      preStatement6.setString(15, manager);
      preStatement6.setString(16, payRate);
      preStatement6.setString(17, location);
      preStatement6.setString(18, payType);
      preStatement6.setString(19, status);
      preStatement6.setInt(20, userId);
      preStatement6.setString(21, onboardingStatus);
      preStatement6.setString(22, profileImage);
      preStatement6.setString(23, regionCode);
      Mail mail = new Mail();
      Login login = new Login();
      login.setOrganizationId(organizationId);
      login.setId(userId);
      login.setUserType(accessLevel);
      String encryptedString;
      try {
        encryptedString = generateToken(login);
      } catch (UnsupportedEncodingException e) {
        return -1;
      }

      String userMailBody = "<html><body>Dear " + firstName + " , <br> " +
          "<p>This is to inform that you have been added as a new user in " + organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1).toLowerCase() + ".<br/>" +
          "<p>Welcome aboard!  " + "<a href='https://" + organizationUrl +
          "/backend/login/checkReset/" + encryptedString +
          "'>Click here</a>  to set your password and login.<br/>"
          + "<br>Kindly reach out to our support team if you face any issues in logging in.<br/><br/>"
          + "<br>Regards,<br/>" + organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1).toLowerCase() + ".\n<br/>" +
          "--------------------------------------------------------------\n<br/>" +
          "This is a system generated mail.\n<br/>" +
          "Please do not reply to this mail\n<br/>" +
          "-------------------------------------------------------------- " +
          "</body></html>";
      mail.sendMail(Vertx.vertx(), userMailBody, workEmail, "User Registration");

      DbHelper.executeUpdate(this.dbHandle, preStatement6, true);
      return userId;
    } else
      return -1;
  }

  @Override
  public Employee getWorkEmailAndAccessLevel(int employeeId, int organizationId) throws SQLException, DbHandleException {
    logger.info("Get work email from db");

    String selectQuery = "SELECT employee_id, user_id, work_email, access_level from Employee where employee_id = ? " +
        "and organization_id = ? and active_status = 1";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setInt(1, employeeId);
    preparedStatement.setInt(2, organizationId);

    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      int retrieveIdx = 0;
      Employee employee = new Employee();
      employee.setEmployee_id(resultSet.getInt(++retrieveIdx));
      employee.setUser_id(resultSet.getInt(++retrieveIdx));
      employee.setWork_email(resultSet.getString(++retrieveIdx));
      employee.setAccess_level(resultSet.getString(++retrieveIdx));
      return employee;
    });
  }

  public int uploadProfileImage(String imagePath, long organizationId, int employeeId) throws SQLException, DbHandleException {
    String updateQuery = " UPDATE Employee set profile_image = ? where employee_id = ? and organization_id = ? ";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(updateQuery);
    preparedStatement.setString(1, imagePath);
    preparedStatement.setInt(2, employeeId);
    preparedStatement.setLong(3, organizationId);
    return preparedStatement.executeUpdate();
  }

  public JsonObject getEmployee(long employeeId, long organizationId, StringBuilder selectFilter) throws SQLException, DbHandleException {
    JsonObject dataObject = new JsonObject();
    String queryString = "select * from  Employee where active_status = 1 and employee_id = ? and organization_id = ? " + selectFilter;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(queryString);
    preparedStatement.setLong(1, employeeId);
    preparedStatement.setLong(2, organizationId);
    ResultSet resultSetObject = preparedStatement.executeQuery();
    if (resultSetObject.isBeforeFirst()) {
      while (resultSetObject.next()) {
        dataObject.put("user_id", resultSetObject.getInt("user_id"));
        dataObject.put("employee_id", resultSetObject.getInt("employee_id"));
        dataObject.put("first_name", resultSetObject.getString("first_name"));
        dataObject.put("last_name", resultSetObject.getString("last_name"));
        dataObject.put("work_email", resultSetObject.getString("work_email"));
        dataObject.put("primary_phone", resultSetObject.getString("primary_phone"));
        dataObject.put("access_level", resultSetObject.getString("access_level"));
        dataObject.put("department", resultSetObject.getString("department"));
        dataObject.put("job_title", resultSetObject.getString("job_title"));
        dataObject.put("city", resultSetObject.getString("city"));
        dataObject.put("state", resultSetObject.getString("state"));
        dataObject.put("country", resultSetObject.getString("country"));
        dataObject.put("profile_image", resultSetObject.getString("profile_image"));
        dataObject.put("onboarding_status", resultSetObject.getString("onboarding_status"));
      }
    } else {
      dataObject.clear();
    }
    return dataObject;
  }

  public int getUserId(int employeeId) throws SQLException, DbHandleException {

    logger.info("Get user id for employee from db");
    int userId = 0;
    String query = "select user_id from Employee where employee_id = ?";
    PreparedStatement preStatement = this.dbHandle.createPreparedStatement(query);
    preStatement.setInt(1, employeeId);
    ResultSet resultSetObject = preStatement.executeQuery();
    while (resultSetObject.next()) {
      userId = resultSetObject.getInt("user_id");
    }
    return userId;
  }

  @Override
  public int updateEmployeePacket(Employee employee) throws SQLException, DbHandleException {
    logger.info("Update employee packte in db");
    String updateQuery = "update Employee set onboarding_packet = ? where employee_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(updateQuery);
    preparedStatement.setString(1, employee.getOnboarding_packet());
    preparedStatement.setInt(2, employee.getEmployee_id());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  public String generateToken(Login loginObject) throws UnsupportedEncodingException {
    logger.info("Generate a token");
    String generatedToken = "";
    String key = JWTUtil.getEncryptKey();
    Algorithm algorithm = Algorithm.HMAC256(key);
    generatedToken = JWT.create().withIssuer("auth0")
        .withClaim("organization_id", String.valueOf(loginObject.getOrganizationId()))
        .withClaim("user_id", String.valueOf(loginObject.getId()))
        .withClaim("user_type", loginObject.getUserType())
        .sign(algorithm);
    return generatedToken;
  }

  @Override
  public List<HashMap<String, String>> getEmployeeEmailIds(long collaboratorId, long adminId)
      throws SQLException, DbHandleException {
    logger.info("Get list of employee ids");
    String query = "select employee_id,personal_email,work_email,first_name,last_name,primary_phone from Employee" +
        " where employee_id in (?,?) and active_status = 1 ";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(1, collaboratorId);
    preparedStatement.setLong(2, adminId);

    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      HashMap<String, String> getEmployeeEmailList = new HashMap<>();
      int retrieveIndex = 0;
      getEmployeeEmailList.put("employee_id", resultSet.getString(++retrieveIndex));
      getEmployeeEmailList.put("personal_email", resultSet.getString(++retrieveIndex));
      getEmployeeEmailList.put("work_email", resultSet.getString(++retrieveIndex));
      getEmployeeEmailList.put("first_name", resultSet.getString(++retrieveIndex));
      getEmployeeEmailList.put("last_name", resultSet.getString(++retrieveIndex));

      return getEmployeeEmailList;
    });
  }

  @Override
  public JsonObject getDashBoardCount(Employee employee) throws SQLException, DbHandleException {
    String selectQuery = "select job.job_count,candidate.candidate_count,task.task_count,interview.interview_count from \n" +
        "(select   (select count(job_id) as job_count from Collaborators,Job where collaborator = ? " +
        "and active_status = 1 and Job.id = Collaborators.job_id and Job.job_status in ('Published', 'Unpublished')) + " +
        "(select count(id) as job_count from Job where hiring_lead = ? and job_active_status = 1 " +
        "and job_status != 'Closed') " +
        "as job_count) job, " +
        "(select \n" +
        "(select count(1) as candidate_count from Candidate, Job where Candidate.job_id = Job.id " +
        "and Job.hiring_lead = ? and Candidate.active_status =1 and Candidate.status != 'Hire' " +
        "and Job.job_status!='Closed') +\n" +
        "(select count(1) as candidate_count from Candidate, Collaborators " +
        "where Candidate.job_id = Collaborators.job_id and Collaborators.collaborator = ? and " +
        "Candidate.active_status =1 " +
      //  " and Candidate.status != 'Hire' " +
        "and Collaborators.active_status = 1 and Collaborators.job_id in " +
        "(select id from Job where job_status != 'Closed')) as candidate_count) candidate, " +
        "(select count(id) as task_count from Job_Tasks where employee_id = ? and complete_flag = 0 " +
        "and inactive_status = 0 and all_flag != 1) task, " +
        "(select count(id) as interview_count from InterviewProcessCandidate " +
        "where InterviewProcessCandidate.collaborator_id = ? and active_status = 1 and is_completed = 0 " +
        " and ((InterviewProcessCandidate.status = 'accept') or (InterviewProcessCandidate.status IS NULL) or (InterviewProcessCandidate.status = '')) and InterviewProcessCandidate.interview_cancel_reason is null " +
        "and str_to_date(concat(interview_date,' ',interview_time), '%Y-%m-%d %H:%i')  > current_timestamp()) interview";

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setInt(1, employee.getEmployee_id());
    preparedStatement.setInt(2, employee.getEmployee_id());
    preparedStatement.setInt(3, employee.getEmployee_id());
    preparedStatement.setInt(4, employee.getEmployee_id());
    preparedStatement.setInt(5, employee.getEmployee_id());
    preparedStatement.setInt(6, employee.getEmployee_id());

    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      JsonObject dataObject = new JsonObject();
      dataObject.put("job_count", resultSet.getInt(1));
      dataObject.put("candidate_count", resultSet.getInt(2));
      dataObject.put("task_count", resultSet.getInt(3));
      dataObject.put("interview_count", resultSet.getInt(4));
      dataObject.put("dummy_data", false);
      return dataObject;
    });
  }
}

