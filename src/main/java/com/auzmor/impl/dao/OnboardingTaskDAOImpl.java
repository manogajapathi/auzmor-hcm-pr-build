package com.auzmor.impl.dao;

import com.auzmor.dao.OnboardingTaskDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.onboarding.OnboardingTask;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OnboardingTaskDAOImpl implements OnboardingTaskDAO {
  private DbHandle dbHandle;

  public OnboardingTaskDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  /**
   * To check if onboarding task already task in packet
   * @param onboardingTask
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public boolean isAlreadyExists(OnboardingTask onboardingTask) throws SQLException, DbHandleException {
    String parameterizedSql = "select 1 from OnboardingTask where packet_id = ? and task_name = ?" +
        " and inactive_status = 0";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setInt(1, onboardingTask.getPacket_id());
    preparedStatement.setString(2, onboardingTask.getTask_name());
    
    return DbHelper.getBoolean(dbHandle, preparedStatement);
  }

  /**
   * Insert row in OnboardingTask table. Rows task_name,task_description,packet_id are mandatory
   * @param onboardingTask
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public int addTask(OnboardingTask onboardingTask) throws SQLException, DbHandleException {
    String sqlQuery = "insert into OnboardingTask(task_name,task_description,viewing_required,allow_doc_upload" +
        ",doc_upload_required,packet_id,referral_file) " +
        "values(?,?,?,?,?,?,?);";
    PreparedStatement insertStatement =
        this.dbHandle.createPreparedStatement(sqlQuery, true);
    insertStatement.setString(1, onboardingTask.getTask_name());
    insertStatement.setString(2, onboardingTask.getTask_description());
    insertStatement.setBoolean(3, onboardingTask.isAllow_doc_upload());
    insertStatement.setBoolean(4, onboardingTask.isViewing_required());
    insertStatement.setBoolean(5, onboardingTask.isDoc_upload_required());
    insertStatement.setInt(6, onboardingTask.getPacket_id());
    insertStatement.setString(7, onboardingTask.getReferralFile());
    
    return DbHelper.executeUpdate(dbHandle, insertStatement, true);
  }

  /**
   * Get packet details based on packet_id
   * @param onboardingTask
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public JsonArray getTasks(OnboardingTask onboardingTask) throws SQLException, DbHandleException {
    JsonArray resultArray = new JsonArray();
    String taskQuery = "select task_id,task_name,task_description,viewing_required," +
        "allow_doc_upload,doc_upload_required,referral_file from OnboardingTask " +
        "where packet_id = ? and inactive_status = 0";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(taskQuery);
    preparedStatement.setInt(1, onboardingTask.getPacket_id());

    ResultSet resultSet = preparedStatement.executeQuery();
    while (resultSet.next()) {
      JsonObject taskObject = new JsonObject();
      taskObject.put("task_id", resultSet.getInt("task_id"));
      taskObject.put("task_name",
          resultSet.getString("task_name"));
      taskObject.put("task_description",
          resultSet.getString("task_description"));
      taskObject.put("viewing_required",
          resultSet.getBoolean("viewing_required"));
      taskObject.put("allow_doc_upload",
          resultSet.getBoolean("allow_doc_upload"));
      taskObject.put("doc_upload_required",
          resultSet.getBoolean("doc_upload_required"));
      taskObject.put("referral_file",
          resultSet.getString("referral_file"));
      resultArray.add(taskObject);
    }
    return resultArray;
  }

  /**
   * Delete task based on packet_id
   * @param onboardingTask
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public int deletePacket(OnboardingTask onboardingTask) throws SQLException, DbHandleException {
    String deleteQuery = "update OnboardingTask set inactive_status = 1 where packet_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(deleteQuery);
    preparedStatement.setInt(1, onboardingTask.getPacket_id());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  /**
   * Delete task based on task_id
   * @param onboardingTask
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public int deleteTask(OnboardingTask onboardingTask) throws SQLException, DbHandleException {
    String deleteQuery = "update OnboardingTask set inactive_status = 1 where task_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(deleteQuery);
    preparedStatement.setInt(1, onboardingTask.getTask_id());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  /**
   * Update task details based on task_id
   * @param requestParams
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public int updateTask(MultiMap requestParams) throws SQLException, DbHandleException {
    StringBuilder stringBuilder = new StringBuilder();
    int i = 1;
    int length = requestParams.size();
    List<String> listOfParams = new ArrayList<String>();
    for(String key : requestParams.names()) {
      if(!key.equals("taskId")) {
        stringBuilder.append(key + " = ?");
        listOfParams.add(requestParams.get(key));
      }


      if (i < length && i != 1) {
        stringBuilder.append(",");
      }
      i++;
    }

    int columnIndex = 1;
    String parameterizedSql = "update OnboardingTask set " + stringBuilder + " where task_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    for(int index = 0; index < listOfParams.size(); index++) {
      preparedStatement.setString(columnIndex, listOfParams.get(index));
      columnIndex++;
    }
    preparedStatement.setInt(listOfParams.size() + 1,
        Integer.valueOf(requestParams.get("taskId")));

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }
}
