package com.auzmor.impl.dao;

import com.auzmor.dao.RoleDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.Role;
import com.auzmor.util.db.DbHandle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class RoleDAOImpl implements RoleDAO{
  private static final Logger logger = LoggerFactory.getLogger(RoleDAOImpl.class);
  private DbHandle dbHandle;

  public RoleDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  @Override
  public List<Role> getRoles(List<Long> roleIds) throws SQLException, DbHandleException {
    StringBuilder parameterizedSql = new StringBuilder("SELECT `id`, `name`, `organization_id` FROM");
    parameterizedSql.append(" `Role` WHERE `id` in (");
    for (int i = 0; i < roleIds.size(); ++i) {
      if (i != 0) {
        parameterizedSql.append(", ");
      }
      parameterizedSql.append("?");
    }
    parameterizedSql.append(")");

    PreparedStatement preparedStatement = dbHandle.createPreparedStatement(parameterizedSql.toString());
    int index = 0;
    for (Long roleId : roleIds) {
      preparedStatement.setLong(++index, roleId);
    }

    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      Role role = new Role();
      int retrieveIndex = 0;
      role.setId(resultSet.getLong(++retrieveIndex));
      role.setName(resultSet.getString(++retrieveIndex));
      role.setOrganizationId(resultSet.getLong(++retrieveIndex));
      return role;
    });
  }

  @Override
  public Role getRoleByName(String name, long organizationId) throws SQLException, DbHandleException {
    StringBuilder parameterizedSql = new StringBuilder("SELECT `id`, `name`, `organization_id` FROM");
    parameterizedSql.append(" `Role` WHERE `name` = ? and `organization_id` = ?");

    PreparedStatement preparedStatement = dbHandle.createPreparedStatement(parameterizedSql.toString());
    int index = 0;
    preparedStatement.setString(++index, name);
    preparedStatement.setLong(++index, organizationId);

    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      Role role = new Role();
      int retrieveIndex = 0;
      role.setId(resultSet.getLong(++retrieveIndex));
      role.setName(resultSet.getString(++retrieveIndex));
      role.setOrganizationId(resultSet.getLong(++retrieveIndex));
      return role;
    });
  }
}
