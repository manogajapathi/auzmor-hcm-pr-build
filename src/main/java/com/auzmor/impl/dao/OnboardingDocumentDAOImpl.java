package com.auzmor.impl.dao;

import com.auzmor.dao.OnboardingDocumentDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.onboarding.OnboardingDocument;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OnboardingDocumentDAOImpl implements OnboardingDocumentDAO{
  private DbHandle dbHandle;

  public OnboardingDocumentDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  /**
   * To check if document already exists in packet before insert
   * @param onboardingDocument
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public boolean isAlreadyExists(OnboardingDocument onboardingDocument)
      throws SQLException, DbHandleException {
    String parameterizedSql = "select 1 from OnboardingDocuments where packet_id = ? " +
        "and document_name = ?" +
        " and inactive_status = 0";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setInt(1, onboardingDocument.getPacket_id());
    preparedStatement.setString(2, onboardingDocument.getDocument_name());

    return DbHelper.getBoolean(dbHandle, preparedStatement);
  }

  /**
   * Insert row in OnboardingDocuments table. Rows document_name,document_path,packet_id
   * are mandatory
   * @param onboardingDocument
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public int addDocument(OnboardingDocument onboardingDocument)
      throws SQLException, DbHandleException {
    String sqlQuery = "insert into OnboardingDocuments (document_name,document_path,viewing_reqd," +
        "signing_required,page,x_coordinate,y_coordinate,packet_id) values (?,?,?,?,?,?,?,?)";
    PreparedStatement preparedStatement =
        this.dbHandle.createPreparedStatement(sqlQuery, true);
    preparedStatement.setString(1,onboardingDocument.getDocument_name());
    preparedStatement.setString(2,onboardingDocument.getDocument_path());
    preparedStatement.setBoolean(3, onboardingDocument.isViewing_reqd());
    preparedStatement.setBoolean(4, onboardingDocument.isSigningRequired());
    preparedStatement.setInt(5, onboardingDocument.getPage());
    preparedStatement.setDouble(6, onboardingDocument.getxCoordinate());
    preparedStatement.setDouble(7, onboardingDocument.getyCoordinate());
    preparedStatement.setInt(8, onboardingDocument.getPacket_id());
    
    return DbHelper.executeUpdate(dbHandle, preparedStatement, true);
  }

  /**
   * Get document details based on packet_id
   * @param onboardingDocument
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public JsonArray getDocuments(OnboardingDocument onboardingDocument)
      throws SQLException, DbHandleException {
    JsonArray resultArray = new JsonArray();
    String documentQuery = "select document_id,document_name,document_path,viewing_reqd," +
        "signing_required,page,x_coordinate,y_coordinate from OnboardingDocuments " +
        "where packet_id = ? and inactive_status = 0";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(documentQuery);
    preparedStatement.setInt(1, onboardingDocument.getPacket_id());

    ResultSet resultSet = preparedStatement.executeQuery();
    while (resultSet.next()) {
      JsonObject jsonObject = new JsonObject();
      jsonObject.put("document_id", resultSet.getInt("document_id"));
      jsonObject.put("document_name",
          resultSet.getString("document_name"));
      jsonObject.put("document_path",
          resultSet.getString("document_path"));
      jsonObject.put("viewing_reqd",
          resultSet.getBoolean("viewing_reqd"));
      jsonObject.put("signing_required",
          resultSet.getBoolean("signing_required"));
      jsonObject.put("page",
          resultSet.getInt("page"));
      jsonObject.put("x_coordinate",
          resultSet.getDouble("x_coordinate"));
      jsonObject.put("y_coordinate",
          resultSet.getDouble("y_coordinate"));
      resultArray.add(jsonObject);
    }

    if (null != resultSet && !resultSet.isClosed()) {
      resultSet.close();
    }
    return resultArray;
  }

  /**
   * Delete document based on packet_id
   * @param onboardingDocument
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public int deletePacket(OnboardingDocument onboardingDocument)
      throws SQLException, DbHandleException {
    String deleteQuery = "update OnboardingDocuments set inactive_status = 1 where packet_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(deleteQuery);
    preparedStatement.setInt(1, onboardingDocument.getPacket_id());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  /**
   * Delete document based on document_id
   * @param onboardingDocument
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public int deleteDocument(OnboardingDocument onboardingDocument)
      throws SQLException, DbHandleException {
    String deleteQuery = "update OnboardingDocuments set inactive_status = 1 where document_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(deleteQuery);
    preparedStatement.setInt(1, onboardingDocument.getDocument_id());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  /**
   * Update document details based on document_id
   * @param requestParams
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public int updateDocument(MultiMap requestParams) throws SQLException, DbHandleException {
    StringBuilder stringBuilder = new StringBuilder();
    int i = 1;
    int length = requestParams.size();
    List<String> listOfParams = new ArrayList<String>();
    for(String key : requestParams.names()) {
      if(!key.equals("document_id")) {
        stringBuilder.append(key + " = ?");
        listOfParams.add(requestParams.get(key));
      }


      if (i < length && i != 1) {
        stringBuilder.append(",");
      }
      i++;
    }

    int columnIndex = 1;
    String parameterizedSql = "update OnboardingDocuments set " + stringBuilder + " where document_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    for(int index = 0; index < listOfParams.size(); index++) {
      preparedStatement.setString(columnIndex, listOfParams.get(index));
      columnIndex++;
    }
    preparedStatement.setInt(listOfParams.size() + 1,
        Integer.valueOf(requestParams.get("document_id")));

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }
}
