package com.auzmor.impl.dao;

import com.auzmor.dao.CandidateCommentDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.models.CandidateR.CommentCandidate;
import com.auzmor.impl.util.DateUtil;
import com.auzmor.util.db.DbHandle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

public class CandidateCommentDAOImpl implements CandidateCommentDAO {
  private static Logger logger = LoggerFactory.getLogger(CandidateCommentDAOImpl.class);
  private DbHandle dbHandle;
  public CandidateCommentDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  @Override
  public int create(CommentCandidate commentCandidate) throws SQLException, DbHandleException {
    logger.info("Comments are inserting for the candidate id=" + commentCandidate.getCandidateId());
    StringBuilder parameterizedSql = new StringBuilder("insert into CandidateComments(candidate_id,");
    parameterizedSql.append("admin_name,comments,comments_date,user_id,is_systemGenerated) ");
    parameterizedSql.append("values (?, ?, ?, ?, ?, ?)");

    java.sql.Date sqlDate = new java.sql.Date(commentCandidate.getCommentsDate().getTime());

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql.
        toString());
    preparedStatement.setLong(1, commentCandidate.getCandidateId());
    preparedStatement.setString(2, commentCandidate.getAdminName());
    preparedStatement.setString(3, commentCandidate.getComments());
    preparedStatement.setDate(4, sqlDate);
    preparedStatement.setLong(5, commentCandidate.getUserId());
    preparedStatement.setBoolean(6,commentCandidate.isSystemGenerated());
    int result = dbHandle.executeUpdate(preparedStatement);
    dbHandle.closePreparedStatement(preparedStatement);
    logger.info("Comments added Successfully!");
    return result;
  }

  @Override
  public List<HashMap<String, String>> getCommentsDetails(String format, long candidateId)
      throws SQLException, DbHandleException {

    StringBuilder parameterizedSql = new StringBuilder("select comments,created,id,user_id");
    parameterizedSql.append(" from CandidateComments where candidate_id = ? and active_status =1 ");
    parameterizedSql.append(" and is_systemGenerated = 0 order by id desc ");

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql.
        toString());
    preparedStatement.setLong(1, candidateId);
    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      HashMap<String, String> commentCandidateList = new HashMap<>();
      commentCandidateList.put("comments", resultSet.getString("comments"));
      // commentCandidateList.put("admin_name",resultSet.getString("admin_name"));
      commentCandidateList.put("id", resultSet.getString("id"));
      String dateFormat = resultSet.getString("created").substring(0, 10);
      String timeFormat = resultSet.getString("created").substring(11, 16);
      try {

        String commentDate = "";
        // checking date difference
        long getDaysDiff = new DateUtil().DateDifferenceBetweenTwoDates(dateFormat);
        dateFormat = new DateUtil().getDateFormatDB(format, dateFormat);
        if (getDaysDiff > 7) {
          commentDate = dateFormat;
        } else {
          commentDate = dateFormat + " at " + timeFormat;
        }
        commentCandidateList.put("comments_date",commentDate);
        commentCandidateList.put("user_id",resultSet.getString("user_id"));
        logger.info("Comments list fetched Successfully!");
        return  commentCandidateList;

      } catch (ParseException e) {
        e.printStackTrace();
        return commentCandidateList;
      }
    });
  }

  @Override
  public List<HashMap<String, String>> getCommentsDetailsInHistoryTab(String format, long candidateId)
      throws SQLException, DbHandleException {

    StringBuilder parameterizedSql = new StringBuilder("select comments,created,id,user_id");
    parameterizedSql.append(" from CandidateComments where candidate_id = ? and " );
    parameterizedSql.append(" active_status =1 and is_systemGenerated = 1 ");
    parameterizedSql.append(" order by id desc ");

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql.
        toString());
    preparedStatement.setLong(1, candidateId);
    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      HashMap<String, String> commentCandidateList = new HashMap<>();
      commentCandidateList.put("comments", resultSet.getString("comments"));
      // commentCandidateList.put("admin_name",resultSet.getString("admin_name"));
      commentCandidateList.put("id", resultSet.getString("id"));
      String dateFormat = resultSet.getString("created").substring(0, 10);
      String timeFormat = resultSet.getString("created").substring(11, 16);
      try {

        String commentDate = "";
        // checking date difference
        long getDaysDiff = new DateUtil().DateDifferenceBetweenTwoDates(dateFormat);
        dateFormat = new DateUtil().getDateFormatDB(format, dateFormat);
        if (getDaysDiff > 7) {
          commentDate = dateFormat;
        } else {
          commentDate = dateFormat + " at " + timeFormat;
        }
        commentCandidateList.put("comments_date",commentDate);
        commentCandidateList.put("user_id",resultSet.getString("user_id"));
        logger.info("Comments list fetched Successfully!");
        return  commentCandidateList;

      } catch (ParseException e) {
        e.printStackTrace();
        return commentCandidateList;
      }
    });
  }

  @Override
  public int updateComment(CommentCandidate commentCandidate) throws SQLException, DbHandleException {

    StringBuilder parameterizedSql = new StringBuilder("update CandidateComments set comments = ? ");
    parameterizedSql.append(" where candidate_id = ? and id = ? and user_id = ? and active_status =1 ");

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql.
        toString());
    preparedStatement.setString(1, commentCandidate.getComments());
    preparedStatement.setLong(2, commentCandidate.getCandidateId());
    preparedStatement.setLong(3, commentCandidate.getId());
    preparedStatement.setLong(4, commentCandidate.getUserId());
    logger.info("Comments updated Successfully!");
    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  @Override
  public int deleteComment(long candidateId, long userId, long id)
      throws SQLException, DbHandleException {

    StringBuilder parameterizedSql = new StringBuilder("update CandidateComments set active_status = 0");
    parameterizedSql.append(" where candidate_id = ? and id = ? and user_id = ? ");

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql.
        toString());
    preparedStatement.setLong(1, candidateId);
    preparedStatement.setLong(2, id);
    preparedStatement.setLong(3, userId);
    logger.info("Comments deleted Successfully!");
    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  @Override
  public boolean checkUserId(long id, long userId) throws SQLException, DbHandleException {
    StringBuilder parameterizedSql = new StringBuilder("SELECT 1 FROM `CandidateComments` WHERE ");
    parameterizedSql.append("(`id` = ? and `user_id` = ?)");

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql
        .toString());
    int index = 0;
    preparedStatement.setLong(++index, id);
    preparedStatement.setLong(++index, userId);
    logger.info("User Id fetched Successfully!");
    return DbHelper.getBoolean(this.dbHandle, preparedStatement);
  }
}
