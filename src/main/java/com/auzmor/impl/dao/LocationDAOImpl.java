package com.auzmor.impl.dao;

import com.auzmor.bo.LocationBO;
import com.auzmor.dao.LocationDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.LocationModel;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LocationDAOImpl implements LocationDAO {
  private DbHandle dbHandle;

  public LocationDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  /**
   * Insert row in Location table
   * location,organization_id are mandatory
   * @param locationModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public int insert(LocationModel locationModel) throws SQLException,DbHandleException {
     String insertQuery = "insert into Location (location,organization_id,company_address_line1," +
        "company_address_line2,country,state,zip,city,updated_flag,active_status) " +
        "values (?,?,?,?,?,?,?,?,?,?)";

     PreparedStatement preparedStatement =
         this.dbHandle.createPreparedStatement(insertQuery, true);
     preparedStatement.setString(1,locationModel.getLocation());
     preparedStatement.setInt(2,locationModel.getOrganizationId());
     preparedStatement.setString(3,locationModel.getCompanyAddressLine1());
     preparedStatement.setString(4,locationModel.getCompanyAddressLine2());
     preparedStatement.setString(5,locationModel.getCountry());
     preparedStatement.setString(6,locationModel.getState());
     preparedStatement.setString(7,locationModel.getZip());
     preparedStatement.setString(8,locationModel.getCity());
     preparedStatement.setBoolean(9,locationModel.isUpdatedFlag());
     preparedStatement.setString(10,"true");

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, true);
  }

  /**
   * To check if location already exists in particular organization before insert
   * @param locationModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public boolean isLocationExistsInOrganization(LocationModel locationModel)
      throws SQLException,DbHandleException{
    String parameterizedSql  = "select 1 from Location where location = ? and organization_id = ?" +
        "  and active_status = 'true'";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setString(1,locationModel.getLocation());
    preparedStatement.setInt(2,locationModel.getOrganizationId());

    return DbHelper.getBoolean(this.dbHandle, preparedStatement);
  }

  /**
   * Fetch location details based on primary key id
   * @param locationModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public LocationModel getLocationDetails(LocationModel locationModel)
      throws SQLException,DbHandleException {
    String parameterizedSql = "select id,location,organization_id,company_address_line1," +
        "company_address_line2,country,state,zip,city from Location where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setInt(1, locationModel.getId());

    return DbHelper.getObject(this.dbHandle, preparedStatement, resultSet -> {
      locationModel.setId(resultSet.getInt(1));
      locationModel.setLocation(resultSet.getString(2));
      locationModel.setOrganizationId(resultSet.getInt(3));
      locationModel.setCompanyAddressLine1(resultSet.getString(4));
      locationModel.setCompanyAddressLine2(resultSet.getString(5));
      locationModel.setCountry(resultSet.getString(6));
      locationModel.setState(resultSet.getString(7));
      locationModel.setZip(resultSet.getString(8));
      locationModel.setCity(resultSet.getString(9));
      return locationModel;
    });
  }

  /**
   * Fetch all locations based on organization_id
   * @param locationModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public List<LocationModel> getAllLocations(LocationModel locationModel)
    throws SQLException,DbHandleException {
    String parameterizedSql = "select id,location,organization_id,company_address_line1," +
        "company_address_line2,country,state,zip,city from Location where organization_id = ? " +
        "and active_status = 'true'";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setInt(1, locationModel.getOrganizationId());

    return DbHelper.getObjectList(this.dbHandle, preparedStatement, resultSet -> {
      LocationModel dataModel = new LocationModel();
      dataModel.setId(resultSet.getInt(1));
      dataModel.setLocation(resultSet.getString(2));
      dataModel.setOrganizationId(resultSet.getInt(3));
      dataModel.setCompanyAddressLine1(resultSet.getString(4));
      dataModel.setCompanyAddressLine2(resultSet.getString(5));
      dataModel.setCountry(resultSet.getString(6));
      dataModel.setState(resultSet.getString(7));
      dataModel.setZip(resultSet.getString(8));
      dataModel.setCity(resultSet.getString(9));
      return dataModel;
    });
  }

  /**
   * Update Location details based on primary key id
   * @param requestParams
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public int updateLocationDetails(MultiMap requestParams) throws SQLException,
      DbHandleException {
    StringBuilder stringBuilder = new StringBuilder();
    int i = 1;
    int length = requestParams.size();
    List<String> listOfParams = new ArrayList<String>();
    for(String key : requestParams.names()) {
      if(!key.equals("id")) {
        stringBuilder.append(key + " = ?");
        listOfParams.add(requestParams.get(key));
      }
      if (i < length && i != 1) {
        stringBuilder.append(",");
      }
      i++;
    }

    int columnIndex = 1;
    String parameterizedSql = "update Location set " + stringBuilder + " where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    for(int index = 0; index < listOfParams.size(); index++) {
      preparedStatement.setString(columnIndex, listOfParams.get(index));
      columnIndex++;
    }
    preparedStatement.setInt(listOfParams.size() + 1,Integer.valueOf(requestParams.get("id")));

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  /**
   * Delete location based on primary key id
   * @param locationModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public int deleteLocation(LocationModel locationModel) throws SQLException,
      DbHandleException {
    String paramaterizedSql = "update Location set active_status = 'false' where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(paramaterizedSql);
    preparedStatement.setInt(1,locationModel.getId());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  @Override
  public String getAddress(String locationName, long organizationId) throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder("Select company_address_line1 from Location " +
        " where location = ? and organization_id = ? and active_status = 'true' ");
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setString(1,locationName);
    preparedStatement.setLong(2,organizationId);
    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      String companyAddress = "";
      companyAddress = resultSet.getString(1);
      return companyAddress;
    });
  }
}
