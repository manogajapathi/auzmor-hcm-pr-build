package com.auzmor.impl.dao;

import com.auzmor.dao.MinimumExperienceDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.FieldOptions.MinimumExperienceModel;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.json.JsonObject;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class MinimumExperienceDAOImpl implements MinimumExperienceDAO {
  private DbHandle dbHandle;

  public MinimumExperienceDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  @Override
  public int insert(MinimumExperienceModel minimumExperienceModel) 
      throws SQLException, DbHandleException {
    String insertQuery = "insert into Minimum_Experience (minimum_experience,organization_id) " +
        "values (?,?)";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(insertQuery, true);
    preparedStatement.setString(1, minimumExperienceModel.getMinimumExperience());
    preparedStatement.setLong(2, minimumExperienceModel.getOrganizationId());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, true);
  }

  @Override
  public boolean isAlreadyMinimumExperience(MinimumExperienceModel minimumExperienceModel)
      throws SQLException, DbHandleException {
    String selectQuery = "select 1 from Minimum_Experience where minimum_experience = ? and " +
        "organization_id = ? and active_status = 1";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setString(1, minimumExperienceModel.getMinimumExperience());
    preparedStatement.setLong(2, minimumExperienceModel.getOrganizationId());

    return DbHelper.getBoolean(dbHandle, preparedStatement);
  }
  
  @Override
  public JsonObject getMinimumExperience(MinimumExperienceModel minimumExperienceModel)
      throws SQLException, DbHandleException {
    String selectQuery = "select id,minimum_experience,organization_id from Minimum_Experience " +
        "where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setLong(1, minimumExperienceModel.getId());

    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      JsonObject dataObject = new JsonObject();
      dataObject.put("id", resultSet.getLong(1));
      dataObject.put("minimum_experience", resultSet.getString(2));
      dataObject.put("organization_id", resultSet.getLong(3));
      return dataObject;
    });
  }
  
  @Override
  public List<JsonObject> getAllMinimumExperience(MinimumExperienceModel minimumExperienceModel)
      throws SQLException, DbHandleException {
    String selectQuery = "select id,minimum_experience,organization_id from Minimum_Experience " +
        "where organization_id = ? and active_status = 1";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setLong(1, minimumExperienceModel.getOrganizationId());

    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      JsonObject dataObject = new JsonObject();
      dataObject.put("id", resultSet.getLong(1));
      dataObject.put("minimum_experience", resultSet.getString(2));
      dataObject.put("organization_id", resultSet.getLong(3));
      return dataObject;
    });
  }
  
  @Override
  public int update(MinimumExperienceModel minimumExperienceModel)
    throws SQLException, DbHandleException {
    String updateQuery = "update Minimum_Experience set minimum_experience =? where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(updateQuery);
    preparedStatement.setString(1, minimumExperienceModel.getMinimumExperience());
    preparedStatement.setLong(2, minimumExperienceModel.getId());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  @Override
  public int delete(MinimumExperienceModel minimumExperienceModel)
      throws SQLException, DbHandleException {
    String updateQuery = "update Minimum_Experience set active_status = 0 where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(updateQuery);
    preparedStatement.setLong(1, minimumExperienceModel.getId());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }
}
