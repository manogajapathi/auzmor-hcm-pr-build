package com.auzmor.impl.dao;

import com.auzmor.dao.CandidateOfferDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.models.CandidateR.OfferProcessCandidate;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.util.db.DbHandle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public class CandidateOfferDAOImpl implements CandidateOfferDAO {
  private static Logger logger = LoggerFactory.getLogger(CandidateOfferDAOImpl.class);

  private DbHandle dbHandle;

  public CandidateOfferDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }


  @Override
  public int create(OfferProcessCandidate offerProcessCandidate) throws SQLException, DbHandleException {
    logger.info("Offer process Details are Inserting for the candidate id=" + offerProcessCandidate.getCandidateId());
    StringBuilder parameterizedSql = new StringBuilder("INSERT INTO OfferProcessCandidate ");
    parameterizedSql.append(" (offer_start_date, offer_pay_rate, offer_position,offer_job_location, ");
    parameterizedSql.append(" offer_contract_details,offer_attachment_name,subject_name,candidate_id, ");
    parameterizedSql.append(" admin_name,offer_mail_description,user_id,currency_type,sub_status) ");
    parameterizedSql.append(" VALUES (?, ?, ?,?,?,?,?,?,?,?,?,?,?)");

    int index = 0;
    java.sql.Date sqlDate = new java.sql.Date(offerProcessCandidate.getOfferStartDate().getTime());
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql
        .toString(), true);
    preparedStatement.setDate(++index, sqlDate);
    preparedStatement.setString(++index, offerProcessCandidate.getOfferPayRate());
    preparedStatement.setString(++index, offerProcessCandidate.getOfferPosition());
    preparedStatement.setString(++index, offerProcessCandidate.getOfferJobLocation());
    preparedStatement.setString(++index, offerProcessCandidate.getOfferContractDetails());
    preparedStatement.setString(++index, offerProcessCandidate.getOfferAttachmentName());
    preparedStatement.setString(++index, offerProcessCandidate.getSubjectName());
    preparedStatement.setLong(++index, offerProcessCandidate.getCandidateId());
    preparedStatement.setString(++index, offerProcessCandidate.getAdminName());
    preparedStatement.setString(++index, offerProcessCandidate.getOfferMailDescription());
    preparedStatement.setLong(++index, offerProcessCandidate.getUserId());
    preparedStatement.setString(++index, offerProcessCandidate.getCurrencyType());
    preparedStatement.setString(++index,offerProcessCandidate.getSubStatus());

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, true);
  }

  @Override
  public OfferProcessCandidate getOfferProcessDetails(long candidateId)
      throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder("select offer_start_date,offer_position,offer_pay_rate,");
    query.append(" offer_job_location,offer_contract_details,offer_attachment_name,subject_name,");
    query.append(" admin_name,offer_decline_reason,status,user_id,currency_type,sub_status,created,modified");
    query.append(" from OfferProcessCandidate where candidate_id = ? ");
    query.append(" order by id desc limit 1");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, candidateId);
    return DbHelper.getObject(this.dbHandle, preparedStatement, resultSet -> {
      OfferProcessCandidate offerProcessCandidate = new OfferProcessCandidate();
      int retrieveIndex = 0;
      offerProcessCandidate.setOfferStartDate(resultSet.getDate(++retrieveIndex));
      offerProcessCandidate.setOfferPosition(resultSet.getString(++retrieveIndex));
      offerProcessCandidate.setOfferPayRate(resultSet.getString(++retrieveIndex));
      offerProcessCandidate.setOfferJobLocation(resultSet.getString(++retrieveIndex));
      offerProcessCandidate.setOfferContractDetails(StringUtil.printValidString(resultSet.getString(++retrieveIndex)));
      offerProcessCandidate.setOfferAttachmentName(resultSet.getString(++retrieveIndex));
      offerProcessCandidate.setSubjectName(resultSet.getString(++retrieveIndex));
      offerProcessCandidate.setAdminName(resultSet.getString(++retrieveIndex));
      offerProcessCandidate.setOfferDeclineReason(StringUtil.printValidString(resultSet.getString(++retrieveIndex)));
      offerProcessCandidate.setStatus(StringUtil.printValidString(resultSet.getString(++retrieveIndex)));
      offerProcessCandidate.setUserId(resultSet.getLong(++retrieveIndex));
      offerProcessCandidate.setCurrencyType(resultSet.getString(++retrieveIndex));
      offerProcessCandidate.setSubStatus(resultSet.getString(++retrieveIndex));
      offerProcessCandidate.setCreated(resultSet.getDate(++retrieveIndex));
      if (!StringUtil.printValidString(String.valueOf(resultSet.getDate("modified"))).equals("")) {
        offerProcessCandidate.setModified(resultSet.getDate("modified"));
      }


      return offerProcessCandidate;
    });

  }

  @Override
  public int updateCancelStatus(String cancelReason, String status, long candidateId, long userId)
      throws SQLException, DbHandleException {
    logger.info("update cancel status");
    StringBuilder query = new StringBuilder("update OfferProcessCandidate set ");
    query.append(" offer_decline_reason = ?,status = ?,user_id= ? ");
    query.append(" where candidate_id = ? and active_status= 1 order by id desc limit 1");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setString(++index, cancelReason);
    preparedStatement.setString(++index, status);
    preparedStatement.setLong(++index, userId);
    preparedStatement.setLong(++index, candidateId);
    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  @Override
  public int updateStatus(String status, long candidateId) throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder(" update OfferProcessCandidate set status = ? ");
    query.append(" where candidate_id = ? and active_status= 1 ");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setString(++index, status);
    preparedStatement.setLong(++index, candidateId);
    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  @Override
  public int updateOfferDetails(OfferProcessCandidate offerProcessCandidate)
      throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder("update OfferProcessCandidate set");
    query.append(" offer_start_date= ?,offer_position=?,offer_job_location=?,offer_contract_details=?,");
    query.append(" offer_attachment_name=?,offer_pay_rate=?,subject_name=?,admin_name=?,");
    query.append(" offer_mail_description=?,user_id =?,currency_type = ?,status=? ");
    query.append(" where candidate_id = ? and active_status= 1 and sub_status = ? ");
    int index = 0;
    java.sql.Date sqlDate = new java.sql.Date(offerProcessCandidate.getOfferStartDate().getTime());
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query
        .toString(), true);
    preparedStatement.setDate(++index, sqlDate);
    preparedStatement.setString(++index, offerProcessCandidate.getOfferPosition());
    preparedStatement.setString(++index, offerProcessCandidate.getOfferJobLocation());
    preparedStatement.setString(++index, offerProcessCandidate.getOfferContractDetails());
    preparedStatement.setString(++index, offerProcessCandidate.getOfferAttachmentName());
    preparedStatement.setString(++index, offerProcessCandidate.getOfferPayRate());
    preparedStatement.setString(++index, offerProcessCandidate.getSubjectName());
    preparedStatement.setString(++index, offerProcessCandidate.getAdminName());
    preparedStatement.setString(++index, offerProcessCandidate.getOfferMailDescription());
    preparedStatement.setLong(++index, offerProcessCandidate.getUserId());
    preparedStatement.setString(++index, offerProcessCandidate.getCurrencyType());
    preparedStatement.setString(++index, StringUtil.printValidString(offerProcessCandidate.getStatus()));
    preparedStatement.setLong(++index, offerProcessCandidate.getCandidateId());
    preparedStatement.setString(++index,offerProcessCandidate.getSubStatus());

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, true);
  }


  @Override
  public List<HashMap<String, String>> getOfferMailList(long candidateId)
      throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder("select subject_name,admin_name,");
    query.append("offer_mail_description,user_id,created,modified ");
    query.append(" from OfferProcessCandidate ");
    query.append(" where candidate_id= ? and offer_mail_description != '' ");
    query.append(" and active_status =1  order by created desc");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.
        createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, candidateId);
    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      HashMap<String, String> getMailList = new HashMap<>();
      int retrieveIndex = 0;
      getMailList.put("subject_name", resultSet.getString(++retrieveIndex));
      getMailList.put("admin_name", resultSet.getString(++retrieveIndex));
      getMailList.put("mail_description", resultSet.getString(++retrieveIndex));
      getMailList.put("user_id", resultSet.getString(++retrieveIndex));

      String date = resultSet.getString(++retrieveIndex);
      String dateFormat = date.substring(0, 10);
      String timeFormat = date.substring(11, 16);
      String createdDate = dateFormat + " at " + timeFormat;

      getMailList.put("created_date", createdDate);

      return getMailList;
    });
  }


  @Override
  public int updateActiveStatus(long candidateId) throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder("update OfferProcessCandidate set active_status = 0 ");
    query.append(" where candidate_id =? and active_status =1");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.
        toString());
    preparedStatement.setLong(++index, candidateId);
    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }


}
