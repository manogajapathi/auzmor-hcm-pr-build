package com.auzmor.impl.dao;

import com.auzmor.dao.OrganizationDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.OrganizationR;
import com.auzmor.impl.model.Role;
import com.auzmor.util.db.DbHandle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class OrganizationDAOImpl implements OrganizationDAO {
  private static Logger logger = LoggerFactory.getLogger(OrganizationDAOImpl.class);
  private DbHandle dbHandle;


  public OrganizationDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  /**
   * @param organizationId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Charles Sam Dilip
   * @lastModifiedDate 21/FEB/2018
   */
  @Override
  public Organization getOrganizationById(long organizationId) throws SQLException,
      DbHandleException {
    logger.info("get the organization details for id=" + organizationId);
    StringBuilder parameterizedSql = new StringBuilder("SELECT `organization_id`, `admin_email`, ");
    parameterizedSql.append("`career_url`, `hris_url`, `first_name`, `last_name`, `password`, `logo_image`, ");
    parameterizedSql.append("`organization_name`, `domain`, `address`, `domain_name`, `ein`, ");
    parameterizedSql.append("`file_size`, `phone_no`, `company_address_line1`, ");
    parameterizedSql.append("`company_address_line2`, `city`, `state`, `zip_code`, `country`, ");
    parameterizedSql.append("`billing_credit_card`, `billing_expiry_date`, `billing_cvv`, ");
    parameterizedSql.append("`billing_contact_person1`, `billing_contact_person2`, ");
    parameterizedSql.append("`billing_contact_person3`, `billing_city`, `billing_state`, `billing_country`, ");
    parameterizedSql.append("`billing_zip_code`, `created_date`, `billing_address_line1`, ");
    parameterizedSql.append("`billing_address_line2`, `modified_date`, `account_owner_email`, ");
    parameterizedSql.append("`active_status`, `employee_profile_image`, `date_format`, `name_format`,");
    parameterizedSql.append("`default_currency`, `default_country`, `favicon`, `logo`, ");
    parameterizedSql.append("`website_url`, `timezone_format`, `created`, `modified` FROM `Organization`");
    parameterizedSql.append(" WHERE organization_id = ?");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql
        .toString());
    preparedStatement.setLong(++index, organizationId);
    return DbHelper.getObject(this.dbHandle, preparedStatement, resultSet -> {
      Organization organization = new Organization();
      organization.setOrganization_id(resultSet.getInt(1));
      organization.setAdmin_email(resultSet.getString(2));
      organization.setCareer_url(resultSet.getString(3));
      organization.setHris_url(resultSet.getString(4));
      // TODO fill the values
      //organization.setLogoI(resultSet.getString(8));
      organization.setOrganization_name(resultSet.getString(9));
      // TODO fill the values
      organization.setAccount_owner_email(resultSet.getString(36));
      // TODO fill the values
      organization.setDate_format(resultSet.getString(39));
      organization.setName_format(resultSet.getString(40));
      organization.setDefault_currency(resultSet.getString(41));
      organization.setDefault_country(resultSet.getString(42));
      organization.setFavicon(resultSet.getString(43));
      organization.setLogo(resultSet.getString(44));
      // TODO fill the values\
      organization.setTimezone_format(resultSet.getString(46));
      organization.setCreated(resultSet.getDate(47));
      organization.setModified(resultSet.getDate(48));

      return organization;
    });
  }


  /**
   * @param domainName
   * @return
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public Organization getOrganizationByDomainName(String domainName) throws SQLException,
      DbHandleException {

    StringBuilder parameterizedSql = new StringBuilder("SELECT `organization_id`, `admin_email`, ");
    parameterizedSql.append("`career_url`, `hris_url`, `first_name`, `last_name`, `password`, `logo_image`, ");
    parameterizedSql.append("`organization_name`, `domain`, `address`, `domain_name`, `ein`, ");
    parameterizedSql.append("`file_size`, `phone_no`, `company_address_line1`, ");
    parameterizedSql.append("`company_address_line2`, `city`, `state`, `zip_code`, `country`, ");
    parameterizedSql.append("`billing_credit_card`, `billing_expiry_date`, `billing_cvv`, ");
    parameterizedSql.append("`billing_contact_person1`, `billing_contact_person2`, ");
    parameterizedSql.append("`billing_contact_person3`, `billing_city`, `billing_state`, `billing_country`, ");
    parameterizedSql.append("`billing_zip_code`, `created_date`, `billing_address_line1`, ");
    parameterizedSql.append("`billing_address_line2`, `modified_date`, `account_owner_email`, ");
    parameterizedSql.append("`active_status`, `employee_profile_image`, `date_format`, `name_format`,");
    parameterizedSql.append("`default_currency`, `default_country`, `favicon`, `logo`, ");
    parameterizedSql.append("`website_url`, `timezone_format`, `account_owner_name`, ");
    parameterizedSql.append("`number_format`, `created`, `modified` FROM `Organization` WHERE ");
    parameterizedSql.append("`hris_url` = ? or `career_url` = ?");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql
        .toString());
    preparedStatement.setString(++index, domainName);
    preparedStatement.setString(++index, domainName);
    return DbHelper.getObject(this.dbHandle, preparedStatement, resultSet -> {
      Organization organization = new Organization();

      organization.setOrganization_id(resultSet.getInt(1));
      organization.setAdmin_email(resultSet.getString(2));
      organization.setCareer_url(resultSet.getString(3));
      organization.setHris_url(resultSet.getString(4));
      organization.setFirst_name(resultSet.getString(5));
      organization.setLast_name(resultSet.getString(6));
      // TODO fill the values
      //organization.setLogoI(resultSet.getString(8));
      organization.setOrganization_name(resultSet.getString(9));
      // TODO fill the values
      organization.setPhone_no(resultSet.getString(15));
      // TODO fill the values
      organization.setAccount_owner_email(resultSet.getString(36));
      // TODO fill the values
      organization.setDate_format(resultSet.getString(39));
      organization.setName_format(resultSet.getString(40));
      organization.setDefault_currency(resultSet.getString(41));
      organization.setDefault_country(resultSet.getString(42));
      organization.setFavicon(resultSet.getString(43));
      organization.setLogo(resultSet.getString(44));
      // TODO fill the values
      organization.setTimezone_format(resultSet.getString(46));
      organization.setAccount_owner_name(resultSet.getString(47));
      organization.setNumber_format(resultSet.getString(48));
      organization.setCreated(resultSet.getDate(49));
      organization.setModified(resultSet.getDate(50));

      return organization;
    });
  }

  @Override
  public boolean isDomainRegistered(String domain) throws SQLException, DbHandleException {
    StringBuilder parameterizedSql = new StringBuilder("SELECT 1 FROM `Organization` WHERE ");
    parameterizedSql.append("(`hris_url` = ? or `career_url` = ?)");

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql
        .toString());
    int index = 0;
    preparedStatement.setString(++index, domain);
    preparedStatement.setString(++index, domain);

    return DbHelper.getBoolean(this.dbHandle, preparedStatement);
  }

  @Override
  public int updateDomains(String atsDomain, String careerDomain, long organizationId)
      throws DbHandleException, SQLException {
    StringBuilder parameterizedSql = new StringBuilder("UPDATE `Organization` SET `hris_url` = ?,");
    parameterizedSql.append(" `career_url` = ? WHERE `organization_id` = ?");

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql
        .toString());
    int index = 0;
    preparedStatement.setString(++index, atsDomain);
    preparedStatement.setString(++index, careerDomain);
    preparedStatement.setLong(++index, organizationId);

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  @Override
  public Organization getOrganizationDetails(String hrisUrl) throws SQLException, DbHandleException {
    logger.info("getting the organization details for " + hrisUrl);
    String selectQuery = "select organization_id,organization_name,logo " +
        "from Organization where hris_url = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setString(1, hrisUrl);

    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      Organization organization = new Organization();
      organization.setOrganization_id(resultSet.getInt(1));
      organization.setOrganization_name(resultSet.getString(2));
      organization.setLogo(resultSet.getString(3));
      return organization;
    });
  }

  @Override
  public int addOrganizationDetails(OrganizationR organization)
      throws SQLException, DbHandleException {
    return 0;
  }

  @Override
  public List<Role> getRoles(long organizationId) throws SQLException, DbHandleException {
    String query = "Select `id`, `name` from `Role` where `organization_id` = ?";

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query);
    preparedStatement.setLong(1, organizationId);

    return DbHelper.getObjectList(this.dbHandle, preparedStatement, resultSet -> {
      Role role = new Role();
      role.setId(resultSet.getLong(1));
      role.setName(resultSet.getString(2));
      role.setOrganizationId(organizationId);
      return role;
    });
  }
}
