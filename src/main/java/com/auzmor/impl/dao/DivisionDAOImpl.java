package com.auzmor.impl.dao;

import com.auzmor.dao.DivisionDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.FieldOptions.DivisionModel;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DivisionDAOImpl implements DivisionDAO {
  private DbHandle dbHandle;

  public DivisionDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  /**
   * Insert into Division table. Rows division_name,organization_id are mandatory
   * @param divisionModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  @Override
  public int insert(DivisionModel divisionModel) throws SQLException,DbHandleException {
    String insertQuery = "insert into Division (division_name,organization_id,active_status) " +
        "values (?,?,?)";

    PreparedStatement preparedStatement =
        this.dbHandle.createPreparedStatement(insertQuery, true);
    preparedStatement.setString(1, divisionModel.getDivisionName());
    preparedStatement.setInt(2, divisionModel.getOrganizationId());
    preparedStatement.setString(3, "true");

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, true);
  }

  /**
   * To check if division already exists in organization before insert
   * @param divisionModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  @Override
  public boolean isDivisionExistsInOrganization(DivisionModel divisionModel)
      throws SQLException,DbHandleException{
    String parameterizedSql  = "select 1 from Division where division_name = ? and organization_id = ?" +
        "  and active_status = 'true'";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setString(1, divisionModel.getDivisionName());
    preparedStatement.setInt(2, divisionModel.getOrganizationId());

    return DbHelper.getBoolean(this.dbHandle,preparedStatement);
  }

  /**
   * Fetch division details based on id
   * @param divisionModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  @Override
  public DivisionModel getDivisionDetails(DivisionModel divisionModel)
      throws SQLException,DbHandleException {
    String parameterizedSql = "select division_id,division_name,organization_id from Division where division_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setInt(1, divisionModel.getDivisionId());

    return DbHelper.getObject(this.dbHandle, preparedStatement, resultSet -> {
      divisionModel.setDivisionId(resultSet.getInt(1));
      divisionModel.setDivisionName(resultSet.getString(2));
      divisionModel.setOrganizationId(resultSet.getInt(3));
      return divisionModel;
    });
  }

  /**
   * Fetch all divisions in an organization
   * @param divisionModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  @Override
  public List<DivisionModel> getAllDivisions(DivisionModel divisionModel)
      throws SQLException,DbHandleException {
    String parameterizedSql = "select division_id,division_name,organization_id from Division " +
        "where organization_id = ? " +
        "and active_status = 'true'";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setInt(1, divisionModel.getOrganizationId());

    return DbHelper.getObjectList(this.dbHandle, preparedStatement, resultSet -> {
      divisionModel.setDivisionId(resultSet.getInt(1));
      divisionModel.setDivisionName(resultSet.getString(2));
      divisionModel.setOrganizationId(resultSet.getInt(3));
      return divisionModel;
    });
  }

  /**
   * Update divsion details based on id
   * @param requestParams
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  @Override
  public int updateDivisionDetails(MultiMap requestParams) throws SQLException,
      DbHandleException {
    /*StringBuilder stringBuilder = new StringBuilder();
    int i = 1;
    int length = requestParams.size();
    List<String> listOfParams = new ArrayList<String>();
    for(String key : requestParams.names()) {
      if(!key.equals("divisionId")) {
        stringBuilder.append(key  + " = ?");
        listOfParams.add(requestParams.get(key));
      }


      if (i < length && i != 1) {
        stringBuilder.append(",");
      }
      i++;
    }*/

    int columnIndex = 1;
    String parameterizedSql = "update Division set division_name = ? where division_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setString(columnIndex, requestParams.get("division_name"));
    columnIndex++;

    preparedStatement.setInt(columnIndex,
        Integer.valueOf(requestParams.get("divisionId")));

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  /**
   * Delete division based on id
   * @param divisionModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  @Override
  public int deleteDivision(DivisionModel divisionModel) throws SQLException,
      DbHandleException {
    String paramaterizedSql = "update Division set active_status = 'false' where division_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(paramaterizedSql);
    preparedStatement.setInt(1, divisionModel.getDivisionId());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }
}
