package com.auzmor.impl.dao;

import com.auzmor.dao.DepartmentDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.DepartmentModel;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DepartmentDAOImpl implements DepartmentDAO {
  private DbHandle dbHandle;

  public DepartmentDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  /**
   * Insert into Department table. Rows department, organization_id are mandatory
   * @param departmentModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  @Override
  public int insert(DepartmentModel departmentModel) throws SQLException,DbHandleException {
    String insertQuery = "insert into Department (department,organization_id,active_status) " +
        "values (?,?,?)";

    PreparedStatement preparedStatement =
        this.dbHandle.createPreparedStatement(insertQuery, true);
    preparedStatement.setString(1,departmentModel.getDepartment());
    preparedStatement.setInt(2,departmentModel.getOrganizationId());
    preparedStatement.setString(3,"true");

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, true);
  }

  /**
   * To check if department already exists in organization_id
   * @param departmentModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  @Override
  public boolean isDepartmentExistsInOrganization(DepartmentModel departmentModel)
      throws SQLException,DbHandleException{
    String parameterizedSql  = "select 1 from Department where department = ? and organization_id = ?" +
        "  and active_status = 'true'";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setString(1,departmentModel.getDepartment());
    preparedStatement.setInt(2,departmentModel.getOrganizationId());

    return DbHelper.getBoolean(this.dbHandle,preparedStatement);
  }

  /**
   * Fetch department details based on primary key id
   * @param departmentModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  @Override
  public DepartmentModel getDepartmentDetails(DepartmentModel departmentModel)
      throws SQLException,DbHandleException {
    String parameterizedSql = "select id,department,organization_id from Department where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setInt(1, departmentModel.getId());

    return DbHelper.getObject(this.dbHandle,preparedStatement, resultSet -> {
      departmentModel.setId(resultSet.getInt(1));
      departmentModel.setDepartment(resultSet.getString(2));
      departmentModel.setOrganizationId(resultSet.getInt(3));
      return departmentModel;
    });
  }

  /**
   * Fetch all department in an organization
   * @param departmentModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  @Override
  public List<DepartmentModel> getAllDepartments(DepartmentModel departmentModel)
      throws SQLException,DbHandleException {
    String parameterizedSql = "select id,department,organization_id from Department " +
        "where organization_id = ? " +
        "and active_status = 'true'";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setInt(1, departmentModel.getOrganizationId());

    return DbHelper.getObjectList(this.dbHandle,preparedStatement, resultSet -> {
      departmentModel.setId(resultSet.getInt(1));
      departmentModel.setDepartment(resultSet.getString(2));
      departmentModel.setOrganizationId(resultSet.getInt(3));
      return departmentModel;
    });
  }

  /**
   * Update department details based on primary key id
   * @param requestParams
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  @Override
  public int updateDepartmentDetails(MultiMap requestParams) throws SQLException,
      DbHandleException {
    /*StringBuilder stringBuilder = new StringBuilder();
    int i = 1;
    int length = requestParams.size();
    List<String> listOfParams = new ArrayList<String>();
    for(String key : requestParams.names()) {
      if(!key.equals("id")) {
        stringBuilder.append(key + " = ?");
        listOfParams.add(requestParams.get(key));
      }


      if (i < length && i != 1) {
        stringBuilder.append(",");
      }
      i++;
    }*/

    int columnIndex = 1;
    String parameterizedSql = "update Department set department = ? where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setString(1,requestParams.get("department"));
    /*preparedStatement.setString(columnIndex,listOfParams.get(index));
    for(int index = 0; index < listOfParams.size(); index++) {
      preparedStatement.setString(columnIndex,listOfParams.get(index));
      columnIndex++;
    }*/
    preparedStatement.setInt(2,Integer.valueOf(requestParams.get("id")));

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  /**
   * Delete department based on primary key id
   * @param departmentModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  @Override
  public int deleteDepartment(DepartmentModel departmentModel) throws SQLException,
      DbHandleException {
    String paramaterizedSql = "update Department set active_status = 'false' where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(paramaterizedSql);
    preparedStatement.setInt(1,departmentModel.getId());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }
}
