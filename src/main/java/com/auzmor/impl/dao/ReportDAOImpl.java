package com.auzmor.impl.dao;

import com.auzmor.dao.ReportDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ReportDAOImpl implements ReportDAO {

  private DbHandle dbHandle;

  public ReportDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }


  /**
   * getJobReports - Get Job Reports Data Access Object
   * @param job
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Balasubramanian
   * @lastModifiedDate 07/MAR/2018
   */
  @Override
  public List<JsonObject> getJobReports(Job job) throws SQLException, DbHandleException {
    StringBuilder parameterizedSql = new StringBuilder("select count(job_status), " +
        "job_status from Job");
    parameterizedSql.append(" where job_status is not null");
    parameterizedSql.append(" and organization_id = ?");
    parameterizedSql.append(" group by job_status order by job_status");

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(
        parameterizedSql.toString());
    preparedStatement.setInt(1, job.getOrganization_id());

    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      JsonObject object = new JsonObject();
      object.put("label",resultSet.getString("job_status"));
      object.put("count",resultSet.getString("count(job_status)"));
      return object;
    });
  }

  /**
   * getCandidateStatusReports - get Candidate Status Reports Data Access Object
   * @param candidateModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Balasubramanian
   * @lastModifiedDate 07/MAR/2018
   */
  @Override
  public List<JsonObject> getCandidateStatusReports(CandidateR candidateModel)
      throws SQLException, DbHandleException {
    StringBuilder parameterizedSql = new StringBuilder();
    parameterizedSql.append("select count(status),status from Candidate ");
    parameterizedSql.append("where status is not null ");
    parameterizedSql.append("and active_status = ? and organization_id = ? ");
    parameterizedSql.append("group by status");

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(
        parameterizedSql.toString());
    preparedStatement.setInt(1, 1);
    preparedStatement.setLong(2, candidateModel.getOrganizationId());

    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      JsonObject object = new JsonObject();
      String label = resultSet.getString("status");
      object.put("label", Character.toUpperCase(label.charAt(0)) + label.substring(1));
      object.put("count", resultSet.getString("count(status)"));
      return object;
    });
  }
}
