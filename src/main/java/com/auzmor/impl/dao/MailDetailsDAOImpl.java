package com.auzmor.impl.dao;

import com.auzmor.bo.EmployeeBO;
import com.auzmor.dao.MailDetailsDAO;
import com.auzmor.impl.bo.EmployeeBOImpl;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.EmailFormat;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.impl.models.CandidateR.MailDetails;
import com.auzmor.util.db.DbHandle;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public class MailDetailsDAOImpl implements MailDetailsDAO {
  private DbHandle dbHandle;

  public MailDetailsDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }
  @Override
  public int create(MailDetails mailDetails) throws SQLException, DbHandleException {

    StringBuilder parameterizedSql = new StringBuilder("Insert into MailDetails ");
    parameterizedSql.append("(candidate_id,user_id,subject_name,attachment_name,mail_description) ");
    parameterizedSql.append(" VALUES (?,?,?,?,?)");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.
        createPreparedStatement(parameterizedSql.toString(),true);
    preparedStatement.setLong(++index, mailDetails.getCandidateId());
    preparedStatement.setLong(++index, mailDetails.getUserId());
   // preparedStatement.setString(++index, mailDetails.getAdminName());
    preparedStatement.setString(++index,mailDetails.getSubjectName());
    preparedStatement.setString(++index, mailDetails.getAttachmentName());
    preparedStatement.setString(++index, mailDetails.getMailDescription());
    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, true);
  }

  @Override
  public List<HashMap<String, String>> getMailDetails(long candidateId)
      throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder("select user_id,subject_name,attachment_name,mail_description,created " +
        " from MailDetails where candidate_id= ?");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.
        createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, candidateId);
    return DbHelper.getObjectList(dbHandle,preparedStatement, resultSet -> {
      HashMap<String,String> getMailList = new HashMap<>();
      int retrieveIndex = 0;
      getMailList.put("user_id",resultSet.getString(++retrieveIndex));
      getMailList.put("subject_name",resultSet.getString(++retrieveIndex));
      getMailList.put("attachment_name",resultSet.getString(++retrieveIndex));
      getMailList.put("mail_description",resultSet.getString(++retrieveIndex));

      String date = resultSet.getString(++retrieveIndex);
      String dateFormat = date.substring(0, 10);
      String timeFormat = date.substring(11, 16);
      String createdDate = dateFormat + " at " + timeFormat;

      getMailList.put("created_date",createdDate);

      return getMailList;
    });
  }

  @Override
  public int updateActiveStatus(long candidateId) throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder("update MailDetails set active_status = 0 ");
    query.append(" where candidate_id =? and active_status =1" );
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.
        toString());
    preparedStatement.setLong(++index, candidateId);
    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  @Override
  public int insertRemainder(EmailFormat emailFormat, String ccAddress)
      throws SQLException, DbHandleException {
    String insertQuery = "insert into MailDetails" +
        "(form_address,to_address,cc_address,subject_name,mail_description) " +
        " values(?,?,?,?,?)";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(insertQuery);
    preparedStatement.setString(1, emailFormat.getFrom_address());
    preparedStatement.setString(2, emailFormat.getTo_address());
    preparedStatement.setString(3, ccAddress);
    preparedStatement.setString(4, emailFormat.getSubject_name());
    preparedStatement.setString(5, emailFormat.getMail_description());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

}
