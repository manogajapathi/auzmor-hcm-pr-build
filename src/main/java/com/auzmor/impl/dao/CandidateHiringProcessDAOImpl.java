package com.auzmor.impl.dao;

import com.auzmor.dao.CandidateHiringDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.models.CandidateR.HiringProcessCandidate;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.util.db.DbHandle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;


public class CandidateHiringProcessDAOImpl implements CandidateHiringDAO {
  private static final Logger logger = LoggerFactory.getLogger(CandidateHiringProcessDAOImpl.class);
  private DbHandle dbHandle;



  public CandidateHiringProcessDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  public int create(HiringProcessCandidate candidateHiringProcessModel)
      throws SQLException, DbHandleException {
    logger.info("Created Hiring process for the candidate id" + candidateHiringProcessModel.getCandidateId());
    StringBuilder parameterizedSql = new StringBuilder("insert into HiringprocessCandidate(status,");
    parameterizedSql.append("candidate_process,candidate_id,admin_name,reason,user_id,other_cancel_reason)");
    parameterizedSql.append(" values (?, ?, ?, ?, ?, ?, ?)");

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql.
        toString());
    preparedStatement.setString(1, candidateHiringProcessModel.getStatus());
    preparedStatement.setString(2, candidateHiringProcessModel.getCandidateProcess());
    preparedStatement.setLong(3, candidateHiringProcessModel.getCandidateId());
    preparedStatement.setString(4, candidateHiringProcessModel.getAdminName());
    preparedStatement.setString(5, StringUtil.printValidString(candidateHiringProcessModel.getReason()));
    preparedStatement.setLong(6, candidateHiringProcessModel.getUserId());
    preparedStatement.setString(7, candidateHiringProcessModel.getOtherCancelReason());

    int result = dbHandle.executeUpdate(preparedStatement);
    dbHandle.closePreparedStatement(preparedStatement);
    logger.info("Candidate Created Successfully!");
    return result;

  }

  @Override
  public HiringProcessCandidate getHiringProcess(long candidateId)
      throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder("select candidate_process,status,admin_name,reason,user_id ");
    query.append(" from HiringprocessCandidate where candidate_id =? order by hiring_id desc limit 1");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.
        toString());
    preparedStatement.setLong(++index, candidateId);
    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      HiringProcessCandidate hiringProcessCandidate = new HiringProcessCandidate();
      hiringProcessCandidate.setCandidateProcess(resultSet.getString("candidate_process"));
      hiringProcessCandidate.setStatus(resultSet.getString("status"));
      hiringProcessCandidate.setAdminName(resultSet.getString("admin_name"));
      hiringProcessCandidate.setReason(resultSet.getString("reason"));
      hiringProcessCandidate.setUserId(resultSet.getLong("user_id"));
      logger.info("Hiring Process fetched Successfully!");
      return hiringProcessCandidate;
    });
  }

  @Override
  public boolean getStatusCheck(String status, String statusReason, long candidateId)
      throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder("Select 1 from HiringprocessCandidate");
    query.append(" where candidate_id =? and status = ? and candidate_process = ? ");
    query.append(" and active_status = 1 ");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.
        toString());
    preparedStatement.setLong(++index, candidateId);
    preparedStatement.setString(++index, status);
    preparedStatement.setString(++index, statusReason);
    logger.info("Status Check fetched Successfully!");
    return DbHelper.getBoolean(this.dbHandle, preparedStatement);
  }

  @Override
  public String getReasonCheck(String status, String statusValue, long id)
      throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder("Select reason from HiringprocessCandidate");
    query.append(" where candidate_id =? and status = ? and candidate_process = ? ");
    query.append(" and active_status = 1 ");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.
        toString());
    preparedStatement.setLong(++index, id);
    preparedStatement.setString(++index, status);
    preparedStatement.setString(++index, statusValue);
    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      HiringProcessCandidate hiringProcessCandidate = new HiringProcessCandidate();
      hiringProcessCandidate.setReason(resultSet.getString(1));
      logger.info("Reason fetched Successfully!");
      return  hiringProcessCandidate.getReason();

    });
  }

  @Override
  public String getCandidateProcess(long candidateId, String status)
      throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder("Select candidate_process from HiringprocessCandidate");
    query.append(" where candidate_id =? and status = ? and ");
    query.append(" active_status =1 order by hiring_id desc limit 1");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.
        toString());
    preparedStatement.setLong(++index, candidateId);
    preparedStatement.setString(++index, status);
    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      HiringProcessCandidate hiringProcessCandidate = new HiringProcessCandidate();
      hiringProcessCandidate.setCandidateProcess(resultSet.getString(1));
      logger.info("Candidate Process fetched Successfully!");
      return  hiringProcessCandidate.getCandidateProcess();

    });
  }

  @Override
  public int updateStatus(long candidateId) throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder("update HiringprocessCandidate set active_status = 0 ");
    query.append(" where candidate_id =? and active_status =1");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.
        toString());
    preparedStatement.setLong(++index, candidateId);
    logger.info("Candidate status updated Successfully!");
    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }


}
