package com.auzmor.impl.dao;

import com.auzmor.dao.OnboardingFormDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.onboarding.OnboardingForm;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OnboardingFormDAOImpl implements OnboardingFormDAO {

  private DbHandle dbHandle;

  public OnboardingFormDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }
  @Override
  public boolean isAlreadyExists(OnboardingForm onboardingForm)
      throws SQLException, DbHandleException{
    String parameterizedSql = "select 1 from OnboardingForm where packet_id = ? and form_name = ? " +
        "and inactive_status = 0";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setInt(1, onboardingForm.getPacket_id());
    preparedStatement.setString(2, onboardingForm.getForm_name());
    
    return DbHelper.getBoolean(dbHandle, preparedStatement);
  }
  
  @Override
  public int addForm(OnboardingForm onboardingForm) throws SQLException, DbHandleException {
    String insertQuery = "insert into OnboardingForm (form_name, form_required, packet_id,is_mandatory) " +
        "values (?,?,?,?)";
    PreparedStatement preparedStatement =
        this.dbHandle.createPreparedStatement(insertQuery, true);
    preparedStatement.setString(1, onboardingForm.getForm_name());
    preparedStatement.setBoolean(2, onboardingForm.isForm_required());
    preparedStatement.setInt(3, onboardingForm.getPacket_id());
    preparedStatement.setBoolean(4, true);

    return DbHelper.executeUpdate(dbHandle, preparedStatement, true);
  }

  @Override
  public JsonObject getForm(OnboardingForm onboardingForm) throws SQLException, DbHandleException {
    JsonObject jsonObject = new JsonObject();
    String parameterizedSql =  "select form_id,form_name,form_required,is_mandatory " +
        "from OnboardingForm where form_name = ? and packet_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setString(1, onboardingForm.getForm_name());
    preparedStatement.setInt(2, onboardingForm.getPacket_id());

    ResultSet resultSet = preparedStatement.executeQuery();
    while (resultSet.next()) {
      jsonObject.put("form_id", resultSet.getInt("form_id"));
      jsonObject.put("form_name",
          resultSet.getString("form_name"));
      jsonObject.put("form_required",
          resultSet.getBoolean("form_required"));
      jsonObject.put("is_mandatory",
          resultSet.getBoolean("is_mandatory"));
    }
    return jsonObject;
  }

  @Override
  public List<String> getAvailableForms(OnboardingForm onboardingForm)
      throws SQLException, DbHandleException {
    String formQuery = "select form_name from AuzmorForms where form_name in ( select form_name " +
        "from OnboardingForm where inactive_status = 0 and packet_id = ? )";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(formQuery);
    preparedStatement.setInt(1, onboardingForm.getPacket_id());

    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      return resultSet.getString("form_name");
    });
  }

  @Override
  public int deletePacket(OnboardingForm onboardingForm) throws SQLException, DbHandleException {
    String deleteQuery = "update OnboardingForm set inactive_status = 1 where packet_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(deleteQuery);
    preparedStatement.setInt(1, onboardingForm.getPacket_id());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  @Override
  public int deleteForm(OnboardingForm onboardingForm) throws SQLException, DbHandleException {
    String deleteQuery = "update OnboardingForm set inactive_status = 1 where form_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(deleteQuery);
    preparedStatement.setInt(1, onboardingForm.getForm_id());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  @Override
  public int updateForm(MultiMap requestParams) throws SQLException, DbHandleException {
    StringBuilder stringBuilder = new StringBuilder();
    int i = 1;
    int length = requestParams.size();
    List<String> listOfParams = new ArrayList<String>();
    for(String key : requestParams.names()) {
      if(!key.equals("form_id")) {
        stringBuilder.append(key + " = ?");
        listOfParams.add(requestParams.get(key));
      }


      if (i < length && i != 1) {
        stringBuilder.append(",");
      }
      i++;
    }

    int columnIndex = 1;
    String parameterizedSql = "update OnboardingForm set " + stringBuilder + " where form_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    for(int index = 0; index < listOfParams.size(); index++) {
      preparedStatement.setString(columnIndex, listOfParams.get(index));
      columnIndex++;
    }
    preparedStatement.setInt(listOfParams.size() + 1,
        Integer.valueOf(requestParams.get("form_id")));

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  @Override
  public JsonArray getAuzmorForms(MultiMap params) throws SQLException, DbHandleException {
    int packetId = Integer.valueOf(params.get("packet_id"));
    String formQuery = "select form_name from AuzmorForms where form_name not in " +
        "(select form_name from OnboardingForm where packet_id = ? " +
        "and inactive_status = 0)";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(formQuery);
    preparedStatement.setInt(1, packetId);

    JsonArray resultArray = new JsonArray();
    ResultSet resultSet = preparedStatement.executeQuery();
    while (resultSet.next()) {
      resultArray.add(resultSet.getString("form_name"));
    }

    if (null != resultSet) {
      resultSet.close();
    }

    dbHandle.closePreparedStatement(preparedStatement);
    return resultArray;
  }
}
