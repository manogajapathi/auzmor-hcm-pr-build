package com.auzmor.impl.dao;

import com.auzmor.dao.TitleDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.TitleModel;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class TitleDAOImpl implements TitleDAO {
  private DbHandle dbHandle;
  private static Logger logger = LoggerFactory.getLogger(TitleDAOImpl.class);
  public TitleDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  /**
   * Insert row in Title table.Rows title,organization_id are mandatory
   * @param titleModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public int insert(TitleModel titleModel) throws SQLException,DbHandleException {
    logger.info("Name of the Title to be inserted :"+titleModel.getTitle());
    String insertQuery = "insert into Title (title,organization_id,active_status) " +
        "values (?,?,?)";

    PreparedStatement preparedStatement =
        this.dbHandle.createPreparedStatement(insertQuery, true);
    preparedStatement.setString(1, titleModel.getTitle());
    preparedStatement.setInt(2, titleModel.getOrganizationId());
    preparedStatement.setString(3, "true");

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, true);
  }

  /**
   * To check if title already exists in organization before insert
   * @param titleModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public boolean isTitleExistsInOrganization(TitleModel titleModel)
      throws SQLException,DbHandleException{
    logger.info("Available Title :"+titleModel.getTitle());
    String parameterizedSql  = "select 1 from Title where title = ? and organization_id = ?" +
        " and active_status = 'true'";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setString(1,titleModel.getTitle());
    preparedStatement.setInt(2,titleModel.getOrganizationId());

    return DbHelper.getBoolean(this.dbHandle, preparedStatement);
  }

  /**
   * Fetch title details based on primary key id
   * @param titleModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public TitleModel getTitleDetails(TitleModel titleModel)
      throws SQLException,DbHandleException {
    logger.info("Details of the :"+titleModel.getId());
    String parameterizedSql = "select id,title,organization_id from Title where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setInt(1, titleModel.getId());

    return DbHelper.getObject(this.dbHandle, preparedStatement, resultSet -> {
      titleModel.setId(resultSet.getInt(1));
      titleModel.setTitle(resultSet.getString(2));
      titleModel.setOrganizationId(resultSet.getInt(3));
      return titleModel;
    });
  }

  /**
   * Fetch all titles in an organization_id
   * @param titleModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public List<TitleModel> getAllTitles(TitleModel titleModel)
      throws SQLException,DbHandleException {
    logger.info("Available Title :"+titleModel.getOrganizationId());
    String parameterizedSql = "select id,title,organization_id from Title where organization_id = ? " +
        "and active_status = 'true'";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setInt(1, titleModel.getOrganizationId());

    return DbHelper.getObjectList(this.dbHandle, preparedStatement, resultSet -> {
      TitleModel resultModel = new TitleModel();
      resultModel.setId(resultSet.getInt(1));
      resultModel.setTitle(resultSet.getString(2));
      resultModel.setOrganizationId(resultSet.getInt(3));
      return resultModel;
    });
  }

  /**
   * Update title details based on primary key id
   * @param requestParams
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public int updateTitleDetails(MultiMap requestParams) throws SQLException,
      DbHandleException {
    logger.info("Update title for :"+requestParams.get("id"));
    StringBuilder stringBuilder = new StringBuilder();
    int i = 1;
    int length = requestParams.size();
    List<String> listOfParams = new ArrayList<String>();
    for(String key:requestParams.names()) {
      if(!key.equals("id")) {
        stringBuilder.append(key + " = ?");
        listOfParams.add(requestParams.get(key));
      }

      if (i < length && i != 1) {
        stringBuilder.append(",");
      }
      i++;
    }

    int columnIndex = 1;
    String parameterizedSql = "update Title set " + stringBuilder + " where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    for(int index = 0; index < listOfParams.size(); index++) {

      preparedStatement.setString(columnIndex, listOfParams.get(index));
      columnIndex++;
    }
    preparedStatement.setInt(listOfParams.size() + 1, Integer.valueOf(requestParams.get("id")));

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  /**
   * Delete title based on primary key id
   * @param titleModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public int deleteTitle(TitleModel titleModel) throws SQLException,
      DbHandleException {
    logger.info("Delete Title :"+titleModel.getId());
    String paramaterizedSql = "update Title set active_status = 'false' where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(paramaterizedSql);
    preparedStatement.setInt(1, titleModel.getId());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }
}
