package com.auzmor.impl.dao;

import com.auzmor.dao.JobDAO;
import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.Job.Collaborator;
import com.auzmor.impl.model.Job.CustomQuestion;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.util.DateUtil;
import com.auzmor.impl.util.Mail;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class JobDAOImpl implements JobDAO {
  private static Logger logger = LoggerFactory.getLogger(JobDAOImpl.class);

  private DbHandle dbHandle;

  public JobDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }


  /**
   * moveCandidatesToTalentPool is a DAO Implementation method
   * used to move not hired candidates to talent pool
   * when the job is closed.
   *
   * @param id
   * @param creatorId
   * @throws SQLException
   * @author Pradeep Balasubramanian
   */
  @Override
  public void moveCandidatesToTalentPool(int id, int creatorId,String organizationName)
      throws SQLException, DbHandleException {


    StringBuilder parameterizedSql = new StringBuilder();
    parameterizedSql.append("select candidate_id,status,email,first_name,last_name from Candidate ");
    parameterizedSql.append(" where job_id = ? and status != ? and active_status = ? ");
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(
        parameterizedSql.toString());
    preparedStatement.setInt(1, id);
    preparedStatement.setString(2, "Hire");
    preparedStatement.setInt(3, 1);

    ResultSet candidateResultSet = preparedStatement.executeQuery();

    while (candidateResultSet.next()) {

      int candidateId = candidateResultSet.getInt("candidate_id");
      StringBuilder updateStatusQuery = new StringBuilder();
      updateStatusQuery.append("update Candidate set status = ? ");
      updateStatusQuery.append(" where candidate_id = ?");

      PreparedStatement updateStatusPreparedStatement = this.dbHandle.createPreparedStatement(
          updateStatusQuery.toString());
      updateStatusPreparedStatement.setString(1, "Not Hired");
      updateStatusPreparedStatement.setInt(2, candidateId);
      int updateStatusChanges = updateStatusPreparedStatement.executeUpdate();

      String candidateStatus = candidateResultSet.getString("status");
      StringBuilder candidateHiringQuery = new StringBuilder();
      candidateHiringQuery.append("insert into HiringprocessCandidate (");
      candidateHiringQuery.append("status,candidate_process,candidate_id,admin_name,");
      candidateHiringQuery.append("active_status, user_id) values");
      candidateHiringQuery.append("(?,?,?,?,?,?)");
      String adminName = getNameBasedOnEmployeeId(creatorId);

      PreparedStatement hiringProcessPreparedStatement = this.dbHandle.createPreparedStatement(
          candidateHiringQuery.toString());
      hiringProcessPreparedStatement.setString(1, "Not Hired");
      hiringProcessPreparedStatement.setString(2, "Archive to Talent Pool");
      hiringProcessPreparedStatement.setInt(3, candidateId);
      hiringProcessPreparedStatement.setString(4, adminName);
      hiringProcessPreparedStatement.setInt(5, 1);
      hiringProcessPreparedStatement.setInt(6, creatorId);
      int hiringProcessChanges = hiringProcessPreparedStatement.executeUpdate();

      StringBuilder candidateCommentsQuery = new StringBuilder();
      candidateCommentsQuery.append("insert into CandidateComments ( ");
      candidateCommentsQuery.append("admin_name,comments,candidate_id,comments_date,user_id) values");
      candidateCommentsQuery.append("(?,?,?,?,?)");

      PreparedStatement candidateCommentsStatement = this.dbHandle.createPreparedStatement(
          candidateCommentsQuery.toString());
      candidateCommentsStatement.setString(1, adminName);
      candidateCommentsStatement.setString(2, "moved from " + candidateStatus +
          " to talent pool (Job is Closed) ");
      candidateCommentsStatement.setInt(3, candidateId);

      SimpleDateFormat formatter = new DateUtil().getDate();
      String currentDate = formatter.format(new Date());
      Date commentsDate = new Date();
      try {
        commentsDate = formatter.parse(currentDate);
      } catch (ParseException e) {
        e.printStackTrace();
      }

      candidateCommentsStatement.setDate(4, new java.sql.Date(commentsDate.getTime()));
      candidateCommentsStatement.setInt(5, creatorId);
      int candidateCommentsAffectedRows = candidateCommentsStatement.executeUpdate();

      String jobTitle = getTitleBasedOnId(id);
      String email = candidateResultSet.getString("email");
      String firstName = candidateResultSet.getString("first_name");
      String lastName = candidateResultSet.getString("last_name");
      String emailBody = "<html><body>Hi " + firstName + " " + lastName + ", " +
          "<br/> The job post titled " + getTitleBasedOnId(id) + " has been closed. <br/>" +
          "Your application has been moved to Talent Pool <br/>" +
          "Thank you,<br/>" + organizationName + " Team "+
          "--------------------------------------------------------------\n<br/>" +
          "This is system generated mail,please do not reply to this mail\n<br/>" +
          "-------------------------------------------------------------- " +
          "</body></html>";

      Mail mail = new Mail();
      String jobClosedInfoStatus = mail.sendMail(Vertx.vertx(),
          emailBody,
          email, "Job Post Application for '" + jobTitle + "'");

    }
  }

  @Override
  public String getNameBasedOnEmployeeId(int employeeId)
      throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder();
    query.append("select concat(first_name, ' ',last_name) as admin_name from Employee ");
    query.append("where employee_id = ?");

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setInt(1, employeeId);

    ResultSet adminNameResultSet = preparedStatement.executeQuery();
    String adminName = "";
    while (adminNameResultSet.next()) {
      adminName = adminNameResultSet.getString("admin_name");
    }
    return adminName;
  }

  /* @Override
   public List<HashMap<String, String>> getJobStatus(String status,long jobId,long organizationId)
       throws SQLException, DbHandleException {

     StringBuilder query = new StringBuilder("select  Any_value (HiringprocessCandidate.candidate_process) ");
         query.append(" as status,count(HiringprocessCandidate.candidate_process) as count " );
         query.append(" from HiringprocessCandidate,Job,Candidate " );
         query.append(" where job_id = Job.id and Candidate.candidate_id = HiringprocessCandidate.candidate_id and " );
         query.append(" HiringprocessCandidate.status=? and ");
         query.append(" Candidate.job_id = ? and Candidate.active_status=1 and Job.organization_id = ? " );
         query.append(" group by HiringprocessCandidate.candidate_process");

         Connection connectionObject = null;
         try {
           connectionObject = new MysqldbConf().getConnection();
         } catch (ClassNotFoundException e) {
           e.printStackTrace();
         }

         int index=0;
         PreparedStatement preparedStatement = connectionObject.prepareStatement(query.toString());
         preparedStatement.setString(++index,status);
         preparedStatement.setLong(++index,jobId);
         preparedStatement.setLong(++index,organizationId);

         List<HashMap<String,String>> jobStatus = new ArrayList<>();

         ResultSet resultSet = preparedStatement.executeQuery();
         while (resultSet.next()){
           int retrieveIndex = 0;
           HashMap<String,String> statusList = new HashMap<>();
           statusList.put(resultSet.getString(++retrieveIndex),resultSet.getString(++retrieveIndex));
           jobStatus.add(statusList);
         }

         connectionObject.close();
        *//* return DbHelper.getObjectList(this.dbHandle,preparedStatement,resultSet -> {
          int retrieveIndex = 0;
          HashMap<String,String> statusList = new HashMap<>();
          statusList.put(resultSet.getString(++retrieveIndex),resultSet.getString(++retrieveIndex));
          return  statusList;
        });*//*

       return jobStatus;

}*/
  @Override
  public JsonArray getJobStatus(String status, long jobId, long organizationId)
      throws SQLException, DbHandleException {

    /* StringBuilder query = new StringBuilder("select  Any_value (HiringprocessCandidate.candidate_process) ");
       query.append(" as status,count(HiringprocessCandidate.candidate_process) as count " );
       query.append(" from HiringprocessCandidate,Job,Candidate " );
       query.append(" where job_id = Job.id and Candidate.candidate_id = HiringprocessCandidate.candidate_id and " );
       query.append(" HiringprocessCandidate.status=? and ");
       query.append(" Candidate.job_id = ? and Candidate.active_status=1 and Job.organization_id = ? " );
       query.append(" group by Candidate.candidate_id")*/
    ;
    StringBuilder query = new StringBuilder("select Any_value(status) as status,count(status) as cnt,candidate_id " +
        "from Candidate,Job where job_id=id and id =? and Job.organization_id = ? and " +
        " Job.organization_id = Candidate.organization_id " +
        " and status = ? and Candidate.active_status = 1" +
        " group by Candidate.candidate_id");
    Connection connectionObject = null;
    try {
      connectionObject = new MysqldbConf().getConnection();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    int index = 0;
    PreparedStatement preparedStatement = connectionObject.prepareStatement(query.toString());
    preparedStatement.setLong(++index, jobId);
    preparedStatement.setLong(++index, organizationId);
    preparedStatement.setString(++index, status);
    JsonArray jobStatus = new JsonArray();
    ResultSet resultSet = preparedStatement.executeQuery();
    while (resultSet.next()) {
      int indexSub = 0;
      StringBuilder subQuery = new StringBuilder("select Any_value(candidate_process),count(candidate_id) " +
          "from HiringprocessCandidate where status = ? and candidate_id=? and active_status = 1 group by candidate_id");
      PreparedStatement preparedStatementSubQuery = connectionObject.prepareStatement(subQuery.toString());
      preparedStatementSubQuery.setString(++indexSub, resultSet.getString("status"));
      preparedStatementSubQuery.setLong(++indexSub, resultSet.getInt("candidate_id"));
      ResultSet resultSetSubQuery = preparedStatementSubQuery.executeQuery();
      // sub domain query for candidate
      while (resultSetSubQuery.next()) {
        int retrieveIndexSub = 0;
        JsonObject jsonObject = new JsonObject();
        // jsonObject.put(resultSetSubQuery.getString(++retrieveIndexSub), resultSetSubQuery.getString(++retrieveIndexSub));                jsonObject.put("hiringstatus", resultSetSubQuery.getString(++retrieveIndexSub));
        jsonObject.put("hiringcount", resultSetSubQuery.getString(++retrieveIndexSub));
        jobStatus.add(jsonObject);
      }

      resultSetSubQuery.close();
      preparedStatementSubQuery.close();
      // total array count            //statusObject.put(status+"status",jobStatus);
      // statusObject.put(status+"Count",resultSet.getString("cnt"));
    }
    
    resultSet.close();
    preparedStatement.close();
    connectionObject.close();
      /* return DbHelper.getObjectList(this.dbHandle,preparedStatement,resultSet -> {
         int retrieveIndex = 0;
         HashMap<String,String> statusList = new HashMap<>();
         statusList.put(resultSet.getString(++retrieveIndex),resultSet.getString(++retrieveIndex));
         return  statusList;
       });*/
    return jobStatus;
  }

  /**
   * getJobStatus is a DAO Implementation Class used to retreive the available job statuses
   *
   * @param constraintString
   * @return
   * @throws SQLException
   */

  public JsonArray getJobStatus(String constraintString) throws SQLException {

    JsonArray jsonArray = new JsonArray();

    StringBuilder parameterizedSql = new StringBuilder();
    parameterizedSql.append("select distinct(job_status) as job_statuses from Job ");

    if (StringUtil.printValidString(constraintString).length() > 0) {
      parameterizedSql.append(constraintString);
    }

    Connection connection = DbHelper.getConnection(false);
    PreparedStatement preparedStatement = connection.prepareStatement(parameterizedSql.toString());
    ResultSet jobStatusResultSet = preparedStatement.executeQuery();

    while (jobStatusResultSet.next()) {
      jsonArray.add(jobStatusResultSet.getString(1));
    }

    jobStatusResultSet.close();
    preparedStatement.close();
    connection.close();

    return jsonArray;
  }


  public List<CustomQuestion> getJobId(long jobId) throws SQLException, DbHandleException {

    StringBuilder parameterizedSql = new StringBuilder("select job_id,custom_answer,custom_knockout,");
    parameterizedSql.append(" custom_question from CustomQuestionJob where job_id= ? ");

    PreparedStatement preparedStatement = dbHandle.createPreparedStatement(parameterizedSql.toString());
    preparedStatement.setLong(1, jobId);
    List<CustomQuestion> customQuestions = dbHandle.getSerializedObject(preparedStatement, resultSet -> {
      CustomQuestion customQuestion = new CustomQuestion();
      customQuestion.setJobId(resultSet.getInt(1));
      customQuestion.setCustomAnswer(resultSet.getString(2));
      customQuestion.setCustomQuestion(resultSet.getString(4));
      customQuestion.setCustomKnockout(resultSet.getBoolean(3));
      return customQuestion;
    });
    if (customQuestions.size() > 0) return (List<CustomQuestion>) customQuestions.get(0);
    else return customQuestions;
  }

  @Override
  public List<HashMap<String, String>> getJobHiringDetails(long candidateId)
      throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder("SELECT applicant,screening,offer,interview,hire,not_hired ");
    query.append(" FROM Job,Candidate where id = job_id and candidate_id= ? ");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.
        createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, candidateId);

    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      HashMap<String, String> getHiringList = new HashMap<>();
      int retrieveIndex = 0;
      getHiringList.put("applicant", resultSet.getString(++retrieveIndex));
      getHiringList.put("screening", StringUtil.printValidString(resultSet.getString(++retrieveIndex)));
      getHiringList.put("offer", resultSet.getString(++retrieveIndex));
      getHiringList.put("interview", resultSet.getString(++retrieveIndex));
      getHiringList.put("hire", resultSet.getString(++retrieveIndex));
      getHiringList.put("not_hired", resultSet.getString(++retrieveIndex));
      return getHiringList;
    });
  }

  @Override
  public Job getJobDetails(long jobId) throws SQLException, DbHandleException {
    logger.info("getting the job details for id=" + jobId);
    StringBuilder query = new StringBuilder("Select title,location,employment_type,");
    query.append(" minimum_experience,department,job_status,hiring_lead,organization_id,job_creator_id from Job where id = ? ");
    query.append(" and job_status != ? ");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.
        createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, jobId);
    preparedStatement.setString(++index, "Closed");
    Job job = new Job();
    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      int retrieveIndex = 0;
      job.setTitle(StringUtil.printValidString(resultSet.getString(++retrieveIndex)));
      job.setLocation(StringUtil.printValidString(resultSet.getString(++retrieveIndex)));
      job.setEmployment_type(StringUtil.printValidString(resultSet.getString(++retrieveIndex)));
      job.setMinimum_experience(StringUtil.printValidString(resultSet.getString(++retrieveIndex)));
      job.setDepartment(StringUtil.printValidString(resultSet.getString(++retrieveIndex)));
      job.setJob_status(StringUtil.printValidString(resultSet.getString(++retrieveIndex)));
      job.setHiring_lead(StringUtil.printValidString(resultSet.getString(++retrieveIndex)));
      job.setOrganization_id(Integer.parseInt(resultSet.getString(++retrieveIndex)));
      job.setJob_creator_id(resultSet.getInt(++retrieveIndex));
      return job;
    });
  }


  @Override
  public Job getJobDetailsWithoutStatus(long jobId) throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder("Select title,location,employment_type,");
    query.append(" minimum_experience,department,job_status,state from Job where id = ? ");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.
        createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, jobId);
    Job job = new Job();
    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      int retrieveIndex = 0;
      job.setTitle(resultSet.getString(++retrieveIndex));
      job.setLocation(StringUtil.printValidString(resultSet.getString(++retrieveIndex)));
      job.setEmployment_type(StringUtil.printValidString(resultSet.getString(++retrieveIndex)));
      job.setMinimum_experience(StringUtil.printValidString(resultSet.getString(++retrieveIndex)));
      job.setDepartment(StringUtil.printValidString(resultSet.getString(++retrieveIndex)));
      job.setJob_status(StringUtil.printValidString(resultSet.getString(++retrieveIndex)));
      job.setState(StringUtil.printValidString(resultSet.getString(++retrieveIndex)));
      return job;
    });
  }

  @Override
  public long getJobCount(long organizationId) throws SQLException, DbHandleException {
    logger.info("getting job count for organization id=" + organizationId);
    StringBuilder query = new StringBuilder("select count(1) from Job where organization_id = ?");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.
        createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, organizationId);
    Job job = new Job();
    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      int retrieveIndex = 0;
      return resultSet.getLong(++retrieveIndex);
    });
  }

  /**
   * isCollaboratorAvailable - DAO Impl class for checking collaborator availability
   *
   * @param collaboratorId
   * @return
   * @throws SQLException
   * @author Pradeep Balasubramanian
   */
  @Override
  public boolean isCollaboratorAvailable(String collaboratorId, int jobId)
      throws SQLException, DbHandleException {

    StringBuilder availableCollaboratorQuery = new StringBuilder();
    availableCollaboratorQuery.append("select collaborator ");
    availableCollaboratorQuery.append("from Collaborators where collaborator = ? and job_id = ?");
    PreparedStatement collabAvailableStatement = this.dbHandle.createPreparedStatement(
        availableCollaboratorQuery.toString());
    collabAvailableStatement.setString(1, collaboratorId);
    collabAvailableStatement.setInt(2, jobId);

    ResultSet collabAvailableResultSet = collabAvailableStatement.executeQuery();
    boolean collaboratorAvailable = false;

    while (collabAvailableResultSet.next()) {
      collaboratorAvailable = true;
      break;
    }

    return collaboratorAvailable;
  }

  /**
   * updateCollaborator - DAO Impl class to update collaborator details
   *
   * @param id
   * @param collaborator
   * @param collabEmailUpdate
   * @throws SQLException
   */
  @Override
  public void updateCollaborator(int id, String collaborator,
                                 Boolean collabEmailUpdate) throws SQLException, DbHandleException {

    StringBuilder updateCollabQueryBuilder = new StringBuilder();
    updateCollabQueryBuilder.append("update Collaborators set collaborator = ?, ");
    updateCollabQueryBuilder.append("collab_email_update = ?, active_status = ? ");
    updateCollabQueryBuilder.append(" where collaborator = ? and job_id = ? ");

    PreparedStatement collabUpdateStatement = this.dbHandle.createPreparedStatement(
        updateCollabQueryBuilder.toString());
    collabUpdateStatement.setString(1, collaborator);
    collabUpdateStatement.setBoolean(2, collabEmailUpdate);
    collabUpdateStatement.setInt(3, 1);
    collabUpdateStatement.setString(4, collaborator);
    collabUpdateStatement.setInt(5, id);

    collabUpdateStatement.executeUpdate();

  }

  /**
   * insertCollaborator - DAO Impl class to insert Collaborator Details
   *
   * @param id
   * @param collaborator
   * @param collabEmailUpdate
   * @throws SQLException
   */
  @Override
  public void insertCollaborator(int id, String collaborator, Boolean collabEmailUpdate)
      throws SQLException, DbHandleException {

    StringBuilder collabInsertBuilder = new StringBuilder();
    collabInsertBuilder.append("insert into Collaborators");
    collabInsertBuilder.append("(collaborator,collab_email_update,job_id) ");
    collabInsertBuilder.append("values (?,?,?)");
    PreparedStatement collabPreparedStatement = this.dbHandle.createPreparedStatement(
        collabInsertBuilder.toString());
    collabPreparedStatement.setString(1, collaborator);
    collabPreparedStatement.setBoolean(2, collabEmailUpdate);
    collabPreparedStatement.setInt(3, id);
    collabPreparedStatement.executeUpdate();

  }

  /**
   * getHiringLeadName - getHiringLeadName based on Hiring Lead Id
   *
   * @param hiringLeadId
   * @return
   * @throws SQLException
   */
  @Override
  public JsonObject getHiringLeadName(String hiringLeadId) throws SQLException {

    String query = "select first_name,last_name,work_email from Employee where employee_id = ?";
    String firstName = "", lastName = "", workEmail = "";
    JsonObject jsonObject = new JsonObject();
    if (StringUtil.isNumber(hiringLeadId)) {
      Connection connection = DbHelper.getConnection(false);
      PreparedStatement preparedStatement = connection.prepareStatement(query);
      preparedStatement.setInt(1, Integer.valueOf(hiringLeadId));

      ResultSet resultSet = preparedStatement.executeQuery();
      while (resultSet.next()) {
        firstName = resultSet.getString("first_name");
        lastName = resultSet.getString("last_name");
        workEmail = resultSet.getString("work_email");
      }

      resultSet.close();
      preparedStatement.close();
      connection.close();

    }

    jsonObject.put("employee_id", StringUtil.isNumber(hiringLeadId)
        ? Integer.valueOf(hiringLeadId) : "");
    jsonObject.put("first_name", firstName);
    jsonObject.put("last_name", lastName);
    jsonObject.put("work_email", workEmail);

    return jsonObject;
  }

  /**
   * getTitleBasedOnId get job title based on job id
   *
   * @param jobId
   * @return
   * @throws SQLException
   */
  @Override
  public String getTitleBasedOnId(long jobId) throws SQLException {

    Connection connection = DbHelper.getConnection(false);
    StringBuilder query = new StringBuilder("Select title from Job where id = ? ");
    int index = 0;
    PreparedStatement preparedStatement = connection.prepareStatement(query.toString());
    preparedStatement.setLong(++index, jobId);
    ResultSet resultSet = preparedStatement.executeQuery();

    String title = "";

    while (resultSet.next()) {
      title = resultSet.getString("title");
      break;
    }

    resultSet.close();
    preparedStatement.close();
    connection.close();

    return title;
  }

  @Override
  public List<HashMap<String, String>> getJobWithCandidateLoginId(long candidateLoginId,
                                                                  long organizationId)
      throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder(" SELECT id,title,location FROM Job" +
        " where id not in (select job_id from Candidate where user_id = ? and active_status = 1 )" +
        " and job_status in ('Published','Unpublished') and organization_id = ?");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.
        createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, candidateLoginId);
    preparedStatement.setLong(++index, organizationId);
    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      HashMap<String, String> getJobList = new HashMap<>();
      int retrieveIndex = 0;
      getJobList.put("id", resultSet.getString(++retrieveIndex));
      getJobList.put("title", resultSet.getString(++retrieveIndex));
      getJobList.put("location", resultSet.getString(++retrieveIndex));
      return getJobList;
    });
  }

  /**
   * getCollaboratorsWithEmailUpdates get collaborator information for collaborators
   * who require email updates
   *
   * @param candidateId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public List<Employee> getCollaboratorsWithEmailUpdates(long candidateId)
      throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder();
    query.append("select employee_id,first_name,last_name,work_email,organization_id");
    query.append(" from Employee where employee_id in ");
    query.append("(select collaborator from Collaborators ");
    query.append("where collab_email_update = true and active_status = 1 and job_id in ");
    query.append("(select job_id from Candidate where candidate_id = ?))");

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(1, candidateId);
    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      Employee employee = new Employee();
      employee.setEmployee_id(resultSet.getInt("employee_id"));
      employee.setFirst_name(resultSet.getString("first_name"));
      employee.setLast_name(resultSet.getString("last_name"));
      employee.setWork_email(resultSet.getString("work_email"));
      employee.setOrganization_id(resultSet.getInt("organization_id"));
      return employee;
    });
  }


  /**
   * getRemovableCollaboratorList - identify removed collaborators
   *
   * @param id
   * @param availableCollaborators
   * @return
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public List<Collaborator> getRemovableCollaboratorList(int id, List<String> availableCollaborators)
      throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder();
    query.append("select collaborator_id, collaborator from Collaborators ");

    String availableCollaboratorQueryBuilder = "";
    for (int j = 0; j < availableCollaborators.size(); j++) {
      availableCollaboratorQueryBuilder += "?";
      if (j < availableCollaborators.size() - 1) {
        availableCollaboratorQueryBuilder += ",";
      }
    }

    query.append(" where active_status = ? and job_id = ? ");
    if (availableCollaboratorQueryBuilder.length() > 0) {
      query.append(" and collaborator not in (" + availableCollaboratorQueryBuilder + ")");
    }
    int columnIndex = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setInt(++columnIndex, 1);
    preparedStatement.setInt(++columnIndex, id);
    for (String collaborators : availableCollaborators) {
      preparedStatement.setString(++columnIndex, collaborators);
    }

    List<Collaborator> removableCollaborators = DbHelper.getObjectList(dbHandle, preparedStatement,
        resultSet -> {
          Collaborator collaborator = new Collaborator();
          collaborator.setCollaborator_id(resultSet.getInt("collaborator_id"));
          collaborator.setCollaborator(resultSet.getString("collaborator"));
          return collaborator;
        });

    if (!removableCollaborators.isEmpty()) {
      return removableCollaborators;
    } else {
      return new ArrayList<>();
    }
  }

  /**
   * removeCollaborator - remove collaborator
   *
   * @param id
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public void removeCollaborator(int id) throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder();
    query.append("update Collaborators set active_status = ? ");
    query.append(" where collaborator_id = ? ");

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setInt(1, 0);
    preparedStatement.setInt(2, id);

    preparedStatement.executeUpdate();
  }

  @Override
  public boolean doesJobPostExists(Job job)
      throws SQLException, DbHandleException {
    StringBuilder parameterizedSql = new StringBuilder();
    parameterizedSql.append("select 1 from Job where job_status in (?,?) ");
    parameterizedSql.append(" and title = ? and location = ? and organization_id = ? ");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(
        parameterizedSql.toString());
    preparedStatement.setString(++index, "Published");
    preparedStatement.setString(++index, "Draft");
    preparedStatement.setString(++index, job.getTitle());
    preparedStatement.setString(++index, job.getLocation());
    preparedStatement.setInt(++index, job.getOrganization_id());

    return DbHelper.getBoolean(dbHandle, preparedStatement);
  }

  @Override
  public boolean isJobIDValid(Job job) throws SQLException, DbHandleException {
    StringBuilder parameterizedSql = new StringBuilder();
    parameterizedSql.append("select 1 from Job where id = ? and organization_id = ? ");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(
        parameterizedSql.toString());
    preparedStatement.setInt(++index, job.getId());
    preparedStatement.setInt(++index, job.getOrganization_id());

    return DbHelper.getBoolean(dbHandle, preparedStatement);
  }

  @Override
  public List<JsonObject> getHiringLeadAndCollaboratorsWithJobId(Job job)
      throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder();
    query.append("select collaborator from Collaborators ");
    query.append("where active_status = 1 and job_id = ? ");
    Job valueObject = getJobDetails(job.getId());
    String hiringLead = StringUtil.printValidString(valueObject != null ?
        valueObject.getHiring_lead() : "");
    JsonObject hiringLeadObject = new JsonObject();

    if (StringUtil.isNumber(StringUtil.printValidString(hiringLead))) {
      hiringLeadObject.put("collaborator_id", hiringLead);
      JsonObject hiringLeadObj = getHiringLeadName(hiringLead);
      String hiringLeadFirstName = hiringLeadObj.getString("first_name");
      String hiringLeadLastName = hiringLeadObj.getString("last_name");
      String hiringLeadWorkEmail = hiringLeadObj.getString("work_email");
      hiringLeadObject.put("collaborator_name", hiringLeadFirstName + " " + hiringLeadLastName);
      hiringLeadObject.put("collaborator_email", hiringLeadWorkEmail);
    }

    List<JsonObject> employeeList = new ArrayList<>();
    if (hiringLeadObject.size() > 0) {
      employeeList.add(hiringLeadObject);
    }

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setInt(1, job.getId());

    List<JsonObject> collaboratorList = DbHelper.getObjectList(dbHandle, preparedStatement,
        resultSet -> {
          JsonObject jsonObject = new JsonObject();
          String collaboratorId = resultSet.getString("collaborator");
          jsonObject.put("collaborator_id", collaboratorId);
          JsonObject collaboratorObject = getHiringLeadName(collaboratorId);
          String firstName = collaboratorObject.getString("first_name");
          String lastName = collaboratorObject.getString("last_name");
          String workEmail = collaboratorObject.getString("work_email");
          jsonObject.put("collaborator_name", firstName + " " + lastName);
          jsonObject.put("collaborator_email", workEmail);
          return jsonObject;
        });

    if (collaboratorList.size() > 0) {
      employeeList.addAll(collaboratorList);
    }
    return employeeList;
  }

  @Override
  public List<HashMap<String, String>> getCollaboratorList(long jobId)
      throws SQLException, DbHandleException {
    logger.info("getting the collaborator list for job id=" + jobId);
    String query = "Select collaborator from Collaborators where job_id = ? and active_status = 1";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(1, jobId);
    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      HashMap<String, String> getCollaboratorList = new HashMap<>();
      getCollaboratorList.put("collaborator_id", resultSet.getString("collaborator"));
      return getCollaboratorList;
    });
  }

 /* @Override
  public boolean isCollaborator(long creatorId, long jobId)
      throws SQLException, DbHandleException {
      String query = "select count(1) from Collaborators where job_id = ? "+
          " and collaborator = ?  and active_status = ? ";
      PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query);
      preparedStatement.setLong(1,jobId);
      preparedStatement.setLong(2,creatorId);
      preparedStatement.setInt(3,1);
    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      int collaboratorId = resultSet.getInt(1);
      if (collaboratorId>0)
        return  true;
      return false;
    });
  }*/

  @Override
  public List<JsonObject> getJobDetailsForEmployee(long employeeId) throws SQLException, DbHandleException {
    String selectQuery = "select id,title,location,hiring_lead,job_status,date_posted" +
        " from Job where id in " +
        "(select job_id as id from Collaborators,Job where collaborator = ? " +
        " and active_status = 1 and Job.id = Collaborators.job_id and Job.job_status in ('Published', 'Unpublished')" +
        "union select id from Job where hiring_lead = ? and job_active_status = 1 and job_status != ?)";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setLong(1, employeeId);
    preparedStatement.setLong(2, employeeId);
    preparedStatement.setString(3, "Closed");

    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      JsonObject jsonObject = new JsonObject();
      jsonObject.put("id", resultSet.getInt(1));
      jsonObject.put("title", resultSet.getString(2));
      jsonObject.put("location", resultSet.getString(3));
      jsonObject.put("hiring_lead", resultSet.getLong(4));
      jsonObject.put("job_status", resultSet.getString(5));
      jsonObject.put("date_posted", resultSet.getString(6));

      return jsonObject;
    });
  }

  @Override
  public JsonObject getCollaboratorInformation(String collaboratorId, int jobId)
      throws SQLException, DbHandleException {
    StringBuilder queryBuilder = new StringBuilder();
    queryBuilder.append("select collaborator_id, collaborator, job_id, " +
        "collab_email_update, active_status ");
    queryBuilder.append("from Collaborators where job_id = ? and collaborator = ?");

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(
        queryBuilder.toString());
    preparedStatement.setInt(1, jobId);
    preparedStatement.setString(2, collaboratorId);

    JsonObject collaboratorObject = new JsonObject();
    collaboratorObject = DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      JsonObject jsonObject = new JsonObject();
      jsonObject.put("collaborator_id", resultSet.getInt(1));
      jsonObject.put("collaborator", resultSet.getString(2));
      jsonObject.put("job_id", resultSet.getInt(3));
      jsonObject.put("collab_email_update", resultSet.getBoolean(4));
      jsonObject.put("active_status", resultSet.getBoolean(5));
      return jsonObject;
    });

    if (collaboratorObject.size() == 0) {
      return new JsonObject();
    }

    return collaboratorObject;

  }

  @Override
  public String getHiringLeadBasedOnCandidate(long candidateId)
      throws SQLException, DbHandleException {
    String hiringLeadId = "";
    StringBuilder query = new StringBuilder();
    query.append("select hiring_lead from Job where id in ");
    query.append("(select job_id from Candidate where candidate_id = ?)");

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(1, candidateId);

    ResultSet resultSet = preparedStatement.executeQuery();
    while (resultSet.next()) {
      hiringLeadId = resultSet.getString("hiring_lead");
    }
    return hiringLeadId;
  }

  /**
   * \
   *
   * @param parameterColumn to be certain that it is not passed as user param, to be used as code param to avoid SQL injection
   * @param parameterValue
   * @param organizationId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public boolean checkFieldOption(String parameterColumn, String parameterValue, long organizationId)
      throws SQLException, DbHandleException {
    String selectQuery = "select 1 from Job where " + parameterColumn + " = ? " +
        "and job_status = 'Published' and job_active_status = 1 and organization_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setString(1, parameterValue);
    preparedStatement.setLong(2, organizationId);

    return DbHelper.getBoolean(dbHandle, preparedStatement);
  }

  /**
   * @param job
   * @return
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public int cloneJob(Job job) throws SQLException, DbHandleException {

    String insertQuery = "insert into Job(title, hiring_lead, internal_job_code, employment_type, " +
        "location, city, zip, country, state, job_status, department, minimum_experience, " +
        "compensation, job_description,  resume, resume_required, date_available, " +
        "date_available_required, cover_letter, cover_letter_required, highest_education, " +
        "highest_education_required, reference, reference_required, desired_salary,  " +
        "desired_salary_required, college, college_required, referred_by, referred_by_required," +
        "linkedin_url, linkedin_url_required, blog, blog_required, company_type_check, company_type," +
        "post_permission, date_posted, collab_resume, collab_date_available, " +
        "collab_cover_letter, collab_highest_education, collab_reference, collab_desired_salary, " +
        "collab_college, collab_referred_by, collab_linkedin_url, collab_blog, organization_id, " +
        "applicant, screening, interview, offer, hire, job_desc_template, job_open_date, " +
        "job_active_status, job_archive_status, not_hired, currency_type)" +
        " select title, hiring_lead,internal_job_code, employment_type, location, city, zip, country," +
        " state, ?, department, minimum_experience, compensation, job_description, resume," +
        " resume_required, date_available, date_available_required, cover_letter, " +
        "cover_letter_required, highest_education, highest_education_required, reference, " +
        "reference_required, desired_salary, desired_salary_required, college, college_required," +
        " referred_by, referred_by_required, linkedin_url, linkedin_url_required, blog, blog_required," +
        " company_type_check, company_type, post_permission, ?, " +
        "collab_resume, collab_date_available, collab_cover_letter, collab_highest_education," +
        " collab_reference, collab_desired_salary, collab_college, collab_referred_by," +
        " collab_linkedin_url, collab_blog, organization_id, applicant, screening, interview, offer," +
        " hire, job_desc_template, ?, job_active_status, job_archive_status, not_hired," +
        " currency_type from Job where id = ?";

    Date jobPostDate = new Date();
    SimpleDateFormat jobPostDateFormat = new DateUtil().getDate();
    String datePostedString = jobPostDateFormat.format(jobPostDate);
    PreparedStatement preparedStatement =
        this.dbHandle.createPreparedStatement(insertQuery, true);
    preparedStatement.setString(1, "Draft");
    preparedStatement.setString(2, datePostedString);
    preparedStatement.setString(3, "00/00/0000");
    preparedStatement.setInt(4, job.getId());

    int generatedJobId = DbHelper.executeUpdate(this.dbHandle, preparedStatement, true);
    if (generatedJobId > 0) {
      String customQuestionQuery = "insert into CustomQuestionJob(custom_question, custom_option1, " +
          "custom_option2, custom_knockout, custom_answer, job_id, custom_question_required)" +
          " select custom_question, custom_option1, custom_option2, custom_knockout, custom_answer, " +
          "?, custom_question_required from CustomQuestionJob where job_id = ?";

      preparedStatement = this.dbHandle.createPreparedStatement(customQuestionQuery, false);
      preparedStatement.setInt(1, generatedJobId);
      preparedStatement.setInt(2, job.getId());

      DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
    }

    return generatedJobId;
  }
}

