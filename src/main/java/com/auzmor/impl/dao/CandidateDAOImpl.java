package com.auzmor.impl.dao;

import com.auzmor.dao.CandidateDAO;
import com.auzmor.dao.EmployeeDAO;
import com.auzmor.dao.JobDAO;
import com.auzmor.dao.LoginDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.handler.CountryStateHandler;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.model.Login;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.models.CandidateR.CandidateCustomQuestionR;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.impl.util.DateUtil;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.util.db.DbHandle;
import com.mysql.cj.core.util.StringUtils;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class CandidateDAOImpl implements CandidateDAO {

  private static Logger logger = LoggerFactory.getLogger(CandidateDAOImpl.class);
  private DbHandle dbHandle;

  public CandidateDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  public int create(CandidateR candidate) throws SQLException, DbHandleException {

    StringBuilder parameterizedSql = new StringBuilder("insert into Candidate(organization_id, ");
    parameterizedSql.append("first_name, last_name,");
    parameterizedSql.append("email,phone_number,unit,source,address_line,city,state,zip,");
    parameterizedSql.append("country,job_id,hiring_lead,internal_jobcode,");
    parameterizedSql.append("resume,date_available,cover_letter,highest_education,");
    parameterizedSql.append("reference,desired_salary,college,referred_by,");
    parameterizedSql.append("linkedin_url,blog,company_type_check,gender,veteran_status,");
    parameterizedSql.append("ethnicity,disability,user_id,creator_id,status,resume_name) ");
    parameterizedSql.append("values (?, ?, ?, ?, ?,?, ");
    parameterizedSql.append("?, ?, ?, ?,?, ?, ?, ?, ?,?, ?, ?, ");
    parameterizedSql.append("?, ?,?, ?, ?, ?, ?,?, ?, ?, ?, ?,?, ?,?,? )");
  /*  LoginDAO loginDAO = new LoginDAOImpl(this.dbHandle);
    Login login = loginDAO.getLoginUserDetails(candidate.getUserId());
    if (null != login && !login.getUserType().equalsIgnoreCase("candidate")) {
      EmployeeDAO employeeDAO = new EmployeeDAOImpl(this.dbHandle);
      Employee employee = employeeDAO.getEmployeeDetails(candidate.getUserId());
      candidate.setUserId((long) (employee.getUser_id()));
    }*/

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.
        createPreparedStatement(parameterizedSql.toString(), true);
    preparedStatement.setLong(++index, candidate.getOrganizationId());
    preparedStatement.setString(++index, candidate.getFirstName());
    preparedStatement.setString(++index, candidate.getLastName());
    preparedStatement.setString(++index, candidate.getEmail());
    preparedStatement.setString(++index, candidate.getPhoneNumber());
    preparedStatement.setString(++index, candidate.getUnit());
    preparedStatement.setString(++index, candidate.getSource());
    preparedStatement.setString(++index, candidate.getAddressLine());
    preparedStatement.setString(++index, candidate.getCity());
    preparedStatement.setString(++index, candidate.getState());
    preparedStatement.setString(++index, candidate.getZip());
    preparedStatement.setString(++index, candidate.getCountry());
    preparedStatement.setLong(++index, candidate.getJobId());
    preparedStatement.setLong(++index, candidate.getHiringLeadId());
    preparedStatement.setString(++index, candidate.getInternalJobCode());
    preparedStatement.setString(++index, candidate.getResume());
    preparedStatement.setString(++index, candidate.getDateAvailable());
    preparedStatement.setString(++index, candidate.getCoverLetter());
    preparedStatement.setString(++index, candidate.getHighestEducation());
    preparedStatement.setString(++index, candidate.getReference());
    preparedStatement.setString(++index, candidate.getDesiredSalary());
    preparedStatement.setString(++index, candidate.getCollege());
    preparedStatement.setString(++index, candidate.getReferredBy());
    preparedStatement.setString(++index, candidate.getLinkedinUrl());
    preparedStatement.setString(++index, candidate.getBlog());
    preparedStatement.setString(++index, candidate.getCompanyTypeCheck());
    preparedStatement.setString(++index, candidate.getGender());
    preparedStatement.setString(++index, candidate.getVeteranStatus());
    preparedStatement.setString(++index, candidate.getEthnicity());
    preparedStatement.setString(++index, candidate.getDisability());
    preparedStatement.setLong(++index, candidate.getUserId());
    preparedStatement.setLong(++index, candidate.getAdminId());
    preparedStatement.setString(++index, "applicant");
    preparedStatement.setString(++index, candidate.getResumeName());
    logger.info("Candidate Created Successfully!");
    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, true);
  }

  @Override
  public CandidateR getCandidateDetails(long candidateId, long organizationId)
      throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder("Select organization_id, first_name, last_name,");
    query.append("email,phone_number,unit,source,address_line,city,state,zip,country,job_id,");
    query.append("hiring_lead,internal_jobcode,resume,");
    query.append("date_available,cover_letter,highest_education,reference,");
    query.append("desired_salary,college,referred_by,linkedin_url,blog,company_type_check,gender,");
    query.append("veteran_status,ethnicity,disability,user_id,created,modified,status,creator_id,rating,candidate_id");
    query.append(" from Candidate WHERE candidate_id = ? and active_status =1 and organization_id = ?");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, candidateId);
    preparedStatement.setLong(++index, organizationId);

    return DbHelper.getObject(this.dbHandle, preparedStatement, resultSet -> {
      int retrieveIndex = 0;
      CandidateR candidateR = new CandidateR();
      candidateR.setOrganizationId(Long.parseLong(resultSet.getString(++retrieveIndex)));
      candidateR.setFirstName(resultSet.getString(++retrieveIndex));
      candidateR.setLastName(resultSet.getString(++retrieveIndex));
      candidateR.setEmail(resultSet.getString(++retrieveIndex));
      candidateR.setPhoneNumber(resultSet.getString(++retrieveIndex));
      candidateR.setUnit(resultSet.getString(++retrieveIndex));
      candidateR.setSource(resultSet.getString(++retrieveIndex));
      candidateR.setAddressLine(resultSet.getString(++retrieveIndex));
      candidateR.setCity(resultSet.getString(++retrieveIndex));
      candidateR.setState(resultSet.getString(++retrieveIndex));
      candidateR.setZip(resultSet.getString(++retrieveIndex));
      candidateR.setCountry(resultSet.getString(++retrieveIndex));
      candidateR.setJobId(Long.parseLong(resultSet.getString(++retrieveIndex)));
      candidateR.setHiringLeadId(Long.parseLong(resultSet.getString(++retrieveIndex)));
      candidateR.setInternalJobCode(resultSet.getString(++retrieveIndex));
      candidateR.setResume(resultSet.getString(++retrieveIndex));
      candidateR.setDateAvailable(resultSet.getString(++retrieveIndex));
      candidateR.setCoverLetter(resultSet.getString(++retrieveIndex));
      candidateR.setHighestEducation(resultSet.getString(++retrieveIndex));
      candidateR.setReference(resultSet.getString(++retrieveIndex));
      candidateR.setDesiredSalary(resultSet.getString(++retrieveIndex));
      candidateR.setCollege(resultSet.getString(++retrieveIndex));
      candidateR.setReferredBy(resultSet.getString(++retrieveIndex));
      candidateR.setLinkedinUrl(resultSet.getString(++retrieveIndex));
      candidateR.setBlog(resultSet.getString(++retrieveIndex));
      candidateR.setCompanyTypeCheck(resultSet.getString(++retrieveIndex));
      candidateR.setGender(resultSet.getString(++retrieveIndex));
      candidateR.setVeteranStatus(resultSet.getString(++retrieveIndex));
      candidateR.setEthnicity(resultSet.getString(++retrieveIndex));
      candidateR.setDisability(resultSet.getString(++retrieveIndex));
      candidateR.setUserId(Long.parseLong(resultSet.getString(++retrieveIndex)));
      candidateR.setCreated(resultSet.getDate(++retrieveIndex));
      candidateR.setModified(resultSet.getDate(++retrieveIndex));
      candidateR.setStatus(resultSet.getString(++retrieveIndex));
      candidateR.setAdminId(resultSet.getLong(++retrieveIndex));
      candidateR.setRating(resultSet.getLong(++retrieveIndex));
      candidateR.setCandidateId(resultSet.getLong(++retrieveIndex));
      logger.info("Candidate Details fetched Successfully!");
      return candidateR;
    });
  }


  /*@Override
  public CandidateR getCandidateDetailsByLoginId(long loginId)
      throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder("Select organization_id, first_name, last_name,");
    query.append("email,phone_number,unit,source,address_line,city,state,zip,country,job_id,");
    query.append("hiring_lead,internal_jobcode,resume,");
    query.append("date_available,cover_letter,highest_education,reference,");
    query.append("desired_salary,college,referred_by,linkedin_url,blog,company_type_check,gender,");
    query.append("veteran_status,ethnicity,disability,user_id,created,modified,status,creator_id,rating,candidate_id");
    query.append(" from Candidate WHERE user_id = ? and active_status =1");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, loginId);

    return DbHelper.getObject(this.dbHandle, preparedStatement, resultSet -> {
      int retrieveIndex = 0;
      CandidateR candidateR = new CandidateR();
      candidateR.setOrganizationId(Long.parseLong(resultSet.getString(++retrieveIndex)));
      candidateR.setFirstName(resultSet.getString(++retrieveIndex));
      candidateR.setLastName(resultSet.getString(++retrieveIndex));
      candidateR.setEmail(resultSet.getString(++retrieveIndex));
      candidateR.setPhoneNumber(resultSet.getString(++retrieveIndex));
      candidateR.setUnit(resultSet.getString(++retrieveIndex));
      candidateR.setSource(resultSet.getString(++retrieveIndex));
      candidateR.setAddressLine(resultSet.getString(++retrieveIndex));
      candidateR.setCity(resultSet.getString(++retrieveIndex));
      candidateR.setState(resultSet.getString(++retrieveIndex));
      candidateR.setZip(resultSet.getString(++retrieveIndex));
      candidateR.setCountry(resultSet.getString(++retrieveIndex));
      candidateR.setJobId(Long.parseLong(resultSet.getString(++retrieveIndex)));
      candidateR.setHiringLeadId(Long.parseLong(resultSet.getString(++retrieveIndex)));
      candidateR.setInternalJobCode(resultSet.getString(++retrieveIndex));
      candidateR.setResume(resultSet.getString(++retrieveIndex));
      candidateR.setDateAvailable(resultSet.getString(++retrieveIndex));
      candidateR.setCoverLetter(resultSet.getString(++retrieveIndex));
      candidateR.setHighestEducation(resultSet.getString(++retrieveIndex));
      candidateR.setReference(resultSet.getString(++retrieveIndex));
      candidateR.setDesiredSalary(resultSet.getString(++retrieveIndex));
      candidateR.setCollege(resultSet.getString(++retrieveIndex));
      candidateR.setReferredBy(resultSet.getString(++retrieveIndex));
      candidateR.setLinkedinUrl(resultSet.getString(++retrieveIndex));
      candidateR.setBlog(resultSet.getString(++retrieveIndex));
      candidateR.setCompanyTypeCheck(resultSet.getString(++retrieveIndex));
      candidateR.setGender(resultSet.getString(++retrieveIndex));
      candidateR.setVeteranStatus(resultSet.getString(++retrieveIndex));
      candidateR.setEthnicity(resultSet.getString(++retrieveIndex));
      candidateR.setDisability(resultSet.getString(++retrieveIndex));
      candidateR.setUserId(Long.parseLong(resultSet.getString(++retrieveIndex)));
      candidateR.setCreated(resultSet.getDate(++retrieveIndex));
      candidateR.setModified(resultSet.getDate(++retrieveIndex));
      candidateR.setStatus(resultSet.getString(++retrieveIndex));
      candidateR.setAdminId(resultSet.getLong(++retrieveIndex));
      candidateR.setRating(resultSet.getLong(++retrieveIndex));
      candidateR.setCandidateId(resultSet.getLong(++retrieveIndex));
      logger.info("Candidate Details fetched Successfully!");
      return candidateR;
    });
  }*/

  @Override
  public CandidateR getCandidate(long candidateId)
      throws SQLException, DbHandleException {
    logger.info("get candidate details for id=" + candidateId);
    StringBuilder query = new StringBuilder("Select organization_id, first_name, last_name,");
    query.append("email,phone_number,unit,source,address_line,city,state,zip,country,job_id,");
    query.append("hiring_lead,internal_jobcode,resume,date_available,cover_letter,highest_education,");
    query.append("reference,desired_salary,college,referred_by,linkedin_url,blog,company_type_check,gender,");
    query.append("veteran_status,ethnicity,disability,user_id,created,modified,status,creator_id,rating,candidate_id ");
    query.append(" from Candidate WHERE candidate_id = ? and active_status =1 ");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, candidateId);
    return DbHelper.getObject(this.dbHandle, preparedStatement, resultSet -> {
      int retrieveIndex = 0;
      CandidateR candidateR = new CandidateR();
      candidateR.setOrganizationId(Long.parseLong(resultSet.getString(++retrieveIndex)));
      candidateR.setFirstName(resultSet.getString(++retrieveIndex));
      candidateR.setLastName(resultSet.getString(++retrieveIndex));
      candidateR.setEmail(resultSet.getString(++retrieveIndex));
      candidateR.setPhoneNumber(resultSet.getString(++retrieveIndex));
      candidateR.setUnit(resultSet.getString(++retrieveIndex));
      candidateR.setSource(resultSet.getString(++retrieveIndex));
      candidateR.setAddressLine(resultSet.getString(++retrieveIndex));
      candidateR.setCity(resultSet.getString(++retrieveIndex));
      candidateR.setState(resultSet.getString(++retrieveIndex));
      candidateR.setZip(resultSet.getString(++retrieveIndex));
      candidateR.setCountry(resultSet.getString(++retrieveIndex));
      candidateR.setJobId(Long.parseLong(resultSet.getString(++retrieveIndex)));
      candidateR.setHiringLeadId(Long.parseLong(resultSet.getString(++retrieveIndex)));
      candidateR.setInternalJobCode(resultSet.getString(++retrieveIndex));
      candidateR.setResume(resultSet.getString(++retrieveIndex));
      candidateR.setDateAvailable(resultSet.getString(++retrieveIndex));
      candidateR.setCoverLetter(resultSet.getString(++retrieveIndex));
      candidateR.setHighestEducation(resultSet.getString(++retrieveIndex));
      candidateR.setReference(resultSet.getString(++retrieveIndex));
      candidateR.setDesiredSalary(resultSet.getString(++retrieveIndex));
      candidateR.setCollege(resultSet.getString(++retrieveIndex));
      candidateR.setReferredBy(resultSet.getString(++retrieveIndex));
      candidateR.setLinkedinUrl(resultSet.getString(++retrieveIndex));
      candidateR.setBlog(resultSet.getString(++retrieveIndex));
      candidateR.setCompanyTypeCheck(resultSet.getString(++retrieveIndex));
      candidateR.setGender(resultSet.getString(++retrieveIndex));
      candidateR.setVeteranStatus(resultSet.getString(++retrieveIndex));
      candidateR.setEthnicity(resultSet.getString(++retrieveIndex));
      candidateR.setDisability(resultSet.getString(++retrieveIndex));
      candidateR.setUserId(Long.parseLong(resultSet.getString(++retrieveIndex)));
      candidateR.setCreated(resultSet.getDate(++retrieveIndex));
      candidateR.setModified(resultSet.getDate(++retrieveIndex));
      candidateR.setStatus(resultSet.getString(++retrieveIndex));
      candidateR.setAdminId(resultSet.getLong(++retrieveIndex));
      candidateR.setRating(resultSet.getLong(++retrieveIndex));
      candidateR.setCandidateId(resultSet.getLong(++retrieveIndex));
      logger.info("Candidate fetched Successfully!");
      return candidateR;
    });
  }

  public int getUserName(String firstName, String email, long organizationId)
      throws SQLException, DbHandleException {

    StringBuilder queryString = new StringBuilder(" SELECT b.id,b.first_name FROM Candidate a,Login b ");
    queryString.append(" where a.organization_id = b.organization_id and a.first_name = ? ");
    queryString.append(" and b.email = ? and b.organization_id = ? ");
    queryString.append(" order by b.id desc limit 1");

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(queryString.toString());
    preparedStatement.setString(1, firstName);
    preparedStatement.setString(2, email);
    preparedStatement.setLong(3, organizationId);

    Integer id = DbHelper.getObject(this.dbHandle, preparedStatement, resultSet -> resultSet.getInt(1));
    logger.info("User Name fetched Successfully!");
    return null == id ? 0 : id;
  }

  @Override
  public List<HashMap<String, String>> getUserIdCandidateDetails(long userId, long organizationId)
      throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder("Select organization_id, first_name, last_name,");
    query.append("email,phone_number,unit,source,address_line,city,state,zip,country,job_id,");
    query.append("hiring_lead,internal_jobcode,");
    query.append("resume,date_available,cover_letter,highest_education,");
    query.append("reference,desired_salary,college,referred_by,linkedin_url,blog,company_type_check,");
    query.append("gender,veteran_status,ethnicity,disability,candidate_id,status,created,modified,resume_name ");
    query.append("from Candidate WHERE user_id = ? and active_status =1 and organization_id = ? ");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, userId);
    preparedStatement.setLong(++index, organizationId);
    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      HashMap<String, String> candidateDetailsList = new HashMap<>();
      int retrieveIndex = 0;
      candidateDetailsList.put("organization_id", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("first_name", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("last_name", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("email", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("phone_number", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("unit", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("source", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("address_line", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("city", resultSet.getString(++retrieveIndex));
      //candidateDetailsList.put("state", resultSet.getString(++retrieveIndex));
      String state = resultSet.getString(++retrieveIndex);
      if (!StringUtils.isNullOrEmpty(state)) {
        candidateDetailsList.put("state", CountryStateHandler.getStateName(state));
      } else {
        candidateDetailsList.put("state", " ");
      }
      candidateDetailsList.put("zip", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("country", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("job_id", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("hiring_lead", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("internal_jobcode", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("resume", resultSet.getString(++retrieveIndex));
      //String file_name[] = candidateDetailsList.get("resume").split("/");
      //String resumeTitle =file_name[file_name.length-1].substring(0,file_name[file_name.length-1].lastIndexOf("_")) ;
      //candidateDetailsList.put("resume_title",resumeTitle);
      candidateDetailsList.put("date_available", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("cover_letter", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("highest_education", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("reference", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("desired_salary", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("college", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("referred_by", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("linkedin_url", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("blog", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("company_type_check", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("gender", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("veteran_status", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("ethnicity", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("disability", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("candidate_id", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("status", resultSet.getString(++retrieveIndex));
      try {
        String createdDate = new DateUtil().
            convertDateintoString(resultSet.getDate(++retrieveIndex));
        candidateDetailsList.put("created_date", createdDate);

        if (!StringUtil.printValidString(String.valueOf(resultSet.getDate("modified"))).equals("")) {
          String modifiedDate = new DateUtil().
              convertDateintoString(resultSet.getDate(++retrieveIndex));
          candidateDetailsList.put("modified_date", modifiedDate);
        }
        candidateDetailsList.put("resume_name", resultSet.getString(++retrieveIndex));
      } catch (ParseException e) {
        e.printStackTrace();
      }

      JobDAO jobDAO = new JobDAOImpl(this.dbHandle);

      if (resultSet.getInt("job_id") != 0) {
        long jobId = resultSet.getInt("job_id");
        try {
          Job job = jobDAO.getJobDetails(jobId);
          if (job != null) {
            candidateDetailsList.put("employment_type", job.getEmployment_type());
            candidateDetailsList.put("location", job.getLocation());
            candidateDetailsList.put("job_status", job.getJob_status());
            candidateDetailsList.put("department", job.getDepartment());
            candidateDetailsList.put("minimum_experience", job.getMinimum_experience());
            candidateDetailsList.put("job_title", job.getTitle());
          } else {
            candidateDetailsList.put("employment_type", "-");
            candidateDetailsList.put("location", "-");
            candidateDetailsList.put("job_status", "-");
            candidateDetailsList.put("department", "-");
            candidateDetailsList.put("minimum_experience", "-");
            candidateDetailsList.put("job_title", "-");
          }
        } catch (DbHandleException e) {
          e.printStackTrace();
        }
      } else {
        candidateDetailsList.put("employment_type", "-");
        candidateDetailsList.put("location", "-");
        candidateDetailsList.put("job_status", "-");
        candidateDetailsList.put("department", "-");
        candidateDetailsList.put("minimum_experience", "-");
        candidateDetailsList.put("job_title", "-");
      }
      logger.info("Candidate Details fetched Successfully!");
      return candidateDetailsList;
    });
  }

  @Override
  public List<HashMap<String, String>> getCandidateId(long userId)
      throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder("Select candidate_id ");
    query.append("from Candidate WHERE user_id = ? and active_status =1 ");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, userId);
    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      HashMap<String, String> candidateDetailsList = new HashMap<>();
      int retrieveIndex = 0;
      // candidateDetailsList.put("job_id", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("candidate_id", resultSet.getString(++retrieveIndex));
      logger.info("Candidate Id fetched Successfully!");
      return candidateDetailsList;
    });
  }

  @Override
  public List<HashMap<String, String>> getJobIdCandidateDetails(long jobId, long organizationId)
      throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder("Select organization_id, first_name, last_name,");
    query.append("email,phone_number,unit,source,address_line,city,state,zip,country,user_id,");
    query.append("hiring_lead,internal_jobcode,resume,date_available,cover_letter,highest_education,");
    query.append("reference,desired_salary,college,referred_by,linkedin_url,blog,company_type_check,");
    query.append("gender,veteran_status,ethnicity,disability,candidate_id,status,rating,created,modified ");
    query.append("from Candidate WHERE job_id = ? and active_status =1 and organization_id = ? ");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, jobId);
    preparedStatement.setLong(++index, organizationId);

    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      HashMap<String, String> candidateDetailsList = new HashMap<>();
      int retrieveIndex = 0;

      candidateDetailsList.put("organization_id", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("first_name", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("last_name", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("email", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("phone_number", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("unit", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("source", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("address_line", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("city", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("state", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("zip", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("country", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("user_id", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("hiring_lead", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("internal_jobcode", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("resume", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("date_available", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("cover_letter", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("highest_education", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("reference", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("desired_salary", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("college", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("referred_by", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("linkedin_url", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("blog", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("company_type_check", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("gender", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("veteran_status", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("ethnicity", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("disability", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("candidate_id", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("status", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("rating", resultSet.getString(++retrieveIndex));
      try {
        String createdDate = new DateUtil().
            convertDateintoString(resultSet.getDate(++retrieveIndex));
        candidateDetailsList.put("applied_date", createdDate);

        if (!StringUtil.printValidString(String.valueOf(resultSet.getDate("modified"))).equals("")) {
          String modifiedDate = new DateUtil().
              convertDateintoString(resultSet.getDate("modified"));
          candidateDetailsList.put("modified_date", modifiedDate);
        }

      } catch (ParseException e) {
        e.printStackTrace();
      }

      JobDAO jobDAO = new JobDAOImpl(this.dbHandle);
      Job job;
      try {
        job = jobDAO.getJobDetails(jobId);
        if (job != null) {
          candidateDetailsList.put("employment_type", job.getEmployment_type());
          candidateDetailsList.put("location", job.getLocation());
          candidateDetailsList.put("job_status", job.getJob_status());
          candidateDetailsList.put("department", job.getDepartment());
          candidateDetailsList.put("minimum_experience", job.getMinimum_experience());
          candidateDetailsList.put("job_title", job.getTitle());
        } else {
          candidateDetailsList.put("employment_type", "-");
          candidateDetailsList.put("location", "-");
          candidateDetailsList.put("job_status", "-");
          candidateDetailsList.put("department", "-");
          candidateDetailsList.put("minimum_experience", "-");
          candidateDetailsList.put("job_title", "-");
        }
      } catch (DbHandleException e) {
        e.printStackTrace();
      }
      logger.info("Candidate Details List fetched Successfully!");
      return candidateDetailsList;
    });
  }

  @Override
  public List<HashMap<String, String>> getJobIdCandidateDetails(long jobId)
      throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder("Select organization_id, first_name, last_name,");
    query.append("email,phone_number,unit,source,address_line,city,state,zip,country,user_id,");
    query.append("hiring_lead,internal_jobcode,resume,date_available,cover_letter,highest_education,");
    query.append("reference,desired_salary,college,referred_by,linkedin_url,blog,company_type_check,");
    query.append("gender,veteran_status,ethnicity,disability,candidate_id,status,rating,created,modified ");
    query.append("from Candidate WHERE job_id = ? and active_status =1 ");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, jobId);

    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      HashMap<String, String> candidateDetailsList = new HashMap<>();
      int retrieveIndex = 0;

      candidateDetailsList.put("organization_id", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("first_name", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("last_name", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("email", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("phone_number", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("unit", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("source", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("address_line", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("city", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("state", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("zip", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("country", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("user_id", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("hiring_lead", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("internal_jobcode", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("resume", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("date_available", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("cover_letter", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("highest_education", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("reference", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("desired_salary", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("college", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("referred_by", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("linkedin_url", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("blog", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("company_type_check", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("gender", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("veteran_status", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("ethnicity", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("disability", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("candidate_id", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("status", resultSet.getString(++retrieveIndex));
      candidateDetailsList.put("rating", resultSet.getString(++retrieveIndex));
      try {
        String createdDate = new DateUtil().
            convertDateintoString(resultSet.getDate(++retrieveIndex));
        candidateDetailsList.put("applied_date", createdDate);

        if (!StringUtil.printValidString(String.valueOf(resultSet.getDate("modified"))).equals("")) {
          String modifiedDate = new DateUtil().
              convertDateintoString(resultSet.getDate("modified"));
          candidateDetailsList.put("modified_date", modifiedDate);
        }

      } catch (ParseException e) {
        e.printStackTrace();
      }

      JobDAO jobDAO = new JobDAOImpl(this.dbHandle);
      Job job;
      try {
        job = jobDAO.getJobDetails(jobId);
        if (job != null) {
          candidateDetailsList.put("employment_type", job.getEmployment_type());
          candidateDetailsList.put("location", job.getLocation());
          candidateDetailsList.put("job_status", job.getJob_status());
          candidateDetailsList.put("department", job.getDepartment());
          candidateDetailsList.put("minimum_experience", job.getMinimum_experience());
          candidateDetailsList.put("job_title", job.getTitle());
        } else {
          candidateDetailsList.put("employment_type", "-");
          candidateDetailsList.put("location", "-");
          candidateDetailsList.put("job_status", "-");
          candidateDetailsList.put("department", "-");
          candidateDetailsList.put("minimum_experience", "-");
          candidateDetailsList.put("job_title", "-");
        }
      } catch (DbHandleException e) {
        e.printStackTrace();
      }
      logger.info("Candidate Details List fetched Successfully!");
      return candidateDetailsList;
    });
  }

  @Override
  public int deleteCandidate(long candidateId) throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder("update Candidate set active_status = 0 ");
    query.append("where candidate_id = ? and active_status =1 ");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, candidateId);
    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  @Override
  public List<HashMap<String, String>> getPostJobFilterDetails(long organizationId)
      throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder(" select (SELECT group_concat(DISTINCT location) FROM Job ");
    query.append("where organization_id = ?  and job_status = 'Published' )");
    query.append("as location,(SELECT group_concat(DISTINCT title)");
    query.append("FROM Job where organization_id = ? and job_status = 'Published' )");
    query.append("as jobtitle,(SELECT group_concat(DISTINCT hiring_lead) ");
    query.append("FROM Job where organization_id =? and job_status = 'Published' ");
    query.append(" and hiring_lead != '' ) as hiringlead ,");
    query.append("(SELECT group_concat(DISTINCT minimum_experience) FROM Job ");
    query.append("where organization_id = ? and job_status = 'Published' and  ");
    query.append("minimum_experience != '') as minimum_experience,");
    query.append("(SELECT group_concat(DISTINCT employment_type) FROM Job ");
    query.append("where organization_id = ? and job_status = 'Published' ");
    query.append("and employment_type != '') as employment_type,");
    query.append("(SELECT group_concat(DISTINCT department) FROM Job ");
    query.append("where organization_id = ? and job_status = 'Published' ");
    query.append("and department != '') as department");


    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, organizationId);
    preparedStatement.setLong(++index, organizationId);
    preparedStatement.setLong(++index, organizationId);
    preparedStatement.setLong(++index, organizationId);
    preparedStatement.setLong(++index, organizationId);
    preparedStatement.setLong(++index, organizationId);
    return DbHelper.getObjectList(this.dbHandle, preparedStatement, resultSet -> {
      HashMap<String, String> filterList = new HashMap<>();
      int retrieveIndex = 0;
      filterList.put("location", resultSet.getString(++retrieveIndex));
      filterList.put("jobTitle", resultSet.getString(++retrieveIndex));
      filterList.put("HiringLead", resultSet.getString(++retrieveIndex));
      filterList.put("minimumExp", resultSet.getString(++retrieveIndex));
      filterList.put("employmentType", resultSet.getString(++retrieveIndex));
      filterList.put("department", resultSet.getString(++retrieveIndex));
      logger.info("Post Job filter details fetched Successfully!");
      return filterList;
    });
  }


  @Override
  public int updateRating(int rating, long candidateId)
      throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder("update Candidate set rating = ? ");
    query.append("where candidate_id = ? and active_status =1 ");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setInt(++index, rating);
    preparedStatement.setLong(++index, candidateId);
    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  @Override
  public int updateForwardEmployee(long employeeId, String notes, String id)
      throws SQLException, DbHandleException {

    java.sql.Date sqlDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
    StringBuilder query = new StringBuilder("update Candidate set collaborator_id = ?,collaborator_notes =?, ");
    query.append(" collaborator_date = ? where candidate_id = ? and active_status = 1 ");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, employeeId);
    preparedStatement.setString(++index, notes);
    preparedStatement.setDate(++index, sqlDate);
    preparedStatement.setString(++index, id);

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  @Override
  public int updateMoveCandidate(long jobId, long candidateId, String status)
      throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder("update Candidate set job_id =? ,status=? ");
    query.append(" WHERE candidate_id  = ? and active_status =1");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, jobId);
    preparedStatement.setString(++index, status);
    preparedStatement.setLong(++index, candidateId);
    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  @Override
  public int updateCandidate(CandidateR candidate) throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder("update Candidate set " +
        "first_name =?,last_name=?,email=?,phone_number=?,unit=?,source=?," +
        "address_line=?,city=?,state=?,country=?,gender=?," +
        " resume=?,cover_letter=?,highest_education=?," +
        "desired_salary=?,linkedin_url=?,date_available=?,blog=?,reference=?,referred_by=?,college=?," +
        "company_type_check = ?,ethnicity=?,disability=?,job_id =?,status=? ");
    query.append(" WHERE candidate_id  = ? and active_status =1");
    int index = 0;

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setString(++index, candidate.getFirstName());
    preparedStatement.setString(++index, candidate.getLastName());
    preparedStatement.setString(++index, candidate.getEmail());
    preparedStatement.setString(++index, candidate.getPhoneNumber());
    preparedStatement.setString(++index, candidate.getUnit());
    preparedStatement.setString(++index, candidate.getSource());
    preparedStatement.setString(++index, candidate.getAddressLine());
    preparedStatement.setString(++index, candidate.getCity());
    preparedStatement.setString(++index, candidate.getState());
    preparedStatement.setString(++index, candidate.getCountry());
    preparedStatement.setString(++index, candidate.getGender());
    preparedStatement.setString(++index, candidate.getResume());
    preparedStatement.setString(++index, candidate.getCoverLetter());
    preparedStatement.setString(++index, candidate.getHighestEducation());
    preparedStatement.setString(++index, candidate.getDesiredSalary());
    preparedStatement.setString(++index, candidate.getLinkedinUrl());
    preparedStatement.setString(++index, candidate.getDateAvailable());
    preparedStatement.setString(++index, candidate.getBlog());
    preparedStatement.setString(++index, candidate.getReference());
    preparedStatement.setString(++index, candidate.getReferredBy());
    preparedStatement.setString(++index, candidate.getCollege());
    preparedStatement.setString(++index, candidate.getCompanyTypeCheck());
    preparedStatement.setString(++index, candidate.getEthnicity());
    preparedStatement.setString(++index, candidate.getDisability());
    preparedStatement.setLong(++index, candidate.getJobId());
    preparedStatement.setString(++index, candidate.getStatus());
    preparedStatement.setLong(++index, candidate.getCandidateId());
    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  @Override

  public List<HashMap<String, String>> getCandidateList(String searchFilter,
                                                        int limit, int offset,
                                                        long organization, String candidateValues,
                                                        String hiringLeadValues, int collaboratorId,
                                                        int id, int employeeId, String stateId)
      throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder("select Candidate.candidate_id,Candidate.first_name,Candidate.last_name,");
    query.append(" Candidate.phone_number,Candidate.rating, ");
    query.append(" Candidate.status,Candidate.created,Candidate.modified,");
    query.append(" Candidate.email,Job.title,Candidate.state ");
    query.append(" from Candidate,Job,HiringprocessCandidate where Candidate.job_id=Job.id ");
    // query.append(" and Candidate.status is null " );
    query.append(" and Job.job_status != 'Closed' and Candidate.active_status = 1 ");
    query.append(" and Job.organization_id = Candidate.organization_id and ");
    query.append(" HiringprocessCandidate.candidate_process not in ('Archive to Talent Pool') ");
    query.append(" and HiringprocessCandidate.active_status = 1  and ");
    query.append(" Candidate.candidate_id = HiringprocessCandidate.candidate_id and ");
    query.append(" Candidate.status = HiringprocessCandidate.status and Candidate.organization_id = ?  ");

    if (candidateValues != null && candidateValues.length() > 0) {
      if (candidateValues.contains(",")) {
        candidateValues = candidateValues.replace(",", "','");
      }
      query.append(" and Candidate.status = ? ");
    }
    if (hiringLeadValues != null && hiringLeadValues.length() > 0 && collaboratorId == 0) {
      if (hiringLeadValues.contains(",")) {
        hiringLeadValues = hiringLeadValues.replace(",", "','");
      }
      query.append(" and Job.hiring_lead = ? ");
    }

    if (collaboratorId > 0) {
      if (StringUtil.printValidString(hiringLeadValues).length() > 0) {
        query.append(" and Candidate.job_id in " +
            "(select job_id from Collaborators where collaborator= ? " +
            " and active_status = 1 " +
            " union select id from Job where hiring_lead = ? " +
            " and job_status!= 'Closed')");
      } else {
        query.append(" and Candidate.job_id in " +
            "(select job_id from Collaborators " +
            "where collaborator= ? and active_status = 1)");
      }
    }
    if (id > 0) {
      query.append(" and Candidate.job_id = ? ");
    }
    if (employeeId > 0) {
      query.append(" and Candidate.employee_id = ? ");
    }

    if (!StringUtils.isNullOrEmpty(searchFilter)) {
      query.append(" and ((UPPER(Candidate.first_name) like ? or UPPER(");
      query.append("Candidate.last_name) like ? ");
      query.append(" or UPPER(CONCAT(Candidate.first_name, ' ', Candidate.last_name)) like ? ");
      query.append(") or (UPPER(Job.title) like ? )");
      if (!StringUtils.isNullOrEmpty(stateId)) {
        query.append(" or (UPPER(Candidate.state) in (?) )");
      }
      query.append(")");
    }
    if (limit != 0) {
      query.append(" group by Candidate.candidate_id ");
      query.append(" order by Candidate.candidate_id desc limit ? offset ? ");
    }
    if (limit == 0) {
      query.append(" group by Candidate.candidate_id ");
      query.append(" order by Candidate.candidate_id desc ");
    }

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    int index = 0;
    preparedStatement.setLong(++index, organization);
    if (candidateValues != null && candidateValues.length() > 0) {
      preparedStatement.setString(++index, candidateValues);
    }
    if (hiringLeadValues != null && hiringLeadValues.length() > 0 && collaboratorId == 0) {
      preparedStatement.setString(++index, hiringLeadValues);
    }
    if (collaboratorId > 0) {
      if (StringUtil.printValidString(hiringLeadValues).length() > 0) {
        preparedStatement.setInt(++index, collaboratorId);
        preparedStatement.setInt(++index, collaboratorId);
      } else {
        preparedStatement.setInt(++index, collaboratorId);
      }
    }
    if (id > 0) {
      preparedStatement.setInt(++index, id);
    }
    if (employeeId > 0) {
      preparedStatement.setInt(++index, employeeId);
    }
    if (!StringUtils.isNullOrEmpty(searchFilter)) {
      preparedStatement.setString(++index, "%" + searchFilter + "%");
      preparedStatement.setString(++index, "%" + searchFilter + "%");
      preparedStatement.setString(++index, "%" + searchFilter + "%");
      preparedStatement.setString(++index, "%" + searchFilter + "%");
      if (!StringUtils.isNullOrEmpty(stateId)) {
        preparedStatement.setString(++index, stateId);
      }
    }
    if (limit != 0) {
      preparedStatement.setInt(++index, limit);
      preparedStatement.setInt(++index, offset);
    }
    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      HashMap<String, String> candidateList = new HashMap<>();
      int retrieveIndex = 0;
      candidateList.put("candidate_id", resultSet.getString(++retrieveIndex));
      candidateList.put("first_name", resultSet.getString(++retrieveIndex));
      candidateList.put("last_name", resultSet.getString(++retrieveIndex));
      candidateList.put("phone_number", resultSet.getString(++retrieveIndex));
      candidateList.put("rating", resultSet.getString(++retrieveIndex));
      candidateList.put("status", resultSet.getString(++retrieveIndex).toUpperCase());
      try {
        String date = new DateUtil().convertDateintoString(resultSet.getDate(++retrieveIndex));
        candidateList.put("created", date);

        String modifiedDate = new DateUtil().convertDateintoString(resultSet.getDate(++retrieveIndex));
        candidateList.put("modified", StringUtil.printValidString(modifiedDate));
      } catch (ParseException e) {
        e.printStackTrace();
        logger.error("parse exception");
      }
      candidateList.put("email", resultSet.getString(++retrieveIndex));
      candidateList.put("job_title", resultSet.getString(++retrieveIndex));
      if (!StringUtils.isNullOrEmpty(resultSet.getString(++retrieveIndex))) {
        candidateList.put("state", CountryStateHandler.getStateName(resultSet.getString("state")));
      } else {
        candidateList.put("state", "");
      }
      // logger.info("Candidate List fetched Successfully!");
      return candidateList;
    });
  }

  @Override
  public List<HashMap<String, String>> getCandidateFilterCount(StringBuilder selectFilter)
      throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder("SELECT Distinct Candidate.candidate_id FROM Job ,Candidate ");
    query.append(" where Candidate.job_id = Job.id and Job.job_status != 'Closed' ");
    query.append(" and Candidate.status != 'Hire' and ");
    query.append("Candidate.active_status ='1'  ");
    query.append(" and Job.organization_id = Candidate.organization_id " + selectFilter.toString() + " ");
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      int retrieveIndex = 0;
      HashMap<String, String> candidateId = new HashMap<>();
      candidateId.put("candidateId", resultSet.getString(++retrieveIndex));
      logger.info("Candidate filter count fetched Successfully!");
      return candidateId;
    });
  }

  @Override
  public int getCandidateListCount(StringBuilder selectFilter)
      throws SQLException, DbHandleException {

    StringBuilder query = new StringBuilder("SELECT Distinct Candidate.candidate_id FROM Job ,Candidate ");
    query.append(" where Candidate.job_id = Job.id and Job.job_status != 'Closed' ");
    query.append(" and Candidate.status != 'Hire' and ");
    query.append(" Candidate.active_status ='1' ");
    query.append(" and Job.organization_id = Candidate.organization_id " + selectFilter.toString() + " ");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> resultSet.getInt(1));
  }

  @Override
  public int getCandidateDateCount(long organizationId)
      throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder("SELECT count(Distinct Candidate.candidate_id) as cnt ");
    query.append(" FROM Job,Candidate where Candidate.job_id = Job.id and ");
    query.append("Job.job_status != 'Closed'  and Candidate.status != 'Hire' ");
    query.append(" and Candidate.active_status =1 and ");
    query.append(" Job.organization_id = Candidate.organization_id ");
    query.append(" and Candidate.organization_id=?");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, organizationId);
    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> resultSet.getInt(1));
  }

  @Override
  public int updateCandidateStatus(String status, long candidateId)
      throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder("update Candidate set status = ? ");
    query.append(" where candidate_id = ? and active_status = 1 ");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setString(++index, status);
    preparedStatement.setLong(++index, candidateId);
    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  @Override
  public CandidateR getUserName(long candidateId) throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder("select first_name, last_name ");
    query.append("from Candidate where candidate_id = ? ");

    CandidateR candidateR = new CandidateR();
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, candidateId);

    ResultSet resultSet = preparedStatement.executeQuery();
    while (resultSet.next()) {
      candidateR.setFirstName(resultSet.getString("first_name"));
      candidateR.setLastName(resultSet.getString("last_name"));
    }
    logger.info("User Name fetched Successfully!");
    return candidateR;
  }

  @Override
  public List<HashMap<String, String>> getCandidateTalentList(long organizationId, String format,
                                                              String searchFilter, int limit, int offset, String stateId)
      throws SQLException, DbHandleException {
    StringBuilder query;
    query = new StringBuilder("select Candidate.candidate_id,Candidate.first_name,Candidate.last_name,");
    query.append("Candidate.phone_number,Candidate.rating,Candidate.status,");
    query.append(" Candidate.email,Candidate.job_id,Candidate.state,Candidate.created,Candidate.modified ");
    query.append(" from Candidate,HiringprocessCandidate ");
    query.append(" WHERE Candidate.candidate_id = HiringprocessCandidate.candidate_id and Candidate.active_status =1 ");
    query.append(" and HiringprocessCandidate.candidate_process = 'Archive to Talent Pool' ");
    query.append(" and HiringprocessCandidate.active_status = 1 and Candidate.status ='Not Hired' ");
    query.append(" and Candidate.organization_id  = ? ");

    if (!StringUtils.isNullOrEmpty(searchFilter)) {
      // searching state id from state name
      query.append(" and ((UPPER(Candidate.first_name) like ? or UPPER(");
      query.append(" Candidate.last_name) like ?) ");
      query.append(" or (UPPER(CONCAT(Candidate.first_name, ' ', Candidate.last_name)) like ?) ");
      query.append(" or (Candidate.job_id in (select id from Job where (UPPER(Job.title) like ?))) ");
      if (!StringUtils.isNullOrEmpty(stateId)) {
        query.append(" or (UPPER(Candidate.state) in (?))");
      }
      query.append(")");
    }
    if (limit != 0) {
      query.append(" group by Candidate.candidate_id order by Candidate.candidate_id desc limit ? offset ?");
    }
    if (limit == 0) {
      query.append(" group by Candidate.candidate_id order by Candidate.candidate_id desc ");
    }

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(++index, organizationId);
    if (!StringUtils.isNullOrEmpty(searchFilter)) {
      preparedStatement.setString(++index, "%" + searchFilter + "%");
      preparedStatement.setString(++index, "%" + searchFilter + "%");
      preparedStatement.setString(++index, "%" + searchFilter + "%");
      preparedStatement.setString(++index, "%" + searchFilter + "%");
      if (!StringUtils.isNullOrEmpty(stateId)) {
        preparedStatement.setString(++index, stateId);
      }
    }
    if (limit != 0) {
      preparedStatement.setLong(++index, limit);
      preparedStatement.setLong(++index, offset);
    }
    return DbHelper.getObjectList(this.dbHandle, preparedStatement, resultSet -> {
      HashMap<String, String> candidateList = new HashMap<>();
      int retrieveIndex = 0;

      candidateList.put("candidate_id", resultSet.getString(++retrieveIndex));
      candidateList.put("first_name", resultSet.getString(++retrieveIndex));
      candidateList.put("last_name", resultSet.getString(++retrieveIndex));
      candidateList.put("phone_number", resultSet.getString(++retrieveIndex));
      candidateList.put("rating", resultSet.getString(++retrieveIndex));
      candidateList.put("status", resultSet.getString(++retrieveIndex));
      candidateList.put("email", resultSet.getString(++retrieveIndex));
      candidateList.put("job_id", resultSet.getString(++retrieveIndex));
      candidateList.put("state", resultSet.getString(++retrieveIndex));
      try {
        java.util.Date createdDate = resultSet.getDate(++retrieveIndex);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format != null ? format : "dd-MMM-yyyy");
        String dateFormat = simpleDateFormat.format(createdDate);
        candidateList.put("created_date", dateFormat);

        if (!StringUtil.printValidString(String.valueOf(resultSet.getDate("modified"))).equals("")) {
          java.util.Date modifiedDate = resultSet.getDate("modified");
          dateFormat = simpleDateFormat.format(modifiedDate);
          candidateList.put("modified_date", dateFormat);
        }

      } catch (Exception e) {
        e.printStackTrace();
      }
      logger.info("Candidate talent list fetched Successfully!");
      return candidateList;
    });

  }

  @Override
  public JsonArray getCandidateStatusValues(CandidateR candidateModel)
      throws SQLException, DbHandleException {

    JsonArray statusArray = new JsonArray();
    StringBuilder parameterizedSql = new StringBuilder("");
    parameterizedSql.append("select distinct(status) from Candidate ");
    parameterizedSql.append("where status is not null ");
    parameterizedSql.append("and active_status = ? and organization_id = ? ");

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(
        parameterizedSql.toString());
    preparedStatement.setInt(1, 1);
    preparedStatement.setLong(2, candidateModel.getOrganizationId());

    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      String status = resultSet.getString(1);
      status = Character.toUpperCase(status.charAt(0)) + status.substring(1);
      statusArray.add(status);
      logger.info("Candidate status value fetched Successfully!");
      return statusArray;
    });

  }

  @Override
  public List<JsonObject> getCandidateListBasedOnStatus(CandidateR candidateModel,
                                                        MultiMap requestParams)
      throws SQLException, DbHandleException {
    StringBuilder parameterizedSql = new StringBuilder();
    parameterizedSql.append("select c.candidate_id, c.first_name, c.last_name," +
        " j.title, c.status, c.created ");
    parameterizedSql.append("from Candidate c, Job j where j.id = c.job_id ");
    parameterizedSql.append(" and c.status is not null ");
    parameterizedSql.append("and c.active_status = ? and c.organization_id = ? ");

    String status = StringUtil.printValidString(candidateModel.getStatus());
    if (status.length() > 0) {
      parameterizedSql.append(" and c.status = ? ");
    }

    if (requestParams.get("limit") != null) {
      parameterizedSql.append(" limit " + Integer.parseInt(requestParams.get("limit")));
      if (requestParams.get("offset") != null) {
        parameterizedSql.append(" offset " + Integer.parseInt(requestParams.get("offset")));
      }
    }

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(
        parameterizedSql.toString());
    int parameterIndex = 0;
    preparedStatement.setInt(++parameterIndex, 1);
    preparedStatement.setLong(++parameterIndex, candidateModel.getOrganizationId());
    if (status.length() > 0) {
      preparedStatement.setString(++parameterIndex, status);
    }

    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      JsonObject candidateObject = new JsonObject();
      candidateObject.put("candidate_id", resultSet.getString("c.candidate_id"));
      candidateObject.put("first_name", resultSet.getString("c.first_name"));
      candidateObject.put("last_name", resultSet.getString("c.last_name"));
      candidateObject.put("job_title", resultSet.getString("j.title"));
      candidateObject.put("status", resultSet.getString("c.status"));
      candidateObject.put("applied_date", resultSet.getString("c.created"));
      // logger.info("Candidate list fetched Successfully!");
      return candidateObject;
    });
  }

  @Override
  public int getCandidatewithLoginId(long organizationId) throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder("Select id from Login ");
    query.append("where organization_id = ? and user_type='candidate' order by id desc limit 1");

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(1, organizationId);
    Integer id = DbHelper.getObject(this.dbHandle, preparedStatement, resultSet -> resultSet.getInt(1));
    logger.info("Candidate with login id fetched Successfully!");
    return null == id ? 0 : id;
  }

  /*
    This DAO method is used to retrieve the available resumes of a candidate
     @param candidateR
   * @return ObjectList
   * @throws SQLException
   * @throws DbHandleException
   */

  public List<CandidateR> getAllResumes(CandidateR candidateR)
      throws SQLException, DbHandleException {
    String parameterizedSql = "select resume,created,resume_name from Candidate where " +
        "user_id = ? and organization_id=?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setLong(1, candidateR.getUserId());
    preparedStatement.setLong(2, candidateR.getOrganizationId());
    return DbHelper.getObjectList(this.dbHandle, preparedStatement, resultSet -> {
      CandidateR data = new CandidateR();
      data.setResume(resultSet.getString(1));
      data.setCreated(resultSet.getDate(2));
      data.setResumeName(resultSet.getString(3));
      logger.info("Candidates with all resumes fetched Successfully!");
      return data;
    });
  }

  @Override
  public int create(CandidateCustomQuestionR candidateCustomQuestionR, long candidateId)
      throws SQLException, DbHandleException {
    StringBuilder parameterizedSql = new StringBuilder("insert into customQuestionCandidate(custom_question, ");
    parameterizedSql.append("custom_answer, candidate_id) ");
    parameterizedSql.append("values (?, ?, ?)");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.
        createPreparedStatement(parameterizedSql.toString(), true);
    preparedStatement.setString(++index, candidateCustomQuestionR.getCustomQuestion());
    preparedStatement.setString(++index, candidateCustomQuestionR.getCustomAnswer());
    preparedStatement.setLong(++index, candidateId);
    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, true);
  }

  @Override
  public List<HashMap<String, String>> getCustomQuestionsList(long candidateId) throws SQLException, DbHandleException {
   /* StringBuilder query = new StringBuilder("Select customquestion_id,custom_question,custom_answer " +
        " from customQuestionCandidate where candidate_id = ? and active_status = 1");*/
    StringBuilder query = new StringBuilder("select a.customquestion_id,b.custom_question,a.custom_answer from customQuestionCandidate a," +
        "CustomQuestionJob b where a.candidate_id = ? and b.custom_question_id = a.custom_question and a.active_status = 1");
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.
        createPreparedStatement(query.toString(), true);
    preparedStatement.setLong(++index, candidateId);
    return DbHelper.getObjectList(this.dbHandle, preparedStatement, resultSet -> {
      int retrieveIndex = 0;
      HashMap<String, String> statusList = new HashMap<>();
      statusList.put("customquestion_id", resultSet.getString(++retrieveIndex));
      statusList.put("custom_question", resultSet.getString(++retrieveIndex));
      statusList.put("custom_answer", resultSet.getString(++retrieveIndex));
      logger.info("Custom Question List fetched Successfully!");
      return statusList;
    });

  }

  @Override
  public long getCandidateCountForJob(long jobId) throws SQLException, DbHandleException {
    String selectQuery = "select count(candidate_id) from Candidate where job_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setLong(1, jobId);
    Integer id = DbHelper.getObject(this.dbHandle, preparedStatement, resultSet -> resultSet.getInt(1));
    logger.info("Candidate with login id fetched Successfully!");
    return null == id ? 0 : id;
  }

  /**
   * @param parameterColumn to be certain that it is not passed as user param, to be used as code param to avoid SQL injection
   * @param parameterValue
   * @param organizationId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public boolean checkFieldOptions(String parameterColumn, String parameterValue, long organizationId)
      throws SQLException, DbHandleException {
    String selectQuery = "select 1 from Candidate where " + parameterColumn + " = ? " +
        "and status != 'Hire' and status != 'Not Hired' and active_status = 1 and organization_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);

    preparedStatement.setString(1, parameterValue);
    preparedStatement.setLong(2, organizationId);

    return DbHelper.getBoolean(dbHandle, preparedStatement);
  }

  @Override
  public boolean isDocumentAssociated(String filePath) throws SQLException, DbHandleException {
    String parameterizedSql = "SELECT 1 FROM `Candidate` WHERE `resume` = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setString(1, filePath);

    return DbHelper.getBoolean(this.dbHandle, preparedStatement);
  }

  @Override
  public int candidateLoginId(String email, long organizationId) throws SQLException, DbHandleException {
    String selectQuery = "select user_id from Candidate where email =? and organization_id = ? and active_status = 1";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setString(1, email);
    preparedStatement.setLong(2, organizationId);
    Integer id = DbHelper.getObject(this.dbHandle, preparedStatement, resultSet -> resultSet.getInt(1));
    logger.info("Candidate id for getting login id!");
    return null == id ? 0 : id;
  }

  @Override
  public boolean checkJobId(long userId, long jobId, long organizationId) throws SQLException, DbHandleException {
    String selectQuery = "select 1 from Candidate where job_id =? and organization_id = ? " +
        " and active_status = 1 and user_id = ? ";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setLong(1, jobId);
    preparedStatement.setLong(2, organizationId);
    preparedStatement.setLong(3, userId);
    logger.info("Candidate id for getting login id!");
    return DbHelper.getBoolean(this.dbHandle, preparedStatement);
  }
}
