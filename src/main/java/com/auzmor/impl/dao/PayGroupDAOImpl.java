package com.auzmor.impl.dao;

import com.auzmor.dao.PayGroupDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.PayGroupModel;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PayGroupDAOImpl implements PayGroupDAO {
  private DbHandle dbHandle;
  private static Logger logger = LoggerFactory.getLogger(PayGroupDAOImpl.class);
  public PayGroupDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  /**
   * Insert query for PayGroup. Rows paygroup_name,organization_id are mandatory
   * @param payGroupModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public int insert(PayGroupModel payGroupModel) throws SQLException,DbHandleException {
    logger.info("Pay Group Name :"+payGroupModel.getPaygroupName());
    String insertQuery = "insert into PayGroup (paygroup_name,organization_id,active_status) " +
        "values (?,?,?)";

    PreparedStatement preparedStatement =
        this.dbHandle.createPreparedStatement(insertQuery, true);
    preparedStatement.setString(1, payGroupModel.getPaygroupName());
    preparedStatement.setInt(2, payGroupModel.getOrganizationId());
    preparedStatement.setString(3, "true");

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, true);
  }

  /**
   * Select query to check if pay group already exists before insert
   * @param payGroupModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public boolean isPayGroupExistsInOrganization(PayGroupModel payGroupModel)
      throws SQLException,DbHandleException{
    logger.info("Pay Group Name :"+payGroupModel.getPaygroupName());
    String parameterizedSql  = "select 1 from PayGroup where paygroup_name = ? and organization_id = ?" +
        "  and active_status = 'true'";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setString(1,payGroupModel.getPaygroupName());
    preparedStatement.setInt(2,payGroupModel.getOrganizationId());

    return DbHelper.getBoolean(this.dbHandle, preparedStatement);
  }

  /**
   * Select query for fetching pay group details of particular id
   * @param payGroupModel    contains pay group id
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  @Override
  public PayGroupModel getPayGroupDetails(PayGroupModel payGroupModel)
      throws SQLException,DbHandleException {
    logger.info("Pay Group Name :"+payGroupModel.getPaygroupId());
    String parameterizedSql = "select paygroup_id,paygroup_name,organization_id from PayGroup where paygroup_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setInt(1, payGroupModel.getPaygroupId());
    
    return DbHelper.getObject(this.dbHandle, preparedStatement, resultSet -> {
      payGroupModel.setPaygroupId(resultSet.getInt(1));
      payGroupModel.setPaygroupName(resultSet.getString(2));
      payGroupModel.setOrganizationId(resultSet.getInt(3));
      return payGroupModel;
    });
  }

  /**
   * Get list of pay group for particular organization
   * @param payGroupModel     contains organization id
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  @Override
  public List<PayGroupModel> getAllPayGroups(PayGroupModel payGroupModel)
      throws SQLException,DbHandleException {
    logger.info("Pay Group Name :"+payGroupModel.getPaygroupId());
    String parameterizedSql = "select paygroup_id,paygroup_name,organization_id from PayGroup " +
        "where paygroup_id = ? " +
        "and active_status = 'true'";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setInt(1, payGroupModel.getOrganizationId());
    
    return DbHelper.getObjectList(this.dbHandle, preparedStatement, resultSet -> {
      PayGroupModel resultModel = new PayGroupModel();
      resultModel.setPaygroupId(resultSet.getInt(1));
      resultModel.setPaygroupName(resultSet.getString(2));
      resultModel.setOrganizationId(resultSet.getInt(3));
      return resultModel;
    });
  }

  /**
   * Update columns of pay group table
   * @param requestParams
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  @Override
  public int updatePayGroupDetails(MultiMap requestParams) throws SQLException,
      DbHandleException {
    logger.info("Available Pay Group Id :"+requestParams.get("paygroupId"));
    /*StringBuilder stringBuilder = new StringBuilder();
    int i = 1;
    int length = requestParams.size();
    List<String> listOfParams = new ArrayList<String>();
    for(String key : requestParams.names()) {
      if(!key.equals("paygroupId")) {
        stringBuilder.append(key + " = ?");
        listOfParams.add(requestParams.get(key));
      }


      if (i < length && i != 1) {
        stringBuilder.append(",");
      }
      i++;
    }
*/
    int columnIndex = 1;
    String parameterizedSql = "update PayGroup set paygroup_name = ? where paygroup_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);

    preparedStatement.setString(columnIndex, requestParams.get("paygroup_name"));
    columnIndex++;
    preparedStatement.setInt(columnIndex,
        Integer.valueOf(requestParams.get("paygroupId")));

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  /**
   * Delete pay group entry
   * @param payGroupModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  @Override
  public int deletePayGroup(PayGroupModel payGroupModel) throws SQLException,
      DbHandleException {
    logger.info("Deleted Pay Group"+payGroupModel.getPaygroupId());
    String paramaterizedSql = "update PayGroup set active_status = 'false' where paygroup_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(paramaterizedSql);
    preparedStatement.setInt(1, payGroupModel.getPaygroupId());

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }
}
