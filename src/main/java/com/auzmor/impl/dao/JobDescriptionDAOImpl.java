package com.auzmor.impl.dao;

import com.auzmor.dao.JobDescriptionDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.JobDescriptionModel;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class JobDescriptionDAOImpl implements JobDescriptionDAO {

  private DbHandle dbHandle;

  public JobDescriptionDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  @Override
  public boolean isJobDescriptionAlreadyExists(JobDescriptionModel jobDescriptionModel)
      throws SQLException, DbHandleException {
    String selectQuery = "select 1 from JobDescription where jobdesctype = ? and organization_id = ? " +
        "and inactive_status = 0";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setString(1, jobDescriptionModel.getJobdesctype());
    preparedStatement.setLong(2, jobDescriptionModel.getOrganizationId());

    return DbHelper.getBoolean(this.dbHandle, preparedStatement);
  }

  @Override
  public int insert(JobDescriptionModel jobDescriptionModel) throws SQLException, DbHandleException {
    String insertQuery = "insert into JobDescription (jobdesctype,jobdescdetails,organization_id) " +
        "values (?,?,?)";

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(insertQuery, true);
    preparedStatement.setString(1,jobDescriptionModel.getJobdesctype());
    preparedStatement.setString(2,jobDescriptionModel.getJobdescdetails());
    preparedStatement.setLong(3,jobDescriptionModel.getOrganizationId());

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, true);
  }

  @Override
  public List<JobDescriptionModel> getAllJobDescriptions(JobDescriptionModel jobDescriptionModel)
      throws SQLException, DbHandleException {
    String selectQuery = "select jobdesctype,jobdescId,jobdescdetails,organization_id FROM JobDescription " +
        "where organization_id = ? and inactive_status = 0 ORDER BY jobdescId";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setLong(1, jobDescriptionModel.getOrganizationId());

    return DbHelper.getObjectList(this.dbHandle, preparedStatement, resultSet -> {
      JobDescriptionModel dataModel = new JobDescriptionModel();
      dataModel.setJobdesctype(resultSet.getString(1));
      dataModel.setJobdescId(resultSet.getLong(2));
      dataModel.setJobdescdetails(resultSet.getString(3));
      dataModel.setOrganizationId(resultSet.getLong(4));
      return dataModel;
    });
  }

  @Override
  public JsonObject getJobDescription(JobDescriptionModel jobDescriptionModel)
      throws SQLException, DbHandleException {
    String selectQuery = "select jobdesctype,jobdescId,jobdescdetails from JobDescription " +
        "where jobdescId = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setLong(1, jobDescriptionModel.getJobdescId());

    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      JsonObject dataObject = new JsonObject();
      dataObject.put("jobdesctype", resultSet.getString(1));
      dataObject.put("jobdescId", resultSet.getLong(2));
      dataObject.put("jobdescdetails", resultSet.getString(3));
      return dataObject;
    });
  }

  @Override
  public int update(JobDescriptionModel jobDescriptionModel) throws SQLException, DbHandleException {
    String updateQuery = "update JobDescription set jobdesctype = ? , jobdescdetails = ? " +
        "where jobdescId = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(updateQuery);
    preparedStatement.setString(1, jobDescriptionModel.getJobdesctype());
    preparedStatement.setString(2, jobDescriptionModel.getJobdescdetails());
    preparedStatement.setLong(3, jobDescriptionModel.getJobdescId());

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  @Override
  public int delete(JobDescriptionModel jobDescriptionModel) throws SQLException, DbHandleException {
    String updateQuery = "update JobDescription set inactive_status = 1 " +
        "where jobdescId = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(updateQuery);
    preparedStatement.setLong(1, jobDescriptionModel.getJobdescId());

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  @Override
  public JobDescriptionModel getJobDescriptionDetails(JobDescriptionModel jobDescriptionModel)
      throws SQLException, DbHandleException {
    String selectQuery = "select jobdescId,jobdesctype,jobdescdetails,organization_id from JobDescription " +
        "where jobdescId = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setLong(1, jobDescriptionModel.getJobdescId());

    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      JobDescriptionModel dataModel = new JobDescriptionModel();
      dataModel.setJobdescId(resultSet.getLong(1));
      dataModel.setJobdesctype(resultSet.getString(2));
      dataModel.setJobdescdetails(resultSet.getString(3));
      dataModel.setOrganizationId(resultSet.getLong(4));
      return dataModel;
    });
  }
}
