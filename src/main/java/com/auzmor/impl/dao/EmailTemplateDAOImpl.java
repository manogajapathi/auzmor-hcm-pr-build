package com.auzmor.impl.dao;

import com.auzmor.dao.EmailTemplateDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.EmailTemplate;
import com.auzmor.util.db.DbHandle;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class EmailTemplateDAOImpl implements EmailTemplateDAO {
  private static Logger logger = LoggerFactory.getLogger(EmailTemplateDAOImpl.class);
  private DbHandle dbHandle;

  public EmailTemplateDAOImpl(DbHandle dbHandle) {
    this.dbHandle =  dbHandle;
  }

  /**
   *
   * @param name
   * @param template
   * @param organizationId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 04/MAR/2018
   */
  @Override
  public long create(String name, String subject, String header, String footer, String template,
                     long organizationId) throws SQLException, DbHandleException {
    logger.info("Adding a email template to DB");
    StringBuilder parameterizedSQL = new StringBuilder("INSERT INTO `EmailTemplate` (`name`, `subject`, ");
    parameterizedSQL.append("`header`, `footer`, `template`, `organization_id`) VALUES (?, ?, ?, ?, ?, ?)");

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSQL
        .toString(), true);
    int index = 0;
    preparedStatement.setString(++index, name);
    preparedStatement.setString(++index, subject);
    preparedStatement.setString(++index, header);
    preparedStatement.setString(++index, footer);
    preparedStatement.setString(++index, template);
    preparedStatement.setLong(++index, organizationId);

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, true);
  }

  /**
   *
   * @param name
   * @param template
   * @param templateId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 04/MAR/2018
   */
  @Override
  public int update(String name, String subject, String header, String footer, String template,
                    long templateId) throws SQLException,
      DbHandleException {
    logger.info("Updating a email template to DB");
    StringBuilder parameterizedSQL = new StringBuilder("UPDATE `EmailTemplate` SET `name` = ?,");
    parameterizedSQL.append("`subject` = ?, `header` = ?, `footer` = ?, `template` = ? ");
    parameterizedSQL.append("WHERE `id` = ?");

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSQL
        .toString());
    int index = 0;

    preparedStatement.setString(++index, name);
    preparedStatement.setString(++index, subject);
    preparedStatement.setString(++index, header);
    preparedStatement.setString(++index, footer);
    preparedStatement.setString(++index, template);
    preparedStatement.setLong(++index, templateId);

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  /**
   *
   * @param templateId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 04/MAR/2018
   */
  @Override
  public int delete(long templateId) throws SQLException, DbHandleException {
    logger.info("Deleting a email template from DB");
    StringBuilder parameterizedSQL = new StringBuilder("UPDATE `EmailTemplate` SET `deleted` = TRUE");
    parameterizedSQL.append(" WHERE `id` = ?");
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSQL
        .toString());
    int index = 0;
    preparedStatement.setLong(++index, templateId);

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  /**
   *
   * @param templateId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 04/MAR/2018
   */
  @Override
  public EmailTemplate getTemplate(long templateId) throws SQLException, DbHandleException {
    logger.info("Get email template from DB");
    StringBuilder parameterizedSQL = new StringBuilder("SELECT `id`, `name`, `subject`, `header`, ");
    parameterizedSQL.append("`footer`, `template`, `organization_id` FROM `EmailTemplate` WHERE `id` = ?");
    parameterizedSQL.append(" AND `deleted` = FALSE");

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSQL
        .toString());
    int index = 0;
    preparedStatement.setLong(++index, templateId);
    return DbHelper.getObject(this.dbHandle, preparedStatement, resultSet -> {
      EmailTemplate emailTemplate = new EmailTemplate();
      int columnIndex = 0;
      emailTemplate.setId(resultSet.getLong(++columnIndex));
      emailTemplate.setName(resultSet.getString(++columnIndex));
      emailTemplate.setSubject(resultSet.getString(++columnIndex));
      emailTemplate.setHeader(resultSet.getString(++columnIndex));
      emailTemplate.setFooter(resultSet.getString(++columnIndex));
      emailTemplate.setTemplate(resultSet.getString(++columnIndex));
      emailTemplate.setOrganizationId(resultSet.getLong(++columnIndex));

      return emailTemplate;
    });
  }

  /**
   *
   * @param organizationId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 04/MAR/2018
   */
  @Override
  public List<EmailTemplate> getTemplates(long organizationId) throws SQLException,
      DbHandleException {
    logger.info("Get email templates from DB with organizationid");
    StringBuilder parameterizedSQL = new StringBuilder("SELECT `id`, `name`, `subject`, `header`, ");
    parameterizedSQL.append("`footer`, `template` FROM `EmailTemplate` WHERE `organization_id` = ?");
    parameterizedSQL.append(" AND `deleted` = FALSE");

    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSQL
        .toString());
    int index = 0;
    preparedStatement.setLong(++index, organizationId);

    return DbHelper.getObjectList(this.dbHandle, preparedStatement, resultSet -> {
      EmailTemplate emailTemplate = new EmailTemplate();
      int columnIndex = 0;
      emailTemplate.setId(resultSet.getLong(++columnIndex));
      emailTemplate.setName(resultSet.getString(++columnIndex));
      emailTemplate.setSubject(resultSet.getString(++columnIndex));
      emailTemplate.setHeader(resultSet.getString(++columnIndex));
      emailTemplate.setFooter(resultSet.getString(++columnIndex));
      emailTemplate.setTemplate(resultSet.getString(++columnIndex));

      return emailTemplate;
    });
  }
}
