package com.auzmor.impl.dao;

import com.auzmor.dao.EmployeePTORequestsDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.PTO.EmployeePTORequests;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.json.JsonObject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class EmployeePTORequestDAOImpl implements EmployeePTORequestsDAO {
  private DbHandle dbHandle;

  public EmployeePTORequestDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  /**
   * Insert into Employee_PTO_Requests table. Rows employee_id,from_date,to_date,hours,
   * time_off_type are mandatory
   * @param employeePTORequests
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  @Override
  public int addRequest(EmployeePTORequests employeePTORequests)
      throws SQLException, DbHandleException {
    String insertQuery = "insert into Employee_PTO_Requests (employee_id,from_date,to_date,hours," +
        "time_off_type,description) values (?,?,?,?,?,?)";
    PreparedStatement preparedStatement = 
        this.dbHandle.createPreparedStatement(insertQuery, true);
    int index = 0;
    preparedStatement.setLong(++index, employeePTORequests.getEmployeeId());
    preparedStatement.setDate(++index, employeePTORequests.getFromDate());
    preparedStatement.setDate(++index, employeePTORequests.getToDate());
    preparedStatement.setDouble(++index, employeePTORequests.getHours());
    preparedStatement.setString(++index, employeePTORequests.getTimeOffType());
    preparedStatement.setString(++index, employeePTORequests.getDescription());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, true);
  }

  /**
   * To check if request already exists for employee before insert
   * @param employeePTORequests
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  @Override
  public boolean alreadyExists(EmployeePTORequests employeePTORequests)
      throws SQLException, DbHandleException {
    String parameterisedSql = "select 1 from Employee_PTO_Requests where employee_id = ? and " +
        "from_date = ? and to_date = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterisedSql);
    preparedStatement.setLong(1, employeePTORequests.getEmployeeId());
    preparedStatement.setDate(2, employeePTORequests.getFromDate());
    preparedStatement.setDate(3, employeePTORequests.getToDate());

    return DbHelper.getBoolean(dbHandle, preparedStatement);
  }

  /**
   * Fetch used hours count based on employee_id
   * @param employeePTORequests
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  @Override
  public double getUsedHours(EmployeePTORequests employeePTORequests)
      throws SQLException, DbHandleException {
    String hoursQuery = "select sum(hours) as hours_count from Employee_PTO_Requests " +
        "where employee_id = ? and request_status in (0,1) and time_off_type = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(hoursQuery);
    preparedStatement.setLong(1, employeePTORequests.getEmployeeId());
    preparedStatement.setString(2, employeePTORequests.getTimeOffType());

    double hoursCount = 0.0d;
    ResultSet resultSet = preparedStatement.executeQuery();
    while (resultSet.next()) {
      hoursCount = resultSet.getDouble("hours_count");
    }
    if(preparedStatement != null && !preparedStatement.isClosed()) {
      preparedStatement.close();
    }

    if (resultSet!=null && !resultSet.isClosed()) {
      resultSet.close();
    }
    return hoursCount;
  }

  /**
   * Fetch all PTO requests based on employee_id
   * @param employeePTORequests
   * @return
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public List<JsonObject> allRequests(EmployeePTORequests employeePTORequests)
      throws SQLException, DbHandleException {
    String parameterisedSql = "select id,employee_id,from_date,to_date,time_off_type,hours," +
        "description,request_status,submitted_date,approved_by,approved_date " +
        "from Employee_PTO_Requests where employee_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterisedSql);
    preparedStatement.setLong(1, employeePTORequests.getEmployeeId());

    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      JsonObject jsonObject = new JsonObject();
      jsonObject.put("id", resultSet.getLong("id"));
      jsonObject.put("employee_id", resultSet.getLong("employee_id"));
      jsonObject.put("from_date", resultSet.getString("from_date"));
      jsonObject.put("to_date", resultSet.getString("to_date"));
      jsonObject.put("time_off_type", resultSet.getString("time_off_type"));
      jsonObject.put("hours", resultSet.getDouble("hours"));
      jsonObject.put("description", resultSet.getString("description"));
      jsonObject.put("request_status", resultSet.getInt("request_status"));
      jsonObject.put("submitted_date", resultSet.getString("submitted_date"));
      jsonObject.put("approved_by", resultSet.getLong("approved_by"));
      jsonObject.put("approved_date", resultSet.getString("approved_date"));
      return jsonObject;
    });
  }

  /**
   * Fetch all used requests details based on employee_id
   * @param employeePTORequests
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  @Override
  public List<JsonObject> allUsed(EmployeePTORequests employeePTORequests)
      throws SQLException, DbHandleException {
    String parameterisedSql = "select id,employee_id,from_date,to_date,time_off_type,hours,description," +
        "request_status from Employee_PTO_Requests where employee_id = ? and request_status = 1";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterisedSql);
    preparedStatement.setLong(1, employeePTORequests.getEmployeeId());

    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      JsonObject jsonObject = new JsonObject();
      jsonObject.put("id", resultSet.getLong("id"));
      jsonObject.put("employee_id", resultSet.getLong("employee_id"));
      jsonObject.put("from_date", resultSet.getString("from_date"));
      jsonObject.put("to_date", resultSet.getString("to_date"));
      jsonObject.put("time_off_type", resultSet.getString("time_off_type"));
      jsonObject.put("hours", resultSet.getDouble("hours"));
      jsonObject.put("description", resultSet.getString("description"));
      jsonObject.put("request_status", resultSet.getInt("request_status"));
      return jsonObject;
    });
  }

  /**
   * Fetch upcoming PTO requests approved based on employee_id
   * @param employeePTORequests
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  @Override
  public List<JsonObject> upcomingPTO(EmployeePTORequests employeePTORequests)
      throws SQLException, DbHandleException {
    String parameterizedSql = "select id,from_date,to_date,time_off_type,hours,description from " +
        "Employee_PTO_Requests where to_date > CURRENT_TIMESTAMP " +
        "and request_status = 1 and employee_id =?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setLong(1, employeePTORequests.getEmployeeId());

    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      JsonObject jsonObject = new JsonObject();
      jsonObject.put("id", resultSet.getLong("id"));
      jsonObject.put("from_date", resultSet.getString("from_date"));
      jsonObject.put("to_date", resultSet.getString("to_date"));
      jsonObject.put("time_off_type", resultSet.getString("time_off_type"));
      jsonObject.put("hours", resultSet.getDouble("hours"));
      jsonObject.put("description", resultSet.getString("description"));
      return jsonObject;
    });
  }
}