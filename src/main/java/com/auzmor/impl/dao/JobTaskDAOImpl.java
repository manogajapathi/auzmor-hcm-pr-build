package com.auzmor.impl.dao;

import com.auzmor.dao.JobTaskDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.Job.JobTaskModel;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.json.JsonObject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

public class JobTaskDAOImpl implements JobTaskDAO {
  private DbHandle dbHandle;

  public JobTaskDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  /**
   * Insert row into Job_Tasks table. Rows job_id,employee_id,task_name,
   * either(complete_within or due_date) are mandatory
   *
   * @param jobTaskModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 20/MAR/2018
   */
  @Override
  public int createTask(JobTaskModel jobTaskModel) throws SQLException, DbHandleException {
    String parameterizedSql = "insert into Job_Tasks (job_id,employee_id,candidate_id,task_name,"
        + "complete_within,due_date,all_flag,creator_id) values (?,?,?,?,?,?,?,?) ";

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setLong(++index, jobTaskModel.getJobId());
    preparedStatement.setLong(++index, jobTaskModel.getEmployeeId());
    preparedStatement.setLong(++index, jobTaskModel.getCandidateId());
    preparedStatement.setString(++index, jobTaskModel.getTaskName());
    preparedStatement.setInt(++index, jobTaskModel.getCompleteWithin());

    if (jobTaskModel.getDueDate().toString().equals(new java.sql.Date(1).toString())) {
      preparedStatement.setNull(++index, Types.DATE);
    } else {
      preparedStatement.setDate(++index, jobTaskModel.getDueDate());
    }
    preparedStatement.setBoolean(++index, jobTaskModel.isAllFlag());
    preparedStatement.setLong(++index, jobTaskModel.getCreatorId());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  /**
   * To check if job task has been already assign to employee/collaborator
   *
   * @param jobTaskModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 20/MAR/2018
   */
  @Override
  public boolean isTaskAlreadyExists(JobTaskModel jobTaskModel)
      throws SQLException, DbHandleException {
    String parameterizedSql = "select 1 from Job_Tasks where job_id = ? and employee_id = ? " +
        "and task_name = ? and complete_flag = 0 and inactive_status = 0";
    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setLong(++index, jobTaskModel.getJobId());
    preparedStatement.setLong(++index, jobTaskModel.getEmployeeId());
    preparedStatement.setString(++index, jobTaskModel.getTaskName());

    return DbHelper.getBoolean(dbHandle, preparedStatement);
  }

  /**
   * Fetch job tasks details based on employee_id
   *
   * @param jobTaskModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 20/MAR/2018
   */
  @Override
  public List<JsonObject> getTasksForEmployee(JobTaskModel jobTaskModel)
      throws SQLException, DbHandleException {
    String parameterizedSql = "select Job_Tasks.id, Job_Tasks.task_name, Job_Tasks.candidate_id, " +
        "Job_Tasks.complete_within, Job_Tasks.due_date, Job_Tasks.all_flag, Job.title, Job_Tasks.job_id\n" +
        " from Job_Tasks, Job where Job_Tasks.job_id = Job.id and Job_Tasks.complete_flag = 0 " +
        "and Job_Tasks.inactive_status=0 and Job_Tasks.employee_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setLong(1, jobTaskModel.getEmployeeId());

    List<JsonObject> resultList = new ArrayList<JsonObject>();
    ResultSet resultSet = preparedStatement.executeQuery();

    while (resultSet.next()) {
      JsonObject jsonObject = new JsonObject();
      jsonObject.put("task_id", resultSet.getLong("id"));
      jsonObject.put("task_name", resultSet.getString("task_name"));
      jsonObject.put("candidate_id", resultSet.getLong("candidate_id"));
      jsonObject.put("complete_within", resultSet.getString("complete_within"));
      jsonObject.put("due_date", resultSet.getString("due_date"));
      jsonObject.put("all_flag", resultSet.getBoolean("all_flag"));
      jsonObject.put("position", resultSet.getString("title"));
      jsonObject.put("job_id", resultSet.getLong("job_id"));
      resultList.add(jsonObject);
    }

    return resultList;
  }


  @Override
  public List<Long> getTaskIdsForEmployee(long employeeId) throws SQLException, DbHandleException {
    String parameterizedSql = "select Job_Tasks.id from Job_Tasks, Job " +
        "where Job_Tasks.job_id = Job.id and Job_Tasks.complete_flag = 0 " +
        "and Job_Tasks.inactive_status=0 and Job_Tasks.employee_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setLong(1, employeeId);

    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> resultSet.getLong(1));
  }

  /**
   * Mark complete flag for job tasks based on array of task_id's
   *
   * @param taskArray
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 20/MAR/2018
   */
  @Override
  public int updateTask(String taskArray) throws SQLException, DbHandleException {

    StringBuilder paramBuilder = new StringBuilder();
    List<Integer> integers = new ArrayList<Integer>();

    String[] splitter = taskArray.toString().split(",");
    for (String value : splitter) {
      paramBuilder.append("?,");
      integers.add(Integer.valueOf(value));
    }
    paramBuilder.deleteCharAt(paramBuilder.length() - 1);

    String updateQuery = "update Job_Tasks set complete_flag = 1 where id in (" + paramBuilder.toString() + ")";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(updateQuery);
    int index = 0;

    for (int i = 0; i < integers.size(); i++) {
      preparedStatement.setInt(++index, integers.get(i));
    }
    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  /**
   * Fetch job_tasks based on job_id
   *
   * @param jobId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 20/MAR/2018
   */
  @Override
  public JsonObject getJobTaskForJob(long jobId) throws SQLException, DbHandleException {
    String jobQuery = "select task_name,job_id,employee_id,complete_within,creator_id,due_date from " +
        "Job_Tasks where job_id = ? and all_flag = 1 and inactive_status = 0";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(jobQuery);
    preparedStatement.setLong(1, jobId);

    ResultSet resultSet = preparedStatement.executeQuery();
    JsonObject jsonObject = new JsonObject();
    while (resultSet.next()) {
      jsonObject.put("task_name", resultSet.getString("task_name"));
      jsonObject.put("job_id", resultSet.getLong("job_id"));
      jsonObject.put("employee_id", resultSet.getInt("employee_id"));
      jsonObject.put("complete_within", resultSet.getLong("complete_within"));
      jsonObject.put("creator_id", resultSet.getLong("creator_id"));
      jsonObject.put("due_date", resultSet.getString("due_date"));
    }

    if (null != resultSet || !resultSet.isClosed()) {
      resultSet.close();
    }
    if (!preparedStatement.isClosed()) {
      preparedStatement.close();
    }
    return jsonObject;
  }

  @Override
  public JsonObject getJobTaskDetails(JobTaskModel jobTaskModel)
      throws SQLException, DbHandleException {
    String selectQuery = "select employee_id,task_name,candidate_id,creator_id from Job_Tasks " +
        "where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setLong(1, jobTaskModel.getId());

    ResultSet resultSet = preparedStatement.executeQuery();
    JsonObject jsonObject = new JsonObject();
    while (resultSet.next()) {
      jsonObject.put("task_name", resultSet.getString("task_name"));
      jsonObject.put("employee_id", resultSet.getLong("employee_id"));
      jsonObject.put("candidate_id", resultSet.getLong("candidate_id"));
      jsonObject.put("creator_id", resultSet.getLong("creator_id"));
    }

    if (null != resultSet || !resultSet.isClosed()) {
      resultSet.close();
    }
    if (!preparedStatement.isClosed()) {
      preparedStatement.close();
    }
    return jsonObject;
  }

  @Override
  public int updateCompletedFlag(long employeeId) throws SQLException, DbHandleException {
    StringBuilder query = new StringBuilder("update Job_Tasks set complete_flag = 1 " +
        " where employee_id = ? and inactive_status = 0 ");
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(query.toString());
    preparedStatement.setLong(1, employeeId);
    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  public JsonObject getJobTaskForCandidateInterview(JobTaskModel jobTaskModel)
      throws SQLException, DbHandleException {
    String selectQuery = "select id,employee_id,task_name,job_id,due_date,complete_within,creator_id" +
        ", candidate_id from Job_Tasks where candidate_id = ?  and task_name = ? " +
        "and complete_flag = 0 and inactive_status = 0";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setLong(1, jobTaskModel.getCandidateId());
    preparedStatement.setString(2, jobTaskModel.getTaskName());

    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      JsonObject dataObject = new JsonObject();
      dataObject.put("id", resultSet.getLong(1));
      dataObject.put("employee_id", resultSet.getLong(2));
      dataObject.put("task_name", resultSet.getString(3));
      dataObject.put("job_id", resultSet.getLong(4));
      dataObject.put("due_date", resultSet.getString(5));
      dataObject.put("complete_within", resultSet.getLong(6));
      dataObject.put("creator_id", resultSet.getLong(7));
      dataObject.put("candidate_id", resultSet.getLong(8));
      return dataObject;
    });
  }

  public int updateTaskForInterview(JobTaskModel jobTaskModel) throws SQLException, DbHandleException {
    String updateQuery = "update Job_Tasks set employee_id = ?, due_date = ? " +
        "where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(updateQuery);
    preparedStatement.setLong(1, jobTaskModel.getEmployeeId());
    preparedStatement.setDate(2, jobTaskModel.getDueDate());
    preparedStatement.setLong(3, jobTaskModel.getId());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }
}
