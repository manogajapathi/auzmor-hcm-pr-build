package com.auzmor.impl.dao;

import com.auzmor.dao.InviteDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.Invite;
import com.auzmor.util.db.DbHandle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 */
public class InviteDAOImpl implements InviteDAO {
  private static Logger logger = LoggerFactory.getLogger(InviteDAOImpl.class);
  private DbHandle dbHandle;

  public InviteDAOImpl(DbHandle dbHandle) {
    this.dbHandle =  dbHandle;
  }

  /**
   *
   * @param invite
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public int create(Invite invite) throws SQLException, DbHandleException {
    logger.info("Adding invite in DB");
    String parameterizedSql = "INSERT INTO Invite(`organization_id`, `email`, `key`" +
        ") VALUES (?, ?, ?)";

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql
        .toString(), true);
    preparedStatement.setLong(++index, invite.getOrganizationId());
    preparedStatement.setString(++index, invite.getEmail());
    preparedStatement.setString(++index, invite.getKey());

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, true);
  }


  @Override
  public Invite getInvite(String inviteKey) throws SQLException, DbHandleException {
    logger.info("Get invite for " + inviteKey + " from DB");
    StringBuilder parameterizedSql = new StringBuilder("SELECT `id`, `key`, `email`, ");
    parameterizedSql.append("`organization_id`, `pending` FROM `Invite` WHERE `deleted` = FALSE");
    parameterizedSql.append(" AND `key` = ?");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql
        .toString());
    preparedStatement.setString(++index, inviteKey);

    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      int retrieveIndex = 0;
      Invite invite = new Invite();
      invite.setId(resultSet.getLong(++retrieveIndex));
      invite.setKey(resultSet.getString(++retrieveIndex));
      invite.setEmail(resultSet.getString(++retrieveIndex));
      invite.setOrganizationId(resultSet.getLong(++retrieveIndex));
      invite.setPending(resultSet.getBoolean(++retrieveIndex));

      return invite;
    });
  }

  @Override
  public Invite getActiveInviteForEmail(String email, long organizationId)
      throws SQLException, DbHandleException {

    logger.info("Checking invite for the email in db");
    StringBuilder parameterizedSql = new StringBuilder("SELECT `id`, `key`, `email`, ");
    parameterizedSql.append("`organization_id`, `pending` FROM `Invite` WHERE `email` = ?");
    parameterizedSql.append(" AND `organization_id` = ? AND `deleted` = FALSE");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql
        .toString());
    preparedStatement.setString(++index, email);
    preparedStatement.setLong(++index, organizationId);

    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      int retrieveIndex = 0;
      Invite invite = new Invite();
      invite.setId(resultSet.getLong(++retrieveIndex));
      invite.setKey(resultSet.getString(++retrieveIndex));
      invite.setEmail(resultSet.getString(++retrieveIndex));
      invite.setOrganizationId(resultSet.getLong(++retrieveIndex));
      invite.setPending(resultSet.getBoolean(++retrieveIndex));

      return invite;
    });
  }
  /**
   *
   * @param email
   * @param organizationId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public boolean hasInviteForEmail(String email, long organizationId) throws SQLException,
      DbHandleException {

    logger.info("Checking the invite is found in DB");
    StringBuilder parameterizedSql = new StringBuilder("SELECT 1 FROM `Invite` WHERE `email` = ? ");
    parameterizedSql.append(" AND `organization_id` = ? AND `pending` = true AND ");
    parameterizedSql.append("`deleted` = false");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql
        .toString());
    preparedStatement.setString(++index, email);
    preparedStatement.setLong(++index, organizationId);

    return DbHelper.getBoolean(this.dbHandle, preparedStatement);
  }


  /**
   *
   * @param email
   * @param organizationId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public boolean isInvitePending(String inviteKey, String email, Long organizationId)
      throws SQLException, DbHandleException {

    logger.info("Checking the invite for the key " + inviteKey);
    StringBuilder parameterizedSql = new StringBuilder("SELECT 1 FROM `Invite` WHERE `key` = ");
    parameterizedSql.append("? AND ");
    if (null != email) {
      logger.info(" for the email " + email);
      parameterizedSql.append("`email` = ? AND ");
    }
    logger.info("in db");
    parameterizedSql.append("`organization_id` = ? AND `pending` = true AND ");
    parameterizedSql.append("`deleted` = FALSE");

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql
        .toString());
    preparedStatement.setString(++index, inviteKey);
    if (null != email) {
      preparedStatement.setString(++index, email);
    }
    preparedStatement.setLong(++index, organizationId);

    return DbHelper.getBoolean(this.dbHandle, preparedStatement);
  }

  /**
   *
   * @param inviteKey
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public int updateInviteAsAccepted(String inviteKey) throws SQLException, DbHandleException {
    logger.info("update invite in db");
    String parameterizedSql = "UPDATE `Invite` SET `pending` = FALSE WHERE `key` = ?";

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setString(++index, inviteKey);

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }

  /**
   *
   * @param invite
   * @return
   * @throws SQLException
   * @throws DbHandleException
   */
  public int delete(Invite invite) throws SQLException, DbHandleException {
    logger.info("Delete an invite from db");
    String parameterizedSql = "DELETE FROM `Invite` SET `deleted` = 1 WHERE id = ?";

    int index = 0;
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(parameterizedSql);
    preparedStatement.setLong(++index, invite.getId());

    return DbHelper.executeUpdate(this.dbHandle, preparedStatement, false);
  }
}
