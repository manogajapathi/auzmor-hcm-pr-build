package com.auzmor.impl.dao;

import com.auzmor.dao.PTOCommentsDAO;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.PTO.PTOComments;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class PTOCommentsDAOImpl implements PTOCommentsDAO{
  private DbHandle dbHandle;

  public PTOCommentsDAOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  @Override
  public boolean alreadyExists(PTOComments ptoComments) throws SQLException, DbHandleException {
    String sqlQuery = "select 1 from PTO_Comments where pto_id = ? and comment_description =? " +
        "and creator_id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(sqlQuery);
    preparedStatement.setLong(1, ptoComments.getPtoId());
    preparedStatement.setString(2, ptoComments.getCommentDescription());
    preparedStatement.setLong(3, ptoComments.getCreatorId());

    return DbHelper.getBoolean(dbHandle, preparedStatement);
  }

  @Override
  public int addComment(PTOComments ptoComments) throws SQLException, DbHandleException {
    String insertQuery = "insert into PTO_Comments (pto_id,comment_description,creator_id," +
        "attachment_file) values (?,?,?,?)";
    int index = 0;
    PreparedStatement preparedStatement = 
        this.dbHandle.createPreparedStatement(insertQuery, true);
    preparedStatement.setLong(++index, ptoComments.getPtoId());
    preparedStatement.setString(++index, ptoComments.getCommentDescription());
    preparedStatement.setLong(++index, ptoComments.getCreatorId());
    preparedStatement.setString(++index, ptoComments.getCommentsFilePath());
    
    return DbHelper.executeUpdate(dbHandle, preparedStatement, true);
  }

  @Override
  public List<JsonObject> getAllComments(PTOComments ptoComments) throws SQLException, DbHandleException {
    JsonArray jsonArray = new JsonArray();
    String commentQuery = "select id,pto_id,comment_description,creator_id,created_date," +
        "attachment_file from PTO_Comments where pto_id = ? and inactive_status = 0 " +
        "order by created_date desc";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(commentQuery);
    preparedStatement.setLong(1, ptoComments.getPtoId());

    return DbHelper.getObjectList(dbHandle, preparedStatement, resultSet -> {
      JsonObject jsonObject = new JsonObject();
      jsonObject.put("id", resultSet.getLong("id"));
      jsonObject.put("pto_id", resultSet.getLong("pto_id"));
      jsonObject.put("comment_description", resultSet.getString("comment_description"));
      jsonObject.put("creator_id", resultSet.getLong("creator_id"));
      jsonObject.put("created_date", resultSet.getString("created_date"));
      jsonObject.put("attachment_file", resultSet.getString("attachment_file"));
      return jsonObject;
    });
  }

  @Override
  public int deleteComment(PTOComments ptoComments) throws SQLException, DbHandleException {
    String deleteQuery = "update PTO_Comments set inactive_status = 1 where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(deleteQuery);
    preparedStatement.setLong(1, ptoComments.getId());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }

  @Override
  public PTOComments getPTOComment(PTOComments ptoComments) throws SQLException, DbHandleException {
    String selectQuery = "select id,pto_id,creator_id,comment_description,attachment_file from " +
        "PTO_Comments where id = ? and inactive_status = 0";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(selectQuery);
    preparedStatement.setLong(1, ptoComments.getId());

    return DbHelper.getObject(dbHandle, preparedStatement, resultSet -> {
      PTOComments ptoComments1 = new PTOComments();
      ptoComments1.setId(resultSet.getLong("id"));
      ptoComments1.setPtoId(resultSet.getLong("pto_id"));
      ptoComments1.setCreatorId(resultSet.getLong("creator_id"));
      ptoComments1.setCommentDescription
          (StringUtil.printValidString(resultSet.getString("comment_description")));
      ptoComments1.setCommentsFilePath(StringUtil.printValidString
          (resultSet.getString("attachment_file")));
      return ptoComments1;
    });
  }

  @Override
  public int updateComment(PTOComments ptoComments) throws SQLException, DbHandleException {
    String updateQuery = "update PTO_Comments set comment_description= ?, attachment_file =? " +
        "where id = ?";
    PreparedStatement preparedStatement = this.dbHandle.createPreparedStatement(updateQuery);
    preparedStatement.setString(1, ptoComments.getCommentDescription());
    preparedStatement.setString(2, ptoComments.getCommentsFilePath());
    preparedStatement.setLong(3, ptoComments.getId());

    return DbHelper.executeUpdate(dbHandle, preparedStatement, false);
  }
}
