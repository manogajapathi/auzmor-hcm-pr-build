package com.auzmor.impl.service;

import com.auzmor.impl.util.db.DbHandleImpl;
import com.auzmor.service.DbHandleService;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.ServiceReference;
import io.vertx.servicediscovery.types.AbstractServiceReference;

public class DbHandleServiceImpl implements DbHandleService {

  @Override
  public String name() {
    return TYPE;
  }

  @Override
  public ServiceReference get(Vertx vertx, ServiceDiscovery serviceDiscovery, Record record, JsonObject jsonObject) {
    return new DbHandleServiceImpl.DbHandleServiceReference(vertx, serviceDiscovery, record);
  }

  private class DbHandleServiceReference extends AbstractServiceReference<DbHandle>
  {
    public DbHandleServiceReference(Vertx vertx, ServiceDiscovery serviceDiscovery, Record record) {
      super(vertx, serviceDiscovery, record);
    }

    @Override
    protected DbHandle retrieve() {
      return new DbHandleImpl();
    }


    @Override
    protected void onClose() {
      ((DbHandle)this.service).checkAndCloseConnection();
    }
  }
}