package com.auzmor.impl.constant.db;

public class HandleConstant {
  public static final String CONNECTION_EXISTS = "Connection already created for the handle";
  public static final String INVALID_CONNECTION = "Handle doesnot contain a valid connection";

  public static final String PREPARED_STATEMENT_NOT_FOUND = "Prepared Statement is not found in the handle";
  public static final String INVALID_PREPARED_STATEMENT = "PreparedStatement is null/already closed";
}
