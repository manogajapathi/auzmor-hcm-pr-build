package com.auzmor.impl.constant.response;

public class MessageConstant {
  public static final String UNALLOWED_FIELD_PRESENT = "Unauthorized params in the request";
  public static final String REQUIRED_FIELDS_MISSING = "Required field missing";
}
