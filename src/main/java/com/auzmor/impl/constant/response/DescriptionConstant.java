package com.auzmor.impl.constant.response;

public class DescriptionConstant {
  public static final String UNALLOWED_FIELD_PRESENT = "Additional fields are in the request";
  public static final String REQUIRED_FIELDS_MISSING = "Not all the required field are present";
}
