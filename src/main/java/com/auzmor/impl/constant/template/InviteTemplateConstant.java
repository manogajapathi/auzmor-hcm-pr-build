package com.auzmor.impl.constant.template;

public class InviteTemplateConstant {
  public static String INVITE_MAIL_TEMPLATE = "<html><body>Hi,<br><br>" +
      "Greeting from ${organization}! You have been invited to join ${organization} as an employee.<br><br>" +
      "Click <a href='https://${url}/auth/register?token=${token}' target='_blank'>here</a> to " +
      "signup.<br><br>Thank you,<br>${organization} Team</br></body></html>";
  public static String INVITE_SIGNUP_MAIL_TEMPLATE = "<html><body>Hi,<br><br>" +
      "Congratulation! Your registration with <a href='https://${url}'>${organization}</a> " +
      "as an employee was successful.<br><br>Thank you,<br>${organization} Team</br></body></html>";
}


