package com.auzmor.impl.constant.template;

public class NginxTemplateConstant {
  public static String ATS_SERVER_TEMPLATE = "server {\n" +
      "\tserver_name ${{domain}};\n" +
      "\t\tlocation / {\n" +
      "\t\t\treturn 301 https://$server_name$request_uri;\n" +
      "\t\t}\n" +
      "}\n" +
      "server {\n" +
      "\t\tserver_name ${{domain}};\n" +
      "\t\tlisten 443 ssl http2;\n" +
      "\t\tssl_certificate ${{sslCrt}};\n" +
      "\t\tssl_certificate_key ${{sslCrtKey}};\n" +
      "\t\tadd_header Strict-Transport-Security “max-age=31536000”;\n" +
      "\t\tlocation / {\n" +
      "\t\t\troot /usr/share/nginx/html/auzmor_frontend_v2/dist;\n" +
      "\t\t\tgzip on;\n" +
      "\t\t\tgzip_types text/plain application/javascript text/css application/octet-stream image/*;\n" +
      "\t\t\texpires 30d;\n" +
      "\n" +
      "\t\t\ttry_files $uri $uri/ /index.html;\n" +
      "\t\t\tindex index.html;\t\n" +
      "\t\t}\n" +
      "\t\tlocation ~* /backend/(.*) {\n" +
      "\t\t\t\tgzip on;\n" +
      "\t\t\t\tgzip_types text/plain application/octet-json application/octet-stream images/*;\n" +
      "\n" +
      "\t\t\t\tproxy_pass http://127.0.0.1:8000/$1$is_args$args;\n" +
      "\t\t\t\tproxy_set_header X-Real-Host $host;\n" +
      "\t\t\t\tproxy_set_header X-Real-IP $remote_addr;\n" +
      "\t\t\t\tproxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;\n" +
      "\t\t}\n" +
      "\t\tlocation ~* /backend/(employee/document/upload) {\n" +
      "\t\t\t\tgzip on;\n" +
      "\t\t\t\tgzip_types application/octet-json;\n" +
      "\t\t\t\tclient_max_body_size 10m;\n" +
      "\n" +
      "\t\t\t\tproxy_pass http://127.0.0.1:8000/$1;\n" +
      "\t\t\t\tproxy_set_header X-Real-Host $host;\n" +
      "\t\t\t\tproxy_set_header X-Real-IP $remote_addr;\n" +
      "\t\t\t\tproxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;\n" +
      "\t\t}\n" +
      "\t\tlocation ~* /backend/(onboardingPacket/welcome/video/\\d+) {\n" +
      "\t\t\t\tgzip on;\n" +
      "\t\t\t\tgzip_types application/octet-json;\n" +
      "\t\t\t\tclient_max_body_size 10m;\n" +
      "\n" +
      "\t\t\t\tproxy_pass http://127.0.0.1:8000/$1;\n" +
      "\t\t\t\tproxy_set_header X-Real-Host $host;\n" +
      "\t\t\t\tproxy_set_header X-Real-IP $remote_addr;\n" +
      "\t\t\t\tproxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;\n" +
      "\t\t}\n" +
      "\n" +
      "}\n" +
      "\n";
  public static String CARRERS_SERVER_TEMPLATE = "server {\n" +
      "\tserver_name ${{domain}};\n" +
      "\t\tlocation / {\n" +
      "\t\t\treturn 301 https://$server_name$request_uri;\n" +
      "\t\t}\n" +
      "}\n" +
      "server {\n" +
      "\t\tserver_name ${{domain}};\n" +
      "\t\tlisten 443 ssl http2;\n" +
      "\t\tssl_certificate ${{sslCrt}};\n" +
      "\t\tssl_certificate_key ${{sslCrtKey}};\n" +
      "\t\tadd_header Strict-Transport-Security “max-age=31536000”;\n" +
      "\t\tlocation / {\n" +
      "\t\t\tretrun 301 /careers;\n" +
      "\t\t}\n" +
      "\t\tlocation /careers {\n" +
      "\t\t\troot /usr/share/nginx/html/auzmor_frontend_v2/dist;\n" +
      "\t\t\tgzip on;\n" +
      "\t\t\tgzip_types text/plain application/javascript text/css application/octet-stream image/*;\n" +
      "\t\t\texpires 30d;\n" +
      "\n" +
      "\t\t\ttry_files $uri $uri/ /index.html;\n" +
      "\t\t\tindex index.html;\t\n" +
      "\t\t}\n" +
      "\t\tlocation ~* /backend/(.*) {\n" +
      "\t\t\t\tgzip on;\n" +
      "\t\t\t\tgzip_types text/plain application/octet-json application/octet-stream images/*;\n" +
      "\n" +
      "\t\t\t\tproxy_pass http://127.0.0.1:8000/$1$is_args$args;\n" +
      "\t\t\t\tproxy_set_header X-Real-Host $host;\n" +
      "\t\t\t\tproxy_set_header X-Real-IP $remote_addr;\n" +
      "\t\t\t\tproxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;\n" +
      "\t\t}\n" +
      "\t\tlocation ~* /backend/(employee/document/upload) {\n" +
      "\t\t\t\tgzip on;\n" +
      "\t\t\t\tgzip_types application/octet-json;\n" +
      "\t\t\t\tclient_max_body_size 10m;\n" +
      "\n" +
      "\t\t\t\tproxy_pass http://127.0.0.1:8000/$1;\n" +
      "\t\t\t\tproxy_set_header X-Real-Host $host;\n" +
      "\t\t\t\tproxy_set_header X-Real-IP $remote_addr;\n" +
      "\t\t\t\tproxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;\n" +
      "\t\t}\n" +
      "\t\tlocation ~* /backend/(onboardingPacket/welcome/video/\\d+) {\n" +
      "\t\t\t\tgzip on;\n" +
      "\t\t\t\tgzip_types application/octet-json;\n" +
      "\t\t\t\tclient_max_body_size 10m;\n" +
      "\n" +
      "\t\t\t\tproxy_pass http://127.0.0.1:8000/$1;\n" +
      "\t\t\t\tproxy_set_header X-Real-Host $host;\n" +
      "\t\t\t\tproxy_set_header X-Real-IP $remote_addr;\n" +
      "\t\t\t\tproxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;\n" +
      "\t\t}\n" +
      "\n" +
      "}\n" +
      "\n";
}
