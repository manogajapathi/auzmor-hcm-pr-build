package com.auzmor.impl.constant.template;

public class EmailTemplateConstant {
  public static final String BODY_HTML_TEMPLATE_PREFIX = "$${{";
  public static final String BODY_HTML_TEMPLATE_SUFFIX = "}}";
  public static final String BODY_HTML = "<!DOCTYPE html>\n" +
      "<html>\n" +
      "<head>\n" +
      "    <meta charset=\"UTF-8\">\n" +
      "    <meta name=\"viewport\" content=\"width=device-width\" initial-scale=\"1\">\n" +
      "    <!--[if !mso]>\n" +
      "        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
      "    <![endif]-->\n" +
      "    <meta name=\"x-apple-disable-message-reformatting\">\n" +
      "    <title></title>\n" +
      "    <!--[if mso]>\n" +
      "        <style>\n" +
      "            * { font-family: sans-serif !important; }\n" +
      "        </style>\n" +
      "    <![endif]-->\n" +
      "    <style>\n" +
      "        *,\n" +
      "        *:after,\n" +
      "        *:before {\n" +
      "            -webkit-box-sizing: border-box;\n" +
      "            -moz-box-sizing: border-box;\n" +
      "            box-sizing: border-box;\n" +
      "        }\n" +
      "        * {\n" +
      "            -ms-text-size-adjust: 100%;\n" +
      "            -webkit-text-size-adjust: 100%;\n" +
      "        }\n" +
      "        html,\n" +
      "        body,\n" +
      "        .document {\n" +
      "            width: 100% !important;\n" +
      "            height: 100% !important;\n" +
      "            margin: 0;\n" +
      "            padding: 0;\n" +
      "        }\n" +
      "        body {\n" +
      "            -webkit-font-smoothing: antialiased;\n" +
      "            -moz-osx-font-smoothing: grayscale;\n" +
      "            text-rendering: optimizeLegibility;\n" +
      "        }\n" +
      "        div[style*=\"margin: 16px 0\"] {\n" +
      "            margin: 0 !important;\n" +
      "        }\n" +
      "        table,\n" +
      "        td {\n" +
      "            mso-table-lspace: 0pt;\n" +
      "            mso-table-rspace: 0pt;\n" +
      "        }\n" +
      "        table {\n" +
      "            border-spacing: 0;\n" +
      "            border-collapse: collapse;\n" +
      "            table-layout: fixed;\n" +
      "            margin: 0 auto;\n" +
      "        }\n" +
      "        img {\n" +
      "            -ms-interpolation-mode: bicubic;\n" +
      "            max-width: 100%;\n" +
      "            border: 0;\n" +
      "        }\n" +
      "        *[x-apple-data-detectors] {\n" +
      "            color: inherit !important;\n" +
      "            text-decoration: none !important;\n" +
      "        }\n" +
      "        .x-gmail-data-detectors,\n" +
      "        .x-gmail-data-detectors *,\n" +
      "        .aBn {\n" +
      "            border-bottom: 0 !important;\n" +
      "            cursor: default !important;\n" +
      "        }\n" +
      "        .btn {\n" +
      "            -webkit-transition: all 200ms ease;\n" +
      "            transition: all 200ms ease;\n" +
      "        }\n" +
      "        .btn:hover {\n" +
      "            background-color: dodgerblue;\n" +
      "        }\n" +
      "        @media screen and (max-width: 750px) {\n" +
      "            .container {\n" +
      "                width: 100%;\n" +
      "                margin: auto;\n" +
      "            }\n" +
      "            .stack {\n" +
      "                display: block;\n" +
      "                width: 100%;\n" +
      "                max-width: 100%;\n" +
      "            }\n" +
      "        }\n" +
      "    </style>\n" +
      "</head>\n" +
      "<body>\n" +
      "    <div style=\"display: none; max-height: 0px; overflow: hidden;\">\n" +
      "        <!-- Preheader message here -->\n" +
      "    </div>\n" +
      "    <div style=\"display: none; max-height: 0px; overflow: hidden;\">&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;</div>\n" +
      "    <table role=\"presentation\" aria-hidden=\"true\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" class=\"document\">\n" +
      "        <tr>\n" +
      "            <td valign=\"top\">\n" +
      "                <table role=\"presentation\" aria-hidden=\"true\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" width=\"600\" class=\"container\">\n" +
      "                    <tr>\n" +
      "                        <td>\n" +
      "                            <table role=\"presentation\" aria-hidden=\"true\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" width=\"100%\" bgcolor=\"#fff\">\n" +
      "                                <tbody>\n" +
      "                                  <tr>\n" +
      "                                     <td align=\"left\" width=\"40%\">\n" +
      "                                        <p style=\"font-family: Helvetica, Arial, sans-serif; padding-left: 30px; padding-right: 30px; padding-top:0px; padding-bottom: 0px;\">\n" +
      "                                        \t<img src=\"$${{organization_logo}}\" alt=\"Logo\" style=\"max-height: 54px\"/>\n" +
      "                                        </p>\n" +
      "                                     </td>\n" +
      "                                     <td align=\"right\" width=\"60%\">\n" +
      "                                        <p style=\"font-family: Helvetica, Arial, sans-serif; color: #25a9df; font-size: 18px; padding-left: 30px; padding-right: 30px; padding-top:0px; padding-bottom: 0px;\">\n" +
      "                                        \tHR Information System\n" +
      "                                        </p>\n" +
      "                                     </td>\n" +
      "                                  </tr>\n" +
      "                               </tbody>\n" +
      "                            </table>\n" +
      "                        </td>\n" +
      "                    </tr>\n" +
      "                </table>\n" +
      "                <table role=\"presentation\" aria-hidden=\"true\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" width=\"600\" class=\"container\">\n" +
      "                    <tr>\n" +
      "                        <td>\n" +
      "                            <table role=\"presentation\" aria-hidden=\"true\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" width=\"100%\" bgcolor=\"#fff\">\n" +
      "                                <tbody>\n" +
      "                                  <tr>\n" +
      "                                     <td align=\"center\" height=\"1\" style=\"font-size:1px; line-height:1px; border-bottom: 1px solid #25a9df\">&nbsp;</td>\n" +
      "                                  </tr>\n" +
      "                               </tbody>\n" +
      "                            </table>\n" +
      "                        </td>\n" +
      "                    </tr>\n" +
      "                </table>\n" +
      "                <table role=\"presentation\" aria-hidden=\"true\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" width=\"600\" class=\"container\">\n" +
      "                    <tr>\n" +
      "                        <td>\n" +
      "                            <table role=\"presentation\" aria-hidden=\"true\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" width=\"100%\" bgcolor=\"#fff\">\n" +
      "                                <tbody>\n" +
      "                                  <tr>\n" +
      "                                     <td align=\"center\" height=\"30\">&nbsp;</td>\n" +
      "                                  </tr>\n" +
      "                               </tbody>\n" +
      "                            </table>\n" +
      "                        </td>\n" +
      "                    </tr>\n" +
      "                </table>\n" +
      "                <table role=\"presentation\" aria-hidden=\"true\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" width=\"600\" class=\"container\">\n" +
      "                    <tr>\n" +
      "                        <td>\n" +
      "                            <table role=\"presentation\" aria-hidden=\"true\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" width=\"100%\" bgcolor=\"#ffffff\">\n" +
      "                                <tbody>\n" +
      "                                  <tr>\n" +
      "                                     <td align=\"left\" style=\"font-family: Helvetica, Arial, sans-serif;font-size: 16px; padding-left: 30px; padding-right: 30px; padding-top:0px; padding-bottom: 0px; color: #666;  line-height: 24px;\">\n" +
      "                                        $${{template}}\n" +
      "                                     </td>\n" +
      "                                  </tr>\n" +
      "                               </tbody>\n" +
      "                            </table>\n" +
      "                        </td>\n" +
      "                    </tr>\n" +
      "                </table>\n" +
      "                <table role=\"presentation\" aria-hidden=\"true\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" width=\"600\" class=\"container\">\n" +
      "                    <tr>\n" +
      "                        <td>\n" +
      "                            <table role=\"presentation\" aria-hidden=\"true\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" width=\"100%\" bgcolor=\"#fff\">\n" +
      "                                <tbody>\n" +
      "                                  <tr>\n" +
      "                                     <td align=\"center\" height=\"30\">&nbsp;</td>\n" +
      "                                  </tr>\n" +
      "                               </tbody>\n" +
      "                            </table>\n" +
      "                        </td>\n" +
      "                    </tr>\n" +
      "                </table>\n" +
      "                <table role=\"presentation\" aria-hidden=\"true\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" width=\"600\" class=\"container\">\n" +
      "                    <tr>\n" +
      "                        <td>\n" +
      "                            <table role=\"presentation\" aria-hidden=\"true\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\"center\" width=\"100%\" bgcolor=\"#ebeff4\">\n" +
      "                                <tbody>\n" +
      "                                  <tr>\n" +
      "                                     <td align=\"left\" width=\"40%\">\n" +
      "                                        <p style=\"font-family: Helvetica, Arial, sans-serif; padding-left: 30px; padding-right: 30px; padding-top:0px; padding-bottom: 0px; color: #adb9c9; font-size: 16px;\">\n" +
      "                                        \tPowered by Auzmor\n" +
      "                                        </p>\n" +
      "                                     </td>\n" +
      "                                     <td align=\"right\" width=\"60%\">\n" +
      "                                        <p style=\"font-family: Helvetica, Arial, sans-serif; padding-left: 30px; padding-right: 30px; padding-top:0px; padding-bottom: 0px; color: #adb9c9\">\n" +
      "                                        \t<a style=\"font-family: Helvetica, Arial, sans-serif; color: #adb9c9; text-decoration: none; font-size: 16px;\" href=\"#\">$${{organization_url}}</a>\n" +
      "                                        </p>\n" +
      "                                     </td>\n" +
      "                                  </tr>\n" +
      "                               </tbody>\n" +
      "                            </table>\n" +
      "                        </td>\n" +
      "                    </tr>\n" +
      "                </table>\n" +
      "                \n" +
      "            </td>\n" +
      "        </tr>\n" +
      "    </table>\n" +
      "</body>\n" +
      "</html>";
}
