package com.auzmor.impl.constant.http;

public class HttpContentTypeConstat {

  public static final String APPLICATION_X_WWW_FOMR_URL_ENCODED = "application/x-www-form-urlencoded";
  public static final String APPLICATION_JSON = "application/json";
}
