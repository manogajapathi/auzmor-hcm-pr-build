package com.auzmor.impl.constant.route;

import io.vertx.core.json.JsonObject;

public class PathConstants {
  public static final JsonObject APIS = new JsonObject("{\"Root\": { \"methods\": [\"GET\"], \"route\": \"/\", \"pattern\": \"/\", \"entity\": \"None\", \"roles\": [\"Anonymous\"]}," +
      "\"Images\": { \"methods\": [\"GET\"], \"route\": \"/images/\", \"pattern\": \"/images/\", \"entity\": \"None\", \"roles\": [\"Anonymous\"]}," +
      "\"FileUploads\": { \"methods\": [\"GET\"], \"route\": \"/file-uploads/\", \"pattern\": \"/file-uploads/\", \"entity\": \"None\", \"roles\": [\"Anonymous\"]}," +
      "\"Public_Static_Contents\": { \"methods\": [\"GET\"], \"route\": \"/static/\", \"pattern\": \"/static/\", \"entity\": \"None\", \"roles\": [\"Anonymous\"]}," +
      "\"Private_Static_Contents\": { \"methods\": [\"GET\"], \"route\": \"/private_static/\", \"pattern\": \"/private_static\", \"entity\": \"None\", \"roles\": [\"Anonymous\"]}," +
      "\"Create_Announcement\": { \"methods\": [\"POST\"], \"route\": \"/announcement/create\", \"pattern\": \"/announcement/create\", \"entity\": \"Announcement\", \"roles\": [\"Admin\"]}," +
      "\"Get_All_Announcements\": { \"methods\": [\"GET\"], \"route\": \"/announcement\", \"pattern\": \"/announcement\", \"entity\": \"Announcement\", \"roles\": [\"Admin\", \"Employee\"]}," +
      "\"Delete_Announcement\": { \"methods\": [\"DELETE\"], \"route\": \"/announcement/:id\", \"pattern\": \"/announcement/\\\\d+\", \"entity\": \"Announcement\", \"roles\": [\"Admin\"]}," +
      "\"Get_Announcement\": { \"methods\": [\"GET\"], \"route\": \"/announcement/:id\", \"pattern\": \"/announcement/\\\\d+\", \"entity\": \"Announcement\", \"roles\": [\"Admin\"]}," +
      "\"Update_Announcement\": { \"methods\": [\"PUT\"], \"route\": \"/announcement/:announcementId\", \"pattern\": \"/announcement/\\\\d+\", \"entity\": \"Announcement\", \"roles\": [\"Admin\"]}," +
      "\"Employee_Details\": { \"methods\": [\"GET\"], \"route\": \"/employee/:id\", \"pattern\": \"/employee/\\\\d+\", \"entity\": \"Employee\", \"roles\": [\"Admin\", \"Employee\"]}," +
      "\"Employee_Detail_Count_Based_on_First_name\": { \"methods\": [\"GET\"], \"route\": \"/employee/listBasedOnIndex/:id\", \"pattern\": \"/employee/listBasedOnIndex/\\\\d+\", \"entity\": \"Employee\", \"roles\": [\"Admin\"]}," +
      "\"Add_Employee\": { \"methods\": [\"POST\"], \"route\": \"/employee/create\", \"pattern\": \"/employee/create\", \"entity\": \"Employee\", \"roles\": [\"Admin\"]}," +
      "\"Display_Employees_based_on_Onboarding_Status\": { \"methods\": [\"GET\"], \"route\": \"/employee/list\", \"pattern\": \"/employee/list\", \"entity\": \"Employee\", \"roles\": [\"Admin\"]}," +
      "\"Get_New_Employee_Details_Based_on_Organization\": { \"methods\": [\"GET\"], \"route\": \"/employee/lastemployeeno\", \"pattern\": \"/employee/lastemployeeno\", \"entity\": \"Employee\", \"roles\": [\"Admin\"]}," +
      "\"Get_Employee_Details_based_on_ID\": { \"methods\": [\"GET\"], \"route\": \"/employee/:id\", \"pattern\": \"/employee/\\\\d+\", \"entity\": \"Employee\", \"roles\": [\"Admin\"]}," +
      "\"Update_Employee\": { \"methods\": [\"PUT\"], \"route\": \"/employee/:employeeId\", \"pattern\": \"/employee/\\\\d+\", \"entity\": \"Employee\", \"roles\": [\"Admin\"]}," +
      "\"Delete_Employee\": { \"methods\": [\"DELETE\"], \"route\": \"/employee/:id\", \"pattern\": \"/employee/\\\\d+\", \"entity\": \"Employee\", \"roles\": [\"Admin\"]}," +
      "\"Get_Employee_Team\": { \"methods\": [\"GET\"], \"route\": \"/TeamEmployee/:employeeId\", \"pattern\": \"/TeamEmployee/\\\\d+\", \"entity\": \"Employee\", \"roles\": [\"Admin\"]}," +
      "\"Terminate_Employee\": { \"methods\": [\"PUT\"], \"route\": \"/employee/terminate/:employeeId\", \"pattern\": \"/employee/terminate/\\\\d+\", \"entity\": \"Employee\", \"roles\": [\"Admin\"]}," +
      "\"Send_Packet_For_Employee\": { \"methods\": [\"PUT\"], \"route\": \"/employee/sendonboardingpacketemployee/\", \"pattern\": \"/employee/sendonboardingpacketemployee/\", \"entity\": \"Employee\", \"roles\": [\"Admin\"]}," +
      "\"Get_Field_Options_For_Employee\": { \"methods\": [\"GET\"], \"route\": \"/admin/employee/fieldoption\", \"pattern\": \"/admin/employee/fieldoption\", \"entity\": \"Employee\", \"roles\": [\"Admin\"]}," +
      "\"Get_Onboarding_Status\": { \"methods\": [\"GET\"], \"route\": \"/employee/getonboardingstatusforemployee/:employeeId\", \"pattern\": \"/employee/getonboardingstatusforemployee/\\\\d+\", \"entity\": \"Employee\", \"roles\": [\"Admin\"]}," +
      "\"Upload_Employee_Profile_Image\": { \"methods\": [\"POST\"], \"route\": \"/employee/profile/image\", \"pattern\": \"/employee/profile/image\", \"entity\": \"Employee\", \"roles\": [\"Admin\"]}," +
      "\"View_All_Employees\": { \"methods\": [\"GET\"], \"route\": \"/employee/ats/viewuser\", \"pattern\": \"/employee/ats/viewuser\", \"entity\": \"Employee\", \"roles\": [\"Admin\"]}," +
      "\"Add_User\": { \"methods\": [\"POST\"], \"route\": \"/employee/ats/adduser\", \"pattern\": \"/employee/ats/adduser\", \"entity\": \"Employee\", \"roles\": [\"Admin\"]}," +
      "\"Delete_User\": { \"methods\": [\"DELETE\"], \"route\": \"/employee/ats/deleteuser\", \"pattern\": \"/employee/ats/deleteuser\", \"entity\": \"Employee\", \"roles\": [\"Admin\"]}," +
      "\"Edit_Employee_\": { \"methods\": [\"PUT\"], \"route\": \"/employee/ats/edituser\", \"pattern\": \"/employee/ats/edituser\", \"entity\": \"Employee\", \"roles\": [\"Admin\", \"Employee\"]}," +
      "\"Upload_Profile_Image\": { \"methods\": [\"POST\"], \"route\": \"/employee/ats/uploadprofileimage\", \"pattern\": \"/employee/ats/uploadprofileimage\", \"entity\": \"Employee\", \"roles\": [\"Admin\", \"Employee\"]}," +
      "\"Upload_Document_For_Tasks_Or_Documents\": { \"methods\": [\"POST\"], \"route\": \"/employee/uploadfile\", \"pattern\": \"/employee/uploadfile\", \"entity\": \"Employee\", \"roles\": [\"Admin\", \"Employee\"]}," +
      "\"Upload_Document\": { \"methods\": [\"POST\"], \"route\": \"/employee/document/upload\", \"pattern\": \"/employee/document/upload\", \"entity\": \"Employee\", \"roles\": [\"Admin\", \"Employee\"]}," +
      "\"Upload_Document_for_tasks\": { \"methods\": [\"POST\"], \"route\": \"/employee/task/upload\", \"pattern\": \"/employee/task/upload\", \"entity\": \"Employee\", \"roles\": [\"Admin\", \"Employee\"]}," +
      "\"For_Sorting_Job\": { \"methods\": [\"GET\"], \"route\": \"/search/filter\", \"pattern\": \"/search/filter\", \"entity\": \"Job\", \"roles\": [\"Admin\"]}," +
      "\"Add_Job\": { \"methods\": [\"POST\"], \"route\": \"/job/create\", \"pattern\": \"/job/create\", \"entity\": \"Job\", \"roles\": [\"Admin\"]}," +
      "\"Job_Deisplay\": { \"methods\": [\"GET\"], \"route\": \"/job\", \"pattern\": \"/job\", \"entity\": \"Job\", \"roles\": [\"Anonymous\"]}," +
      "\"Job_Filter\": { \"methods\": [\"GET\"], \"route\": \"/job/filter\", \"pattern\": \"/job/filter\", \"entity\": \"Job\", \"roles\": [\"Anonymous\"]}," +
      "\"Job_Update\": { \"methods\": [\"PUT\"], \"route\": \"/job/:id\", \"pattern\": \"/job/\\\\d+\", \"entity\": \"Job\", \"roles\": [\"Admin\",\"Hiring Lead\",\"Collaborator\"]}," +
      "\"Job_Delete\": { \"methods\": [\"DELETE\"], \"route\": \"/job/:id\", \"pattern\": \"/job/\\\\d+\", \"entity\": \"Job\", \"roles\": [\"Admin\"]}," +
      "\"Archive_Job\": { \"methods\": [\"PUT\"], \"route\": \"/job/archive/:id\", \"pattern\": \"/job/archive/\\\\d+\", \"entity\": \"Job\", \"roles\": [\"Admin\"]}," +
      "\"Get_All_Job_Description\": { \"methods\": [\"GET\"], \"route\": \"/jobdesc\", \"pattern\": \"/jobdesc\", \"entity\": \"Job\", \"roles\": [\"Admin\"]}," +
      "\"Get_Job_Description\": { \"methods\": [\"GET\"], \"route\": \"/jobdesc/:id\", \"pattern\": \"/jobdesc/\\\\d+\", \"entity\": \"Job\", \"roles\": [\"Admin\"]}," +
      "\"Create_Job_Description\": { \"methods\": [\"POST\"], \"route\": \"/jobdesc/create\", \"pattern\": \"/jobdesc/create\", \"entity\": \"Job\", \"roles\": [\"Admin\"]}," +
      "\"Login_Check\": { \"methods\": [\"POST\"], \"route\": \"/login/check\", \"pattern\": \"/login/check\", \"entity\": \"Login\", \"roles\": [\"Anonymous\"]}," +
      "\"User_Signup\": { \"methods\": [\"POST\"], \"route\": \"/signup/create\", \"pattern\": \"/signup/create\", \"entity\": \"Login\", \"roles\": [\"Anonymous\"]}," +
      "\"Reset_Password\": { \"methods\": [\"POST\"], \"route\": \"/login/resetpwd\", \"pattern\": \"/login/resetpwd\", \"entity\": \"Login\", \"roles\": [\"Anonymous\"]}," +
      "\"Reset_Password_Check\": { \"methods\": [\"GET\"], \"route\": \"/login/checkReset/:token\", \"pattern\": \"/login/checkReset/[^/]+\", \"entity\": \"Login\", \"roles\": [\"Anonymous\"]}," +
      "\"Update_Password\": { \"methods\": [\"POST\"], \"route\": \"/login/updatepwd\", \"pattern\": \"/login/updatepwd\", \"entity\": \"Login\", \"roles\": [\"Anonymous\"]}," +
      "\"Change_Password\": { \"methods\": [\"PUT\"], \"route\": \"/login/changePassword\", \"pattern\": \"/login/changePassword\", \"entity\": \"Login\", \"roles\": [\"Anonymous\"]}," +
      "\"Approve_Employee\": { \"methods\": [\"PUT\"], \"route\": \"/signup/approve\", \"pattern\": \"/signup/approve\", \"entity\": \"Employee\", \"roles\": [\"Admin\", \"Super Owner\"]}," +
      "\"Organization_Edit\": { \"methods\": [\"GET\"], \"route\": \"/organization/setting\", \"pattern\": \"/organization/setting\", \"entity\": \"Organization\", \"roles\": [\"Admin\", \"Super Owner\"]}," +
      "\"Preview_Organization\": { \"methods\": [\"GET\"], \"route\": \"/organization/list\", \"pattern\": \"/organization/list\", \"entity\": \"Organization\", \"roles\": [\"Admin\", \"Super Owner\"]}," +
      "\"Organization_Exist\": { \"methods\": [\"GET\"], \"route\": \"/organization/check\", \"pattern\": \"/organization/check\", \"entity\": \"Organization\", \"roles\": [\"Anonymous\"]}," +
      "\"Get_Organization_Details\": { \"methods\": [\"GET\"], \"route\": \"/organization\", \"pattern\": \"/organization\", \"entity\": \"Organization\", \"roles\": [\"Admin\", \"Anonymous\"]}," +
      "\"Get_Organization_Count\": { \"methods\": [\"GET\"], \"route\": \"/organization/count\", \"pattern\": \"/organization/count\", \"entity\": \"Organization\", \"roles\": [\"Admin\", \"Employee\"]}," +
      "\"Get_Organization_Url_And_Logo\": { \"methods\": [\"GET\"], \"route\": \"/organization/urlandlogo/:id\", \"pattern\": \"/organization/urlandlogo/\\\\d+\", \"entity\": \"Organization\", \"roles\": [\"Admin\"]}," +
      "\"Delete_Organization\": { \"methods\": [\"DELETE\"], \"route\": \"/organization/:id\", \"pattern\": \"/organization/\\\\d+\", \"entity\": \"Organization\", \"roles\": [\"Admin\"]}," +
      "\"Upload_Favicon_Image\": { \"methods\": [\"POST\"], \"route\": \"/organization/images/favicon\", \"pattern\": \"/organization/images/favicon\", \"entity\": \"Organization\", \"roles\": [\"Admin\"]}," +
      "\"Upload_Logo_Image\": { \"methods\": [\"POST\"], \"route\": \"/organization/images/logo\", \"pattern\": \"/organization/images/logo\", \"entity\": \"Organization\", \"roles\": [\"Admin\"]}," +
      "\"Add_Organization\": { \"methods\": [\"POST\"], \"route\": \"/organization/create\", \"pattern\": \"/organization/create\", \"entity\": \"Organization\", \"roles\": [\"Anonymous\"]}," +
      "\"Update_Organization\": { \"methods\": [\"PUT\"], \"route\": \"/organization/:organizationId\", \"pattern\": \"/organization/\\\\d+\", \"entity\": \"Organization\", \"roles\": [\"Admin\"]}," +
      "\"Update_Welcome_Form\": { \"methods\": [\"PUT\"], \"route\": \"/onboardingPacket/welcome/:organizationId\", \"pattern\": \"/onboardingPacket/welcome/\\\\d+\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Upload_Welcome_Video\": { \"methods\": [\"POST\"], \"route\": \"/onboardingPacket/welcome/video/:organizationId\", \"pattern\": \"/onboardingPacket/welcome/video/\\\\d+\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Get_Coordinates\": { \"methods\": [\"GET\"], \"route\": \"/onboardingPacket/welcome/map\", \"pattern\": \"/onboardingPacket/welcome/map\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Update_Welcome_Address\": { \"methods\": [\"PUT\"], \"route\": \"/onboardingPacket/mapAddress/\", \"pattern\": \"/onboardingPacket/mapAddress/\", \"entity\": \"Onboarding\", \"roles\": [\"Anonymous\"]}," +
      "\"Add_New_Packet\": { \"methods\": [\"POST\"], \"route\": \"/onboardingPacket/add\", \"pattern\": \"/onboardingPacket/add\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Add_New_Form\": { \"methods\": [\"POST\"], \"route\": \"/onboardingPacket/form\", \"pattern\": \"/onboardingPacket/form\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Upload_Referral_Doc\": { \"methods\": [\"POST\"], \"route\": \"/onboardingPacket/task/uploadReferralDoc\", \"pattern\": \"/onboardingPacket/task/uploadReferralDoc\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Add_New_Task\": { \"methods\": [\"POST\"], \"route\": \"/onboardingPacket/task\", \"pattern\": \"/onboardingPacket/task\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Add_Document\": { \"methods\": [\"POST\"], \"route\": \"/onboardingPacket/document\", \"pattern\": \"/onboardingPacket/document\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Get_Packet_Details\": { \"methods\": [\"GET\"], \"route\": \"/onboardingPacket\", \"pattern\": \"/onboardingPacket\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Send_Remainder_Mail\": { \"methods\": [\"POST\"], \"route\": \"/onboardingPacket/sendremainder\", \"pattern\": \"/onboardingPacket/sendremainder\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Upload_Document_Packet\": { \"methods\": [\"POST\"], \"route\": \"/onboardingPacket/document/upload\", \"pattern\": \"/onboardingPacket/document/upload\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Delete_Packet\": { \"methods\": [\"DELETE\"], \"route\": \"/onboardingPacket/:packetId\", \"pattern\": \"/onboardingPacket/\\\\d+\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Delete_Form\": { \"methods\": [\"DELETE\"], \"route\": \"/onboardingPacket/form/:formId\", \"pattern\": \"/onboardingPacket/form/\\\\d+\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Delete_Task\": { \"methods\": [\"DELETE\"], \"route\": \"/onboardingPacket/task/:taskId\", \"pattern\": \"/onboardingPacket/task/\\\\d+\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Delete_Document\": { \"methods\": [\"DELETE\"], \"route\": \"/onboardingPacket/document/:documentId\", \"pattern\": \"/onboardingPacket/document/\\\\d+\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Send_Packet_To_Employee\": { \"methods\": [\"PUT\"], \"route\": \"/onboardingPacket/send/:employeeId\", \"pattern\": \"/onboardingPacket/send/\\\\d+\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Update_Packet\": { \"methods\": [\"PUT\"], \"route\": \"/onboardingPacket/:packetId\", \"pattern\": \"/onboardingPacket/\\\\d+\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Update_Form\": { \"methods\": [\"PUT\"], \"route\": \"/onboardingPacket/form/:formId\", \"pattern\": \"/onboardingPacket/form/\\\\d+\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Update_Task\": { \"methods\": [\"PUT\"], \"route\": \"/onboardingPacket/task/:taskId\", \"pattern\": \"/onboardingPacket/task/\\\\d+\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Update_Document\": { \"methods\": [\"PUT\"], \"route\": \"/onboardingPacket/document/:documentId\", \"pattern\": \"/onboardingPacket/document/\\\\d+\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Get_Packet_Details_For_Employee\": { \"methods\": [\"GET\"], \"route\": \"/onboardingPacket/getPacketForEmployee\", \"pattern\": \"/onboardingPacket/getPacketForEmployee\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\", \"Employee\"]}," +
      "\"Get_Auzmor_Forms\": { \"methods\": [\"GET\"], \"route\": \"/onboardingPacket/forms/auzmor\", \"pattern\": \"/onboardingPacket/forms/auzmor\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Get_Auzmor_Documents\": { \"methods\": [\"GET\"], \"route\": \"/onboardingPacket/documents/auzmor\", \"pattern\": \"/onboardingPacket/documents/auzmor\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Get_In_House_Documents\": { \"methods\": [\"GET\"], \"route\": \"/onboardingPacket/documents/in-house\", \"pattern\": \"/onboardingPacket/documents/in-house\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Newsfeed_Admin_Reports\": { \"methods\": [\"GET\"], \"route\": \"/newsfeed/admin\", \"pattern\": \"/newsfeed/admin\", \"entity\": \"Newsfeed\", \"roles\": [\"Admin\"]}," +
      "\"Add_Holiday_List\": { \"methods\": [\"POST\"], \"route\": \"/holidayList/create\", \"pattern\": \"/holidayList/create\", \"entity\": \"Holiday\", \"roles\": [\"Admin\"]}," +
      "\"Get_All_Holiday_Lists\": { \"methods\": [\"GET\"], \"route\": \"/holidayList/list\", \"pattern\": \"/holidayList/list\", \"entity\": \"Holiday\", \"roles\": [\"Admin\"]}," +
      "\"Get_holiday_List\": { \"methods\": [\"GET\"], \"route\": \"/holidayList\", \"pattern\": \"/holidayList\", \"entity\": \"Holiday\", \"roles\": [\"Admin\", \"Employee\"]}," +
      "\"Update_Holiday_list\": { \"methods\": [\"PUT\"], \"route\": \"/holidayList/:listId\", \"pattern\": \"/holidayList/\\\\d+\", \"entity\": \"Holiday\", \"roles\": [\"Admin\"]}," +
      "\"Delete_Holiday_list\": { \"methods\": [\"DELETE\"], \"route\": \"/holidayList/:list_id\", \"pattern\": \"/holidayList/\\\\d+\", \"entity\": \"Holiday\", \"roles\": [\"Admin\"]}," +
      "\"Copy_Holiday_list\": { \"methods\": [\"POST\"], \"route\": \"/holidayList/copy\", \"pattern\": \"/holidayList/copy\", \"entity\": \"Holiday\", \"roles\": [\"Admin\"]}," +
      "\"Add_Holiday\": { \"methods\": [\"POST\"], \"route\": \"/holiday/create\", \"pattern\": \"/holiday/create\", \"entity\": \"Holiday\", \"roles\": [\"Admin\"]}," +
      "\"Get_all_Holidays\": { \"methods\": [\"GET\"], \"route\": \"/holiday/list\", \"pattern\": \"/holiday/list\", \"entity\": \"Holiday\", \"roles\": [\"Admin\"]}," +
      "\"Get_holiday_details\": { \"methods\": [\"GET\"], \"route\": \"/holiday\", \"pattern\": \"/holiday\", \"entity\": \"Holiday\", \"roles\": [\"Admin\"]}," +
      "\"Update_Holiday\": { \"methods\": [\"PUT\"], \"route\": \"/holiday/:holiday_id\", \"pattern\": \"/holiday/\\\\d+\", \"entity\": \"Holiday\", \"roles\": [\"Admin\"]}," +
      "\"Delete_Holiday\": { \"methods\": [\"DELETE\"], \"route\": \"/holiday/:holiday_id\", \"pattern\": \"/holiday/\\\\d+\", \"entity\": \"Reports\", \"roles\": [\"Admin\"]}," +
      "\"Job_Post_Report\": { \"methods\": [\"GET\"], \"route\": \"/report/jobposting\", \"pattern\": \"/report/jobposting\", \"entity\": \"Reports\", \"roles\": [\"Admin\"]}," +
      "\"Get_Job_Report_Details\": { \"methods\": [\"GET\"], \"route\": \"/report/jobposting/details\", \"pattern\": \"/report/jobposting/details\", \"entity\": \"Reports\", \"roles\": [\"Admin\"]}," +
      "\"Current_Status_Report\": { \"methods\": [\"GET\"], \"route\": \"/report/currentStatus\", \"pattern\": \"/report/currentStatus\", \"entity\": \"Reports\", \"roles\": [\"Admin\"]}," +
      "\"Average_Status_Report\": { \"methods\": [\"GET\"], \"route\": \"/report/averageStatus\", \"pattern\": \"/report/averageStatus\", \"entity\": \"Reports\", \"roles\": [\"Admin\"]}," +
      "\"Applicant_Status_Report\": { \"methods\": [\"GET\"], \"route\": \"/report/applicantstatus\", \"pattern\": \"/report/applicantstatus\", \"entity\": \"Reports\", \"roles\": [\"Admin\", \"Collaborator\", \"Hiring Lead\"]}," +
      "\"Source_Efficiency_Report\": { \"methods\": [\"PUT\"], \"route\": \"/report/source/:jobId\", \"pattern\": \"/report/source/\\\\d+\", \"entity\": \"Reports\", \"roles\": [\"Admin\"]}," +
      "\"Add_Source_Report_Count\": { \"methods\": [\"GET\"], \"route\": \"/report/source\", \"pattern\": \"/report/source\", \"entity\": \"Reports\", \"roles\": [\"Admin\"]}," +
      "\"Invite__New_Employee\": { \"methods\": [\"POST\"], \"route\": \"/invite/employee\", \"pattern\": \"/invite/employee\", \"entity\": \"Invite\", \"roles\": [\"Admin\"]}," +
      "\"Invite_Token_Status\": { \"methods\": [\"POST\"], \"route\": \"/invite/token/status\", \"pattern\": \"/invite/token/status\", \"entity\": \"Invite\", \"roles\": [\"Only Anonymous\"]}," +
      "\"Add_Invited_Employee\": { \"methods\": [\"POST\"], \"route\": \"/invite/employee/add\", \"pattern\": \"/invite/employee/add\", \"entity\": \"Invite\", \"roles\": [\"Only Anonymous\"]}," +
      "\"Get_all_Titles\": { \"methods\": [\"GET\"], \"route\": \"/title/list\", \"pattern\": \"/title/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Get_title\": { \"methods\": [\"GET\"], \"route\": \"/title/:id\", \"pattern\": \"/title/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\", \" Employee\"]}," +
      "\"Delete_Title\": { \"methods\": [\"DELETE\"], \"route\": \"/title/:id\", \"pattern\": \"/title/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Add_Title\": { \"methods\": [\"POST\"], \"route\": \"/title/create\", \"pattern\": \"/title/create\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Update_Title\": { \"methods\": [\"PUT\"], \"route\": \"/title/:id\", \"pattern\": \"/title/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Get_all_Field_options\": { \"methods\": [\"GET\"], \"route\": \"/fieldOptions/list\", \"pattern\": \"/fieldOptions/list\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Get_all_divisions\": { \"methods\": [\"GET\"], \"route\": \"/division/list/:organizationId\", \"pattern\": \"/division/list/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Get_division\": { \"methods\": [\"GET\"], \"route\": \"/division/:divisionId\", \"pattern\": \"/division/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\", \" Employee\"]}," +
      "\"Delete_Division\": { \"methods\": [\"DELETE\"], \"route\": \"/division/:divisionId\", \"pattern\": \"/division/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Add_Division\": { \"methods\": [\"POST\"], \"route\": \"/division/create\", \"pattern\": \"/division/create\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Update_Division\": { \"methods\": [\"PUT\"], \"route\": \"/division/:divisionId\", \"pattern\": \"/division/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Get_all_Paygroups\": { \"methods\": [\"GET\"], \"route\": \"/paygroup/list/:organizationId\", \"pattern\": \"/paygroup/list/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Get_paygroup\": { \"methods\": [\"GET\"], \"route\": \"/paygroup/:paygroupId\", \"pattern\": \"/paygroup/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\", \" Employee\"]}," +
      "\"Delete_paygroup\": { \"methods\": [\"DELETE\"], \"route\": \"/paygroup/:paygroupId\", \"pattern\": \"/paygroup/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Add_Paygroup\": { \"methods\": [\"POST\"], \"route\": \"/paygroup/create\", \"pattern\": \"/paygroup/create\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Update_Paygroup\": { \"methods\": [\"PUT\"], \"route\": \"/paygroup/:paygroupId\", \"pattern\": \"/paygroup/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Get_All_Employment\": { \"methods\": [\"GET\"], \"route\": \"/employmentType/list\", \"pattern\": \"/employmentType/list\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Get_Employment_Type\": { \"methods\": [\"GET\"], \"route\": \"/employmentType/:id\", \"pattern\": \"/employmentType/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\", \" Employee\"]}," +
      "\"Delete_Employment_Type\": { \"methods\": [\"DELETE\"], \"route\": \"/employmentType/:id\", \"pattern\": \"/employmentType/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Add_Employment_Type\": { \"methods\": [\"POST\"], \"route\": \"/employmentType/create\", \"pattern\": \"/employmentType/create\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Update_Employment\": { \"methods\": [\"PUT\"], \"route\": \"/employmentType/:id\", \"pattern\": \"/employmentType/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Get_All_Department\": { \"methods\": [\"GET\"], \"route\": \"/department/list\", \"pattern\": \"/department/list\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Get_Department\": { \"methods\": [\"GET\"], \"route\": \"/department/:id\", \"pattern\": \"/department/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\", \" Employee\"]}," +
      "\"Delete_Department\": { \"methods\": [\"DELETE\"], \"route\": \"/department/:id\", \"pattern\": \"/department/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Add_Department\": { \"methods\": [\"POST\"], \"route\": \"/department/create\", \"pattern\": \"/department/create\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Update_Department\": { \"methods\": [\"PUT\"], \"route\": \"/department/:id\", \"pattern\": \"/department/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Get_Location\": { \"methods\": [\"GET\"], \"route\": \"/location/:id\", \"pattern\": \"/location/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Add_Location\": { \"methods\": [\"POST\"], \"route\": \"/location/create\", \"pattern\": \"/location/create\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Update_Location\": { \"methods\": [\"PUT\"], \"route\": \"/location/:id\", \"pattern\": \"/location/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Delete_location\": { \"methods\": [\"DELETE\"], \"route\": \"/location/:id\", \"pattern\": \"/location/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Preview_Candidate\": { \"methods\": [\"GET\"], \"route\": \"/candidate/list\", \"pattern\": \"/candidate/list\", \"entity\": \"Candidate\", \"roles\": [\"Admin\", \"Collaborator\", \"Hiring Lead\"]}," +
      "\"Filter_Candidate\": { \"methods\": [\"GET\"], \"route\": \"/candidate/filter\", \"pattern\": \"/candidate/filter\", \"entity\": \"Candidate\", \"roles\": [\"Anonymous\"]}," +
      "\"Update_Candidate_Details\": { \"methods\": [\"PUT\"], \"route\": \"/candidate/:candidateId\", \"pattern\": \"/candidate/\\\\d+\", \"entity\": \"Candidate\", \"roles\": [\"Admin\",\"Candidate\"]}," +
      "\"Upload_Resume\": { \"methods\": [\"POST\"], \"route\": \"/candidate/resume\", \"pattern\": \"/candidate/resume\", \"entity\": \"Candidate\", \"roles\": [\"Anonymous\"]}," +
      "\"Upload_CoverLetter\": { \"methods\": [\"POST\"], \"route\": \"/candidate/coverletter\", \"pattern\": \"/candidate/coverletter\", \"entity\": \"Candidate\", \"roles\": [\"Anonymous\"]}," +
      "\"Upload_ProfileImage\": { \"methods\": [\"POST\"], \"route\": \"/candidate/profile/image\", \"pattern\": \"/candidate/profile/image\", \"entity\": \"Candidate\", \"roles\": [\"Anonymous\"]}," +
      "\"Create_Candidate\": { \"methods\": [\"POST\"], \"route\": \"/candidate/create\", \"pattern\": \"/candidate/create\", \"entity\": \"Candidate\", \"roles\": [\"Anonymous\"]}," +
      "\"Delete_Candidate\": { \"methods\": [\"DELETE\"], \"route\": \"/candidate/:id\", \"pattern\": \"/candidate/\\\\d+\", \"entity\": \"Candidate\", \"roles\": [\"Admin\",\"Candidate\",\"Employee\"]}," +
      "\"Create_InterviewProcess\": { \"methods\": [\"POST\"], \"route\": \"/candidate/interviewprocess/create\", \"pattern\": \"/candidate/interviewprocess/create\", \"entity\": \"Candidate InterviewProcess\", \"roles\": [\"Admin\",\"Hiring Lead\"]}," +
      "\"Update_InterviewProcess\": { \"methods\": [\"PUT\"], \"route\": \"/candidate/interviewprocess/:candidateId\", \"pattern\": \"/candidate/interviewprocess/\\\\d+\", \"entity\": \"Candidate InterviewProcess\", \"roles\": [\"Admin\",\"Candidate\", \"Collaborator\",\"Hiring Lead\"]}," +
      "\"Preview_InterviewProcess_List\": { \"methods\": [\"GET\"], \"route\": \"/candidate/interviewprocess/list\", \"pattern\": \"/candidate/interviewprocess/list\", \"entity\": \"Candidate InterviewProcess\", \"roles\": [\"Admin\", \"Collaborator\",\"Hiring Lead\"]}," +
      "\"Create_SendMail\": { \"methods\": [\"POST\"], \"route\": \"/candidate/sendmail\", \"pattern\": \"/candidate/sendmail\", \"entity\": \"Candidate SendMail\", \"roles\": [\"Admin\",\"Hiring Lead\"]}," +
      "\"Multiple_Attachment\": { \"methods\": [\"POST\"], \"route\": \"/candidate/attachment\", \"pattern\": \"/candidate/attachment\", \"entity\": \"Candidate SendMail Attachment\", \"roles\": [\"Admin\",\"Hiring Lead\"]}," +
      "\"Create_Candidate_Offer\": { \"methods\": [\"POST\"], \"route\": \"/candidate/offerprocess/create\", \"pattern\": \"/candidate/offerprocess/create\", \"entity\": \"Candidate  OfferProcess\", \"roles\": [\"Admin\",\"Hiring Lead\"]}," +
      "\"OfferProcess_Update\": { \"methods\": [\"PUT\"], \"route\": \"/candidate/offerprocess/:candidateId\", \"pattern\": \"/candidate/offerprocess/\\\\d+\", \"entity\": \"Candidate  OfferProcess\", \"roles\": [\"Admin\",\"Candidate\",\"Hiring Lead\"]}," +
      "\"OfferProcess_Attachment\": { \"methods\": [\"POST\"], \"route\": \"/candidate/offerprocess/attachment\", \"pattern\": \"/candidate/offerprocess/attachment\", \"entity\": \"Candidate  OfferProcess\", \"roles\": [\"Admin\",\"Hiring Lead\"]}," +
      "\"Create_Candidate_Status\": { \"methods\": [\"POST\"], \"route\": \"/candidate/hiringprocess/create\", \"pattern\": \"/candidate/hiringprocess/create\", \"entity\": \"Candidate HiringProcess\", \"roles\": [\"Admin\",\"Candidate\",\"Hiring Lead\"]}," +
      "\"Create_Comments\": { \"methods\": [\"POST\"], \"route\": \"/candidate/comments/create\", \"pattern\": \"/candidate/comments/create\", \"entity\": \"Candidate Comments\", \"roles\": [\"Admin\",\"Candidate\", \"Collaborator\", \"Employee\",\"Hiring Lead\"]}," +
      "\"Delete_Comments\": { \"methods\": [\"DELETE\"], \"route\": \"/candidate/comments/delete\", \"pattern\": \"/candidate/comments/delete\", \"entity\": \"Candidate Comments\", \"roles\": [\"Admin\",\"Hiring Lead\"]}," +
      "\"Update_Comments_Details\": { \"methods\": [\"PUT\"], \"route\": \"/candidate/comments/update\", \"pattern\": \"/candidate/comments/update\", \"entity\": \"Candidate Comments\", \"roles\": [\"Admin\",\"Hiring Lead\"]}," +
      "\"Get_Job_Tasks\": { \"methods\": [\"GET\"], \"route\": \"/jobTask\", \"pattern\": \"/jobTask\", \"entity\": \"Job Tasks\", \"roles\": [\"Collaborator\", \"Admin\", \"Employee\",\"Hiring Lead\"]}," +
      "\"Add_Job_Task\": { \"methods\": [\"POST\"], \"route\": \"/jobTask/create\", \"pattern\": \"/jobTask/create\", \"entity\": \"Job Tasks\", \"roles\": [\"Admin\",\"Hiring Lead\"]}," +
      "\"Complete_Job_Tasks\": { \"methods\": [\"PUT\"], \"route\": \"/jobTask\", \"pattern\": \"/jobTask\", \"entity\": \"Job Tasks\", \"roles\": [\"Collaborator\",\"Hiring Lead\"]}," +
      "\"Email_Template_Variables\": { \"methods\": [\"GET\"], \"route\": \"/email_templates/variables\", \"pattern\": \"/email_templates/variables\", \"entity\": \"Email Template\", \"roles\": [\"Admin\"]}," +
      "\"Get_Email_Template\": { \"methods\": [\"GET\"], \"route\": \"/email_templates/:templateId\", \"pattern\": \"/email_templates/\\\\d+\", \"entity\": \"Email Template\", \"roles\": [\"Admin\"]}," +
      "\"Get_Email_Templates\": { \"methods\": [\"GET\"], \"route\": \"/email_templates/all/:organizationId\", \"pattern\": \"/email_templates/all/\\\\d+\", \"entity\": \"Email Template\", \"roles\": [\"Admin\"]}," +
      "\"Preview_Emails\": { \"methods\": [\"GET\"], \"route\": \"/email_templates/:templateId/preview\", \"pattern\": \"/email_templates/\\\\d+/preview\", \"entity\": \"Email Template\", \"roles\": [\"Admin\"]}," +
      "\"Send_Emails\": { \"methods\": [\"GET\"], \"route\": \"/email_templates/:templateId/send\", \"pattern\": \"/email_templates/\\\\d+/send\", \"entity\": \"Email Template\", \"roles\": [\"Admin\"]}," +
      "\"Delete_Email_Template\": { \"methods\": [\"DELETE\"], \"route\": \"/email_templates/:templateId\", \"pattern\": \"/email_templates/\\\\d+\", \"entity\": \"Email Template\", \"roles\": [\"Admin\"]}," +
      "\"Create_a_new_Email_Template\": { \"methods\": [\"POST\"], \"route\": \"/email_templates\", \"pattern\": \"/email_templates\", \"entity\": \"Email Template\", \"roles\": [\"Admin\"]}," +
      "\"Update_a_Email_Template\": { \"methods\": [\"PUT\"], \"route\": \"/email_templates/:templateId\", \"pattern\": \"/email_templates/\\\\d+\", \"entity\": \"Email Template\", \"roles\": [\"Admin\"]}," +
      "\"Create_a_PTO_Request\": { \"methods\": [\"POST\"], \"route\": \"/ptoRequests/create\", \"pattern\": \"/ptoRequests/create\", \"entity\": \"PTO Request\", \"roles\": [\"Admin\", \" Employee\"]}," +
      "\"Create_a_new_comment\": { \"methods\": [\"POST\"], \"route\": \"/ptoComments/create\", \"pattern\": \"/ptoComments/create\", \"entity\": \"PTO Request\", \"roles\": [\"Admin\", \" Employee\"]}," +
      "\"list_all__PTO_requests\": { \"methods\": [\"GET\"], \"route\": \"/ptoRequests\", \"pattern\": \"/ptoRequests\", \"entity\": \"PTO Request\", \"roles\": [\"Admin\", \" Employee\"]}," +
      "\"List_all_Used_PTO_requests\": { \"methods\": [\"GET\"], \"route\": \"/ptoRequests/used\", \"pattern\": \"/ptoRequests/used\", \"entity\": \"PTO Request\", \"roles\": [\"Admin\", \" Employee\"]}," +
      "\"List_available_hours\": { \"methods\": [\"GET\"], \"route\": \"/ptoAvailableHours\", \"pattern\": \"/ptoAvailableHours\", \"entity\": \"PTO Request\", \"roles\": [\"Admin\", \" Employee\"]}," +
      "\"List_upcoming_time_off\": { \"methods\": [\"GET\"], \"route\": \"/upcomingTimeOff\", \"pattern\": \"/upcomingTimeOff\", \"entity\": \"PTO Request\", \"roles\": [\"Admin\", \" Employee\"]}," +
      "\"Upload_attachments_in_comment\": { \"methods\": [\"POST\"], \"route\": \"/ptoComments/upload\", \"pattern\": \"/ptoComments/upload\", \"entity\": \"PTO Request\", \"roles\": [\"Admin\", \" Employee\"]}," +
      "\"Authenticate\": { \"methods\": [\"POST\"], \"route\": \"/login/authenticate\", \"pattern\": \"/login/authenticate\", \"entity\": \"Login\", \"roles\": [\"Only Anonymous\"]}," +
      "\"Logout\": { \"methods\": [\"POST\"], \"route\": \"/session/logout\", \"pattern\": \"/session/logout\", \"entity\": \"Login\", \"roles\": [\"Admin\", \"Employee\", \"Candidate\"]}," +
      "\"Getting_dropdownfields\": { \"methods\": [\"GET\"], \"route\": \"/task/project/fieldoption\", \"pattern\": \"/task/project/fieldoption\", \"entity\": \"Tasks\", \"roles\": [\"Admin\"]}," +
      "\"Creating_New_project\": { \"methods\": [\"POST\"], \"route\": \"/task/project/create\", \"pattern\": \"/task/project/create\", \"entity\": \"Projects\", \"roles\": [\"Admin\"]}," +
      "\"Update_a_Project\": { \"methods\": [\"PUT\"], \"route\": \"/task/project/update\", \"pattern\": \"/task/project/update\", \"entity\": \"Projects\", \"roles\": [\"Admin\"]}," +
      "\"Getting_list_of_projects_based_on_organization_id\": { \"methods\": [\"GET\"], \"route\": \"/task/project/list\", \"pattern\": \"/task/project/list\", \"entity\": \"Projects\", \"roles\": [\"Admin\"]}," +
      "\"Delete_Project\": { \"methods\": [\"DELETE\"], \"route\": \"/task/project/delete\", \"pattern\": \"/task/project/delete\", \"entity\": \"Projects\", \"roles\": [\"Admin\"]}," +
      "\"Getting_Project_based_on_organization_id_and_project_id\": { \"methods\": [\"GET\"], \"route\": \"/task/project\", \"pattern\": \"/task/project\", \"entity\": \"Projects\", \"roles\": [\"Admin\"]}," +
      "\"Getting_list_of_Tasks_based_on_organization_id\": { \"methods\": [\"GET\"], \"route\": \"/task/list\", \"pattern\": \"/task/list\", \"entity\": \"Tasks\", \"roles\": [\"Admin\"]}," +
      "\"Getting_Task_based_on_organization_id_and_task_id\": { \"methods\": [\"GET\"], \"route\": \"/task\", \"pattern\": \"/task\", \"entity\": \"Tasks\", \"roles\": [\"Admin\"]}," +
      "\"Delete_a_Task\": { \"methods\": [\"DELETE\"], \"route\": \"/task/delete/:taskId\", \"pattern\": \"/task/delete/\\\\d+\", \"entity\": \"Tasks\", \"roles\": [\"Admin\"]}," +
      "\"Creating_New_Task\": { \"methods\": [\"POST\"], \"route\": \"/task/create/\", \"pattern\": \"/task/create/\", \"entity\": \"Tasks\", \"roles\": [\"Admin\"]}," +
      "\"Updatig_a_Task\": { \"methods\": [\"PUT\"], \"route\": \"/task/update\", \"pattern\": \"/task/update\", \"entity\": \"Tasks\", \"roles\": [\"Admin\"]}," +
      "\"Updating_Task_Status_for_Multiple_task_ids\": { \"methods\": [\"PUT\"], \"route\": \"/task/update/status\", \"pattern\": \"/task/update/status\", \"entity\": \"Tasks\", \"roles\": [\"Admin\"]}," +
      "\"Talentpool_Preview_Candidate\": { \"methods\": [\"GET\"], \"route\": \"/candidate/talentpool\", \"pattern\": \"/candidate/talentpool\", \"entity\": \"Candidate\", \"roles\": [\"Admin\"]}," +
      "\"Update_Candidate\": { \"methods\": [\"PUT\"], \"route\": \"/candidate/update/:candidateId\", \"pattern\": \"/candidate/update/\\\\d+\", \"entity\": \"Candidate\", \"roles\": [\"Admin\", \"Candidate\"]}," +
      "\"Preview_Candidate_Status\": { \"methods\": [\"GET\"], \"route\": \"/candidate/list/status\", \"pattern\": \"/candidate/list/status\", \"entity\": \"Candidate\", \"roles\": [\"Admin\"]}," +
      "\"Organization_Image_And_Url\": { \"methods\": [\"GET\"], \"route\": \"/organization/urlandlogo/:id\", \"pattern\": \"/organization/urlandlogo/\\\\d+\", \"entity\": \"Organization\", \"roles\": [\"Anonymous\"]}," +
      "\"Organization_Domain_Availability_CHeck\": { \"methods\": [\"GET\"], \"route\": \"/organization/domain/availabilityCheck\", \"pattern\": \"/organization/domain/availabilityCheck\", \"entity\": \"Organization\", \"roles\": [\"Anonymous\"]}," +
      "\"Update_Job_Description\": { \"methods\": [\"PUT\"], \"route\": \"/jobdesc/:id\", \"pattern\": \"/jobdesc/\\\\d+\", \"entity\": \"Job\", \"roles\": [\"Admin\"]}," +
      "\"Delete_Job_Description\": { \"methods\": [\"DELETE\"], \"route\": \"/jobdesc/:id\", \"pattern\": \"/jobdesc/\\\\d+\", \"entity\": \"Job\", \"roles\": [\"Admin\"]}," +
      "\"Create_Highest_Education\": { \"methods\": [\"DELETE\"], \"route\": \"/jobdesc/:id\", \"pattern\": \"/jobdesc/\\\\d+\", \"entity\": \"Job\", \"roles\": [\"Admin\"]}," +
      "\"Get_All_Highest_Education\": { \"methods\": [\"GET\"], \"route\": \"/highesteducation/list\", \"pattern\": \"/highesteducation/list\", \"entity\": \"Field Options\", \"roles\": [\"Admin\",\"Anonymous\", \"Candidate\"]}," +
      "\"Get_Highest_Education\": { \"methods\": [\"GET\"], \"route\": \"/highesteducation/:id\", \"pattern\": \"/highesteducation/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\", \" Employee\"]}," +
      "\"Delete_Highest_Education\": { \"methods\": [\"DELETE\"], \"route\": \"/highesteducation/:id\", \"pattern\": \"/highesteducation/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Add_Highest_Education\": { \"methods\": [\"POST\"], \"route\": \"/highesteducation/create\", \"pattern\": \"/highesteducation/create\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Update_Highest_Education\": { \"methods\": [\"PUT\"], \"route\": \"/highesteducation/:id\", \"pattern\": \"/highesteducation/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Country_List\": { \"methods\": [\"GET\"], \"route\": \"/country/list\", \"pattern\": \"/country/list\", \"entity\": \"Field Options\", \"roles\": [\"Anonymous\"]}," +
      "\"Country_With_States\": { \"methods\": [\"GET\"], \"route\": \"/country_withstates/:countryId\", \"pattern\": \"/country_withstates/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Anonymous\"]}," +
      "\"Candidate_Login_Check\": { \"methods\": [\"GET\"], \"route\": \"/login/candidatecheck\", \"pattern\": \"/login/candidatecheck\", \"entity\": \"Field Options\", \"roles\": [\"Anonymous\"]}," +
      "\"Get_Candidate_Resume\": { \"methods\": [\"GET\"], \"route\": \"/candidate/resumes/:userid\", \"pattern\": \"/candidate/resumes/\\\\d+\", \"entity\": \"Candidate\", \"roles\": [\"Admin\",\"Employee\",\"Candidate\"]},"+
      "\"Update_welcome_details_video\": { \"methods\": [\"PUT\"], \"route\": \"/onboardingPacket/welcome/video/:organizationId\", \"pattern\": \"/onboardingPacket/welcome/video/\\\\d+\", \"entity\": \"Onboarding\", \"roles\": [\"Admin\"]}," +
      "\"Get_Job_Collaborator_List\": { \"methods\": [\"GET\"], \"route\": \"/job/collaborator/list\", \"pattern\": \"/job/collaborator/list\", \"entity\": \"Job\", \"roles\": [\"Admin\",\"Hiring Lead\"]}," +
      "\"Calling_Code\": { \"methods\": [\"GET\"], \"route\": \"/callingcode/\", \"pattern\": \"/callingcode/\", \"entity\": \"Phone Number\", \"roles\": [\"Anonymous\"]}," +
      "\"Get_All_Minimum_Experience\": { \"methods\": [\"GET\"], \"route\": \"/minimumexperience/list\", \"pattern\": \"/minimumexperience/list\", \"entity\": \"Field Options\", \"roles\": [\"Admin\",\"Anonymous\", \"Candidate\"]}," +
      "\"Get_Minimum_Experience\": { \"methods\": [\"GET\"], \"route\": \"/minimumexperience/:id\", \"pattern\": \"/minimumexperience/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\", \" Employee\", \"Candidate\"]}," +
      "\"Delete_Minimum_Experience\": { \"methods\": [\"DELETE\"], \"route\": \"/minimumexperience/:id\", \"pattern\": \"/minimumexperience/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Add_Minimum_Experience\": { \"methods\": [\"POST\"], \"route\": \"/minimumexperience/create\", \"pattern\": \"/minimumexperience/create\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Update_Minimum_Experience\": { \"methods\": [\"PUT\"], \"route\": \"/minimumexperience/:id\", \"pattern\": \"/minimumexperience/\\\\d+\", \"entity\": \"Field Options\", \"roles\": [\"Admin\"]}," +
      "\"Get_Dashboard_Count_Employee\": { \"methods\": [\"GET\"], \"route\": \"/employee/dashboard/count/:employee_id\", \"pattern\": \"/employee/dashboard/count/\\\\d+\", \"entity\": \"Employee\", \"roles\": [\"Employee\", \"Hiring Lead\", \"Collaborator\"]}," +
      "\"Get_Job_List_Employee\": { \"methods\": [\"GET\"], \"route\": \"/job/list/employee/:employeeId\", \"pattern\": \"/job/list/employee/\\\\d+\", \"entity\": \"Job\", \"roles\": [\"Hiring Lead\", \"Collaborator\"]}," +
      "\"Default_Currency\": { \"methods\": [\"GET\"], \"route\": \"/organization/defaultCurrency/\", \"pattern\": \"/organization/defaultCurrency\", \"entity\": \"Currency\", \"roles\": [\"Anonymous\"]}," +
      "\"Country_With_Region_Code\": { \"methods\": [\"GET\"], \"route\": \"/country_withstates/:country_region_code\", \"pattern\": \"/country_withstates/:country_region_code\", \"entity\": \"Candidate\", \"roles\": [\"Anonymous\"]}," +
      "\"Delete_Candidate_Document\": { \"methods\": [\"DELETE\"], \"route\": \"/candidate/resume/:filePath\", \"pattern\": \"/candidate/resume/.*\", \"entity\": \"Candidate\", \"roles\": [\"Admin\", \"Employee\", \"Candidate\"]}," +
      "\"Clone_Job\": { \"methods\": [\"POST\"], \"route\": \"/job/clone\", \"pattern\": \"/job/clone\", \"entity\": \"Job\", \"roles\": [\"Admin\"]}," +
      "\"Get_Logged_In_User_Roles\": { \"methods\": [\"GET\"], \"route\": \"/login/user/roles\", \"pattern\": \"/login/user/roles\", \"entity\": \"Login\", \"roles\": [\"Admin\", \"Employee\", \"Candidate\", \"Super Owner\", \"Collaborator\", \"Hiring Lead\"]}" + "}"

  );

  public static String[] DEFAULT_ROLES = {"Admin", "Employee", "Candidate", "Super Owner", "Collaborator", "Hiring Lead"};
}