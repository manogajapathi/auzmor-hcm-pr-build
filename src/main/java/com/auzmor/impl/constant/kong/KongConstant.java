package com.auzmor.impl.constant.kong;

public class KongConstant {
  public static final String JWT = "jwt";
  public static final String PLUGIN = "plugins";
  public static final String ACL = "acls";
}
