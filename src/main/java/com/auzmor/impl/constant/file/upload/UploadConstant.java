package com.auzmor.impl.constant.file.upload;

import io.vertx.core.Vertx;

public class UploadConstant {
  public static String UPLOAD_DIRECTORY = Vertx.currentContext().config()
      .getString(Environment.DEFAULT_UPLOAD_DIRECTORY);
  // Environment Constants
  public static class Environment {
    // Default upload directory
    public static final String DEFAULT_UPLOAD_DIRECTORY = "server.upload.defaultDirectory";
    public static final String PUBLIC_STATIC_ROOT = "server.publicStaticRoot";
    public static final String PUBLIC_STATIC_IMAGES = "server.publicStaticImages";
    public static final String PRIVATE_STATIC_ROOT = "server.privateStaticRoot";
    public static final String PUBLIC_STATIC_UPLOAD_ROOT = "server.privateStaticUploadRoot";
    public static final String PRIVATE_STATIC_CANDIDATE_FILE = "server.privateStaticCandidateFile";
  }
}
