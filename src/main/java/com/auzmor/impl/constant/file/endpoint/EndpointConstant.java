package com.auzmor.impl.constant.file.endpoint;

import com.auzmor.impl.constant.file.upload.*;
import io.vertx.core.Vertx;

import java.io.File;

public class EndpointConstant {
  public static final String publicStaticRoot = Vertx.currentContext().config()
      .getString(UploadConstant.Environment.PUBLIC_STATIC_ROOT);
  public static final String privateStaticRoot = Vertx.currentContext().config()
      .getString(UploadConstant.Environment.PRIVATE_STATIC_ROOT);
  public static final String publicStaticImage = publicStaticRoot + File.separator +
      Vertx.currentContext().config().getString(UploadConstant.Environment.PUBLIC_STATIC_IMAGES);
  public static final String privateStaticUploadRoot = Vertx.currentContext().config()
      .getString(UploadConstant.Environment.PUBLIC_STATIC_UPLOAD_ROOT);
  public static final String publicStaticCandidateFile = privateStaticUploadRoot + File.separator +
      Vertx.currentContext().config().getString(UploadConstant.Environment.PRIVATE_STATIC_CANDIDATE_FILE);
}
