package com.auzmor.impl.constant.nginx;

import io.vertx.core.Vertx;

public class NginxConstant {
  public static String CONF_DIRECTORY_PATH = Vertx.currentContext().config()
      .getString(Environment.CONF_DIRECTORY);

  public static class Environment {
    public static String CONF_DIRECTORY = "nginx.confDir";
  }
}
