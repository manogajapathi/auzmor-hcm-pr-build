package com.auzmor.impl.enumarator.kong;

public enum KongHttpStatusCode {
  ADD(201),
  EDIT(200),
  DELETE(204),
  GET(200);

  private int statusCode;
  KongHttpStatusCode(int statusCode) {
    this.statusCode = statusCode;
  }

  public int getStatusCode() {
    return this.statusCode;
  }
}
