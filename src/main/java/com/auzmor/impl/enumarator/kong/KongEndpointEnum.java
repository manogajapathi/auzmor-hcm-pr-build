package com.auzmor.impl.enumarator.kong;

public enum KongEndpointEnum {
  API("/apis/"),
  CONSUMER("/consumers/"),
  SNI("/snis/"),
  CERTIFICATE("/certificates/"),
  UPSTREAM("/upstreams/"),
  TARGET("/targets/"),
  PLUGIN("/plugins/"),
  ACL("/acls/");


  private String endpointUrl;
  KongEndpointEnum(String endpointUrl) {
    this.endpointUrl = endpointUrl;
  }

  public String getEndpointUrl() {
    return this.endpointUrl;
  }
}
