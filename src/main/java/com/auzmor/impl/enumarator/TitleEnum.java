package com.auzmor.impl.enumarator;

public enum TitleEnum {
  EXISTS_ALREADY("Title Already exists"),
  ADDITION_FAILED("Title could not be added"),
  ADDITION_SUCCESS("Title Added successfully"),
  UPDATE_SUCCESS("Title Updated successfully"),
  DELETE_SUCCESS("Title Deleted successfully"),
  CANNOT_DELETE("Title Cannot be deleted"),
  NO_SUCH_VALUE("No such value present"),
  VALUE_SUCCESS("Value retrived"),
  RESTRICTED("Title already used in Jobs"),
  NO_CHANGES_MADE("No changes made");

  private String description;
  TitleEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
