package com.auzmor.impl.enumarator;

public enum FieldOptionsEnum {
	VALUE_SUCCESS("Value retrived"),
	NO_SUCH_VALUE("No such value");

	private String description;
	FieldOptionsEnum(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}
}
