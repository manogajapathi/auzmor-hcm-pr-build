package com.auzmor.impl.enumarator;

public enum NewsFeedEnum {
  VALUE_SUCCESS("Value retrieved");

  private String description;
  NewsFeedEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
