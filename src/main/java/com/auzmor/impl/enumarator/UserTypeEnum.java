package com.auzmor.impl.enumarator;

public enum UserTypeEnum {
  ADMIN("admin"),
  EMPLOYEE("employee"),
  CANDIDATE("candidate");

  private String userTypeValue;
  UserTypeEnum(String userTypeValue) {
    this.userTypeValue = userTypeValue;
  }

  public String getUserTypeValue() {
    return userTypeValue;
  }
}

