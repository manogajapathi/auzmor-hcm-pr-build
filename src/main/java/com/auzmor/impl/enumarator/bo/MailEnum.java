package com.auzmor.impl.enumarator.bo;

public enum MailEnum {
  SEND_MAIL_SUCCESS("Email has been Send Successfully"),
  SEND_MAIL_FAILED("Email Cannot be Send");

  private String description;
  MailEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
