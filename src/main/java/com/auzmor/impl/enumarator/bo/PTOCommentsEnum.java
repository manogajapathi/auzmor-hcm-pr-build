package com.auzmor.impl.enumarator.bo;

public enum PTOCommentsEnum {
  EXISTS_ALREADY("Comment Already exists"),
  ADDITION_FAILED("Comment could not be added"),
  ADDITION_SUCCESS("Comment Added successfully"),
  UPDATE_SUCCESS("Comment Updated successfully"),
  NO_UPDATE("No new changes for update"),
  DELETE_SUCCESS("Comment Deleted successfully"),
  CANNOT_DELETE("Comment Cannot be deleted"),
  NO_SUCH_VALUE("No such value present"),
  NO_ACCESS("User not allowed to update this comment"),
  VALUE_SUCCESS("Value retrived");

  private String description;
  PTOCommentsEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
