package com.auzmor.impl.enumarator.bo;

public enum CandidateCommentEnum {

  SUCCESS("Thanks for applying,Login Information has been sent successfully to your e-mail!"),
  FAILED("Required Fields are missing"),
  UPDATE_SUCCESS("Comment Updated Successfully"),
  UPDATE_FAILED("you are not supposed to edit this comment"),
  UPDATE_NO_CHANGES("No changes are updated"),
  DELETE_SUCCESS("Delete success");

  private String description;
  CandidateCommentEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
