package com.auzmor.impl.enumarator.bo;

public enum JobBOEnum {

  INVALID_JOB_ID("Invalid Job ID Entered"),
  COLLOBARATOR_LIST_RETREIVED("Collaborator List Retreived"),
  JOB_POST_SUCCESS("Job Posted Successfully"),
  JOB_POST_INVALID("Job Post Invalid");
  private String description;
  JobBOEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
