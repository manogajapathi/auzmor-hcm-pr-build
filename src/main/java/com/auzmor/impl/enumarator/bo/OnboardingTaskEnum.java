package com.auzmor.impl.enumarator.bo;

public enum OnboardingTaskEnum {
  UPDATE_SUCCESS("Task details updated successfully"),
  UPDATE_FAILURE("No values for update"),
  ADD_SUCCESS("Task add successfully"),
  ALREADY_EXISTS("Task already exists"),
  ADD_FAILURE("Task could not be added"),
  NO_SUCH_VALUE("No such value present"),
  DELETE_SUCCESS("Task Deleted"),
  DELETE_FAILURE("Task could not Deleted"),
  VALUE_SUCCESS("Value retrived");

  private String description;
  OnboardingTaskEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
