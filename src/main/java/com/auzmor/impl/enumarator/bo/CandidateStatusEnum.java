package com.auzmor.impl.enumarator.bo;

public enum CandidateStatusEnum {
  EXISTS_ALREADY("Job Already Applied,please login to view your applied jobs!"),
  SEND_FAILED("Sending Email to the user failed"),
  EXISTS_JOB_ALREADY("Job Already Applied!"),
  APPLIED_JOB("Thanks for applying,please login to view your applied jobs!"),
  SUCCESS_MESSAGE("Candidate Applied Successfully!"),
  SUCCESS_MESSAGE_STATUS("Candidate Successfully Moved to Talent Pool!"),
  SUCCESS_MESSAGE_STATUS_EXISTS("Candidate Already Moved to Talent Pool!"),
  SUCCESS("Thanks for applying,Login Information has been sent successfully to your e-mail!"),
  FAILED("Required Fields are missing"),
  FORWARD_SUCCESS("Candidate Forward to Employee Successfully!"),
  FORWARD_FAILED("Candidate Forward to Employee Failed!"),
  MOVE_SUCCESS("Candidate Moved Successfully!"),
  MOVE_FAILED("Candidate Moved Failed!"),
  STATUS_SUCCESS("Candidate Status Updated Successfully!"),
  STATUS_FAILED("Candidate Status Update Failed!"),
  UPDATE_SUCCESS("Candidate Updated Successfully!"),
  UPDATE_FAILED("Candidate Update Failed!"),
  GET_CANDIDATE_SUCCESS("Getting candidate list!"),
  GET_CANDIDATE_FAILED("Getting candidate list failed!"),
  CANDIDATE_DELETED_SUCESSFULLY("Candidate Deleted Sucessfully"),
  CANDIDATE_DELETED_FAILED("Candidate Deleted Failed"),
  CANDIDATE_STATUS_INVALID("Candidate Status Invalid"),
  LOGIN_CHECK("Candidate Applied Successfully!"),
  LOGIN_STATUS("Candidate Successfully Moved to Talent Pool!"),
  NO_DATA_STATUS("No data false status"),
  NO_RESUME("Resumes not available for this candidate"),
  NOT_ALLOWED_EMAIL("You are not supposed to apply for this Job!"),
  LIST_AVAILABLE_RESUMES("List of available resumes for this candidate");

  private String description;

  CandidateStatusEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
