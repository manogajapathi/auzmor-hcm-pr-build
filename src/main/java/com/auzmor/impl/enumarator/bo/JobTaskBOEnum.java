package com.auzmor.impl.enumarator.bo;

public enum  JobTaskBOEnum {
  TASK_ADDED("Job task added successfully"),
  TASK_ADD_FAILED("Failed due to server problem"),
  ALREADY_EXISTS("Task already assigned to Employee"),
  FAILED("Failed due to some db inconsistency reason"),
  MISSING_DUE("Both due date and complete within values are missing"),
  MISSING_COMPLETE_WITHIN("Complete within cannot be null"),
  NO_VALUES("No tasks assigned to employee"),
  UPDATE_TASK_SUCCESS("Tasks completed"),
  FUTURE_DATE("Due date should be in future"),
  CURRENT_CANDIDATES("All Current candidates are in advanced stage, " +
      "Job Tasks will be added for new candidates"),
  VALUE_SUCCESS("Value retrived"),
  IS_COLLABORATOR("This user is a Collaborator");

  private String description;
  JobTaskBOEnum(String description) {
    this.description = description;
  }

  public  String getDescription() {
    return description;
  }
}
