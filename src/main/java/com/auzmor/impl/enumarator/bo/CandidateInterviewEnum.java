package com.auzmor.impl.enumarator.bo;

public enum CandidateInterviewEnum {
  SUCCESS("Interview Scheduled Successfully!"),
  NO_UPDATE("No Changes are Done!"),
  FAILED("Interview Scheduled has been Failed!"),
  STATUS_REJECT(" Thank you for your response." +
      "We are disappointed that you are not satisfied," +
      " but will take feedback into consideration! " +
      "If necessary,someone from our company will reach out to you."),
  STATUS_SUCCESS("Thank you for your response!" +
      " We look forward to speaking with you!"),
  SEND_FAILED("Sending Interview Details Failed"),
  UPDATE_STATUS("Interview Scheduled has been updated Successfully"),
  UPDATE_FAILED("Interview Scheduled has been updated Failed!"),
  SEND_MAIL_SUCCESS("Email has been Send Successfully"),
  SEND_MAIL_FAILED("Email Cannot be Send"),
  GET_INTERVIEW_SUCCESS("Getting Interview List"),
  GET_INTERVIEW_FAILED("Getting Interview List Failed"),
  COMPLETED_SUCCESS("Interview Completed Successfully"),
  FUTURE_DATE("Interview Date and Time should be in future"),
  ACCESS_CHECK("You Cannot Consume this Service");


  private String description;
  CandidateInterviewEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
