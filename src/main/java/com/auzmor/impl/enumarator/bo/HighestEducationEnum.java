package com.auzmor.impl.enumarator.bo;

public enum HighestEducationEnum {
  EXISTS_ALREADY("Highest Education Already exists"),
  ADDITION_FAILED("Highest Education could not be added"),
  ADDITION_SUCCESS("Highest Education Added successfully"),
  UPDATE_SUCCESS("Highest Education Updated successfully"),
  DELETE_SUCCESS("Highest Education Deleted successfully"),
  CANNOT_DELETE("Highest Education Cannot be deleted"),
  NO_SUCH_VALUE("No such value present"),
  VALUE_SUCCESS("Value retrived"),
  RESTRICTED("Highest Education used in Candidate"),
  NO_CHANGES_MADE("No changes made");

  private String description;
  HighestEducationEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
