package com.auzmor.impl.enumarator.bo;

public enum CandidateOfferEnum {
  SUCCESS("Offer Extended Successfully!"),
  FAILED("Offer Extended has been Failed!"),
  NO_UPDATE("No Changes are Done!"),
  STATUS_REJECT(" Thank you for your response." +
      "We are disappointed that you are not satisfied," +
      " but will take feedback into consideration! " +
      "If necessary,someone from our company will reach out to you."),
  STATUS_SUCCESS("Thank you for your response!" +
      " We look forward to speaking with you!"),
  SEND_FAILED("Sending Offer Extended Details Failed"),
  UPDATE_STATUS("Offer Extended has been updated Successfully"),
  UPDATE_FAILED("Offer Extended Update has been Failed"),
  SEND_MAIL_SUCCESS("Email has been Send Successfully"),
  ACCESS_CHECK("You Cannot Consume this Service"),
  SEND_MAIL_FAILED("Email Cannot be Send");

  private String description;
  CandidateOfferEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
