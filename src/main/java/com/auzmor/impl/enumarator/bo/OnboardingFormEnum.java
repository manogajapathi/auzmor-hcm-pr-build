package com.auzmor.impl.enumarator.bo;

public enum OnboardingFormEnum {
  UPDATE_SUCCESS("Form details updated successfully"),
  UPDATE_FAILURE("No values for update"),
  ADD_SUCCESS("Form add successfully"),
  ALREADY_EXISTS("Form already exists"),
  ADD_FAILURE("Form could not be added"),
  NO_SUCH_VALUE("No such value present"),
  DELETE_SUCCESS("Form Deleted"),
  DELETE_FAILURE("Form could not Deleted"),
  VALUE_SUCCESS("Value retrived");

  private String description;
  OnboardingFormEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
