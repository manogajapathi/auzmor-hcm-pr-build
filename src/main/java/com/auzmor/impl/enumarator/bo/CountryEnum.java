package com.auzmor.impl.enumarator.bo;

public enum CountryEnum {
  SUCCESS("Getting states details from CountryId"),
  FAILED("Getting states details failed"),
  NO_DATA_STATUS("No data available");
  private String description;

  CountryEnum(String description) {
    this.description = description;

  }

  public String getDescription() {
    return description;
  }

}
