package com.auzmor.impl.enumarator.bo;

public enum MinimumExperienceEnum {
  EXISTS_ALREADY("Minimum Experience Already exists"),
  ADDITION_FAILED("Minimum Experience could not be added"),
  ADDITION_SUCCESS("Minimum Experience Added successfully"),
  UPDATE_SUCCESS("Minimum Experience Updated successfully"),
  DELETE_SUCCESS("Minimum Experience Deleted successfully"),
  CANNOT_DELETE("Minimum Experience Cannot be deleted"),
  NO_SUCH_VALUE("No such value present"),
  VALUE_SUCCESS("Value retrived"),
  RESTRICTED("Minimum Experience already used in Jobs"),
  NO_CHANGES_MADE("No changes made");

  private String description;
  MinimumExperienceEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
