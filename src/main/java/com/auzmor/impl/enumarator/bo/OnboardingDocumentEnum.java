package com.auzmor.impl.enumarator.bo;

public enum OnboardingDocumentEnum {
  UPDATE_SUCCESS("Document details updated successfully"),
  UPDATE_FAILURE("No values for update"),
  ADD_SUCCESS("Document added successfully"),
  ALREADY_EXISTS("Task already exists"),
  ADD_FAILURE("Document could not be added"),
  NO_SUCH_VALUE("No such value present"),
  DELETE_SUCCESS("Document Deleted"),
  DELETE_FAILURE("Document could not Deleted"),
  VALUE_SUCCESS("Value retrived");

  private String description;
  OnboardingDocumentEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
