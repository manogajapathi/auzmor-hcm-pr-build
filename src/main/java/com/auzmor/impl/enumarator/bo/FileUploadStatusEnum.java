package com.auzmor.impl.enumarator.bo;

public enum FileUploadStatusEnum {
  IMAGE_SUCCESS("File Upload Successfully!"),
  IMAGE_FAILED("You can upload only png/jpeg images!"),
  FILE_SIZE("File size issues"),
  SUCCESS("File Upload Successfully!"),
  FAILED("You can only upload PDF/DOC file!");

  private String description;
  FileUploadStatusEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
