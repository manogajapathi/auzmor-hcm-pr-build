package com.auzmor.impl.enumarator.bo;

import org.omg.PortableInterceptor.SUCCESSFUL;

public enum OrganizationBOEnum {
  ATS_DOMAIN_NOT_AVAILABLE("Required ATS Domain is not available", "Required ATS Domain is not available"),
  CAREER_DOMAIN_NOT_AVAILABLE("Required Career Domain is not available", "Required Career Domain is not available"),
  DOMAINS_UPDATE_SUCCESS("Domains updated successfully", "Domains updated successfully"),
  DOMAIN_NOT_AVAILABLE("Domain not available", "Domain not available"),
  DOMAIN_AVAILABLE_CHECK("Domain Available Check", "Domain Available Check"),
  DOMAIN_ALREADY_EXISTS("Domain already Exist","Domain Already Exists"),
  SUCCESSFUL("Success", "Success"),
  FAILED("Organization creation failed","Organization creation failed");

  private String message;
  private String description;
  OrganizationBOEnum(String message, String description) {
    this.message = message;
    this.description = description;
  }

  public String getMessage() {
    return message;
  }
  public String getDescription() {
    return description;
  }
}
