package com.auzmor.impl.enumarator.bo;

public enum OnboardingPacketEnum {
  UPDATE_SUCCESS("Packet details updated successfully"),
  UPDATE_FAILURE("No values for update"),
  ADD_SUCCESS("Packet add successfully"),
  ALREADY_EXISTS("Packet already exists"),
  ADD_FAILURE("Packet could not be added"),
  NO_SUCH_VALUE("No such value present"),
  REMAINDER_SENT("Remainder mail sent"),
  DELETE_SUCCESS("Packet Deleted"),
  DELETE_FAILURE("Packet could not Deleted"),
  PACKET_ALREADY_SENT("Packet already sent"),
  PACKET_SENT("Packet sent"),
  VALUE_SUCCESS("Value retrived");

  private String description;
  OnboardingPacketEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
