package com.auzmor.impl.enumarator.bo;

public enum EmployeePTOEnum {
  REQUEST_ADDED("Request added sucessfully"),
  REQUEST_ADD_FAILED("Failed due to server problem"),
  ALREADY_EXISTS("Request already exists for the same dates"),
  MAX_VALUE_MISSING("Maximum value for time off missing, Contact admin for help"),
  EXCEEDED_MAX_VALUE("Maximum allowed hours exceeded"),
  FAILED("Failed due to some db inconsistency reason"),
  FUTURE_DATE_NOT_APPLICABLE("Future date is not applicable for Sick type PTO"),
  DELETE_SUCCESS("deleted successfully"),
  UPDATE_SUCCESS("updated success"),
  UPDATE_FAILED("updated failure"),
  DELETE_FAILED("not able to delete the Request"),
  TABLE_EMPTY("table is empty"),
  PARAMS_MISSING("From Date/ To date is missing"),
  TABLE_VALUES("view");

  private String description;
  EmployeePTOEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
