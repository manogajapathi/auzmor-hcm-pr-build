package com.auzmor.impl.enumarator.bo;

public enum EmployeeBOEnum {
  EMPLOYEE_ADDED("Employee added sucessfully"),
  EMPLOYEE_ADD_FAILED("Failed due to server problem"),
  ALREADY_EXISTS("Employee already exists"),
  FAILED("Failed due to some db inconsistency reason"),
  DELETE_SUCCESS("deleted successfully"),
  UPDATE_SUCCESS("updated success"),
  UPDATE_FAILED("updated failure"),
  DELETE_FAILED("not able to delete the user"),
  USER_ALREADY_EXISTS("user name already exists with mail"),
  TABLE_EMPTY("table is empty"),
  TABLE_VALUES("view"),
  IMAGE_FORMAT_WRONG("not a proper image"),
  EMPLOYEE_ACCESS("employee"),
  ADMIN_ACCESS("admin");

  private String description;
  EmployeeBOEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
