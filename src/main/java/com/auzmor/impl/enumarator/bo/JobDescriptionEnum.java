package com.auzmor.impl.enumarator.bo;

public enum JobDescriptionEnum {
  EXISTS_ALREADY("Description Already exists"),
  ADDITION_FAILED("Description could not be added"),
  ADDITION_SUCCESS("Description Added successfully"),
  UPDATE_SUCCESS("Description Updated successfully"),
  DELETE_SUCCESS("Description Deleted successfully"),
  CANNOT_DELETE("Description Cannot be deleted"),
  NO_SUCH_VALUE("No such value present"),
  VALUE_SUCCESS("Value retrived"),
  RESTRICTED("Job Description already used in Jobs"),
  NO_CHANGES_MADE("No changes made");

  private String description;
  JobDescriptionEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
