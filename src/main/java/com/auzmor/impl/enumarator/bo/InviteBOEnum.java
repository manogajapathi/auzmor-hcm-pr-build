package com.auzmor.impl.enumarator.bo;

public enum InviteBOEnum {
  EXISTS_ALREADY("Employee already exists in organization"),
  PENDING("An Invite is already Pending for the requested mail"),
  SEND_FAILED("Sending Email to the user failed"),
  TOKEN_GENERATION_FAILED("Sending Email to the user failed"),
  INVITE_SUCCESS("Invited successfully"),

  TOKEN_NOT_VALID("Invite token is not valid"),
  TOKEN_VALID("Token Valid"),
  INVITE_NOT_FOUND("The invite is not valid with organization/user"),
  EMPLOYEE_ADD_FAILED("Unable to add the employee"),
  EMPLOYEE_ADDED("Added the ivited employee"),
  NO_INVITE_PENDING("No Invite has been initiated previously"),
  FAILED("Failed to add due to db inconsistency"),

  CANCEL_SUCCESS("Cancelled successfully"),

  MAIL_TYPE_INVITE("Invitation mail"),
  MAIL_TYPE_SIGNUP_THANKS("THANKS mail");

  private String description;
  InviteBOEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}