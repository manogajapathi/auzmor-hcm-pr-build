package com.auzmor.impl.enumarator.bo;

public enum LoginBOEnum {
  ALREADY_EXISTS("User already exists"),
  PASSWORD_MATCH_FAILED("Password match failed"),
  USER_LOGIN_ADDED("User added successfully"),
  PASSWORD_CHANGED("Password updated successfully"),
  PASSWORD_NOCHANGE("No changes to old and new password"),
  PASSWORD_INCORRECT("Password is incorrect"),
  EMPLOYEE_APPROVED("employee approved "),
  EMPLOYEE_REJECTED("employee rejected"),
  RESET_MAIL_SENT("A link has been sent to E-Mail Address provided, Please follow instructions"),
  EMPLOYEE_APPROVED_PACKETSENT("employee approved and packet has been sent"),
  REDIRECT("Redirect link"),
  EMPLOYEE_NOT_EXISTS("Please use your registered E-Mail Address"),
  INVALID_DATA("invalid data"),
  CANDIDATE_LOGIN_FAIL("candidate not exist with this organization"),
  CANDIDATE_LOGIN_SUCCESS("candidate is already present with this organization"),
  LINK_USED_EMPLOYEE("Link has been used once"),
  LINK_USED_CANDIDATE("Link has been used once"),
  CANDIDATE("candidate");
  private String description;

  LoginBOEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
