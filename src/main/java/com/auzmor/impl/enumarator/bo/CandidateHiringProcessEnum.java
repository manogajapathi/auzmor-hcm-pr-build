package com.auzmor.impl.enumarator.bo;

public enum CandidateHiringProcessEnum {
  SUCCESS("Candidate status inserted successfully!"),
  SEND_MAIL_FAILED("Sending Mail Failed!"),
  FAILED("Candidate status insertion failed");
  private String description;
  CandidateHiringProcessEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
