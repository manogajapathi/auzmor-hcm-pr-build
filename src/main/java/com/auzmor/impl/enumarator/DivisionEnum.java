package com.auzmor.impl.enumarator;

public enum DivisionEnum {
  EXISTS_ALREADY("Division Already exists"),
  ADDITION_FAILED("Division could not be added"),
  ADDITION_SUCCESS("Division Added successfully"),
  UPDATE_SUCCESS("Division Updated successfully"),
  DELETE_SUCCESS("Division Deleted successfully"),
  CANNOT_DELETE("Division cannot be deleted"),
  NO_SUCH_VALUE("No such value present"),
  VALUE_SUCCESS("Value retrived");

  private String description;
  DivisionEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
