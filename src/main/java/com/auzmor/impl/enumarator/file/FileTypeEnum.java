package com.auzmor.impl.enumarator.file;

public enum FileTypeEnum {
  ENV_CONF(".conf", "environment/conf"), // custom content type
  PNG(".png", "image/png"),
  JPEG(".jpeg", "image/jpeg"),
  DOC(".doc", "application/msword"),
  DOCX(".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"),
  PDF(".pdf", "application/pdf"),
  GIF(".gif","image/gif"),
  ICO(".ico","image/x-icon"),
  CRT(".crt","application/x-x509-ca-cert"),
  KEY(".key","application/pkcs8"),
  MP4(".mp4", "video/mp4"),
  JPG(".jpg","image/jpg"),
  PPT(".ppt","application/vnd.ms-powerpoint"),
  XLS(".xls","application/vnd.ms-excel"),
  TXT(".txt","text/plain"),
  XLSX(".xlsx","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

  private String extension;
  private String contentType;
  FileTypeEnum(String extension, String contentType) {
    this.extension = extension;
    this.contentType = contentType;
  }

  public String getExtension() {
    return extension;
  }

  public String getContentType() { return contentType;}
}
