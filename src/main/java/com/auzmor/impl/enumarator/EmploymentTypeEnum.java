package com.auzmor.impl.enumarator;

public enum EmploymentTypeEnum {
  EXISTS_ALREADY("Employment Type Already exists"),
  ADDITION_FAILED("Employment Type could not be added"),
  ADDITION_SUCCESS("Employment Type Added successfully"),
  UPDATE_SUCCESS("Employment Type Updated successfully"),
  DELETE_SUCCESS("Employment Type Deleted successfully"),
  NO_SUCH_VALUE("No such value present"),
  VALUE_SUCCESS("Value retrived"),
  CANNOT_DELETE("Employment Type cannot be deleted"),
  RESTRICTED("Employment Type already used in Jobs"),
  NO_CHANGES_MADE("No changes made");

  private String description;
  EmploymentTypeEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
