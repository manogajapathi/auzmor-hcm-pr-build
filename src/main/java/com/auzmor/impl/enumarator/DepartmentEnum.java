package com.auzmor.impl.enumarator;

public enum DepartmentEnum {
  EXISTS_ALREADY("Department Already exists"),
  ADDITION_FAILED("Department could not be added"),
  ADDITION_SUCCESS("Department Added successfully"),
  UPDATE_SUCCESS("Department Updated successfully"),
  DELETE_SUCCESS("Department Deleted successfully"),
  CANNOT_DELETE("Department Cannot be deleted"),
  NO_SUCH_VALUE("No such value present"),
  VALUE_SUCCESS("Value retrived"),
  RESTRICTED("Department used in job"),
  NO_CHANGES_MADE("No changes made");

  private String description;
  DepartmentEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
