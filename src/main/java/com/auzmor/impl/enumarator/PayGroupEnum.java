package com.auzmor.impl.enumarator;

public enum PayGroupEnum {
	EXISTS_ALREADY("Pay Group Already exists"),
	ADDITION_FAILED("Pay Group could not be added"),
	ADDITION_SUCCESS("Pay Group Added successfully"),
	UPDATE_SUCCESS("Pay Group Updated successfully"),
	DELETE_SUCCESS("Pay Group Deleted successfully"),
	CANNOT_DELETE("Pay Group cannot be deleted"),
	NO_SUCH_VALUE("No such value present"),
	VALUE_SUCCESS("Value retrived");

	private String description;
	PayGroupEnum(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}
}
