package com.auzmor.impl.enumarator;

public enum OnboardingWelcomeEnum {
  UPDATE_SUCCESS("Welcome details updated successfully"),
  UPDATE_FAILURE("No values for update"),
  VIDEO_UPLOAD_SUCCESS("Video upload successfully"),
  VIDEO_UPLOAD_FAILURE("Video could not upload"),
  VIDEO_FORMAT_FAILURE("Improper format"),
  NO_SUCH_VALUE("No such value present"),
  NO_FILE_PRESENT("File Not available"),
  VALUE_SUCCESS("Value retrived");

  private String description;
  OnboardingWelcomeEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }

}
