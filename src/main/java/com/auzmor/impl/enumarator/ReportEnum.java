package com.auzmor.impl.enumarator;

public enum ReportEnum {
  EXISTS_ALREADY("Report Already exists"),
  ADDITION_FAILED("Report could not be added"),
  ADDITION_SUCCESS("Report Added successfully"),
  UPDATE_SUCCESS("Report Updated successfully"),
  DELETE_SUCCESS("Report Deleted successfully"),
  CANNOT_DELETE("Report Cannot be deleted"),
  NO_SUCH_VALUE("No such value present"),
  VALUE_SUCCESS("Value retrived"),
  DATA_AVAILABLE("no_data is false"),
  DATA_NOT_AVAILABLE("no_data is true");


  private String description;
  ReportEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }
}
