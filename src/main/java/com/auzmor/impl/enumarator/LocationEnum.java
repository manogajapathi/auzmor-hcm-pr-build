package com.auzmor.impl.enumarator;

public enum LocationEnum {
  EXISTS_ALREADY("Location Already exists"),
  ADDITION_FAILED("Location could not be added"),
  ADDITION_SUCCESS("Location Added successfully"),
  UPDATE_SUCCESS("Location Updated successfully"),
  DELETE_SUCCESS("Location Deleted successfully"),
  CANNOT_DELETE("Location Cannot be deleted"),
  NO_SUCH_VALUE("No such value present"),
  VALUE_SUCCESS("Value retrived"),
  RESTRICTED("Location already used in Jobs"),
  NO_CHANGES_MADE("No changes made");

  private String description;
  LocationEnum(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }

}
