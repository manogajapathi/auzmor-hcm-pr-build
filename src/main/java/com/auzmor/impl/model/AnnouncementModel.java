package com.auzmor.impl.model;


public class AnnouncementModel {
    private int announcement_id;
    private String announcement_title;
    private String announcement_details;
    private String start_date;
    private String end_date;
    private String audience;
    private String specific_audience;
    private int organization_id;

    public int getAnnouncement_id() {
        return announcement_id;
    }

    public void setAnnouncement_id(int announcement_id) {
        this.announcement_id = announcement_id;
    }

    public String getAnnouncement_title() {
        return announcement_title;
    }

    public void setAnnouncement_title(String announcement_title) {
        this.announcement_title = announcement_title;
    }

    public String getAnnouncement_details() {
        return announcement_details;
    }

    public void setAnnouncement_details(String announcement_details) {
        this.announcement_details = announcement_details;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getAudience() {
        return audience;
    }

    public void setAudience(String audience) {
        this.audience = audience;
    }

    public String getSpecific_audience() {
        return specific_audience;
    }

    public void setSpecific_audience(String specific_audience) {
        this.specific_audience = specific_audience;
    }

    public int getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(int organization_id) {
        this.organization_id = organization_id;
    }
}
