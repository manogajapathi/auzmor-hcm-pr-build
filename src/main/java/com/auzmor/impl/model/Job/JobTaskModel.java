package com.auzmor.impl.model.Job;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

public class JobTaskModel {
  private long id;
  private long employeeId;
  private long jobId;
  private long candidateId;
  private String taskName;
  private int completeWithin;
  private java.sql.Date dueDate;
  private boolean allFlag;
  private boolean completeFlag;
  private long creatorId;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getEmployeeId() {
    return employeeId;
  }

  public void setEmployeeId(long employeeId) {
    this.employeeId = employeeId;
  }

  public long getJobId() {
    return jobId;
  }

  public void setJobId(long jobId) {
    this.jobId = jobId;
  }

  public long getCandidateId() {
    return candidateId;
  }

  public void setCandidateId(long candidateId) {
    this.candidateId = candidateId;
  }

  public String getTaskName() {
    return taskName;
  }

  public void setTaskName(String taskName) {
    this.taskName = taskName;
  }

  public int getCompleteWithin() {
    return completeWithin;
  }

  public void setCompleteWithin(int completeWithin) {
    this.completeWithin = completeWithin;
  }

  public static Map<String, FieldOptions> getValidatorMap() {
    return jobTaskFieldOptionsMap;
  }

  public Date getDueDate() {
    return dueDate;
  }

  public void setDueDate(Date dueDate) {
    this.dueDate = dueDate;
  }

  public boolean isAllFlag() {
    return allFlag;
  }

  public void setAllFlag(boolean allFlag) {
    this.allFlag = allFlag;
  }

  public boolean isCompleteFlag() {
    return completeFlag;
  }

  public void setCompleteFlag(boolean completeFlag) {
    this.completeFlag = completeFlag;
  }

  public long getCreatorId() {
    return creatorId;
  }

  public void setCreatorId(long creatorId) {
    this.creatorId = creatorId;
  }

  private static Map<String, FieldOptions> jobTaskFieldOptionsMap = new HashMap<String, FieldOptions>()
  {{
    put("id", FieldOptionsCommon.NUMBER);
    put("employee_id", FieldOptionsCommon.NUMBER);
    put("job_id", FieldOptionsCommon.NUMBER);
    put("candidate_id", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 11));
    put("task_name", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
    put("complete_within", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY, -1, 11));
    put("all_flag", FieldOptionsCommon.ANY);
    put("due_date", FieldOptionsCommon.ANY_ALLOW_EMPTY);
    put("creator_id", FieldOptionsCommon.NUMBER);
  }};
}
