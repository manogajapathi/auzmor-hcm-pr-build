package com.auzmor.impl.model.Job;

import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Collaborator {

  public int collaborator_id;
  public String collaborator;

  public int job_id;

  public int getCollaborator_id() {
    return collaborator_id;
  }

  public void setCollaborator_id(int collaborator_id) {
    this.collaborator_id = collaborator_id;
  }

  public String getCollaborator() {
    return collaborator;
  }

  public void setCollaborator(String collaborator) {
    this.collaborator = collaborator;
  }

  public int getJob_id() {
    return job_id;
  }

  public void setJob_id(int job_id) {
    this.job_id = job_id;
  }

  /**
   * getCollaboratorArray Generate available Collaborator Items based on job ID
   *
   * @param connectionObject
   * @param id
   * @return JsonArray
   * @throws Exception
   */
  public JsonArray getCollaboratorArray(Connection connectionObject, int id) {

    try {

      PreparedStatement preparedStatement = null;
      JsonArray collaboratorArray = new JsonArray();

      String queryString = "SELECT collaborator,collab_email_update FROM Collaborators" +
          " where active_status = 1 and job_id=" + id;
      preparedStatement = connectionObject.prepareStatement(queryString);
      ResultSet resultSetObject = preparedStatement.executeQuery();
      while (resultSetObject.next()) {
        JsonObject collaboratorObject = new JsonObject();
        collaboratorObject.put("collaborator", resultSetObject.getString(1));
        collaboratorObject.put("collab_email_update", resultSetObject.getBoolean(2));
        collaboratorArray.add(collaboratorObject);
      }

      return collaboratorArray;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return new JsonArray();
  }

  public boolean isJobCollaborator(DbHandle dbHandle, long employeeId, long jobId)
      throws SQLException, DbHandleException {
      PreparedStatement preparedStatement = null;
      JsonArray collaboratorArray = new JsonArray();

      String queryString = "SELECT 1 FROM Collaborators" +
          " where active_status = 1 and job_id = ? and collaborator = ?";
      preparedStatement = dbHandle.createPreparedStatement(queryString);
      preparedStatement.setLong(1, jobId);
      preparedStatement.setString(2, "" + employeeId);

      return DbHelper.getBoolean(dbHandle, preparedStatement);

  }

  public boolean isNotJobCollaborator(DbHandle dbHandle, long employeeId, long jobId)
      throws SQLException, DbHandleException {
    return !isJobCollaborator(dbHandle, employeeId, jobId);
  }

}
