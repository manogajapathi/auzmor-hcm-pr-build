package com.auzmor.impl.model.Job;

public class HiringProcess {

    private int hiringProcessId;
    private String applicant;
    private String screening;
    private String interview;
    private String offer;
    private String hire;

    private int jobId;
    public int userId;
    public String hiringStatus;
    public String hiringStatusComments;
    public String comments;
    public int rating;

    public String getInterview() {

        return interview;

    }
    public void setInterview(String interview) {

        this.interview = interview;

    }
    public int getHiringProcessId() {
        return hiringProcessId;
    }

    public void setHiringProcessId(int hiringProcessId) {
        this.hiringProcessId = hiringProcessId;
    }

    public String getApplicant() {
        return applicant;
    }

    public void setApplicant(String applicant) {
        this.applicant = applicant;
    }

    public String getScreening() {
        return screening;
    }

    public void setScreening(String screening) {
        this.screening = screening;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getHire() {
        return hire;
    }

    public void setHire(String hire) {
        this.hire = hire;
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getHiringStatus() {
        return hiringStatus;
    }

    public void setHiringStatus(String hiringStatus) {
        this.hiringStatus = hiringStatus;
    }

    public String getHiringStatusComments() {
        return hiringStatusComments;
    }

    public void setHiringStatusComments(String hiringStatusComments) {
        this.hiringStatusComments = hiringStatusComments;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}