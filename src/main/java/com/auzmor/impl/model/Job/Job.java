package com.auzmor.impl.model.Job;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.util.validator.FieldOptions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Job {
  private int id;
  private int job_creator_id;
  private String title;
  private String hiring_lead;
  private String internal_job_code;
  private String employment_type;
  private String location;
  private String city;
  private String zip;
  private String country;
  private String state;
  private String job_status;
  private String department;
  private String minimum_experience;
  private String currency_type;
  private String compensation;
  private String job_desc_template;
  private String job_open_date;
  private String job_description;
  private boolean resume;
  private boolean resume_required;
  private boolean date_available;
  private boolean date_available_required;
  private boolean cover_letter;
  private boolean cover_letter_required;
  private boolean highest_education;
  private boolean highest_education_required;
  private boolean reference;
  private boolean reference_required;
  private boolean desired_salary;
  private boolean desired_salary_required;
  private boolean college;
  private boolean college_required;
  private boolean referred_by;
  private boolean referred_by_required;
  private boolean linkedin_url;
  private boolean linkedin_url_required;
  private boolean blog;
  private boolean blog_required;
  private boolean company_type_check;
  private String company_type;
  private boolean post_permission;
  private String date_posted;
  private String modified_date;
  private int organization_id;
  private boolean collab_resume;
  private boolean collab_date_available;
  private boolean collab_cover_letter;
  private boolean collab_highest_education;
  private boolean collab_reference;
  private boolean collab_desired_salary;
  private boolean collab_college;
  private boolean collab_referred_by;
  private boolean collab_linkedin_url;
  private boolean collab_blog;
  private String applicant;
  private String screening;
  private String interview;
  private String offer;
  private String hire;
  private String not_hired;
  private boolean job_active_status;
  private List<CustomQuestion> customQuestionModels;
  private List<Collaborator> collaborators;

  public Job() {

  }

  public int getJob_creator_id() {
    return job_creator_id;
  }
  
  public void setJob_creator_id(int job_creator_id) {
    this.job_creator_id = job_creator_id;
  }
  
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getHiring_lead() {
    return hiring_lead;
  }

  public void setHiring_lead(String hiring_lead) {
    this.hiring_lead = hiring_lead;
  }

  public String getInternal_job_code() {
    return internal_job_code;
  }

  public String getJob_desc_template() {
    return job_desc_template;
  }

  public void setJob_desc_template(String job_desc_template) {
    this.job_desc_template = job_desc_template;
  }

  public void setInternal_job_code(String internal_job_code) {
    this.internal_job_code = internal_job_code;
  }

  public String getEmployment_type() {
    return employment_type;
  }

  public void setEmployment_type(String employment_type) {
    this.employment_type = employment_type;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getZip() {
    return zip;
  }

  public void setZip(String zip) {
    this.zip = zip;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getJob_status() {
    return job_status;
  }

  public void setJob_status(String job_status) {
    this.job_status = job_status;
  }

  public String getDepartment() {
    return department;
  }

  public void setDepartment(String department) {
    this.department = department;
  }

  public String getMinimum_experience() {
    return minimum_experience;
  }

  public void setMinimum_experience(String minimum_experience) {
    this.minimum_experience = minimum_experience;
  }

  public String getCompensation() {
    return compensation;
  }

  public void setCompensation(String compensation) {
    this.compensation = compensation;
  }

  public String getJob_description() {
    return job_description;
  }

  public void setJob_description(String job_description) {
    this.job_description = job_description;
  }

  public boolean isResume() {
    return resume;
  }

  public void setResume(boolean resume) {
    this.resume = resume;
  }

  public boolean isResume_required() {
    return resume_required;
  }

  public void setResume_required(boolean resume_required) {
    this.resume_required = resume_required;
  }

  public boolean isDate_available() {
    return date_available;
  }

  public void setDate_available(boolean date_available) {
    this.date_available = date_available;
  }

  public boolean isDate_available_required() {
    return date_available_required;
  }

  public void setDate_available_required(boolean date_available_required) {
    this.date_available_required = date_available_required;
  }

  public boolean isCover_letter() {
    return cover_letter;
  }

  public void setCover_letter(boolean cover_letter) {
    this.cover_letter = cover_letter;
  }

  public boolean isCover_letter_required() {
    return cover_letter_required;
  }

  public void setCover_letter_required(boolean cover_letter_required) {
    this.cover_letter_required = cover_letter_required;
  }

  public boolean isHighestEducation() {
    return highest_education;
  }

  public void setHighestEducation(boolean highestEducation) {
    this.highest_education = highestEducation;
  }

  public boolean isHighest_education_required() {
    return highest_education_required;
  }

  public void setHighest_education_required(boolean highest_education_required) {
    this.highest_education_required = highest_education_required;
  }

  public boolean isReference() {
    return reference;
  }

  public void setReference(boolean reference) {
    this.reference = reference;
  }

  public boolean isReference_required() {
    return reference_required;
  }

  public void setReference_required(boolean reference_required) {
    this.reference_required = reference_required;
  }

  public boolean isDesired_salary() {
    return desired_salary;
  }

  public void setDesired_salary(boolean desired_salary) {
    this.desired_salary = desired_salary;
  }

  public boolean isDesired_salary_required() {
    return desired_salary_required;
  }

  public void setDesired_salary_required(boolean desired_salary_required) {
    this.desired_salary_required = desired_salary_required;
  }

  public boolean isCollege() {
    return college;
  }

  public void setCollege(boolean college) {
    this.college = college;
  }

  public boolean isCollege_required() {
    return college_required;
  }

  public void setCollege_required(boolean college_required) {
    this.college_required = college_required;
  }

  public boolean isReferred_by() {
    return referred_by;
  }

  public void setReferred_by(boolean referred_by) {
    this.referred_by = referred_by;
  }

  public boolean isReferred_by_required() {
    return referred_by_required;
  }

  public void setReferred_by_required(boolean referred_by_required) {
    this.referred_by_required = referred_by_required;
  }

  public boolean isLinkedin_url() {
    return linkedin_url;
  }

  public void setLinkedin_url(boolean linkedin_url) {
    this.linkedin_url = linkedin_url;
  }

  public boolean isLinkedin_url_required() {
    return linkedin_url_required;
  }

  public void setLinkedin_url_required(boolean linkedin_url_required) {
    this.linkedin_url_required = linkedin_url_required;
  }

  public boolean isBlog() {
    return blog;
  }

  public void setBlog(boolean blog) {
    this.blog = blog;
  }

  public boolean isBlog_required() {
    return blog_required;
  }

  public void setBlog_required(boolean blog_required) {
    this.blog_required = blog_required;
  }

  public boolean isCompany_type_check() {
    return company_type_check;
  }

  public void setCompany_type_check(boolean company_type_check) {
    this.company_type_check = company_type_check;
  }

  public String getCompany_type() {
    return company_type;
  }

  public void setCompany_type(String company_type) {
    this.company_type = company_type;
  }

  public boolean isPost_permission() {
    return post_permission;
  }

  public void setPost_permission(boolean post_permission) {
    this.post_permission = post_permission;
  }

  public String getDate_posted() {
    return date_posted;
  }

  public void setDate_posted(String date_posted) {
    this.date_posted = date_posted;
  }

  public String getModified_date() {
    return modified_date;
  }

  public void setModified_date(String modified_date) {
    this.modified_date = modified_date;
  }

  public int getOrganization_id() {
    return organization_id;
  }

  public void setOrganization_id(int organization_id) {
    this.organization_id = organization_id;
  }

  public List<CustomQuestion> getCustomQuestionModels() {
    return customQuestionModels;
  }

  public void setCustomQuestionModels(List<CustomQuestion> customQuestionModels) {
    this.customQuestionModels = customQuestionModels;
  }

  public String getNot_hired() {
    return not_hired;
  }

  public void setNot_hired(String not_hired) {
    this.not_hired = not_hired;
  }

  public List<Collaborator> getCollaborators() {
    return collaborators;
  }

  public void setCollaborators(List<Collaborator> collaborators) {
    this.collaborators = collaborators;
  }

  public String getJob_open_date() {
    return job_open_date;
  }

  public void setJob_open_date(String job_open_date) {
    this.job_open_date = job_open_date;
  }

  public boolean isHighest_education() {
    return highest_education;
  }

  public void setHighest_education(boolean highest_education) {
    this.highest_education = highest_education;
  }

  public String getApplicant() {
    return applicant;
  }

  public void setApplicant(String applicant) {
    this.applicant = applicant;
  }

  public String getScreening() {
    return screening;
  }

  public void setScreening(String screening) {
    this.screening = screening;
  }

  public String getInterview() {
    return interview;
  }

  public void setInterview(String interview) {
    this.interview = interview;
  }

  public String getOffer() {
    return offer;
  }

  public void setOffer(String offer) {
    this.offer = offer;
  }

  public String getHire() {
    return hire;
  }

  public void setHire(String hire) {
    this.hire = hire;
  }

  public boolean isCollab_resume() {
    return collab_resume;
  }

  public void setCollab_resume(boolean collab_resume) {
    this.collab_resume = collab_resume;
  }

  public boolean isCollab_date_available() {
    return collab_date_available;
  }

  public void setCollab_date_available(boolean collab_date_available) {
    this.collab_date_available = collab_date_available;
  }

  public boolean isCollab_cover_letter() {
    return collab_cover_letter;
  }

  public void setCollab_cover_letter(boolean collab_cover_letter) {
    this.collab_cover_letter = collab_cover_letter;
  }

  public boolean isCollab_highest_education() {
    return collab_highest_education;
  }

  public void setCollab_highest_education(boolean collab_highest_education) {
    this.collab_highest_education = collab_highest_education;
  }

  public boolean isCollab_reference() {
    return collab_reference;
  }

  public void setCollab_reference(boolean collab_reference) {
    this.collab_reference = collab_reference;
  }

  public boolean isCollab_desired_salary() {
    return collab_desired_salary;
  }

  public void setCollab_desired_salary(boolean collab_desired_salary) {
    this.collab_desired_salary = collab_desired_salary;
  }

  public boolean isCollab_college() {
    return collab_college;
  }

  public void setCollab_college(boolean collab_college) {
    this.collab_college = collab_college;
  }

  public boolean isCollab_referred_by() {
    return collab_referred_by;
  }

  public void setCollab_referred_by(boolean collab_referred_by) {
    this.collab_referred_by = collab_referred_by;
  }

  public boolean isCollab_linkedin_url() {
    return collab_linkedin_url;
  }

  public void setCollab_linkedin_url(boolean collab_linkedin_url) {
    this.collab_linkedin_url = collab_linkedin_url;
  }

  public boolean isCollab_blog() {
    return collab_blog;
  }

  public void setCollab_blog(boolean collab_blog) {
    this.collab_blog = collab_blog;
  }

  public boolean isJob_active_status() {
    return job_active_status;
  }

  public void setJob_active_status(boolean job_active_status) {
    this.job_active_status = job_active_status;
  }

  public String getCurrency_type() {
    return currency_type;
  }

  public void setCurrency_type(String currency_type) {
    this.currency_type = currency_type;
  }
}