package com.auzmor.impl.model.Job;

import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.*;

public class CustomQuestion {

  private long id;
  private String customQuestion;
  private String customOption1;
  private String customOption2;
  private boolean customKnockout;
  private String customAnswer;
  private boolean customQuestionRequired;
  private long jobId;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getCustomQuestion() {
    return customQuestion;
  }

  public void setCustomQuestion(String customQuestion) {
    this.customQuestion = customQuestion;
  }

  public String getCustomOption1() {
    return customOption1;
  }

  public void setCustomOption1(String customOption1) {
    this.customOption1 = customOption1;
  }

  public String getCustomOption2() {
    return customOption2;
  }

  public void setCustomOption2(String customOption2) {
    this.customOption2 = customOption2;
  }

  public boolean isCustomKnockout() {
    return customKnockout;
  }

  public void setCustomKnockout(boolean customKnockout) {
    this.customKnockout = customKnockout;
  }

  public String getCustomAnswer() {
    return customAnswer;
  }

  public void setCustomAnswer(String customAnswer) {
    this.customAnswer = customAnswer;
  }

  public long getJobId() {
    return jobId;
  }

  public void setJobId(long jobId) {
    this.jobId = jobId;
  }

  public void setCustomQuestionRequired(boolean customQuestionRequired) {
    this.customQuestionRequired = customQuestionRequired;
  }

  public boolean getCustomQuestionRequired() {
    return customQuestionRequired;
  }

  /**
   * getCustomQuestionArray Generate available Custom Question Items based on job ID
   *
   * @param connectionObject
   * @param id
   * @return JsonArray
   * @throws Exception
   * @author Pradeep Balasubramanian
   * Modified at ${date}
   */
  public JsonArray getCustomQuestionArray(Connection connectionObject, int id)
      throws DecodeException,SQLException {


    PreparedStatement preparedStatement = null;
    JsonArray customQuestionArray = new JsonArray();

    String queryString = "SELECT custom_question_id,custom_question,custom_option1," +
        "custom_option2,custom_knockout,custom_answer,custom_question_required " +
        "FROM CustomQuestionJob where job_id=" + id;
    preparedStatement = connectionObject.prepareStatement(queryString);
    ResultSet resultSetObject = preparedStatement.executeQuery();
    ResultSetMetaData resultSetMetaData = resultSetObject.getMetaData();

    String columnName = "", columnType = "";

    while (resultSetObject.next()) {
      JsonObject customQuestionObject = new JsonObject();
      for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {

        columnName = resultSetMetaData.getColumnName(i);
        columnType = resultSetMetaData.getColumnTypeName(i);

        if ("BIT".equalsIgnoreCase(columnType)) {
          customQuestionObject.put(columnName, resultSetObject.getBoolean(columnName));
        } else if ("INT".equalsIgnoreCase(columnType)) {
          customQuestionObject.put(columnName, resultSetObject.getInt(columnName));
        } else if ("VARCHAR".equalsIgnoreCase(columnType) || "BLOB".equalsIgnoreCase(columnType)) {
          customQuestionObject.put(columnName, resultSetObject.getString(columnName));
        }
      }
      customQuestionArray.add(customQuestionObject);
    }

    return customQuestionArray;
  }
}
