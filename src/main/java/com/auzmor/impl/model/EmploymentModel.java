package com.auzmor.impl.model;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.util.StringUtil;

import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class EmploymentModel {

  PreparedStatement ptmt = null;
  ResultSet resultSet = null;
  StringUtil stringUtil = new StringUtil();
  public int id;
  public String employmentType;
  public int organizationId;
  public String activeStatus;

  public EmploymentModel() {

  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getEmploymentType() {
    return employmentType;
  }

  public void setEmploymentType(String employmentType) {
    this.employmentType = employmentType;
  }

  public int getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(int organizationId) {
    this.organizationId = organizationId;
  }

  public String getActiveStatus() {
    return activeStatus;
  }

  public void setActiveStatus(String activeStatus) {
    this.activeStatus = activeStatus;
  }

  public static Map<String, FieldOptions> getValidatorMap() {
    return employmentFieldOptionsMap;
  }

  private static Map<String, FieldOptions> employmentFieldOptionsMap = new HashMap<String, FieldOptions>()
  {{
    put("organization_id", FieldOptionsCommon.NUMBER);
    put("id", FieldOptionsCommon.NUMBER);
    put("employmentType", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 100));
    put("active_status", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 10));
  }};
}
