package com.auzmor.impl.model;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.util.StringUtil;

import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class DepartmentModel {

  private int id;
  private String department;
  private String activeStatus;
  private int organizationId;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getDepartment() {
    return department;
  }

  public void setDepartment(String department) {
    this.department = department;
  }

  public int getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(int organizationId) {
    this.organizationId = organizationId;
  }

  public String getActiveStatus() {
    return activeStatus;
  }

  public void setActiveStatus(String active_status) {
    this.activeStatus = active_status;
  }

  public static Map<String, FieldOptions> getValidatorMap() {
    return departmentFieldOptionsMap;
  }

  private static Map<String, FieldOptions> departmentFieldOptionsMap = new HashMap<String, FieldOptions>()
  {{
    put("organization_id", FieldOptionsCommon.NUMBER);
    put("id", FieldOptionsCommon.NUMBER);
    put("department", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
    put("active_status", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 10));
  }};
}
