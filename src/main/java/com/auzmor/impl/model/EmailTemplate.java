package com.auzmor.impl.model;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

public class EmailTemplate {
  private long id;
  private String name;
  private String subject;
  private String header;
  private String footer;
  private String template;
  private long organizationId;
  private Date created;
  private Date modified;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getHeader() {
    return header;
  }

  public void setHeader(String header) {
    this.header = header;
  }

  public String getFooter() {
    return footer;
  }

  public void setFooter(String footer) {
    this.footer = footer;
  }

  public String getTemplate() {
    return template;
  }

  public void setTemplate(String template) {
    this.template = template;
  }

  public long getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(long organizationId) {
    this.organizationId = organizationId;
  }

  public Date getCreated() {
    return created;
  }

  public Date getModified() {
    return modified;
  }

  public static Map<String, FieldOptions> getValidatorMap() {
    return emailTemplateFieldOptionsMap;
  }
  private static Map<String, FieldOptions> emailTemplateFieldOptionsMap =
      new HashMap<String, FieldOptions>()
  {{
    put("templateId", FieldOptionsCommon.NUMBER);
    put("name", new FieldOptionsImpl(FieldOptionsCommon.ANY_HTML_ESCAPE, -1, 1000));
    put("subject", new FieldOptionsImpl(FieldOptionsCommon.ANY_HTML_ESCAPE, -1, 200));
    put("header", new FieldOptionsImpl(new FieldOptionsImpl("^.+$", true).setHtmlEscape(true),
        -1, 200));
    put("footer", new FieldOptionsImpl(new FieldOptionsImpl("^.+$", true).setHtmlEscape(true),
        -1, 200));
    put("template", FieldOptionsCommon.ANY_HTML_ESCAPE);
    put("organization-id", FieldOptionsCommon.NUMBER);
    put("organizationId", FieldOptionsCommon.NUMBER);
    put("ids", new FieldOptionsImpl("^\\d+(,\\d+)*$"));
  }};
}
