package com.auzmor.impl.model;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.Field;
import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Model for New Organization
 * This model will be called from OrganizationHandler for setters and getters
 */

public class Organization {

  private static final Logger logger = LoggerFactory.getLogger(Organization.class);

  public int organization_id;
  public String domain_name;
  public String career_url;
  public String hris_url;
  public String logo;
  public String organization_name;
  public String file_size;
  public String first_name;
  public String last_name;
  public String account_owner_email;
  public String ein;
  public String phone_no;
  public String company_address_line1;
  public String company_address_line2;
  public String city;
  public String state;
  public String country;
  public int zip_code;
  public String billing_credit_card;
  public String billing_expiry_date;
  public int billing_cvv;
  public String billing_contact_person1;
  public String billing_contact_person2;
  public String billing_contact_person3;
  public String billing_address_line1;
  public String billing_address_line2;
  public String billing_city;
  public String billing_state;
  public String billing_country;
  public int billing_zip_code;
  public String favicon;
  public String date_format;
  public String number_format;
  public String name_format;
  public String default_currency;
  public String default_country;
  public String employee_profile_image;
  public String timezone_format;
  public String website_url;
  public String created_date;
  public String modified_date;
  public int plan_version_id;
  public String admin_email;
  public String account_owner_name;

  public Date created;
  public Date modified;

  public String region_code;

  public String getRegion_code() {
    return region_code;
  }

  public void setRegion_code(String region_code) {
    this.region_code = region_code;
  }

  public void setOrganization_id(int organization_id) {
    this.organization_id = organization_id;
  }

  public void setDomain_name(String domain_name) {
    this.domain_name = domain_name;
  }

  public void setCareer_url(String career_url) {
    this.career_url = career_url;
  }

  public void setHris_url(String hris_url) {
    this.hris_url = hris_url;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  public void setOrganization_name(String organization_name) {
    this.organization_name = organization_name;
  }

  public String getFirst_name() {
    return first_name;
  }

  public void setFirst_name(String first_name) {
    this.first_name = first_name;
  }

  public String getLast_name() {
    return last_name;
  }

  public void setLast_name(String last_name) {
    this.last_name = last_name;
  }

  public void setFile_size(String file_size) {
    this.file_size = file_size;
  }

  public void setAccount_owner_email(String account_owner_email) {
    this.account_owner_email = account_owner_email;
  }

  public void setEin(String ein) {
    this.ein = ein;
  }

  public void setPhone_no(String phone_no) {
    this.phone_no = phone_no;
  }

  public void setCompany_address_line1(String company_address_line1) {
    this.company_address_line1 = company_address_line1;
  }

  public void setCompany_address_line2(String company_address_line2) {
    this.company_address_line2 = company_address_line2;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public void setState(String state) {
    this.state = state;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public void setZip_code(int zip_code) {
    this.zip_code = zip_code;
  }

  public void setBilling_credit_card(String billing_credit_card) {
    this.billing_credit_card = billing_credit_card;
  }

  public void setBilling_expiry_date(String billing_expiry_date) {
    this.billing_expiry_date = billing_expiry_date;
  }

  public void setBilling_cvv(int billing_cvv) {
    this.billing_cvv = billing_cvv;
  }

  public void setBilling_contact_person1(String billing_contact_person1) {
    this.billing_contact_person1 = billing_contact_person1;
  }

  public void setBilling_contact_person2(String billing_contact_person2) {
    this.billing_contact_person2 = billing_contact_person2;
  }

  public void setBilling_contact_person3(String billing_contact_person3) {
    this.billing_contact_person3 = billing_contact_person3;
  }

  public String getAdmin_email() {
    return admin_email;
  }

  public void setAdmin_email(String admin_email) {
    this.admin_email = admin_email;
  }

  public void setBilling_address_line1(String billing_address_line1) {
    this.billing_address_line1 = billing_address_line1;
  }

  public void setBilling_address_line2(String billing_address_line2) {
    this.billing_address_line2 = billing_address_line2;
  }

  public void setBilling_city(String billing_city) {
    this.billing_city = billing_city;
  }

  public void setBilling_state(String billing_state) {
    this.billing_state = billing_state;
  }

  public void setBilling_country(String billing_country) {
    this.billing_country = billing_country;
  }

  public void setBilling_zip_code(int billing_zip_code) {
    this.billing_zip_code = billing_zip_code;
  }

  public void setFavicon(String favicon) {
    this.favicon = favicon;
  }

  public void setDate_format(String date_format) {
    this.date_format = date_format;
  }

  public void setNumber_format(String number_format) {
    this.number_format = number_format;
  }

  public void setName_format(String name_format) {
    this.name_format = name_format;
  }

  public void setDefault_currency(String default_currency) {
    this.default_currency = default_currency;
  }

  public void setDefault_country(String default_country) {
    this.default_country = default_country;
  }

  public void setEmployee_profile_image(String employee_profile_image) {
    this.employee_profile_image = employee_profile_image;
  }

  public void setTimezone_format(String timezone_format) {
    this.timezone_format = timezone_format;
  }

  public void setWebsite_url(String website_url) {
    this.website_url = website_url;
  }

  public void setCreated_date(String created_date) {
    this.created_date = created_date;
  }

  public void setModified_date(String modified_date) {
    this.modified_date = modified_date;
  }

  public int getOrganization_id() {
    return organization_id;
  }

  public String getDomain_name() {
    return domain_name;
  }

  public String getCareer_url() {
    return career_url;
  }

  public String getHris_url() {
    return hris_url;
  }

  public String getLogo() {
    return logo;
  }

  public String getOrganization_name() {
    return organization_name;
  }

  public String getFile_size() {
    return file_size;
  }

  public String getAccount_owner_email() {
    return account_owner_email;
  }

  public String getEin() {
    return ein;
  }

  public String getPhone_no() {
    return phone_no;
  }

  public String getCompany_address_line1() {
    return company_address_line1;
  }

  public String getCompany_address_line2() {
    return company_address_line2;
  }

  public String getCity() {
    return city;
  }

  public String getState() {
    return state;
  }

  public String getCountry() {
    return country;
  }

  public int getZip_code() {
    return zip_code;
  }

  public String getBilling_credit_card() {
    return billing_credit_card;
  }

  public String getBilling_expiry_date() {
    return billing_expiry_date;
  }

  public int getBilling_cvv() {
    return billing_cvv;
  }

  public String getBilling_contact_person1() {
    return billing_contact_person1;
  }

  public String getBilling_contact_person2() {
    return billing_contact_person2;
  }

  public String getBilling_contact_person3() {
    return billing_contact_person3;
  }

  public String getBilling_address_line1() {
    return billing_address_line1;
  }

  public String getBilling_address_line2() {
    return billing_address_line2;
  }

  public String getBilling_city() {
    return billing_city;
  }

  public String getBilling_state() {
    return billing_state;
  }

  public String getBilling_country() {
    return billing_country;
  }

  public int getBilling_zip_code() {
    return billing_zip_code;
  }

  public String getFavicon() {
    return favicon;
  }

  public String getDate_format() {
    return date_format;
  }

  public String getNumber_format() {
    return number_format;
  }

  public String getName_format() {
    return name_format;
  }

  public String getDefault_currency() {
    return default_currency;
  }

  public String getDefault_country() {
    return default_country;
  }

  public String getEmployee_profile_image() {
    return employee_profile_image;
  }

  public String getTimezone_format() {
    return timezone_format;
  }

  public String getWebsite_url() {
    return website_url;
  }

  public String getCreated_date() {
    return created_date;
  }

  public String getModified_date() {
    return modified_date;
  }

  public int getPlan_version_id() {
    return plan_version_id;
  }

  public void setPlan_version_id(int plan_version_id) {
    this.plan_version_id = plan_version_id;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public Date getModified() {
    return modified;
  }

  public void setModified(Date modified) {
    this.modified = modified;
  }

  public String getAccount_owner_name() {
    return account_owner_name;
  }

  public void setAccount_owner_name(String account_owner_name) {
    this.account_owner_name = account_owner_name;
  }

  public Organization set(String fieldName, Object value) throws NoSuchFieldException {

    Field field = this.getClass().getField(fieldName);
    if (field.getType().toString().equals("boolean")) {
      try {
        field.setBoolean(this, Boolean.parseBoolean((String) value));
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      }
    } else if (field.getType().toString().equals("int")) {
      try {
        field.setInt(this, Integer.valueOf(String.valueOf(value)));
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      }
    } else {
      try {
        field.set(this, value);
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      }
    }
    return this;
  }

  public static Map<String, FieldOptions> getValidatorMap() {
    return ORGANIZATION_FIELD_OPTIONS;
  }

  private static Map<String, FieldOptions> ORGANIZATION_FIELD_OPTIONS =
      new HashMap<String, FieldOptions>()
      {{
        put("id", FieldOptionsCommon.NUMBER);
        put("organizationId", FieldOptionsCommon.NUMBER);
        put("domain", new FieldOptionsImpl(FieldOptionsCommon.DOMAIN, -1, 255));
        put("auzmorDomain", new FieldOptionsImpl(FieldOptionsCommon.DOMAIN, -1, 255));
        put("careerDomain", new FieldOptionsImpl(FieldOptionsCommon.DOMAIN, -1, 255));
        put("auzmorDomainCrt", FieldOptionsCommon.ANY);
        put("careerDomainCrt", FieldOptionsCommon.ANY);
        put("auzmorDomainCrtKey", FieldOptionsCommon.ANY);
        put("careerDomainCrtKey", FieldOptionsCommon.ANY);
      }};
}
