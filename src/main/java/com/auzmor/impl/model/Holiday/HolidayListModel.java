package com.auzmor.impl.model.Holiday;

public class HolidayListModel {
    private int list_id;
    private String list_name;
    private String year;
    private String assign_to;
    private String location;
    private String division;
    private String department;
    private String pay_group;
    private String employees;
    private int organization_id;
    private boolean paid_flag;
    private boolean completed_status;

    public int getList_id() {
        return list_id;
    }

    public void setList_id(int list_id) {
        this.list_id = list_id;
    }

    public String getList_name() {
        return list_name;
    }

    public void setList_name(String list_name) {
        this.list_name = list_name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getAssign_to() {
        return assign_to;
    }

    public void setAssign_to(String assign_to) {
        this.assign_to = assign_to;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPay_group() {
        return pay_group;
    }

    public void setPay_group(String pay_group) {
        this.pay_group = pay_group;
    }

    public String getEmployees() {
        return employees;
    }

    public void setEmployees(String employees) {
        this.employees = employees;
    }

    public int getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(int organization_id) {
        this.organization_id = organization_id;
    }

    public boolean isPaid_flag() {
        return paid_flag;
    }

    public void setPaid_flag(boolean paid_flag) {
        this.paid_flag = paid_flag;
    }

    public boolean isCompleted_status() {
        return completed_status;
    }

    public void setCompleted_status(boolean completed_status) {
        this.completed_status = completed_status;
    }
}
