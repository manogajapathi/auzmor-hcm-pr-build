package com.auzmor.impl.model;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.util.HashMap;
import java.util.Map;

public class PayGroupModel {
  private int paygroupId;
  private String paygroupName;
  private int organizationId;
  private String activeStatus;

  public int getPaygroupId() {
    return paygroupId;
  }

  public void setPaygroupId(int paygroupId) {
    this.paygroupId = paygroupId;
  }

  public String getPaygroupName() {
    return paygroupName;
  }

  public void setPaygroupName(String paygroupName) {
    this.paygroupName = paygroupName;
  }

  public int getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(int organizationId) {
    this.organizationId = organizationId;
  }

  public String getActiveStatus() {
    return activeStatus;
  }

  public void setActiveStatus(String activeStatus) {
    this.activeStatus = activeStatus;
  }

  public static Map<String, FieldOptions> getValidatorMap() {
    return payGroupFieldOptionsMap;
  }

  private static Map<String, FieldOptions> payGroupFieldOptionsMap = new HashMap<String, FieldOptions>() {{
    put("organization_id", FieldOptionsCommon.NUMBER);
    put("organizationId", FieldOptionsCommon.NUMBER);
    put("paygroupId", FieldOptionsCommon.NUMBER);
    put("paygroup_name", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
    put("active_status", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 10));
  }};
}
