package com.auzmor.impl.model;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.util.HashMap;
import java.util.Map;

public class Login {

  private long id;
  private String consumerId;
  private String firstName;
  private String lastName;
  private String userType;
  private String email;
  private String password;
  private long organizationId;
  private String created;
  private String modified;
  private String token;
  private String confirmPassword;
  private String keyValue;
  private String hashValue;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getConsumerId() {
    return consumerId;
  }

  public void setConsumerId(String consumerId) {
    this.consumerId = consumerId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getUserType() {
    return userType;
  }

  public void setUserType(String userType) {
    this.userType = userType;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public long getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(long organizationId) {
    this.organizationId = organizationId;
  }

  public String getCreated() {
    return created;
  }

  public void setCreated(String created) {
    this.created = created;
  }

  public String getModified() {
    return modified;
  }

  public void setModified(String modified) {
    this.modified = modified;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getConfirmPassword() {
    return confirmPassword;
  }

  public void setConfirmPassword(String confirmPassword) {
    this.confirmPassword = confirmPassword;
  }

  public String getKeyValue() {
    return keyValue;
  }

  public void setKeyValue(String keyValue) {
    this.keyValue = keyValue;
  }

  public String getHashValue() {
    return hashValue;
  }

  public void setHashValue(String hashValue) {
    this.hashValue = hashValue;
  }

  public static Map<String, FieldOptions> getValidatorMap() {
    return loginFieldOptionsMap;
  }

  private static Map<String, FieldOptions> loginFieldOptionsMap = new HashMap<String, FieldOptions>() {{
    put("first_name", FieldOptionsCommon.ANY);
    put("last_name", FieldOptionsCommon.ANY);
    put("user_id", FieldOptionsCommon.NUMBER);
    put("organization_id", FieldOptionsCommon.NUMBER);
    put("organization-id", FieldOptionsCommon.NUMBER);
    put("access_level", FieldOptionsCommon.ANY);
    put("approve_status",FieldOptionsCommon.ANY);
    put("approve_onboardingstatus",FieldOptionsCommon.ANY);
    put("onboarding_packet",FieldOptionsCommon.ANY);
    put("email", new FieldOptionsImpl(FieldOptionsCommon.EMAIL, -1, 100));
    put("password", new FieldOptionsImpl(FieldOptionsCommon.ANY, 8, 20));
    put("user_type", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 100));
  }};
}

