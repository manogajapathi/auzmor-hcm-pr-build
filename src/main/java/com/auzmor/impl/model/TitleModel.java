package com.auzmor.impl.model;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.impl.builder.QueryStringBuilder;
import com.auzmor.impl.util.StringUtil;

import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;
import io.vertx.ext.web.RoutingContext;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TitleModel {

  PreparedStatement ptmt = null;
  ResultSet resultSet = null;
  StringUtil stringUtil = new StringUtil();
  public int id;
  public String title;
  public int organizationId;
  public String activeStatus;

  public TitleModel() {

  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public int getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(int organizationId) {
    this.organizationId = organizationId;
  }

  public String getActiveStatus() {
    return activeStatus;
  }

  public void setActiveStatus(String activeStatus) {
    this.activeStatus = activeStatus;
  }

  public static Map<String, FieldOptions> getValidatorMap() {
    return titleFieldOptionsMap;
  }

  private static Map<String, FieldOptions> titleFieldOptionsMap = new HashMap<String, FieldOptions>()
  {{
    put("organization_id", FieldOptionsCommon.NUMBER);
    put("id", FieldOptionsCommon.NUMBER);
    put("title", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 150));
    put("active_status", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 10));
  }};
}
