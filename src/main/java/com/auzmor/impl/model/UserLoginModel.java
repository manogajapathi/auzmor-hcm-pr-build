package com.auzmor.impl.model;

import com.auzmor.impl.util.AESUtil;
import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.util.StringUtil;

import io.vertx.ext.web.RoutingContext;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserLoginModel {

    Connection connection = null;
    PreparedStatement ptmt = null; PreparedStatement ptmt1 = null;
    ResultSet resultSet = null;
    StringUtil stringUtil = new StringUtil();
    public int id;
    public String username;
    public String email;
    public String password;
    public String confirm_password;
    public int organization_id;
    public boolean isArchived;

    public UserLoginModel() {

    }
    private Connection getConnection() throws SQLException {
        Connection conn = null;
        MysqldbConf mysqldbConf = new MysqldbConf();
        try {
            conn = mysqldbConf.getConnection();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUserName(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirm_password() {
        return confirm_password;
    }

    public void setConfirm_password(String confirm_password) {
        this.confirm_password = confirm_password;
    }

    public boolean getisArchived() {
        return isArchived;
    }

    public void setisArchived(boolean archived) {
        isArchived = archived;
    }

    public int getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(int organization_id) {
        this.organization_id = organization_id;
    }

    public void set(String fieldName, Object value) throws NoSuchFieldException {
        //Map values to fields in class
        Field field = this.getClass().getField(fieldName);
        if (field.getType().toString().equals("boolean")) {
            try {
                field.setBoolean(this, Boolean.parseBoolean((String) value));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else if (field.getType().toString().equals("int")){
            try {
                field.setInt(this, Integer.valueOf(String.valueOf(value)));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else {
            try {
                field.set(this, value);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public UserLoginModel add(RoutingContext routingContext, UserLoginModel userLoginModel) throws NoSuchFieldException, SQLException {
        String[] strings = stringUtil.joinRegisterList(routingContext, userLoginModel);
        Field[] fields = this.getClass().getFields();
        System.out.println(fields.toString());
        String keyString = strings[0];
        String valuesString = strings[1];
        String queryString = "insert into Login(" + keyString +") values(" + valuesString + ")";
        System.out.println(queryString);
        connection = getConnection();
        ptmt = connection.prepareStatement(queryString, Statement.RETURN_GENERATED_KEYS);
        ptmt.executeUpdate();
        resultSet = ptmt.getGeneratedKeys();
        //To send Job id as response
        while (resultSet.next()){
            id = resultSet.getInt(1);
            setId(id);
        }
        System.out.println("Posted Successfully");

        return userLoginModel;
    }

    public UserLoginModel Select(int id) throws SQLException {
        UserLoginModel userLoginModel = new UserLoginModel();
        String queryString = "SELECT id,username, password FROM Login WHERE id="+id;
        connection = getConnection();
        ptmt = connection.prepareStatement(queryString);
        resultSet = ptmt.executeQuery();
        while (resultSet.next()) {
            this.setId(resultSet.getInt("id"));
            this.setUserName(resultSet.getString("username"));
            this.setPassword(resultSet.getString("password"));
        }
        return userLoginModel;
    }

    public UserLoginModel update(int id, UserLoginModel userLoginModel, RoutingContext routingContext) throws SQLException, NoSuchFieldException {

        StringBuilder statement = stringUtil.updateLoginList(routingContext, userLoginModel);
        //System.out.println(statement);
        String queryString = "update Login set "+statement+" WHERE id = "+id;
        System.out.println(queryString);
        connection = getConnection();
        ptmt = connection.prepareStatement(queryString);
        ptmt.executeUpdate();
        System.out.println("Table Updated Successfully");

        return userLoginModel;
    }

    public UserLoginModel delete(int id) throws SQLException,IllegalAccessException  {
        UserLoginModel userLoginModel = new UserLoginModel();
        //update Login table row to isArchived
        userLoginModel.setisArchived(true);
        String queryString = "update Login set isArchived=TRUE WHERE id="+id;
        connection = getConnection();
        ptmt = connection.prepareStatement(queryString);
        ptmt.executeUpdate();

        //insert the archived row to new table
        String queryString1 = "INSERT INTO LoginArchive select * from Login WHERE id="+id;
        connection = getConnection();
        ptmt = connection.prepareStatement(queryString1);
        ptmt.executeUpdate();

        //delete the archived row from Login table
        /*String queryString2 = "delete from Login WHERE id="+id;
        connection = getConnection();
        ptmt = connection.prepareStatement(queryString2);
        ptmt.executeUpdate();*/
        return userLoginModel;
    }

    /**
     * Returns UserLoginModel object which has the details of the particular user
     * which are obtained by using the params listed. If the particular user details are not
     * found, SQLException is thrown
     * @param loginModel
     * @param routingContext
     * @return
     * @throws Exception
     * @throws SQLException
     * @author Pradeep Sudhakaran
     * @lastModifiedDate 23-10-2017
     */
    public UserLoginModel updatePassword(UserLoginModel loginModel,RoutingContext routingContext) throws Exception,SQLException{

        connection = getConnection();
        int upperCount = 0, lowerCount = 0, numberCount = 0,specialCount=0;
        Pattern special;
        Matcher specialMatcher;
        boolean matches = false;
        String encryptedPassword = getEncryptedForm(connection, AESUtil.getAESEncyptString(loginModel.getPassword()));
        //String encryptedPassword = loginModel.getPassword();
        String statement = "password = '"+encryptedPassword+"',confirm_password = '"+encryptedPassword+"'";
        int user_id = Integer.valueOf(routingContext.request().getParam("id"));
        String passwordEntered = loginModel.getPassword();
        String confirmPassword = loginModel.getConfirm_password();

        if(passwordEntered.equals(null) || passwordEntered.equals("")
                || user_id == 0
                || confirmPassword.equals(null) || confirmPassword.equals(""))
            throw new Exception("Required fields are missing");
        else if(passwordEntered.length() < 8)
            throw new Exception("Password should have atleast 8 characters");
        else if(!passwordEntered.equals(confirmPassword))
            throw new Exception("Passwords should be same in both boxes");

        String queryString = "update Login set "+statement+" WHERE id = '"+user_id+"'";

        ptmt = connection.prepareStatement(queryString);
        int updateCount =0;
        updateCount = ptmt.executeUpdate();

        if(updateCount == 0)
            throw new SQLException("Updation failure");

        return loginModel;
    }

    public static String getEncryptedForm(Connection connection,String string) throws Exception {
        Statement preparedStatement = connection.createStatement();
        String query = "select SHA1('"+string+"') as encryptString from dual";
        ResultSet resultSet = preparedStatement.executeQuery(query);
        String encryptedString ="";
        while(resultSet.next()){
            encryptedString = resultSet.getString("encryptString");
        }

        return encryptedString;
    }
}
