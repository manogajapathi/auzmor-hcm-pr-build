package com.auzmor.impl.model.onboarding;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.util.HashMap;
import java.util.Map;

public class OnboardingForm {

    private int form_id;
    private boolean form_required;
    private int packet_id;
    private String form_name;

    public String getForm_name() {
        return form_name;
    }

    public void setForm_name(String form_name) {
        this.form_name = form_name;
    }

    public int getPacket_id() {
        return packet_id;
    }

    public void setPacket_id(int packet_id) {
        this.packet_id = packet_id;
    }

    public boolean isForm_required() {
        return form_required;
    }

    public void setForm_required(boolean form_required) {
        this.form_required = form_required;
    }

    public int getForm_id() {
        return form_id;
    }

    public void setForm_id(int form_id) {
        this.form_id = form_id;
    }

    public static Map<String, FieldOptions> getValidatorMap() {
        return formFieldOptionsMap;
    }

    private static Map<String, FieldOptions> formFieldOptionsMap = new HashMap<String, FieldOptions>() {{
        put("organization_id", FieldOptionsCommon.NUMBER);
        put("packet_id", FieldOptionsCommon.NUMBER);
        put("form_name", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
        put("form_required", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 6));
        put("formId", FieldOptionsCommon.NUMBER);
    }};
}
