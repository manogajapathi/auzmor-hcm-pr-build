package com.auzmor.impl.model.onboarding;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.util.HashMap;
import java.util.Map;

public class OnboardingDocument {

    private int document_id;
    private String document_name;
    private String document_path;
    private boolean viewing_reqd;
    private int packet_id;
    private boolean signingRequired;
    private int page;
    private double xCoordinate;
    private double yCoordinate;

    public int getDocument_id() {
        return document_id;
    }

    public void setDocument_id(int document_id) {
        this.document_id = document_id;
    }

    public String getDocument_name() {
        return document_name;
    }

    public void setDocument_name(String document_name) {
        this.document_name = document_name;
    }

    public String getDocument_path() {
        return document_path;
    }

    public void setDocument_path(String document_path) {
        this.document_path = document_path;
    }

    public boolean isViewing_reqd() {
        return viewing_reqd;
    }

    public void setViewing_reqd(boolean viewing_reqd) {
        this.viewing_reqd = viewing_reqd;
    }

    public int getPacket_id() {
        return packet_id;
    }

    public void setPacket_id(int packet_id) {
        this.packet_id = packet_id;
    }

    public boolean isSigningRequired() {
        return signingRequired;
    }

    public void setSigningRequired(boolean signingRequired) {
        this.signingRequired = signingRequired;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public double getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(double xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public double getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(double yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public static Map<String, FieldOptions> getValidatorMap() {
        return formFieldOptionsMap;
    }

    private static Map<String, FieldOptions> formFieldOptionsMap = new HashMap<String, FieldOptions>()
    {{
        put("organization_id", FieldOptionsCommon.NUMBER);
        put("packet_id", FieldOptionsCommon.NUMBER);
        put("documentId", FieldOptionsCommon.NUMBER);
        put("document_name", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
        put("document_file_path", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
        put("signing_required", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 6));
        put("viewing_reqd", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 6));
        put("page", FieldOptionsCommon.NUMBER);
        put("x_coordinate", FieldOptionsCommon.DECIMAL);
        put("y_coordinate", FieldOptionsCommon.DECIMAL);
    }};
}
