package com.auzmor.impl.model.onboarding;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.util.HashMap;
import java.util.Map;

public class OnboardingTask {

    private int task_id;
    private String task_name;
    private String task_description;
    private boolean viewing_required;
    private boolean allow_doc_upload;
    private boolean doc_upload_required;
    private int packet_id;
    private String referralFile;

    public int getTask_id() {
        return task_id;
    }

    public void setTask_id(int task_id) {
        this.task_id = task_id;
    }

    public String getTask_name() {
        return task_name;
    }

    public void setTask_name(String task_name) {
        this.task_name = task_name;
    }

    public String getTask_description() {
        return task_description;
    }

    public void setTask_description(String task_description) {
        this.task_description = task_description;
    }

    public boolean isViewing_required() {
        return viewing_required;
    }

    public void setViewing_required(boolean viewing_required) {
        this.viewing_required = viewing_required;
    }

    public boolean isAllow_doc_upload() {
        return allow_doc_upload;
    }

    public void setAllow_doc_upload(boolean allow_doc_upload) {
        this.allow_doc_upload = allow_doc_upload;
    }

    public boolean isDoc_upload_required() {
        return doc_upload_required;
    }

    public void setDoc_upload_required(boolean doc_upload_required) {
        this.doc_upload_required = doc_upload_required;
    }

    public int getPacket_id() {
        return packet_id;
    }

    public void setPacket_id(int packet_id) {
        this.packet_id = packet_id;
    }

    public String getReferralFile() {
        return referralFile;
    }

    public void setReferralFile(String referralFile) {
        this.referralFile = referralFile;
    }

    public static Map<String, FieldOptions> getValidatorMap() {
        return formFieldOptionsMap;
    }

    private static Map<String, FieldOptions> formFieldOptionsMap = new HashMap<String, FieldOptions>()
    {{
        put("organization_id", FieldOptionsCommon.NUMBER);
        put("packet_id", FieldOptionsCommon.NUMBER);
        put("taskId", FieldOptionsCommon.NUMBER);
        put("task_name", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 500));
        put("task_description", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1,  500));
        put("task_file_path", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 6));
        put("allow_doc_upload", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 6));
        put("viewing_required", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 6));
        put("doc_upload_required", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 6));
    }};
}
