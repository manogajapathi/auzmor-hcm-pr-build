package com.auzmor.impl.model.onboarding;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.util.HashMap;
import java.util.Map;

public class OnboardingPacket {


    private int packetId;
    private String packetName;
    private String welcomeHeader;
    private String welcomeDescription;
    private String welcomeVideo;
    private String organizationAddress;
    private int creatorId;
    private int organizationId;
    private double latitude;
    private double longitude;
    public int getPacket_id() {
        return packetId;
    }

    public void setPacket_id(int packetId) {
        this.packetId = packetId;
    }

    public String getPacket_name() {
        return packetName;
    }

    public void setPacket_name(String packetName) {
        this.packetName = packetName;
    }

    public String getWelcome_header() {
        return welcomeHeader;
    }

    public void setWelcome_header(String welcomeHeader) {
        this.welcomeHeader = welcomeHeader;
    }

    public String getWelcome_description() {
        return welcomeDescription;
    }

    public void setWelcome_description(String welcomeDescription) {
        this.welcomeDescription = welcomeDescription;
    }

    public String getWelcome_video() {
        return welcomeVideo;
    }

    public void setWelcome_video(String welcomeVideo) {
        this.welcomeVideo = welcomeVideo;
    }

    public String getOrganization_address() {
        return organizationAddress;
    }

    public void setOrganization_address(String organizationAddress) {
        this.organizationAddress = organizationAddress;
    }

    public int getCreator_id() {
        return creatorId;
    }

    public void setCreator_id(int creatorId) {
        this.creatorId = creatorId;
    }

    public int getOrganization_id() {
        return organizationId;
    }

    public void setOrganization_id(int organizationId) {
        this.organizationId = organizationId;
    }

    public static Map<String, FieldOptions> getValidatorMap() {
        return welcomeFieldOptionsMap;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    private static Map<String, FieldOptions> welcomeFieldOptionsMap = new HashMap<String, FieldOptions>()
    {{
        put("organizationId", FieldOptionsCommon.NUMBER);
        put("organization_id", FieldOptionsCommon.NUMBER);
        put("packetId", FieldOptionsCommon.NUMBER);
        put("packet_name", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
        put("welcome_header", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
        put("welcome_description", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
        put("welcome_video", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
        put("creator_id", FieldOptionsCommon.NUMBER);
        put("latitude", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 15));
        put("longitude", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 15));
    }};

}
