package com.auzmor.impl.model;

import java.sql.Date;

public class SourceReport {

    private int organizationId;
    private int jobId;
    private int facebookCount;
    private int twitterCount;
    private int googleplusCount;
    private int directCount;
    private int monsterCount;
    private int careerBuilderCount;
    private int glassDoorCount;
    private int usjobsCount;
    private int linkedinCount;
    private Date sourceDate;

    public int getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(int organizationId) {
        this.organizationId = organizationId;
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public int getFacebookCount() {
        return facebookCount;
    }

    public void setFacebookCount(int facebookCount) {
        this.facebookCount = facebookCount;
    }

    public int getTwitterCount() {
        return twitterCount;
    }

    public void setTwitterCount(int twitterCount) {
        this.twitterCount = twitterCount;
    }

    public int getGoogleplusCount() {
        return googleplusCount;
    }

    public void setGoogleplusCount(int googleplusCount) {
        this.googleplusCount = googleplusCount;
    }

    public int getDirectCount() {
        return directCount;
    }

    public void setDirectCount(int directCount) {
        this.directCount = directCount;
    }

    public int getMonsterCount() {
        return monsterCount;
    }

    public void setMonsterCount(int monsterCount) {
        this.monsterCount = monsterCount;
    }

    public int getCareerBuilderCount() {
        return careerBuilderCount;
    }

    public void setCareerBuilderCount(int careerBuilderCount) {
        this.careerBuilderCount = careerBuilderCount;
    }

    public int getGlassDoorCount() {
        return glassDoorCount;
    }

    public void setGlassDoorCount(int glassDoorCount) {
        this.glassDoorCount = glassDoorCount;
    }

    public int getUsjobsCount() {
        return usjobsCount;
    }

    public void setUsjobsCount(int usjobsCount) {
        this.usjobsCount = usjobsCount;
    }

    public int getLinkedinCount() {
        return linkedinCount;
    }

    public void setLinkedinCount(int linkedinCount) {
        this.linkedinCount = linkedinCount;
    }

    public Date getSourceDate() {
        return sourceDate;
    }

    public void setSourceDate(java.util.Date sourceDate) {

        this.sourceDate = new java.sql.Date(sourceDate.getTime());

    }


}
