package com.auzmor.impl.model;

import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.util.StringUtil;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.LinkedHashMap;
import java.lang.Object;
import java.lang.Exception;

public class EmailTemplateModel{
    Connection connection =null;
    PreparedStatement preparedStatement=null;
    ResultSet resultSet = null;

    public int template_Id;
    public String template_type;
    public String template_address_line;
    public String template_details;
    public String template_footer_line;
    public int organization_id;

    public int getTemplate_Id() {
        return template_Id;
    }

    public void setTemplate_Id(int template_Id) {
        this.template_Id = template_Id;
    }

    public String getTemplate_type() {
        return template_type;
    }

    public void setTemplate_type(String template_type) {
        this.template_type = template_type;
    }


    public String getTemplate_details() {
        return template_details;
    }

    public void setTemplate_details(String template_details) {
        this.template_details = template_details;
    }

    public int getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(int organization_id) {
        this.organization_id = organization_id;
    }

    public String getTemplate_address_line() {
        return template_address_line;
    }

    public void setTemplate_address_line(String template_address_line) {
        this.template_address_line = template_address_line;
    }

    public String getTemplate_footer_line() {
        return template_footer_line;
    }

    public void setTemplate_footer_line(String template_footer_line) {
        this.template_footer_line = template_footer_line;
    }

    private Connection getConnection() throws SQLException {
        Connection conn = null;
        MysqldbConf mysqldbConf = new MysqldbConf();
        try {
            conn = mysqldbConf.getConnection();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return conn;
    }

    public void set(String fieldName, Object value) throws NoSuchFieldException {
        //Map values to fields in class
        Field field = this.getClass().getField(fieldName);
        if (field.getType().toString().equals("boolean")) {
            try {
                field.setBoolean(this, Boolean.parseBoolean((String) value));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else if (field.getType().toString().equals("int")){
            try {
                field.setInt(this, Integer.valueOf(String.valueOf(value)));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else {
            try {
                field.set(this, value);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public  List<EmailModel> SelectAll(String organization_id) throws SQLException,IllegalAccessException{
        List<EmailModel> listOfTemplates = new ArrayList<>();
        String query = "select template_Id,template_type from EmailTemplateConstant where organization_id="+organization_id+" ORDER By template_Id";
        connection = getConnection();
        preparedStatement = connection.prepareStatement(query);
        resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            EmailModel emailTemplateModel = new EmailModel();
            emailTemplateModel.setTemplate_Id(resultSet.getInt(1));
            emailTemplateModel.setTemplate_type(resultSet.getString(2));
            listOfTemplates.add(emailTemplateModel);
        }

        connection.close();
        return listOfTemplates;
    }

    public EmailTemplateModel getTemplate(int emailTempId) throws SQLException{
        EmailTemplateModel templateModel = new EmailTemplateModel();
        // List<JobDescriptionModel> jobdesc = new ArrayList<>();
        String queryString = "SELECT template_details,template_type,template_address_line,template_footer_line,organization_id FROM EmailTemplateConstant WHERE template_Id="+emailTempId;
        connection = getConnection();
        preparedStatement = connection.prepareStatement(queryString);
        resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            templateModel.setTemplate_Id(emailTempId);
            templateModel.setTemplate_type(resultSet.getString("template_type"));
            templateModel.setTemplate_address_line(resultSet.getString("template_address_line"));
            templateModel.setTemplate_details(resultSet.getString("template_details"));
            templateModel.setTemplate_footer_line(resultSet.getString("template_footer_line"));
            templateModel.setOrganization_id(resultSet.getInt("organization_id"));
        }
        connection.close();
        return templateModel;
    }

    public int addTemplateDetails(EmailTemplateModel templateModel) throws SQLException,Exception{

        connection = getConnection();
        Map<String,Object> insertObjects = new LinkedHashMap<>();
        StringUtil.checkRequiredFields(insertObjects,"template_type",templateModel.getTemplate_type());
        StringUtil.checkRequiredFields(insertObjects,"template_details",templateModel.getTemplate_details());

        insertObjects.put("organization_id", templateModel.getOrganization_id());

        String columnValueString = "";
        String entryValueString = "";
        String appender = "";
        for (Map.Entry<String,Object> columnEntries: insertObjects.entrySet()) {

            columnValueString += columnEntries.getKey()+",";

            appender = "'"+columnEntries.getValue()+"'";

            entryValueString += appender + ",";

        }

        entryValueString = entryValueString.substring(0,entryValueString.length()-1);
        columnValueString = columnValueString.substring(0,columnValueString.length()-1);


        String sqlQueryForEmployee = "insert into EmailTemplateConstant("+columnValueString+") values("+entryValueString+");";


        Statement preparedStatement = connection.createStatement();
        preparedStatement.executeUpdate(sqlQueryForEmployee,Statement.RETURN_GENERATED_KEYS);
        ResultSet rs = preparedStatement.getGeneratedKeys();

        int templateId = 0;

        if (rs.next()) {
            templateId = rs.getInt(1);
        }

        connection.close();

        return templateId;

    }
}