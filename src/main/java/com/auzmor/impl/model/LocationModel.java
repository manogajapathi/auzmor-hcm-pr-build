package com.auzmor.impl.model;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.impl.builder.QueryStringBuilder;
import com.auzmor.impl.util.StringUtil;

import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;
import io.vertx.ext.web.RoutingContext;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LocationModel {
  public int id;
  public String location;
  public String companyAddressLine1;
  public String companyAddressLine2;
  public String country;
  public String state;
  public String zip;
  public String city;
  public boolean updatedFlag;
  public int organizationId;
  public String activeStatus;

  public LocationModel() {

  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public int getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(int organization_id) {
    this.organizationId = organization_id;
  }

  public String getActiveStatus() {
    return activeStatus;
  }

  public void setActiveStatus(String active_status) {
    this.activeStatus = active_status;
  }

  public String getCompanyAddressLine1() {
    return companyAddressLine1;
  }

  public void setCompanyAddressLine1(String companyAddressLine1) {
    this.companyAddressLine1 = companyAddressLine1;
  }

  public String getCompanyAddressLine2() {
    return companyAddressLine2;
  }

  public void setCompanyAddressLine2(String companyAddressLine2) {
    this.companyAddressLine2 = companyAddressLine2;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getZip() {
    return zip;
  }

  public void setZip(String zip) {
    this.zip = zip;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public boolean isUpdatedFlag() {
    return updatedFlag;
  }

  public void setUpdatedFlag(boolean updatedFlag) {
    this.updatedFlag = updatedFlag;
  }

  public static Map<String, FieldOptions> getValidatorMap() {
    return locationFieldOptionsMap;
  }

  private static Map<String, FieldOptions> locationFieldOptionsMap = new HashMap<String, FieldOptions>()
  {{
    put("organization_id", FieldOptionsCommon.NUMBER);
    put("id", FieldOptionsCommon.NUMBER);
    put("location", new FieldOptionsImpl(FieldOptionsCommon.ANY,-1, 100));
    put("companyAddressLine1", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY,
        -1, 350));
    put("companyAddressLine2", new FieldOptionsImpl(FieldOptionsCommon.ANY,
        -1, 100));
    put("country", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 100));
    put("zip", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 100));
    put("city", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 50));
    put("state", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 100));
    put("active_status", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 10));
    put("company_address_line1", new FieldOptionsImpl(FieldOptionsCommon.ANY_VALUES,
        -1, 350));
  }};
}