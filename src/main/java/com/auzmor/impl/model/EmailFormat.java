package com.auzmor.impl.model;

import java.lang.reflect.Field;

public class EmailFormat {

  public String from_address;
  public String to_address;
  public String subject_name;
  public String mail_description;
  public String user_name;

  public String getFrom_address() {
    return from_address;
  }

  public void setFrom_address(String from_address) {
    this.from_address = from_address;
  }

  public String getTo_address() {
    return to_address;
  }

  public void setTo_address(String to_address) {
    this.to_address = to_address;
  }

  public String getSubject_name() {
    return subject_name;
  }

  public void setSubject_name(String subject_name) {
    this.subject_name = subject_name;
  }

  public String getMail_description() {
    return mail_description;
  }

  public void setMail_description(String mail_description) {
    this.mail_description = mail_description;
  }

  public String getUser_name() {
    return user_name;
  }

  public void setUser_name(String user_name) {
    this.user_name = user_name;
  }

  public EmailFormat set(String fieldName, Object value) throws NoSuchFieldException {
    //Map values to fields in class
    Field field = this.getClass().getField(fieldName);
    if (field.getType().toString().equals("boolean")) {
      try {
        field.setBoolean(this, Boolean.parseBoolean((String) value));
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      }
    } else if (field.getType().toString().equals("int")) {
      try {
        field.setInt(this, Integer.valueOf(String.valueOf(value)));
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      }
    } else {
      try {
        field.set(this, value);
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      }
    }
    return this;
  }
    

}
