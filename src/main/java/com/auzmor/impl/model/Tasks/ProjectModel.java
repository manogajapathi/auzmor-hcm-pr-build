package com.auzmor.impl.model.Tasks;

import java.lang.reflect.Field;

public class ProjectModel {

    public int project_id;
    public String project_name;
    public int admin_id;
    public String created_date;
    public String modified_date;
    public int organization_id;

    public int getProject_id() {
        return project_id;
    }

    public void setProject_id(int project_id) {
        this.project_id = project_id;
    }

    public int getAdmin_id() {
        return admin_id;
    }

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public void setAdmin_id(int admin_id) {
        this.admin_id = admin_id;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getModified_date() {
        return modified_date;
    }

    public void setModified_date(String modified_date) {
        this.modified_date = modified_date;
    }

    public int getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(int organization_id) {
        this.organization_id = organization_id;
    }

    public ProjectModel set(String fieldName, Object value) throws NoSuchFieldException {
        //Map values to fields in class

        Field field = this.getClass().getField(fieldName);
        if (field.getType().toString().equals("boolean")) {
            try {
                field.setBoolean(this, Boolean.parseBoolean((String) value));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else if (field.getType().toString().equals("int")) {
            try {
                field.setInt(this, Integer.valueOf(String.valueOf(value)));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else {
            try {
                field.set(this, value);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return this;
    }

}
