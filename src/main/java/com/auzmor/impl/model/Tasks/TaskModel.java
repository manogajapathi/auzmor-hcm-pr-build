package com.auzmor.impl.model.Tasks;

import java.lang.reflect.Field;

public class TaskModel {

    public int task_id;
    public String task_title;
    public int assign_to;
    public int assign_by;
    public String due_date;
    public String time_due;
    public int project_id;
    public String recurrence;
    public String priority;
    public String description;
    public String task_status;
    public String created_date;
    public String modified_date;
    public int organization_id;


    public int getTask_id() {
        return task_id;
    }

    public void setTask_id(int task_id) {
        this.task_id = task_id;
    }

    public String getTask_title() {
        return task_title;
    }

    public void setTask_title(String task_title) {
        this.task_title = task_title;
    }

    public int getAssign_to() {
        return assign_to;
    }

    public void setAssign_to(int assign_to) {
        this.assign_to = assign_to;
    }

    public int getAssign_by() {
        return assign_by;
    }

    public void setAssign_by(int assign_by) {
        this.assign_by = assign_by;
    }

    public String getDue_date() {
        return due_date;
    }

    public void setDue_date(String due_date) {
        this.due_date = due_date;
    }

    public String getTime_due() {
        return time_due;
    }

    public void setTime_due(String time_due) {
        this.time_due = time_due;
    }

    public int getProject_id() {
        return project_id;
    }

    public void setProject_id(int project_id) {
        this.project_id = project_id;
    }

    public String getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(String recurrence) {
        this.recurrence = recurrence;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getModified_date() {
        return modified_date;
    }

    public void setModified_date(String modified_date) {
        this.modified_date = modified_date;
    }

    public int getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(int organization_id) {
        this.organization_id = organization_id;
    }

    public String getTask_status() {
        return task_status;
    }

    public void setTask_status(String task_status) {
        this.task_status = task_status;
    }

    public TaskModel set(String fieldName, Object value) throws NoSuchFieldException {
        //Map values to fields in class

        Field field = this.getClass().getField(fieldName);
        if (field.getType().toString().equals("boolean")) {
            try {
                field.setBoolean(this, Boolean.parseBoolean((String) value));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else if (field.getType().toString().equals("int")) {
            try {
                field.setInt(this, Integer.valueOf(String.valueOf(value)));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else {
            try {
                field.set(this, value);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return this;
    }
}
