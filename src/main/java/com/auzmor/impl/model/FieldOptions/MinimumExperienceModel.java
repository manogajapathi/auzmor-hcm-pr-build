package com.auzmor.impl.model.FieldOptions;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.util.HashMap;
import java.util.Map;

public class MinimumExperienceModel {
	private long id;
	private String minimumExperience;
	private long organizationId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMinimumExperience() {
		return minimumExperience;
	}

	public void setMinimumExperience(String minimumExperience) {
		this.minimumExperience = minimumExperience;
	}

	public long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}

	public static Map<String, FieldOptions> getValidatorMap() {
		return minimumExperienceMap;
	}

	private static Map<String, FieldOptions> minimumExperienceMap = new HashMap<String, FieldOptions>()
	{{
		put("organization_id", FieldOptionsCommon.NUMBER);
		put("organizationId", FieldOptionsCommon.NUMBER);
		put("id", FieldOptionsCommon.NUMBER);
		put("minimum_experience", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
	}};
}
