package com.auzmor.impl.model.FieldOptions;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.util.HashMap;
import java.util.Map;

public class DivisionModel {
    private int divisionId;
    private String divisionName;
    private String activeStatus;
    private int organizationId;
    private int employeeCount;

    public int getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(int divisionId) {
        this.divisionId = divisionId;
    }

    public String getDivisionName() {
        return divisionName;
    }

    public void setDivisionName(String divisionName) {
        this.divisionName = divisionName;
    }

    public String isActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus;
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(int organizationId) {
        this.organizationId = organizationId;
    }

    public int getEmployeeCount() { return employeeCount; }

    public void setEmployeeCount(int employeeCount) { this.employeeCount = employeeCount; }

    public static Map<String, FieldOptions> getValidatorMap() {
        return divisionFieldOptionsMap;
    }

    private static Map<String, FieldOptions> divisionFieldOptionsMap = new HashMap<String, FieldOptions>()
    {{
        put("organization_id", FieldOptionsCommon.NUMBER);
        put("organizationId", FieldOptionsCommon.NUMBER);
        put("divisionId", FieldOptionsCommon.NUMBER);
        put("division_name", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
        put("active_status", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 10));
    }};
}
