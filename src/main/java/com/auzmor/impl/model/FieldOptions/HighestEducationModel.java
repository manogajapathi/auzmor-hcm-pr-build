package com.auzmor.impl.model.FieldOptions;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.util.HashMap;
import java.util.Map;

public class HighestEducationModel {
	private long id;
	private String highestEducation;
	private long organizationId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getHighestEducation() {
		return highestEducation;
	}

	public void setHighestEducation(String highestEducation) {
		this.highestEducation = highestEducation;
	}

	public long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(long organizationId) {
		this.organizationId = organizationId;
	}

	public static Map<String, FieldOptions> getValidatorMap() {
		return highestEducationFieldOptionsMap;
	}

	private static Map<String, FieldOptions> highestEducationFieldOptionsMap = new HashMap<String, FieldOptions>()
	{{
		put("organization_id", FieldOptionsCommon.NUMBER);
		put("organizationId", FieldOptionsCommon.NUMBER);
		put("id", FieldOptionsCommon.NUMBER);
		put("highest_education", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
	}};
}
