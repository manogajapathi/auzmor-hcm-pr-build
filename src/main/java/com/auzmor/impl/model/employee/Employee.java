package com.auzmor.impl.model.employee;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.model.Invite;
import com.auzmor.util.validator.FieldOptions;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class Employee {

  private int employee_id;
  private String first_name;
  private String middle_name;
  private String preferred_name;
  private String last_name;
  private String address_line1;
  private String address_line2;
  private String city;
  private String state;
  private String zip;
  private String country;
  private String marital_status;
  private String gender;
  private String date_of_birth;
  private String ssn;
  private String ethnicity;
  private String hire_date;
  private String job_title;
  private String manager;
  private String department;
  private String division;
  private String location;
  private String pay_rate;
  private String pay_type;
  private String employment_status;
  private String pay_group;
  private String primary_phone;
  private String secondary_phone;
  private String work_phone;
  private String personal_email;
  private String work_email;
  private String created_date;
  private String modified_date;
  private int organization_id;
  private int candidate_id;
  private String disability_status;
  private String veteran_status;
  private String veteran_classification;
  private String military_discharge_date;
  private String contact1_first_name;
  private String contact2_first_name;
  private String contact1_last_name;
  private String contact2_last_name;
  private String contact1_phone_number;
  private String contact2_phone_number;
  private String contact1_relationship;
  private String contact2_relationship;
  private String deposit_first_name;
  private String deposit_last_name;
  private String deposit_bank_name;
  private String deposit_account_type;
  private String deposit_account_number;
  private String deposit_routing_number;
  private String profile_image;
  private String currency_type;
  private String access_level;
  private int user_id;
  private String employee_number;
  private String status;
  private int onboarding_progress;
  private String onboarding_packet;
  private String onboarding_status;
  private String onboarding_timestamp;
  private String onboarding_packet_date;
  private String primary_region_code;
  private String secondary_region_code;
  private String work_region_code;
  private String timezone_format;

  public int getOnboarding_progress() {
    return onboarding_progress;
  }

  public void setOnboarding_progress(int onboarding_progress) {
    this.onboarding_progress = onboarding_progress;
  }

  public String getEmployee_number() {
    return employee_number;
  }

  public void setEmployee_number(String employee_number) {
    this.employee_number = employee_number;
  }

  public String getCurrency_type() {
    return currency_type;
  }

  public void setCurrency_type(String currency_type) {
    this.currency_type = currency_type;
  }
  // private boolean selfserviceaccess;


  public String getPrimary_region_code() {
    return primary_region_code;
  }

  public void setPrimary_region_code(String primary_region_code) {
    this.primary_region_code = primary_region_code;
  }

  public String getSecondary_region_code() {
    return secondary_region_code;
  }

  public void setSecondary_region_code(String secondary_region_code) {
    this.secondary_region_code = secondary_region_code;
  }

  public String getWork_region_code() {
    return work_region_code;
  }

  public void setWork_region_code(String work_region_code) {
    this.work_region_code = work_region_code;
  }

  public int getEmployee_id() {
    return employee_id;
  }

  public void setEmployee_id(int employee_id) {
    this.employee_id = employee_id;
  }

  public String getAddress_line1() {
    return address_line1;
  }

  public void setAddress_line1(String address_line1) {
    this.address_line1 = address_line1;
  }

  public String getAddress_line2() {
    return address_line2;
  }

  public void setAddress_line2(String address_line2) {
    this.address_line2 = address_line2;
  }

  public String getDate_of_birth() {
    return date_of_birth;
  }

  public void setDate_of_birth(String date_of_birth) {
    this.date_of_birth = date_of_birth;
  }

  public String getFirst_name() {
    return first_name;
  }

  public void setFirst_name(String first_name) {
    this.first_name = first_name;
  }

  public int getOrganization_id() {
    return organization_id;
  }

  public void setOrganization_id(int organization_id) {
    this.organization_id = organization_id;
  }

  public String getMiddle_name() {
    return middle_name;
  }

  public void setMiddle_name(String middle_name) {
    this.middle_name = middle_name;
  }

  public String getPreferred_name() {
    return preferred_name;
  }

  public void setPreferred_name(String preferred_name) {
    this.preferred_name = preferred_name;
  }

  public String getLast_name() {
    return last_name;
  }

  public void setLast_name(String last_name) {
    this.last_name = last_name;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getZip() {
    return zip;
  }

  public void setZip(String zip) {
    this.zip = zip;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getMarital_status() {
    return marital_status;
  }

  public void setMarital_status(String marital_status) {
    this.marital_status = marital_status;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getSsn() {
    return ssn;
  }

  public void setSsn(String ssn) {
    this.ssn = ssn;
  }

  public String getEthnicity() {
    return ethnicity;
  }

  public void setEthnicity(String ethnicity) {
    this.ethnicity = ethnicity;
  }

  public String getHire_date() {
    return hire_date;
  }

  public void setHire_date(String hire_date) {
    this.hire_date = hire_date;
  }

  public String getJob_title() {
    return job_title;
  }

  public void setJob_title(String job_title) {
    this.job_title = job_title;
  }

  public String getManager() {
    return manager;
  }

  public void setManager(String manager) {
    this.manager = manager;
  }

  public String getDepartment() {
    return department;
  }

  public void setDepartment(String department) {
    this.department = department;
  }

  public String getDivision() {
    return division;
  }

  public void setDivision(String division) {
    this.division = division;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public String getPay_rate() {
    return pay_rate;
  }

  public void setPay_rate(String pay_rate) {
    this.pay_rate = pay_rate;
  }

  public String getPay_type() {
    return pay_type;
  }

  public void setPay_type(String pay_type) {
    this.pay_type = pay_type;
  }

  public String getEmployment_status() {
    return employment_status;
  }

  public void setEmployment_status(String employment_status) {
    this.employment_status = employment_status;
  }

  public String getPay_group() {
    return pay_group;
  }

  public void setPay_group(String pay_group) {
    this.pay_group = pay_group;
  }

  public String getPersonal_email() {
    return personal_email;
  }

  public void setPersonal_email(String personal_email) {
    this.personal_email = personal_email;
  }

  public String getWork_email() {
    return work_email;
  }

  public void setWork_email(String work_email) {
    this.work_email = work_email;
  }

  public String getPrimary_phone() {
    return primary_phone;
  }

  public void setPrimary_phone(String primary_phone) {
    this.primary_phone = primary_phone;
  }

  public String getSecondary_phone() {
    return secondary_phone;
  }

  public void setSecondary_phone(String secondary_phone) {
    this.secondary_phone = secondary_phone;
  }

  public String getWork_phone() {
    return work_phone;
  }

  public void setWork_phone(String work_phone) {
    this.work_phone = work_phone;
  }

  public String getCreated_date() {
    return created_date;
  }

  public void setCreated_date(String created_date) {
    this.created_date = created_date;
  }

  public String getModified_date() {
    return modified_date;
  }

  public void setModified_date(String modified_date) {
    this.modified_date = modified_date;
  }

  public String getDisability_status() {
    return disability_status;
  }

  public void setDisability_status(String disability_status) {
    this.disability_status = disability_status;
  }

  public String getVeteran_status() {
    return veteran_status;
  }

  public void setVeteran_status(String veteran_status) {
    this.veteran_status = veteran_status;
  }

  public String getVeteran_classification() {
    return veteran_classification;
  }

  public void setVeteran_classification(String veteran_classification) {
    this.veteran_classification = veteran_classification;
  }

  public String getMilitary_discharge_date() {
    return military_discharge_date;
  }

  public void setMilitary_discharge_date(String military_discharge_date) {
    this.military_discharge_date = military_discharge_date;
  }

  public String getContact1_first_name() {
    return contact1_first_name;
  }

  public void setContact1_first_name(String contact1_first_name) {
    this.contact1_first_name = contact1_first_name;
  }

  public String getContact2_first_name() {
    return contact2_first_name;
  }

  public void setContact2_first_name(String contact2_first_name) {
    this.contact2_first_name = contact2_first_name;
  }

  public String getContact1_last_name() {
    return contact1_last_name;
  }

  public void setContact1_last_name(String contact1_last_name) {
    this.contact1_last_name = contact1_last_name;
  }

  public String getContact2_last_name() {
    return contact2_last_name;
  }

  public void setContact2_last_name(String contact2_last_name) {
    this.contact2_last_name = contact2_last_name;
  }

  public String getContact1_phone_number() {
    return contact1_phone_number;
  }

  public void setContact1_phone_number(String contact1_phone_number) {
    this.contact1_phone_number = contact1_phone_number;
  }

  public String getContact2_phone_number() {
    return contact2_phone_number;
  }

  public void setContact2_phone_number(String contact2_phone_number) {
    this.contact2_phone_number = contact2_phone_number;
  }

  public String getContact1_relationship() {
    return contact1_relationship;
  }

  public void setContact1_relationship(String contact1_relationship) {
    this.contact1_relationship = contact1_relationship;
  }

  public String getContact2_relationship() {
    return contact2_relationship;
  }

  public void setContact2_relationship(String contact2_relationship) {
    this.contact2_relationship = contact2_relationship;
  }

  public String getDeposit_first_name() {
    return deposit_first_name;
  }

  public void setDeposit_first_name(String deposit_first_name) {
    this.deposit_first_name = deposit_first_name;
  }

  public String getDeposit_last_name() {
    return deposit_last_name;
  }

  public void setDeposit_last_name(String deposit_last_name) {
    this.deposit_last_name = deposit_last_name;
  }

  public String getDeposit_bank_name() {
    return deposit_bank_name;
  }

  public void setDeposit_bank_name(String deposit_bank_name) {
    this.deposit_bank_name = deposit_bank_name;
  }

  public String getDeposit_account_type() {
    return deposit_account_type;
  }

  public void setDeposit_account_type(String deposit_account_type) {
    this.deposit_account_type = deposit_account_type;
  }

  public String getDeposit_account_number() {
    return deposit_account_number;
  }

  public void setDeposit_account_number(String deposit_account_number) {
    this.deposit_account_number = deposit_account_number;
  }

  public String getDeposit_routing_number() {
    return deposit_routing_number;
  }

  public void setDeposit_routing_number(String deposit_routing_number) {
    this.deposit_routing_number = deposit_routing_number;
  }

   /* public boolean isSelfserviceaccess() {
        return selfserviceaccess;
    }

    public void setSelfserviceaccess(boolean selfserviceaccess) {
        this.selfserviceaccess = selfserviceaccess;
    }
*/

  public String getAccess_level() {
    return access_level;
  }

  public void setAccess_level(String access_level) {
    this.access_level = access_level;
  }

  public String getOnboarding_packet() {
    return onboarding_packet;
  }

  public void setOnboarding_packet(String onboarding_packet) {
    this.onboarding_packet = onboarding_packet;
  }

  public int getCandidate_id() {
    return candidate_id;
  }

  public void setCandidate_id(int candidate_id) {
    this.candidate_id = candidate_id;
  }

  public String getOnboarding_status() {
    return onboarding_status;
  }

  public void setOnboarding_status(String onboarding_status) {
    this.onboarding_status = onboarding_status;
  }

  public String getOnboarding_timestamp() {
    return onboarding_timestamp;
  }

  public void setOnboarding_timestamp(String onboarding_timestamp) {
    this.onboarding_timestamp = onboarding_timestamp;
  }

  public String getProfile_image() {
    return profile_image;
  }

  public void setProfile_image(String profile_image) {
    this.profile_image = profile_image;
  }

  public int getUser_id() {
    return user_id;
  }

  public void setUser_id(int user_id) {
    this.user_id = user_id;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getOnboarding_packet_date() {
    return onboarding_packet_date;
  }

  public void setOnboarding_packet_date(String onboarding_packet_date) {
    this.onboarding_packet_date = onboarding_packet_date;
  }

  public String getTimeZoneFormat() {
    return timezone_format;
  }

  public void setTimeZoneFormat(String timeZoneFormat) {
    this.timezone_format = timeZoneFormat;
  }


  public Employee set(String fieldName, Object value) throws NoSuchFieldException {
    //Map values to fields in class
    Field field = this.getClass().getField(fieldName);
    field.setAccessible(true);
    if (field.getType().toString().equals("boolean")) {
      try {
        field.setBoolean(this, Boolean.parseBoolean((String) value));
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      }
    } else if (field.getType().toString().equals("int")) {
      try {
        field.setInt(this, Integer.valueOf(String.valueOf(value)));
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      }
    } else {
      try {
        field.set(this, value);
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      }
    }
    return this;

  }
}