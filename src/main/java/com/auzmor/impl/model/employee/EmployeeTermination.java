package com.auzmor.impl.model.employee;

public class EmployeeTermination {
    private int employee_id;
    private String termination_effective_date;
    private String termination_type;
    private String termination_reason;
    private String rehire_eligibility;
    private String termination_comment;

    public int getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(int employee_id) {
        this.employee_id = employee_id;
    }

    public String getTermination_effective_date() {
        return termination_effective_date;
    }

    public void setTermination_effective_date(String termination_effective_date) {
        this.termination_effective_date = termination_effective_date;
    }

    public String getTermination_type() {
        return termination_type;
    }

    public void setTermination_type(String termination_type) {
        this.termination_type = termination_type;
    }

    public String getTermination_reason() {
        return termination_reason;
    }

    public void setTermination_reason(String termination_reason) {
        this.termination_reason = termination_reason;
    }

    public String getRehire_eligibility() {
        return rehire_eligibility;
    }

    public void setRehire_eligibility(String rehire_eligibility) {
        this.rehire_eligibility = rehire_eligibility;
    }

    public String getTermination_comment() {
        return termination_comment;
    }

    public void setTermination_comment(String termination_comment) {
        this.termination_comment = termination_comment;
    }
}
