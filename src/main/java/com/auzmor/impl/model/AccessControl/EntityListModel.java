package com.auzmor.impl.model.AccessControl;

public class EntityListModel {
    private int id;
    private String entity_class;
    private String entity_method;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEntity_class() {
        return entity_class;
    }

    public void setEntity_class(String entity_class) {
        this.entity_class = entity_class;
    }

    public String getEntity_method() {
        return entity_method;
    }

    public void setEntity_method(String entity_method) {
        this.entity_method = entity_method;
    }
}
