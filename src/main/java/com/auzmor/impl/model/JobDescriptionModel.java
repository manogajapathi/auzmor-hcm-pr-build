package com.auzmor.impl.model;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JobDescriptionModel {


  // private static final AtomicInteger COUNTER = new AtomicInteger();
  Connection connection = null;
  PreparedStatement ptmt = null;
  ResultSet resultSet = null;
  StringUtil stringUtil = new StringUtil();

  public long jobdescId;

  public String jobdesctype;

  public String jobdescdetails;

  public long organizationId;

  public long getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(long organizationId) {
    this.organizationId = organizationId;
  }

  // creating database connection

  public long getJobdescId() {
    return jobdescId;
  }

  public void setJobdescId(long jobdescId) {
    this.jobdescId = jobdescId;
  }

  public String getJobdesctype() {
    return jobdesctype;
  }

  public void setJobdesctype(String jobdesctype) {
    this.jobdesctype = jobdesctype;
  }

  public String getJobdescdetails() {
    return jobdescdetails;
  }

  public void setJobdescdetails(String jobdescdetails) {
    this.jobdescdetails = jobdescdetails;
  }

  public static Map<String, FieldOptions> getValidatorMap() {
    return jobDescFieldOptionsMap;
  }

  private static Map<String, FieldOptions> jobDescFieldOptionsMap = new HashMap<String, FieldOptions>()
  {{
    put("id", FieldOptionsCommon.NUMBER);
    put("jobdescId", FieldOptionsCommon.NUMBER);
    put("jobdesctype", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
    put("jobdescdetails", FieldOptionsCommon.ANY);
    put("organization_id", FieldOptionsCommon.NUMBER);
  }};

}
