package com.auzmor.impl.model.PTO;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

public class PTOComments {
  private long id;
  private long ptoId;
  private String commentDescription;
  private long creatorId;
  private Date createdDate;
  private String commentsFilePath;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getPtoId() {
    return ptoId;
  }

  public void setPtoId(long ptoId) {
    this.ptoId = ptoId;
  }

  public String getCommentDescription() {
    return commentDescription;
  }

  public void setCommentDescription(String commentDescription) {
    this.commentDescription = commentDescription;
  }

  public long getCreatorId() {
    return creatorId;
  }

  public void setCreatorId(long creatorId) {
    this.creatorId = creatorId;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public String getCommentsFilePath() {
    return commentsFilePath;
  }

  public void setCommentsFilePath(String commentsFilePath) {
    this.commentsFilePath = commentsFilePath;
  }

  public static Map<String, FieldOptions> getValidatorMap() {
    return ptoFieldOptionsMap;
  }

  private static Map<String, FieldOptions> ptoFieldOptionsMap = new HashMap<String, FieldOptions>()
  {{
    put("pto_id", FieldOptionsCommon.NUMBER);
    put("id", FieldOptionsCommon.NUMBER);
    put("creator_id", FieldOptionsCommon.NUMBER);
    put("creator_date", FieldOptionsCommon.DATE_FORMAT);
    put("comment_description", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
    put("pto_comments_file_path", new FieldOptionsImpl(FieldOptionsCommon.ANY_ALLOW_EMPTY,
        -1, 255));
  }};
}
