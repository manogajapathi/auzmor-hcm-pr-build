package com.auzmor.impl.model.PTO;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

public class EmployeePTORequests {
  private long id;
  private long employeeId;
  private Date fromDate;
  private Date toDate;
  private String timeOffType;
  private double hours;
  private String description;
  private int requestStatus;
  private Date submittedDate;
  private String approvedBy;
  private Date approvedDate;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getEmployeeId() {
    return employeeId;
  }

  public void setEmployeeId(long employeeId) {
    this.employeeId = employeeId;
  }

  public Date getFromDate() {
    return fromDate;
  }

  public void setFromDate(Date fromDate) {
    this.fromDate = fromDate;
  }

  public Date getToDate() {
    return toDate;
  }

  public void setToDate(Date toDate) {
    this.toDate = toDate;
  }

  public String getTimeOffType() {
    return timeOffType;
  }

  public void setTimeOffType(String timeOffType) {
    this.timeOffType = timeOffType;
  }

  public double getHours() {
    return hours;
  }

  public void setHours(double hours) {
    this.hours = hours;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public int getRequestStatus() {
    return requestStatus;
  }

  public void setRequestStatus(int requestStatus) {
    this.requestStatus = requestStatus;
  }

  public Date getSubmittedDate() {
    return submittedDate;
  }

  public void setSubmittedDate(Date submittedDate) {
    this.submittedDate = submittedDate;
  }

  public String getApprovedBy() {
    return approvedBy;
  }

  public void setApprovedBy(String approvedBy) {
    this.approvedBy = approvedBy;
  }

  public Date getApprovedDate() {
    return approvedDate;
  }

  public void setApprovedDate(Date approvedDate) {
    this.approvedDate = approvedDate;
  }

  public static Map<String, FieldOptions> getValidatorMap() {
    return ptoFieldOptionsMap;
  }

  private static Map<String, FieldOptions> ptoFieldOptionsMap = new HashMap<String, FieldOptions>()
  {{
    put("employee_id", FieldOptionsCommon.NUMBER);
    put("id", FieldOptionsCommon.NUMBER);
    put("from_date", FieldOptionsCommon.DATE_FORMAT);
    put("to_date", FieldOptionsCommon.DATE_FORMAT);
    put("hours", FieldOptionsCommon.DECIMAL);
    put("description", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
    put("time_off_type", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 50));
    put("request_status", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 6));
    put("approved_by", new FieldOptionsImpl(FieldOptionsCommon.ANY, -1, 255));
  }};
}
