package com.auzmor.impl.model.PTO;

public class PTODescriptor {
  private long id;
  private long employeeId;
  private double maxHours;
  private String timeOffType;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getEmployeeId() {
    return employeeId;
  }

  public void setEmployeeId(long employeeId) {
    this.employeeId = employeeId;
  }

  public double getMaxHours() {
    return maxHours;
  }

  public void setMaxHours(double maxHours) {
    this.maxHours = maxHours;
  }

  public String getTimeOffType() {
    return timeOffType;
  }

  public void setTimeOffType(String timeOffType) {
    this.timeOffType = timeOffType;
  }
}
