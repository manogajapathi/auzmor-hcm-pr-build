package com.auzmor.impl.model;

import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.util.validator.FieldOptions;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class OrganizationR {
  
  private long id;
  private String domainName;
  private String careerUrl;
  private String hrisUrl;
  private String logo;
  private String organizationName;
  private String fileSize;
  private String firstName;
  private String last_name;
  private String accountOwnerEmail;
  private String ein;
  private String phoneNumber;
  private String addressLine1;
  private String addressLine2;
  private String city;
  private String state;
  private String country;
  private int zipCode;
  private String favicon;
  private String dateFormat;
  private String numberFormat;
  private String nameFormat;
  private String defaultCurrency;
  private String defaultCountry;
  private String profileImage;
  private String timezoneFormat;
  private String websiteUrl;
  private long plan_version_id;
  private Date created;
  private Date modified;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }
  public String getDomainName() {
    return domainName;
  }

  public void setDomainName(String domainName) {
    this.domainName = domainName;
  }

  public String getCareerUrl() {
    return careerUrl;
  }

  public void setCareerUrl(String careerUrl) {
    this.careerUrl = careerUrl;
  }

  public String getHrisUrl() {
    return hrisUrl;
  }

  public void setHrisUrl(String hrisUrl) {
    this.hrisUrl = hrisUrl;
  }

  public String getLogo() {
    return logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  public String getOrganizationName() {
    return organizationName;
  }

  public void setOrganizationName(String organizationName) {
    this.organizationName = organizationName;
  }

  public String getFileSize() {
    return fileSize;
  }

  public void setFileSize(String fileSize) {
    this.fileSize = fileSize;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLast_name() {
    return last_name;
  }

  public void setLast_name(String last_name) {
    this.last_name = last_name;
  }

  public String getAccountOwnerEmail() {
    return accountOwnerEmail;
  }

  public void setAccountOwnerEmail(String accountOwnerEmail) {
    this.accountOwnerEmail = accountOwnerEmail;
  }

  public String getEin() {
    return ein;
  }

  public void setEin(String ein) {
    this.ein = ein;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getAddressLine1() {
    return addressLine1;
  }

  public void setAddressLine1(String addressLine1) {
    this.addressLine1 = addressLine1;
  }

  public String getAddressLine2() {
    return addressLine2;
  }

  public void setAddressLine2(String addressLine2) {
    this.addressLine2 = addressLine2;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public int getZipCode() {
    return zipCode;
  }

  public void setZipCode(int zipCode) {
    this.zipCode = zipCode;
  }

  public String getFavicon() {
    return favicon;
  }

  public void setFavicon(String favicon) {
    this.favicon = favicon;
  }

  public String getDateFormat() {
    return dateFormat;
  }

  public void setDateFormat(String dateFormat) {
    this.dateFormat = dateFormat;
  }

  public String getNumberFormat() {
    return numberFormat;
  }

  public void setNumberFormat(String numberFormat) {
    this.numberFormat = numberFormat;
  }

  public String getNameFormat() {
    return nameFormat;
  }

  public void setNameFormat(String nameFormat) {
    this.nameFormat = nameFormat;
  }

  public String getDefaultCurrency() {
    return defaultCurrency;
  }

  public void setDefaultCurrency(String defaultCurrency) {
    this.defaultCurrency = defaultCurrency;
  }

  public String getDefaultCountry() {
    return defaultCountry;
  }

  public void setDefaultCountry(String defaultCountry) {
    this.defaultCountry = defaultCountry;
  }

  public String getProfileImage() {
    return profileImage;
  }

  public void setProfileImage(String profileImage) {
    this.profileImage = profileImage;
  }

  public String getTimezoneFormat() {
    return timezoneFormat;
  }

  public void setTimezoneFormat(String timezoneFormat) {
    this.timezoneFormat = timezoneFormat;
  }

  public String getWebsiteUrl() {
    return websiteUrl;
  }

  public void setWebsiteUrl(String websiteUrl) {
    this.websiteUrl = websiteUrl;
  }

  public long getPlan_version_id() {
    return plan_version_id;
  }

  public void setPlan_version_id(long plan_version_id) {
    this.plan_version_id = plan_version_id;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public Date getModified() {
    return modified;
  }

  public void setModified(Date modified) {
    this.modified = modified;
  }

  public static Map<String, FieldOptions> getValidatorMap() {
    return candidateFieldOptionsMap;
  }


  private static Map<String, FieldOptions> candidateFieldOptionsMap = new HashMap<String, FieldOptions>()
  {{
    put("organization_id", FieldOptionsCommon.NUMBER);
    put("id", FieldOptionsCommon.NUMBER);
    put("first_name", FieldOptionsCommon.ALPHABETSWITHSPACE);
    put("last_name", FieldOptionsCommon.ALPHABETSWITHSPACE);
    put("domain_name", FieldOptionsCommon.ALPHABETSWITHSPACE);
    put("career_url",FieldOptionsCommon.ANY_ALLOW_EMPTY);
    put("hris_url",FieldOptionsCommon.ANY);
    put("logo",FieldOptionsCommon.ANY_ALLOW_EMPTY);
    put("organization_name",FieldOptionsCommon.ALPHANUMERICWITHSPACE);
    put("file_size",FieldOptionsCommon.ALPHANUMERICWITHSPACE);
    put("account_owner_email",FieldOptionsCommon.EMAIL);
    put("ein",FieldOptionsCommon.NUMBER);
    put("phone_no",FieldOptionsCommon.NUMBER);
    put("company_address_line1",FieldOptionsCommon.ANY_ALLOW_EMPTY);
    put("company_address_line2",FieldOptionsCommon.ANY_ALLOW_EMPTY);
    put("city", FieldOptionsCommon.ANY_ALLOW_EMPTY);
    put("state", FieldOptionsCommon.ANY_ALLOW_EMPTY);
    put("country", FieldOptionsCommon.ANY_ALLOW_EMPTY);
    put("zip_code", FieldOptionsCommon.ANY_ALLOW_EMPTY);
    put("favicon",FieldOptionsCommon.ANY_ALLOW_EMPTY);
    put("date_format",FieldOptionsCommon.ANY_ALLOW_EMPTY);
    put("number_format",FieldOptionsCommon.NUMBERWITHCOMMA);
    put("name_format",FieldOptionsCommon.ALPHABETSWITHSPACE);
    put("default_currency",FieldOptionsCommon.ANY_ALLOW_EMPTY);
    put("default_country",FieldOptionsCommon.ANY_ALLOW_EMPTY);
    put("employee_profile_image",FieldOptionsCommon.ANY_ALLOW_EMPTY);
    put("timezone_format",FieldOptionsCommon.ANY_ALLOW_EMPTY);
    put("website_url",FieldOptionsCommon.ANY);
  }};


}

