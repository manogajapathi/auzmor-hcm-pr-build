package com.auzmor.impl.model;


import com.auzmor.impl.common.FieldOptionsCommon;
import com.auzmor.impl.util.validator.FieldOptionsImpl;
import com.auzmor.util.validator.FieldOptions;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

public class Invite {
  private long id;
  private long organizationId;
  private String email;
  private String key;
  private boolean pending;
  private Timestamp created;
  private Timestamp modified;
  private Timestamp deleted;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public long getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(long organizationId) {
    this.organizationId = organizationId;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public boolean isPending() {
    return pending;
  }

  public void setPending(boolean pending) {
    this.pending = pending;
  }

  public Timestamp getCreated() {
    return created;
  }

  public void setCreated(Timestamp created) {
    this.created = created;
  }

  public Timestamp getModified() {
    return modified;
  }

  public void setModified(Timestamp modified) {
    this.modified = modified;
  }

  public Timestamp getDeleted() {
    return deleted;
  }

  public void setDeleted(Timestamp deleted) {
    this.deleted = deleted;
  }

  public static Map<String, FieldOptions> getValidatorMap() {
    return inviteFieldOptionsMap;
  }

  private static Map<String, FieldOptions> inviteFieldOptionsMap = new HashMap<String, FieldOptions>()
  {{
    put("organization_id", FieldOptionsCommon.NUMBER);
    put("email", new FieldOptionsImpl(FieldOptionsCommon.EMAIL, -1, 150));
    put("key", FieldOptionsCommon.ANY_HTML_ESCAPE);
    put("token", FieldOptionsCommon.ANY);
  }};
}