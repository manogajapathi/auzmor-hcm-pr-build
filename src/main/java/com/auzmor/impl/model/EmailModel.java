package com.auzmor.impl.model;

public class EmailModel{
    public int template_Id;
    public String template_type;

    public int getTemplate_Id() {
        return template_Id;
    }

    public void setTemplate_Id(int template_Id) {
        this.template_Id = template_Id;
    }

    public String getTemplate_type() {
        return template_type;
    }

    public void setTemplate_type(String template_type) {
        this.template_type = template_type;
    }
}