package com.auzmor.impl.controller;

import com.auzmor.impl.handler.HolidayHandler;
import com.auzmor.impl.handler.HolidayListHandler;

import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;

public class HolidayController {
  private HolidayListHandler listHandler = new HolidayListHandler();
  private HolidayHandler holidayHandler = new HolidayHandler();

  public HolidayController(Router router) {
    router.route("/holidayList*").handler(BodyHandler.create());
    router.route("/holiday*").handler(BodyHandler.create());

    router.post("/holidayList/create").handler(listHandler::addHolidayList); // create a new list
    router.get("/holidayList/list").handler(listHandler::getAll);  // get all existing lists
    router.get("/holidayList").handler(listHandler::getHolidayList);  // get the particular list
    router.put("/holidayList/:listId").handler(listHandler::updateHolidayList);  // update the particular list
    router.delete("/holidayList/:list_id").handler(listHandler::deleteHolidayList);  // delete the particular list

    router.post("/holidayList/copy").handler(listHandler::copyHolidayList);  // copy the existing list

    router.post("/holiday/create").handler(holidayHandler::addHoliday); // create new holiday in a list
    router.get("/holiday/list").handler(holidayHandler::getAllHolidays);  // get all holidays in a list
    router.get("/holiday").handler(holidayHandler::getHoliday);  // get details of a holiday
    router.put("/holiday/:holiday_id").handler(holidayHandler::updateHoliday);  // edit details of a holiday
    router.delete("/holiday/:holiday_id").handler(holidayHandler::deleteHoliday); // delete a holiday
  }
}
