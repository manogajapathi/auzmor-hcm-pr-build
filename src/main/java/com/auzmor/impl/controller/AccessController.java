package com.auzmor.impl.controller;

import com.auzmor.impl.handler.AccessHandler;

import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;

public class AccessController {
    private AccessHandler accessHandler = new AccessHandler();

    public AccessController(Router router){
        router.route("/accessControl*").handler(BodyHandler.create());
        router.get("/accessControl/list").handler(accessHandler::getAllAccess);
        router.get("/accessControl/permissionCheck").handler(accessHandler::getCheckPermissionAllowed);
    }
}
