package com.auzmor.impl.controller;

import com.auzmor.impl.handler.FilterHandler;

import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;

public class FilterController {

  private FilterHandler filterHandler = new FilterHandler();

  public FilterController(Router router) {
    router.route("/search/filter*").handler(BodyHandler.create());
    router.get("/search/filter").handler(filterHandler::getFilterList); // Call Filter Handler
  }
}
