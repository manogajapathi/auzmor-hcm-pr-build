package com.auzmor.impl.controller;

import com.auzmor.impl.handler.ReportHandler;

import io.vertx.core.MultiMap;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

/**
 * {@link ReportController} - A Controller Class containing REST API Implementation for generating reports
 */
public class ReportController {

  private ReportHandler handlerObject = new ReportHandler();

  // ReportController constructor containing routes
  public ReportController(Router router) {
    router.get("/report/jobposting/details").handler(this::getJobPostingReportDetails);
    router.get("/report/currentStatus").handler(handlerObject::getCandidateCurrentStatusReport);
    router.get("/report/averageStatus").handler(handlerObject::getCandidateAverageStatusReport);
    router.get("/report/source").handler(handlerObject::getSourceReport);
    router.route("/report*").consumes("application/x-www-form-urlencoded")
            .produces("application/json")
            .handler(BodyHandler.create());
    router.put("/report/source/:jobId").pathRegex("/report/source/(?<jobId>\\d+)")
            .handler(handlerObject::updateJobSources);

  }

  /**
   * getJobPostingReportDetails - display job posting report details
   *
   * @param routingContext
   */
  private void getJobPostingReportDetails(RoutingContext routingContext) {
    MultiMap mapObject = (routingContext.request()).params();
    String limit = mapObject.get("limit") == null ? "" : mapObject.get("limit");
    String offset = mapObject.get("offset") == null ? "" : mapObject.get("offset");
    new ReportHandler().getJobReportDetails(limit, offset, routingContext);
  }
}
