package com.auzmor.impl.controller;

import com.auzmor.impl.handler.LoginHandler;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;

public class LoginController {

  public LoginHandler handler = new LoginHandler();

  public LoginController(Router router) {
    router.get("/login/candidatecheck").handler(LoginHandler::candidateLoginCheck);
    router.route("/login*").handler(BodyHandler.create());
    //router.post("/login/check").handler(handler::checkLogin);
    router.route("/signup*").handler(BodyHandler.create());
    router.post("/signup/create").handler(handler::registerUser);
    router.post("/login/resetpwd").handler(handler::resetPwd);
    router.get("/login/checkReset/:token").handler(handler::checkResetPwd);
    router.get("/login/user/roles").handler(LoginHandler::getLoggedInUserRoles);
    router.post("/login/updatepwd").handler(handler::updatePassword);
    router.put("/signup/approve").handler(handler::approveUserSignup);
    router.put("/signup/ats/approve").handler(handler::approveUserSignup);
    router.put("/login/changePassword").handler(handler::updatePasswordForReset);
    router.post("/login/authenticate").handler(LoginHandler::authenticate);
  }
}