package com.auzmor.impl.controller;

import com.auzmor.impl.handler.AnnouncementHandler;

import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;

public class AnnouncementController {
    private AnnouncementHandler handler = new AnnouncementHandler();
    public AnnouncementController(Router router){
        router.route("/announcement*").handler(BodyHandler.create());
        router.post("/announcement/create").handler(handler::addAnnouncement);
        router.get("/announcement").handler(handler::getAllAnnouncements);
        router.delete("/announcement/:id").handler(handler::deleteAnnouncement);
        router.get("/announcement/:id").handler(handler::getAnnouncementId);
        router.put("/announcement/:announcementId").handler(handler::updateAnnouncement);
    }
}
