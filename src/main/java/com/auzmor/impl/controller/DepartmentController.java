package com.auzmor.impl.controller;

import com.auzmor.impl.configuration.MysqldbConf;
import com.auzmor.impl.model.DepartmentModel;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.impl.util.ModelUtil;

import io.vertx.core.MultiMap;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class DepartmentController {

    DepartmentModel departmentModel = new DepartmentModel();

    public DepartmentController(Router router){
        router.get("/department/list").handler(this::getAll);
        //router.get("/department/list/").handler(this::getAllOrganization);
        router.route("/department*").handler(BodyHandler.create());
        router.post("/department/create").handler(this::addDepartment);
        router.get("/department/:id").handler(this::getDepartment);
        router.put("/department/:id").handler(this::updateDepartment);
        router.delete("/department/:id").handler(this::deleteDepartment);
    }

    private void addDepartment(RoutingContext routingContext) {
        // Read the request's content and create an instance of Data.
        MultiMap params = routingContext.request().params();
        JsonObject responseObject = new JsonObject();
        JsonObject errorObject = new JsonObject();
        JsonObject dataObject = new JsonObject();
        try {
        for (String key : params.names()) {
                ModelUtil.setValues(key,params.get(key),departmentModel);

        }
        // Add it to the backend map

            if(departmentModel.getDepartment().equals("")||departmentModel.getDepartment().equals(null)
                    ||departmentModel.getOrganizationId()==0){
                throw new Exception("Required Params are missing");
            }
            departmentModel.setActiveStatus("true");
           // departmentModel.add(routingContext, departmentModel);


            dataObject.put("message","Department created successfully!");
            dataObject.put("id",departmentModel.getId());

        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            errorObject.put("code",400);
            errorObject.put("message","Invalid Data");
            errorObject.put("description",e.getMessage());
            responseObject.put("error",errorObject);
            routingContext.response()
                .setStatusCode(400)
                .putHeader("Content-Type","application/json; charset=utf-8")
                .end(responseObject.toString());
        } catch (SQLException e) {
            e.printStackTrace();
            errorObject.put("code",400);
            errorObject.put("message","Invalid Data");
            errorObject.put("description",e.getMessage());
            responseObject.put("error",errorObject);
            routingContext.response()
                    .setStatusCode(400)
                    .putHeader("Content-Type","application/json; charset=utf-8")
                    .end(responseObject.toString());
        } catch (Exception e) {
            e.printStackTrace();
            errorObject.put("code",400);
            errorObject.put("message","Invalid Data");
            errorObject.put("description",e.getMessage());
            responseObject.put("error",errorObject);
            routingContext.response()
                    .setStatusCode(400)
                    .putHeader("Content-Type","application/json; charset=utf-8")
                    .end(responseObject.toString());
        }
        // Return the created data as JSON
        routingContext.response()
                .setStatusCode(201)
                .putHeader("Content-Type", "application/json; charset=utf-8; id=" + departmentModel.getId())
                .end("{\"code\":201,\"status\": \"success\", \"data\": " +dataObject+"}");

    }

    private void getDepartment(RoutingContext routingContext) {
        String id = routingContext.request().getParam("id");
        JsonObject responseObject = new JsonObject();
        JsonObject errorObject = new JsonObject();
        JsonObject jsonSuccessObject = new JsonObject();
        if (id == null) {
            errorObject.put("code",400);
            errorObject.put("message","Invalid Data");
            errorObject.put("description","Data Not Found");
            responseObject.put("error",errorObject);
            routingContext.response().setStatusCode(404).end(responseObject.toString());
        } else {
            try {
                final Integer idAsInteger = Integer.valueOf(id);
               // departmentModel.Select(idAsInteger);
                if (departmentModel == null) {
                    errorObject.put("code",400);
                    errorObject.put("message","Invalid Data");
                    errorObject.put("description","Data Not Found");
                    responseObject.put("error",errorObject);
                    routingContext.response().setStatusCode(404).end(responseObject.toString());
                } else {
                    routingContext.response()
                            .setStatusCode(200)
                            .putHeader("Content-Type","application/json; charset=utf-8")
                            .end("{\"code\":200,\"status\": \"success\", \"data\": " +Json.encodePrettily(departmentModel)+"}");
                }
            } catch (Exception e) {
                e.printStackTrace();
                errorObject.put("code",400);
                errorObject.put("message","Invalid Data");
                errorObject.put("description",e.getMessage());
                responseObject.put("error",errorObject);
                routingContext.response()
                        .setStatusCode(400)
                        .putHeader("Content-Type","application/json; charset=utf-8")
                        .end(responseObject.toString());
            }
        }
    }

    /**
     * last modified by Pradeep Sudhakaran
     * @param routingContext
     * @modified_date 12-12-2017
     */
    private void updateDepartment(RoutingContext routingContext) {
        final String id = routingContext.request().getParam("id");
        JsonObject responseObject = new JsonObject();
        JsonObject errorObject = new JsonObject();
        JsonObject successObject = new JsonObject();
        if (id == null) {
            errorObject.put("code",400);
            errorObject.put("message","Invalid Data");
            errorObject.put("description","Data Not Found");
            responseObject.put("error",errorObject);
            routingContext.response().setStatusCode(404).end(responseObject.toString());
        } else {
            final Integer idAsInteger = Integer.valueOf(id);
            MultiMap params = routingContext.request().params();
            for (String key : params.names()) {
                try {
                    //departmentModel.set(key, params.get(key));
                    ModelUtil.setValues(key,params.get(key),departmentModel);
                } catch (Exception e) {
                    e.printStackTrace();
                    errorObject.put("code",400);
                    errorObject.put("message","Invalid Data");
                    errorObject.put("description",e.getMessage());
                    responseObject.put("error",errorObject);
                    routingContext.response()
                            .setStatusCode(400)
                            .putHeader("Content-Type","application/json; charset=utf-8")
                            .end(responseObject.toString());
                }
            }
            try {
                if(departmentModel.getDepartment().equals("")||departmentModel.getDepartment().equals(null)
                        ||departmentModel.getOrganizationId()==0){
                    throw new Exception("Required Params are missing");
                }
               // departmentModel.update(idAsInteger, departmentModel,routingContext);

            } catch (NoSuchFieldException e) {
                e.printStackTrace();
                errorObject.put("code",400);
                errorObject.put("message","Invalid Data");
                errorObject.put("description",e.getMessage());
                responseObject.put("error",errorObject);
                routingContext.response()
                        .setStatusCode(400)
                        .putHeader("Content-Type","application/json; charset=utf-8")
                        .end(responseObject.toString());
            } catch (SQLException e) {
                e.printStackTrace();
                errorObject.put("code",400);
                errorObject.put("message","Invalid Data");
                errorObject.put("description",e.getMessage());
                responseObject.put("error",errorObject);
                routingContext.response()
                        .setStatusCode(400)
                        .putHeader("Content-Type","application/json; charset=utf-8")
                        .end(responseObject.toString());
            } catch (Exception e) {
                e.printStackTrace();
                errorObject.put("code",400);
                errorObject.put("message","Invalid Data");
                errorObject.put("description",e.getMessage());
                responseObject.put("error",errorObject);
                routingContext.response()
                        .setStatusCode(400)
                        .putHeader("Content-Type","application/json; charset=utf-8")
                        .end(responseObject.toString());
            }
            successObject.put("message","Department updated successfully");

            responseObject.put("code",202);
            responseObject.put("status",true);
            responseObject.put("data",successObject);
            routingContext.response()
                    .setStatusCode(202)
                    .putHeader("Content-Type","application/json; charset=utf-8")
                    .end(responseObject.toString());
        }
    }

    /**
     * last modified by Pradeep Sudhakaran
     * @param routingContext @modified_date 12-12-2017
     */
    private void deleteDepartment(RoutingContext routingContext) {
        String id = routingContext.request().getParam("id");
        JsonObject responseObject = new JsonObject();
        JsonObject errorObject = new JsonObject();

        if (id == null) {
            errorObject.put("code",406);
            errorObject.put("message","Invalid Data");
            errorObject.put("description","Data Not Found");
            responseObject.put("error",errorObject);
            routingContext.response().setStatusCode(404).end(responseObject.toString());
        } else {
            try {

              //  departmentModel=departmentModel.delete(Integer.valueOf(id));
            } catch (Exception e) {
                e.printStackTrace();
                errorObject.put("code",406);
                errorObject.put("message","Invalid Data");
                errorObject.put("description",e.getMessage());
                responseObject.put("error",errorObject);
                routingContext.response()
                        .setStatusCode(400)
                        .putHeader("Content-Type","application/json; charset=utf-8")
                        .end(responseObject.toString());
            }
            JsonObject jsonSuccessObject = new JsonObject();

            jsonSuccessObject.put("message",departmentModel.getDepartment()+" deleted Successfully");

            responseObject.put("code",200);
            responseObject.put("success",true);
            responseObject.put("data",jsonSuccessObject);
            routingContext.response()
                    .setStatusCode(200)
                    .putHeader("Content-Type","application/json; charset=utf-8")
                    .end(responseObject.toString());
        }
    }
    /**
     * last modified by Pradeep Sudhakaran
     * @param routingContext @modified_date 12-12-2017
     */
    private void getAll(RoutingContext routingContext) {
        // Write the HTTP response
        // The response is in JSON using the utf-8 encoding
        // We returns the list of bottles
        JsonObject responseObject = new JsonObject();
        try {
            //final Integer idAsInteger = Integer.valueOf(id);
            //JobModel jobModel = this.jobData.get(idAsInteger);
            MultiMap mapObject = (routingContext.request()).params();
            String id = mapObject.get("organization_id")==null?"":mapObject.get("organization_id");
            List<DepartmentModel> departments = null;
           // departments = departmentModel.SelectAll(id);
            if (departmentModel == null) {
                routingContext.response().setStatusCode(404).end("{\"status\": \"failure\",  \"message\":\"Client Error\"}");
            } else {
                JsonObject jsonObject = new JsonObject();
                Connection connection = new MysqldbConf().getConnection();

                for(DepartmentModel department:departments) {
                    String formQueryString = "select count(department) as count from Employee where department = '"+department.getDepartment()
                        +"' and organization_id = "+id;
                ResultSet resultSet = DatabaseUtil.getResultSetForSelectQuery(connection, formQueryString);
                int count = 0;
                while(resultSet.next()){
                    count = resultSet.getInt(1);
                }
                jsonObject.put(department.getDepartment(),count);
            }
            connection.close();
                routingContext.response()
                        .setStatusCode(200)
                        .putHeader("Content-Type","application/json; charset=utf-8")
                        .end("{\"code\":200,\"status\": \"success\", \"data\": " +Json.encodePrettily(departments)+",\"department_count\":"+jsonObject+"}");
            }
        } catch (SQLException e) {
            routingContext.response()
                    .setStatusCode(400)
                    .putHeader("Content-Type","application/json; charset=utf-8")
                    .end("{\"code\":400,\"status\": \"failure\",  \"message\":\"" +e.getMessage()+ "\"}");
        } catch (Exception e){
            routingContext.response()
                    .setStatusCode(400)
                    .putHeader("Content-Type","application/json; charset=utf-8")
                    .end("{\"code\":400,\"status\": \"failure\",  \"message\":\"" +e.getMessage()+ "\"}");
        }
    }


    private void getAllOrganization(RoutingContext routingContext) {
        // Write the HTTP response
        // The response is in JSON using the utf-8 encoding
        // We returns the list of bottles
        try {
            //final Integer idAsInteger = Integer.valueOf(id);
            //JobModel jobModel = this.jobData.get(idAsInteger);
           // String id = routingContext.request().getParam("organization_id");
            List<DepartmentModel> departments = null;
          //  departments = departmentModel.SelectAll();
            if (departmentModel == null) {
                routingContext.response().setStatusCode(404).end("{\"status\": \"failure\",  \"message\":\"Client Error\"}");
            } else {
                routingContext.response()
                        .setStatusCode(200)
                        .putHeader("Content-Type","application/json; charset=utf-8")
                        .end("{\"code\":200,\"status\":\"success\",\"status\": \"success\", \"data\": "+Json.encodePrettily(departments)+"}");
            }
        } catch (Exception e) {
            routingContext.response()
                .setStatusCode(400)
                .putHeader("Content-Type", "application/json; charset=utf-8")
                .end("{\"status\": \"failure\",  \"message\":\"" + e.getMessage() + "\"}");
        }

    }
}
