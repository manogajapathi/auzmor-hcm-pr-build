package com.auzmor.impl.controller;

import com.auzmor.impl.handler.JobHandler;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;

/**
 * JobController    A Controller class which contains job request with routers
 */

public class JobController {

  private JobHandler handlerObject = new JobHandler();

  public JobController(Router router) {
    router.get("/job").handler(handlerObject::getJobDetails);
    router.get("/job/collaborator/list").handler(handlerObject::getCollaboratorList);
    router.get("/job/filter").handler(handlerObject::getJobBasedOnFilters);
    router.route("/job*").produces("application/json")
        .consumes("application/x-www-form-urlencoded").handler(BodyHandler.create());
    router.post("/job/create").handler(handlerObject::addJob);
    router.put("/job/:id").pathRegex("/job/(?<id>\\d+)")
        .handler(handlerObject::updateJobDetails);
    router.delete("/job/:id").pathRegex("/job/(?<id>\\d+)")
        .handler(handlerObject::deleteJobDetails);
    router.put("/job/archive/:id").pathRegex("/job/archive/(?<id>\\d+)")
            .handler(handlerObject::archiveJobBasedOnId);
    router.post("/job/clone").handler(handlerObject::cloneJob);
    router.get("/job/list/employee/:employeeId").pathRegex("/job/list/employee/(?<employeeId>\\d+)")
        .handler(handlerObject::getJobForEmployee);
  }

}



