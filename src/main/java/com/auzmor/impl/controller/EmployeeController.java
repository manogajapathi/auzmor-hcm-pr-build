package com.auzmor.impl.controller;

import com.auzmor.impl.constant.file.upload.UploadConstant;
import com.auzmor.impl.handler.EmployeeHandler;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;

public class EmployeeController {

  EmployeeHandler handlerObject = new EmployeeHandler();

  public EmployeeController(Router router) {
    router.get("/employee/").handler(handlerObject::getAllEmployees);
    router.get("/employee/listBasedOnIndex/:id").pathRegex("/employee/listBasedOnIndex/(?<id>\\d+)")
        .handler(handlerObject::getEmployeeListBasedOnIndex);
    router.get("/employee/list").handler(handlerObject::getEmployeeOnboardingStatus);
    router.get("/employee/lastemployeeno").handler(handlerObject::getLastEmployeeNo);
    router.get("/employee/:id").pathRegex("/employee/(?<id>\\d+)")
        .handler(handlerObject::getEmployee);
    /*router.get("/TeamEmployee/:employeeId").pathRegex("/TeamEmployee/(?<employeeId>\\d+)")
        .handler(handlerObject::getTeam);*/
    /*router.get("/OrganizationChart/:id").pathRegex("/OrganizationChart/(?<id>\\d+)")
        .handler(handlerObject::getOrganizationChart);*/

    router.route("/employee*").produces("application/json")
        .consumes("application/x-www-form-urlencoded").handler(BodyHandler.create());
    router.post("/employee/create").handler(handlerObject::addEmployee);
    router.put("/employee/:employeeId").pathRegex("/employee/(?<employeeId>\\d+)")
        .handler(handlerObject::updateEmployee);
    router.delete("/employee/:id").pathRegex("/employee/(?<id>\\d+)")
        .handler(handlerObject::deleteEmployee);
    router.put("/employee/terminate/:employeeId").pathRegex("/employee/terminate/(?<employeeId>\\d+)")
        .handler(handlerObject::addEmployeeTermination);
    router.put("/employee/sendonboardingpacketemployee/").
        handler(handlerObject::updateOnboardingPacket);
    router.get("/admin/employee/fieldoption").handler(handlerObject::getFieldOption);
    router.get("/employee/getonboardingstatusforemployee/:employeeId")
        .pathRegex("/employee/getonboardingstatusforemployee/(?<employeeId>\\d+)")
        .handler(handlerObject::getStatusForEmployee);
    router.post("/employee/profile/image").handler(handlerObject::addEmployeeImage);
    router.get("/employee/ats/viewuser").handler(handlerObject::viewUserList);
    router.post("/employee/ats/adduser").handler(handlerObject::addUser);
    router.delete("/employee/ats/deleteuser/:employeeId").pathRegex("/employee/ats/deleteuser/(?<employeeId>\\d+)")
        .handler(handlerObject::deleteUser);
    router.put("/employee/ats/edituser").handler(handlerObject::editUser);

    router.route("/employee/ats/uploadprofileimage/").consumes("multipart/form-data")
        .produces("application/json").handler(BodyHandler.create(UploadConstant.UPLOAD_DIRECTORY)
        .setMergeFormAttributes(true));
    router.post("/employee/ats/uploadprofileimage/").handler(handlerObject::uploadProfileImage);

    router.post("/employee/uploadfile/*").consumes("multipart/form-data")
        .produces("multipart/form-data").handler(BodyHandler.create(UploadConstant.UPLOAD_DIRECTORY)
        .setMergeFormAttributes(true));
    router.post("/employee/uploadfile").handler(handlerObject::uploadFilesForEmployee);
    router.post("/employee/document/upload").handler(handlerObject::uploadEmployeeDocument);
    router.post("/employee/task/upload").handler(handlerObject::uploadTaskDocuments);

    router.get("/employee/dashboard/count/:employeeId").pathRegex("/employee/dashboard/count/(?<employeeId>\\d+)")
        .handler(handlerObject::getDashBoardCount);
  }
}