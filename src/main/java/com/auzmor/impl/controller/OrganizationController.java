package com.auzmor.impl.controller;

import com.auzmor.impl.builder.ApiBuilder;
import com.auzmor.impl.handler.OrganizationHandler;

import com.auzmor.impl.constant.file.upload.*;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.Organization;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import org.apache.http.HttpStatus;

public class OrganizationController extends ApiBuilder {

  OrganizationHandler handlerObject = new OrganizationHandler();

  // Controller for Creating, listing and checking the Organizations
  public OrganizationController(Router router) {
    router.get("/organization/setting").handler(handlerObject::addOrganizationSettingOptions);
    // router.get("/organization/list").handler(handlerObject::getAllOrganization);
    router.get("/organization/check").handler(handlerObject::getOrganizationStatus);
    router.get("/organization").handler(handlerObject::getOrganizationDetails);
    router.get("/organization/count").handler(handlerObject::getOrganizationCount);
    router.get("/organization/defaultCurrency").pathRegex("/organization/defaultCurrency").consumes("").produces("application/json").handler(
        routingContext -> ResponseHelper.writeSuccessResponse(HttpStatus.SC_OK, "Default currency of organization",
            new JsonObject().put("defaultCurrency", ((Organization)routingContext.data().get("organization")).getDefault_currency()),
            routingContext.response())
    );
    router.get("/organization/urlandlogo/:id").pathRegex("/organization/urlandlogo/(?<id>\\d+)")
        .handler(OrganizationHandler::getOrganizationUrlAndLogo);
    router.get("/organization/domain/availabilityCheck").pathRegex("/organization/domain/availabilityCheck" +
        "")
        .handler(OrganizationHandler::checkAvailability);
    router.delete("/organization/:id").pathRegex("/organization/(?<id>\\d+)")
        .handler(handlerObject::deleteOrganization);

    // File Upload Body Handler
    router.post("/organization/images/*").handler(BodyHandler
        .create(UploadConstant.UPLOAD_DIRECTORY).setMergeFormAttributes(true));
    router.post("/organization/images/favicon").blockingHandler(handlerObject::addOrganizationImageFavicon);
    router.post("/organization/images/logo").blockingHandler(handlerObject::addOrganizationImage);
    router.post("/organization/*").handler(BodyHandler
        .create(UploadConstant.UPLOAD_DIRECTORY).setDeleteUploadedFilesOnEnd(true));
    router.post("/organization/:organizationId/crtfile").blockingHandler(handlerObject::uploadOrganizationCertFile);
    router.post("/organization/attachment/keystorefile").blockingHandler(handlerObject::uploadOrganizationKeyCertFile);

    router.route("/organization/*").handler(BodyHandler
        .create(UploadConstant.UPLOAD_DIRECTORY).setDeleteUploadedFilesOnEnd(true));
    router.post("/organization/create").handler(handlerObject::addOrganization);
    router.put("/organization/:organizationId").handler(handlerObject::updateOrganization);
    router.put("/organization/:organizationId/updateDomains").pathRegex("/organization/(?<organizationId>\\d+)/updateDomains")
        .blockingHandler(OrganizationHandler::updateDomains);
  }
}