package com.auzmor.impl.controller;

import com.auzmor.impl.handler.DemoHandler;

import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;

public class DemoController {
    DemoHandler handlerObject = new DemoHandler();

    public DemoController(Router router){
        router.route("/demo*").handler(BodyHandler.create());
        router.post("/demo/create").handler(handlerObject::addDemoPacket);
        router.get("/demo").handler(handlerObject::getDemoPacket);
        router.put("/demo/:form_id").handler(handlerObject::updateDemoForm);
//        router.put("/demo/:candidate_id").handler(handlerObject::updateOfferprocess);
    }
}
