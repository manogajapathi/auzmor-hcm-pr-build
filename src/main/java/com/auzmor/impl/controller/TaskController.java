package com.auzmor.impl.controller;

import com.auzmor.impl.handler.TaskHandler;

import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;

public class TaskController {

    TaskHandler taskHandler = new TaskHandler();

    public TaskController(Router router){

        router.route("/task*").handler(BodyHandler.create());
        router.post("/task/create").handler(taskHandler::addtaskcontroller);
        router.get("/task").handler(taskHandler::gettaskcontroller);
        router.put("/task/update").handler(taskHandler::updatetaskcontroller);
        router.delete("/task/delete/:task_id").handler(taskHandler::deletetaskcontroller);

        //------------------- checking for project in task ----------------------
        router.route("/task/project*").handler(BodyHandler.create());
        router.post("/task/project/create").handler(taskHandler::addtaskprojectcontroller);
        router.get("/task/project").handler(taskHandler::gettaskprojectcontroller);

        router.get("/task/project/list").handler(taskHandler::gettaskprojectlistcontroller);
        router.put("/task/project/update").handler(taskHandler::updatetaskprojectcontroller);
        router.delete("/task/project/delete").handler(taskHandler::deletetaskprojectcontroller);

        router.get("/task/project/fieldoption").handler(taskHandler::getfieldOption);


    }
}
