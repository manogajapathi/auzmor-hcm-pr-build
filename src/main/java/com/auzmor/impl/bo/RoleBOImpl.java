package com.auzmor.impl.bo;

import com.auzmor.bo.RoleBO;
import com.auzmor.dao.RoleDAO;
import com.auzmor.impl.dao.RoleDAOImpl;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.Role;
import com.auzmor.util.db.DbHandle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;

public class RoleBOImpl implements RoleBO {
  private static final Logger logger = LoggerFactory.getLogger(RoleBOImpl.class);
  private DbHandle dbHandle;
  public  RoleDAO roleDAO;

  public RoleBOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  @Override
  public Role getByName(String name, long organizationId) throws SQLException, DbHandleException {
    return new RoleDAOImpl(dbHandle).getRoleByName(name, organizationId);
  }

  public RoleDAO getRoleDAO() {
    return roleDAO;
  }
  public void setRoleDAO(RoleDAO roleDAO) {
    this.roleDAO = roleDAO;
  }
}
