package com.auzmor.impl.bo;

import com.auzmor.bo.EmployeeBO;
import com.auzmor.bo.LoginBO;
import com.auzmor.dao.EmployeeDAO;
import com.auzmor.dao.JobDAO;
import com.auzmor.dao.OrganizationDAO;
import com.auzmor.impl.dao.*;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.KongServiceHelper;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;


public class EmployeeBOImpl implements EmployeeBO {
  private static Logger logger = LoggerFactory.getLogger(EmployeeBOImpl.class);

  private DbHandle dbHandle;
  private EmployeeDAO employeeDAO;

  public EmployeeBOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  /**
   * @param paramsMap
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Charles Sam Dilip
   * @lastModifiedDate 07/FEB/2018
   */
  @Override
  public Employee addNewEmployee(MultiMap paramsMap) throws SQLException, DbHandleException {

    logger.info("Add a new employee");
    String personalEmail = paramsMap.get("personal_email");
    Long organizationId = Long.parseLong(paramsMap.get("organization_id")); // TODO set in the employee organization id once done.
    Employee employee = new Employee();
    if (!(new EmployeeDAOImpl(dbHandle).isEmployeeExistsInOrganization(paramsMap.get("work_email"), organizationId))) {

      employee.setFirst_name(paramsMap.get("first_name"));
      employee.setLast_name(paramsMap.get("last_name"));
      employee.setPersonal_email(personalEmail);
      employee.setWork_email(paramsMap.get("work_email"));
      employee.setJob_title(paramsMap.get("job_title"));
      employee.setManager(paramsMap.get("manager"));
      employee.setLocation(paramsMap.get("location"));
      employee.setPay_rate(paramsMap.get("pay_rate"));
      employee.setPay_type(paramsMap.get("pay_type"));
      employee.setEmployment_status(paramsMap.get("employment_status"));
      employee.setPrimary_phone(paramsMap.get("primary_phone"));
      employee.setStatus(paramsMap.get("status"));
      employee.setOrganization_id(Integer.parseInt(paramsMap.get("organization_id")));
      employee.setHire_date(paramsMap.get("hire_date"));
      employee.setOnboarding_status(paramsMap.get("onboarding_status"));
      employee.setAccess_level(paramsMap.get("access_level"));

      paramsMap.set("user_type", "employee");
      LoginBO loginBO = new LoginBOImpl(this.dbHandle);
      employee.setUser_id((int) loginBO.addNewUser(paramsMap));
      if (employee.getUser_id() < 1) {
        return null;
      }

      EmployeeDAOImpl employeeDAO = new EmployeeDAOImpl(this.dbHandle);
      employee.setEmployee_id(employeeDAO.create(employee));
    } else {
      logger.info("Employee already exists in organization");
      return null;
    }
    return employee;
  }

  /**
   * checks the employee is on the organization
   *
   * @param email
   * @param organizationId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Charles Sam Dilip
   * @lastModifiedDate 07/FEB/2018
   */
  @Override
  public boolean isEmployeeExists(String email, long organizationId) throws SQLException,
      DbHandleException {

    EmployeeDAOImpl employeeDAO = new EmployeeDAOImpl(this.dbHandle);
    if (employeeDAO.isEmployeeExistsInOrganization(email, organizationId)) {
      return true;
    }
    return false;
  }

  @Override
  public JsonArray getAllEmployees(MultiMap paramsMap)
      throws SQLException, DbHandleException {

    EmployeeDAOImpl employeeDAO = new EmployeeDAOImpl(this.dbHandle);
    JsonArray resultArray = employeeDAO.getAllEmployees(paramsMap);

    return resultArray;
  }

  @Override
  public Employee getEmployeeDetails(long employeeId) throws SQLException, DbHandleException {
    return new EmployeeDAOImpl(dbHandle).getEmployeeDetails(employeeId);
  }

  @Override
  public Employee getEmployeeDetailsByLoginId(long loginId) throws SQLException, DbHandleException {
    return new EmployeeDAOImpl(dbHandle).getEmployeeDetailsByLoginId(loginId);
  }

  public int addNewUser(MultiMap paramsMap) throws SQLException, DbHandleException {
    EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
    int addId = employeeDAO.addNewUser(paramsMap);
    return addId;
  }

  @Override
  public int uploadProfileImage(String imagePath, long organizationId, int employeeId)
      throws SQLException, DbHandleException {
    EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
    int uploadedCount = employeeDAO.uploadProfileImage(imagePath,organizationId,employeeId);
    return uploadedCount;
  }

  @Override
  public List<HashMap<String, String>> getEmployeeEmailIds(long collaboratorId, long adminId)
      throws SQLException, DbHandleException {
    EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
    List<HashMap<String, String>> getEmailDetails;
    getEmailDetails = employeeDAO.getEmployeeEmailIds(collaboratorId, adminId);

    return getEmailDetails;
  }

  public int deleteUser(MultiMap paramsMap) throws SQLException, DbHandleException {
    int organizationId = Integer.valueOf(paramsMap.get("organization_id"));
    int employeeId = Integer.valueOf(paramsMap.get("employeeId"));
    EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
    int deletedCount = employeeDAO.deleteUser(organizationId, employeeId);
    return deletedCount;
  }

  public JsonArray getAllUsersList(MultiMap paramsMap)
      throws SQLException, DbHandleException {

    EmployeeDAOImpl employeeDAO = new EmployeeDAOImpl(this.dbHandle);
    JsonArray resultArray = employeeDAO.getAllUsersList(paramsMap);
    return resultArray;
  }

  public int editUser(Vertx vertx, MultiMap params) throws SQLException, DbHandleException {
    logger.info("Edit a user");
    EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
    OrganizationDAO organizationDAO = new OrganizationDAOImpl(dbHandle);
    int editedRowsCount = 0;
    Organization organization = new Organization();
    String orgAdminMailId = "";
    int organizationId = Integer.valueOf(params.get("organization_id"));
    int employeeId = Integer.valueOf(params.get("employee_id"));
    String workEmail = params.get("work_email");
    String userType = params.get("access_level");
    Employee employee = employeeDAO.getWorkEmailAndAccessLevel(employeeId, organizationId);

    organization = organizationDAO.getOrganizationById((long) organizationId);
    orgAdminMailId = organization.getAccount_owner_email();

    // Employee and organization update in all scenerios
    if((null != orgAdminMailId) && (!orgAdminMailId.isEmpty()) && (!employee.getWork_email().isEmpty())) {
      if (!employee.getWork_email().equals(workEmail) && (employeeDAO.isEmployeeExistsInOrganization(workEmail,
          (long) organizationId))) {
        return -1;
      }
      logger.info("updating User info");
      if (("admin").equals(userType) && orgAdminMailId.equals(employee.getWork_email())) {
        logger.info("Updating owner email for the organization");
        employeeDAO.updateOrganizationDetails(organizationId, workEmail);
      }
      editedRowsCount = employeeDAO.editUser(params);
      
      if (!userType.equals(employee.getAccess_level())) {
        logger.info("Updating the change of role");
        String consumerId = new LoginDAOImpl(dbHandle).getConsumerId(employee.getUser_id());
        String oldRole =  "" + (new RoleDAOImpl(dbHandle).getRoleByName(employee.getAccess_level().equals("admin") ?
                "Admin" : "Employee", organizationId).getId());
        String role = "" + (new RoleDAOImpl(dbHandle).getRoleByName(userType.equals("admin") ? "Admin" : "Employee",
            organizationId).getId());
        HttpClient kongClient = KongServiceHelper.executeWithKongClient(vertx);
        KongServiceHelper.deleteConsumerRole(kongClient, consumerId, oldRole, httpClientResponse -> {
          KongServiceHelper.editConsumerRoles(kongClient, consumerId, role,
              httpClientEditResponse -> {
              });
        });
      }
    } else {
      return 0;
    }
    return editedRowsCount;
  }

  public JsonObject getDashBoardCount(MultiMap requestParams) throws SQLException, DbHandleException {
    logger.info("Employee id for count :" + requestParams.get("employeeId"));
    Employee employee = new Employee();
    employee.setEmployee_id(Integer.valueOf(requestParams.get("employeeId")));
    EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
    employee = employeeDAO.getEmployeeDetails(employee.getEmployee_id());
    employee.setEmployee_id(Integer.valueOf(requestParams.get("employeeId")));
    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    long jobCount = jobDAO.getJobCount(employee.getOrganization_id());
    JsonObject dataObject = new JsonObject();

    if (jobCount == 0) {
      dataObject.put("job_count", 1);
      dataObject.put("candidate_count", 1);
      dataObject.put("task_count", 1);
      dataObject.put("interview_count", 1);
      dataObject.put("dummy_data", true);
    } else {
      dataObject = employeeDAO.getDashBoardCount(employee);
    }
    return dataObject;
  }


  public EmployeeDAO getEmployeeDAO() {
    return employeeDAO;
  }

  public void setEmployeeDAO(EmployeeDAO employeeDAO) {
    this.employeeDAO = employeeDAO;
  }

//
////   public JsonArray getEmployee(long employeeId)
////            throws SQLException, DbHandleException{
////        EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
////
////        JsonArray employee = employeeDAO.getEmployee(employeeId);
////        return employee;
//
//    }
}

