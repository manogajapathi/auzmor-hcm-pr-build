package com.auzmor.impl.bo;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auzmor.bo.LoginBO;
import com.auzmor.dao.EmployeeDAO;
import com.auzmor.dao.LoginDAO;
import com.auzmor.dao.OrganizationDAO;
import com.auzmor.impl.dao.EmployeeDAOImpl;
import com.auzmor.impl.dao.LoginDAOImpl;
import com.auzmor.impl.dao.OrganizationDAOImpl;
import com.auzmor.impl.enumarator.bo.LoginBOEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.Login;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.util.AESUtil;
import com.auzmor.impl.util.JWTUtil;
import com.auzmor.impl.util.Mail;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * @author Charles Sam Dilip
 * @lastModifiedDate 05/FEB/2018
 */
public class LoginBOImpl implements LoginBO {
  private static Logger logger = LoggerFactory.getLogger(LoginBOImpl.class);

  private DbHandle dbHandle;
  private LoginDAOImpl loginDAO;

  public LoginBOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  public void setLoginDAO(LoginDAOImpl loginDAO) {
    this.loginDAO = loginDAO;
  }

  @Override
  public long addNewUser(MultiMap requestParams)
      throws SQLException, DbHandleException {

    logger.info("Add a new user in Login");
    Login login = new Login();
    login.setFirstName(requestParams.get("first_name"));
    login.setLastName(requestParams.get("last_name"));
    login.setEmail(requestParams.get("email"));
    login.setUserType(requestParams.get("user_type"));
    // TODO conver int id to long
    login.setOrganizationId(Integer.parseInt(requestParams.get("organization_id")));
    login.setPassword(requestParams.get("password"));

    login.setPassword(AESUtil.getAESEncyptString(login.getPassword()));
    LoginDAO loginDAO = new LoginDAOImpl(this.dbHandle);
    return loginDAO.create(login);
  }

  @Override
  public int addNewUser(Login login) throws SQLException, DbHandleException {
    return new LoginDAOImpl(dbHandle).create(login);
  }

  @Override
  public void updateConsumerId(String consumerId, long loginId) throws SQLException,
      DbHandleException {
    new LoginDAOImpl(this.dbHandle).updateConsumerId(consumerId, loginId);
  }

  public boolean isUserExists(String email, long organizationId) throws SQLException,
      DbHandleException {
    logger.info("Check user already exists " + email);
    LoginDAO loginDAO = new LoginDAOImpl(this.dbHandle);
    if (loginDAO.isLoginAlreadyExists(email, organizationId)) {
      return true;
    }
    logger.info("User not found");
    return false;
  }

  public LoginBOEnum changePassword(MultiMap requestParams) throws SQLException, DbHandleException {
    logger.info("Changing user password");
    LoginDAO loginDAO = new LoginDAOImpl(this.dbHandle);
    String oldPassword = StringUtil.printValidString(requestParams.get("old_password"));
    String password = StringUtil.printValidString(requestParams.get("password"));
    String confirmPassword = StringUtil.printValidString(requestParams.get("confirm_password"));
    String userType = StringUtil.printValidString(requestParams.get("user_type"));
    int loginId = Integer.valueOf(StringUtil.printValidString(requestParams.get("id")));

    if (!userType.equals("candidate")) {
      logger.info("User not a candidate, get user id match");
      EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
      int empLoginId = employeeDAO.getUserIdForEmployee(loginId);
      if (loginId > 0) {
        loginId = empLoginId;
      } else {
        logger.info("password incorrect");
        return LoginBOEnum.PASSWORD_INCORRECT;
      }
    }

    if (!password.equals(confirmPassword)) {
      logger.info("Confirm password match failed");
      return LoginBOEnum.PASSWORD_MATCH_FAILED;
    }

    if (loginDAO.isOldPasswordCorrect(oldPassword, loginId)) {
      logger.info("Updating password");
      int updateFlag = loginDAO.updatePassword(confirmPassword, loginId);
      if (updateFlag > 0) {
        logger.info("password changed");
        return LoginBOEnum.PASSWORD_CHANGED;
      } else {
        logger.info("no change in password");
        return LoginBOEnum.PASSWORD_NOCHANGE;
      }
    } else {
      logger.info("Incorrect old password");
      return LoginBOEnum.PASSWORD_INCORRECT;
    }
  }

  public LoginBOEnum approveEmployeeSignup(MultiMap params) throws SQLException, DbHandleException {
    LoginDAO loginDAO = new LoginDAOImpl(dbHandle);
    logger.info("approve signup details");
    LoginBOEnum loginBOEnum = loginDAO.approveEmployeeSignup(params);
    return loginBOEnum;
  }


  @Override
  public Login validateCredentialsAndGetDetails(String email, String password, long organizationId)
      throws SQLException, DbHandleException {
    return new LoginDAOImpl(this.dbHandle).isValidCredentialAndGetLogin(email, password,
        organizationId);
  }

  @Override
  public Login getLoginById(long loginId)
      throws SQLException, DbHandleException {
    return new LoginDAOImpl(this.dbHandle).getLoginUserDetails(loginId);
  }

  @Override
  public boolean candidateLoginCheck(Login login) throws SQLException, DbHandleException {
    logger.info("Check for candidate email exists");
    LoginDAO loginDAO = new LoginDAOImpl(dbHandle);
    if (loginDAO.candidateLoginCheck(login)) {
      logger.info("email exists");
      return true;
    } else {
      logger.info("email not  exists");
      return false;
    }
  }

  @Override
  public List<HashMap<String, String>> GetAdminList(Long organization_id) throws SQLException, DbHandleException {
    LoginDAO loginDAO = new LoginDAOImpl(dbHandle);
    return loginDAO.GetAdminList(organization_id);
  }

  @Override
  public LoginBOEnum resetPassword(MultiMap params)
      throws SQLException, DbHandleException, UnsupportedEncodingException {
    logger.info("Reset password");
    String email = params.get("email");
    String hrisUrl = params.get("hris_url");
    String messageContent = "";
    String encryptedString = "";
    OrganizationDAO organizationDAO = new OrganizationDAOImpl(dbHandle);
    Organization organization = new Organization();
    organization = organizationDAO.getOrganizationDetails(hrisUrl);

    if (organization == null) {
      logger.info("Invalid details for reset password");
      return LoginBOEnum.INVALID_DATA;
    }

    Login login = new Login();
    LoginDAO loginDAO = new LoginDAOImpl(dbHandle);
    login = loginDAO.getLoginDetails(email, organization.getOrganization_id());

    if (login == null) {
      logger.info("Login not found");
      return LoginBOEnum.EMPLOYEE_NOT_EXISTS;
    }

    login.setKeyValue(UUID.randomUUID().toString());
    login.setHashValue(UUID.randomUUID().toString());
    int updateRows = 0;
    updateRows = loginDAO.updateKeyValue(login);

    Mail mailObject = new Mail();
    encryptedString = generateTokenForResetPwd(login);

    messageContent = mailObject.resetMail(Vertx.vertx(), login.getFirstName(),
        encryptedString, email, organization.getOrganization_name(), organization.getLogo(),
        hrisUrl);
    logger.info("employee  exists and mail sent");
    return LoginBOEnum.RESET_MAIL_SENT;

  }

  public String generateToken(Login loginObject) throws UnsupportedEncodingException {

    String generatedToken = "";
    //java.sql.Timestamp expiryTime = new Timestamp(System.currentTimeMillis() + 30 * 60 * 1000);
    String key = JWTUtil.getEncryptKey();
    Algorithm algorithm = Algorithm.HMAC256(key);
    generatedToken = JWT.create().withIssuer("auth0")
        .withClaim("organization_id", String.valueOf(loginObject.getOrganizationId()))
        .withClaim("user_id", String.valueOf(loginObject.getId()))
        .withClaim("user_type", loginObject.getUserType())
        .sign(algorithm);
    return generatedToken;
  }

  public String generateTokenForResetPwd(Login loginObject) throws UnsupportedEncodingException {

    String generatedToken = "";
    long expiry = System.currentTimeMillis() + 30 * 60 * 1000;  // 30 minutes check
    java.sql.Timestamp expiryTime = new Timestamp(expiry);
    String key = JWTUtil.getEncryptKey();

    Algorithm algorithm = Algorithm.HMAC256(key);
    generatedToken = JWT.create().withIssuer("auth0").withExpiresAt(expiryTime)
        .withClaim("organization_id", String.valueOf(loginObject.getOrganizationId()))
        .withClaim("user_id", String.valueOf(loginObject.getId()))
        .withClaim("user_type", loginObject.getUserType())
        .sign(algorithm);
    return generatedToken;
  }

  @Override
  public ReturnObject<LoginBOEnum, JsonObject> checkResetMail(MultiMap params)
      throws SQLException, DbHandleException, UnsupportedEncodingException {
    logger.info("Check reset mail");
    String encryptedString = params.get("token");
    String[] parts = encryptedString.split("\\.");
    String payLoad = StringUtils.newStringUtf8(Base64.decodeBase64(parts[1]));
    JsonObject payloadJson = new JsonObject(payLoad);
    String key = JWTUtil.getEncryptKey();

    long userId = Long.valueOf(payloadJson.getString("user_id"));
    long organizationId = Long.valueOf(payloadJson.getString("organization_id"));

    Algorithm algorithm = Algorithm.HMAC256(key);

    JWTVerifier verifier = JWT.require(algorithm).withIssuer("auth0").build();
    DecodedJWT jwt = verifier.verify(encryptedString);


    JsonObject jsonObject = new JsonObject();
    Login login = new Login();
    login.setId(userId);
    LoginDAO loginDAO = new LoginDAOImpl(dbHandle);
    String keyValue = "";
    String hashValue = "";
    String userType = "";
    Login loginHashValues = new Login();
    loginHashValues = loginDAO.getKeyValues(login);
    keyValue = loginHashValues.getKeyValue();
    hashValue = loginHashValues.getHashValue();
    userType = loginHashValues.getUserType();

    Organization organization = new Organization();
    OrganizationDAO organizationDAO = new OrganizationDAOImpl(dbHandle);
    organization = organizationDAO.getOrganizationById(organizationId);
    logger.info("redirecting the check password link");
    jsonObject.put("hris_url", organization.getHris_url());

    if (hashValue == null) {
      if (userType.equals(LoginBOEnum.CANDIDATE.getDescription())) {
        return new ReturnObjectImpl<LoginBOEnum, JsonObject>(LoginBOEnum.LINK_USED_CANDIDATE, jsonObject);
      } else {
        return new ReturnObjectImpl<LoginBOEnum, JsonObject>(LoginBOEnum.LINK_USED_EMPLOYEE, jsonObject);
      }
    } /*else {
      int updateHash = loginDAO.updateHashValue(login);
    }*/

    jsonObject.put("key_value", keyValue);
    return new ReturnObjectImpl<LoginBOEnum, JsonObject>(LoginBOEnum.REDIRECT, jsonObject);
  }

  @Override
  public ReturnObject<LoginBOEnum, JsonObject> changePasswordForReset(MultiMap params) throws SQLException, DbHandleException {
    logger.info("Change password for reset");
    String password = StringUtil.printValidString(params.get("password"));
    String confirmPassword = StringUtil.printValidString(params.get("confirm_password"));
    String userType = StringUtil.printValidString(params.get("user_id"));
    JsonObject dataObject = new JsonObject();
    if (!password.equals(confirmPassword)) {
      logger.info(" password not matched");
      return new ReturnObjectImpl<LoginBOEnum, JsonObject>
          (LoginBOEnum.PASSWORD_MATCH_FAILED, dataObject);
    }
    Login login = new Login();
    login.setPassword(password);
    login.setKeyValue(userType);

    LoginDAO loginDAO = new LoginDAOImpl(dbHandle);
    long loginId = loginDAO.getLoginIdWithKeyValue(login);
    login.setId(loginId);

    int updateRows = 0;
    updateRows = loginDAO.updatePasswordWithKeyValue(login);
    login = loginDAO.getLoginUserDetails(loginId);
    dataObject.put("id", loginId);
    dataObject.put("first_name", login.getFirstName());
    dataObject.put("last_name", login.getLastName());
    dataObject.put("organization_id", login.getOrganizationId());
    dataObject.put("user_type", login.getUserType());
    dataObject.put("consumer_id", login.getConsumerId());
    dataObject.put("email", login.getEmail());

    login.setId(loginId);

    int updateHash = loginDAO.updateHashValue(login);  //update hash value to restrict reset link usage

    if (updateRows > 0) {
      logger.info("password changed");
      return new ReturnObjectImpl<LoginBOEnum, JsonObject>
          (LoginBOEnum.PASSWORD_CHANGED, dataObject);
    } else {
      logger.info(" not changed password");
      return new ReturnObjectImpl<LoginBOEnum, JsonObject>
          (LoginBOEnum.PASSWORD_NOCHANGE, dataObject);
    }
  }
}