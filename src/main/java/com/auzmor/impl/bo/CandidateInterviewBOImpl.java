package com.auzmor.impl.bo;

import com.auzmor.bo.CandidateInterviewBO;
import com.auzmor.bo.EmployeeBO;
import com.auzmor.bo.JobBO;
import com.auzmor.bo.OrganizationBO;
import com.auzmor.dao.*;
import com.auzmor.impl.dao.*;
import com.auzmor.impl.enumarator.bo.CandidateInterviewEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.model.Login;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.Role;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.impl.models.CandidateR.CommentCandidate;
import com.auzmor.impl.models.CandidateR.HiringProcessCandidate;
import com.auzmor.impl.models.CandidateR.InterviewProcessCandidate;
import com.auzmor.impl.util.DateUtil;
import com.auzmor.impl.util.Mail;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.util.db.DbHandle;
import com.mysql.cj.core.util.StringUtils;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CandidateInterviewBOImpl implements CandidateInterviewBO {
  private static Logger logger = LoggerFactory.getLogger(CandidateInterviewBOImpl.class);

  private DbHandle dbHandle;
  private CandidateInterviewDAO candidateInterviewDAO;
  private CandidateHiringDAO candidateHiringDAO;

  public CandidateInterviewBOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  public void setCandidateHiringDAO(CandidateHiringDAO candidateHiringDAO) {
    this.candidateHiringDAO = candidateHiringDAO;
  }

  public void setCandidateInterviewDAO(CandidateInterviewDAO candidateInterviewDAO) {
    this.candidateInterviewDAO = candidateInterviewDAO;
  }

  @Override
  public long addInterviewProcess(MultiMap paramsMap)
      throws SQLException, DbHandleException, ParseException {

    String interviewDate = paramsMap.get("interview_date");
    InterviewProcessCandidate candidateInterviewProcess = new InterviewProcessCandidate();
    candidateInterviewProcess.setCandidateId(Long.parseLong(paramsMap.get("candidate_id")));
    candidateInterviewProcess.setInterviewDate(new DateUtil().getDate().parse(interviewDate));

    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    java.util.Date dateWithoutTime = cal.getTime();

    java.util.Date currentDate = new java.util.Date();

    if (candidateInterviewProcess.getInterviewDate().compareTo(dateWithoutTime) == 0) {
      SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
      SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
      String dateString = dateFormat.format(dateWithoutTime);
      java.util.Date interviewTime = null;
      interviewTime =
          simpleDateFormat.parse(dateString + " " + paramsMap.get("interview_time"));

      long timeDifference = interviewTime.getTime() - currentDate.getTime();
      if (timeDifference <= 0) {
        return 0;
      }
    } else if (candidateInterviewProcess.getInterviewDate().before(dateWithoutTime)) {
      return 0;
    }
    candidateInterviewProcess.setInterviewTime(paramsMap.get("interview_time"));
    candidateInterviewProcess.setInterviewDuration(paramsMap.get("interview_duration"));
    candidateInterviewProcess.setInterviewEndTime(paramsMap.get("interview_end_time"));
    candidateInterviewProcess.setInterviewLocation(paramsMap.get("interview_location"));
    candidateInterviewProcess.setSubjectName(paramsMap.get("subject_name"));
    candidateInterviewProcess.setAdminName(paramsMap.get("admin_name"));
    candidateInterviewProcess.setInterviewMailDescription(paramsMap.get("interview_mail_description"));
    candidateInterviewProcess.setInterviewType(paramsMap.get("interview_type"));
    candidateInterviewProcess.setInterviewDetails(paramsMap.get("interview_details"));
    candidateInterviewProcess.setType(paramsMap.get("type"));
    candidateInterviewProcess.setUserId(Long.parseLong(paramsMap.get("user_id")));
    candidateInterviewProcess.setCollaboratorId(Long.parseLong(paramsMap.get("collaborator_id")));
    candidateInterviewProcess.setSubStatus(paramsMap.get("sub_status"));

    CandidateInterviewDAOImpl interviewDAO = new CandidateInterviewDAOImpl(this.dbHandle);
    interviewDAO.updateActiveStatus(Long.parseLong(paramsMap.get("candidate_id")));
    logger.info("Interview process added Successfully");
    return interviewDAO.create(candidateInterviewProcess);

  }

  @Override
  public CandidateInterviewEnum updateInterviewProcess(MultiMap paramsMap, RoutingContext routingContext)
      throws SQLException, DbHandleException {

    CandidateInterviewDAOImpl interviewDAO = new CandidateInterviewDAOImpl(this.dbHandle);

    String status = StringUtil.printValidString(paramsMap.get("status"));
    String cancelReason = StringUtil.printValidString(paramsMap.get("interview_cancel_reason"));
    String otherReason = StringUtil.printValidString(paramsMap.get("interview_other_reason"));
    String adminName = "";
    String comments = "";
    int id = 0;
    long candidateId = Long.parseLong(paramsMap.get("candidateId"));
    long userId;
    if (!StringUtil.printValidString(paramsMap.get("user_id")).equals("")) {
      userId = Long.parseLong(paramsMap.get("user_id"));
    } else {
      userId = 0;
    }
    if (!StringUtil.printValidString(paramsMap.get("admin_name")).equals("")) {
      adminName = paramsMap.get("admin_name");
    } else {
      adminName = "";
    }

    /*----------------checking access role for admin/HiringLead/Collaborator ---------------------*/
    String userType = routingContext.data().get("userType").toString();

    CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
    JobBO jobBO = new JobBOImpl(dbHandle);
    int hiringLead = 0;

    if ((!status.equals("") || status.length() > 0) &&
        (!cancelReason.equals("") || cancelReason.length() > 0)) {
      HiringProcessCandidate candidateHiringProcessModel = new HiringProcessCandidate();
      CandidateHiringProcessDAOImpl candidateHiringProcessDao =
          new CandidateHiringProcessDAOImpl(dbHandle);
      comments = " rejected your interview with a response of: " + cancelReason;
      if (cancelReason.equalsIgnoreCase("No longer interested")) {
        id = interviewDAO.updateCancelStatus(cancelReason, status, candidateId, userId);
        if (id > 0) {
          candidateHiringProcessModel.setStatus("Not Hired");
          candidateHiringProcessModel.setCandidateProcess("Declined Offer");
          candidateHiringProcessModel.setReason(cancelReason);
          candidateHiringProcessModel.setCandidateId(candidateId);
          candidateHiringProcessModel.setUserId(userId);
          id = candidateHiringProcessDao.create(candidateHiringProcessModel);
        } else {
          return CandidateInterviewEnum.FAILED;
        }
      } else {
        id = interviewDAO.updateStatus(status, candidateId);
        candidateHiringProcessModel.setStatus("Interview");
        candidateHiringProcessModel.setCandidateProcess("Schedule Interview");
        candidateHiringProcessModel.setReason(cancelReason);
        candidateHiringProcessModel.setCandidateId(candidateId);
        candidateHiringProcessModel.setUserId(userId);
        if (cancelReason.equalsIgnoreCase("Other")) {
          candidateHiringProcessModel.setOtherCancelReason(otherReason);
        }
        id = candidateHiringProcessDao.create(candidateHiringProcessModel);
        if (id == 0) {
          return CandidateInterviewEnum.FAILED;
        }
      }
      InterviewProcessCandidate interviewProcessCandidate = interviewDAO.
          getInterviewProcessDetails(candidateId);
      EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);


      if (interviewProcessCandidate != null) {

        long collaboratorEmployeeId = interviewProcessCandidate.getCollaboratorId();
        long adminId = interviewProcessCandidate.getUserId();
        OrganizationBO organizationBO = new OrganizationBOImpl(dbHandle);
        CandidateR candidateR = candidateDAO.getCandidate(candidateId);
        Organization organization = organizationBO.getOrganizationDetails(candidateR.getOrganizationId());
        String organizationName = organization.getOrganization_name();
        organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);
        Mail mail = new Mail();
        Job job = jobBO.getJobDetails(candidateR.getJobId());
        String hiringLeadEmployeeId = job.getHiring_lead();

        if (cancelReason.equalsIgnoreCase("I would like to reschedule")) {
          //Mail for Collaborator
          if (collaboratorEmployeeId != 0) {
            Employee employee = employeeBO.getEmployeeDetails(collaboratorEmployeeId);
            if (employee != null) {
              String body = "<html><body>Dear Collaborator, <br/>" +
                  " This is to inform you that" +
                  " we have received a request to reschedule the interview for the position of " + job.getTitle() + "" +
                  " which was earlier slated for " + interviewProcessCandidate.getInterviewDate() + " at " +
                  " " + interviewProcessCandidate.getInterviewTime() + ". \n<br/> <br/>" +
                  "Thank you,\n<br/> " + organizationName + " Team\n<br/>" +
                  "--------------------------------------------------------------\n<br/>" +
                  "This is a system generated mail.\n<br/>" +
                  "Please do not reply to this mail\n<br/>" +
                  "-------------------------------------------------------------- " +
                  "</body></html>";

              String subjectName = "Request to reschedule interview-For Collaborator";
              if (!"success".equals(mail.sendMail(Vertx.vertx(),
                  body,
                  employee.getWork_email(), subjectName))) {
                return CandidateInterviewEnum.SEND_FAILED;
              }
            }
          }
          //Mail for Hiringlead
          if (!(null == hiringLeadEmployeeId) && !(hiringLeadEmployeeId.isEmpty())) {
            Employee employee = employeeBO.getEmployeeDetails(Long.parseLong(hiringLeadEmployeeId));
            if (employee != null) {
              String body = "<html><body>Dear Hiring lead/collaborator, <br/>" +
                  " This is to inform you that" +
                  " we have received a request to reschedule the interview for the position of " + job.getTitle() + "" +
                  " which was earlier slated for " + interviewProcessCandidate.getInterviewDate() + " at " +
                  " " + interviewProcessCandidate.getInterviewTime() + ". \n<br/> <br/>" +
                  "Thank you,\n<br/> " + organizationName + " Team\n<br/>" +
                  "--------------------------------------------------------------\n<br/>" +
                  "This is a system generated mail.\n<br/>" +
                  "Please do not reply to this mail\n<br/>" +
                  "-------------------------------------------------------------- " +
                  "</body></html>";

              String subjectName = "Request to reschedule interview";
              if (!"success".equals(mail.sendMail(Vertx.vertx(),
                  body,
                  employee.getWork_email(), subjectName))) {
                return CandidateInterviewEnum.SEND_FAILED;
              }
            }
          }
          if (adminId != 0) {
            Employee employee = employeeBO.getEmployeeDetails(adminId);
            if (employee != null) {
              String interviewDetails = "";

              if (interviewProcessCandidate.getInterviewType().equalsIgnoreCase("Web")) {
                interviewDetails = interviewProcessCandidate.getInterviewDetails();
              } else {
                interviewDetails = interviewProcessCandidate.getInterviewLocation();
              }

              String body = "<html><body>Dear Admin,<br/>" +
                  "This is to inform you that " + candidateR.getFirstName() + " " + candidateR.getLastName() + "" +
                  " has requested to reschedule the interview for " +
                  "the position of " + job.getTitle() + " at " + organizationName + " " +
                  "which was earlier scheduled to happen on " + interviewProcessCandidate.getInterviewDate() + " " +
                  " at " + interviewProcessCandidate.getInterviewTime() + "  at " + interviewDetails + ". \n<br/> <br/>" +
                  "Thank you,\n<br/> " + organizationName + " Team <br/>" +
                  "--------------------------------------------------------------\n<br/>" +
                  "This is a system generated mail.\n<br/>" +
                  "Please do not reply to this mail\n<br/>" +
                  "-------------------------------------------------------------- " +
                  "</body></html>";

              String subjectName = "Request to reschedule interview";
              if (!"success".equals(mail.sendMail(Vertx.vertx(),
                  body,
                  employee.getWork_email(), subjectName))) {
                return CandidateInterviewEnum.SEND_FAILED;
              }
            }
          }

          String userName = candidateR.getFirstName() + " " + candidateR.getLastName();

          String body = "<html><body>Dear " + userName + ",<br/>" +
              " This is to inform you that we have received your request to reschedule the interview for " +
              " the position of " + job.getTitle() + " with " + organizationName + ". " +
              " Your request is being processed. We will get back to you with the revised interview schedule. . \n<br/><br/> " +
              "Thank you,\n<br/> " + organizationName + " Team <br/>" +
              "--------------------------------------------------------------\n<br/>" +
              "This is a system generated mail.\n<br/>" +
              "Please do not reply to this mail\n<br/>" +
              "-------------------------------------------------------------- " +
              "</body></html>";

          String subjectName = "Request to reschedule interview";
          if (!"success".equals(mail.sendMail(Vertx.vertx(),
              body,
              candidateR.getEmail(), subjectName))) {
            return CandidateInterviewEnum.SEND_FAILED;
          }

        } else if (cancelReason.equalsIgnoreCase("Took another position") ||
            cancelReason.equalsIgnoreCase("Not interested")) {
          Employee employee = employeeBO.getEmployeeDetails(adminId);
          if (employee != null) {
            String body = "<html><body>Dear Admin, <br/>" +
                "  This is to bring to your notice that " + candidateR.getFirstName() + " " + candidateR.getLastName() + " " +
                " has taken up a job position with another organization and is " +
                "no longer available to attend the interview scheduled on " +
                " " + interviewProcessCandidate.getInterviewDate() + " at " + interviewProcessCandidate.getInterviewTime() + " " +
                " for the position of " + job.getTitle() + " at  " + organizationName + " \n<br/><br/> " +
                " Thank you,\n<br/> " + organizationName + " Team <br/> " +
                "--------------------------------------------------------------\n<br/>" +
                "This is a system generated mail.\n<br/>" +
                "Please do not reply to this mail\n<br/>" +
                "-------------------------------------------------------------- " +
                "</body></html>";

            String subjectName = "Response to interview schedule";
            if (!"success".equals(mail.sendMail(Vertx.vertx(),
                body,
                employee.getWork_email(), subjectName))) {
              return CandidateInterviewEnum.SEND_FAILED;
            }
          }
        }
      }
    } else if (!cancelReason.equals("") || cancelReason.length() > 0) {

      CandidateR candidateR = candidateDAO.getCandidate(candidateId);
      Job job = jobBO.getJobDetails(candidateR.getJobId());
      if (!userType.equalsIgnoreCase("admin")) {

        int employeeId = ((Employee) routingContext.data().get("employee")).getEmployee_id();

        String hiringLeadId = job.getHiring_lead();

        if (!StringUtils.isNullOrEmpty(hiringLeadId)) {
          hiringLead = Integer.parseInt(hiringLeadId);
        }
        // checking hiringLead whether employeeId had access to this job.
        if (employeeId != hiringLead && hiringLead > 0) {
          return CandidateInterviewEnum.ACCESS_CHECK;
        }
      }

      comments = "Interview Cancelled";
      status = "Admin Rejected";
      id = interviewDAO.updateCancelStatus(cancelReason, status, candidateId, userId);
            /* candidate status update */
      candidateDAO.updateCandidateStatus("Interview",
          Long.parseLong(paramsMap.get("candidateId")));

    } else if (status.equals("accept")) {
      id = interviewDAO.updateStatus(status, candidateId);
    }
    // interview completed status
    else if (!StringUtil.printValidString(paramsMap.get("sub_status")).equals("") &&
        !StringUtil.printValidString(paramsMap.get("is_completed")).equals("")) {

      CandidateR candidateR = candidateDAO.getCandidate(candidateId);
      Job job = jobBO.getJobDetails(candidateR.getJobId());

      InterviewProcessCandidate interviewProcessCandidate = interviewDAO.
          getInterviewProcessDetails(candidateId);

      long collaboratorId = interviewProcessCandidate.getCollaboratorId();

      if (!userType.equalsIgnoreCase("admin")) {

        int employeeId = ((Employee) routingContext.data().get("employee")).getEmployee_id();
        String hiringLeadId = job.getHiring_lead();

        if (!StringUtils.isNullOrEmpty(hiringLeadId)) {
          hiringLead = Integer.parseInt(hiringLeadId);
        }
        // checking hiringLead or collaborator whether employeeId had access to this job.
        if (collaboratorId != employeeId) {
        //  if(employeeId != hiringLead && hiringLead > 0 )
          return CandidateInterviewEnum.ACCESS_CHECK;
        }
      }
      int count = interviewDAO.updateCompletedStatus(candidateId, paramsMap.get("sub_status"));
      if (count > 0) {
        if (collaboratorId != 0) {
          JobTaskDAO jobTaskDAO = new JobTaskDAOImpl(dbHandle);
          jobTaskDAO.updateCompletedFlag(collaboratorId);
          return CandidateInterviewEnum.COMPLETED_SUCCESS;
        } else {
          return CandidateInterviewEnum.FAILED;
        }
      } else {
        return CandidateInterviewEnum.FAILED;
      }
    } else {

      CandidateR candidateR = candidateDAO.getCandidate(candidateId);
      Job job = jobBO.getJobDetails(candidateR.getJobId());

      if (!userType.equalsIgnoreCase("admin")) {

        int employeeId = ((Employee) routingContext.data().get("employee")).getEmployee_id();

        String hiringLeadId = job.getHiring_lead();

        if (!StringUtils.isNullOrEmpty(hiringLeadId)) {
          hiringLead = Integer.parseInt(hiringLeadId);
        }
        // checking hiringLead or collaborator whether employeeId had access to this job.
        if (employeeId != hiringLead && hiringLead > 0) {
          return CandidateInterviewEnum.ACCESS_CHECK;
        }
      }

      comments = "Interview Rescheduled on " + paramsMap.get("interview_date") + " ";
      InterviewProcessCandidate candidateInterviewProcess = new InterviewProcessCandidate();
      candidateInterviewProcess.setCandidateId(candidateId);
      if (!StringUtil.printValidString(paramsMap.get("interview_date")).equals("")) {
        String interviewDate = paramsMap.get("interview_date");
        try {
          candidateInterviewProcess.setInterviewDate(new DateUtil().getDate().parse(interviewDate));
        } catch (ParseException e) {
          logger.error("Date Format Exception");
        }
      }
      if (!StringUtil.printValidString(paramsMap.get("interview_time")).equals("")) {
        candidateInterviewProcess.setInterviewTime(paramsMap.get("interview_time"));
      }
      if (!StringUtil.printValidString(paramsMap.get("interview_duration")).equals("")) {
        candidateInterviewProcess.setInterviewDuration(paramsMap.get("interview_duration"));
      }
      if (!StringUtil.printValidString(paramsMap.get("subject_name")).equals("")) {
        candidateInterviewProcess.setSubjectName(paramsMap.get("subject_name"));
      }
      if (!StringUtil.printValidString(paramsMap.get("admin_name")).equals("")) {
        candidateInterviewProcess.setAdminName(paramsMap.get("admin_name"));
      }
      if (!StringUtil.printValidString(paramsMap.get("interview_mail_description")).equals("")) {
        candidateInterviewProcess.setInterviewMailDescription(paramsMap.get("interview_mail_description"));
      }
      if (!StringUtil.printValidString(paramsMap.get("interview_type")).equals("")) {
        candidateInterviewProcess.setInterviewType(paramsMap.get("interview_type"));
      }
      if (StringUtil.printValidString(paramsMap.get("type")).equals("")) {
        candidateInterviewProcess.setType("mail");
      } else {
        candidateInterviewProcess.setType(paramsMap.get("type"));
      }
      if (!StringUtil.printValidString(paramsMap.get("interview_end_time")).equals("")) {
        candidateInterviewProcess.setInterviewEndTime(paramsMap.get("interview_end_time"));
      }
      if (!StringUtil.printValidString(paramsMap.get("collaborator_id")).equals("")) {
        candidateInterviewProcess.setCollaboratorId(Long.parseLong(paramsMap.get("collaborator_id")));
      }

      candidateInterviewProcess.setInterviewDetails(StringUtil.printValidString(paramsMap.get("interview_details")));
      candidateInterviewProcess.setInterviewLocation(StringUtil.printValidString(paramsMap.get("interview_location")));
      candidateInterviewProcess.setSubStatus(StringUtil.printValidString(paramsMap.get("sub_status")));

      id = interviewDAO.updateInterviewDetails(candidateInterviewProcess);
      /* candidate status update */
      candidateDAO.updateCandidateStatus("Interview",
          Long.parseLong(paramsMap.get("candidateId")));
    }
    if (id > 0) {
      if (!comments.equals("")) {
        CommentCandidate commentsModel = new CommentCandidate();
        commentsModel.setAdminName(adminName);
        java.util.Date dNow = new java.util.Date();
        String currentDate = new DateUtil().getDate().format(dNow);
        try {
          commentsModel.setCommentsDate(new DateUtil().getDate().parse(currentDate));
        } catch (ParseException e) {
          logger.error("Date Format Exception");
        }
        commentsModel.setComments(comments);
        commentsModel.setCandidateId(candidateId);
        commentsModel.setUserId(userId);
        commentsModel.setSystemGenerated(true);

        CandidateCommentDAO candidateCommentDAO = new CandidateCommentDAOImpl(dbHandle);
        id = candidateCommentDAO.create(commentsModel);
        if (id == 0) {
          return CandidateInterviewEnum.FAILED;
        }
      }
      logger.info("Interview Updated Successfully");
      return CandidateInterviewEnum.SUCCESS;
    } else

    {
      logger.info("Interview Update Failed");
      return CandidateInterviewEnum.NO_UPDATE;
    }

  }

  @Override
  public InterviewProcessCandidate getInterviewDetails(long candidateId)
      throws SQLException, DbHandleException {
    InterviewProcessCandidate interviewProcessCandidate;
    CandidateInterviewDAOImpl interviewDAO = new CandidateInterviewDAOImpl(this.dbHandle);
    interviewProcessCandidate = interviewDAO.getInterviewProcessDetails(candidateId);
    logger.info("Interview Details fetched Successfully");
    return interviewProcessCandidate;
  }

  @Override
  public List<HashMap<String, String>> getListOfInterviewDetails(String dateFormat, String fieldList,
                                                                 long organizationId, int limit, int offset, long collaboratorId)
      throws SQLException, DbHandleException {
    String fieldColumnName = "";
    StringBuilder columnName = new StringBuilder();
    List<String> fieldName = new ArrayList<>();
    if (fieldList != null && fieldList.length() > 0) {
      String[] fields = fieldList.split(",");
      for (String field : fields) {
        if (field.equals("id")) {
          fieldColumnName = "Any_Value(InterviewProcessCandidate." + field.toString() + ") as " + field.toString() + ",";
        } else if (field.equals("job_title")) {
          fieldColumnName = "Any_Value(Job.title) as " + field.toString() + ",";
        } else {
          fieldColumnName = "Any_Value(" + field.toString() + ") as " + field.toString() + ",";
        }
        columnName.append(fieldColumnName);
        fieldName.add(field.toString());
      }
    }
    List<HashMap<String, String>> interviewProcessCandidate;

    CandidateInterviewDAOImpl interviewDAO = new CandidateInterviewDAOImpl(this.dbHandle);
    interviewProcessCandidate = interviewDAO.getListOfInterviewProcess(dateFormat, columnName,
        organizationId, limit, offset, fieldName, collaboratorId);
    return interviewProcessCandidate;
  }

  @Override
  public List<HashMap<String, String>> getInterviewMailDetails(long candidateId)
      throws SQLException, DbHandleException {
    CandidateInterviewDAO mailDetailsDAO = new CandidateInterviewDAOImpl(dbHandle);
    List<HashMap<String, String>> mailDetails = mailDetailsDAO.getInterviewMailList(candidateId);
    logger.info("Interview Mail Details fetched Successfully");
    return mailDetails;
  }

  /*@Override
  public String getStatus(long candidateId, String status) throws SQLException, DbHandleException {
    return null;
  }*/
}

