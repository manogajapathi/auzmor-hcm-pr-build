package com.auzmor.impl.bo;

import com.auzmor.bo.OnboardingTaskBO;
import com.auzmor.dao.OnboardingTaskDAO;
import com.auzmor.impl.dao.OnboardingTaskDAOImpl;
import com.auzmor.impl.enumarator.bo.OnboardingTaskEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.onboarding.OnboardingTask;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;

public class OnboardingTaskBOImpl implements OnboardingTaskBO {
  /**
   * Add a new Task
   * @param dbHandle
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public ReturnObject<OnboardingTaskEnum, JsonObject> addTask(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    JsonObject dataObject = new JsonObject();
    OnboardingTaskDAO onboardingTaskDAO = new OnboardingTaskDAOImpl(dbHandle);
    OnboardingTask onboardingTask = new OnboardingTask();
    onboardingTask.setPacket_id(Integer.valueOf(params.get("packet_id")));
    onboardingTask.setTask_name(params.get("task_name"));
    onboardingTask.setTask_description(params.get("task_description"));

    boolean allowDocUpload = StringUtil.printValidString(params.get("allow_doc_upload")) == "" ? false :
        Boolean.valueOf(params.get("allow_doc_upload"));
    boolean viewingRequired = StringUtil.printValidString(params.get("viewing_required")) == "" ? false :
        Boolean.valueOf(params.get("viewing_required"));
    boolean docUploadRequired = StringUtil.printValidString(params.get("doc_upload_required")) == "" ? false :
        Boolean.valueOf(params.get("doc_upload_required"));

    onboardingTask.setAllow_doc_upload(allowDocUpload);
    onboardingTask.setViewing_required(viewingRequired);
    onboardingTask.setDoc_upload_required(docUploadRequired);
    onboardingTask.setReferralFile(params.get("task_file_path"));
    boolean alreadyExists = onboardingTaskDAO.isAlreadyExists(onboardingTask);

    if (alreadyExists) {
      dataObject.put("message", "Task already exists");
      return new ReturnObjectImpl<OnboardingTaskEnum, JsonObject>
          (OnboardingTaskEnum.ALREADY_EXISTS, new JsonObject());
    }

    int insertRows = onboardingTaskDAO.addTask(onboardingTask);
    if (insertRows > 0) {
      dataObject.put("message", "Task added successfully");
      return new ReturnObjectImpl<OnboardingTaskEnum, JsonObject>
          (OnboardingTaskEnum.ADD_SUCCESS, dataObject);
    } else {
      dataObject.put("message", "Task could not added");
      return new ReturnObjectImpl<OnboardingTaskEnum, JsonObject>
          (OnboardingTaskEnum.ADD_FAILURE, new JsonObject());
    }
  }

  /**
   * Delete a task
   * @param dbHandle
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public ReturnObject<OnboardingTaskEnum, JsonObject> deleteTask(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    JsonObject dataObject = new JsonObject();
    OnboardingTaskDAO onboardingTaskDAO = new OnboardingTaskDAOImpl(dbHandle);
    OnboardingTask onboardingTask = new OnboardingTask();
    onboardingTask.setTask_id(Integer.valueOf(params.get("taskId")));

    int deleteRows = onboardingTaskDAO.deleteTask(onboardingTask);
    if (deleteRows > 0) {
      dataObject.put("message", "Task deleted successfully");
      return new ReturnObjectImpl<OnboardingTaskEnum, JsonObject>
          (OnboardingTaskEnum.DELETE_SUCCESS, dataObject);
    } else {
      dataObject.put("message", "Task does not exists");
      return new ReturnObjectImpl<OnboardingTaskEnum, JsonObject>
          (OnboardingTaskEnum.DELETE_FAILURE, dataObject);
    }
  }

  /**
   * Update task details
   * @param dbHandle
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public ReturnObject<OnboardingTaskEnum, JsonObject> updateTask(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    JsonObject dataObject = new JsonObject();
    OnboardingTaskDAO onboardingTaskDAO = new OnboardingTaskDAOImpl(dbHandle);
    int updateRows = onboardingTaskDAO.updateTask(params);
    if (updateRows > 0) {
      dataObject.put("message", "Task updated successfully");
      return new ReturnObjectImpl<OnboardingTaskEnum, JsonObject>
          (OnboardingTaskEnum.UPDATE_SUCCESS, dataObject);
    } else {
      dataObject.put("message", "No new details to update");
      return new ReturnObjectImpl<OnboardingTaskEnum, JsonObject>
          (OnboardingTaskEnum.UPDATE_FAILURE, dataObject);
    }
  }
}
