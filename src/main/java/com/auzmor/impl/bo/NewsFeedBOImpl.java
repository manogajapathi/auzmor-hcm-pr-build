package com.auzmor.impl.bo;

import com.auzmor.bo.NewsFeedBO;
import com.auzmor.dao.NewsFeedDAO;
import com.auzmor.impl.dao.NewsFeedDAOImpl;
import com.auzmor.impl.enumarator.NewsFeedEnum;
import com.auzmor.impl.handler.NewsFeedHandler;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.util.bo.ReturnObject;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class NewsFeedBOImpl implements NewsFeedBO{
  @Override
  public ReturnObject<NewsFeedEnum, JsonObject> getNewsFeedForAdmin(RestClient restClient,
                                                                    int organizationId)
      throws IOException {

    HashMap<String, Integer> sourceItems = new HashMap<>();

    boolean announcementExists = true;
    boolean onboardingExists = true;
    boolean jobTasksExists = true;
    Response indexResponse = null;
    String endPoint = "";
    try {
      indexResponse = restClient.performRequest("GET", "/announcement");
    } catch (IOException ioe) {
      announcementExists = false;
    }

    try {
      indexResponse = restClient.performRequest("GET", "/onboarding");
    } catch (IOException ioe) {
      onboardingExists = false;
    }

    try {
      indexResponse = restClient.performRequest("GET", "/jobtasks");
    } catch (IOException ioe){
      jobTasksExists = false;
    }

    Response response = null;
    JsonObject feedObject = new JsonObject();
    JsonObject jsonObject = new JsonObject();
    JsonArray hitsArray = new JsonArray();

    NewsFeedDAO newsFeedDAO = new NewsFeedDAOImpl();

    if (announcementExists) {


      JsonArray sourceArray = new JsonArray();
      sourceArray = newsFeedDAO.getAnnouncementDetails(restClient, organizationId);

      feedObject.put("announcement", sourceArray);
      sourceItems.put("announcement", sourceArray.size());
    } else {
      feedObject.put("announcement", new JsonArray());
      sourceItems.put("announcement", 0);
    }

    if (onboardingExists) {

      JsonArray sourceArray = newsFeedDAO.getOnboardingDetails(restClient, organizationId);

      feedObject.put("onboarding", sourceArray);
      sourceItems.put("onboarding", sourceArray.size());
    } else {
      feedObject.put("onboarding", new JsonArray());
      sourceItems.put("onboarding", 0);
    }

    if(jobTasksExists) {
      JsonArray sourceArray = newsFeedDAO.getJobTasksDetails(restClient, organizationId);
      feedObject.put("jobtasks", sourceArray);
      sourceItems.put("jobtasks", sourceArray.size());
    } else {
      feedObject.put("jobtasks", new JsonArray());
      sourceItems.put("jobtasks", 0);
    }

    JsonObject orderObject = new JsonObject();
    Map<String, Integer> orderedSourceItems = new NewsFeedHandler().sortByValue(sourceItems);
    int priorityNumber = 0;

    for (Map.Entry<String, Integer> entry : orderedSourceItems.entrySet()) {
      orderObject.put(entry.getKey(), ++priorityNumber);
    }

    feedObject.put("priority", orderObject);

    return new ReturnObjectImpl<NewsFeedEnum, JsonObject>(NewsFeedEnum.VALUE_SUCCESS, feedObject);
  }
}
