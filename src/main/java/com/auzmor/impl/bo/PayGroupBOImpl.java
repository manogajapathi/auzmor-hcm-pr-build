package com.auzmor.impl.bo;

import com.auzmor.bo.PayGroupBO;
import com.auzmor.dao.EmployeeDAO;
import com.auzmor.dao.PayGroupDAO;
import com.auzmor.impl.dao.EmployeeDAOImpl;
import com.auzmor.impl.dao.PayGroupDAOImpl;
import com.auzmor.impl.enumarator.PayGroupEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.PayGroupModel;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;

public class PayGroupBOImpl implements PayGroupBO {
  private static Logger logger = LoggerFactory.getLogger(PayGroupBOImpl.class);


  private DbHandle dbHandle;
  private  PayGroupDAO payGroupDAO;
  public PayGroupBOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }
  /**
   * Add new pay group
   * @param dbHandle
   * @param payGroupModel          contains paygroup name and organization id
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  @Override
  public ReturnObject<PayGroupEnum, JsonObject> addPayGroup(DbHandle dbHandle,
                                                            PayGroupModel payGroupModel)
      throws SQLException,DbHandleException {
    PayGroupDAO payGroupDAO = new PayGroupDAOImpl(dbHandle);
    if (payGroupDAO.isPayGroupExistsInOrganization(payGroupModel)) {
      return new ReturnObjectImpl<>(PayGroupEnum.EXISTS_ALREADY, new JsonObject());
    }
    int result = payGroupDAO.insert(payGroupModel);
    if(result > 0) {
      JsonObject resultObject =  new JsonObject();
      resultObject.put("paygroup_id", result);
      resultObject.put("paygroup_name", payGroupModel.getPaygroupName());
      resultObject.put("organization_id", payGroupModel.getOrganizationId());
      logger.info("Added PayGroup :"+result);
      return new ReturnObjectImpl<PayGroupEnum, JsonObject>
          (PayGroupEnum.ADDITION_SUCCESS,resultObject);
    } else {
      logger.error("Pay Group did not added :"+result);
      return new ReturnObjectImpl<PayGroupEnum, JsonObject>
          (PayGroupEnum.ADDITION_FAILED,new JsonObject());
    }
  }

  /**
   * Get details of new pay group
   * @param dbHandle
   * @param payGroupModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  @Override
  public ReturnObject<PayGroupEnum,JsonObject> getPayGroup(DbHandle dbHandle,
                                                           PayGroupModel payGroupModel)
      throws SQLException,DbHandleException {
    PayGroupDAO payGroupDAO = new PayGroupDAOImpl(dbHandle);
    PayGroupModel resultModel = payGroupDAO.getPayGroupDetails(payGroupModel);

    JsonObject resultObject =  new JsonObject();
    resultObject.put("paygroup_id", resultModel.getPaygroupId());
    resultObject.put("paygroup_name", resultModel.getPaygroupName());
    resultObject.put("organization_id", resultModel.getOrganizationId());
    logger.info("Available Pay Group :"+resultObject.getString("paygroup_name"));
    return new ReturnObjectImpl<PayGroupEnum, JsonObject>
          (PayGroupEnum.VALUE_SUCCESS, resultObject);

  }

  /**
   * Update details of pay group
   * @param dbHandle
   * @param requestParams
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  @Override
  public ReturnObject<PayGroupEnum,JsonObject> updatePayGroup(DbHandle dbHandle,
                                                              MultiMap requestParams)
      throws SQLException,DbHandleException {
    PayGroupDAO payGroupDAO = new PayGroupDAOImpl(dbHandle);
    int updateFlag = payGroupDAO.updatePayGroupDetails(requestParams);
    if(updateFlag > 0) {
      JsonObject resultObject = new JsonObject();
      resultObject.put("message", "Pay Group updated Successfully");
      logger.info("Pay Group update status :"+updateFlag);
      return new ReturnObjectImpl<PayGroupEnum, JsonObject>
          (PayGroupEnum.UPDATE_SUCCESS, resultObject);
    } else {
      logger.error("Pay Group update :"+updateFlag);
      return new ReturnObjectImpl<PayGroupEnum, JsonObject>
          (PayGroupEnum.NO_SUCH_VALUE, new JsonObject());
    }
  }

  /**
   * Delete pay group
   * @param dbHandle
   * @param payGroupModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  @Override
  public ReturnObject<PayGroupEnum,JsonObject> deletePayGroup(DbHandle dbHandle,
                                                              PayGroupModel payGroupModel)
      throws SQLException,DbHandleException {
    PayGroupDAO payGroupDAO = new PayGroupDAOImpl(dbHandle);
    PayGroupModel dataModel = new PayGroupModel();
    dataModel = payGroupDAO.getPayGroupDetails(payGroupModel);

    if (StringUtil.printValidString(dataModel.getPaygroupName()).equals("")) {
      return new ReturnObjectImpl<PayGroupEnum, JsonObject>
          (PayGroupEnum.NO_SUCH_VALUE, new JsonObject());
    }

    EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
    boolean deleteFlag = employeeDAO.isFieldOptionPresent("pay_group",
        dataModel.getPaygroupName(),dataModel.getOrganizationId());
    if(deleteFlag) {
      return new ReturnObjectImpl<PayGroupEnum, JsonObject>
          (PayGroupEnum.CANNOT_DELETE, new JsonObject());
    }

    int updateFlag = payGroupDAO.deletePayGroup(payGroupModel);
    if(updateFlag > 0) {
      JsonObject resultObject = new JsonObject();
      resultObject.put("message", "Pay Group deleted Successfully");
      logger.info("Deleted Pay Group :"+updateFlag);
      return new ReturnObjectImpl<PayGroupEnum, JsonObject>
          (PayGroupEnum.DELETE_SUCCESS, resultObject);
    } else {
      logger.error("Not Deleted Pay Group :"+updateFlag);
      return new ReturnObjectImpl<PayGroupEnum, JsonObject>
          (PayGroupEnum.NO_SUCH_VALUE, new JsonObject());
    }
  }

  /**
   * Get list of pay groups
   * @param dbHandle
   * @param payGroupModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  @Override
  public ReturnObject<PayGroupEnum,JsonArray> getAllPayGroups(DbHandle dbHandle,
                                                              PayGroupModel payGroupModel)
      throws SQLException,DbHandleException {
    PayGroupDAO payGroupDAO = new PayGroupDAOImpl(dbHandle);
    List<PayGroupModel> resultList = payGroupDAO.getAllPayGroups(payGroupModel);
    if(resultList.size() == 0) {
      logger.error("There are no available pay groups : "+resultList.size());
      return new ReturnObjectImpl<PayGroupEnum, JsonArray>
          (PayGroupEnum.NO_SUCH_VALUE, new JsonArray());
    } else {
      JsonArray resultArray = new JsonArray();
      for(PayGroupModel resultModel : resultList) {
        JsonObject resultObject = new JsonObject();
        resultObject.put("paygroup_id", resultModel.getPaygroupId());
        resultObject.put("paygroup_name", resultModel.getPaygroupName());
        resultObject.put("organization_id", resultModel.getOrganizationId());
        resultArray.add(resultObject);
      }
      logger.info("Total Pay Groups :"+resultArray.size());
      return new ReturnObjectImpl<PayGroupEnum, JsonArray>
          (PayGroupEnum.VALUE_SUCCESS, resultArray);
    }
  }

  public PayGroupDAO getPayGroupDAO() {
    return payGroupDAO;
  }
  public void setPayGroupDAO(PayGroupDAO payGroupDAO) {
    this.payGroupDAO= payGroupDAO;
  }

}
