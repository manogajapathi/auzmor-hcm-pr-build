package com.auzmor.impl.bo;

import com.auzmor.bo.EmployeePTOBO;
import com.auzmor.dao.EmployeeDAO;
import com.auzmor.dao.EmployeePTORequestsDAO;
import com.auzmor.dao.PTOCommentsDAO;
import com.auzmor.dao.PTODescriptorDAO;
import com.auzmor.impl.dao.EmployeeDAOImpl;
import com.auzmor.impl.dao.EmployeePTORequestDAOImpl;
import com.auzmor.impl.dao.PTOCommentsDAOImpl;
import com.auzmor.impl.dao.PTODescriptorDAOImpl;
import com.auzmor.impl.enumarator.bo.EmployeePTOEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.PTO.EmployeePTORequests;
import com.auzmor.impl.model.PTO.PTOComments;
import com.auzmor.impl.model.PTO.PTODescriptor;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EmployeePTOBOImpl implements EmployeePTOBO {
  /**
   * Add new PTO Request
   * @param dbHandle
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @throws ParseException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  @Override
  public ReturnObject<EmployeePTOEnum, JsonObject> addRequest(DbHandle dbHandle, MultiMap params)
    throws SQLException, DbHandleException, ParseException {
    JsonObject dataObject = new JsonObject();
    EmployeePTORequests employeePTORequests = new EmployeePTORequests();
    employeePTORequests.setEmployeeId(Long.valueOf(params.get("employee_id")));
    employeePTORequests.setDescription(params.get("description"));

    double hours = StringUtil.printValidString(params.get("hours")) == "" ? 0.0d :
        Double.valueOf(params.get("hours"));
    employeePTORequests.setHours(hours);
    
    java.util.Date fromDate = null;
    java.util.Date toDate = null;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
    if (StringUtil.printValidString(params.get("from_date")).trim().length() != 0) {
      fromDate = simpleDateFormat.parse(params.get("from_date"));
      employeePTORequests.setFromDate(new java.sql.Date(fromDate.getTime()));
    } else {
      return new ReturnObjectImpl<EmployeePTOEnum, JsonObject>
          (EmployeePTOEnum.PARAMS_MISSING, dataObject);
    }

    if (StringUtil.printValidString(params.get("to_date")).trim().length() != 0) {
      toDate = simpleDateFormat.parse(params.get("to_date"));
      employeePTORequests.setToDate(new java.sql.Date(toDate.getTime()));
    } else {
      return new ReturnObjectImpl<EmployeePTOEnum, JsonObject>
          (EmployeePTOEnum.PARAMS_MISSING, dataObject);
    }
    employeePTORequests.setTimeOffType(params.get("time_off_type"));
    EmployeePTORequestsDAO employeePTORequestsDAO = new EmployeePTORequestDAOImpl(dbHandle);

    boolean alreadyExists = employeePTORequestsDAO.alreadyExists(employeePTORequests);
    if (alreadyExists) {
      dataObject.put("message", "Request already exists for the same dates");
      return new ReturnObjectImpl<EmployeePTOEnum, JsonObject>
          (EmployeePTOEnum.ALREADY_EXISTS, dataObject);
    }

    if (employeePTORequests.getTimeOffType().equals("Sick")) {
      java.util.Date date = new java.util.Date();
      java.util.Date toDateUtil = new java.util.Date(employeePTORequests.getToDate().getTime());
      java.util.Date fromDateUtil = new java.util.Date(employeePTORequests.getFromDate().getTime());

      if (fromDateUtil.after(date) || toDateUtil.after(date)) {
        return new ReturnObjectImpl<EmployeePTOEnum, JsonObject>
            (EmployeePTOEnum.FUTURE_DATE_NOT_APPLICABLE, dataObject);
      }
    }

    double hoursCount = 0.0d;
    hoursCount = employeePTORequestsDAO.getUsedHours(employeePTORequests);

    PTODescriptor ptoDescriptor = new PTODescriptor();
    ptoDescriptor.setEmployeeId(employeePTORequests.getEmployeeId());
    ptoDescriptor.setTimeOffType(employeePTORequests.getTimeOffType());

    double maxAllowedHours = 0.0d;
    PTODescriptorDAO ptoDescriptorDAO = new PTODescriptorDAOImpl(dbHandle);
    JsonObject maxObject = new JsonObject();
    maxObject = ptoDescriptorDAO.getPTOMaxHours(ptoDescriptor);

    maxAllowedHours = maxObject.getDouble("max_hours") == null ?
        0.0d : maxObject.getDouble("max_hours");

    if (maxAllowedHours == 0.0d) {
      dataObject.put("message", "Maximum value for time off missing, Contact admin for help");
      return new ReturnObjectImpl<EmployeePTOEnum, JsonObject>
          (EmployeePTOEnum.MAX_VALUE_MISSING, dataObject);
    }

    double estimatedHoursUsed = hoursCount + employeePTORequests.getHours();
    if (maxAllowedHours < estimatedHoursUsed) {
      dataObject.put("message", "Maximum allowed hours exceeded");
      return new ReturnObjectImpl<EmployeePTOEnum, JsonObject>
          (EmployeePTOEnum.EXCEEDED_MAX_VALUE, dataObject);
    }

    int insertRows = employeePTORequestsDAO.addRequest(employeePTORequests);
    if (insertRows > 0) {
      dataObject.put("message", "Request added successfully");
      return new ReturnObjectImpl<EmployeePTOEnum, JsonObject>
          (EmployeePTOEnum.REQUEST_ADDED, dataObject);
    } else {
      return new ReturnObjectImpl<EmployeePTOEnum, JsonObject>
          (EmployeePTOEnum.REQUEST_ADD_FAILED, dataObject);
    }
  }

  /**
   * Get all request for employee
   * @param dbHandle
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @throws ParseException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  @Override
  public ReturnObject<EmployeePTOEnum, JsonArray> allRequest(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException, ParseException {
    JsonObject dataObject = new JsonObject();
    JsonArray resultArray = new JsonArray();
    EmployeePTORequests employeePTORequests = new EmployeePTORequests();
    employeePTORequests.setEmployeeId(Long.valueOf(params.get("employee_id")));
    EmployeePTORequestsDAO employeePTORequestsDAO = new EmployeePTORequestDAOImpl(dbHandle);
    List<JsonObject> resultList = new ArrayList<JsonObject>();
    resultList = employeePTORequestsDAO.allRequests(employeePTORequests);
    if (resultList.size() == 0) {
      return new ReturnObjectImpl<EmployeePTOEnum, JsonArray>
          (EmployeePTOEnum.TABLE_EMPTY, resultArray);
    }

    EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("MM/dd/yy");

    for (JsonObject jsonObject : resultList) {
      JsonObject resultObject = new JsonObject();
      resultObject.put("id", jsonObject.getLong("id"));
      resultObject.put("employee_id", jsonObject.getLong("employee_id"));
      resultObject.put("time_off_type", jsonObject.getString("time_off_type"));
      resultObject.put("hours", jsonObject.getDouble("hours"));
      resultObject.put("description", jsonObject.getString("description"));

      if (StringUtil.printValidString(jsonObject.getString("submitted_date")).trim().length() != 0) {
        java.util.Date submittedDate = simpleDateFormat.parse(jsonObject.getString("submitted_date"));
        resultObject.put("submitted", simpleDateFormat1.format(submittedDate));
      }

      if (StringUtil.printValidString(jsonObject.getString("from_date")).trim().length() != 0) {
        java.util.Date fromDate = simpleDateFormat.parse(jsonObject.getString("from_date"));
        resultObject.put("from_date", simpleDateFormat1.format(fromDate));
      }

      if (StringUtil.printValidString(jsonObject.getString("to_date")).trim().length() != 0) {
        java.util.Date toDate = simpleDateFormat.parse(jsonObject.getString("to_date"));
        resultObject.put("to_date", simpleDateFormat1.format(toDate));
      }

      if(jsonObject.getInteger("request_status") == 0) {
        long empId = jsonObject.getLong("employee_id");
        Employee employee = new Employee();
        employee = employeeDAO.getEmployeeDetails(empId);
        resultObject.put("status", "Requested (" +employee.getFirst_name() + " "
            + employee.getLast_name() + " " + resultObject.getString("submitted") + ")");
      } else if (jsonObject.getInteger("request_status") == 1) {
        long empId = jsonObject.getLong("approved_by");
        Employee employee = new Employee();
        employee = employeeDAO.getEmployeeDetails(empId);
        java.util.Date approvedDate =
            simpleDateFormat.parse(jsonObject.getString("approved_date"));
        resultObject.put("status", "Approved (" +employee.getFirst_name() + " "
            + employee.getLast_name() + " " + simpleDateFormat1.format(approvedDate) + ")");
      } else {
        long empId = jsonObject.getLong("approved_by");
        Employee employee = new Employee();
        employee = employeeDAO.getEmployeeDetails(empId);
        java.util.Date approvedDate =
            simpleDateFormat.parse(jsonObject.getString("approved_date"));
        resultObject.put("status", "Rejected (" +employee.getFirst_name() + " "
            + employee.getLast_name() + " " + simpleDateFormat1.format(approvedDate) + ")");
      }

      PTOCommentsDAO ptoCommentsDAO = new PTOCommentsDAOImpl(dbHandle);
      PTOComments ptoComments = new PTOComments();
      ptoComments.setPtoId(jsonObject.getLong("id"));
      List<JsonObject> commentsList = new ArrayList<>();
      commentsList = ptoCommentsDAO.getAllComments(ptoComments);

      resultObject.put("comments", commentsList.size());

      resultArray.add(resultObject);
    }
    return new ReturnObjectImpl<EmployeePTOEnum, JsonArray>
        (EmployeePTOEnum.TABLE_VALUES, resultArray);
  }

  /**
   * Get all used request for employee
   * @param dbHandle
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @throws ParseException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  @Override
  public ReturnObject<EmployeePTOEnum, JsonArray> allUsedRequest(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException, ParseException {
    JsonObject dataObject = new JsonObject();
    JsonArray resultArray = new JsonArray();
    EmployeePTORequests employeePTORequests = new EmployeePTORequests();
    employeePTORequests.setEmployeeId(Long.valueOf(params.get("employee_id")));
    EmployeePTORequestsDAO employeePTORequestsDAO = new EmployeePTORequestDAOImpl(dbHandle);
    List<JsonObject> resultList = new ArrayList<JsonObject>();
    resultList = employeePTORequestsDAO.allUsed(employeePTORequests);
    if (resultList.size() == 0) {
      return new ReturnObjectImpl<EmployeePTOEnum, JsonArray>
          (EmployeePTOEnum.TABLE_EMPTY, resultArray);
    }

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("MM/dd/yy");

    for (JsonObject jsonObject : resultList) {
      JsonObject resultObject = new JsonObject();
      resultObject.put("id", jsonObject.getLong("id"));
      resultObject.put("employee_id", jsonObject.getLong("employee_id"));
      resultObject.put("description", jsonObject.getString("description"));

      if (StringUtil.printValidString(jsonObject.getString("from_date")).trim().length() != 0) {
        java.util.Date fromDate = simpleDateFormat.parse(jsonObject.getString("from_date"));
        resultObject.put("from_date", simpleDateFormat1.format(fromDate));
      }

      if (StringUtil.printValidString(jsonObject.getString("to_date")).trim().length() != 0) {
        java.util.Date toDate = simpleDateFormat.parse(jsonObject.getString("to_date"));
        resultObject.put("to_date", simpleDateFormat1.format(toDate));
      }

      PTODescriptor ptoDescriptor = new PTODescriptor();
      ptoDescriptor.setEmployeeId(jsonObject.getLong("employee_id"));
      ptoDescriptor.setTimeOffType(jsonObject.getString("time_off_type"));
      PTODescriptorDAO ptoDescriptorDAO = new PTODescriptorDAOImpl(dbHandle);
      JsonObject accuredObject = new JsonObject();
      accuredObject = ptoDescriptorDAO.getPTOMaxHours(ptoDescriptor);

      resultObject.put("Accured", accuredObject.getDouble("max_hours"));
      resultObject.put("Used", jsonObject.getDouble("hours"));
      resultObject.put("Balance",
          accuredObject.getDouble("max_hours") - jsonObject.getDouble("hours"));

      resultArray.add(resultObject);
    }
    return new ReturnObjectImpl<EmployeePTOEnum, JsonArray>
        (EmployeePTOEnum.TABLE_VALUES, resultArray);
  }

  /**
   * Get available hour count for employee
   * @param dbHandle
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  @Override
  public ReturnObject<EmployeePTOEnum, JsonObject> getAvailableCount(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    JsonObject dataObject = new JsonObject();
    PTODescriptor ptoDescriptor = new PTODescriptor();
    ptoDescriptor.setEmployeeId(Long.valueOf(params.get("employee_id")));

    PTODescriptorDAO ptoDescriptorDAO = new PTODescriptorDAOImpl(dbHandle);
    List<JsonObject> resultList = new ArrayList<>();
    resultList = ptoDescriptorDAO.getAllHours(ptoDescriptor);
    if (resultList.size() == 0) {
      dataObject.put("Vacation", 0.0d);
      dataObject.put("Unpaid_Time_off", 0.0d);
      dataObject.put("Sick", 0.0d);
      return new ReturnObjectImpl<EmployeePTOEnum, JsonObject>
          (EmployeePTOEnum.TABLE_EMPTY, dataObject);
    }

    EmployeePTORequestsDAO employeePTORequestsDAO = new EmployeePTORequestDAOImpl(dbHandle);
    for (JsonObject jsonObject : resultList) {
      EmployeePTORequests employeePTORequests = new EmployeePTORequests();
      employeePTORequests.setEmployeeId(ptoDescriptor.getEmployeeId());
      employeePTORequests.setTimeOffType(jsonObject.getString("time_off_type"));
      double usedHours = 0.0d;
      usedHours = employeePTORequestsDAO.getUsedHours(employeePTORequests);
      double maxHours = 0.0d;
      maxHours = jsonObject.getDouble("max_hours");
      double availableHours = maxHours - usedHours;

      dataObject.put(jsonObject.getString("time_off_type").equals("Unpaid Time-Off") ?
          "Unpaid_Time_off" : jsonObject.getString("time_off_type"), availableHours);
    }
    return new ReturnObjectImpl<EmployeePTOEnum, JsonObject>
        (EmployeePTOEnum.TABLE_VALUES, dataObject);
  }

  /**
   * Get upcoming time-off for employee
   * @param dbHandle
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @throws ParseException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 19/MAR/2018
   */
  @Override
  public ReturnObject<EmployeePTOEnum, JsonArray> getUpcomingTimeOff(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException, ParseException {
    JsonObject dataObject = new JsonObject();
    JsonArray jsonArray = new JsonArray();
    EmployeePTORequests employeePTORequests = new EmployeePTORequests();
    employeePTORequests.setEmployeeId(Long.valueOf(params.get("employee_id")));
    EmployeePTORequestsDAO employeePTORequestsDAO = new EmployeePTORequestDAOImpl(dbHandle);

    List<JsonObject> resultList = new ArrayList<>();
    resultList = employeePTORequestsDAO.upcomingPTO(employeePTORequests);

    if(resultList.size() == 0) {
      return new ReturnObjectImpl<EmployeePTOEnum, JsonObject>
          (EmployeePTOEnum.TABLE_EMPTY, dataObject);
    }

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("MMMM dd");

    for (JsonObject jsonObject : resultList) {
      dataObject.put("id", jsonObject.getLong("id"));
      dataObject.put("time_off_type", jsonObject.getString("time_off_type"));

      dataObject.put("hours", jsonObject.getDouble("hours"));
      dataObject.put("description", jsonObject.getString("description"));

      if (StringUtil.printValidString(jsonObject.getString("from_date")).trim().length() != 0) {
        java.util.Date fromDate = simpleDateFormat.parse(jsonObject.getString("from_date"));
        dataObject.put("from_date", simpleDateFormat1.format(fromDate));
      }

      if (StringUtil.printValidString(jsonObject.getString("to_date")).trim().length() != 0) {
        java.util.Date toDate = simpleDateFormat.parse(jsonObject.getString("to_date"));
        dataObject.put("to_date", simpleDateFormat1.format(toDate));
      }

      jsonArray.add(dataObject);
    }
    return new ReturnObjectImpl<EmployeePTOEnum, JsonArray>
        (EmployeePTOEnum.TABLE_VALUES, jsonArray);
  }
}
