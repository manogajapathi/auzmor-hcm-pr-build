package com.auzmor.impl.bo;

import com.auzmor.bo.HighestEducationBO;
import com.auzmor.dao.CandidateDAO;
import com.auzmor.dao.HighestEducationDAO;
import com.auzmor.impl.dao.CandidateDAOImpl;
import com.auzmor.impl.dao.HighestEducationDAOImpl;
import com.auzmor.impl.enumarator.bo.HighestEducationEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.FieldOptions.HighestEducationModel;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HighestEducationBOImpl implements HighestEducationBO {
  private static Logger logger = LoggerFactory.getLogger(HighestEducationBOImpl.class);

  public ReturnObject<HighestEducationEnum, JsonObject> addHighestEducation(DbHandle dbHandle,
                                                                            MultiMap params)
      throws SQLException, DbHandleException {
    HighestEducationModel highestEducationModel = new HighestEducationModel();
    highestEducationModel.setHighestEducation(params.get("highest_education"));
    long organizationId = StringUtil.printValidString(params.get("organization_id")).equals("") ? 0 :
        Long.valueOf(params.get("organization_id"));
    highestEducationModel.setOrganizationId(organizationId);

    HighestEducationDAO highestEducationDAO = new HighestEducationDAOImpl(dbHandle);
    boolean alreadyExists = false ;
    alreadyExists = highestEducationDAO.isAlreadyHighestEducation(highestEducationModel);

    if (alreadyExists) {
      return new ReturnObjectImpl<HighestEducationEnum, JsonObject>
          (HighestEducationEnum.EXISTS_ALREADY, new JsonObject());
    }

    int insertRows = 0;
    insertRows = highestEducationDAO.insert(highestEducationModel);
    if (insertRows > 0) {
      logger.info("Highest Education insert : "+insertRows);
      return new ReturnObjectImpl<HighestEducationEnum, JsonObject>
          (HighestEducationEnum.ADDITION_SUCCESS, new JsonObject());
    } else {
      return new ReturnObjectImpl<HighestEducationEnum, JsonObject>
          (HighestEducationEnum.ADDITION_FAILED, new JsonObject());
    }
  }

  public ReturnObject<HighestEducationEnum, JsonObject> getHighestEducation(DbHandle dbHandle,
                                                                            MultiMap params)
      throws SQLException, DbHandleException {
    HighestEducationModel highestEducationModel = new HighestEducationModel();
    long id = StringUtil.printValidString(params.get("id")).equals("") ? 0 :
        Long.valueOf(params.get("id"));
    highestEducationModel.setId(id);

    HighestEducationDAO highestEducationDAO = new HighestEducationDAOImpl(dbHandle);
    JsonObject dataObject = new JsonObject();
    dataObject = highestEducationDAO.getHighestEducation(highestEducationModel);
    if (dataObject.getLong("id") == 0) {
      return new ReturnObjectImpl<HighestEducationEnum, JsonObject>
          (HighestEducationEnum.NO_SUCH_VALUE, new JsonObject());
    } else {
      logger.info("Highest Education details : "+dataObject.getLong("id"));
      return new ReturnObjectImpl<HighestEducationEnum, JsonObject>
          (HighestEducationEnum.VALUE_SUCCESS, dataObject);
    }
  }

  public ReturnObject<HighestEducationEnum, JsonArray> getAllHighestEducation(DbHandle dbHandle,
                                                                              MultiMap params)
      throws SQLException, DbHandleException {
    HighestEducationModel highestEducationModel = new HighestEducationModel();
    long organizationId = StringUtil.printValidString(params.get("organization_id")).equals("") ? 0 :
        Long.valueOf(params.get("organization_id"));
    highestEducationModel.setOrganizationId(organizationId);

    HighestEducationDAO highestEducationDAO = new HighestEducationDAOImpl(dbHandle);
    List<JsonObject> resultList = new ArrayList<>();
    resultList = highestEducationDAO.getAllHighestEducation(highestEducationModel);
    if (resultList.size() == 0) {
      return new ReturnObjectImpl<HighestEducationEnum, JsonArray>
          (HighestEducationEnum.NO_SUCH_VALUE, new JsonArray());
    } else {
      JsonArray resultArray = new JsonArray();
      for (JsonObject dataObject : resultList) {
        resultArray.add(dataObject);
      }
      logger.info("Highest Education details : "+resultArray.size());
      return new ReturnObjectImpl<HighestEducationEnum, JsonArray>
          (HighestEducationEnum.VALUE_SUCCESS, resultArray);
    }
  }

  public ReturnObject<HighestEducationEnum, JsonObject> updateHighestEducation(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    HighestEducationModel highestEducationModel = new HighestEducationModel();
    highestEducationModel.setHighestEducation(params.get("highest_education"));
    long id = StringUtil.printValidString(params.get("id")).equals("") ? 0 :
        Long.valueOf(params.get("id"));
    highestEducationModel.setId(id);

    HighestEducationDAO highestEducationDAO = new HighestEducationDAOImpl(dbHandle);
    JsonObject dataObject = new JsonObject();
    dataObject = highestEducationDAO.getHighestEducation(highestEducationModel);

    if (dataObject.getString("highest_education").equals(params.get("highest_education"))) {
      return new ReturnObjectImpl<HighestEducationEnum, JsonObject>
          (HighestEducationEnum.NO_CHANGES_MADE, new JsonObject());
    }

    CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
    boolean isUsedInCandidate = false;
    isUsedInCandidate = candidateDAO.checkFieldOptions("highest_education", dataObject.getString("highest_education"), dataObject.getLong("organization_id"));

    if (isUsedInCandidate) {
      return new ReturnObjectImpl<HighestEducationEnum, JsonObject>
          (HighestEducationEnum.RESTRICTED, new JsonObject());
    }

    int updateRows = 0;
    updateRows = highestEducationDAO.update(highestEducationModel);
    if (updateRows > 0) {
      logger.info("Highest Education details : "+updateRows);
      return new ReturnObjectImpl<HighestEducationEnum, JsonObject>
          (HighestEducationEnum.UPDATE_SUCCESS, new JsonObject());
    } else {
      return new ReturnObjectImpl<HighestEducationEnum, JsonObject>
          (HighestEducationEnum.NO_SUCH_VALUE, new JsonObject());
    }
  }

  public ReturnObject<HighestEducationEnum, JsonObject> deleteHighestEducation(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    HighestEducationModel highestEducationModel = new HighestEducationModel();
    long id = StringUtil.printValidString(params.get("id")).equals("") ? 0 :
        Long.valueOf(params.get("id"));
    highestEducationModel.setId(id);

    HighestEducationDAO highestEducationDAO = new HighestEducationDAOImpl(dbHandle);
    int deleteRows = 0;
    deleteRows = highestEducationDAO.delete(highestEducationModel);
    if (deleteRows > 0) {
      return new ReturnObjectImpl<HighestEducationEnum, JsonObject>
          (HighestEducationEnum.DELETE_SUCCESS, new JsonObject());
    } else {
      return new ReturnObjectImpl<HighestEducationEnum, JsonObject>
          (HighestEducationEnum.NO_SUCH_VALUE, new JsonObject());
    }
  }
}
