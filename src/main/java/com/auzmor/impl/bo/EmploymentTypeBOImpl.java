package com.auzmor.impl.bo;

import com.auzmor.bo.EmploymentTypeBO;
import com.auzmor.dao.EmployeeDAO;
import com.auzmor.dao.EmploymentTypeDAO;
import com.auzmor.dao.JobDAO;
import com.auzmor.impl.dao.EmployeeDAOImpl;
import com.auzmor.impl.dao.EmploymentTypeDAOImpl;
import com.auzmor.impl.dao.JobDAOImpl;
import com.auzmor.impl.enumarator.EmploymentTypeEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.EmploymentModel;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;

public class EmploymentTypeBOImpl implements EmploymentTypeBO {
  private static Logger logger = LoggerFactory.getLogger(EmploymentTypeBOImpl.class);

  /**
   * Add employment type
   * @param dbHandle
   * @param employmentModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13-02-2018
   */
  @Override
  public ReturnObject<EmploymentTypeEnum, JsonObject> addEmploymentType(DbHandle dbHandle,
                                                                        EmploymentModel employmentModel)
      throws SQLException,DbHandleException {
    EmploymentTypeDAO employmentTypeDAO = new EmploymentTypeDAOImpl(dbHandle);
    if (employmentTypeDAO.isEmploymentTypeExistsInOrganization(employmentModel)) {
      return new ReturnObjectImpl<>(EmploymentTypeEnum.EXISTS_ALREADY, new JsonObject());
    }
    int result = employmentTypeDAO.insert(employmentModel);
    if(result > 0) {
      JsonObject resultObject =  new JsonObject();
      resultObject.put("id", result);
      resultObject.put("employmentType", employmentModel.getEmploymentType());
      resultObject.put("organization_id", employmentModel.getOrganizationId());
      logger.info("Employment type insert : "+result);
      return new ReturnObjectImpl<EmploymentTypeEnum, JsonObject>
          (EmploymentTypeEnum.ADDITION_SUCCESS,resultObject);
    } else {
      return new ReturnObjectImpl<EmploymentTypeEnum, JsonObject>
          (EmploymentTypeEnum.ADDITION_FAILED,new JsonObject());
    }
  }

  /**
   * Get employment type
   * @param dbHandle
   * @param employmentModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13-02-2018
   */
  @Override
  public ReturnObject<EmploymentTypeEnum, JsonObject> getEmploymentType(DbHandle dbHandle,
                                                                        EmploymentModel employmentModel)
      throws SQLException,DbHandleException {
    EmploymentTypeDAO employmentTypeDAO = new EmploymentTypeDAOImpl(dbHandle);
    EmploymentModel resultModel = employmentTypeDAO.getEmploymentTypeDetails(employmentModel);

    JsonObject resultObject =  new JsonObject();
    resultObject.put("id", resultModel.getId());
    resultObject.put("employmentType", resultModel.getEmploymentType());
    resultObject.put("organization_id", resultModel.getOrganizationId());
    logger.info("Employment type details : "+resultModel.getId());
    return new ReturnObjectImpl<EmploymentTypeEnum, JsonObject>
          (EmploymentTypeEnum.VALUE_SUCCESS, resultObject);

  }

  /**
   * Update employment type
   * @param dbHandle
   * @param requestParams
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13-02-2018
   */
  @Override
  public ReturnObject<EmploymentTypeEnum, JsonObject> updateEmploymentType(DbHandle dbHandle,
                                                                  MultiMap requestParams)
      throws SQLException,DbHandleException {
    EmploymentTypeDAO employmentTypeDAO = new EmploymentTypeDAOImpl(dbHandle);
    EmploymentModel employmentModel = new EmploymentModel();
    employmentModel.setId(Integer.valueOf(requestParams.get("id")));

    employmentModel = employmentTypeDAO.getEmploymentTypeDetails(employmentModel);

    if (employmentModel.getEmploymentType().equals(requestParams.get("employmentType"))) {
      return new ReturnObjectImpl<EmploymentTypeEnum, JsonObject>
          (EmploymentTypeEnum.NO_CHANGES_MADE, new JsonObject());
    }

    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    boolean isUsedInJob = false;
    isUsedInJob = jobDAO.checkFieldOption("employment_type", employmentModel.getEmploymentType(),
        (long) employmentModel.getOrganizationId());

    if (isUsedInJob) {
      logger.info("Update restricted"+isUsedInJob);
      return new ReturnObjectImpl<EmploymentTypeEnum, JsonObject>(EmploymentTypeEnum.RESTRICTED, new JsonObject());
    }

    EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
    boolean deleteFlag = employeeDAO.isFieldOptionPresent("employment_status",
        employmentModel.getEmploymentType(),employmentModel.getOrganizationId());
    if(deleteFlag) {
      return new ReturnObjectImpl<EmploymentTypeEnum, JsonObject>
          (EmploymentTypeEnum.CANNOT_DELETE, new JsonObject());
    }

    int updateFlag = employmentTypeDAO.updateEmploymentTypeDetails(requestParams);
    if(updateFlag > 0) {
      JsonObject resultObject = new JsonObject();
      resultObject.put("message", "Employment Type updated Successfully");
      logger.info("Employment type update : "+updateFlag);
      return new ReturnObjectImpl<EmploymentTypeEnum, JsonObject>
          (EmploymentTypeEnum.UPDATE_SUCCESS, resultObject);
    } else {
      return new ReturnObjectImpl<EmploymentTypeEnum, JsonObject>
          (EmploymentTypeEnum.NO_SUCH_VALUE, new JsonObject());
    }
  }

  /**
   * Delete employment type
   * @param dbHandle
   * @param employmentModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13-02-2018
   */
  @Override
  public ReturnObject<EmploymentTypeEnum, JsonObject> deleteEmploymentType(DbHandle dbHandle,
                                                                           EmploymentModel employmentModel)
      throws SQLException,DbHandleException {

    String typeFromDb = "";
    EmploymentTypeDAO employmentTypeDAO = new EmploymentTypeDAOImpl(dbHandle);
    EmploymentModel resultModel = new EmploymentModel();
    resultModel = employmentTypeDAO.getEmploymentTypeDetails(employmentModel);

    if (StringUtil.printValidString(resultModel.getEmploymentType()).equals("")) {
      return new ReturnObjectImpl<EmploymentTypeEnum, JsonObject>
          (EmploymentTypeEnum.NO_SUCH_VALUE, new JsonObject());
    }

    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    boolean isUsedInJob = false;
    isUsedInJob = jobDAO.checkFieldOption("employment_type", employmentModel.getEmploymentType(),
        (long) employmentModel.getOrganizationId());

    if (isUsedInJob) {
      logger.info("Delete restricted"+isUsedInJob);
      return new ReturnObjectImpl<EmploymentTypeEnum, JsonObject>(EmploymentTypeEnum.RESTRICTED, new JsonObject());
    }

    EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
    boolean deleteFlag = employeeDAO.isFieldOptionPresent("employment_status",
        resultModel.getEmploymentType(),resultModel.getOrganizationId());
    if(deleteFlag) {
      return new ReturnObjectImpl<EmploymentTypeEnum, JsonObject>
          (EmploymentTypeEnum.CANNOT_DELETE, new JsonObject());
    }

    typeFromDb = resultModel.getEmploymentType();

    if(typeFromDb.equals("Part-Time") || typeFromDb.equals("Full-Time")
        || typeFromDb.equals("Contractor - W2") || typeFromDb.equals("Contractor - 1099")){
      return new ReturnObjectImpl<EmploymentTypeEnum, JsonObject>
          (EmploymentTypeEnum.CANNOT_DELETE, new JsonObject());
    }

    int updateFlag = employmentTypeDAO.deleteEmploymentType(employmentModel);
    if(updateFlag > 0) {logger.info("Employment type update : "+updateFlag);
      JsonObject resultObject = new JsonObject();
      resultObject.put("message", "Employment Type deleted Successfully");
      logger.info("Employment type delete : "+updateFlag);
      return new ReturnObjectImpl<EmploymentTypeEnum, JsonObject>
          (EmploymentTypeEnum.DELETE_SUCCESS, resultObject);
    } else {
      return new ReturnObjectImpl<EmploymentTypeEnum, JsonObject>
          (EmploymentTypeEnum.NO_SUCH_VALUE, new JsonObject());
    }
  }

  /**
   * Get all employment types
   * @param dbHandle
   * @param employmentModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13-02-2018
   */
  @Override
  public ReturnObject<EmploymentTypeEnum, JsonArray> getAllEmploymentTypes(DbHandle dbHandle,
                                                                           EmploymentModel employmentModel)
      throws SQLException,DbHandleException {
    EmploymentTypeDAO employmentTypeDAO = new EmploymentTypeDAOImpl(dbHandle);
    List<EmploymentModel> resultList = employmentTypeDAO.getAllEmploymentTypes(employmentModel);
    if(resultList.size() == 0) {
      return new ReturnObjectImpl<EmploymentTypeEnum, JsonArray>
          (EmploymentTypeEnum.NO_SUCH_VALUE, new JsonArray());
    } else {
      JsonArray resultArray = new JsonArray();
      for(EmploymentModel resultModel : resultList) {
        JsonObject resultObject = new JsonObject();
        resultObject.put("id", resultModel.getId());
        resultObject.put("employmentType", resultModel.getEmploymentType());
        resultObject.put("organization_id", resultModel.getOrganizationId());
        resultArray.add(resultObject);
      }
      logger.info("Employment type details : "+resultArray.size());
      return new ReturnObjectImpl<EmploymentTypeEnum, JsonArray>
          (EmploymentTypeEnum.VALUE_SUCCESS, resultArray);
    }
  }
}
