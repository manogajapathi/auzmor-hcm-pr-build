package com.auzmor.impl.bo;

import com.auzmor.bo.OnboardingDocumentBO;
import com.auzmor.dao.OnboardingDocumentDAO;
import com.auzmor.impl.dao.OnboardingDocumentDAOImpl;
import com.auzmor.impl.enumarator.bo.OnboardingDocumentEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.onboarding.OnboardingDocument;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;

public class OnboardingDocumentBOImpl implements OnboardingDocumentBO{
  /**
   * Add a new document
   * @param dbHandle
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public ReturnObject<OnboardingDocumentEnum, JsonObject> addDocument(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    JsonObject jsonObject = new JsonObject();
    OnboardingDocument onboardingDocument =  new OnboardingDocument();
    onboardingDocument.setDocument_name(params.get("document_name"));
    onboardingDocument.setDocument_path("document_file_path");
    onboardingDocument.setPacket_id(Integer.valueOf(params.get("packet_id")));

    int page = StringUtil.printValidString(params.get("page")) == "" ? 
        0 : Integer.valueOf(params.get("page"));
    double xCoordinate = StringUtil.printValidString(params.get("x_coordinate")) == "" ? 0.0d :
        Double.valueOf(params.get("x_coordinate"));
    double yCoordinate = StringUtil.printValidString(params.get("y_coordinate")) == "" ? 0.0d :
        Double.valueOf(params.get("y_coordinate"));

    boolean viewingRequired = StringUtil.printValidString(params.get("viewing_reqd")) == "" ? false :
        Boolean.valueOf(params.get("allow_doc_upload"));
    boolean signingRequired = StringUtil.printValidString(params.get("signing_required")) == "" ? false :
        Boolean.valueOf(params.get("viewing_required"));
    
    onboardingDocument.setPage(page);
    onboardingDocument.setxCoordinate(xCoordinate);
    onboardingDocument.setyCoordinate(yCoordinate);
    onboardingDocument.setViewing_reqd(viewingRequired);
    onboardingDocument.setSigningRequired(signingRequired);

    OnboardingDocumentDAO onboardingDocumentDAO = new OnboardingDocumentDAOImpl(dbHandle);
    boolean alreadyExists = onboardingDocumentDAO.isAlreadyExists(onboardingDocument);
    
    if (alreadyExists) {
      return new ReturnObjectImpl<OnboardingDocumentEnum, JsonObject>
          (OnboardingDocumentEnum.ALREADY_EXISTS, new JsonObject());
    }
    
    int insertRows = onboardingDocumentDAO.addDocument(onboardingDocument);
    if (insertRows > 0) {
      jsonObject.put("message", "Document added successfully");
      return new ReturnObjectImpl<OnboardingDocumentEnum, JsonObject>
          (OnboardingDocumentEnum.ADD_SUCCESS, jsonObject);
    } else {
      jsonObject.put("message", "Document already exists");
      return new ReturnObjectImpl<OnboardingDocumentEnum, JsonObject>
          (OnboardingDocumentEnum.ALREADY_EXISTS, new JsonObject());
    }
  }

  /**
   * Delete a document
   * @param dbHandle
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public ReturnObject<OnboardingDocumentEnum, JsonObject> deleteDocument(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    OnboardingDocument onboardingDocument =  new OnboardingDocument();
    JsonObject dataObject = new JsonObject();
    OnboardingDocumentDAO onboardingDocumentDAO = new OnboardingDocumentDAOImpl(dbHandle);
    onboardingDocument.setDocument_id(Integer.valueOf(params.get("documentId")));

    int deleteRows = onboardingDocumentDAO.deleteDocument(onboardingDocument);
    if (deleteRows > 0) {
      dataObject.put("message", "Document deleted successfully");
      return new ReturnObjectImpl<OnboardingDocumentEnum, JsonObject>
          (OnboardingDocumentEnum.DELETE_SUCCESS, dataObject);
    } else {
      dataObject.put("message", "Document does not exists");
      return new ReturnObjectImpl<OnboardingDocumentEnum, JsonObject>
          (OnboardingDocumentEnum.DELETE_FAILURE, new JsonObject());
    }
  }

  /**
   * Update document details
   * @param dbHandle
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public ReturnObject<OnboardingDocumentEnum, JsonObject> updateDocument(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    JsonObject dataObject = new JsonObject();
    OnboardingDocumentDAO onboardingDocumentDAO = new OnboardingDocumentDAOImpl(dbHandle);
    int updateRows = onboardingDocumentDAO.updateDocument(params);
    if (updateRows > 0) {
      dataObject.put("message", "Document updated successfully");
      return new ReturnObjectImpl<OnboardingDocumentEnum, JsonObject>
          (OnboardingDocumentEnum.UPDATE_SUCCESS, dataObject);
    } else {
      dataObject.put("message", "No new details to update");
      return new ReturnObjectImpl<OnboardingDocumentEnum, JsonObject>
          (OnboardingDocumentEnum.UPDATE_FAILURE, new JsonObject());
    }
  }
}
