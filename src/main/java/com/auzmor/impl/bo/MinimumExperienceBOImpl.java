package com.auzmor.impl.bo;

import com.auzmor.bo.MinimumExperienceBO;
import com.auzmor.dao.JobDAO;
import com.auzmor.dao.MinimumExperienceDAO;
import com.auzmor.impl.dao.JobDAOImpl;
import com.auzmor.impl.dao.MinimumExperienceDAOImpl;
import com.auzmor.impl.enumarator.bo.MinimumExperienceEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.FieldOptions.MinimumExperienceModel;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MinimumExperienceBOImpl implements MinimumExperienceBO {

  private static Logger logger = LoggerFactory.getLogger(MinimumExperienceBOImpl.class);
  
  @Override
  public ReturnObject<MinimumExperienceEnum, JsonObject> addMinimumExperience(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    MinimumExperienceModel minimumExperienceModel = new MinimumExperienceModel();
    minimumExperienceModel.setMinimumExperience(params.get("minimum_experience"));
    long organizationId = StringUtil.printValidString(params.get("organization_id")).equals("") ? 0 :
        Long.valueOf(params.get("organization_id"));
    minimumExperienceModel.setOrganizationId(organizationId);

    MinimumExperienceDAO minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
    boolean alreadyExists = false ;
    alreadyExists = minimumExperienceDAO.isAlreadyMinimumExperience(minimumExperienceModel);

    if (alreadyExists) {
      return new ReturnObjectImpl<MinimumExperienceEnum, JsonObject>
          (MinimumExperienceEnum.EXISTS_ALREADY, new JsonObject());
    }

    int insertRows = 0;
    insertRows = minimumExperienceDAO.insert(minimumExperienceModel);
    if (insertRows > 0) {
      logger.info("Highest Education insert : "+insertRows);
      return new ReturnObjectImpl<MinimumExperienceEnum, JsonObject>
          (MinimumExperienceEnum.ADDITION_SUCCESS, new JsonObject());
    } else {
      return new ReturnObjectImpl<MinimumExperienceEnum, JsonObject>
          (MinimumExperienceEnum.ADDITION_FAILED, new JsonObject());
    }
  }
  
  @Override
  public ReturnObject<MinimumExperienceEnum, JsonObject> getMinimumExperience(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    MinimumExperienceModel minimumExperienceModel = new MinimumExperienceModel();
    long id = StringUtil.printValidString(params.get("id")).equals("") ? 0 :
        Long.valueOf(params.get("id"));
    minimumExperienceModel.setId(id);
    
    MinimumExperienceDAO minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
    JsonObject dataObject = new JsonObject();
    dataObject = minimumExperienceDAO.getMinimumExperience(minimumExperienceModel);
    if (dataObject.getLong("id") == 0) {
      return new ReturnObjectImpl<MinimumExperienceEnum, JsonObject>
          (MinimumExperienceEnum.NO_SUCH_VALUE, new JsonObject());
    } else {
      logger.info("Minimum Experience details : "+dataObject.getLong("id"));
      return new ReturnObjectImpl<MinimumExperienceEnum, JsonObject>
          (MinimumExperienceEnum.VALUE_SUCCESS, dataObject);
    }
  }
  
  @Override
  public ReturnObject<MinimumExperienceEnum, JsonArray> getAllMinimumExperience(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    MinimumExperienceModel minimumExperienceModel = new MinimumExperienceModel();
    long organizationId = StringUtil.printValidString(params.get("organization_id")).equals("") ? 0 :
        Long.valueOf(params.get("organization_id"));
    minimumExperienceModel.setOrganizationId(organizationId);

    MinimumExperienceDAO minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
    List<JsonObject> resultList = new ArrayList<>();
    resultList = minimumExperienceDAO.getAllMinimumExperience(minimumExperienceModel);
    if (resultList.size() == 0) {
      return new ReturnObjectImpl<MinimumExperienceEnum, JsonArray>
          (MinimumExperienceEnum.NO_SUCH_VALUE, new JsonArray());
    } else {
      JsonArray resultArray = new JsonArray();
      for (JsonObject dataObject : resultList) {
        resultArray.add(dataObject);
      }
      logger.info("Minimum Experience details : "+resultArray.size());
      return new ReturnObjectImpl<MinimumExperienceEnum, JsonArray>
          (MinimumExperienceEnum.VALUE_SUCCESS, resultArray);
    }
  }

  public ReturnObject<MinimumExperienceEnum, JsonObject> updateMinimumExperience(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    MinimumExperienceModel minimumExperienceModel = new MinimumExperienceModel();
    minimumExperienceModel.setMinimumExperience(params.get("minimum_experience"));
    long id = StringUtil.printValidString(params.get("id")).equals("") ? 0 :
        Long.valueOf(params.get("id"));
    minimumExperienceModel.setId(id);

    MinimumExperienceDAO minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
    JsonObject dataModel = new JsonObject();

    dataModel = minimumExperienceDAO.getMinimumExperience(minimumExperienceModel);

    if (dataModel.getString("minimum_experience").equals(params.get("minimum_experience"))) {
      return new ReturnObjectImpl<MinimumExperienceEnum, JsonObject>
          (MinimumExperienceEnum.NO_CHANGES_MADE, new JsonObject());
    }

    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    boolean isUsedInJob = false;
    isUsedInJob = jobDAO.checkFieldOption("minimum_experience", dataModel.getString("minimum_experience"),
        (long) dataModel.getLong("organization_id"));

    if (isUsedInJob) {
      return new ReturnObjectImpl<MinimumExperienceEnum, JsonObject>
          (MinimumExperienceEnum.RESTRICTED, new JsonObject());
    }

    int updateRows = 0;
    updateRows = minimumExperienceDAO.update(minimumExperienceModel);
    if (updateRows > 0) {
      logger.info("Highest Education details : "+updateRows);
      return new ReturnObjectImpl<MinimumExperienceEnum, JsonObject>
          (MinimumExperienceEnum.UPDATE_SUCCESS, new JsonObject());
    } else {
      return new ReturnObjectImpl<MinimumExperienceEnum, JsonObject>
          (MinimumExperienceEnum.NO_SUCH_VALUE, new JsonObject());
    }
  }

  public ReturnObject<MinimumExperienceEnum, JsonObject> deleteMinimumExperience(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    MinimumExperienceModel minimumExperienceModel = new MinimumExperienceModel();
    long id = StringUtil.printValidString(params.get("id")).equals("") ? 0 :
        Long.valueOf(params.get("id"));
    minimumExperienceModel.setId(id);

    MinimumExperienceDAO minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
    JsonObject dataModel = new JsonObject();

    dataModel = minimumExperienceDAO.getMinimumExperience(minimumExperienceModel);

    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    boolean isUsedInJob = false;
    isUsedInJob = jobDAO.checkFieldOption("minimum_experience", dataModel.getString("minimum_experience"),
        (long) dataModel.getLong("organization_id"));

    if (isUsedInJob) {
      return new ReturnObjectImpl<MinimumExperienceEnum, JsonObject>
          (MinimumExperienceEnum.RESTRICTED, new JsonObject());
    }

    int deleteRows = 0;
    deleteRows = minimumExperienceDAO.delete(minimumExperienceModel);
    if (deleteRows > 0) {
      return new ReturnObjectImpl<MinimumExperienceEnum, JsonObject>
          (MinimumExperienceEnum.DELETE_SUCCESS, new JsonObject());
    } else {
      return new ReturnObjectImpl<MinimumExperienceEnum, JsonObject>
          (MinimumExperienceEnum.NO_SUCH_VALUE, new JsonObject());
    }
  }
  
}
