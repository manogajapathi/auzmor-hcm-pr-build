package com.auzmor.impl.bo;

import com.auzmor.bo.CandidateBO;
import com.auzmor.bo.CandidateCommentBO;
import com.auzmor.dao.CandidateCommentDAO;
import com.auzmor.impl.dao.CandidateCommentDAOImpl;
import com.auzmor.impl.enumarator.bo.CandidateCommentEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.models.CandidateR.CommentCandidate;
import com.auzmor.util.db.DbHandle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

public class CandidateCommentBOImpl implements CandidateCommentBO {

  private static Logger logger = LoggerFactory.getLogger(CandidateCommentBOImpl.class);
  private DbHandle dbHandle;

  public CandidateCommentBOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  @Override
  public CandidateCommentEnum addCommentDetails(CommentCandidate commentCandidate, Boolean flag)
      throws SQLException, DbHandleException {
    CandidateCommentDAO candidateCommentDAO = new CandidateCommentDAOImpl(dbHandle);

    int Id = candidateCommentDAO.create(commentCandidate);

    if (Id > 0) {
      logger.info("Comments added Successfully");
      return CandidateCommentEnum.SUCCESS;
    } else {
      logger.error("Adding Comments Failed");
      return CandidateCommentEnum.FAILED;
    }
  }

  @Override
  public CandidateCommentEnum updateCommentDetails(CommentCandidate commentCandidate)
      throws SQLException, DbHandleException {
    CandidateCommentDAO candidateCommentDAO = new CandidateCommentDAOImpl(dbHandle);

    boolean checkUserId = candidateCommentDAO.checkUserId(commentCandidate.getId(),
        commentCandidate.getUserId());

    if (checkUserId == true) {
      int Id = candidateCommentDAO.updateComment(commentCandidate);
      if (Id > 0) {
        logger.info("Comments updated Successfully");
        return CandidateCommentEnum.SUCCESS;
      } else {
        logger.info("No changes made in the Comments");
        return CandidateCommentEnum.UPDATE_NO_CHANGES;
      }
    } else {
      logger.error("Comments update Failed");
      return CandidateCommentEnum.FAILED;
    }

  }

  @Override
  public CandidateCommentEnum deleteCommentDetails(long candidateId, long userId, long id)
      throws SQLException, DbHandleException {
    int count = 0;
    CandidateCommentDAO candidateCommentDAO = new CandidateCommentDAOImpl(dbHandle);
    boolean checkUserId = candidateCommentDAO.checkUserId(id, userId);

    if (checkUserId) {
      count = candidateCommentDAO.deleteComment(candidateId, userId, id);
    } else {
      return CandidateCommentEnum.UPDATE_NO_CHANGES;
    }
    if (count > 0) {
      logger.info("Comments deleted Successfully");
      return CandidateCommentEnum.SUCCESS;
    } else {
      logger.error("Comments deletion Failed");
      return CandidateCommentEnum.FAILED;
    }
  }

  @Override
  public List<HashMap<String, String>> getCommentList(String format, long candidateId)
      throws SQLException, DbHandleException {
    CandidateCommentDAO commentDAO = new CandidateCommentDAOImpl(dbHandle);
    List<HashMap<String, String>> commentsDetails = commentDAO.getCommentsDetails(format, candidateId);
    logger.info("CComments list fetched successfully!");
    return commentsDetails;
  }

  @Override
  public List<HashMap<String, String>> getHistoryList(String format, long candidateId)
      throws SQLException, DbHandleException {
    CandidateCommentDAO commentDAO = new CandidateCommentDAOImpl(dbHandle);
    List<HashMap<String, String>> commentsDetails = commentDAO.getCommentsDetailsInHistoryTab(format, candidateId);
    logger.info("CComments list fetched successfully!");
    return commentsDetails;
  }
}
