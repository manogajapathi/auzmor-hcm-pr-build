package com.auzmor.impl.bo;

import com.auzmor.bo.CandidateBO;
import com.auzmor.bo.EmailTemplateBO;
import com.auzmor.bo.JobBO;
import com.auzmor.dao.EmailTemplateDAO;
import com.auzmor.impl.constant.template.EmailTemplateConstant;
import com.auzmor.impl.dao.EmailTemplateDAOImpl;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.EmailTemplate;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.impl.util.DateUtil;
import com.auzmor.impl.util.Mail;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.commons.text.StrSubstitutor;

import java.io.File;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class EmailTemplateBOImpl implements EmailTemplateBO {
  private DbHandle dbHandle;
  private static final String EMAIL_TEMPLATE_PREFIX = "{{";
  private static final String EMAIL_TEMPLATE_SUFFIX = "}}";
  private EmailTemplateDAO emailTemplateDAO;

  public EmailTemplateBOImpl(DbHandle dbHandle) {this.dbHandle =  dbHandle;}

  /**
   *
   * @param name
   * @param subject
   * @param header
   * @param footer
   * @param template
   * @param organizationId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 07/MAR/2018
   */
  @Override
  public long addTemplate(String name, String subject, String header, String footer, String template,
                          long organizationId) throws SQLException,
      DbHandleException {
    return new EmailTemplateDAOImpl(this.dbHandle).create(name, subject, header, footer,
        template, organizationId);
  }

  /**
   *
   * @param name
   * @param template
   * @param emailTemplateId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 07/MAR/2018
   */
  @Override
  public Boolean editTemplate(String name, String subject, String header, String footer,
                              String template, long emailTemplateId)
      throws SQLException, DbHandleException {
    EmailTemplateDAO emailTemplateDAO = new EmailTemplateDAOImpl(this.dbHandle);
    EmailTemplate emailTemplate = emailTemplateDAO.getTemplate(emailTemplateId);
    if (null == emailTemplate) {
      return null;
    }

    boolean diff = false;
    if (!emailTemplate.getName().equals(name)) {
      diff = true;
    } else if (!emailTemplate.getSubject().equals(subject)) {
      diff = true;
    } /* else if (null != header && (null == emailTemplate.getHeader() ||
        !emailTemplate.getHeader().equals(header))) {
      diff = true;
    } else if (null != footer && (null == emailTemplate.getFooter() ||
        !emailTemplate.getFooter().equals(footer))) {
      diff = true;
    } */ else if (!emailTemplate.getTemplate().equals(template)) {
      diff = true;
    }

    if (!diff) {
      return true;
    }
    return emailTemplateDAO.update(name, subject, header, footer, template, emailTemplateId) > 0 ?
        true : false;
  }

  /**
   *
   * @param emailTemplateId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 04/MAR/2018
   */
  @Override
  public Boolean deleteTemplate(long emailTemplateId) throws SQLException, DbHandleException {
    EmailTemplateDAO emailTemplateDAO = new EmailTemplateDAOImpl(this.dbHandle);
    if (null == emailTemplateDAO.getTemplate(emailTemplateId)) {
      return null;
    }

    return emailTemplateDAO.delete(emailTemplateId) > 0 ? true : false;
  }

  /**
   *
   * @param emailTemplateId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public JsonObject getTemplate(long emailTemplateId) throws SQLException, DbHandleException {
    EmailTemplate emailTemplate = new EmailTemplateDAOImpl(this.dbHandle).getTemplate(emailTemplateId);
    return templateToJson(emailTemplate, true);
  }

  /**
   *
   * @param organizationId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public JsonArray getTemplates(long organizationId) throws SQLException,
      DbHandleException {
    List<EmailTemplate> emailTemplates = new EmailTemplateDAOImpl(this.dbHandle)
        .getTemplates(organizationId);

    JsonArray jsonArray = new JsonArray();
    if (null != emailTemplates) {
      for (EmailTemplate emailTemplate : emailTemplates) {
        jsonArray.add(templateToJson(emailTemplate, true));
      }
    }

    return jsonArray;
  }

  /**
   *
   * @return
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public List<String> getTemplateVariables() {

    return Arrays.stream(getVariables()).map(template -> EMAIL_TEMPLATE_PREFIX  + template +
        EMAIL_TEMPLATE_SUFFIX)
        .collect(Collectors.toList());
  }

  /**
   *
   * @param candidateIds
   * @param templateId
   * @return
   * @throws SQLException
   * @throws DbHandleException'
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public JsonArray generateEmails(List<Long> candidateIds, long templateId)
      throws SQLException, DbHandleException {
    EmailTemplate emailTemplate = new EmailTemplateDAOImpl(this.dbHandle).getTemplate(templateId);

    return generateEmails(candidateIds, emailTemplate, false);
  }

  /**
   *
   * @param candidateIds
   * @param templateId
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 08/MAR/2018
   */
  @Override
  public void sendEmails(List<Long> candidateIds, long templateId) throws SQLException,
      DbHandleException {
    EmailTemplate emailTemplate = new EmailTemplateDAOImpl(this.dbHandle).getTemplate(templateId);
    JsonArray jsonArray = generateEmails(candidateIds, emailTemplate, true);
    Organization organization = new OrganizationBOImpl(this.dbHandle).getOrganizationDetails(emailTemplate.getOrganizationId());
    Map<String, String> map = new HashMap<>();
    map.put("organization_logo", Vertx.currentContext().config().getString("api_url") +
        File.separator + organization.getLogo());
    map.put("organization_url", organization.getHris_url());

    for (int i = 0; i < jsonArray.size(); ++i) {
      JsonObject jsonObject = jsonArray.getJsonObject(i);
      if (null != jsonObject.getLong("candidate_id")) {
        map.put("template", jsonObject.getString("template"));
        new Mail().sendMail(Vertx.vertx(), new StrSubstitutor(map, EmailTemplateConstant.BODY_HTML_TEMPLATE_PREFIX,
                EmailTemplateConstant.BODY_HTML_TEMPLATE_SUFFIX).replace(EmailTemplateConstant.BODY_HTML),
            jsonObject.getString("candidate_mail"),jsonObject.getString("subject"));
      }
    }
  }

  /***
   *
   * @param map
   * @param templateId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 07/MAR/2018
   */
  private JsonObject getSubstitutedJson(JsonObject emailTemplate, Map<String, String> map,
                                        long templateId) {
    JsonObject jsonObject = new JsonObject();
    if (emailTemplate != null) {
      StrSubstitutor strSubstitutor = new StrSubstitutor(map, EMAIL_TEMPLATE_PREFIX,
          EMAIL_TEMPLATE_SUFFIX);
      jsonObject.put("subject", strSubstitutor.replace(emailTemplate.getString("subject")));
      // jsonObject.put("header", strSubstitutor.replace(emailTemplate.getString("header")));
      // jsonObject.put("footer", strSubstitutor.replace(emailTemplate.getString("footer")));
      jsonObject.put("template", strSubstitutor.replace(emailTemplate.getString("template")));
    }

    return jsonObject;
  }

  /**
   *
   * @return
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 04/MAR/2018
   */
  private String[] getVariables() {
    String[] variables = { "First_Name", "Last_Name", "Position", "Company_Name", "Date"};

    return variables;
  }

  /**
   *
   * @param emailTemplate
   * @return
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 08/MAR/2018
   */
  private JsonObject templateToJson(EmailTemplate emailTemplate, boolean unEscapeHtml) {
    JsonObject jsonObject = new JsonObject();
    if (null != emailTemplate) {
      jsonObject.put("id", emailTemplate.getId());
      jsonObject.put("name", emailTemplate.getName());
      // jsonObject.put("header", emailTemplate.getHeader());
      // jsonObject.put("footer", emailTemplate.getFooter());
      jsonObject.put("subject", emailTemplate.getSubject());
      jsonObject.put("template", unEscapeHtml ? StringUtil.unEscapeHtml(emailTemplate.getTemplate()) :
          emailTemplate.getTemplate());
    }

    // TODO Temp add to the template, should be removed if finalized on header/footer addition
    return jsonObject;
  }

  /**
   *
   * @param candidateIds
   * @param emailTemplate
   * @param withMailId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Charles Sam Dilip
   * @lastModifiedDate 08/MAR/2018
   */
  private JsonArray generateEmails(List<Long> candidateIds, EmailTemplate emailTemplate,
                                   boolean withMailId)
      throws SQLException, DbHandleException {
    Organization organization = new OrganizationBOImpl(this.dbHandle).getOrganizationDetails(
        emailTemplate.getOrganizationId());
    CandidateBO candidateBO = new CandidateBOImpl(this.dbHandle);
    JobBO jobBO = new JobBOImpl(this.dbHandle);
    JsonObject template = templateToJson(emailTemplate, true);
    JsonArray jsonArray = new JsonArray();

    for (Long candidateId : candidateIds) {
      CandidateR candidateR = candidateBO.getCandidate(this.dbHandle, candidateId);
      Job job = jobBO.getJobDetails( candidateR.getJobId());
      Map<String, String> map = new HashMap<String, String>();
      String dateString = new DateUtil().getDate().format(Calendar.getInstance().getTime());
      if (null != candidateR) {
        map.put("First_Name", candidateR.getFirstName());
        map.put("Last_Name", candidateR.getLastName());
        map.put("Position", job.getTitle());
        map.put("Company_Name", organization.getOrganization_name());
        map.put("Date", dateString);
      } else {
        map.put("Date", dateString);
      }

      JsonObject jsonObject = getSubstitutedJson(template, map, emailTemplate.getId());
      jsonObject.put("candidate_id", candidateId);
      if (withMailId) {
        jsonObject.put("candidate_mail", candidateR.getEmail());
      }
      jsonArray.add(jsonObject);
    }
    return jsonArray;
  }

    public EmailTemplateDAO getEmailTemplateDAO() {
        return emailTemplateDAO;
    }
    public void setEmailTemplateDAO(EmailTemplateDAO emailTemplateDAO) {
        this.emailTemplateDAO = emailTemplateDAO;
    }

}
