package com.auzmor.impl.bo;

import com.auzmor.bo.MailDetailsBO;
import com.auzmor.dao.MailDetailsDAO;
import com.auzmor.impl.dao.MailDetailsDAOImpl;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.models.CandidateR.MailDetails;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;


public class MailDetailsBOImpl implements MailDetailsBO {
  private DbHandle dbHandle;
  private MailDetailsDAO mailDetailsDAO;

  public void setMailDetailsDAO(MailDetailsDAO mailDetailsDAO) {
    this.mailDetailsDAO = mailDetailsDAO;
  }

  public MailDetailsDAO getMailDetailsDAO() {
    return mailDetailsDAO;
  }

  public MailDetailsBOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }



  @Override
  public List<HashMap<String,String>> getMailDetails(long candidateId)
      throws SQLException, DbHandleException {

    MailDetailsDAO mailDetailsDAO = new MailDetailsDAOImpl(dbHandle);
    List<HashMap<String,String>> mailDetails = mailDetailsDAO.getMailDetails(candidateId);

    return mailDetails;
  }

  @Override
  public int create(MailDetails mailDetails) throws SQLException, DbHandleException {

    MailDetailsDAO mailDetailsDAO = new MailDetailsDAOImpl(dbHandle);

    int mailId = mailDetailsDAO.create(mailDetails);

    return mailId;
  }
}
