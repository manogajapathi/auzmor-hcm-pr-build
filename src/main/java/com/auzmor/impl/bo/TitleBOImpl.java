package com.auzmor.impl.bo;

import com.auzmor.bo.TitleBO;
import com.auzmor.dao.EmployeeDAO;
import com.auzmor.dao.JobDAO;
import com.auzmor.impl.dao.EmployeeDAOImpl;
import com.auzmor.impl.dao.JobDAOImpl;
import com.auzmor.impl.dao.TitleDAOImpl;
import com.auzmor.impl.enumarator.TitleEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.TitleModel;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;

public class TitleBOImpl implements TitleBO {
  private static Logger logger = LoggerFactory.getLogger(TitleBOImpl.class);

  /**
   * Add a new location in a particular organization
   * @param dbHandle
   * @param titleModel
   * @return Appropirate BO Enum
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public ReturnObject<TitleEnum, JsonObject> addTitle(DbHandle dbHandle, TitleModel titleModel)
      throws SQLException,DbHandleException {
    TitleDAOImpl titleDAO = new TitleDAOImpl(dbHandle);
    if (titleDAO.isTitleExistsInOrganization(titleModel)) {
      return new ReturnObjectImpl<TitleEnum, JsonObject>(TitleEnum.EXISTS_ALREADY, new JsonObject());
    }
    int result = titleDAO.insert(titleModel);
    if(result > 0) {
      JsonObject resultObject =  new JsonObject();
      resultObject.put("id", result);
      resultObject.put("title", titleModel.getTitle());
      resultObject.put("organization_id", titleModel.getOrganizationId());
      logger.info("Title Added Successfully"+result);
      return new ReturnObjectImpl<TitleEnum, JsonObject>(TitleEnum.ADDITION_SUCCESS, resultObject);
    } else {
      logger.error("Title did not got added"+result);
      return new ReturnObjectImpl<TitleEnum, JsonObject>(TitleEnum.ADDITION_FAILED, new JsonObject());
    }
  }

  /**
   * Fetch details of particular location
   * @param dbHandle
   * @param titleModel
   * @return appropiate BO Enum
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public ReturnObject<TitleEnum,JsonObject> getTitle(DbHandle dbHandle,TitleModel titleModel)
      throws SQLException,DbHandleException {
    TitleDAOImpl titleDAO = new TitleDAOImpl(dbHandle);
    TitleModel resultModel = titleDAO.getTitleDetails(titleModel);

    JsonObject resultObject =  new JsonObject();
    resultObject.put("id",resultModel.getId());
    resultObject.put("title",resultModel.getTitle());
    resultObject.put("organization_id",resultModel.getOrganizationId());
    logger.info("Name of the Title"+resultObject.getString("title"));
    return new ReturnObjectImpl<TitleEnum, JsonObject>(TitleEnum.VALUE_SUCCESS, resultObject);

  }

  /**
   * Update a particular location
   * @param dbHandle
   * @param requestParams
   * @return appropiate BO Enum
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public ReturnObject<TitleEnum,JsonObject> updateTitle(DbHandle dbHandle,MultiMap requestParams)
      throws SQLException,DbHandleException {
    TitleDAOImpl titleDAO = new TitleDAOImpl(dbHandle);
    TitleModel titleModel = new TitleModel();
    titleModel.setId(Integer.valueOf(requestParams.get("id")));
    titleModel = titleDAO.getTitleDetails(titleModel);

    if (titleModel.getTitle().equals(requestParams.get("title"))) {
      return new ReturnObjectImpl<TitleEnum, JsonObject>(TitleEnum.NO_CHANGES_MADE, new JsonObject());
    }

    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    boolean isUsedInJob = false;
    isUsedInJob = jobDAO.checkFieldOption("title", titleModel.getTitle(),
        (long) titleModel.getOrganizationId());

    if (isUsedInJob) {
      logger.info("Update restricted"+isUsedInJob);
      return new ReturnObjectImpl<TitleEnum, JsonObject>(TitleEnum.RESTRICTED, new JsonObject());
    }

    EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
    boolean deleteFlag = employeeDAO.isFieldOptionPresent("job_title",
        titleModel.getTitle(),titleModel.getOrganizationId());
    if(deleteFlag) {
      logger.error("Can not delete Title"+deleteFlag);
      return new ReturnObjectImpl<TitleEnum, JsonObject>(TitleEnum.CANNOT_DELETE, new JsonObject());
    }

    int updateFlag = titleDAO.updateTitleDetails(requestParams);
    if(updateFlag > 0) {
      JsonObject resultObject = new JsonObject();
      resultObject.put("message","Title updated Successfully");
      logger.info("Updated Title"+updateFlag);
      return new ReturnObjectImpl<TitleEnum, JsonObject>(TitleEnum.UPDATE_SUCCESS, resultObject);
    } else {
      logger.error("Not updated Title"+updateFlag);
      return new ReturnObjectImpl<TitleEnum, JsonObject>(TitleEnum.NO_SUCH_VALUE, new JsonObject());
    }
  }

  /**
   * Delete a particular location
   * @param dbHandle
   * @param titleModel
   * @return appropiate BO Enum
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public ReturnObject<TitleEnum,JsonObject> deleteTitle(DbHandle dbHandle,TitleModel titleModel)
      throws SQLException,DbHandleException {
    TitleDAOImpl titleDAO = new TitleDAOImpl(dbHandle);
    TitleModel dataModel = new TitleModel();
    dataModel = titleDAO.getTitleDetails(titleModel);

    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    boolean isUsedInJob = false;
    isUsedInJob = jobDAO.checkFieldOption("title", dataModel.getTitle(),
        (long) dataModel.getOrganizationId());

    if (isUsedInJob) {
      logger.info("Delete restricted"+isUsedInJob);
      return new ReturnObjectImpl<TitleEnum, JsonObject>(TitleEnum.RESTRICTED, new JsonObject());
    }

    EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
    boolean deleteFlag = employeeDAO.isFieldOptionPresent("job_title",
        dataModel.getTitle(),dataModel.getOrganizationId());
    if(deleteFlag) {
      logger.error("Can not delete Title"+deleteFlag);
      return new ReturnObjectImpl<TitleEnum, JsonObject>(TitleEnum.CANNOT_DELETE, new JsonObject());
    }


    int updateFlag = titleDAO.deleteTitle(titleModel);
    if(updateFlag > 0) {
      JsonObject resultObject = new JsonObject();
      resultObject.put("message","Title deleted Successfully");
      logger.info("Deleted Title :"+updateFlag);
      return new ReturnObjectImpl<TitleEnum, JsonObject>(TitleEnum.DELETE_SUCCESS, resultObject);
    } else {
      logger.error("Not Able to Delete"+updateFlag);
      return new ReturnObjectImpl<TitleEnum, JsonObject>(TitleEnum.NO_SUCH_VALUE, new JsonObject());
    }
  }

  /**
   * Fetch all locations in a particular organization
   * @param dbHandle
   * @param titleModel
   * @return appropiate BO Enum
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public ReturnObject<TitleEnum,JsonArray> getAllTitles(DbHandle dbHandle, TitleModel titleModel)
      throws SQLException,DbHandleException {
    TitleDAOImpl titleDAO = new TitleDAOImpl(dbHandle);
    List<TitleModel> resultList = titleDAO.getAllTitles(titleModel);
    if(resultList.size() == 0) {
      logger.error("Un avilable Title"+resultList.size());
      return new ReturnObjectImpl<TitleEnum, JsonArray>(TitleEnum.NO_SUCH_VALUE, new JsonArray());
    } else {
      JsonArray resultArray = new JsonArray();
      for(TitleModel resultModel : resultList) {
        JsonObject resultObject = new JsonObject();
        resultObject.put("id", resultModel.getId());
        resultObject.put("title", resultModel.getTitle());
        resultObject.put("organization_id", resultModel.getOrganizationId());
        resultArray.add(resultObject);
      }
      logger.info("Total number of available titles"+resultArray.size());
      return new ReturnObjectImpl<TitleEnum, JsonArray>(TitleEnum.VALUE_SUCCESS, resultArray);
    }
  }
}
