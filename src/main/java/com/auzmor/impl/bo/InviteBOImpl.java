package com.auzmor.impl.bo;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.Claim;
import com.auzmor.bo.EmployeeBO;
import com.auzmor.bo.InviteBO;
import com.auzmor.dao.InviteDAO;
import com.auzmor.dao.OrganizationDAO;
import com.auzmor.impl.constant.template.InviteTemplateConstant;
import com.auzmor.impl.dao.InviteDAOImpl;
import com.auzmor.impl.dao.OrganizationDAOImpl;
import com.auzmor.impl.enumarator.bo.InviteBOEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.Invite;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.util.DateUtil;
import com.auzmor.impl.util.JWTUtil;
import com.auzmor.impl.util.Mail;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.text.StrSubstitutor;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Invite method Business operation
 */
public class InviteBOImpl implements InviteBO {

  private static Logger logger = LoggerFactory.getLogger(InviteBOImpl.class);

  private DbHandle dbHandle;
  private InviteDAO inviteDAO;

  public InviteBOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  /**
   * @param email
   * @param organizationId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Charles Sam Dilip
   * @lastModifiedDate 11/FEB/2018
   */
  public InviteBOEnum inviteNewEmployee(String email, Long organizationId) throws SQLException,
      DbHandleException {
    EmployeeBO employeeBO = new EmployeeBOImpl(this.dbHandle);
    if (employeeBO.isEmployeeExists(email, organizationId)) {
      logger.info("Employee already exists");
      return InviteBOEnum.EXISTS_ALREADY;
    }

    InviteDAO inviteDAO = new InviteDAOImpl(this.dbHandle);
    Invite invite = inviteDAO.getActiveInviteForEmail(email, organizationId);
    if (null != invite) {
      logger.info("Invite already pending");
      return InviteBOEnum.PENDING;
    }
    invite = addInvite(email, organizationId);
    if (!sendMail(this.dbHandle, invite, InviteBOEnum.MAIL_TYPE_INVITE)) {
      logger.info("Failed to send email");
      return InviteBOEnum.SEND_FAILED;
    }
    logger.info("Successfully invited");
    return InviteBOEnum.INVITE_SUCCESS;
  }

  /**
   * @param paramsMap
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Charles Sam Dilip
   * @lastModifiedDate 07/FEB/2018
   */
  @Override
  public ReturnObject<InviteBOEnum, Employee> addInvitedEmployee(MultiMap paramsMap)
      throws SQLException, DbHandleException {

    logger.info("Adding a Invited Employee");
    String token = paramsMap.get("token");
    String email = paramsMap.get("email");
    Long organizationId = Long.parseLong(paramsMap.get("organization_id"));
    String inviteKey = getKeyFromToken(token);
    InviteDAO inviteDAO = new InviteDAOImpl(this.dbHandle);
    Invite invite = inviteDAO.getInvite(inviteKey);
    if (null == invite) {
      logger.info("No pending invite");
      return new ReturnObjectImpl<>(InviteBOEnum.NO_INVITE_PENDING, null);
    }
    if (!isInviteValid(invite, email, organizationId)) {
      logger.info("Token not valid");
      return new ReturnObjectImpl<>(InviteBOEnum.TOKEN_NOT_VALID, null);
    }

    EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
    if (employeeBO.isEmployeeExists(email, organizationId)) {
      return new ReturnObjectImpl<>(InviteBOEnum.EXISTS_ALREADY, null);
    }
    paramsMap.set("personal_email", email);
    paramsMap.set("work_email", email);
    paramsMap.set("onboarding_status","Packet Not Sent");
    paramsMap.set("access_level","employee");

    paramsMap = setDummies(paramsMap);
    Employee employee = employeeBO.addNewEmployee(paramsMap);
    if (employee.getEmployee_id() > 0) {
      inviteDAO.updateInviteAsAccepted(inviteKey);
      if (!sendMail(dbHandle, invite, InviteBOEnum.MAIL_TYPE_SIGNUP_THANKS)) {
        return new ReturnObjectImpl<>(InviteBOEnum.SEND_FAILED, employee);
      }
      return new ReturnObjectImpl<>(InviteBOEnum.EMPLOYEE_ADDED, employee);
    } else {
      logger.info("unable to add employee");
      return new ReturnObjectImpl<>(InviteBOEnum.EMPLOYEE_ADD_FAILED, null);
    }
  }

  /**
   * Resend a invitation
   *
   * @param email
   * @param organizationId
   * @return appropriate BO Enum accordingly
   * @throws SQLException
   * @throws DbHandleException
   * @author Charles Sam Dilip
   * @lastModifiedDate 07/FEB/2018
   */
  @Override
  public InviteBOEnum inviteResend(String email, Long organizationId)
      throws SQLException, DbHandleException {
    logger.info("Invite Resesnd");
    EmployeeBO employeeBO = new EmployeeBOImpl(this.dbHandle);
    if (employeeBO.isEmployeeExists(email, organizationId)) {
      logger.info("Employee already exists in the system");
      return InviteBOEnum.EXISTS_ALREADY;
    }
    InviteDAO inviteDAO = new InviteDAOImpl(this.dbHandle);
    Invite invite = inviteDAO.getActiveInviteForEmail(email, organizationId);
    if (null == invite) {
      logger.info("No Pending invite found");
      return InviteBOEnum.NO_INVITE_PENDING;
    }

    inviteDAO.delete(invite);
    invite = addInvite(invite.getEmail(), invite.getOrganizationId());
    if (!sendMail(this.dbHandle, invite, InviteBOEnum.MAIL_TYPE_INVITE)) {
      logger.info("Sending Invite failed");
      return InviteBOEnum.SEND_FAILED;
    }

    return InviteBOEnum.INVITE_SUCCESS;
  }

  /**
   * @param email
   * @param organizationId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Charles Sam Dilip
   * @lastModifiedDate 11/FEB/2018
   */
  public InviteBOEnum cancelInvite(String email, Long organizationId) throws SQLException,
      DbHandleException {
    InviteDAO inviteDAO = new InviteDAOImpl(this.dbHandle);
    Invite invite = inviteDAO.getActiveInviteForEmail(email, organizationId);
    if (null == invite) {
      logger.info("No Invite found");
      return InviteBOEnum.NO_INVITE_PENDING;
    }

    inviteDAO.delete(invite);
    return InviteBOEnum.CANCEL_SUCCESS;
  }

  /**
   * @param email
   * @param organizationId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Charles Sam Dilip
   * @lastModifiedDate 10/FEB/2018
   */
  public Invite addInvite(String email, long organizationId) throws SQLException,
      DbHandleException {
    Invite invite = new Invite();
    invite.setEmail(email);
    invite.setOrganizationId(organizationId);
    invite.setKey(UUID.randomUUID().toString());

    InviteDAO inviteDAO = new InviteDAOImpl(this.dbHandle);
    long id = inviteDAO.create(invite);

    invite.setId(id);
    return invite;
  }

  /**
   * @param email
   * @param organizationId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Charles Sam Dilip
   * @lastModifiedDate 10/FEB/2018
   */
  public boolean hasInviteForEmail(String email, long organizationId) throws SQLException,
      DbHandleException {
    InviteDAO inviteDAO = new InviteDAOImpl(this.dbHandle);
    if (inviteDAO.hasInviteForEmail(email, organizationId)) {
      return true;
    }
    return false;
  }

  /**
   * @param invite
   * @param email
   * @param organizationId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Charles Sam Dilip
   * @lastModifiedDate 10/FEB/2018
   */
  private boolean isInviteValid(Invite invite, String email,
                                Long organizationId) {
    if (null == invite || !invite.isPending() || invite.getOrganizationId() != organizationId ||
        !invite.getEmail().equals(email)) {
      return false;
    }

    return true;
  }


  /**
   * @param token
   * @return
   * @author Charles Sam Dilip
   * @lastModifiedDate 10/FEB/2018
   */
  public String getKeyFromToken(String token) {
    JWT jwt = null;
    try {
      jwt = JWT.decode(token);
    } catch (JWTDecodeException jwtDecodeException) {
      logger.error("Unable to decode JWT");
      return null;
    }
    if (!"auth0".equals(jwt.getIssuer())) {
      logger.error("Not issued by a valid issuer");
      return null;
    }

    Claim claim = jwt.getClaim("unique_key");
    return claim != null ? claim.asString() : null;
  }

  /**
   * @param inviteKey
   * @return
   * @throws UnsupportedEncodingException
   * @author Charles Sam Dilip
   * @lastModifiedDate 07/FEB/2018
   */
  private static String generateInvitationToken(String inviteKey)
      throws UnsupportedEncodingException {

    String key = JWTUtil.getEncryptKey();
    Algorithm algorithm = Algorithm.HMAC256(key);
    String generatedToken = JWT.create().withIssuer("auth0")
        .withClaim("unique_key", inviteKey)
        .sign(algorithm);

    return generatedToken;
  }


  /**
   * @param token
   * @param organization
   * @return
   * @author Charles Sam Dilip
   * @lastModifiedDate 07/FEB/2018
   */
  private static String getMailBodyFromTemplate(String token, Organization organization,
                                                String mailTemplate) {
    Map<String, String> valueMap = new HashMap<>();
    valueMap.put("organization", organization.getOrganization_name());
    valueMap.put("token", token);
    valueMap.put("url", organization.getHris_url());

    StrSubstitutor inviteTemplateSubtitutor = new StrSubstitutor(valueMap);
    return inviteTemplateSubtitutor.replace(mailTemplate);
  }

  /**
   * @param dbHandle
   * @param invite
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Charles Sam Dilip
   * @lastModifiedDate 07/FEB/2018
   */
  private static boolean sendMail(DbHandle dbHandle, Invite invite, InviteBOEnum inviteBOEnum)
      throws SQLException, DbHandleException {

    String inviteToken = null;
    String subject = null;
    String template = null;
    try {
      OrganizationDAO organizationDAO = new OrganizationDAOImpl(dbHandle);
      Organization organization = organizationDAO.getOrganizationById(invite.getOrganizationId());
      if (InviteBOEnum.MAIL_TYPE_INVITE == inviteBOEnum) {
        logger.info("Genrating mail from invite template");
        inviteToken = generateInvitationToken(invite.getKey());
        subject = "Invite from " + organization.getOrganization_name();
        template = InviteTemplateConstant.INVITE_MAIL_TEMPLATE;
      } else if (InviteBOEnum.MAIL_TYPE_SIGNUP_THANKS == inviteBOEnum) {
        logger.info("Genrating mail from invite signup template");
        subject = "Thanks for sign up with " + organization.getOrganization_name();
        template = InviteTemplateConstant.INVITE_SIGNUP_MAIL_TEMPLATE;
      } else {
        logger.info("Not a valid type of mail");
        return false;
      }

      String mailBody = getMailBodyFromTemplate(inviteToken, organization, template);
      Mail mail = new Mail();
      if (!"success".equals(mail.sendMail(Vertx.vertx(), mailBody,
          invite.getEmail(), subject))) {
        logger.error("Failed to send mail");
        return false;
      }
      return true;
    } catch (UnsupportedEncodingException unexpectedToken) {
      logger.error("Unable to create JWT Token for the invite key" + invite.getKey());
      return false;
    }
  }

  // TODO Remove this one normalized DB

  /**
   * @param params
   * @return
   * @author Charles Sam Dilip
   * @lastModifiedDate 07/FEB/2018
   */
  private MultiMap setDummies(MultiMap params) {
    params.set("status", " ");
    params.set("job_title", " ");
    params.set("manager", " ");
    params.set("location", " ");
    params.set("pay_rate", " ");
    params.set("pay_type", " ");
    params.set("employment_status", " ");
    params.set("primary_phone", " ");
    params.set("hire_date", new DateUtil().getDate().format(Calendar.getInstance().getTime()));

    return params;
  }

  public InviteDAO getInviteDAO() {
    return inviteDAO;
  }
  public void setInviteDAO(InviteDAO inviteDAO) {
    this.inviteDAO = inviteDAO;
  }
}