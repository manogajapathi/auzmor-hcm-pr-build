package com.auzmor.impl.bo;

import com.auzmor.bo.DivisionBO;
import com.auzmor.dao.DivisionDAO;
import com.auzmor.dao.EmployeeDAO;
import com.auzmor.impl.dao.DivisionDAOImpl;
import com.auzmor.impl.dao.EmployeeDAOImpl;
import com.auzmor.impl.enumarator.DivisionEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.FieldOptions.DivisionModel;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;

public class DivisionBOImpl implements DivisionBO {
  private static Logger logger = LoggerFactory.getLogger(DivisionBOImpl.class);

  /**
   * Add a new division
   * @param dbHandle
   * @param divisionModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  @Override
  public ReturnObject<DivisionEnum, JsonObject> addDivision(DbHandle dbHandle,
                                                              DivisionModel divisionModel)
      throws SQLException,DbHandleException {
    DivisionDAO divisionDAO = new DivisionDAOImpl(dbHandle);
    if (divisionDAO.isDivisionExistsInOrganization(divisionModel)) {
      return new ReturnObjectImpl<>(DivisionEnum.EXISTS_ALREADY, new JsonObject());
    }
    int result = divisionDAO.insert(divisionModel);
    if(result > 0) {
      JsonObject resultObject =  new JsonObject();
      resultObject.put("division_id", result);
      resultObject.put("division_name", divisionModel.getDivisionName());
      resultObject.put("organization_id", divisionModel.getOrganizationId());
      logger.info("Division create : "+result);
      return new ReturnObjectImpl<DivisionEnum, JsonObject>
          (DivisionEnum.ADDITION_SUCCESS,resultObject);
    } else {
      return new ReturnObjectImpl<DivisionEnum, JsonObject>
          (DivisionEnum.ADDITION_FAILED,new JsonObject());
    }
  }

  /**
   * Fetch division details
   * @param dbHandle
   * @param divisionModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  @Override
  public ReturnObject<DivisionEnum,JsonObject> getDivision(DbHandle dbHandle,
                                                           DivisionModel divisionModel)
      throws SQLException,DbHandleException {
    DivisionDAO divisionDAO = new DivisionDAOImpl(dbHandle);
    DivisionModel resultModel = divisionDAO.getDivisionDetails(divisionModel);

    JsonObject resultObject =  new JsonObject();
    resultObject.put("division_id", resultModel.getDivisionId());
    resultObject.put("division_name", resultModel.getDivisionName());
    resultObject.put("organization_id", resultModel.getOrganizationId());
    logger.info("Division details : "+resultModel.getDivisionId());
    return new ReturnObjectImpl<DivisionEnum, JsonObject>
          (DivisionEnum.VALUE_SUCCESS, resultObject);
  }

  /**
   * Update division details
   * @param dbHandle
   * @param requestParams
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  @Override
  public ReturnObject<DivisionEnum,JsonObject> updateDivision(DbHandle dbHandle,
                                                                  MultiMap requestParams)
      throws SQLException,DbHandleException {
    DivisionDAO divisionDAO = new DivisionDAOImpl(dbHandle);
    int updateFlag = divisionDAO.updateDivisionDetails(requestParams);
    if(updateFlag > 0) {
      JsonObject resultObject = new JsonObject();
      resultObject.put("message", "Division updated Successfully");
      logger.info("Division update : "+updateFlag);
      return new ReturnObjectImpl<DivisionEnum, JsonObject>
          (DivisionEnum.UPDATE_SUCCESS, resultObject);
    } else {
      return new ReturnObjectImpl<DivisionEnum, JsonObject>
          (DivisionEnum.NO_SUCH_VALUE, new JsonObject());
    }
  }

  /**
   * Delete division
   * @param dbHandle
   * @param divisionModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  @Override
  public ReturnObject<DivisionEnum,JsonObject> deleteDivision(DbHandle dbHandle,
                                                              DivisionModel divisionModel)
      throws SQLException,DbHandleException {
    DivisionDAO divisionDAO = new DivisionDAOImpl(dbHandle);
    DivisionModel dataModel = new DivisionModel();
    dataModel = divisionDAO.getDivisionDetails(divisionModel);

    if ( StringUtil.printValidString(dataModel.getDivisionName()).equals("")) {
      return new ReturnObjectImpl<DivisionEnum, JsonObject>
          (DivisionEnum.NO_SUCH_VALUE, new JsonObject());
    }

    EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
    boolean deleteFlag = employeeDAO.isFieldOptionPresent("division",
        dataModel.getDivisionName(),dataModel.getOrganizationId());
    if(deleteFlag) {
      return new ReturnObjectImpl<DivisionEnum, JsonObject>
          (DivisionEnum.CANNOT_DELETE, new JsonObject());
    }

    int updateFlag = divisionDAO.deleteDivision(divisionModel);
    if(updateFlag > 0) {
      JsonObject resultObject = new JsonObject();
      resultObject.put("message", "Division deleted Successfully");
      logger.info("Division delete : "+updateFlag);
      return new ReturnObjectImpl<DivisionEnum, JsonObject>
          (DivisionEnum.DELETE_SUCCESS, resultObject);
    } else {
      return new ReturnObjectImpl<DivisionEnum, JsonObject>
          (DivisionEnum.NO_SUCH_VALUE, new JsonObject());
    }
  }

  /**
   * Fetch all divisions
   * @param dbHandle
   * @param divisionModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 15-02-2018
   */
  @Override
  public ReturnObject<DivisionEnum,JsonArray> getAllDivisions(DbHandle dbHandle,
                                                              DivisionModel divisionModel)
      throws SQLException,DbHandleException {
    DivisionDAO divisionDAO = new DivisionDAOImpl(dbHandle);
    List<DivisionModel> resultList = divisionDAO.getAllDivisions(divisionModel);
    if(resultList.size() == 0) {
      return new ReturnObjectImpl<DivisionEnum, JsonArray>
          (DivisionEnum.NO_SUCH_VALUE, new JsonArray());
    } else {
      JsonArray resultArray = new JsonArray();
      for(DivisionModel resultModel : resultList) {
        JsonObject resultObject = new JsonObject();
        resultObject.put("division_id", resultModel.getDivisionId());
        resultObject.put("division_name", resultModel.getDivisionName());
        resultObject.put("organization_id", resultModel.getOrganizationId());
        resultArray.add(resultObject);
      }
      logger.info("Division details : "+resultArray.size());
      return new ReturnObjectImpl<DivisionEnum, JsonArray>
          (DivisionEnum.VALUE_SUCCESS, resultArray);
    }
  }
}
