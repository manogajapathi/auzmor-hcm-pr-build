package com.auzmor.impl.bo;

import com.auzmor.bo.CandidateBO;
import com.auzmor.bo.CandidateHiringProcessBO;
import com.auzmor.bo.EmployeeBO;
import com.auzmor.bo.OrganizationBO;
import com.auzmor.dao.*;
import com.auzmor.impl.dao.*;
import com.auzmor.impl.enumarator.bo.CandidateHiringProcessEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.impl.models.CandidateR.CommentCandidate;
import com.auzmor.impl.models.CandidateR.HiringProcessCandidate;
import com.auzmor.impl.util.DateUtil;
import com.auzmor.impl.util.Mail;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CandidateHiringProcessBOImpl implements CandidateHiringProcessBO {
  private static Logger logger = LoggerFactory.getLogger(CandidateHiringProcessBOImpl.class);
  private DbHandle dbHandle;
  private CandidateHiringDAO candidateHiringDAO;

  public CandidateHiringProcessBOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  /**
   * @param dbHandle
   * @param candidateHiringProcessModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author SadhanaVenkatesan
   */
  @Override
  public CandidateHiringProcessEnum insertCandidateStatus(DbHandle dbHandle,
                                                          HiringProcessCandidate
                                                              candidateHiringProcessModel)
      throws SQLException, DbHandleException {
    CandidateHiringProcessDAOImpl candidateHiringProcessDao = new
        CandidateHiringProcessDAOImpl(this.dbHandle);

    CandidateDAO candidateDAO = new CandidateDAOImpl(this.dbHandle);
    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
    Mail mail = new Mail();
    CandidateR candidate = candidateDAO.
        getCandidate(candidateHiringProcessModel.getCandidateId());
    OrganizationBO organizationBO = new OrganizationBOImpl(dbHandle);
    Organization organization = organizationBO.
        getOrganizationDetails(candidate.getOrganizationId());
    String organizationName = organization.getOrganization_name().substring(0, 1).toUpperCase() +
        organization.getOrganization_name().substring(1);
    logger.info("Candidate Status Update method");

    if (candidateHiringProcessModel.getStatus().equalsIgnoreCase("Hire")) {
      long candidateLoginId = candidate.getUserId(); // getting login id to disable remaining job for this userId
      String hiredJobTitle = "";
      Job hiredJob = jobDAO.getJobDetails(candidate.getJobId()); // getting hired job title
      if (hiredJob != null) {
        hiredJobTitle = hiredJob.getTitle();
      }
      List<HashMap<String, String>> getCandidateList = candidateDAO.
          getCandidateId(candidateLoginId);
      for (HashMap<String, String> map : getCandidateList) {
        for (Map.Entry<String, String> mapEntry : map.entrySet()) {
          String key = mapEntry.getKey();
          String value = mapEntry.getValue();
          long candidateId = 0;
          if (key.equalsIgnoreCase("candidate_id")) {
            candidateId = Long.parseLong(value);
          }
          candidate = candidateDAO.getCandidate(candidateId);

          HiringProcessCandidate hiringProcessCandidateOldStatus = candidateHiringProcessDao.
              getHiringProcess(candidateId);

          if (candidateHiringProcessModel.getCandidateId() != candidateId &&
              !candidate.getStatus().equalsIgnoreCase("Hire") &&
              !hiringProcessCandidateOldStatus.getCandidateProcess().equalsIgnoreCase("Archive to Talent Pool")) {

            // Hiringprocess Insert
            HiringProcessCandidate hiringProcessCandidate = new HiringProcessCandidate();
            hiringProcessCandidate.setCandidateProcess("Took Another Position");
            hiringProcessCandidate.setStatus("Not Hired");
            hiringProcessCandidate.setCandidateId(candidateId);
            hiringProcessCandidate.setUserId((long) 0);

            int count = candidateHiringProcessDao.create(hiringProcessCandidate);
            String comments = "moved from " + hiringProcessCandidateOldStatus.getCandidateProcess()
                + " to " + hiringProcessCandidate.getCandidateProcess();

            if (count > 0) {
              // candidate status update
              candidateDAO.updateCandidateStatus("Not Hired",
                  candidateId);

              String adminName = candidate.getFirstName() + " " + candidate.getLastName();
              CommentCandidate commentsModel = new CommentCandidate();
              commentsModel.setAdminName(StringUtil.printValidString(adminName));
              commentsModel.setComments(comments);
              java.util.Date dNow = new java.util.Date();
              String currentDate = new DateUtil().getDate().format(dNow);
              try {
                commentsModel.setCommentsDate(new DateUtil().getDate().parse(currentDate));
              } catch (ParseException e) {
                e.printStackTrace();
              }
              commentsModel.setCandidateId(candidateId);
              commentsModel.setUserId(0);
              commentsModel.setSystemGenerated(true);
              CandidateCommentDAO candidateCommentDAO = new CandidateCommentDAOImpl(dbHandle);
              // comments insertion
              int commentCount = candidateCommentDAO.create(commentsModel);
              if (commentCount > 0) {
                if (candidate.getJobId() != 0) {
                  Job job = jobDAO.getJobDetails(candidate.getJobId());
                  if (job != null) {
                    if (job.getHiring_lead().length() > 0
                        || !StringUtil.printValidString(job.getHiring_lead()).equals("")) {

                      long hiringLeadId = Long.parseLong(job.getHiring_lead());
                      Employee employee = employeeBO.getEmployeeDetails(hiringLeadId);
                      CandidateBO candidateBO = new CandidateBOImpl(dbHandle);
                      candidateBO.sendStatusUpdatesToAdmin(hiringProcessCandidate.getStatus(),
                          candidateHiringProcessModel.getStatus(),
                          hiringProcessCandidate.getStatus(), candidateId, candidateHiringProcessModel.getUserId());

                      if (employee != null) {

                        String body = "<html><body><p>Dear Hiring lead,<br/><br/>" +
                            " This is to inform you that <b>" + adminName + "</b>  " +
                            " who had applied for the position of <b>" + job.getTitle() + "</b> " +
                            " has already been hired for the position of <b>" + hiredJobTitle + ".</b> " +
                            " Kindly do not process this candidate for any pending rounds " +
                            " and remove him/her from possible wait lists, if any.<br/><br/><br/>" +
                            " Thank you,\n<br/> " + organizationName + " Team <br/> " +
                            "--------------------------------------------------------------\n<br/>" +
                            "This is a system generated mail.\n<br/>" +
                            "Please do not reply to this mail\n<br/>" +
                            "-------------------------------------------------------------- " +
                            "</body></html>";

                        String subjectName = "Pause interview process-For HiringLead";

                        // same mail has to go to collaborator also
                        if (!"success".equals(mail.sendMail(Vertx.vertx(),
                            body,
                            employee.getWork_email(), subjectName))) {
                          logger.error("Sending Mail Failed");
                          return CandidateHiringProcessEnum.SEND_MAIL_FAILED;
                        }
                        logger.info("mail send success");
                      }
                    }
                  }
                  long collaboratorId = 0;

                  List<HashMap<String, String>> getCollaboratorList = jobDAO.
                      getCollaboratorList(candidate.getJobId());

                  for (HashMap<String, String> HashMap : getCollaboratorList) {
                    for (Map.Entry<String, String> HashMapEntry : HashMap.entrySet()) {
                      String keyCollaborator = HashMapEntry.getKey();
                      String valueCollaborator = HashMapEntry.getValue();

                      if (keyCollaborator.equalsIgnoreCase("collaborator_id")) {

                        if (!value.equals("")) {
                          collaboratorId = Long.parseLong(valueCollaborator);
                          Employee employee = employeeBO.getEmployeeDetails(collaboratorId);
                          if (employee != null) {

                            String body = "<html><body><p>Dear Collaborator,<br/><br/>" +
                                " This is to inform you that <b>" + adminName + "</b>  " +
                                " who had applied for the position of <b>" + job.getTitle() + "</b> " +
                                " has already been hired for the position of <b>" + hiredJobTitle + ".</b> " +
                                " Kindly do not process this candidate for any pending rounds " +
                                " and remove him/her from possible wait lists, if any.<br/><br/><br/>" +
                                " Thank you,\n<br/> " + organizationName + " Team <br/> " +
                                "--------------------------------------------------------------\n<br/>" +
                                "This is a system generated mail.\n<br/>" +
                                "Please do not reply to this mail\n<br/>" +
                                "-------------------------------------------------------------- " +
                                "</body></html>";
                            String subjectName = "Pause interview process-For Collaborator";

                            // same mail has to go to collaborator also
                            if (!"success".equals(mail.sendMail(Vertx.vertx(),
                                body,
                                employee.getWork_email(), subjectName))) {
                              logger.error("Sending Mail Failed");
                              return CandidateHiringProcessEnum.SEND_MAIL_FAILED;
                            }
                            logger.info("mail send success");
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }/* else {
      // if interview process is completed collaborator also get notified that its completed for particular candidate
      if (candidate.getStatus().equalsIgnoreCase("Interview")) {
        CandidateInterviewDAO candidateInterviewDAO = new CandidateInterviewDAOImpl(dbHandle);
      *//*  InterviewProcessCandidate interviewProcessCandidate = candidateInterviewDAO.
            getInterviewProcessDetails(candidateHiringProcessModel.getCandidateId());
        long collaboratorId = interviewProcessCandidate.getCollaboratorId();
        if(collaboratorId != 0){
          JobTaskDAO jobTaskDAO = new JobTaskDAOImpl(dbHandle);
          jobTaskDAO.updateCompletedFlag(collaboratorId);*//*
        candidateInterviewDAO.updateCompletedStatus(candidateHiringProcessModel.getCandidateId());
        *//*}*//*
      }
    }*/
      int hiringId = candidateHiringProcessDao.create(candidateHiringProcessModel);
      if (hiringId > 0) {
        logger.info("Candidate hiring process Success");
        candidateDAO.updateCandidateStatus(candidateHiringProcessModel.getStatus(),
            candidateHiringProcessModel.getCandidateId());
        logger.info("Candidate Status inserted Successfully!");
        return CandidateHiringProcessEnum.SUCCESS;
      }
    logger.error("Candidate Status insertion Failed!");
    return CandidateHiringProcessEnum.FAILED;
  }

  /**
   * @param candidateId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author SadhanaVenkatesan
   */
  @Override
  public HiringProcessCandidate getHiringProcess(long candidateId)
      throws SQLException, DbHandleException {
    CandidateHiringProcessDAOImpl candidateHiringProcessDao = new
        CandidateHiringProcessDAOImpl(dbHandle);
    HiringProcessCandidate hiringProcessCandidate =
        candidateHiringProcessDao.getHiringProcess(candidateId);
    logger.info("Hiring Process fetched Successfully!");
    return hiringProcessCandidate;
  }

  @Override
  public String getStatus(long candidateId, String status)
      throws SQLException, DbHandleException {
    CandidateHiringProcessDAOImpl candidateHiringProcessDao = new
        CandidateHiringProcessDAOImpl(dbHandle);
    String statusCheck = candidateHiringProcessDao.getCandidateProcess(candidateId, status);
    logger.info("Status fetched Successfully!");
    return statusCheck;
  }

  public CandidateHiringDAO getCandidateHiringDAO() {
    return candidateHiringDAO;
  }

  public void setCandidateHiringDAO(CandidateHiringDAO candidateHiringDAO) {
    this.candidateHiringDAO = candidateHiringDAO;
  }
}

