package com.auzmor.impl.bo;

import com.auzmor.bo.LocationBO;
import com.auzmor.dao.EmployeeDAO;
import com.auzmor.dao.JobDAO;
import com.auzmor.impl.dao.EmployeeDAOImpl;
import com.auzmor.impl.dao.JobDAOImpl;
import com.auzmor.impl.dao.LocationDAOImpl;
import com.auzmor.impl.enumarator.LocationEnum;
import com.auzmor.impl.enumarator.bo.JobDescriptionEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.LocationModel;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;

public class LocationBOImpl implements LocationBO {
  private static Logger logger = LoggerFactory.getLogger(LocationBOImpl.class);

  /**
   * Add a new location in a particular organization
   * @param dbHandle
   * @param locationModel
   * @return Appropirate BO Enum
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public ReturnObject<LocationEnum, JsonObject> addLocation(DbHandle dbHandle, LocationModel locationModel)
      throws SQLException,DbHandleException {
    LocationDAOImpl locationDao = new LocationDAOImpl(dbHandle);
    if (locationDao.isLocationExistsInOrganization(locationModel)) {
      return new ReturnObjectImpl<LocationEnum, JsonObject>(LocationEnum.EXISTS_ALREADY, new JsonObject());
    }
    int result = locationDao.insert(locationModel);
    if(result > 0) {
      JsonObject resultObject =  new JsonObject();
      resultObject.put("id", result);
      resultObject.put("location", locationModel.getLocation());
      resultObject.put("organization_id", locationModel.getOrganizationId());
      resultObject.put("company_address_line1", locationModel.getCompanyAddressLine1());
      resultObject.put("company_address_line2", locationModel.getCompanyAddressLine2());
      resultObject.put("city", locationModel.getCity());
      resultObject.put("zip", locationModel.getZip());
      resultObject.put("country", locationModel.getCountry());
      resultObject.put("state", locationModel.getState());
      resultObject.put("message", "Location added successfully");
      logger.info("Location insert : "+result);
      return new ReturnObjectImpl<LocationEnum, JsonObject>(LocationEnum.ADDITION_SUCCESS,resultObject);
    } else {
      return new ReturnObjectImpl<LocationEnum, JsonObject>(LocationEnum.ADDITION_FAILED,new JsonObject());
    }
  }

  /**
   * Fetch details of particular location
   * @param dbHandle
   * @param locationModel
   * @return appropiate BO Enum
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public ReturnObject<LocationEnum,JsonObject> getLocation(DbHandle dbHandle,LocationModel locationModel)
      throws SQLException,DbHandleException {
    LocationDAOImpl locationDao = new LocationDAOImpl(dbHandle);
    LocationModel resultModel = locationDao.getLocationDetails(locationModel);

    JsonObject resultObject =  new JsonObject();
    resultObject.put("id", resultModel.getId());
    resultObject.put("location", resultModel.getLocation());
    resultObject.put("organization_id", resultModel.getOrganizationId());
    resultObject.put("company_address_line1", resultModel.getCompanyAddressLine1());
    resultObject.put("company_address_line2", resultModel.getCompanyAddressLine2());
    resultObject.put("city", resultModel.getCity());
    resultObject.put("zip", resultModel.getZip());
    resultObject.put("country", resultModel.getCountry());
    resultObject.put("state", resultModel.getState());
    logger.info("Location details : "+resultObject.getString("location"));
    return new ReturnObjectImpl<LocationEnum, JsonObject>(LocationEnum.VALUE_SUCCESS, resultObject);
  }

  /**
   * Update a particular location
   * @param dbHandle
   * @param requestParams
   * @return appropiate BO Enum
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public ReturnObject<LocationEnum,JsonObject> updateLocation(DbHandle dbHandle,MultiMap requestParams)
      throws SQLException,DbHandleException {
    LocationDAOImpl locationDao = new LocationDAOImpl(dbHandle);
    LocationModel locationModel = new LocationModel();
    locationModel.setId(Integer.valueOf(requestParams.get("id")));

    locationModel = locationDao.getLocationDetails(locationModel);

    if (locationModel.getLocation().equals(requestParams.get("location"))
        && StringUtils.defaultString(locationModel.getCompanyAddressLine1()).equals(
        StringUtils.defaultString(requestParams.get("company_address_line1")))) {
      return new ReturnObjectImpl<LocationEnum, JsonObject>
          (LocationEnum.NO_CHANGES_MADE, new JsonObject());
    }

    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    boolean isUsedInJob = false;
    isUsedInJob = jobDAO.checkFieldOption("location", locationModel.getLocation(),
        (long) locationModel.getOrganizationId());

    if (isUsedInJob && !requestParams.get("location").equals(locationModel.getLocation())) {
      return new ReturnObjectImpl<LocationEnum, JsonObject>
          (LocationEnum.RESTRICTED, new JsonObject());
    }

    EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
    boolean deleteFlag = employeeDAO.isFieldOptionPresent("location",
        locationModel.getLocation(),locationModel.getOrganizationId());
    if(deleteFlag) {
      return new ReturnObjectImpl<LocationEnum, JsonObject>(LocationEnum.CANNOT_DELETE, new JsonObject());
    }

    int updateFlag = locationDao.updateLocationDetails(requestParams);
    if(updateFlag > 0) {
      JsonObject resultObject = new JsonObject();
      logger.info("Location update : "+updateFlag);
      resultObject.put("message","Location updated Successfully");
      return new ReturnObjectImpl<LocationEnum, JsonObject>(LocationEnum.UPDATE_SUCCESS, resultObject);
    } else {
      return new ReturnObjectImpl<LocationEnum, JsonObject>(LocationEnum.NO_SUCH_VALUE, new JsonObject());
    }
  }

  /**
   * Delete a particular location
   * @param dbHandle
   * @return appropiate BO Enum
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public ReturnObject<LocationEnum,JsonObject> deleteLocation(DbHandle dbHandle,LocationModel locationModel)
      throws SQLException,DbHandleException {
    LocationDAOImpl locationDao = new LocationDAOImpl(dbHandle);
    LocationModel dataModel = new LocationModel();
    dataModel = locationDao.getLocationDetails(locationModel);
    EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
    boolean deleteFlag = employeeDAO.isFieldOptionPresent("location",
        dataModel.getLocation(),dataModel.getOrganizationId());
    if(deleteFlag) {
      return new ReturnObjectImpl<LocationEnum, JsonObject>(LocationEnum.CANNOT_DELETE, new JsonObject());
    }

    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    boolean isUsedInJob = false;
    isUsedInJob = jobDAO.checkFieldOption("location", dataModel.getLocation(),
        (long) dataModel.getOrganizationId());

    if (isUsedInJob) {
      return new ReturnObjectImpl<LocationEnum, JsonObject>
          (LocationEnum.RESTRICTED, new JsonObject());
    }

    int updateFlag = locationDao.deleteLocation(locationModel);
    if(updateFlag > 0) {
      JsonObject resultObject = new JsonObject();
      logger.info("Location deleted : "+updateFlag);
      resultObject.put("message","Location deleted Successfully");
      return new ReturnObjectImpl<LocationEnum, JsonObject>(LocationEnum.DELETE_SUCCESS, resultObject);
    } else {
      return new ReturnObjectImpl<LocationEnum, JsonObject>(LocationEnum.NO_SUCH_VALUE, new JsonObject());
    }
  }

  /**
   * Fetch all locations in a particular organization
   * @param dbHandle
   * @return appropiate BO Enum
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 05/FEB/2018
   */
  @Override
  public ReturnObject<LocationEnum,JsonArray> getAllLocations(DbHandle dbHandle, LocationModel locationModel)
      throws SQLException,DbHandleException {
    LocationDAOImpl locationDao = new LocationDAOImpl(dbHandle);
    List<LocationModel> resultList = locationDao.getAllLocations(locationModel);
    if(resultList.size() == 0) {
      return new ReturnObjectImpl<LocationEnum, JsonArray>(LocationEnum.NO_SUCH_VALUE, new JsonArray());
    } else {
      JsonArray resultArray = new JsonArray();
      for(LocationModel resultModel : resultList) {
        JsonObject resultObject = new JsonObject();
        resultObject.put("id", resultModel.getId());
        resultObject.put("location", resultModel.getLocation());
        resultObject.put("organization_id", resultModel.getOrganizationId());
        resultObject.put("company_address_line1", resultModel.getCompanyAddressLine1());
        resultObject.put("company_address_line2", resultModel.getCompanyAddressLine2());
        resultObject.put("city", resultModel.getCity());
        resultObject.put("zip", resultModel.getZip());
        resultObject.put("country", resultModel.getCountry());
        resultObject.put("state", resultModel.getState());
        resultArray.add(resultObject);
      }
      logger.info("Location size : "+resultArray.size());
      return new ReturnObjectImpl<LocationEnum, JsonArray>(LocationEnum.VALUE_SUCCESS, resultArray);
    }
  }
}