package com.auzmor.impl.bo;

import com.auzmor.bo.OrganizationBO;
import com.auzmor.dao.OrganizationDAO;
import com.auzmor.impl.constant.nginx.NginxConstant;
import com.auzmor.impl.constant.template.NginxTemplateConstant;
import com.auzmor.impl.dao.OrganizationDAOImpl;
import com.auzmor.impl.enumarator.bo.OrganizationBOEnum;
import com.auzmor.impl.enumarator.file.FileTypeEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.FileHelper;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.OrganizationR;
import com.auzmor.impl.model.Role;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.Vertx;
import org.apache.commons.text.StrSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrganizationBOImpl implements OrganizationBO {
  private static Logger logger = LoggerFactory.getLogger(OrganizationBOImpl.class);
  private DbHandle dbHandle;
  private OrganizationDAO organizationDAO;

  public OrganizationBOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  /**
   * @param template
   * @param fileName
   * @param map
   * @author Charles Sam Dilip
   * @lastModifiedDate 04/MAR/2018
   */
  private static void writeConfFile(String template, String fileName, Map<String, String> map) {
    StrSubstitutor strSubstitutor = new StrSubstitutor(map, "${{", "}}");
    String content = strSubstitutor.replace(template);
    FileHelper.write(fileName + FileTypeEnum.ENV_CONF.getExtension(),
        NginxConstant.CONF_DIRECTORY_PATH, content.getBytes());
  }

  public void setOrganizationDAO(OrganizationDAO organizationDAO) {
    this.organizationDAO = organizationDAO;
  }

  /**
   * @param domain
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Charles Sam Dilip
   * @lastModifiedDate 03/MAR/2018
   */
  @Override
  public boolean isDomainAvailable(String domain) throws SQLException, DbHandleException {
    return !(new OrganizationDAOImpl(this.dbHandle).isDomainRegistered(domain));
  }

  /**
   * @param organizationId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Charles Sam Dilip
   * @lastModifiedDate 07/MAR/2018
   */
  public Organization getOrganizationDetails(Long organizationId)
      throws SQLException, DbHandleException {

    return new OrganizationDAOImpl(this.dbHandle).getOrganizationById(organizationId);
  }

  /**
   * @param domainName
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Charles Sam Dilip
   * @lastModifiedDate 21/MAR/2018
   */
  @Override
  public Organization getOrganizationWithDomainName(String domainName) throws SQLException, DbHandleException {
    return new OrganizationDAOImpl(this.dbHandle).getOrganizationByDomainName(domainName);
  }

  @Override
  public void addNewAtsDomain(String atsDomain, long organizationId) {
    String nginxConfDirPath = NginxConstant.CONF_DIRECTORY_PATH;

    String sslCert = Vertx.currentContext().config().getString("ssl.certPath");
    String sslKey = Vertx.currentContext().config().getString("ssl.certKeyPath");
    Map<String, String> map = new HashMap<String, String>() {{
      put("sslCrt", sslCert);
      put("sslCrtKey", sslKey);
      put("domain", atsDomain);
    }};

    writeConfFile(NginxTemplateConstant.ATS_SERVER_TEMPLATE, atsDomain, map);
  }

  /**
   * @param atsDomain
   * @param careerDomain
   * @param organizationId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Charles Sam Dilip
   * @lastModifiedDate 03/MAR/2018
   */
  @Override
  public OrganizationBOEnum updateDomains(String atsDomain, String careerDomain,
                                          long organizationId, String auzmorKeyFile,
                                          String auzmorCrtFile, String careerKeyFile,
                                          String careerCrtFile)
      throws SQLException, DbHandleException {
    String nginxConfDirPath = NginxConstant.CONF_DIRECTORY_PATH;
    OrganizationDAO organizationDAO = new OrganizationDAOImpl(this.dbHandle);

    Organization organization = organizationDAO.getOrganizationById(organizationId);
    String currentAtsDomain = organization.getHris_url();
    String currentCareerDomain = organization.getCareer_url();

    if (!(null != careerDomain && currentAtsDomain.equals(careerDomain) &&
        currentCareerDomain.equals(atsDomain))) {
      if (!isDomainAvailable(atsDomain)) {
        return OrganizationBOEnum.ATS_DOMAIN_NOT_AVAILABLE;
      }
      if (null != careerDomain && !isDomainAvailable(careerDomain)) {
        return OrganizationBOEnum.CAREER_DOMAIN_NOT_AVAILABLE;
      }
    }

    organizationDAO.updateDomains(atsDomain, careerDomain, organizationId);
    String sslCert = Vertx.currentContext().config().getString("ssl.certPath");
    String sslKey = Vertx.currentContext().config().getString("ssl.certKeyPath");
    Map<String, String> map = new HashMap<String, String>() {{
      put("sslCrt", sslCert);
      put("sslCrtKey", sslKey);
    }};
    if (null != careerDomain) {
      if (null != currentCareerDomain) {
        FileHelper.safeDelete(nginxConfDirPath, currentCareerDomain);
      }
      FileHelper.safeDelete(nginxConfDirPath, currentCareerDomain + FileTypeEnum.ENV_CONF.getExtension());
      map.put("domain", careerDomain);
      if (!careerKeyFile.equals("") || careerKeyFile.length() == 0) {
        map.put("sslCrtKey", careerKeyFile);
      } else if (!careerCrtFile.equals("") || careerCrtFile.length() == 0) {
        map.put("sslCrt", careerCrtFile);
      }
      writeConfFile(NginxTemplateConstant.CARRERS_SERVER_TEMPLATE, careerDomain, map);
    }
    FileHelper.safeDelete(nginxConfDirPath, currentAtsDomain + FileTypeEnum.ENV_CONF.getExtension());
    map.put("domain", atsDomain);
    if (!auzmorKeyFile.equals("") || auzmorKeyFile.length() == 0) {
      map.put("sslCrtKey", auzmorKeyFile);
    } else if (!auzmorCrtFile.equals("") || auzmorCrtFile.length() == 0) {
      map.put("sslCrt", auzmorCrtFile);
    }
    writeConfFile(NginxTemplateConstant.ATS_SERVER_TEMPLATE, atsDomain, map);
    return OrganizationBOEnum.DOMAINS_UPDATE_SUCCESS;
  }

  @Override
  public OrganizationBOEnum addOrganizationDetails(OrganizationR organization)
      throws SQLException, DbHandleException {
    OrganizationDAO organizationDAO = new OrganizationDAOImpl(this.dbHandle);
    int organizationId = organizationDAO.addOrganizationDetails(organization);
    if (organizationId < 0) {
      return OrganizationBOEnum.SUCCESSFUL;
    }
    return null;
  }

  @Override
  public Map<Long, String> getAllRolesAsMap(long organizationId) throws SQLException, DbHandleException {
    List<Role> roles = new OrganizationDAOImpl(this.dbHandle).getRoles(organizationId);
    Map<Long, String> map = new HashMap<>();

    for (Role role : roles) {
      map.put(role.getId(), role.getName());
    }

    return map;
  }
}
