package com.auzmor.impl.bo;

import com.auzmor.bo.JobDescriptionBO;
import com.auzmor.dao.JobDAO;
import com.auzmor.dao.JobDescriptionDAO;
import com.auzmor.impl.dao.JobDAOImpl;
import com.auzmor.impl.dao.JobDescriptionDAOImpl;
import com.auzmor.impl.enumarator.bo.JobDescriptionEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.JobDescriptionModel;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;
import java.util.List;

public class JobDescriptionBOImpl implements JobDescriptionBO {

  @Override
  public ReturnObject<JobDescriptionEnum, JsonObject> addJobDescription(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    JobDescriptionModel jobDescriptionModel = new JobDescriptionModel();
    jobDescriptionModel.setJobdesctype(params.get("jobdesctype"));
    jobDescriptionModel.setJobdescdetails(params.get("jobdescdetails"));
    long organizationId = StringUtil.printValidString(params.get("organization_id")).equals("") ? 0 :
        Long.valueOf(params.get("organization_id"));
    jobDescriptionModel.setOrganizationId(organizationId);

    JobDescriptionDAO jobDescriptionDAO = new JobDescriptionDAOImpl(dbHandle);
    boolean alreadyExists = jobDescriptionDAO.isJobDescriptionAlreadyExists(jobDescriptionModel);

    if(alreadyExists) {
      return new ReturnObjectImpl<JobDescriptionEnum, JsonObject>
          (JobDescriptionEnum.EXISTS_ALREADY, new JsonObject());
    }

    int insertRows = 0;
    insertRows = jobDescriptionDAO.insert(jobDescriptionModel);

    if(insertRows > 0) {
      JsonObject dataObject = new JsonObject();
      dataObject.put("message", JobDescriptionEnum.ADDITION_SUCCESS.getDescription());
      dataObject.put("jobdesctype", jobDescriptionModel.getJobdesctype());
      dataObject.put("jobdescId", jobDescriptionModel.getJobdescId());
      dataObject.put("jobdescdetails", jobDescriptionModel.getJobdescdetails());
      return new ReturnObjectImpl<JobDescriptionEnum, JsonObject>
          (JobDescriptionEnum.ADDITION_SUCCESS, dataObject);
    } else {
      return new ReturnObjectImpl<JobDescriptionEnum, JsonObject>
          (JobDescriptionEnum.ADDITION_FAILED, new JsonObject());
    }
  }

  @Override
  public ReturnObject<JobDescriptionEnum, JsonArray> getAllJobDescriptions(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    JobDescriptionModel jobDescriptionModel = new JobDescriptionModel();
    long organizationId = StringUtil.printValidString(params.get("organization_id")).equals("") ? 0 :
        Long.valueOf(params.get("organization_id"));
    jobDescriptionModel.setOrganizationId(organizationId);

    JobDescriptionDAO jobDescriptionDAO = new JobDescriptionDAOImpl(dbHandle);
    List<JobDescriptionModel> resultList = jobDescriptionDAO.getAllJobDescriptions(jobDescriptionModel);
    if (resultList.size() == 0) {
      return new ReturnObjectImpl<JobDescriptionEnum, JsonArray>
          (JobDescriptionEnum.NO_SUCH_VALUE, new JsonArray());
    } else {
      JsonArray resultArray = new JsonArray();
      for (JobDescriptionModel resultModel : resultList) {
        JsonObject resultObject = new JsonObject();
        resultObject.put("jobdescId", resultModel.getJobdescId());
        resultObject.put("jobdesctype", resultModel.getJobdesctype());
        resultObject.put("jobdescdetails", resultModel.getJobdescdetails());
        resultObject.put("organization_id", resultModel.getOrganizationId());
        resultArray.add(resultObject);
      }
      return new ReturnObjectImpl<JobDescriptionEnum, JsonArray>
          (JobDescriptionEnum.VALUE_SUCCESS, resultArray);
    }
  }

  @Override
  public ReturnObject<JobDescriptionEnum, JsonObject> getJobDescription(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    JobDescriptionModel jobDescriptionModel = new JobDescriptionModel();
    long jobDescId = StringUtil.printValidString(params.get("id")).equals("") ? 0 :
        Long.valueOf(params.get("id"));
    jobDescriptionModel.setJobdescId(jobDescId);
    JobDescriptionDAO jobDescriptionDAO = new JobDescriptionDAOImpl(dbHandle);
    JsonObject dataObject = new JsonObject();
    dataObject = jobDescriptionDAO.getJobDescription(jobDescriptionModel);

    if (dataObject.equals("")){
      return new ReturnObjectImpl<JobDescriptionEnum, JsonObject>
          (JobDescriptionEnum.NO_SUCH_VALUE, new JsonObject());
    }else {
      return new ReturnObjectImpl<JobDescriptionEnum, JsonObject>
          (JobDescriptionEnum.VALUE_SUCCESS, dataObject);
    }
  }

  @Override
  public ReturnObject<JobDescriptionEnum, JsonObject> updateJobDescription(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    JobDescriptionModel jobDescriptionModel = new JobDescriptionModel();
    long jobDescId = StringUtil.printValidString(params.get("id")).equals("") ? 0 :
        Long.valueOf(params.get("id"));
    jobDescriptionModel.setJobdescId(jobDescId);
    jobDescriptionModel.setJobdesctype(params.get("jobdesctype"));

    JobDescriptionDAO jobDescriptionDAO = new JobDescriptionDAOImpl(dbHandle);

    if (StringUtil.printValidString(params.get("jobdescdetails")).equals("")) {
      JsonObject dataObject = new JsonObject();
      dataObject = jobDescriptionDAO.getJobDescription(jobDescriptionModel);
      jobDescriptionModel.setJobdescdetails(dataObject.getString("jobdescdetails"));
    } else {
      jobDescriptionModel.setJobdescdetails(params.get("jobdescdetails"));
    }

    JobDescriptionModel dataModel = new JobDescriptionModel();
    dataModel.setJobdescId(jobDescriptionModel.getJobdescId());

    dataModel = jobDescriptionDAO.getJobDescriptionDetails(dataModel);

    if (dataModel.getJobdescdetails().equals(params.get("jobdescdetails"))
        && dataModel.getJobdesctype().equals(params.get("jobdesctype"))) {
      return new ReturnObjectImpl<JobDescriptionEnum, JsonObject>
          (JobDescriptionEnum.NO_CHANGES_MADE, new JsonObject());
    }

    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    boolean isUsedInJob = false;
    isUsedInJob = jobDAO.checkFieldOption("job_description", dataModel.getJobdescdetails(),
        (long) dataModel.getOrganizationId());

    if (isUsedInJob) {
      return new ReturnObjectImpl<JobDescriptionEnum, JsonObject>
          (JobDescriptionEnum.RESTRICTED, new JsonObject());
    }

    int updateRows = 0;
    updateRows = jobDescriptionDAO.update(jobDescriptionModel);

    if (updateRows > 0) {
      return new ReturnObjectImpl<JobDescriptionEnum, JsonObject>
          (JobDescriptionEnum.UPDATE_SUCCESS, new JsonObject());
    } else {
      return new ReturnObjectImpl<JobDescriptionEnum, JsonObject>
          (JobDescriptionEnum.NO_SUCH_VALUE, new JsonObject());
    }

  }

  @Override
  public ReturnObject<JobDescriptionEnum, JsonObject> deleteJobDescription(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    JobDescriptionModel jobDescriptionModel = new JobDescriptionModel();
    long jobDescId = StringUtil.printValidString(params.get("id")).equals("") ? 0 :
        Long.valueOf(params.get("id"));
    jobDescriptionModel.setJobdescId(jobDescId);

    JobDescriptionModel dataModel = new JobDescriptionModel();

    JobDescriptionDAO jobDescriptionDAO = new JobDescriptionDAOImpl(dbHandle);

    dataModel = jobDescriptionDAO.getJobDescriptionDetails(jobDescriptionModel);

    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    boolean isUsedInJob = false;
    isUsedInJob = jobDAO.checkFieldOption("job_description", dataModel.getJobdescdetails(),
        (long) dataModel.getOrganizationId());

    if (isUsedInJob) {
      return new ReturnObjectImpl<JobDescriptionEnum, JsonObject>
          (JobDescriptionEnum.RESTRICTED, new JsonObject());
    }

    int deleteRows =0;
    deleteRows = jobDescriptionDAO.delete(jobDescriptionModel);
    if (deleteRows > 0) {
      return new ReturnObjectImpl<JobDescriptionEnum, JsonObject>
          (JobDescriptionEnum.DELETE_SUCCESS, new JsonObject());
    } else {
      return new ReturnObjectImpl<JobDescriptionEnum, JsonObject>
          (JobDescriptionEnum.NO_SUCH_VALUE, new JsonObject());
    }
  }
}
