package com.auzmor.impl.bo;

import com.auzmor.bo.OnboardingWelcomeBO;
import com.auzmor.dao.OnboardingWelcomeDAO;
import com.auzmor.impl.dao.OnboardingWelcomeDAOImpl;
import com.auzmor.impl.enumarator.OnboardingWelcomeEnum;
import com.auzmor.impl.enumarator.file.FileTypeEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.FileHelper;
import com.auzmor.impl.helper.FileUploadHelper;
import com.auzmor.impl.model.onboarding.OnboardingPacket;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderGeometry;
import com.google.code.geocoder.model.GeocoderRequest;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.RoutingContext;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Set;

public class OnboardingWelcomeBOImpl implements OnboardingWelcomeBO{
  /**
   * Update welcome details
   * @param dbHandle
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 01/MAR/2018
   */
  @Override
  public ReturnObject<OnboardingWelcomeEnum, JsonObject> updateWelcomeScreen(DbHandle dbHandle, 
                                                                             MultiMap params) 
    throws SQLException, DbHandleException {
    JsonObject dataObject = new JsonObject();
    OnboardingWelcomeDAO onboardingWelcomeDAO = new OnboardingWelcomeDAOImpl(dbHandle);
    int updateFlag = onboardingWelcomeDAO.updateWelcomeScreen(params);
    if(updateFlag > 0) {
      dataObject.put("message", "Welcome details updated");
      return new ReturnObjectImpl<OnboardingWelcomeEnum, JsonObject>
          (OnboardingWelcomeEnum.UPDATE_SUCCESS, dataObject);
    } else {
      dataObject.put("message", "No new details to update");
      return new ReturnObjectImpl<OnboardingWelcomeEnum, JsonObject>
          (OnboardingWelcomeEnum.UPDATE_FAILURE, new JsonObject());
    }
  }

  /**
   * Upload video for welcome page
   * @param dbHandle
   * @param fileUploadSet
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 01/MAR/2018
   */
  @Override
  public ReturnObject<OnboardingWelcomeEnum, JsonObject> uploadVideo(DbHandle dbHandle, 
                                                                     Set<FileUpload> fileUploadSet, 
                                                                     MultiMap params)
    throws SQLException, DbHandleException {

    int organizationId = Integer.valueOf(params.get("organizationId"));
    String videoFilePath = "";
    JsonObject dataObject = new JsonObject();
    for (FileUpload fileUpload : fileUploadSet) {
      FileTypeEnum fileTypeEnum = FileHelper.getFileType(fileUpload);
      if (fileUpload.contentType().equalsIgnoreCase("video/mp4")) {
        String sourceFile = fileUpload.uploadedFileName();
        String destinationFile = fileUpload.uploadedFileName().split("/")[1] +
            fileTypeEnum.getExtension();
        String destinationFolderPath = "private_static/" + organizationId + "/Organization_video/";
        FileHelper.move(sourceFile, destinationFolderPath, destinationFile);
        videoFilePath = destinationFolderPath + destinationFile;
      } else {
        dataObject.put("message", "Please upload in videos in mp4 format");
        return new ReturnObjectImpl<OnboardingWelcomeEnum, JsonObject>
            (OnboardingWelcomeEnum.VIDEO_FORMAT_FAILURE, new JsonObject());
      }
    }
    
    if(StringUtil.printValidString(params.get("welcome_video")).trim().length() == 0) {
      params.add("welcome_video", videoFilePath);
    } else {
      params.remove("welcome_video");
      params.add("welcome_video", videoFilePath);
    }
    
    OnboardingWelcomeDAO onboardingWelcomeDAO = new OnboardingWelcomeDAOImpl(dbHandle);
    int updateFlag = onboardingWelcomeDAO.updateWelcomeScreen(params);
    if (updateFlag > 0) {
      dataObject.put("message", "Video upload successfully");
      return new ReturnObjectImpl<OnboardingWelcomeEnum, JsonObject>
          (OnboardingWelcomeEnum.VIDEO_UPLOAD_SUCCESS, new JsonObject());
    } else {
      dataObject.put("message", "No new video uploaded");
      return new ReturnObjectImpl<OnboardingWelcomeEnum, JsonObject>
          (OnboardingWelcomeEnum.VIDEO_UPLOAD_FAILURE, new JsonObject());
    }
  }

  /**
   * Get welcome page details
   * @param dbHandle
   * @param onboardingPacket
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 01/MAR/2018
   */
  @Override
  public ReturnObject<OnboardingWelcomeEnum, JsonObject> getWelcomeDetails(DbHandle dbHandle, 
                                                                           OnboardingPacket onboardingPacket)
    throws SQLException, DbHandleException {
    OnboardingWelcomeDAO onboardingWelcomeDAO = new OnboardingWelcomeDAOImpl(dbHandle);
    onboardingPacket = onboardingWelcomeDAO.getWelcomeDetails(onboardingPacket);
    JsonObject dataObject = new JsonObject();

    if (StringUtil.printValidString(onboardingPacket.getWelcome_header()).trim().length() == 0) {
      dataObject.put("welcome_header", "Welcome!");
      dataObject.put("welcome_description", "We are excited to see you onboarded with our organization.");
      dataObject.put("dataDummy", true);
    } else {
      dataObject.put("welcome_header", onboardingPacket.getWelcome_header());
      dataObject.put("welcome_description", onboardingPacket.getWelcome_description());
      dataObject.put("welcome_video", onboardingPacket.getWelcome_video());
      dataObject.put("latitude", onboardingPacket.getLatitude());
      dataObject.put("longitude", onboardingPacket.getLongitude());
      dataObject.put("dataDummy", false);
    }
    return new ReturnObjectImpl<OnboardingWelcomeEnum,JsonObject>
        (OnboardingWelcomeEnum.VALUE_SUCCESS, dataObject);
  }

  /**
   * Add map address for welcome page
   * @param dbHandle
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 01/MAR/2018
   */
  @Override
  public ReturnObject<OnboardingWelcomeEnum, JsonObject> appMapAddress(DbHandle dbHandle,
                                                                       MultiMap params)
    throws SQLException, DbHandleException {
    JsonObject dataObject = new JsonObject();
    String company_address_line1 = StringUtil.printValidString(params.get("company_address_line1"));
    String company_address_line2 = StringUtil.printValidString(params.get("company_address_line2"));
    String city = StringUtil.printValidString(params.get("city"));
    String state = StringUtil.printValidString(params.get("state"));
    String zip = StringUtil.printValidString(params.get("zip"));
    String country = StringUtil.printValidString(params.get("country"));

    StringBuilder address = new StringBuilder();
    address.append(company_address_line1);
    address.append(",");
    address.append(company_address_line2);
    address.append(",");
    address.append(city);
    address.append(",");
    address.append(state);
    address.append(",");
    address.append(zip);
    address.append(",");
    address.append(country);

    params.remove("company_address_line1");
    params.remove("company_address_line2");
    params.remove("city");
    params.remove("state");
    params.remove("zip");
    params.remove("country");
    params.add("organization_address", address);
    OnboardingWelcomeDAO onboardingWelcomeDAO = new OnboardingWelcomeDAOImpl(dbHandle);
    int updateFlag = onboardingWelcomeDAO.updateWelcomeScreen(params);
    if(updateFlag > 0) {
      final Geocoder coder = new Geocoder();
      GeocoderRequest geocoderRequest = new GeocoderRequestBuilder().setAddress(address.toString()).setLanguage("en").getGeocoderRequest();
      GeocodeResponse geocoderResponse = coder.geocode(geocoderRequest);

      GeocoderGeometry geocoderGeometry = geocoderResponse.getResults().get(0).getGeometry();
      BigDecimal latitude = geocoderGeometry.getLocation().getLat();
      BigDecimal longitude = geocoderGeometry.getLocation().getLng();
      params.remove("organization_address");
      params.add("latitude", String.valueOf(latitude));
      params.add("longitude", String.valueOf(longitude));
      boolean mapFlag = onboardingWelcomeDAO.updateLatLongValues(params);

      if (mapFlag) {
        dataObject.put("message", "Map details updated successfully");
        return new ReturnObjectImpl<OnboardingWelcomeEnum,JsonObject>
            (OnboardingWelcomeEnum.UPDATE_SUCCESS, new JsonObject());
      } else {
        dataObject.put("message", "No new details to update");
        return new ReturnObjectImpl<OnboardingWelcomeEnum,JsonObject>
            (OnboardingWelcomeEnum.UPDATE_FAILURE, new JsonObject());
      }
    } else {
      dataObject.put("message", "No new details to update");
      return new ReturnObjectImpl<OnboardingWelcomeEnum,JsonObject>
          (OnboardingWelcomeEnum.UPDATE_FAILURE, new JsonObject());
    }
  }
}
