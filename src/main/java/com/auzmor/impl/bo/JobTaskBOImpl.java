package com.auzmor.impl.bo;

import com.auzmor.bo.JobTaskBO;
import com.auzmor.bo.LoginBO;
import com.auzmor.bo.OrganizationBO;
import com.auzmor.dao.*;
import com.auzmor.impl.dao.*;
import com.auzmor.impl.enumarator.bo.JobTaskBOEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.model.Job.JobTaskModel;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.impl.models.CandidateR.CommentCandidate;
import com.auzmor.impl.util.DateUtil;
import com.auzmor.impl.util.Mail;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class JobTaskBOImpl implements JobTaskBO {

  private static Logger logger = LoggerFactory.getLogger(JobTaskBOImpl.class);

  /**
   * Add a new job task
   *
   * @param dbHandle
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @throws ParseException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 20/MAR/2018
   */
  @Override
  public ReturnObject<JobTaskBOEnum, JsonObject> addJobTask(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException, ParseException {
    JobTaskDAO jobTaskDAO = new JobTaskDAOImpl(dbHandle);
    java.util.Date date = null;
    java.util.Date tresholdDate = null;
    JobTaskModel jobTaskModel = new JobTaskModel();
    long creatorId = StringUtil.printValidString(params.get("creator_id")).equals("") ? 0 :
        Long.valueOf(params.get("creator_id"));
    long jobId = Long.valueOf(StringUtil.printValidString(params.get("job_id")));
    /*if (isCollaborator(creatorId,jobId,dbHandle))
    {
      return new ReturnObjectImpl<JobTaskBOEnum, JsonObject>(JobTaskBOEnum.IS_COLLABORATOR, new JsonObject());
    }*/
    //else
    jobTaskModel.setCreatorId(creatorId);
    if (StringUtil.printValidString(params.get("due_date")).trim().length() != 0) {
      date = new DateUtil().getDate().parse(params.get("due_date"));
      tresholdDate = new java.util.Date();
      if (date.before(tresholdDate)) {
        return new ReturnObjectImpl<JobTaskBOEnum, JsonObject>
            (JobTaskBOEnum.FUTURE_DATE, new JsonObject());
      }
      jobTaskModel.setDueDate(new java.sql.Date(date.getTime()));
    } else {
      jobTaskModel.setDueDate(new java.sql.Date(1));
    }

    if (StringUtil.printValidString(params.get("candidate_id")).trim().length() != 0) {
      jobTaskModel.setCandidateId(Long.valueOf(params.get("candidate_id")));
    } else {
      jobTaskModel.setCandidateId(0);
    }

    if (StringUtil.printValidString(params.get("all_flag")).trim().length() != 0) {
      jobTaskModel.setAllFlag(Boolean.valueOf(params.get("all_flag")));
    } else {
      jobTaskModel.setAllFlag(false);
    }

    jobTaskModel.setJobId(Long.valueOf(StringUtil.printValidString(params.get("job_id"))));
    jobTaskModel.setEmployeeId(Long.valueOf(StringUtil.printValidString(params.get("employee_id"))));
    jobTaskModel.setTaskName(StringUtil.printValidString(params.get("task_name")));

    int completeWithin =
        StringUtil.printValidString(params.get("complete_within")).equals("") ? 0 :
            Integer.valueOf(params.get("complete_within"));
    jobTaskModel.setCompleteWithin(completeWithin);

    if (jobTaskModel.getDueDate() == (new Date(1))
        && jobTaskModel.getCompleteWithin() == 0) {
      return new ReturnObjectImpl<JobTaskBOEnum, JsonObject>
          (JobTaskBOEnum.MISSING_DUE, new JsonObject());
    }


    boolean taskFlag = jobTaskDAO.isTaskAlreadyExists(jobTaskModel);

    if (taskFlag) {
      return new ReturnObjectImpl<JobTaskBOEnum, JsonObject>
          (JobTaskBOEnum.ALREADY_EXISTS, new JsonObject());
    }

    EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
    Employee creator = new Employee();
    Employee employee = new Employee();
    creator = employeeDAO.getEmployeeDetails(creatorId);
    employee = employeeDAO.getEmployeeDetails(jobTaskModel.getEmployeeId());

    CommentCandidate candidateCommentsModel = new CommentCandidate();
    candidateCommentsModel.setAdminName(creator.getFirst_name() + " " + creator.getLast_name());
    java.util.Date dNow = new java.util.Date();
    candidateCommentsModel.setCommentsDate(dNow);
    String commentContent = jobTaskModel.getTaskName() + " assigned to " + employee.getFirst_name() + " " +
        employee.getLast_name();

    candidateCommentsModel.setComments(commentContent);
    candidateCommentsModel.setUserId(creatorId);
    CandidateCommentDAO candidateCommentDAO = new CandidateCommentDAOImpl(dbHandle);

    int insertRows = 0;
    int insertComment = 0;
    int candidateCount = 0;
    if (jobTaskModel.isAllFlag() && jobTaskModel.getCandidateId() == 0) {             //for all candidates, individual tasks are created

      CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
      List<HashMap<String, String>> candidateList;
      candidateList = candidateDAO.getJobIdCandidateDetails(jobTaskModel.getJobId());

      for (int index = 0; index < candidateList.size(); index++) {
        HashMap<String, String> candidateDetail = candidateList.get(index);
        jobTaskModel.setCandidateId(Long.valueOf(candidateDetail.get("candidate_id")));
        java.util.Date createdDate = new java.util.Date(candidateDetail.get("applied_date"));
        if (jobTaskModel.getCompleteWithin() == 0) {         //to check if due date is over or no. of  complete within days over
          createdDate = jobTaskModel.getDueDate();
        } else {
          Calendar calendar = Calendar.getInstance();
          calendar.setTime(createdDate);
          calendar.add(Calendar.DATE, jobTaskModel.getCompleteWithin());    //to calculate due date based on complete within no. and created date
          createdDate = calendar.getTime();
        }
        java.util.Date currentDate = new java.util.Date();

        if (createdDate.before(currentDate)) {
          candidateCount++;
          continue;
        }

        candidateCommentsModel.setCandidateId(jobTaskModel.getCandidateId());
        jobTaskModel.setDueDate(new java.sql.Date(createdDate.getTime()));
        insertRows = jobTaskDAO.createTask(jobTaskModel);
        insertComment = candidateCommentDAO.create(candidateCommentsModel);
      }
      if (candidateCount > 0 && candidateCount == candidateList.size()) {    //creating task entry which is not visible until candidate is added
        jobTaskModel.setCandidateId(0);
        insertRows = jobTaskDAO.createTask(jobTaskModel);
        return new ReturnObjectImpl<JobTaskBOEnum, JsonObject>
            (JobTaskBOEnum.CURRENT_CANDIDATES, new JsonObject());
      } else if (candidateList.size() == 0) {
        insertRows = jobTaskDAO.createTask(jobTaskModel);
        insertComment = 1;
      }
    } else {
      insertRows = jobTaskDAO.createTask(jobTaskModel);
      candidateCommentsModel.setCandidateId(jobTaskModel.getCandidateId());
      insertComment = candidateCommentDAO.create(candidateCommentsModel);
    }
    CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
    CandidateR candidateR = new CandidateR();

    /*candidateR = candidateDAO.getCandidate(
        Long.valueOf(params.get("candidate_id")));*/     // commented since candidate details are not used in mail

    OrganizationBO organizationBO = new OrganizationBOImpl(dbHandle);
    Organization organization = organizationBO.getOrganizationDetails((long)employee.getOrganization_id());
    Mail mail = new Mail();
    logger.info("Inserted task and comment - task :" + insertRows + " comment :" + insertComment);
    if (insertRows > 0 && insertComment > 0) {
      JobDAO jobDAO = new JobDAOImpl(dbHandle);
      Job job = new Job();
      job = jobDAO.getJobDetails(jobId);
      long adminid = 0;
      LoginBO loginBO = new LoginBOImpl(dbHandle);
      long organization_id = (long)employee.getOrganization_id();
      List<HashMap<String, String>> adminList = loginBO.GetAdminList(organization_id);
      for (HashMap<String, String> map : adminList) {
        for (Map.Entry<String, String> mapEntry : map.entrySet()) {
          String key = mapEntry.getKey();
          String value = mapEntry.getValue();
          if (key.equals("id")) {
            adminid = Long.parseLong(value);
          }
          if (key.equals("email")) {
            mapEntry.getValue();
            String AdminEmail = value;
            Employee employee_user = new EmployeeDAOImpl(dbHandle).getEmployeeDetailsByLoginId(adminid);
            String result = mail.sendAddTaskMail(AdminEmail, employee_user, candidateR, job.getTitle()
                , Vertx.vertx(), organization.getOrganization_name());
          }
        }
      }
      String result = mail.sendAddTaskMail(creator.getWork_email(), employee, candidateR, job.getTitle()
          , Vertx.vertx(), organization.getOrganization_name());
      return new ReturnObjectImpl<JobTaskBOEnum, JsonObject>
          (JobTaskBOEnum.TASK_ADDED, new JsonObject());
    } else {
      return new ReturnObjectImpl<JobTaskBOEnum, JsonObject>
          (JobTaskBOEnum.TASK_ADD_FAILED, new JsonObject());
    }
  }

  /**
   * Get all tasks assign to collaborator
   *
   * @param dbHandle
   * @param employeeId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @throws ParseException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 20/MAR/2018
   */
  @Override
  public ReturnObject<JobTaskBOEnum, JsonArray> getTasksForEmployee(DbHandle dbHandle, long employeeId)
      throws SQLException, DbHandleException, ParseException {
    JobTaskDAO jobTaskDAO = new JobTaskDAOImpl(dbHandle);
    JsonArray resultArray = new JsonArray();
    JobTaskModel jobTaskModel = new JobTaskModel();
    jobTaskModel.setEmployeeId(employeeId);

    List<JsonObject> resultList = jobTaskDAO.getTasksForEmployee(jobTaskModel);
    if (resultList.size() == 0) {
      Employee employeeDetails = new EmployeeDAOImpl(dbHandle)
          .getEmployeeDetails(employeeId);
      int organizationId = employeeDetails.getOrganization_id();
      boolean doesEmployeeExists = (employeeDetails != null);
      if (doesEmployeeExists) {
        Organization organization = (Organization)
            new OrganizationDAOImpl(dbHandle).getOrganizationById(organizationId);
        boolean doesOrganizationExists = (organization != null);
        int jobCount = Integer.parseInt(new DatabaseUtil().
            getTablename("Job", "count(1)",
                "organization_id =" + organizationId));
        if (doesOrganizationExists && jobCount == 0) {
          JsonObject taskObject = new JsonObject();
          taskObject.put("task_id", 0);
          taskObject.put("task_name", "Conduct Interview");
          taskObject.put("job_id", 0);
          taskObject.put("complete_by", "21-May-2018");
          taskObject.put("name", "John Smith");
          taskObject.put("candidate_id", 1);
          taskObject.put("position", "Software Engineer");

          resultArray.add(taskObject);
          return new ReturnObjectImpl<JobTaskBOEnum, JsonArray>(JobTaskBOEnum.VALUE_SUCCESS, resultArray);
        }
      }


      return new ReturnObjectImpl<JobTaskBOEnum, JsonArray>(JobTaskBOEnum.NO_VALUES, resultArray);
    }

    for (JsonObject jsonObject : resultList) {
      JsonObject resultObject = new JsonObject();
      resultObject.put("task_id", jsonObject.getLong("task_id"));
      resultObject.put("task_name", jsonObject.getString("task_name"));
      resultObject.put("job_id", jsonObject.getLong("job_id"));

      SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
      SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd-MMM-yyyy");

      if (jsonObject.getBoolean("all_flag").equals(true)
          && StringUtil.printValidString(jsonObject.getString("due_date")).equals("")) {
        continue;
      }

      java.util.Date date = simpleDateFormat.parse(jsonObject.getString("due_date"));
      resultObject.put("complete_by", simpleDateFormat1.format(date));


      if (jsonObject.getLong("candidate_id") != 0) {
        CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
        CandidateR candidateR = candidateDAO.getUserName(jsonObject.getLong("candidate_id"));
        resultObject.put("name", candidateR.getFirstName() + " " + candidateR.getLastName());
      } else if (jsonObject.getLong("candidate_id") == 0
          && jsonObject.getBoolean("all_flag").equals(false)) {
        resultObject.put("name", "NA");
      } else {
        continue;
      }

      resultObject.put("candidate_id", jsonObject.getLong("candidate_id"));

      resultObject.put("position", jsonObject.getString("position"));

      logger.info("Result Array size : " + resultArray.size());
      resultArray.add(resultObject);
    }
    return new ReturnObjectImpl<JobTaskBOEnum, JsonArray>(JobTaskBOEnum.VALUE_SUCCESS, resultArray);
  }

  /**
   * Mark complete flag for tasks
   *
   * @param dbHandle
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 20/MAR/2018
   */
  @Override
  public ReturnObject<JobTaskBOEnum, JsonObject> completeTask(DbHandle dbHandle, MultiMap
      params)
      throws SQLException, DbHandleException {
    String taskArray = params.get("task_array");
    JobTaskDAO jobTaskDAO = new JobTaskDAOImpl(dbHandle);
    int updateRows = jobTaskDAO.updateTask(taskArray);
    if (updateRows > 0) {
      JsonObject dataObject = new JsonObject();
      dataObject.put("message", "Task(s) completed successfully");
      List<Integer> integers = new ArrayList<Integer>();
      Mail mail = new Mail();
      String[] splitter = taskArray.toString().split(",");
      for (String value : splitter) {
        integers.add(Integer.valueOf(value));
      }
      for (Integer integer : integers) {
        JsonObject taskObject = new JsonObject();
        JobTaskModel jobTaskModel = new JobTaskModel();
        jobTaskModel.setId(Long.valueOf(integer));
        taskObject = jobTaskDAO.getJobTaskDetails(jobTaskModel);

        EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
        Employee creatorEmployee = new Employee();
        Employee employee = new Employee();

        creatorEmployee = employeeDAO.getEmployeeDetails(
            Long.valueOf(taskObject.getLong("creator_id")));
        employee = employeeDAO.getEmployeeDetails(
            Long.valueOf(taskObject.getLong("employee_id")));

        CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
        CandidateR candidateR = new CandidateR();
        OrganizationBO organizationBO = new OrganizationBOImpl(dbHandle);
        Organization organization = organizationBO.getOrganizationDetails((long) employee.getOrganization_id());
        if (taskObject.getLong("candidate_id") > 0) {
          candidateR = candidateDAO.getCandidate(
              Long.valueOf(taskObject.getLong("candidate_id")));

          // task complete for an particular job candidate also get notified that interview is completed
          if (taskObject.getString("task_name").equalsIgnoreCase("interview scheduled") &&
              candidateR.getStatus().equalsIgnoreCase("Interview")) {
            CandidateInterviewDAO candidateInterviewDAO = new CandidateInterviewDAOImpl(dbHandle);
            int statusCount = candidateInterviewDAO.updateCompletedStatus(Long.valueOf(taskObject.getLong("candidate_id")));
            if (statusCount == 0)
              return new ReturnObjectImpl<JobTaskBOEnum, JsonObject>(JobTaskBOEnum.FAILED, null);
          }
        }
        long adminid = 0;
        LoginBO loginBO = new LoginBOImpl(dbHandle);
        long organization_id = employee.getOrganization_id();
        List<HashMap<String, String>> adminList = loginBO.GetAdminList(organization_id);
        for (HashMap<String, String> map : adminList) {
          for (Map.Entry<String, String> mapEntry : map.entrySet()) {
            String key = mapEntry.getKey();
            String value = mapEntry.getValue();
            if (key.equals("id")) {
              adminid = Long.parseLong(value);
            }
            if (key.equals("email")) {
              mapEntry.getValue();
              String AdminEmail = value;
              Employee employee_user = new EmployeeDAOImpl(dbHandle).getEmployeeDetailsByLoginId(adminid);
              String result = mail.sendCompleteTaskMail(AdminEmail, employee_user, candidateR,
                  taskObject.getString("task_name"), Vertx.vertx(), organization.getOrganization_name());
            }
          }
        }
        String result = mail.sendCompleteTaskMail(creatorEmployee.getWork_email(), employee, candidateR,
            taskObject.getString("task_name"), Vertx.vertx(), organization.getOrganization_name());
      }
      logger.info("Tasks completed :" + updateRows);
      return new ReturnObjectImpl<JobTaskBOEnum, JsonObject>(JobTaskBOEnum.UPDATE_TASK_SUCCESS, dataObject);
    } else {
      JsonObject dataObject = new JsonObject();
      dataObject.put("message", "No tasks to complete");
      return new ReturnObjectImpl<JobTaskBOEnum, JsonObject>(JobTaskBOEnum.NO_VALUES, dataObject);
    }
  }

  /*
      Functionality is used for checking whether the task creator is collaborator
      @params creatorId, jobId
      @Exceptions DbHandleException, SQLException
   */
/*  public static boolean isCollaborator(long creatorId,long jobId, DbHandle dbHandle) throws DbHandleException,SQLException
  {
      JobDAO jobDAO = new JobDAOImpl(dbHandle);
      boolean result = jobDAO.isCollaborator(creatorId,jobId);
      return result;
  }
  */

  @Override
  public List<Long> getTaskIdByEmployeeId(DbHandle dbHandle, long employeeId) throws SQLException,
      DbHandleException {
    JobTaskDAO jobDAO = new JobTaskDAOImpl(dbHandle);
    return jobDAO.getTaskIdsForEmployee(employeeId);
  }
}
