package com.auzmor.impl.bo;

import com.auzmor.bo.DepartmentBO;
import com.auzmor.dao.DepartmentDAO;
import com.auzmor.dao.EmployeeDAO;
import com.auzmor.dao.JobDAO;
import com.auzmor.impl.dao.DepartmentDAOImpl;
import com.auzmor.impl.dao.EmployeeDAOImpl;
import com.auzmor.impl.dao.JobDAOImpl;
import com.auzmor.impl.enumarator.DepartmentEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.DepartmentModel;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;

public class DepartmentBOImpl implements DepartmentBO {
  private static Logger logger = LoggerFactory.getLogger(DepartmentBOImpl.class);

  private DbHandle dbHandle;
  private  DepartmentDAO departmentDAO;
  public DepartmentBOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }
  /**
   * Add a new department for an organization
   * @param dbHandle
   * @param departmentModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */

  @Override
  public ReturnObject<DepartmentEnum, JsonObject> addDepartment(DbHandle dbHandle, 
                                                                DepartmentModel departmentModel) 
      throws SQLException,DbHandleException {
    DepartmentDAO departmentDAO = new DepartmentDAOImpl(dbHandle);
    if (departmentDAO.isDepartmentExistsInOrganization(departmentModel)) {
      return new ReturnObjectImpl<>(DepartmentEnum.EXISTS_ALREADY, new JsonObject());
    }
    int result = departmentDAO.insert(departmentModel);
    if(result > 0) {
      JsonObject resultObject =  new JsonObject();
      resultObject.put("id", result);
      resultObject.put("department", departmentModel.getDepartment());
      resultObject.put("organization_id", departmentModel.getOrganizationId());
      logger.info("department insert : "+result);
      return new ReturnObjectImpl<DepartmentEnum, JsonObject>
          (DepartmentEnum.ADDITION_SUCCESS,resultObject);
    } else {
      return new ReturnObjectImpl<DepartmentEnum, JsonObject>
          (DepartmentEnum.ADDITION_FAILED,new JsonObject());
    }
  }

  /**
   * Get details of department
   * @param dbHandle
   * @param departmentModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  @Override
  public ReturnObject<DepartmentEnum,JsonObject> getDepartment(DbHandle dbHandle, 
                                                               DepartmentModel departmentModel) 
      throws SQLException,DbHandleException {
    DepartmentDAO departmentDAO = new DepartmentDAOImpl(dbHandle);
    DepartmentModel resultModel = departmentDAO.getDepartmentDetails(departmentModel);
    JsonObject resultObject =  new JsonObject();
    resultObject.put("id", resultModel.getId());
    resultObject.put("department", resultModel.getDepartment());
    resultObject.put("organization_id", resultModel.getOrganizationId());
    logger.info("department  : "+resultObject.getLong("id"));
    return new ReturnObjectImpl<DepartmentEnum, JsonObject>
          (DepartmentEnum.VALUE_SUCCESS, resultObject);

  }

  /**
   * Update details of department
   * @param dbHandle
   * @param requestParams  parameters for required for update
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  @Override
  public ReturnObject<DepartmentEnum,JsonObject> updateDepartment(DbHandle dbHandle, 
                                                                  MultiMap requestParams) 
      throws SQLException,DbHandleException {
    DepartmentDAO departmentDAO = new DepartmentDAOImpl(dbHandle);

    DepartmentModel departmentModel = new DepartmentModel();
    departmentModel.setId(Integer.valueOf(requestParams.get("id")));
    departmentModel = departmentDAO.getDepartmentDetails(departmentModel);

    if (departmentModel.getDepartment().equals(requestParams.get("department"))) {
      return new ReturnObjectImpl<DepartmentEnum, JsonObject>
          (DepartmentEnum.NO_CHANGES_MADE, new JsonObject());
    }

    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    boolean isUsedInJob = false;
    isUsedInJob = jobDAO.checkFieldOption("department", departmentModel.getDepartment(),
        (long) departmentModel.getOrganizationId());

    if (isUsedInJob) {
      logger.info("Update restricted"+isUsedInJob);
      return new ReturnObjectImpl<DepartmentEnum, JsonObject>(DepartmentEnum.RESTRICTED, new JsonObject());
    }

    EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
    boolean deleteFlag = employeeDAO.isFieldOptionPresent("department",
        departmentModel.getDepartment(),departmentModel.getOrganizationId());
    if(deleteFlag) {
      return new ReturnObjectImpl<DepartmentEnum, JsonObject>(DepartmentEnum.CANNOT_DELETE, new JsonObject());
    }

    int updateFlag = departmentDAO.updateDepartmentDetails(requestParams);

    if(updateFlag > 0) {
      JsonObject resultObject = new JsonObject();
      resultObject.put("message", "Department updated Successfully");
      logger.info("Department update :"+updateFlag);
      return new ReturnObjectImpl<DepartmentEnum, JsonObject>
          (DepartmentEnum.UPDATE_SUCCESS, resultObject);
    } else {
      return new ReturnObjectImpl<DepartmentEnum, JsonObject>
          (DepartmentEnum.NO_SUCH_VALUE, new JsonObject());
    }


  }

  /**
   * Delete a department
   * @param dbHandle
   * @param departmentModel  contains the id for deleting the particular department
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  @Override
  public ReturnObject<DepartmentEnum,JsonObject> deleteDepartment(DbHandle dbHandle, 
                                                                  DepartmentModel departmentModel) 
      throws SQLException,DbHandleException {
    DepartmentDAO departmentDAO = new DepartmentDAOImpl(dbHandle);
    DepartmentModel dataModel = new DepartmentModel();
    dataModel = departmentDAO.getDepartmentDetails(departmentModel);

    if (StringUtil.printValidString(dataModel.getDepartment()).equals("")) {
      return new ReturnObjectImpl<DepartmentEnum, JsonObject>
          (DepartmentEnum.NO_SUCH_VALUE, new JsonObject());
    }

    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    boolean isUsedInJob = false;
    isUsedInJob = jobDAO.checkFieldOption("department", dataModel.getDepartment(),
        (long) dataModel.getOrganizationId());

    if (isUsedInJob) {
      logger.info("Delete restricted"+isUsedInJob);
      return new ReturnObjectImpl<DepartmentEnum, JsonObject>(DepartmentEnum.RESTRICTED, new JsonObject());
    }

    EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
    boolean deleteFlag = employeeDAO.isFieldOptionPresent("department",
        dataModel.getDepartment(),dataModel.getOrganizationId());
    if(deleteFlag) {
      return new ReturnObjectImpl<DepartmentEnum, JsonObject>(DepartmentEnum.CANNOT_DELETE, new JsonObject());
    }

    int updateFlag = departmentDAO.deleteDepartment(departmentModel);
    if(updateFlag > 0) {
      JsonObject resultObject = new JsonObject();
      resultObject.put("message", "Department deleted Successfully");
      logger.info("Department delete :"+updateFlag);
      return new ReturnObjectImpl<DepartmentEnum, JsonObject>
          (DepartmentEnum.DELETE_SUCCESS, resultObject);
    } else {
      return new ReturnObjectImpl<DepartmentEnum, JsonObject>
          (DepartmentEnum.NO_SUCH_VALUE, new JsonObject());
    }
  }

  /**
   * Get all department details in an organization
   * @param dbHandle
   * @param departmentModel  contains the organization_id
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 13/FEB/2018
   */
  @Override
  public ReturnObject<DepartmentEnum,JsonArray> getAllDepartments(DbHandle dbHandle, 
                                                                  DepartmentModel departmentModel) 
      throws SQLException,DbHandleException {
    DepartmentDAO departmentDAO = new DepartmentDAOImpl(dbHandle);
    List<DepartmentModel> resultList = departmentDAO.getAllDepartments(departmentModel);
    if(resultList.size() == 0) {
      return new ReturnObjectImpl<DepartmentEnum, JsonArray>
          (DepartmentEnum.NO_SUCH_VALUE, new JsonArray());
    } else {
      JsonArray resultArray = new JsonArray();
      for(DepartmentModel resultModel : resultList) {
        JsonObject resultObject = new JsonObject();
        resultObject.put("id", resultModel.getId());
        resultObject.put("department", resultModel.getDepartment());
        resultObject.put("organization_id", resultModel.getOrganizationId());
        resultArray.add(resultObject);
      }
      logger.info("Department details : "+ resultArray.size());
      return new ReturnObjectImpl<DepartmentEnum, JsonArray>
          (DepartmentEnum.VALUE_SUCCESS, resultArray);
    }
  }
  public DepartmentDAO getDepartmentDAO() {
    return departmentDAO;
  }
  public void setDepartmentDAO(DepartmentDAO departmentDAO) {
    this.departmentDAO= departmentDAO;
  }
}
