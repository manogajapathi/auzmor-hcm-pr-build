package com.auzmor.impl.bo;

import com.auzmor.bo.*;
import com.auzmor.dao.*;
import com.auzmor.impl.dao.*;
import com.auzmor.impl.enumarator.bo.CandidateHiringProcessEnum;
import com.auzmor.impl.enumarator.bo.CandidateStatusEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.handler.CountryStateHandler;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.model.Job.JobTaskModel;
import com.auzmor.impl.model.Login;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.models.CandidateR.*;
import com.auzmor.impl.util.DateUtil;
import com.auzmor.impl.util.Mail;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import com.mysql.cj.core.util.StringUtils;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class CandidateBOImpl implements CandidateBO {

  private static final Logger logger = LoggerFactory.getLogger(CandidateBOImpl.class);

  private DbHandle dbHandle;
  private CandidateDAO candidateDAO;

  public CandidateBOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  public CandidateDAO getCandidateDAO() {
    return candidateDAO;
  }

  public void setCandidateDAO(CandidateDAO candidateDAO) {
    this.candidateDAO = candidateDAO;
  }

  @Override
  public CandidateStatusEnum createCandidate(RoutingContext routingContext, CandidateR candidateModel)
      throws SQLException, DbHandleException {
    CandidateDAOImpl candidateDao = new CandidateDAOImpl(this.dbHandle);
    String mailMessage = "";
    long id = 0;

    /*String chkUserId = StringUtil.printValidString(new DatabaseUtil().
        getTablename("Candidate", "user_id",
            "email = '" + candidateModel.getEmail() + "' " +
                "and organization_id = " + candidateModel.getOrganizationId()) + "");
*/
    int chkUserId = candidateDao.candidateLoginId(candidateModel.getEmail(), candidateModel.getOrganizationId());

    OrganizationDAOImpl organizationDAO = new OrganizationDAOImpl(this.dbHandle);
    Organization organization = organizationDAO.getOrganizationById(
        candidateModel.getOrganizationId());
    String organizationName = organization.getOrganization_name();
    organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);

    logger.info("getting job id in candidate model " + candidateModel.getJobId());
    // need to check whether email id is same to hiringlead/collaborator and admin
    if (candidateModel.getJobId() > 0) {
      JobDAO jobDAO = new JobDAOImpl(this.dbHandle);
      Job job = jobDAO.getJobDetails(candidateModel.getJobId());
      int hiringLead = 0;
      int jobCreatorId = 0;

      if (!StringUtils.isNullOrEmpty(job.getHiring_lead())) {
        hiringLead = Integer.parseInt(job.getHiring_lead());
      }
      if (!StringUtils.isNullOrEmpty(String.valueOf(job.getJob_creator_id()))) {
        jobCreatorId = job.getJob_creator_id();
      }
      List<HashMap<String, String>> getCollaboratorList = jobDAO.
          getCollaboratorList(candidateModel.getJobId());
      EmployeeDAO employeeDAO = new EmployeeDAOImpl(this.dbHandle);

      if (jobCreatorId > 0) {
        Employee employee = employeeDAO.getEmployeeDetails(jobCreatorId);
        if (employee.getWork_email().equals(candidateModel.getEmail()) ||
            employee.getPersonal_email().equals(candidateModel.getEmail())) {
          logger.error("hiringLead for this job cannot applied as candidate");
          return CandidateStatusEnum.NOT_ALLOWED_EMAIL;
        }
      }

      if (hiringLead > 0) {
        Employee employee = employeeDAO.getEmployeeDetails(hiringLead);
        if (employee.getWork_email().equals(candidateModel.getEmail()) ||
            employee.getPersonal_email().equals(candidateModel.getEmail())) {
          logger.error("hiringLead for this job cannot applied as candidate");
          return CandidateStatusEnum.NOT_ALLOWED_EMAIL;
        }
      }
      if (getCollaboratorList.size() > 0) {

        for (HashMap<String, String> HashMap : getCollaboratorList) {
          for (Map.Entry<String, String> HashMapEntry : HashMap.entrySet()) {
            String keyCollaborator = HashMapEntry.getKey();
            String valueCollaborator = HashMapEntry.getValue();

            if (keyCollaborator.equalsIgnoreCase("collaborator_id")) {
              if (!valueCollaborator.equals("")) {
                long collaboratorId = Long.parseLong(valueCollaborator);
                Employee employee = employeeDAO.getEmployeeDetails(collaboratorId);
                if (employee != null) {
                  if (employee.getWork_email().equals(candidateModel.getEmail()) ||
                      employee.getPersonal_email().equals(candidateModel.getEmail())) {

                    logger.error("hiringLead for this job cannot applied as candidate");
                    return CandidateStatusEnum.NOT_ALLOWED_EMAIL;
                  }
                }
              }
            }
          }
        }
      }
    }


    if (candidateModel.getUserId() == 0) {
      if (chkUserId > 0) {
        candidateModel.setUserId((long) chkUserId);
        // checking already applied to this job
       /* String chkJobId = StringUtil.printValidString(new DatabaseUtil().
            getTablename("Candidate", "job_id", "job_id=" + candidateModel.getJobId() +
                " and active_status =1 and organization_id = " + candidateModel.getOrganizationId() +
                " and user_id = " + candidateModel.getUserId() + " "));*/
        boolean checkJobId = candidateDao.checkJobId(candidateModel.getUserId(),
            candidateModel.getJobId(), candidateModel.getOrganizationId());
        if (checkJobId == true && candidateModel.getJobId() == 0) {
          logger.info("candidate with id" + candidateModel.getJobId() + "has been moved to talent pool");
          return CandidateStatusEnum.SUCCESS_MESSAGE_STATUS_EXISTS;
        } else {
          if (checkJobId == true) {
            logger.error("Job with job id " + candidateModel.getJobId() + " has been already applied.");
            return CandidateStatusEnum.EXISTS_ALREADY;
          }
        }
      } else {
        String userName = "";
        int loginId = candidateDao.getUserName(candidateModel.getFirstName(),
            candidateModel.getEmail(), candidateModel.getOrganizationId());
        if (loginId > 0) {
          loginId = (int) ((Login) routingContext.data().get("login")).getId();
          candidateModel.setUserId((long) loginId);
          logger.error("This email id already exists" + loginId);
          userName = Integer.toString(loginId);   // email address is already registered with us
        } else {
          userName = candidateModel.getEmail();
          userName = userName.trim();
        }
        if (!userName.contains("@")) {
          candidateModel.setUserId(Long.parseLong(userName));
        } else {
          StringUtil util = new StringUtil();
          String pwd = util.randomString(8);
          Login login = new Login();
          login.setFirstName(candidateModel.getFirstName());
          login.setLastName(candidateModel.getLastName());
          login.setEmail(userName.toLowerCase().trim());     // process need to check properly
          // whether the username checking in DB or not
          login.setUserType("candidate");
          login.setPassword(pwd);
          login.setKeyValue(UUID.randomUUID().toString());
          login.setHashValue(UUID.randomUUID().toString());
          login.setOrganizationId(candidateModel.getOrganizationId());
          LoginDAO loginDAO = new LoginDAOImpl(this.dbHandle);
          //id = loginDAO.create(login);
          id = loginDAO.createWithToken(login);
          candidateModel.setUserId(id);
          Mail mailObject = new Mail();
          String url = organization.getHris_url();
          String encryptedString = "";
          LoginBOImpl loginBO = new LoginBOImpl(dbHandle);
          try {
            login.setId(id);
            candidateModel.setUserId(id);
            encryptedString = loginBO.generateToken(login);
          } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            logger.error("Token Generator Failed");
          }

          mailMessage = mailObject.LoginDetailsWithToken(userName.toLowerCase().trim(), encryptedString,
              candidateModel.getFirstName(), candidateModel.getEmail(), url, organizationName, Vertx.vertx());

         /* mailMessage = mailObject.LoginDetails(userName.toLowerCase().trim(), pwd,
              candidateModel.getFirstName(), candidateModel.getEmail(), url, organizationName, Vertx.vertx());*/
          if (!"success".equals(mailMessage)) {
            logger.error("mail sending failed.");
            return CandidateStatusEnum.SEND_FAILED;
          }
        }
      }
    } else {
      long loginId = ((Login) routingContext.data().get("login")).getId();
      candidateModel.setUserId(loginId);
    }

    /*String chkJobId = StringUtil.printValidString(new DatabaseUtil().
        getTablename("Candidate", "job_id", "job_id=" + candidateModel.getJobId() +
            " and active_status =1 and organization_id = " + candidateModel.getOrganizationId() +
            " and user_id = " + candidateModel.getUserId() + " "));*/
    boolean checkJobId = candidateDao.checkJobId(candidateModel.getUserId(),
        candidateModel.getJobId(), candidateModel.getOrganizationId());

    if (checkJobId == false) {

      // getting employee login id from routing contect only they usertype not equals to candidate
      int applicantId = candidateDao.create(candidateModel);
      // CandidateR candidate = candidateDao.getCandidate(applicantId);
      if (applicantId > 0) {
        String adminName = "";

        List<CandidateCustomQuestionR> customQuestionList = candidateModel.getCustomQuestionModels();
        if (customQuestionList != null && customQuestionList.size() > 0) {
          for (int j = 0; j < customQuestionList.size(); j++) {
            CandidateCustomQuestionR customQuestion = customQuestionList.get(j);
            int customCount = candidateDao.create(customQuestion, applicantId);
            if (customCount == 0) {
              logger.error("There are no custom questions");
              return CandidateStatusEnum.FAILED;
            }
          }
        }

       /* JobDAOImpl jobDao = new JobDAOImpl(this.dbHandle);

        List<CustomQuestion> customQuestion = jobDao.
            getJobId(candidateModel.getJobId());
*/
        //if (customQuestion.size() > 0) {

        //  for (int i = 0; i < customQuestion.size(); i++) {

         /* if (count == 0) {
            if (customQuestion.get(i).isCustomKnockout()) {

              // checking cuatomAnswer
              String candidateCustomAnswer = StringUtil.printValidString(new DatabaseUtil().
                  getTablename("customQuestionCandidate", "custom_answer",
                      "candidate_id = " + applicantId + " and active_status  = '0'" +
                          " and custom_question =  '" + customQuestion + "' "));
              //status should be updated as "knockout" for particular candidate.
              if (candidateCustomAnswer.equalsIgnoreCase(customQuestion.get(i).getCustomAnswer())) {
                String status = "update Candidate set status = 'Not Qualified / Knockout' " +
                    "where candidate_id = '" + applicantId + "' and active_status ='0' ";
                try {
                  count = new DatabaseUtil().updateRecord(status);
                } catch (ClassNotFoundException e) {
                  e.printStackTrace();
                }
              }
            }
          }*/

        //  }
        // } else {

        String candidateStatusChk = StringUtil.printValidString(new DatabaseUtil().
            getTablename("Candidate", "status",
                "candidate_id = " + applicantId + " " +
                    "and status != '' "));

        HiringProcessCandidate candidateHiringProcessModel = new HiringProcessCandidate();
        candidateModel.setCandidateId((long) applicantId);
        candidateHiringProcessModel.setCandidateId((long) applicantId);

        // only candidate name should come on first time
        //adminName = StringUtil.printValidString(new DatabaseUtil().getTablename("Job",
        // "hiring_lead", "id = " + candidateModel.getJobId()));

        candidateHiringProcessModel.setCandidateId((long) applicantId);
        if (candidateModel.getJobId() != 0) {
          logger.info("Job exists with Job id :" + candidateModel.getJobId());
          if (adminName.equals("") || adminName.length() == 0) {
            adminName = candidateModel.getFirstName() + " " + candidateModel.getLastName();
          }
          candidateHiringProcessModel.setAdminName(adminName);
          candidateHiringProcessModel.setUserId((long) 0);
          if (candidateStatusChk.equals("") || candidateStatusChk.length() == 0) {
            candidateHiringProcessModel.setStatus("applicant");
            candidateHiringProcessModel.setCandidateProcess("New");
          } else {
            candidateHiringProcessModel.setStatus("Not Hired");
            candidateHiringProcessModel.setCandidateProcess("Not Qualified / Knockout");
          }
          CandidateHiringProcessBO candidateHiringProcessBO = new CandidateHiringProcessBOImpl(dbHandle);
          CandidateHiringProcessEnum candidateHiringProcessEnum = candidateHiringProcessBO.
              insertCandidateStatus(dbHandle, candidateHiringProcessModel);

          if (candidateHiringProcessEnum == CandidateHiringProcessEnum.FAILED) {
            logger.error("candidate hiring failed");
            return CandidateStatusEnum.FAILED;
          } else {
            /* adding comments*/
            CommentCandidate commentsModel = new CommentCandidate();
            java.util.Date dNow = new java.util.Date();
            String currentDate = new DateUtil().getDate().format(dNow);
            try {
              commentsModel.setCommentsDate(new DateUtil().getDate().parse(currentDate));
            } catch (ParseException e) {
              e.printStackTrace();
            }
            String comments = "moved from No Status to New Applicant";
            commentsModel.setAdminName(adminName);
            commentsModel.setComments(comments);
            commentsModel.setCandidateId((long) applicantId);
            commentsModel.setSystemGenerated(true);

            CandidateCommentDAO candidateCommentDAO = new CandidateCommentDAOImpl(dbHandle);
            int commentCount = candidateCommentDAO.create(commentsModel);

            if (commentCount > 0) {
              JobTaskDAO jobTaskDAO = new JobTaskDAOImpl(dbHandle);
              JsonObject dataObject;
              dataObject = jobTaskDAO.getJobTaskForJob(candidateModel.getJobId());

              if (!StringUtil.printValidString(dataObject.getString("task_name")).equals("")) {
                JobTaskModel jobTaskModel = new JobTaskModel();
                jobTaskModel.setEmployeeId(dataObject.getLong("employee_id"));
                jobTaskModel.setTaskName(dataObject.getString("task_name"));
                jobTaskModel.setCandidateId((long) applicantId);
                jobTaskModel.setJobId(candidateModel.getJobId());
                jobTaskModel.setCreatorId(dataObject.getLong("creator_id"));
                int completeWithin = dataObject.getInteger("complete_within");
                if (completeWithin == 0) {
                  DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
                  Date createdDate = null;
                  try {
                    createdDate = dateFormat.parse(dataObject.getString("due_date"));
                    java.util.Date currentTaskDate = new java.util.Date();
                    if (createdDate.after(currentTaskDate)) {
                      jobTaskModel.setDueDate(new java.sql.Date(createdDate.getTime()));
                    } else {
                      jobTaskModel.setDueDate(new java.sql.Date(currentTaskDate.getTime()));
                    }
                  } catch (ParseException e) {
                    logger.error("DbHandle Exception while creating candidate");
                    ResponseHelper.writeInternalServerError(routingContext.response());
                    return CandidateStatusEnum.FAILED;
                  }
                } else {
                  java.util.Date utilDate = new java.util.Date();
                  Calendar calendar = Calendar.getInstance();
                  calendar.setTime(utilDate);
                  calendar.add(Calendar.DATE, completeWithin);
                  utilDate = calendar.getTime();
                  jobTaskModel.setDueDate(new java.sql.Date(utilDate.getTime()));
                }
                EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
                Employee creator = new Employee();
                Employee employee = new Employee();
                creator = employeeDAO.getEmployeeDetails(jobTaskModel.getCreatorId());
                employee = employeeDAO.getEmployeeDetails(jobTaskModel.getEmployeeId());

                CommentCandidate candidateCommentsModel = new CommentCandidate();
                candidateCommentsModel.setAdminName(creator.getFirst_name() + " " + creator.getLast_name());
                candidateCommentsModel.setCommentsDate(dNow);
                String commentContent = "Task ('" + jobTaskModel.getTaskName() + "') assigned to "
                    + employee.getFirst_name() + " " + employee.getLast_name();

                candidateCommentsModel.setComments(commentContent);
                candidateCommentsModel.setUserId(jobTaskModel.getCreatorId());
                candidateCommentsModel.setCandidateId((long) applicantId);
                commentsModel.setSystemGenerated(true);

                int insertRows = jobTaskDAO.createTask(jobTaskModel);
                int taskCommentCount = candidateCommentDAO.create(candidateCommentsModel);

                if (insertRows <= 0 && taskCommentCount <= 0) {
                  logger.error("Inserting the comment failed");
                  return CandidateStatusEnum.FAILED;
                }
              }
            }


            if (candidateModel.getAdminId() > 0 && commentCount > 0 && id > 0) {
              // mail sending done by Abbareddi Mounika
              JobBO jobBO = new JobBOImpl(dbHandle);
              Job job = jobBO.getJobDetails(candidateModel.getJobId());
              String mailDescription = "<html><body>Dear <b>" + candidateModel.getFirstName() + " " + candidateModel.getLastName() + "</b>, <br> <br>" +
                  "<p>Thank you for applying for the position of " + job.getTitle() + " at ! " +
                  " We will get back to you with the details of the interview schedule once the screening process is over. \n" +
                  " You will receive the mail in your registered email id and mobile number." +
                  " We wish you luck for the next phase!</p> <br><br> <br>" +
                  " Thank you, <br>" + organizationName + " recruitment Team <br>" +
                  " </body></html>";

              String subjectName = "Confirmation of job application";
              MailDetails mailDetails = new MailDetails();
              mailDetails.setCandidateId(applicantId);
              mailDetails.setAdminName(StringUtil.printValidString(adminName));
              mailDetails.setSubjectName(StringUtil.printValidString(subjectName));
              mailDetails.setAttachmentName(StringUtil.printValidString(""));
              mailDetails.setMailDescription(mailDescription);
              mailDetails.setUserId(candidateModel.getAdminId());
              MailDetailsBO mailDetailsBO = new MailDetailsBOImpl(dbHandle);
              int mailId = mailDetailsBO.create(mailDetails);
              if (mailId > 0) {
                Mail mail = new Mail();
                try {
                  mailMessage = mail.mailDetails(Vertx.vertx(), mailDescription,
                      candidateModel.getEmail(), subjectName, "");
                } catch (FileNotFoundException e) {
                  e.printStackTrace();
                }
              }
              logger.info("Login check is successfully");
              return CandidateStatusEnum.LOGIN_CHECK;
            } else if (candidateModel.getAdminId() > 0 && commentCount > 0) {
              // mail sending done by Abbareddi Mounika
              JobBO jobBO = new JobBOImpl(dbHandle);
              Job job = jobBO.getJobDetails(candidateModel.getJobId());
              String mailDescription = "<html><body>Dear <b>" + candidateModel.getFirstName() + " " +
                  " " + candidateModel.getLastName() + "</b>, <br> <br>" +
                  "<p>Thank you for applying for the position of " + job.getTitle() + " at " + organizationName + "! " +
                  " We will get back to you with the details of the interview schedule once the screening process is over. \n" +
                  " You will receive the mail in your registered email id and mobile number." +
                  " We wish you luck for the next phase!</p> <br><br> <br>" +
                  " Thank you, <br> " + organizationName + " recruitment Team <br>" +
                  " </body></html>";

              String subjectName = "Confirmation of job application";
              MailDetails mailDetails = new MailDetails();
              mailDetails.setCandidateId(applicantId);
              mailDetails.setAdminName(StringUtil.printValidString(adminName));
              mailDetails.setSubjectName(StringUtil.printValidString(subjectName));
              mailDetails.setAttachmentName(StringUtil.printValidString(""));
              mailDetails.setMailDescription(mailDescription);
              mailDetails.setUserId(candidateModel.getAdminId());
              MailDetailsBO mailDetailsBO = new MailDetailsBOImpl(dbHandle);
              int mailId = mailDetailsBO.create(mailDetails);
              if (mailId > 0) {
                Mail mail = new Mail();
                try {
                  mailMessage = mail.mailDetails(Vertx.vertx(), mailDescription,
                      candidateModel.getEmail(), subjectName, "");
                } catch (FileNotFoundException e) {
                  e.printStackTrace();
                }
              }
              logger.info("candidate applied successfully");
              return CandidateStatusEnum.SUCCESS_MESSAGE;
            } else if (id > 0) {
              // mail sending done by Abbareddi Mounika
              JobBO jobBO = new JobBOImpl(dbHandle);
              Job job = jobBO.getJobDetails(candidateModel.getJobId());
              String mailDescription = "<html><body>Dear <b>" + candidateModel.getFirstName() + "" +
                  " " + candidateModel.getLastName() + "</b>, <br> <br>" +
                  "<p>Thank you for applying for the position of " + job.getTitle() + " at " + organizationName + "! " +
                  " We will get back to you with the details of the interview schedule once the screening process is over. \n" +
                  " You will receive the mail in your registered email id and mobile number." +
                  " We wish you luck for the next phase!</p> <br><br> <br>" +
                  " Thank you, <br> " + organizationName + " recruitment Team <br>" +
                  " </body></html>";

              String subjectName = "Confirmation of job application";
              MailDetails mailDetails = new MailDetails();
              mailDetails.setCandidateId(applicantId);
              mailDetails.setAdminName(StringUtil.printValidString(adminName));
              mailDetails.setSubjectName(StringUtil.printValidString(subjectName));
              mailDetails.setAttachmentName(StringUtil.printValidString(""));
              mailDetails.setMailDescription(mailDescription);
              mailDetails.setUserId(candidateModel.getAdminId());
              MailDetailsBO mailDetailsBO = new MailDetailsBOImpl(dbHandle);
              int mailId = mailDetailsBO.create(mailDetails);
              if (mailId > 0) {
                Mail mail = new Mail();
                try {
                  mailMessage = mail.mailDetails(Vertx.vertx(), mailDescription,
                      candidateModel.getEmail(), subjectName, "");
                } catch (FileNotFoundException e) {
                  e.printStackTrace();
                }
              }
              logger.info("mail with login deatils has been sent to email id successfully");
              return CandidateStatusEnum.SUCCESS;
            } else if (commentCount > 0) {
              // mail sending done by Abbareddi Mounika
              JobBO jobBO = new JobBOImpl(dbHandle);
              Job job = jobBO.getJobDetails(candidateModel.getJobId());
              String mailDescription = "<html><body>Dear <b>" + candidateModel.getFirstName() + " " + candidateModel.getLastName() + "</b>, <br> <br>" +
                  "<p>Thank you for applying for the position of " + job.getTitle() + " at " + organizationName + "! " +
                  " We will get back to you with the details of the interview schedule once the screening process is over. \n" +
                  " You will receive the mail in your registered email id and mobile number." +
                  " We wish you luck for the next phase!</p> <br><br> <br>" +
                  " Thank you, <br> " + organizationName + " recruitment Team <br>" +
                  " </body></html>";

              String subjectName = "Confirmation of job application";
              MailDetails mailDetails = new MailDetails();
              mailDetails.setCandidateId(applicantId);
              mailDetails.setAdminName(StringUtil.printValidString(adminName));
              mailDetails.setSubjectName(StringUtil.printValidString(subjectName));
              mailDetails.setAttachmentName(StringUtil.printValidString(""));
              mailDetails.setMailDescription(mailDescription);
              mailDetails.setUserId(candidateModel.getAdminId());
              MailDetailsBO mailDetailsBO = new MailDetailsBOImpl(dbHandle);
              int mailId = mailDetailsBO.create(mailDetails);
              if (mailId > 0) {
                Mail mail = new Mail();
                try {
                  mailMessage = mail.mailDetails(Vertx.vertx(), mailDescription,
                      candidateModel.getEmail(), subjectName, "");
                } catch (FileNotFoundException e) {
                  e.printStackTrace();
                }
              }
              logger.info("Job is applied successfully");
              return CandidateStatusEnum.APPLIED_JOB;
            }
          }
        } else {
          adminName = StringUtil.printValidString(new DatabaseUtil().getTablename("Employee",
              "first_name", "employee_id = " + candidateModel.getAdminId()));
          candidateHiringProcessModel.setAdminName(adminName);
          candidateHiringProcessModel.setUserId(candidateModel.getAdminId());
          candidateHiringProcessModel.setStatus("Not Hired");
          candidateHiringProcessModel.setCandidateProcess("Archive to Talent Pool");
          CandidateHiringProcessBO candidateHiringProcessBO = new CandidateHiringProcessBOImpl(dbHandle);
          CandidateHiringProcessEnum candidateHiringProcessEnum = candidateHiringProcessBO.
              insertCandidateStatus(dbHandle, candidateHiringProcessModel);
          if (candidateHiringProcessEnum == CandidateHiringProcessEnum.FAILED) {
            logger.error("candidate was not hired");
            return CandidateStatusEnum.FAILED;
          } else {
            /* adding comments*/
            CommentCandidate commentsModel = new CommentCandidate();
            java.util.Date dNow = new java.util.Date();
            String currentDate = new DateUtil().getDate().format(dNow);
            try {
              commentsModel.setCommentsDate(new DateUtil().getDate().parse(currentDate));
            } catch (ParseException e) {
              e.printStackTrace();
            }
            String comments = "moved from No Status to Archive to Talent Pool";
            commentsModel.setAdminName(adminName);
            commentsModel.setComments(comments);
            commentsModel.setCandidateId((long) applicantId);
            commentsModel.setUserId(candidateModel.getAdminId());
            commentsModel.setSystemGenerated(true);

            CandidateCommentDAO candidateCommentDAO = new CandidateCommentDAOImpl(this.dbHandle);
            int commentCount = candidateCommentDAO.create(commentsModel);
            if (candidateModel.getAdminId() > 0 && id > 0) {
              // mail sending done by Abbareddi Mounika
              String mailDescription = "<html><body>Hello <b>" + candidateModel.getFirstName() + " " + candidateModel.getLastName() + "</b>, <br> <br>" +
                  "<p>Your application is on Hold for the organization " + organization.getOrganization_name() + ". </p> <br><br> <br>" +
                  " Thank you,<br> " + organizationName + " recruitment Team <br>" +
                  " </body></html>";
              String subjectName = "Re:Job Status";
              MailDetails mailDetails = new MailDetails();
              mailDetails.setCandidateId(applicantId);
              mailDetails.setAdminName(StringUtil.printValidString(adminName));
              mailDetails.setSubjectName(StringUtil.printValidString(subjectName));
              mailDetails.setAttachmentName(StringUtil.printValidString(""));
              mailDetails.setMailDescription(mailDescription);
              mailDetails.setUserId(candidateModel.getAdminId());
              MailDetailsBO mailDetailsBO = new MailDetailsBOImpl(dbHandle);
              int mailId = mailDetailsBO.create(mailDetails);
              if (mailId > 0) {
                Mail mail = new Mail();
                try {
                  mailMessage = mail.mailDetails(Vertx.vertx(), mailDescription,
                      candidateModel.getEmail(), subjectName, "");
                } catch (FileNotFoundException e) {
                  e.printStackTrace();
                }
              }
              logger.info("candidate has been moved to talent pool successfully");
              return CandidateStatusEnum.LOGIN_STATUS;
            } else if (candidateModel.getAdminId() > 0) {
              // mail sending done by Abbareddi Mounika
              String mailDescription = "<html><body>Hello <b>" + candidateModel.getFirstName() + " " + candidateModel.getLastName() + "</b>, <br> <br>" +
                  "<p>Your application is on Hold for the organization " + organizationName + ". </p> <br><br> <br>" +
                  " Thank you,<br>" + organizationName + " recruitment Team <br>" +
                  " </body></html>";
              String subjectName = "Re:Job Status";
              MailDetails mailDetails = new MailDetails();
              mailDetails.setCandidateId(applicantId);
              mailDetails.setAdminName(StringUtil.printValidString(adminName));
              mailDetails.setSubjectName(StringUtil.printValidString(subjectName));
              mailDetails.setAttachmentName(StringUtil.printValidString(""));
              mailDetails.setMailDescription(mailDescription);
              mailDetails.setUserId(candidateModel.getAdminId());
              MailDetailsBO mailDetailsBO = new MailDetailsBOImpl(dbHandle);
              int mailId = mailDetailsBO.create(mailDetails);
              if (mailId > 0) {
                Mail mail = new Mail();
                try {
                  mailMessage = mail.mailDetails(Vertx.vertx(), mailDescription,
                      candidateModel.getEmail(), subjectName, "");
                } catch (FileNotFoundException e) {
                  e.printStackTrace();
                }
              }
              logger.info("candidate moved to talent pool successfully");
              return CandidateStatusEnum.SUCCESS_MESSAGE_STATUS;
            } else if (commentCount > 0) {
              // mail sending done by Abbareddi Mounika
              JobBO jobBO = new JobBOImpl(dbHandle);
              Job job = jobBO.getJobDetails(candidateModel.getJobId());
              String mailDescription = "<html><body>Hello <b>" + candidateModel.getFirstName() + " " + candidateModel.getLastName() + "</b>, <br> <br>" +
                  "<p>Thank you for Applying for the position of  " + job.getTitle() + ".</p> <br><br> <br>" +
                  " Thank you, <br>" + organizationName + " recruitment Team <br>" +
                  " </body></html>";
              String subjectName = "Re:Applied Job";
              MailDetails mailDetails = new MailDetails();
              mailDetails.setCandidateId(applicantId);
              mailDetails.setAdminName(StringUtil.printValidString(adminName));
              mailDetails.setSubjectName(StringUtil.printValidString(subjectName));
              mailDetails.setAttachmentName(StringUtil.printValidString(""));
              mailDetails.setMailDescription(mailDescription);
              mailDetails.setUserId(candidateModel.getAdminId());
              MailDetailsBO mailDetailsBO = new MailDetailsBOImpl(dbHandle);
              int mailId = mailDetailsBO.create(mailDetails);
              if (mailId > 0) {
                Mail mail = new Mail();
                try {
                  mailMessage = mail.mailDetails(Vertx.vertx(), mailDescription,
                      candidateModel.getEmail(), subjectName, "");
                } catch (FileNotFoundException e) {
                  e.printStackTrace();
                }
              }
              logger.info("mail has been sent for successful job application");
              return CandidateStatusEnum.APPLIED_JOB;
            }
          }
        }
      } else {
        logger.error("Bad request");
        return CandidateStatusEnum.FAILED;
      }
    } else {
      logger.error("this job has been already applied");
      return CandidateStatusEnum.EXISTS_JOB_ALREADY;
    }
    logger.error("bad request");
    return CandidateStatusEnum.FAILED;
  }

  @Override
  public CandidateR getCandidateId(DbHandle dbHandle, long candidateId, long organizationId)
      throws SQLException, DbHandleException {
    CandidateDAO candidateDAO = new CandidateDAOImpl(this.dbHandle);
    CandidateR candidateR = candidateDAO.getCandidateDetails(candidateId, organizationId);
    logger.info("Candidate exists with id :" + candidateR.getCandidateId());
    return candidateR;
  }

  @Override
  public CandidateR getCandidate(DbHandle dbHandle, long candidateId)
      throws SQLException, DbHandleException {
    CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
    CandidateR candidateR = candidateDAO.getCandidate(candidateId);
    logger.info("Candidate exists with id :" + candidateR.getCandidateId());
    return candidateR;
  }

  @Override
  public List<HashMap<String, String>> getUserId(DbHandle dbHandle, long userId, long organizationId)
      throws SQLException, DbHandleException {

    List<HashMap<String, String>> candidateList;
    CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
    candidateList = candidateDAO.getUserIdCandidateDetails(userId, organizationId);
    logger.info("Available users :" + candidateList.size());
    return candidateList;
  }

  @Override
  public List<HashMap<String, String>> getJobId(DbHandle dbHandle, long jobId, long organizationId)
      throws SQLException, DbHandleException {

    List<HashMap<String, String>> candidateList;
    CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
    candidateList = candidateDAO.getJobIdCandidateDetails(jobId, organizationId);
    logger.info("available jobs : " + candidateList.size());
    return candidateList;
  }


  @Override
  public CandidateStatusEnum deleteCandidate(long candidate)
      throws SQLException, DbHandleException {
    CandidateDAO candidateDAO = new CandidateDAOImpl(this.dbHandle);
    int deleteCount = candidateDAO.deleteCandidate(candidate);
    CandidateStatusEnum candidateStatusEnum = null;
    if (deleteCount > 0) {
      // customCandidate need to be deleted.
      logger.info("candidate deleted successfully");
      return candidateStatusEnum.CANDIDATE_DELETED_SUCESSFULLY;
    }
    logger.error("could not able to delete candidate");
    return candidateStatusEnum.CANDIDATE_DELETED_FAILED;
  }

  @Override
  public List<HashMap<String, String>> getPostJobFilterDetails(DbHandle dbHandle, long organizationId)
      throws SQLException, DbHandleException {
    CandidateDAO candidateDAO = new CandidateDAOImpl(this.dbHandle);
    List<HashMap<String, String>> getPostJob = candidateDAO.getPostJobFilterDetails(organizationId);
    return getPostJob;
  }


  @Override
  public CandidateStatusEnum updateRating(int rating, long candidateId, long userId)
      throws SQLException, DbHandleException {

    CandidateStatusEnum candidateStatusEnum = null;
    CandidateDAO candidateDAO = new CandidateDAOImpl(this.dbHandle);

    int updateCount = candidateDAO.updateRating(rating, candidateId);

    if (updateCount > 0) {

      CandidateCommentDAO candidateCommentDAO = new CandidateCommentDAOImpl(this.dbHandle);
      CommentCandidate commentCandidate = new CommentCandidate();
      String comments = "rated as " + rating;

      EmployeeBO employeeBO = new EmployeeBOImpl(this.dbHandle);
      Employee employee = employeeBO.getEmployeeDetails(userId);

      java.util.Date dNow = new java.util.Date();
      String currentDate = new DateUtil().getDate().format(dNow);
      try {
        commentCandidate.setCommentsDate(new DateUtil().getDate().parse(currentDate));
      } catch (ParseException e) {
        e.printStackTrace();
      }

      commentCandidate.setAdminName(employee.getFirst_name());
      commentCandidate.setComments(comments);
      commentCandidate.setCandidateId(candidateId);
      commentCandidate.setUserId(userId);

      updateCount = candidateCommentDAO.create(commentCandidate);

      if (updateCount == 0) {
        logger.error("candidate update failed");
        return candidateStatusEnum.STATUS_FAILED;
      }
    }
    logger.info("candidate updated successfully");
    return candidateStatusEnum.STATUS_SUCCESS;
  }

  @Override
  public CandidateStatusEnum updateForwardEmployee(long employeeId, String notes,
                                                   String id, long userId)
      throws SQLException, DbHandleException {

    CandidateStatusEnum candidateStatusEnum = null;
    CandidateDAO candidateDAO = new CandidateDAOImpl(this.dbHandle);

    List<String> multipleCandidate = new ArrayList<>();
    multipleCandidate.add(id);

    for (int i = 0; i < multipleCandidate.size(); i++) {

      java.util.Date dNow = new java.util.Date();
      String currentDate = new DateUtil().getDate().format(dNow);

      int updateCount = candidateDAO.
          updateForwardEmployee(employeeId, notes, multipleCandidate.get(i).toString());

      if (updateCount > 0) {

        CandidateCommentDAO candidateCommentDAO = new CandidateCommentDAOImpl(this.dbHandle);
        CommentCandidate commentCandidate = new CommentCandidate();
        String comments = "forward candidate to employee ";

        EmployeeBO employeeBO = new EmployeeBOImpl(this.dbHandle);
        Employee employee = employeeBO.getEmployeeDetails(userId);

        try {
          commentCandidate.setCommentsDate(new DateUtil().getDate().parse(currentDate));
        } catch (ParseException e) {
          e.printStackTrace();
        }

        commentCandidate.setUserId(userId);
        commentCandidate.setAdminName(employee.getFirst_name());
        commentCandidate.setComments(comments);
        commentCandidate.setCandidateId(Long.parseLong(multipleCandidate.get(i)));

        updateCount = candidateCommentDAO.create(commentCandidate);
        if (updateCount == 0) {
          logger.error("candidate forward failed to the employee");
          return candidateStatusEnum.FORWARD_FAILED;
        }
      }
    }
    logger.info("candidate forwarded to the employee is successfull");
    return candidateStatusEnum.FORWARD_SUCCESS;
  }

  @Override
  public CandidateStatusEnum updateMoveCandidate(long jobId, long candidateId,
                                                 long userId)
      throws SQLException, DbHandleException {

    CandidateStatusEnum candidateStatusEnum = null;
    CandidateDAO candidateDAO = new CandidateDAOImpl(this.dbHandle);
    EmployeeBO employeeBO = new EmployeeBOImpl(this.dbHandle);
    Employee employee = employeeBO.getEmployeeDetails(userId);
    CandidateR candidate = candidateDAO.getCandidate(candidateId);
    String status = "applicant";

    // inserting one new record with moved jobID
    CandidateR candidateMovedRecord = new CandidateR();
    candidateMovedRecord.setFirstName(candidate.getFirstName());
    candidateMovedRecord.setLastName(candidate.getLastName());
    candidateMovedRecord.setUserId(candidate.getUserId());
    candidateMovedRecord.setResume(candidate.getResume());
    candidateMovedRecord.setRating(candidate.getRating());
    candidateMovedRecord.setResumeName(candidate.getResumeName());
    candidateMovedRecord.setUnit(candidate.getUnit());
    candidateMovedRecord.setAddressLine(candidate.getAddressLine());
    candidateMovedRecord.setCity(candidate.getCity());
    candidateMovedRecord.setZip(candidate.getZip());
    candidateMovedRecord.setOrganizationId(candidate.getOrganizationId());
    candidateMovedRecord.setPhoneNumber(candidate.getPhoneNumber());
    candidateMovedRecord.setCountry(candidate.getCountry());
    candidateMovedRecord.setState(candidate.getState());
    candidateMovedRecord.setEmail(candidate.getEmail());
    candidateMovedRecord.setAdminId(userId);
    candidateMovedRecord.setJobId(jobId);
    candidateMovedRecord.setHiringLeadId((long) 0);

    int movedCandidateId = candidateDAO.create(candidateMovedRecord);

    if (movedCandidateId > 0) {

      CandidateHiringDAO candidateHiringDAO = new CandidateHiringProcessDAOImpl(this.dbHandle);

      java.util.Date dNow = new java.util.Date();
      String currentDate = new DateUtil().getDate().format(dNow);

      HiringProcessCandidate hiringProcessCandidate = new HiringProcessCandidate();
      hiringProcessCandidate.setCandidateId((long) movedCandidateId);
      hiringProcessCandidate.setCandidateProcess("New");
      hiringProcessCandidate.setStatus(status);
      hiringProcessCandidate.setAdminName(employee.getFirst_name());
      hiringProcessCandidate.setUserId(userId);

      int updateCount = candidateHiringDAO.create(hiringProcessCandidate);
      if (updateCount > 0) {
        CandidateCommentDAO candidateCommentDAO = new CandidateCommentDAOImpl(this.dbHandle);
        CommentCandidate commentCandidate = new CommentCandidate();
        String comments = "moved candidate to Job";
        try {
          commentCandidate.setCommentsDate(new DateUtil().getDate().parse(currentDate));
        } catch (ParseException e) {
          e.printStackTrace();
        }

        commentCandidate.setUserId(userId);
        commentCandidate.setAdminName(employee.getFirst_name() + " " + employee.getLast_name());
        commentCandidate.setComments(comments);
        commentCandidate.setCandidateId(movedCandidateId);
        commentCandidate.setSystemGenerated(true);

        updateCount = candidateCommentDAO.create(commentCandidate);

        if (updateCount > 0) {
          logger.info("candidate moved successfully'");
          return candidateStatusEnum.MOVE_SUCCESS;
        }
      }
    }

/*
    int updateCount = candidateDAO.updateMoveCandidate(jobId, candidateId, status);

    if (updateCount > 0) {
      updateCount = candidateHiringDAO.updateStatus(candidateId);
      if (updateCount > 0) {
        CandidateInterviewDAO interviewDAO = new CandidateInterviewDAOImpl(this.dbHandle);
        updateCount = interviewDAO.updateActiveStatus(candidateId);

        CandidateOfferDAO offerDAO = new CandidateOfferDAOImpl(this.dbHandle);
        updateCount = offerDAO.updateActiveStatus(candidateId);

        MailDetailsDAO mailDetailsDAO = new MailDetailsDAOImpl(this.dbHandle);
        updateCount = mailDetailsDAO.updateActiveStatus(candidateId);

        HiringProcessCandidate hiringProcessCandidate = new HiringProcessCandidate();
        hiringProcessCandidate.setCandidateId(candidateId);
        hiringProcessCandidate.setCandidateProcess("New");
        hiringProcessCandidate.setStatus(status);
        hiringProcessCandidate.setAdminName(employee.getFirst_name());
        hiringProcessCandidate.setUserId(userId);

        updateCount = candidateHiringDAO.create(hiringProcessCandidate);
        if (updateCount > 0) {
          CandidateCommentDAO candidateCommentDAO = new CandidateCommentDAOImpl(this.dbHandle);
          CommentCandidate commentCandidate = new CommentCandidate();
          String comments = "moved candidate to Job";
          try {
            commentCandidate.setCommentsDate(new DateUtil().getDate().parse(currentDate));
          } catch (ParseException e) {
            e.printStackTrace();
          }

          commentCandidate.setUserId(userId);
          commentCandidate.setAdminName(employee.getFirst_name());
          commentCandidate.setComments(comments);
          commentCandidate.setCandidateId(candidateId);
          commentCandidate.setSystemGenerated(true);

          updateCount = candidateCommentDAO.create(commentCandidate);

          if (updateCount > 0) {
            logger.info("candidate moved successfully'");
            return candidateStatusEnum.MOVE_SUCCESS;
          }
        }
      }
    }*/

    logger.error("could not able to move the candidate");
    return candidateStatusEnum.MOVE_FAILED;
  }

  @Override
  public CandidateStatusEnum updateCandidate(String dateFormat, MultiMap multiMap)
      throws SQLException, DbHandleException {
    CandidateStatusEnum candidateStatusEnum = null;
    CandidateR candidateRecord;

    long candidate = Long.parseLong(multiMap.get("candidate_id"));

    CandidateDAO candidateDAO = new CandidateDAOImpl(this.dbHandle);
    candidateRecord = candidateDAO.getCandidate(candidate);

    if (candidateRecord != null) {

      if (!StringUtil.printValidString(multiMap.get("first_name")).equals("")) {
        candidateRecord.setFirstName(multiMap.get("first_name"));
      }
      if (!StringUtil.printValidString(multiMap.get("last_name")).equals("")) {
        candidateRecord.setLastName(multiMap.get("last_name"));
      }
      if (!StringUtil.printValidString(multiMap.get("email")).equals("")) {
        candidateRecord.setEmail(multiMap.get("email"));
      }
      if (!StringUtil.printValidString(multiMap.get("phone_number")).equals("")) {
        candidateRecord.setPhoneNumber(multiMap.get("phone_number"));
      }
      if (!StringUtil.printValidString(multiMap.get("unit")).equals("")) {
        candidateRecord.setUnit(multiMap.get("unit"));
      }
      if (!StringUtil.printValidString(multiMap.get("source")).equals("")) {
        candidateRecord.setSource(multiMap.get("source"));
      }
      if (!StringUtil.printValidString(multiMap.get("address_line")).equals("")) {
        candidateRecord.setAddressLine(multiMap.get("address_line"));
      }
      if (!StringUtil.printValidString(multiMap.get("city")).equals("")) {
        candidateRecord.setCity(multiMap.get("city"));
      }
      if (!StringUtil.printValidString(multiMap.get("state")).equals("")) {
        candidateRecord.setState(multiMap.get("state"));
      }
      if (!StringUtil.printValidString(multiMap.get("zip")).equals("")) {
        candidateRecord.setZip(multiMap.get("zip"));
      }
      if (!StringUtil.printValidString(multiMap.get("country")).equals("")) {
        candidateRecord.setCountry(multiMap.get("country"));
      }
      if (!StringUtil.printValidString(multiMap.get("gender")).equals("")) {
        candidateRecord.setGender(multiMap.get("gender"));
      }

      if (!StringUtil.printValidString(multiMap.get("internal_jobcode")).equals("")) {
        candidateRecord.setInternalJobCode(multiMap.get("internal_jobcode"));
      }

      if (!StringUtil.printValidString(multiMap.get("resume")).equals("")) {
        candidateRecord.setResume(multiMap.get("resume"));
      }

      if (!StringUtil.printValidString(multiMap.get("cover_letter")).equals("")) {
        candidateRecord.setCoverLetter(multiMap.get("cover_letter"));
      }

      if (!StringUtil.printValidString(multiMap.get("highest_education")).equals("")) {
        candidateRecord.setHighestEducation(multiMap.get("highest_education"));
      }

      if (!StringUtil.printValidString(multiMap.get("desired_salary")).equals("")) {
        candidateRecord.setDesiredSalary(multiMap.get("desired_salary"));
      }

      if (!StringUtil.printValidString(multiMap.get("linkedin_url")).equals("")) {
        candidateRecord.setLinkedinUrl(multiMap.get("linkedin_url"));
      }

      if (!StringUtil.printValidString(multiMap.get("date_available")).equals("")) {
        candidateRecord.setDateAvailable(multiMap.get("date_available"));
      }

      if (!StringUtil.printValidString(multiMap.get("blog")).equals("")) {
        candidateRecord.setBlog(multiMap.get("blog"));
      }

      if (!StringUtil.printValidString(multiMap.get("reference")).equals("")) {
        candidateRecord.setReference(multiMap.get("reference"));
      }

      if (!StringUtil.printValidString(multiMap.get("referred_by")).equals("")) {
        candidateRecord.setReferredBy(multiMap.get("referred_by"));
      }

      if (!StringUtil.printValidString(multiMap.get("college")).equals("")) {
        candidateRecord.setCollege(multiMap.get("college"));
      }

      if (!StringUtil.printValidString(multiMap.get("company_type_check")).equals("")) {
        candidateRecord.setCompanyTypeCheck(multiMap.get("company_type_check"));
      }

      if (!StringUtil.printValidString(multiMap.get("verteran_status")).equals("")) {
        candidateRecord.setVeteranStatus(multiMap.get("verteran_status"));
      }

      if (!StringUtil.printValidString(multiMap.get("ethnicity")).equals("")) {
        candidateRecord.setEthnicity(multiMap.get("ethnicity"));
      }

      if (!StringUtil.printValidString(multiMap.get("disability")).equals("")) {
        candidateRecord.setDisability(multiMap.get("disability"));
      }

      if (!StringUtil.printValidString(multiMap.get("status")).equals("")) {
        candidateRecord.setStatus(multiMap.get("status"));
      }

      if (!StringUtil.printValidString(multiMap.get("job_id")).equals("")) {
        candidateRecord.setJobId(Long.parseLong(multiMap.get("job_id")));
      }

      if (!StringUtil.printValidString(multiMap.get("rating")).equals("")) {
        candidateRecord.setRating(Long.parseLong(multiMap.get("rating")));
      }

      candidateRecord.setCandidateId(candidate);

      int updateCandidate = candidateDAO.updateCandidate(candidateRecord);

      if (updateCandidate == 0) {
        logger.error("could not able to update candidate");
        candidateStatusEnum = candidateStatusEnum.UPDATE_FAILED;
      }
    } else {
      logger.error("could not able to update candidate");
      return candidateStatusEnum.UPDATE_FAILED;
    }
    logger.info("candidate updated successfully");
    return candidateStatusEnum.UPDATE_SUCCESS;
  }


  @Override
  public JsonObject getCandidateList(String dateFormat, MultiMap params)
      throws SQLException, DbHandleException {

    int organizationId = 0;    //--- for organic spec
    int collaboratorId = 0;
    int id = 0;
    int employeeId = 0;
    String limitValues = StringUtil.printValidString(params.get("limit"));
    String offsetValues = StringUtil.printValidString(params.get("offset"));
    String candidateValues = StringUtil.printValidString(params.get("candidatestatus"));
    String searchFilter = StringUtil.printValidString(params.get("searchFilter"));
    String hiringLeadValues = params.get("hiring_lead");
    int limit = 0;
    int offset = 0;

    if (!limitValues.equals("") && limitValues.length() > 0) {
      limit = Integer.parseInt(limitValues);
    }
    if (!offsetValues.equals("") && offsetValues.length() > 0) {
      offset = Integer.parseInt(offsetValues);
    }
    if (params.get("job_id") != null) {
      id = Integer.parseInt(params.get("job_id"));
    }
    if (params.get("employee_id") != null) {
      employeeId = Integer.parseInt(params.get("employee_id"));
    }
    if (params.get("collaborator_id") != null) {
      collaboratorId = Integer.parseInt(params.get("collaborator_id"));
    }
    if (!StringUtils.isNullOrEmpty(params.get("organization_id"))) {
      organizationId = Integer.parseInt(params.get("organization_id"));
    }

   /* if (candidateValues != null && candidateValues.length() > 0) {
      if (candidateValues.contains(",")) {
        candidateValues = candidateValues.replace(",", "','");
      }
      selectFilter.append(" and Candidate.status in ('" + candidateValues + "')");
    }
    if (hiringLeadValues != null && hiringLeadValues.length() > 0 && collaboratorId == 0) {
      if (hiringLeadValues.contains(",")) {
        hiringLeadValues = hiringLeadValues.replace(",", "','");
      }
      selectFilter.append(" and Job.hiring_lead in ('" + hiringLeadValues + "')");
    }

    if (collaboratorId > 0) {
      if (StringUtil.printValidString(hiringLeadValues).length() > 0) {
        selectFilter.append(" and Candidate.job_id in " +
            "(select job_id from Collaborators where collaborator= " + collaboratorId + "" +
            " and active_status = 1 " +
            " union select id from Job where hiring_lead = " + hiringLeadValues +
            " and job_status!= 'Closed')");
      } else {
        selectFilter.append(" and Candidate.job_id in " +
            "(select job_id from Collaborators " +
            "where collaborator= " + collaboratorId + " and active_status = 1)");
      }
    }
    if (id > 0) {
      selectFilter.append(" and Candidate.job_id = " + id + " ");
    }
    if (employeeId > 0) {
      selectFilter.append(" and Candidate.employee_id = " + employeeId + " ");
    }*/

    String stateId = "";
    if (!StringUtils.isNullOrEmpty(searchFilter)) {
      searchFilter = searchFilter.toUpperCase();
      // searching state id from state name
      stateId = CountryStateHandler.getStateIdWithLikeCondition(searchFilter);

     /* selectFilter.append(" and ((UPPER(Candidate.first_name) like '%" + searchFilter + "%' or UPPER(");
      selectFilter.append("Candidate.last_name) like '%" + searchFilter);
      selectFilter.append("%' or UPPER(CONCAT(Candidate.first_name, ' ', Candidate.last_name)) like '%");
      selectFilter.append(searchFilter + "%') or (UPPER(Job.title) like '%" + searchFilter + "%')");
      if (!StringUtils.isNullOrEmpty(stateId)) {
        selectFilter.append(" or (UPPER(Candidate.state) in (" + stateId + "))");
      }
      selectFilter.append(")");*/
    }

    CandidateDAO candidateDAO = new CandidateDAOImpl(this.dbHandle);
    List<HashMap<String, String>> candidateList = candidateDAO.
        getCandidateList(searchFilter, limit, offset, organizationId, candidateValues,
            hiringLeadValues, collaboratorId, id, employeeId, stateId);

    List<HashMap<String, String>> candidateTotalCount = candidateDAO.
        getCandidateList(searchFilter, 0, 0, organizationId, candidateValues,
            hiringLeadValues, collaboratorId, id, employeeId, stateId);

    CandidateInterviewDAO candidateInterviewDAO = new CandidateInterviewDAOImpl(this.dbHandle);
    CandidateOfferDAO candidateOfferDAO = new CandidateOfferDAOImpl(this.dbHandle);

    JsonArray candidateArray = new JsonArray();
    String rejectReason = "";
    String adminCancelledReason = "";
    String offerReject = "";
    String adminReason = "";
    long candidateId = 0;
    List<String> statusList = new ArrayList<>();
    for (HashMap<String, String> map : candidateList) {
      JsonObject dataObject = new JsonObject();
      for (Map.Entry<String, String> mapEntry : map.entrySet()) {
        String key = mapEntry.getKey();
        String value = mapEntry.getValue();
        String status = "";
        adminCancelledReason = "";
        rejectReason = "";
        offerReject = "";
        adminReason = "";

        if (key.equals("candidate_id")) {
          candidateId = Long.parseLong(value);
        }
        if (key.equals("created")) {
          try {
            String date = new DateUtil().getDateFormat(dateFormat, value);
            dataObject.put("applied_date", date);
          } catch (ParseException e) {
            e.printStackTrace();
          }
        }
        if (key.equals("modified")) {
          try {
            String date = new DateUtil().getDateFormat(dateFormat, value);
            dataObject.put("modified_date", date);
          } catch (ParseException e) {
            e.printStackTrace();
          }
        }
        if (key.equals("status")) {
          status = value;
          dataObject.put("status", status);
          if (status.equalsIgnoreCase("Interview")) {
            InterviewProcessCandidate interviewProcessCandidate = candidateInterviewDAO.
                getInterviewProcessDetails(candidateId);

            if (interviewProcessCandidate != null) {
              rejectReason = StringUtil.printValidString(interviewProcessCandidate.getStatus());
              adminCancelledReason = interviewProcessCandidate.getInterviewCancelReason();
              if (interviewProcessCandidate.isCompleted() == true &&
                  StringUtils.isNullOrEmpty(adminCancelledReason)) {
                rejectReason = "interview";
              }

              logger.info("testing candidate cancel reason :" + adminCancelledReason);
            }

          } else if (status.equalsIgnoreCase("Offer")) {
            OfferProcessCandidate offerProcessCandidate = candidateOfferDAO.
                getOfferProcessDetails(candidateId);
            if (offerProcessCandidate != null) {
              rejectReason = offerProcessCandidate.getStatus();
              offerReject = offerProcessCandidate.getOfferDeclineReason();
            }
          }
          if (StringUtils.isNullOrEmpty(rejectReason)) {
            rejectReason = "-";
          } else {
            if (rejectReason.equals("accept")) {
              rejectReason = "Accepted by Candidate";
            } else if (rejectReason.equals("interview")) {
              rejectReason = "Interview Completed";
            } else {
              rejectReason = "Rejected by Candidate";
            }
          }
          // offer admin reject or accept
          if (!StringUtils.isNullOrEmpty(offerReject)) {
            adminReason = "Rejected by Admin/HiringLead";
          }
          // interview admin reject or accept
          if (!StringUtils.isNullOrEmpty(adminCancelledReason)) {
            adminReason = "Cancelled by Admin/HiringLead";
          }
          if (!StringUtils.isNullOrEmpty(adminReason)) {
            rejectReason = adminReason;
          }
        } else {
          dataObject.put(key, value);
        }
        dataObject.put("reject_reason", rejectReason);
      }
      candidateArray.add(dataObject);
    }

    // getting only status value from the loop
    for (HashMap<String, String> map : candidateTotalCount) {
      for (Map.Entry<String, String> mapEntry : map.entrySet()) {
        String key = mapEntry.getKey();
        String value = mapEntry.getValue();
        if (key.equals("status")) {
          String status = value;
          statusList.add(status);
        }
      }
    }

    int totalCount = candidateTotalCount.size();
    JsonArray candidateStatus = new JsonArray();
    JsonObject successObject = new JsonObject();
    List StatusValue = new ArrayList(new HashSet(statusList)); //no order
    Iterator<String> iterator = StatusValue.iterator();
    String statusValue = "";
    while (iterator.hasNext()) {
      JsonObject statusCheck = new JsonObject();
      statusValue = iterator.next();
      statusCheck.put("candidatestatus", statusValue);
      candidateStatus.add(statusCheck);
    }

    successObject.put("code", HttpStatus.SC_OK);
    successObject.put("success", true);
    successObject.put("totalcount", totalCount);
    successObject.put("candidateStatus", candidateStatus);
    if (totalCount == 0) {
      OrganizationDAO organizationDAO = new OrganizationDAOImpl(this.dbHandle);
      Organization organization = organizationDAO.getOrganizationById(organizationId);
      boolean doesOrganizationExists = (organization != null);
      int jobCount = Integer.parseInt(new DatabaseUtil().
          getTablename("Job", "count(1)",
              "organization_id =" + organizationId));
      if (jobCount == 0 && doesOrganizationExists) {
        JsonArray resultSetArray = new JsonArray();
        JsonObject dumpData = new JsonObject();
        dumpData.put("first_name", "John");
        dumpData.put("last_name", "Smith");
        dumpData.put("phone_number", "98954522556");
        dumpData.put("status", "New");
        dumpData.put("email", "xxx@gmail.com");
        dumpData.put("applied_date", "30-Jan-2018");
        dumpData.put("modified_date", "30-Jan-2018");
        dumpData.put("job_title", "Developer");
        dumpData.put("rating", "3875454845");
        dumpData.put("reject_reason", "-");
        dumpData.put("candidate_id", 0);
        resultSetArray.add(dumpData);

        successObject.put("no_data", true);
        successObject.put("data", resultSetArray);
      } else {
        successObject.put("no_data", true);
        successObject.put("data", candidateArray);
      }
    } else {
      successObject.put("no_data", false);
      successObject.put("data", candidateArray);
    }

    return successObject;
  }

  @Override
  public int updateStatus(String status, long candidateId)
      throws SQLException, DbHandleException {
    CandidateDAO candidateDAO = new CandidateDAOImpl(this.dbHandle);
    int updateCount = candidateDAO.updateCandidateStatus(status, candidateId);
    logger.info("total candidates updated :" + updateCount);
    return updateCount;
  }

  @Override
  public List<HashMap<String, String>> getCandidateTalentList(long organizationId, String format, String searchFilter,
                                                              int limit, int offset, String stateId)
      throws SQLException, DbHandleException {
    List<HashMap<String, String>> candidateList;
    CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
    candidateList = candidateDAO.getCandidateTalentList(organizationId, format, searchFilter, limit, offset, stateId);
    logger.info("total number of candidates available in talent pool :" + candidateList.size());
    return candidateList;
  }

  @Override
  public ReturnObject<CandidateStatusEnum, JsonObject> getCandidateListBasedOnStatus(
      DbHandle dbHandle, CandidateR candidateModel, MultiMap requestParams)
      throws SQLException, DbHandleException {

    CandidateStatusEnum candidateStatusEnum = null;
    String candidateStatus = candidateModel.getStatus();

    String format = requestParams.get("format");
    CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
    JsonArray candidateStatusArray = candidateDAO.getCandidateStatusValues(candidateModel);
    if (null == candidateStatusArray) {
      candidateStatusArray = new JsonArray();
    }
    JsonObject dataObject = new JsonObject();
    if (!candidateStatusArray.isEmpty()) {
      dataObject.put("available_status", candidateStatusArray);
    }
    if (candidateStatus != null) {
      JsonArray statusCheckerArray = new JsonArray();
      for (int i = 0; i < candidateStatusArray.size(); i++) {
        statusCheckerArray.add(candidateStatusArray.getValue(i).toString().toLowerCase());
      }
      if (!statusCheckerArray.contains(candidateStatus.toLowerCase())) {
        logger.error("invalid candidate status");
        candidateStatusEnum = CandidateStatusEnum.CANDIDATE_STATUS_INVALID;
        return new ReturnObjectImpl<>(candidateStatusEnum, new JsonObject());
      }
    }

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format != null ? format : "dd-MMM-yyyy");
    List<JsonObject> candidateList = new ArrayList<>();
    if (candidateStatusArray != null) {
      candidateDAO = new CandidateDAOImpl(dbHandle);
      candidateList = candidateDAO.getCandidateListBasedOnStatus(candidateModel, requestParams);
      for (int i = 0; i < candidateList.size(); i++) {

        String date = candidateList.get(i).getString("applied_date");
        Timestamp timestamp = Timestamp.valueOf(date);
        Date dateObject = new Date(timestamp.getTime());
        candidateList.get(i).remove("applied_date");

        candidateList.get(i).put("applied_date",
            simpleDateFormat.format(dateObject));
      }
    }

    JsonArray reportArray = new JsonArray();
    for (int i = 0; i < candidateList.size(); i++) {
      reportArray.add(candidateList.get(i));
    }

    if (!reportArray.isEmpty()) {
      dataObject.put("report_information", reportArray);
    } else {
      Organization organization = new OrganizationDAOImpl(dbHandle).
          getOrganizationById(Long.parseLong(requestParams.get("organization_id")));
      boolean doesOrganizationExists = (organization != null);
      int jobCount = Integer.parseInt(new DatabaseUtil().
          getTablename("Job", "count(1)",
              "organization_id =" + requestParams.get("organization_id")));
      if (doesOrganizationExists && jobCount == 0) {
        dataObject.put("available_status", new JsonArray().add("Applicant"));
        JsonObject sampleObject = new JsonObject();
        sampleObject.put("candidate_id", 0);
        sampleObject.put("first_name", "John");
        sampleObject.put("last_name", "Smith");
        sampleObject.put("job_title", "Software Engineer");
        sampleObject.put("status", "Applicant");
        sampleObject.put("applied_date", "23-Mar-2018");
        dataObject.put("report_information", new JsonArray().add(sampleObject));
      }
    }
    logger.info("candidate list :" + dataObject.size());
    candidateStatusEnum = CandidateStatusEnum.GET_CANDIDATE_SUCCESS;
    return new ReturnObjectImpl<>(candidateStatusEnum, dataObject);
  }

  @Override
  public int getCandidatewithLoginId(long organizationId) throws SQLException, DbHandleException {
    CandidateDAO candidateDAO = new CandidateDAOImpl(this.dbHandle);
    int candidateId = candidateDAO.getCandidatewithLoginId(organizationId);
    logger.info("candidate with login id :" + candidateId);
    return candidateId;
  }

  /**
   * sendEmailUpdatesToCollaborator - send status updates to collaborator via email
   *
   * @param existingStatus
   * @param requiredStatus
   * @param candidateId
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public void sendStatusUpdatesToCollaborator(String existingStatus,
                                              String requiredStatus,
                                              String subStatus,
                                              int candidateId)
      throws SQLException, DbHandleException {
    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    List<Employee> employeeList = jobDAO.getCollaboratorsWithEmailUpdates(candidateId);

    if (!employeeList.isEmpty()) {
      for (int i = 0; i < employeeList.size(); i++) {
        Mail mail = new Mail();
        String recipientAddress = employeeList.get(i).getWork_email();
        String employeeFirstName = employeeList.get(i).getFirst_name();
        String employeeLastName = employeeList.get(i).getLast_name();
        int organizationId = employeeList.get(i).getOrganization_id();

        Organization organization = new OrganizationDAOImpl(dbHandle)
            .getOrganizationById(organizationId);
        String organizationName = organization.getOrganization_name();
        organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);

        CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
        CandidateR candidate = candidateDAO.getCandidate(candidateId);

        Job job = jobDAO.getJobDetails(candidate.getJobId());
        String title = job.getTitle();

        if (candidate != null) {

          String content = "";

          if ("screening".equalsIgnoreCase(requiredStatus)) {

            content = "Get a glimpse of the candidates' status for the position of " + title + "! "
                + "<br/>" + candidate.getFirstName() + " " + candidate.getLastName() +
                " has been moved from " + existingStatus +
                " to the screening stage in the hiring process. " +
                "We'll get back to you when the status changes.";

          } else if ("interview".equalsIgnoreCase(requiredStatus)) {

            content = "Looks like there's been some progress! " +
                "<br/>" + candidate.getFirstName() + " " + candidate.getLastName() +
                " has cleared the " + existingStatus + " stage and is through to the interview. " +
                "We will get back to you with the interview schedule shortly.";

          } else if ("offer".equalsIgnoreCase(requiredStatus)) {

            content = "Congratulations! After a carefully curated " + existingStatus + " process," +
                " the search for a new employee is finally over!  " +
                candidate.getFirstName() + " " + candidate.getLastName() + " has bagged the" +
                " position of " + title + " and an offer has been extended. Fingers crossed, " +
                "let's hope <he/she> accepts the offer!";

          } else if ("hire".equalsIgnoreCase(requiredStatus)) {

            content = "Congratulations! " + candidate.getFirstName() + " " + candidate.getLastName()
                + " has accepted the offer for the position of " + title + " !";

          } else if ("not hired".equalsIgnoreCase(requiredStatus)) {

            content = candidate.getFirstName() + " " + candidate.getLastName() + " has been removed "
                + "from hiring process (Reason : <b>" + subStatus
                + "</b>).";

          }

          String emailBody = "<html><body>Dear " + employeeFirstName + " " + employeeLastName + ", "
              + "<br/> " + content + "<br/>Thank you, <br/> " + organizationName + " Team <br/>" +
              "--------------------------------------------------------------\n<br/>" +
              "This is system generated mail,please do not reply to this mail\n<br/>" +
              "-------------------------------------------------------------- " +
              "</body></html>";

          if (!recipientAddress.isEmpty()) {
            logger.info("mail has been sent to employee successfully");
            mail.sendMail(Vertx.vertx(), emailBody, recipientAddress,
                "Applicant Status Update");
          }
        }
      }
    }
  }

  /* This method is used to retrieve available resumes for a candidate
   *  @params dbHandle, candidateR
   *  @Exception DbHandleException, SQLException
   *  @return ReturnObjectImpl
   */
  public ReturnObject<CandidateStatusEnum, JsonArray> getAllResumes(DbHandle dbHandle, CandidateR candidateR)
      throws DbHandleException, SQLException {
    CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
    List<CandidateR> resultList = candidateDAO.getAllResumes(candidateR);
    if (resultList.size() == 0) {
      logger.error("There are no resumes available with this user :" + resultList.size());
      return new ReturnObjectImpl<CandidateStatusEnum, JsonArray>(CandidateStatusEnum.NO_RESUME, new JsonArray());
    } else {
      JsonArray resultArray = new JsonArray();

      List<String> result = new ArrayList<>();
      for (CandidateR resultModel : resultList) {
        JsonObject object = new JsonObject();
        //String file_name[] = resultModel.getResume().split("/");
        //String resume =file_name[file_name.length-1].substring(0,file_name[file_name.length-1].lastIndexOf("_")) ;
        String resumeTitle = resultModel.getResumeName();
        Date created_date = resultModel.getCreated();
        String file_path = resultModel.getResume();
        object.put("Resume_Title", resumeTitle);
        object.put("Resume_Path", file_path); // as per sadhana comments on May 30, 2018
        object.put("Last_Used_on", created_date.toString());
        resultArray.add(object);

      }
      /*JsonObject successObject = new JsonObject();
      successObject.put("resume",resultArray);*/
      logger.info("Total number of available resumes for this user :" + resultArray.size());
      return new ReturnObjectImpl<CandidateStatusEnum, JsonArray>(CandidateStatusEnum.LIST_AVAILABLE_RESUMES, resultArray);
    }
  }

  @Override
  public List<CandidateCustomQuestionR> getCustomQuestions(Map<String, String> customQuestionMap)
      throws DecodeException {
    String customQuestionObjectString = customQuestionMap.get("custom_question");
    String customAnswerObjectString = customQuestionMap.get("custom_answer");
    JsonArray customQuestionArray = new JsonArray(customQuestionObjectString);
    JsonArray customAnswerArray = new JsonArray(customAnswerObjectString);

    List<CandidateCustomQuestionR> customQuestions = new ArrayList<>();
    for (int i = 0; i < customQuestionArray.size(); i++) {
      CandidateCustomQuestionR customQuestionObject = new CandidateCustomQuestionR();
      customQuestionObject.setCustomQuestion(customQuestionArray.getString(i));
      customQuestionObject.setCustomAnswer(customAnswerArray.getString(i));
      customQuestions.add(customQuestionObject);
    }
    logger.info("custom questions are present");
    return customQuestions;
  }

  @Override
  public List<HashMap<String, String>> getCustomQuestionsList(long candidateId)
      throws SQLException, DbHandleException {

    CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
    List<HashMap<String, String>> customQuestionsList = candidateDAO.getCustomQuestionsList(candidateId);
    logger.info("List of custom questions :" + customQuestionsList.size()
    );
    return customQuestionsList;
  }

  /**
   * sendStatusUpdatesToHiringLead - send email updates to hiring lead
   *
   * @param existingStatus
   * @param requiredStatus
   * @param candidateId
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public void sendStatusUpdatesToHiringLead(String existingStatus,
                                            String requiredStatus,
                                            String subStatus,
                                            int candidateId)
      throws SQLException, DbHandleException {

    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    String hiringLeadId = jobDAO.getHiringLeadBasedOnCandidate(candidateId);

    if (StringUtil.isNumber(hiringLeadId)) {

      Employee employee = new EmployeeDAOImpl(dbHandle).getEmployeeDetails(
          Integer.parseInt(hiringLeadId));
      Mail mail = new Mail();
      String recipientAddress = employee.getWork_email();
      String employeeFirstName = employee.getFirst_name();
      String employeeLastName = employee.getLast_name();
      int organizationId = employee.getOrganization_id();

      Organization organization = new OrganizationDAOImpl(dbHandle)
          .getOrganizationById(organizationId);
      String organizationName = organization.getOrganization_name();
      organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);

      CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
      CandidateR candidate = candidateDAO.getCandidate(candidateId);

      Job job = jobDAO.getJobDetails(candidate.getJobId());
      String title = job.getTitle();

      if (candidate != null) {

        String content = "";

        if ("screening".equalsIgnoreCase(requiredStatus)) {

          content = "Get a glimpse of the candidates' status for the position of " + title + "! " +
              "<br/>" + candidate.getFirstName() + " " + candidate.getLastName() +
              " has been moved from " + existingStatus + " to the screening stage in the hiring process. " +
              "We'll get back to you when the status changes.";

        } else if ("interview".equalsIgnoreCase(requiredStatus)) {

          content = "Looks like there's been some progress! " +
              "<br/>" + candidate.getFirstName() + " " + candidate.getLastName() +
              " has cleared the " + existingStatus + " stage and is through to the interview. " +
              "We will get back to you with the interview schedule shortly.";

        } else if ("offer".equalsIgnoreCase(requiredStatus)) {

          content = "Congratulations! After a carefully curated " + existingStatus + " process," +
              " the search for a new employee is finally over!  " +
              candidate.getFirstName() + " " + candidate.getLastName() + " has bagged the" +
              " position of " + title + " and an offer has been extended. Fingers crossed, " +
              "let's hope <he/she> accepts the offer!";

        } else if ("hire".equalsIgnoreCase(requiredStatus)) {

          content = "Congratulations! " + candidate.getFirstName() + " " + candidate.getLastName()
              + " has accepted the offer for the position of " + title + " !";

        } else if ("not hired".equalsIgnoreCase(requiredStatus)) {

          content = candidate.getFirstName() + " " + candidate.getLastName() + " has been removed " +
              "from hiring process. (Reason : <b>" + subStatus + "</b>)";
        }

        String emailBody = "<html><body>Dear " + employeeFirstName + " " + employeeLastName + ", " +
            "<br/><br/> " + content + "<br/><br/>Thank you,<br/> " + organizationName + " Team <br/>" +
            "--------------------------------------------------------------\n<br/>" +
            "This is system generated mail,please do not reply to this mail\n<br/>" +
            "-------------------------------------------------------------- " +
            "</body></html>";

        if (!recipientAddress.isEmpty()) {
          logger.info("mail has been sent to employee successfully");
          mail.sendMail(Vertx.vertx(), emailBody, recipientAddress,
              "Applicant Status Update");
        }

      }
    }
  }

  @Override
  public void sendStatusUpdatesToAdmin(String existingStatus, String requiredStatus, String subStatus,
                                       long candidateId, long user_id) throws SQLException, DbHandleException {
    CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
    CandidateR candidate = candidateDAO.getCandidate(candidateId);
    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    Job job = jobDAO.getJobDetails(candidate.getJobId());
    String title = job.getTitle();
    long adminid = 0;
    Employee employee_user = new EmployeeDAOImpl(dbHandle).getEmployeeDetails(user_id);
    LoginBO loginBO = new LoginBOImpl(dbHandle);
    long organization_id = candidate.getOrganizationId();
    // adding this line to get the organization name in every mail
    OrganizationBO organizationBO = new OrganizationBOImpl(dbHandle);
    Organization organization = organizationBO.getOrganizationDetails((long) job.getOrganization_id());
    String organizationName = organization.getOrganization_name();
    organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);

    List<HashMap<String, String>> adminList = loginBO.GetAdminList(organization_id);
    for (HashMap<String, String> map : adminList) {
      for (Map.Entry<String, String> mapEntry : map.entrySet()) {
        String key = mapEntry.getKey();
        String value = mapEntry.getValue();
        if (key.equals("id")) {
          adminid = Long.parseLong(value);
        }
        if (key.equals("email")) {
          mapEntry.getValue();
          String AdminEmail = value;
          Employee employee = new EmployeeDAOImpl(dbHandle).getEmployeeDetailsByLoginId(adminid);
          if (null != employee) {
            if (adminid != employee_user.getUser_id()) {
              Mail mail = new Mail();
              String employeeFirstName = employee.getFirst_name();
              String employeeLastName = employee.getLast_name();
              if (null != candidate) {
                String content = "";

                if ("screening".equalsIgnoreCase(requiredStatus)) {

                  content = "Get a glimpse of the candidates' status for the position of " + title + "! " +
                      "<br/>" + candidate.getFirstName() + " " + candidate.getLastName() +
                      " has been moved from " + existingStatus + " to the screening stage in the hiring process. " +
                      "We'll get back to you when the status changes.";

                } else if ("interview".equalsIgnoreCase(requiredStatus)) {

                  content = "Looks lik e there's been some progress! " +
                      "<br/>" + candidate.getFirstName() + " " + candidate.getLastName() +
                      " has cleared the " + existingStatus + " stage and is through to the interview. " +
                      "We will get back to you with the interview schedule shortly.";

                } else if ("offer".equalsIgnoreCase(requiredStatus)) {

                  content = "Congratulations! After a carefully curated " + existingStatus + " process," +
                      " the search for a new employee is finally over!  " +
                      candidate.getFirstName() + " " + candidate.getLastName() + " has bagged the" +
                      " position of " + title + " and an offer has been extended. Fingers crossed, " +
                      "let's hope <he/she> accepts the offer!";

                } else if ("hire".equalsIgnoreCase(requiredStatus)) {

                  content = "Congratulations! " + candidate.getFirstName() + " " + candidate.getLastName()
                      + " has accepted the offer for the position of " + title + " !";

                } else if ("Not Hired".equalsIgnoreCase(requiredStatus)) {

                  content = candidate.getFirstName() + " " + candidate.getLastName() + " has been removed " +
                      "from hiring process. (Reason : <b>" + subStatus + "</b>)";

                } else {
                  content = candidate.getFirstName() + " " + candidate.getLastName() + " has been changed status to <b>" + subStatus + "</b>";
                }

                String emailBody = "<html><body>Dear " + employeeFirstName + " " + employeeLastName + ", " +
                    "<br/><br/> " + content + "<br/><br/>Thank you,<br/>" + organizationName + " Team <br/>" +
                    "--------------------------------------------------------------\n<br/>" +
                    "This is system generated mail,please do not reply to this mail\n<br/>" +
                    "-------------------------------------------------------------- " +
                    "</body></html>";

                if (!AdminEmail.isEmpty()) {
                  logger.info("mail has been sent to employee successfully");
                  mail.sendMail(Vertx.vertx(), emailBody, AdminEmail,
                      "Applicant Status Update");
                }
              }
            }
          }
        }
      }
    }
  }
}



