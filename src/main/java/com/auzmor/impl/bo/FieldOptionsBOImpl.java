package com.auzmor.impl.bo;

import com.auzmor.bo.FieldOptionsBO;
import com.auzmor.dao.EmployeeDAO;
import com.auzmor.impl.dao.EmployeeDAOImpl;
import com.auzmor.impl.enumarator.FieldOptionsEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;

public class FieldOptionsBOImpl implements FieldOptionsBO {
  private static Logger logger = LoggerFactory.getLogger(FieldOptionsBOImpl.class);

  /**
   * To fetch field Options values
   * @param dbHandle
   * @param requestParams
   * @return
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public ReturnObject<FieldOptionsEnum, JsonArray> getFieldOptionValues(DbHandle dbHandle,
                                                                        MultiMap requestParams)
      throws SQLException,DbHandleException{
    String location = "id,location,company_address_line1";
    String jobTitle = "id,title";
    String department = "id,department";
    String payGroup = "paygroup_id,paygroup_name";
    String division = "division_id,division_name";
    String employmentStatus = "id,employmentType";
    String fields = requestParams.get("fields");
    int organizationId = Integer.valueOf(requestParams.get("organization_id"));
    JsonArray resultSetArray = new JsonArray();
    
    EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
    JsonObject dataObject = new JsonObject();
    if (StringUtil.printValidString(fields).equals("")) {
      return new ReturnObjectImpl<FieldOptionsEnum, JsonArray>
          (FieldOptionsEnum.NO_SUCH_VALUE,new JsonArray());
    }
    if (fields.contains("location")) {
      dataObject.put("location", employeeDAO.getEmployeeCountForFieldOptions(location,
          "Location","location",organizationId));
    }
    if (fields.contains("jobTitle")) {
      dataObject.put("jobTitle", employeeDAO.getEmployeeCountForFieldOptions(jobTitle,
          "Title", "job_title",  organizationId));
    }
    if (fields.contains("department")) {
      dataObject.put("department", employeeDAO.getEmployeeCountForFieldOptions(department, 
          "Department","department", organizationId));
    }
    if (fields.contains("paygroup")) {
      dataObject.put("paygroup", employeeDAO.getEmployeeCountForFieldOptions(payGroup,
          "PayGroup","pay_group", organizationId));
    }
    if (fields.contains("division")) {
      dataObject.put("division", employeeDAO.getEmployeeCountForFieldOptions(division, 
          "Division","division", organizationId));
    }
    if (fields.contains("employmentType")) {
      dataObject.put("employmentType", employeeDAO.getEmployeeCountForFieldOptions(employmentStatus,
          "EmploymentType", "employment_status", organizationId));
    }
    resultSetArray.add(dataObject);
    logger.info("Field Options details :"+resultSetArray.size());
    return new ReturnObjectImpl<FieldOptionsEnum, JsonArray>
        (FieldOptionsEnum.VALUE_SUCCESS,resultSetArray);
  }
}
