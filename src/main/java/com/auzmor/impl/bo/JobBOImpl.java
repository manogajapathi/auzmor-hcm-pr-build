package com.auzmor.impl.bo;

import com.auzmor.bo.JobBO;
import com.auzmor.bo.LoginBO;
import com.auzmor.bo.OrganizationBO;
import com.auzmor.dao.CandidateDAO;
import com.auzmor.dao.CandidateHiringDAO;
import com.auzmor.dao.EmployeeDAO;
import com.auzmor.dao.JobDAO;
import com.auzmor.impl.dao.*;
import com.auzmor.impl.enumarator.bo.JobBOEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.Job.Collaborator;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.impl.util.DateUtil;
import com.auzmor.impl.util.Mail;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.Vertx;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JobBOImpl implements JobBO {

  private static Logger logger = LoggerFactory.getLogger(LoginBOImpl.class);

  private DbHandle dbHandle;

  private JobDAO jobDAO;

  public JobBOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  public JobBOImpl() {

  }
  int i =0;

  public JobDAO getJobDAO() {
    return jobDAO;
  }

  public void setJobDAO(JobDAO jobDAO) {
    this.jobDAO = jobDAO;
  }

  @Override
  public void moveCandidatesToTalentPool(int jobId, int creatorId, String organizationName)
      throws SQLException, DbHandleException {

    JobDAO jobDAO = new JobDAOImpl(this.dbHandle);
    jobDAO.moveCandidatesToTalentPool(jobId, creatorId, organizationName);
    logger.info("Not Hired Candidates moved to Talent Pool ");
  }

  @Override
  public JsonObject getJobHiringDetails(long candidateId)
      throws SQLException, DbHandleException {
    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    List<HashMap<String, String>> jobHiringDetails = jobDAO.getJobHiringDetails(candidateId);

    logger.info("Job Hire Details for a candidate obtained ");
    JsonObject dataObject = new JsonObject();
    for (HashMap<String, String> map : jobHiringDetails) {
      for (Map.Entry<String, String> mapEntry : map.entrySet()) {
        String key = mapEntry.getKey();
        String value = mapEntry.getValue();
          dataObject.put(key, this.getJobDetails(value, key, candidateId));
        // passing applicant value for checking true or false
      }
    }
    return dataObject;
  }

  @Override
  public Job getJobDetails(long jobId) throws SQLException, DbHandleException {

    JobDAO jobDAO = new JobDAOImpl(this.dbHandle);
    Job job = jobDAO.getJobDetails(jobId);
    logger.info("Job Details based on Job ID " + jobId + "Obtained");
    return job;
  }


  @Override
  public Job getJobDetailsWithoutStatus(long jobId) throws SQLException, DbHandleException {

    JobDAO jobDAO = new JobDAOImpl(this.dbHandle);
    Job job = jobDAO.getJobDetailsWithoutStatus(jobId);
    logger.info("Job details based on JobID " + jobId + " obtained");
    return job;
  }

  @Override
  public JsonArray getJobStatus(String status, long jobId, long organizationId)
      throws SQLException, DbHandleException {
    JobDAO jobDAO = new JobDAOImpl(this.dbHandle);
    JsonArray statusList = jobDAO.getJobStatus(status, jobId, organizationId);
    logger.info("Job status details obtained for Job ID " + jobId);
    return statusList;
  }

  @Override
  public JsonArray getAvailableJobStatus(String constraintString) throws SQLException {
    JobDAO jobDAO = new JobDAOImpl(this.dbHandle);

    logger.info("Available Job Statuses are listed ");
    return jobDAO.getJobStatus(constraintString);
  }

  /**
   * updateCollaborators - update Collaborator Information
   *
   * @param id
   * @param collaboratorParam
   * @throws SQLException
   * @throws DecodeException
   * @throws DbHandleException
   */
  @Override
  public List<String> updateCollaborators(int id, String collaboratorParam)
      throws SQLException, DecodeException, DbHandleException {

    JsonArray collaboratorArray = new JsonArray(collaboratorParam);

    JobDAO jobDAO = new JobDAOImpl(this.dbHandle);

    List<String> availableCollaborators = new ArrayList<>();
    List<String> newCollaborators = new ArrayList<>();

    for (int i = 0; i < collaboratorArray.size(); i++) {

      JsonObject collaboratorObject = collaboratorArray.getJsonObject(i);
      String collaborator = collaboratorObject.getString("collaborator");

      boolean isCollaboratorAvailable = jobDAO.isCollaboratorAvailable(collaborator, id);

      if (isCollaboratorAvailable) {
        JsonObject collaboratorInformation = jobDAO.getCollaboratorInformation(collaborator, id);
        boolean isCollaboratorActive = collaboratorInformation.getBoolean("collab_email_update");

        jobDAO.updateCollaborator(id, collaborator,
            collaboratorObject.getBoolean("collab_email_update"));
        if (!isCollaboratorActive) {
          notifyNewCollaboratorsViaEmail(collaborator, id);
        }

        availableCollaborators.add(collaborator);
        logger.info("Collaborator information updated for JOB id " + id);

      } else {
        jobDAO.insertCollaborator(id, collaborator,
            collaboratorObject.getBoolean("collab_email_update"));
        availableCollaborators.add(collaborator);
        newCollaborators.add(collaborator);
        this.notifyNewCollaboratorsViaEmail(collaborator, id);
        logger.info("Collaborator with employee id " + collaborator + "inserted for job id " + id);
      }

    }

    List<Collaborator> removableCollaboratorList = jobDAO.getRemovableCollaboratorList(id,
        availableCollaborators);

    if (!removableCollaboratorList.isEmpty()) {
      for (int k = 0; k < removableCollaboratorList.size(); k++) {

        jobDAO.removeCollaborator(removableCollaboratorList.get(k).getCollaborator_id());
        logger.info("Collaborator with employee id "
            + removableCollaboratorList.get(k).getCollaborator_id() +
            " removed as a collaborator for job id " + id);
        String collaboratorID = removableCollaboratorList.get(k).getCollaborator();
        String collaboratorEmailID = new DatabaseUtil()
            .getTablename("Employee", "work_email",
                "employee_id = " + collaboratorID);
        if (!collaboratorEmailID.isEmpty()) {
          JsonObject employeeObject = jobDAO.getHiringLeadName(collaboratorID);
          String employeeFirstName = employeeObject.getString("first_name");
          String employeeLastName = employeeObject.getString("last_name");
          Job job = jobDAO.getJobDetails(id);
          String title = job.getTitle();
          // adding this line to get the organization name in every mail
          OrganizationBO organizationBO = new OrganizationBOImpl(dbHandle);
          Organization organization = organizationBO.getOrganizationDetails((long) job.getOrganization_id());
          String organizationName = organization.getOrganization_name();
          organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);
          String emailBody = "<html><body>Hi " + employeeFirstName + " " + employeeLastName + ", " +
              "<br/> You have been removed as a " +
              "collaborator for the job post titled " + title + ". <br/>" +
              "Thank you,<br/>" + organizationName + " Team <br/>" +
              "----------------------------------------------------- ---------\n<br/>" +
              "This is system generated mail,please do not reply to this mail\n<br/>" +
              "-------------------------------------------------------------- " +
              "</body></html>";


          String collaboratorMailDeliveryStatus = new Mail().sendMail(Vertx.vertx(),
              emailBody,
              collaboratorEmailID, "Collaborator for Job Post");
          long adminid = 0;
          LoginBO loginBO = new LoginBOImpl(dbHandle);
          long organization_id = job.getOrganization_id();
          List<HashMap<String, String>> adminList = loginBO.GetAdminList(organization_id);
          for (HashMap<String, String> map : adminList) {
            JsonObject dataObject = new JsonObject();
            for (Map.Entry<String, String> mapEntry : map.entrySet()) {
              String key = mapEntry.getKey();
              String value = mapEntry.getValue();
              if (key.equals("id")) {
                adminid = Long.parseLong(value);
              }
              if (key.equals("email")) {
                mapEntry.getValue();
                String AdminEmail = value;
                  emailBody = "<html><body>Hi Admin,</br>" + employeeFirstName + " " + employeeLastName + ", " +
                      "have been removed as a " +
                      "collaborator for the job post titled " + title + ". <br/>" +
                      "Thank you, <br/>" + organizationName + " Team <br/> " +
                      "--------------------------------------------------------------\n<br/>" +
                      "This is system generated mail,please do not reply to this mail\n<br/>" +
                      "-------------------------------------------------------------- " +
                      "</body></html>";


                  new Mail().sendMail(Vertx.vertx(),
                      emailBody,
                      AdminEmail, "Collaborator for Job Post-Admin");
                }
              }
          }
        }
      }
    }
    return newCollaborators;

  }

  /*public JsonArray getJobDetails(String status, String key, long candidateId, String candidateStatus)
      throws SQLException, DbHandleException {
    CandidateHiringDAO hiringDAO = new CandidateHiringProcessDAOImpl(this.dbHandle);
    JsonArray getHiringList = new JsonArray();

    if (status != null && status.length() > 0) {
      String stringArray[] = status.split("::");
      for (int k = 0; k < stringArray.length; k++) {
        JsonObject object = new JsonObject();
        object.put(key, stringArray[k]);

        if (candidateStatus.equalsIgnoreCase("applicant")) {

          if ((key.equalsIgnoreCase("screening")) || (key.equalsIgnoreCase("offer"))||
              (key.equalsIgnoreCase("interview")) || (key.equalsIgnoreCase("hire")) ||
              (key.equalsIgnoreCase("not_hired"))) {
            object.put("status", false);
          } else {
            object.put("status", hiringDAO.getStatusCheck(key,
                stringArray[k], candidateId));
          }
        }
        else if (candidateStatus.equalsIgnoreCase("screening")) {

          if (key.equalsIgnoreCase("screening")) {
            object.put("status", hiringDAO.getStatusCheck(key,
                stringArray[k], candidateId));
          } else {
            if(key.equalsIgnoreCase("applicant")) {
              object.put("status", true);
            }
            else if((key.equalsIgnoreCase("offer")) ||
                (key.equalsIgnoreCase("interview")) || (key.equalsIgnoreCase("hire")) ||
                (key.equalsIgnoreCase("not_hired"))){
              object.put("status", false);
            }
          }
        }

        else if (candidateStatus.equalsIgnoreCase("Interview")) {

          if ((key.equalsIgnoreCase("Interview") &&
              stringArray[k].equalsIgnoreCase("Schedule Interview"))) {

            String reason = hiringDAO.getReasonCheck(key, stringArray[k], candidateId);

            if (reason != null && !reason.equals("")) {
              object.put("reason", reason);
              object.put("status", false);
            } else {
              object.put("status", hiringDAO.getStatusCheck(key,
                  stringArray[k], candidateId));
            }
          }  else {
            if((key.equalsIgnoreCase("applicant")) ||
                (key.equalsIgnoreCase("screening"))) {
              object.put("status", true);
            }
            else if((key.equalsIgnoreCase("offer")) ||
                (key.equalsIgnoreCase("hire"))||
                (key.equalsIgnoreCase("not_hired"))){
              object.put("status", false);
            }
          }
        }

        else if (candidateStatus.equalsIgnoreCase("Offer")) {

          if ((key.equalsIgnoreCase("Offer") &&
              stringArray[k].equalsIgnoreCase("Offer Extended"))) {

            String reason = hiringDAO.getReasonCheck(key, stringArray[k], candidateId);

            if (reason != null && !reason.equals("")) {
              object.put("reason", reason);
              object.put("status", false);
            } else {
              object.put("status", hiringDAO.getStatusCheck(key,
                  stringArray[k], candidateId));
            }
          }  else {
            if((key.equalsIgnoreCase("applicant")) ||
                (key.equalsIgnoreCase("screening")) || (key.equalsIgnoreCase("Interview"))) {
              object.put("status", true);
            }
            else if((key.equalsIgnoreCase("hire"))||
                (key.equalsIgnoreCase("not_hired"))){
              object.put("status", false);
            }
          }
        }

        else if (candidateStatus.equalsIgnoreCase("hire")) {

          if (key.equalsIgnoreCase("hire")) {
            object.put("status", hiringDAO.getStatusCheck(key,
                stringArray[k], candidateId));
          } else {
            if((key.equalsIgnoreCase("applicant")) || (key.equalsIgnoreCase("screening"))
                || (key.equalsIgnoreCase("offer")) ||
                (key.equalsIgnoreCase("interview"))) {
              object.put("status", true);
            }
            else if((key.equalsIgnoreCase("not_hired"))){
              object.put("status", false);
            }
          }
        }

        else if (candidateStatus.equalsIgnoreCase("Not Hired")) {
          if (key.equalsIgnoreCase("not_hired")) {
            object.put("status", hiringDAO.getStatusCheck("Not Hired",
                stringArray[k], candidateId));
          } else {
            if((key.equalsIgnoreCase("applicant")) || (key.equalsIgnoreCase("screening"))
                || (key.equalsIgnoreCase("offer")) ||  (key.equalsIgnoreCase("hire")) ||
                (key.equalsIgnoreCase("interview"))) {
              object.put("status", true);
            }
            }
        }
        getHiringList.add(object);
      }
    }
    return getHiringList;
  }*/

  public JsonArray getJobDetails(String status, String key, long candidateId)
      throws SQLException, DbHandleException {
    CandidateHiringDAO hiringDAO = new CandidateHiringProcessDAOImpl(this.dbHandle);
    JsonArray getHiringList = new JsonArray();
    if (status != null && status.length() > 0) {
      String stringArray[] = status.split("::");
        for (int subStatus = 0; subStatus < stringArray.length; subStatus++) {
          JsonObject object = new JsonObject();
          object.put(key, stringArray[subStatus]);
          if ((key.equalsIgnoreCase("Interview")) ||
              (key.equalsIgnoreCase("Offer"))) {
            String reason = hiringDAO.getReasonCheck(key, stringArray[subStatus], candidateId);
            if (reason != null && !reason.equals("")) {
              object.put("reason", reason);
              object.put("status", false);

            } else {
              // stringArray[k] is candidate Process in hiringProcess
              object.put("status", hiringDAO.getStatusCheck(key,
                  stringArray[subStatus], candidateId));
              if( hiringDAO.getStatusCheck(key,
                  stringArray[subStatus], candidateId) && key.equalsIgnoreCase("Interview")){
                CandidateInterviewDAOImpl candidateInterviewDAO = new CandidateInterviewDAOImpl(dbHandle);
                // getting interview substatus and checking is completed true or false
                object.put("status", candidateInterviewDAO.checkStatus( stringArray[subStatus]));
              }
            }
          } else {
            if (key.equalsIgnoreCase("not_hired")) {
              object.put("status", hiringDAO.getStatusCheck("Not Hired",
                  stringArray[subStatus], candidateId));
            } else {
              object.put("status", hiringDAO.getStatusCheck(key,
                  stringArray[subStatus], candidateId));
            }
          }
          getHiringList.add(object);
      }
    }
    return getHiringList;
  }

  /**
   * getJobCount
   *
   * @param organizationId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public long getJobCount(long organizationId) throws SQLException, DbHandleException {
    return new JobDAOImpl(this.dbHandle).getJobCount(organizationId);
  }

  @Override
  public List<HashMap<String, String>> getJobWithCandidateLoginId(long candidateLoginId,
                                                                  long organizationId)
      throws SQLException, DbHandleException {
    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    List<HashMap<String, String>> getJobDetails = jobDAO.getJobWithCandidateLoginId(candidateLoginId,
        organizationId);

    return getJobDetails;
  }

  /**
   * doesJobPostExists - checks whether a job post already exists
   *
   * @param job
   * @return
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public boolean doesJobPostExists(Job job) throws SQLException, DbHandleException {
    if (("Published".equalsIgnoreCase(job.getJob_status())) ||
        ("Draft".equalsIgnoreCase(job.getJob_status()))) {
      JobDAO jobDAO = new JobDAOImpl(dbHandle);
      return jobDAO.doesJobPostExists(job);
    } else {
      return false;
    }
  }

  /**
   * validateJobStatus - check whether job status update is valid
   *
   * @param id
   * @param jobStatus
   * @return
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public boolean validateJobStatus(int id, String jobStatus)
      throws SQLException, DbHandleException {

    JobDAO jobDAO = new JobDAOImpl(this.dbHandle);
    Job job = jobDAO.getJobDetailsWithoutStatus(id);
    String existingStatus = job.getJob_status();
    boolean isExistingStatusDraft = "Draft".equalsIgnoreCase(existingStatus);
    boolean isExistingStatusPublished = "Published".equalsIgnoreCase(existingStatus);
    boolean isExistingStatusUnpublished = "Unpublished".equalsIgnoreCase(existingStatus);
    boolean isExistingStatusClosed = "Closed".equalsIgnoreCase(existingStatus);

    boolean isRequiredStatusDraft = "Draft".equalsIgnoreCase(jobStatus);
    boolean isRequiredStatusPublished = "Published".equalsIgnoreCase(jobStatus);
    boolean isRequiredStatusUnpublished = "Unpublished".equalsIgnoreCase(jobStatus);
    boolean isRequiredStatusClosed = "Closed".equalsIgnoreCase(jobStatus);

    if (isExistingStatusPublished) {
      if (isRequiredStatusDraft) {
        return false;
      }
      if (isRequiredStatusClosed || isRequiredStatusPublished || isRequiredStatusUnpublished) {
        return true;
      }
    }

    if (isExistingStatusUnpublished) {
      return true;
    }

    if (isExistingStatusDraft) {
      if (isRequiredStatusPublished || isRequiredStatusDraft) {
        return true;
      }
      if (isRequiredStatusUnpublished || isRequiredStatusClosed) {
        return false;
      }
    }

    if (isExistingStatusClosed) {
      return false;
    }

    return false;
  }

  /**
   * getCollaboratorList Get Collaborator List based on job ID
   *
   * @param job
   * @return
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public ReturnObject<JobBOEnum, JsonObject> getCollaboratorList(Job job)
      throws SQLException, DbHandleException {
    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    JobBOEnum jobBOEnum;
    JsonObject object = new JsonObject();
    JsonArray collaboratorArray = new JsonArray();

    boolean doesJobExists = jobDAO.isJobIDValid(job);
    if (!doesJobExists) {
      int jobCount = Integer.parseInt(new DatabaseUtil().
          getTablename("Job", "count(1)",
              "organization_id =" + job.getOrganization_id()));
      Organization organization = new OrganizationDAOImpl(dbHandle)
          .getOrganizationById(job.getOrganization_id());
      boolean isOrganizationAvailable = (organization != null);
      if (jobCount == 0 && isOrganizationAvailable && job.getId() == 0) {
        JsonObject sampleObject = new JsonObject();
        sampleObject.put("collaborator_id", "0");
        sampleObject.put("collaborator_name", "Loga B");

        collaboratorArray = new JsonArray();
        collaboratorArray.add(sampleObject);
        object.put("collaborators", collaboratorArray);
        object.put("collaborators_count", 1);
        return new ReturnObjectImpl<JobBOEnum, JsonObject>(JobBOEnum.COLLOBARATOR_LIST_RETREIVED,
            object);
      }

      if (job.getId() == 0) {
        object.put("collaborators", collaboratorArray);
        object.put("collaborators_count", 0);
        jobBOEnum = JobBOEnum.COLLOBARATOR_LIST_RETREIVED;
        return new ReturnObjectImpl<JobBOEnum, JsonObject>(jobBOEnum, object);
      }

      jobBOEnum = JobBOEnum.INVALID_JOB_ID;
      return new ReturnObjectImpl<JobBOEnum, JsonObject>(jobBOEnum, object);
    }
    JsonArray jsonArray = new JsonArray();
    List<JsonObject> collaboratorList = jobDAO.getHiringLeadAndCollaboratorsWithJobId(job);
    logger.info("Collaborator Details obtained based on job id " + job.getId());
    for (JsonObject collabObject : collaboratorList) {
      jsonArray.add(collabObject);
    }
    object.put("collaborators", jsonArray);
    object.put("collaborators_count", jsonArray.size());
    return new ReturnObjectImpl<JobBOEnum, JsonObject>(
        JobBOEnum.COLLOBARATOR_LIST_RETREIVED, object);
  }

  /**
   * isHiringLeadValid - Check whether the hiring lead is valid
   *
   * @param organizationId
   * @param hiringLeadId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public boolean isHiringLeadValid(int organizationId, String hiringLeadId)
      throws SQLException, DbHandleException {
    int hiringLeadID = StringUtil.isNumber(hiringLeadId) ?
        Integer.parseInt(hiringLeadId) : 0;
    Employee employee = new EmployeeDAOImpl(dbHandle).getEmployeeDetails(hiringLeadID);
    if (employee == null)
      return false;
    if (employee.getOrganization_id() != organizationId) {
      return false;
    }

    return true;
  }

  /**
   * notifyNewHiringLeadViaEmail - notify new hiring lead via email
   *
   * @param hiringLead
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public void notifyNewHiringLeadViaEmail(int hiringLead, int jobId)
      throws SQLException, DbHandleException {
    String hiringLeadMailID = new DatabaseUtil()
        .getTablename("Employee", "work_email",
            "employee_id = " + hiringLead);

    if (!hiringLeadMailID.isEmpty()) {

      Mail mail = new Mail();
      JobDAO jobDAO = new JobDAOImpl(dbHandle);
      JsonObject employeeObject = jobDAO.getHiringLeadName(String.valueOf(hiringLead));
      String employeeFirstName = employeeObject.getString("first_name");
      String employeeLastName = employeeObject.getString("last_name");
      Job job = jobDAO.getJobDetails(jobId);
      String title = job.getTitle();
      // adding this line to get the organization name in every mail
      OrganizationBO organizationBO = new OrganizationBOImpl(dbHandle);
      Organization organization = organizationBO.getOrganizationDetails((long) job.getOrganization_id());
      String organizationName = organization.getOrganization_name();
      organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);

      String emailBody = "<html><body>Hi " + employeeFirstName + " " + employeeLastName + ", " +
          "<br/> You have been added as a " +
          "hiring lead for the job post titled " + title + ". <br/>" +
          "Thank you,<br/>"+ organizationName +" Team <br/>" +
          "--------------------------------------------------------------\n<br/>" +
          "This is system generated mail,please do not reply to this mail\n<br/>" +
          "-------------------------------------------------------------- " +
          "</body></html>";

      String hiringLeadMailDeliveryStatus = mail.sendMail(Vertx.vertx(),
          emailBody,
          hiringLeadMailID, "Hiring Lead for Job Post");
      logger.info("Hiring Lead has been sent mail notifications for Job ID " + jobId);
      long adminid = 0;
      LoginBO loginBO = new LoginBOImpl(dbHandle);
      long organization_id = job.getOrganization_id();
      List<HashMap<String, String>> adminList = loginBO.GetAdminList(organization_id);
      for (HashMap<String, String> map : adminList) {
        for (Map.Entry<String, String> mapEntry : map.entrySet()) {
          String key = mapEntry.getKey();
          String value = mapEntry.getValue();
          if (key.equals("id")) {
            adminid = Long.parseLong(value);
          }
          if (key.equals("email")) {
            mapEntry.getValue();
            String AdminEmail = value;
              emailBody = "<html><body>Hi Admin,</br>" + employeeFirstName + " " + employeeLastName + ", " +
                  "collaborator for the job post titled " + title + ". <br/>" +
                  "Thank you,<br/>"+ organizationName +" Team <br/>" +
                  "--------------------------------------------------------------\n<br/>" +
                  "This is system generated mail,please do not reply to this mail\n<br/>" +
                  "-------------------------------------------------------------- " +
                  "</body></html>";
              new Mail().sendMail(Vertx.vertx(),
                  emailBody,
                  AdminEmail, "Hiring for Job Post-Admin");
          }
        }
      }
    }
  }

  /**
   * getJobDetailsForEmployee
   *
   * @param employeeId
   * @param format
   * @return
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public JsonArray getJobDetailsForEmployee(long employeeId, String format)
      throws SQLException, DbHandleException {
    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    List<JsonObject> dataList = new ArrayList<>();
    dataList = jobDAO.getJobDetailsForEmployee(employeeId);
    JsonArray resultArray = new JsonArray();
    for (JsonObject jsonObject : dataList) {
      EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
      if (jsonObject.getLong("hiring_lead") == 0) {
        jsonObject.put("hiring_lead", "-");
      } else {
        Employee employee = employeeDAO.getEmployeeDetails(jsonObject.getLong("hiring_lead"));
        JsonObject employeeObject = new JsonObject();
        employeeObject.put("employee_id", jsonObject.getLong("hiring_lead"));
        employeeObject.put("first_name", employee.getFirst_name());
        employeeObject.put("last_name", employee.getLast_name());
        employeeObject.put("profile_image", employee.getProfile_image());
        jsonObject.put("hiring_lead", employeeObject);
      }
      CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
      long candidateCount = candidateDAO.getCandidateCountForJob(jsonObject.getLong("id"));
      jsonObject.put("candidate_count", candidateCount);
      if (!jsonObject.getString("date_posted").contains("00")) {
        String date = null;
        try {
          date = new DateUtil().getDateFormat(format, StringUtil
              .printValidString(jsonObject.getString("date_posted")));
        } catch (ParseException e) {
          e.printStackTrace();
        }
        jsonObject.put("date_posted", date);
      }

      resultArray.add(jsonObject);
    }
    return resultArray;
  }

  /**
   * notifyNewCollaboratorsViaEmail - notify new collaborators via email
   *
   * @param collaborator
   * @param jobId
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public void notifyNewCollaboratorsViaEmail(String collaborator, int jobId)
      throws SQLException, DbHandleException {

    String collaboratorEmailId = new DatabaseUtil()
        .getTablename("Employee", "work_email",
            "employee_id = " + collaborator);
    if (!collaboratorEmailId.isEmpty()) {

      Mail mail = new Mail();
      JobDAO jobDAO = new JobDAOImpl(dbHandle);
      JsonObject employeeObject = jobDAO.getHiringLeadName(collaborator);
      String employeeFirstName = employeeObject.getString("first_name");
      String employeeLastName = employeeObject.getString("last_name");
      Job job = jobDAO.getJobDetails(jobId);
      String title = job.getTitle();
      // adding this line to get the organization name in every mail
      OrganizationBO organizationBO = new OrganizationBOImpl(dbHandle);
      Organization organization = organizationBO.getOrganizationDetails((long) job.getOrganization_id());
      String organizationName = organization.getOrganization_name();
      organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);

      String emailBody = "<html><body>Hi " + employeeFirstName + " " + employeeLastName + ", " +
          "<br/> You have been added as a " +
          "collaborator for the job post titled " + title + ". <br/>" +
          "Thank you,<br/>" + organizationName + " Team <br/> " +
          "--------------------------------------------------------------\n<br/>" +
          "This is system generated mail,please do not reply to this mail\n<br/>" +
          "-------------------------------------------------------------- " +
          "</body></html>";

      String collaboratorMailDeliveryStatus = mail.sendMail(Vertx.vertx(),
          emailBody,
          collaboratorEmailId, "Collaborator for Job Post");
      long adminid = 0;
      LoginBO loginBO = new LoginBOImpl(dbHandle);
      long organization_id = job.getOrganization_id();
      List<HashMap<String, String>> adminList = loginBO.GetAdminList(organization_id);
      for (HashMap<String, String> map : adminList) {
        for (Map.Entry<String, String> mapEntry : map.entrySet()) {
          String key = mapEntry.getKey();
          String value = mapEntry.getValue();
          if (key.equals("id")) {
            adminid = Long.parseLong(value);
          }
          if (key.equals("email")) {
            mapEntry.getValue();
            String AdminEmail = value;
              emailBody = "<html><body>Hi Admin,</br>" + employeeFirstName + " " + employeeLastName + ", " +
                  "collaborator for the job post titled " + title + ". <br/>" +
                  "Thank you,<br/>" + organizationName + " Team<br/>" +
                  "--------------------------------------------------------------\n<br/>" +
                  "This is system generated mail,please do not reply to this mail\n<br/>" +
                  "-------------------------------------------------------------- " +
                  "</body></html>";


              new Mail().sendMail(Vertx.vertx(),
                  emailBody,
                  AdminEmail, "Collaborator for Job Post-Admin");
            }
          }
        }
      }
  }

  /**
   * @param dbHandle
   * @param job
   * @return
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public ReturnObject<JobBOEnum, JsonObject> cloneJob(DbHandle dbHandle, Job job)
      throws SQLException, DbHandleException {
    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    boolean doesJobExists = jobDAO.isJobIDValid(job);
    JobBOEnum jobBOEnum;
    if (!doesJobExists) {
      jobBOEnum = JobBOEnum.INVALID_JOB_ID;
      return new ReturnObjectImpl<JobBOEnum, JsonObject>(jobBOEnum, new JsonObject());
    }

    int generatedJobID = jobDAO.cloneJob(job);

    if (generatedJobID > 0) {
      JsonObject object = new JsonObject();
      object.put("id", generatedJobID);
      object.put("message", "Job Cloned Successfully");
      jobBOEnum = JobBOEnum.JOB_POST_SUCCESS;

      return new ReturnObjectImpl<JobBOEnum, JsonObject>(jobBOEnum, object);
    }
    jobBOEnum = JobBOEnum.JOB_POST_INVALID;
    return new ReturnObjectImpl<JobBOEnum, JsonObject>(jobBOEnum, new JsonObject());

  }
}
