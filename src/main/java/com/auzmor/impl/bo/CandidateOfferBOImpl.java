package com.auzmor.impl.bo;

import com.auzmor.bo.CandidateOfferBO;
import com.auzmor.bo.EmployeeBO;
import com.auzmor.bo.JobBO;
import com.auzmor.bo.OrganizationBO;
import com.auzmor.dao.*;
import com.auzmor.impl.dao.*;
import com.auzmor.impl.enumarator.bo.CandidateOfferEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.ResponseHelper;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.impl.models.CandidateR.CommentCandidate;
import com.auzmor.impl.models.CandidateR.HiringProcessCandidate;
import com.auzmor.impl.models.CandidateR.OfferProcessCandidate;
import com.auzmor.impl.util.DateUtil;
import com.auzmor.impl.util.Mail;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.util.db.DbHandle;
import com.mysql.cj.core.util.StringUtils;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.ext.web.RoutingContext;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

public class CandidateOfferBOImpl implements CandidateOfferBO {

  private static Logger logger = LoggerFactory.getLogger(CandidateOfferBOImpl.class);
  private CandidateOfferDAO candidateOfferDAO;
  private DbHandle dbHandle;

  public CandidateOfferBOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  public void setCandidateOfferDAO(CandidateOfferDAO candidateOfferDAO) {
    this.candidateOfferDAO = candidateOfferDAO;
  }

  /**
   * @param paramsMap
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author sadhanaVenkatesan
   */
  @Override
  public long addOfferProcess(MultiMap paramsMap) throws SQLException, DbHandleException {
    OfferProcessCandidate offerProcessCandidate = new OfferProcessCandidate();
    String date = paramsMap.get("offer_start_date");
    DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    try {
      offerProcessCandidate.setOfferStartDate(formatter.parse(date));
      logger.info("offer set date is " + formatter.parse(date));
    } catch (ParseException e) {
      e.printStackTrace();
      logger.info("offer set date is not Format");
    }
    offerProcessCandidate.setCandidateId(Long.parseLong(paramsMap.get("candidate_id")));
    offerProcessCandidate.setOfferPosition(paramsMap.get("offer_position"));
    offerProcessCandidate.setOfferPayRate(paramsMap.get("offer_pay_rate"));
    offerProcessCandidate.setOfferJobLocation(paramsMap.get("offer_job_location"));
    offerProcessCandidate.setOfferContractDetails(StringUtil.printValidString(
        paramsMap.get("offer_contract_details")));
    offerProcessCandidate.setAdminName(paramsMap.get("admin_name"));
    offerProcessCandidate.setSubjectName(paramsMap.get("subject_name"));
    offerProcessCandidate.setOfferAttachmentName(StringUtil.printValidString(
        paramsMap.get("offer_attachment_name")));
    offerProcessCandidate.setUserId(Long.parseLong(paramsMap.get("user_id")));
    offerProcessCandidate.setCurrencyType(paramsMap.get("currency_type"));
    offerProcessCandidate.setSubStatus(paramsMap.get("sub_status"));

    CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);
    CandidateR candidateR = candidateDAO.getCandidate(
        Long.parseLong(paramsMap.get("candidate_id")));

    OrganizationDAO organizationDAO = new OrganizationDAOImpl(dbHandle);
    Organization organization = organizationDAO.
        getOrganizationById(candidateR.getOrganizationId());

    offerProcessCandidate.setOfferMailDescription(organization.getOrganization_name() +
        " would like to extend a job offer to you for the" +
        "  position of " + paramsMap.get("offer_position") + "." +
        organization.getOrganization_name() + " would like to offer you a pay rate of" +
        " [" + paramsMap.get("currency_type") + ".'" + paramsMap.get("offer_pay_rate") + "']" +
        " which would be effective on your first day of work on '" +
        paramsMap.get("offer_start_date") + "'" +
        " at our '" + paramsMap.get("offer_job_location") + "' location. <br/>" +
        " Please review any attachments/details and <a href= https://" + organization.getHris_url() +
        "/auth/candidate-login>click here</a> to respond to your offer. <br/> " +
        " '" + StringUtil.printValidString(paramsMap.get("offer_contract_details")) + "' <br");

    CandidateOfferDAOImpl offerDAO = new CandidateOfferDAOImpl(this.dbHandle);
    return offerDAO.create(offerProcessCandidate);
  }

  /**
   * @param paramsMap
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author sadhanaVenkatesan
   */
  @Override
  public CandidateOfferEnum updateOfferProcess(MultiMap paramsMap, RoutingContext routingContext)
      throws SQLException, DbHandleException {

    String status = StringUtil.printValidString(paramsMap.get("status"));
    String cancelReason = StringUtil.printValidString(paramsMap.get("offer_decline_reason"));
    String otherReason = StringUtil.printValidString(paramsMap.get("offer_other_reason"));
    long candidateId = Long.parseLong(paramsMap.get("candidateId"));
    long userId;
    String adminName = "";
    if (!StringUtil.printValidString(paramsMap.get("user_id")).equals("")) {
      userId = Long.parseLong(paramsMap.get("user_id"));
    } else {
      userId = 0;
    }
    if (!StringUtil.printValidString(paramsMap.get("admin_name")).equals("")) {
      adminName = paramsMap.get("admin_name");
    }

    String comments = "";
    CandidateOfferDAO candidateOfferDAO = new CandidateOfferDAOImpl(this.dbHandle);
    int id;

    EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
    JobDAO jobDAO = new JobDAOImpl(dbHandle);
    CandidateDAO candidateDAO = new CandidateDAOImpl(dbHandle);

    CandidateR candidate = candidateDAO.getCandidate(candidateId);
    Job job = jobDAO.getJobDetails(candidate.getJobId());
    String userName = candidate.getFirstName() + " " + candidate.getLastName();

    OrganizationBO organizationBO = new OrganizationBOImpl(dbHandle);
    Organization organization = organizationBO.getOrganizationDetails(candidate.getOrganizationId());
    String organizationName = organization.getOrganization_name();
    organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);
    Mail mail = new Mail();

    if ((!status.equals("") || status.length() > 0) &&
        (!cancelReason.equals("") || cancelReason.length() > 0)) {

      HiringProcessCandidate candidateHiringProcessModel = new HiringProcessCandidate();
      CandidateHiringProcessDAOImpl candidateHiringProcessDao =
          new CandidateHiringProcessDAOImpl(dbHandle);

      comments = "rejected your offer with a response of: " + cancelReason;
      if (cancelReason.equalsIgnoreCase("No longer interested")) {
        id = candidateOfferDAO.updateCancelStatus(cancelReason, status, candidateId, userId);
        if (id > 0) {
          candidateHiringProcessModel.setStatus("Not Hired");
          candidateHiringProcessModel.setCandidateProcess("Declined Offer");
          candidateHiringProcessModel.setReason(cancelReason);
          candidateHiringProcessModel.setCandidateId(candidateId);
          candidateHiringProcessModel.setUserId(userId);

          id = candidateHiringProcessDao.create(candidateHiringProcessModel);
        } else {
          return CandidateOfferEnum.FAILED;
        }
      } else {
        id = candidateOfferDAO.updateStatus(status, candidateId);
        if (id > 0) {
          candidateHiringProcessModel.setStatus("Offer");
          candidateHiringProcessModel.setCandidateProcess("Offer Extended");
          candidateHiringProcessModel.setReason(cancelReason);
          candidateHiringProcessModel.setCandidateId(candidateId);
          candidateHiringProcessModel.setUserId(userId);
          if (cancelReason.equalsIgnoreCase("Incorrect Information") ||
              cancelReason.equalsIgnoreCase("Other")) {
            candidateHiringProcessModel.setOtherCancelReason(otherReason);
          }
          id = candidateHiringProcessDao.create(candidateHiringProcessModel);
        } else {
          return CandidateOfferEnum.FAILED;
        }
      }

      OfferProcessCandidate offerProcessCandidate = candidateOfferDAO.
          getOfferProcessDetails(candidateId);
      long employeeId = offerProcessCandidate.getUserId();
      Employee employee = employeeBO.getEmployeeDetails(employeeId);

      if (cancelReason.equalsIgnoreCase("Incorrect Information")) {
        String body = "<html><body> Dear Admin, <br> <br>" +
            "<p>This is to inform you that " + userName + " has rejected the offer for the " +
            " position of " + job.getTitle() + ".\n " +
            "</b></p> <br><br> <br>" +
            "Thank you,\n<br/> " + organizationName + " Team <br/>" +
            "--------------------------------------------------------------\n<br/>" +
            "This is a system generated mail.\n<br/>" +
            "Please do not reply to this mail\n<br/>" +
            "-------------------------------------------------------------- " +
            "</body></html>";

        String subjectName = "Rejection of offer";
        if (!"success".equals(mail.sendMail(Vertx.vertx(),
            body,
            employee.getWork_email(), subjectName))) {
          return CandidateOfferEnum.SEND_FAILED;

        }
      } else if (cancelReason.equalsIgnoreCase("Pay is too low")) {

        if (employee != null) {
          String body = "<html><body> Dear Admin, <br> <br>" +
              "<p>This is to inform you that " + userName + " has declined the offer extended " +
              " for the position of " + job.getTitle() + ".\n " +
              " citing low salary package as the reason.  </b></p> <br><br> <br>" +
              " Thank you,\n<br/> " + organizationName + " Team <br/>" +
              "--------------------------------------------------------------\n<br/>" +
              "This is a system generated mail.\n<br/>" +
              "Please do not reply to this mail\n<br/>" +
              "-------------------------------------------------------------- " +
              "</body></html>";

          String subjectName = "Rejection of offer";
          if (!"success".equals(mail.sendMail(Vertx.vertx(),
              body,
              employee.getWork_email(), subjectName))) {
            return CandidateOfferEnum.SEND_FAILED;

          }
        }

        if (job.getHiring_lead().length() > 0 || !StringUtil.printValidString(job.getHiring_lead()).equals("")) {
          Employee employeeHiringLead = employeeBO.getEmployeeDetails(Long.parseLong(job.getHiring_lead()));
          if (employeeHiringLead != null) {
            String body = "<html><body> Dear HiringLead, <br> <br>" +
                "<p>This is to inform you that " + userName + " has declined the offer extended for the position of " + job.getTitle() + ".\n " +
                "citing low salary package as the reason.  </b></p> <br><br> <br>" +
                "Thank you,\n<br/> " + organizationName + " Team <br/>" +
                "--------------------------------------------------------------\n<br/>" +
                "This is a system generated mail.\n<br/>" +
                "Please do not reply to this mail\n<br/>" +
                "-------------------------------------------------------------- " +
                "</body></html>";

            String subjectName = "Rejection of offer-For HiringLead";
            if (!"success".equals(mail.sendMail(Vertx.vertx(),
                body,
                employeeHiringLead.getWork_email(), subjectName))) {
              return CandidateOfferEnum.SEND_FAILED;

            }
          }
        }
      } else if (cancelReason.equalsIgnoreCase("Took another position") ||
          cancelReason.equalsIgnoreCase("Not interested")) {

        if (employee != null) {
          String body = "<html><body> Dear Admin, <br> <br>" +
              "<p>This is to inform you that " + userName + " has declined the offer extended for" +
              " the position of " + job.getTitle() + ".\n " +
              " with the cited reason being acceptance of a job position with another organization." +
              "</b></p> <br><br> <br>" +
              "Thank you,\n<br/> " + organizationName + " Team <br/>" +
              "--------------------------------------------------------------\n<br/>" +
              "This is a system generated mail.\n<br/>" +
              "Please do not reply to this mail\n<br/>" +
              "-------------------------------------------------------------- " +
              "</body></html>";

          String subjectName = "Rejection of offer";
          if (!"success".equals(mail.sendMail(Vertx.vertx(),
              body,
              employee.getWork_email(), subjectName))) {
            return CandidateOfferEnum.SEND_FAILED;

          }
        }

        if (job.getHiring_lead().length() > 0 || !StringUtil.printValidString(job.getHiring_lead()).equals("")) {
          Employee employeeHiringLead = employeeBO.getEmployeeDetails(Long.parseLong(job.getHiring_lead()));
          if (employeeHiringLead != null) {
            String body = "<html><body> Dear HiringLead, <br> <br>" +
                "<p>This is to inform you that " + userName + " has declined the offer " +
                " extended for the position of " + job.getTitle() + ".\n " +
                " with the cited reason being acceptance of a job position with another organization.  </b></p> <br><br> <br>" +
                "Thank you,\n<br/>" + organizationName + " Team <br/>" +
                "--------------------------------------------------------------\n<br/>" +
                "This is a system generated mail.\n<br/>" +
                "Please do not reply to this mail\n<br/>" +
                "-------------------------------------------------------------- " +
                "</body></html>";

            String subjectName = "Rejection of offer-For HiringLead";
            if (!"success".equals(mail.sendMail(Vertx.vertx(),
                body,
                employeeHiringLead.getWork_email(), subjectName))) {
              logger.info("mail sent failed");
              return CandidateOfferEnum.SEND_FAILED;

            }
            logger.info("mail sent success");
          }
        }
      }
    } else if (!cancelReason.equals("") || cancelReason.length() > 0) {
      /*----------------checking access role for admin/HiringLead/Collaborator ---------------------*/
      String userType = routingContext.data().get("userType").toString();
      int hiringLead = 0;

      // checking userType not equal to admin
      if (!userType.equalsIgnoreCase("admin")) {

        int employeeId = ((Employee) routingContext.data().get("employee")).getEmployee_id();

        String hiringLeadId = job.getHiring_lead();

        if (!StringUtils.isNullOrEmpty(hiringLeadId)) {
          hiringLead = Integer.parseInt(hiringLeadId);
        }
        // checking hiringLead whether employeeId had access to this job.
        if (employeeId != hiringLead && hiringLead > 0) {
        return CandidateOfferEnum.ACCESS_CHECK;
        }
      }

      comments = "decline an Offer";
      status = "Admin Rejected";
      id = candidateOfferDAO.updateCancelStatus(cancelReason, status, candidateId, userId);
      /* candidate status update */
      candidateDAO.updateCandidateStatus("Offer", candidateId);
    } else if (status.equals("accept")) {
      id = candidateOfferDAO.updateStatus(status, candidateId);

      OfferProcessCandidate offerProcessCandidate = candidateOfferDAO.
          getOfferProcessDetails(candidateId);
      long employeeId = offerProcessCandidate.getUserId();
      Employee employee = employeeBO.getEmployeeDetails(employeeId);

      String body = "";
      String subjectName = "Offer acceptance by candidate";
      if (employee != null) {
        body = "<html><body>Dear Admin, <br> <br>" +
            "<p> \n" +
            " Congratulations! We are glad to inform you that " + userName + " has accepted the offer " +
            " for the position of " + job.getTitle() + " in your organization. " +
            " Time to celebrate and welcome the new employee!!</p> <br> <br>" +
            " Thank you, <br>" + organizationName + " Team <br/>" +
            " </body></html>";

        if (!"success".equals(mail.sendMail(Vertx.vertx(),
            body,
            employee.getWork_email(), subjectName))) {
          return CandidateOfferEnum.SEND_FAILED;

        }
      }

      if (job.getHiring_lead().length() > 0 || !StringUtil.printValidString(job.getHiring_lead()).equals("")) {
        Employee employeeHiringLead = employeeBO.getEmployeeDetails(Long.parseLong(job.getHiring_lead()));
        if (employeeHiringLead != null) {
          body = "<html><body>Dear Hiring lead, <br> <br>" +
              "<p> \n" +
              " Congratulations! We are glad to inform you that " + userName + " has accepted the offer " +
              " for the position of " + job.getTitle() + " in your organization. " +
              " Time to celebrate and welcome the new employee!!</p> <br> <br>" +
              " Thank you, <br>" + organizationName + " Team <br/>" +
              " </body></html>";

          if (!"success".equals(mail.sendMail(Vertx.vertx(),
              body,
              employeeHiringLead.getWork_email(), subjectName + "-For HiringLead"))) {
            logger.info("mail sent failed");
            return CandidateOfferEnum.SEND_FAILED;

          }
          logger.info("mail sent success");
        }
      }

    } else {
       /*----------------checking access role for admin/HiringLead/Collaborator ---------------------*/
      String userType = routingContext.data().get("userType").toString();
      int hiringLead = 0;

      // checking userType not equal to admin
      if (!userType.equalsIgnoreCase("admin")) {

        int employeeId = ((Employee) routingContext.data().get("employee")).getEmployee_id();

        String hiringLeadId = job.getHiring_lead();

        if (!StringUtils.isNullOrEmpty(hiringLeadId)) {
          hiringLead = Integer.parseInt(hiringLeadId);
        }
        // checking hiringLead whether employeeId had access to this job.
        if (employeeId != hiringLead && hiringLead > 0) {
          return CandidateOfferEnum.ACCESS_CHECK;
        }
      }
      comments = "Extended offer updated";
      OfferProcessCandidate offerProcessCandidate = new OfferProcessCandidate();

      if (!StringUtil.printValidString(paramsMap.get("offer_start_date")).equals("")) {
        String offerDate = paramsMap.get("offer_start_date");
        try {
          offerProcessCandidate.setOfferStartDate(new DateUtil().getDate().parse(offerDate));
        } catch (ParseException e) {
          logger.error("Date Format Exception");
        }
      }
      offerProcessCandidate.setCandidateId(candidateId);
      offerProcessCandidate.setOfferPosition(paramsMap.get("offer_position"));
      offerProcessCandidate.setOfferPayRate(paramsMap.get("offer_pay_rate"));
      offerProcessCandidate.setOfferJobLocation(paramsMap.get("offer_job_location"));
      offerProcessCandidate.setOfferContractDetails(
          StringUtil.printValidString(paramsMap.get("offer_contract_details")));
      offerProcessCandidate.setAdminName(paramsMap.get("admin_name"));
      offerProcessCandidate.setSubjectName(paramsMap.get("subject_name"));
      offerProcessCandidate.setOfferAttachmentName(
          StringUtil.printValidString(paramsMap.get("offer_attachment_name")));
      offerProcessCandidate.setUserId(userId);
      offerProcessCandidate.setCurrencyType(paramsMap.get("currency_type"));
      offerProcessCandidate.setSubStatus(paramsMap.get("sub_status"));

      id = candidateOfferDAO.updateOfferDetails(offerProcessCandidate);
      /* candidate status update */
      candidateDAO.updateCandidateStatus("Offer", candidateId);
    }
    if (id > 0) {
      if (!comments.equals("")) {
        CommentCandidate commentsModel = new CommentCandidate();
        java.util.Date dNow = new java.util.Date();
        String currentDate = new DateUtil().getDate().format(dNow);
        try {
          commentsModel.setCommentsDate(new DateUtil().getDate().parse(currentDate));
        } catch (ParseException e) {
          logger.error("Date Format Exception");
        }
        commentsModel.setAdminName(adminName);
        commentsModel.setComments(comments);
        commentsModel.setCandidateId(candidateId);
        commentsModel.setUserId(userId);
        commentsModel.setSystemGenerated(true);
        CandidateCommentDAO candidateCommentDAO = new CandidateCommentDAOImpl(dbHandle);
        id = candidateCommentDAO.create(commentsModel);
        if (id == 0) {
          logger.info("offer update Failed");
          return CandidateOfferEnum.FAILED;
        }
      }
      logger.info("offer update success");
      return CandidateOfferEnum.SUCCESS;
    } else {
      logger.info("No update");
      return CandidateOfferEnum.NO_UPDATE;
    }

  }

  /**
   * @param candidateId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author sadhanaVenkatesan
   */
  @Override
  public OfferProcessCandidate getOfferDetails(long candidateId)
      throws SQLException, DbHandleException {
    OfferProcessCandidate offerProcessCandidate;
    CandidateOfferDAO offerDAO = new CandidateOfferDAOImpl(this.dbHandle);
    offerProcessCandidate = offerDAO.getOfferProcessDetails(candidateId);
    return offerProcessCandidate;
  }

  /**
   * @param candidateId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author sadhanaVenkatesan
   */
  @Override
  public CandidateOfferEnum mailSent(long candidateId, String subjectName)
      throws SQLException, DbHandleException {
    CandidateDAO candidateDAO = new CandidateDAOImpl(this.dbHandle);
    CandidateR candidateR = candidateDAO.getCandidate(candidateId);

    OrganizationDAO organizationDAO = new OrganizationDAOImpl(this.dbHandle);
    Organization organization = organizationDAO.
        getOrganizationById(candidateR.getOrganizationId());
    String organizationName = organization.getOrganization_name();
    organizationName = organizationName.substring(0, 1).toUpperCase() + organizationName.substring(1);


    CandidateOfferBO candidateOfferBO = new CandidateOfferBOImpl(dbHandle);
    OfferProcessCandidate offerProcessCandidate = candidateOfferBO.
        getOfferDetails(candidateId);

    if (!"success".equals(new Mail().offerLetter(subjectName, candidateR.getEmail(), candidateR.getFirstName(),
        offerProcessCandidate, organizationName,
        organization.getHris_url(), Vertx.vertx(), candidateR.getUserId(), candidateId))) {
      return CandidateOfferEnum.SEND_FAILED;
    }
    return CandidateOfferEnum.SUCCESS;
  }

  /**
   * @param candidateId
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author sadhanaVenkatesan
   */
  @Override
  public List<HashMap<String, String>> getOfferMailDetails(long candidateId)
      throws SQLException, DbHandleException {

    CandidateOfferDAO mailDetailsDAO = new CandidateOfferDAOImpl(dbHandle);
    List<HashMap<String, String>> mailDetails = mailDetailsDAO.getOfferMailList(candidateId);
    return mailDetails;
  }

  /*@Override
  public String getStatus(long candidateId, String status)
      throws SQLException, DbHandleException {
    CandidateOfferDAO offerDAO = new CandidateOfferDAOImpl(dbHandle);
    String candidateProcess = offerDAO.getStatus(candidateId,status);
    return candidateProcess;
  }*/
}
