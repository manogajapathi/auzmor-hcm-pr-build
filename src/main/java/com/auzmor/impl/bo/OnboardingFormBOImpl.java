package com.auzmor.impl.bo;

import com.auzmor.bo.OnboardingFormBO;
import com.auzmor.dao.OnboardingFormDAO;
import com.auzmor.impl.dao.OnboardingFormDAOImpl;
import com.auzmor.impl.enumarator.bo.OnboardingFormEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.onboarding.OnboardingForm;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;

public class OnboardingFormBOImpl implements OnboardingFormBO{
  private DbHandle dbHandle;
  private OnboardingFormDAO onboardingFormDAO;
  public OnboardingFormBOImpl(DbHandle dbHandle){this.dbHandle = dbHandle;}

  @Override
  public ReturnObject<OnboardingFormEnum, JsonObject> addForm(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    JsonObject dataObject= new JsonObject();
    OnboardingFormDAO onboardingFormDAO = new OnboardingFormDAOImpl(dbHandle);
    OnboardingForm onboardingForm =  new OnboardingForm();
    onboardingForm.setForm_name(params.get("form_name"));
    onboardingForm.setPacket_id(Integer.valueOf(params.get("packet_id")));
    onboardingForm.setForm_required(true);

    boolean AlreadyExists = onboardingFormDAO.isAlreadyExists(onboardingForm);
    if (AlreadyExists) {
      return new ReturnObjectImpl<OnboardingFormEnum, JsonObject>(OnboardingFormEnum.ALREADY_EXISTS, dataObject);
    }
    
    int insertRows = onboardingFormDAO.addForm(onboardingForm);
    if (insertRows > 0) {
      dataObject.put("message", "Form added successfully");
      return new ReturnObjectImpl<OnboardingFormEnum, JsonObject>
          (OnboardingFormEnum.ADD_SUCCESS, dataObject);
    } else {
      dataObject.put("message", "Form could not be added");
      return new ReturnObjectImpl<OnboardingFormEnum, JsonObject>
          (OnboardingFormEnum.ADD_FAILURE, dataObject);
    }
  }

  @Override
  public ReturnObject<OnboardingFormEnum, JsonObject> deleteForm(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    JsonObject dataObject = new JsonObject();
    OnboardingFormDAO onboardingFormDAO = new OnboardingFormDAOImpl(dbHandle);
    OnboardingForm onboardingForm = new OnboardingForm();
    onboardingForm.setForm_id(Integer.valueOf(params.get("formId")));

    int deleteRows = onboardingFormDAO.deleteForm(onboardingForm);
    if (deleteRows > 0) {
      dataObject.put("message", "Form deleted successfully");
      return new ReturnObjectImpl<OnboardingFormEnum, JsonObject>
          (OnboardingFormEnum.DELETE_SUCCESS, dataObject);
    } else {
      dataObject.put("message", "Form does not exists");
      return new ReturnObjectImpl<OnboardingFormEnum, JsonObject>
          (OnboardingFormEnum.DELETE_FAILURE, dataObject);
    }
  }

  @Override
  public ReturnObject<OnboardingFormEnum, JsonObject> updateForm(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    JsonObject dataObject = new JsonObject();
    OnboardingFormDAO onboardingFormDAO = new OnboardingFormDAOImpl(dbHandle);
    int updateRows = onboardingFormDAO.updateForm(params);
    if (updateRows > 0) {
      dataObject.put("message", "Form updated successfully");
      return new ReturnObjectImpl<OnboardingFormEnum, JsonObject>
          (OnboardingFormEnum.UPDATE_SUCCESS, dataObject);
    } else {
      dataObject.put("message", "No new details for update");
      return new ReturnObjectImpl<OnboardingFormEnum, JsonObject>
          (OnboardingFormEnum.UPDATE_FAILURE, dataObject);
    }
  }

  @Override
  public ReturnObject<OnboardingFormEnum, JsonObject> getAuzmorForm(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    JsonObject dataObject = new JsonObject();
    OnboardingFormDAO onboardingFormDAO = new OnboardingFormDAOImpl(dbHandle);
    JsonArray jsonArray = new JsonArray();
    jsonArray = onboardingFormDAO.getAuzmorForms(params);

    if (jsonArray.size() > 0) {
      dataObject.put("auzmor_forms", jsonArray);
      return new ReturnObjectImpl<OnboardingFormEnum, JsonObject>
          (OnboardingFormEnum.VALUE_SUCCESS, dataObject);
    } else {
      dataObject.put("auzmor_forms", jsonArray);
      return new ReturnObjectImpl<OnboardingFormEnum, JsonObject>
          (OnboardingFormEnum.NO_SUCH_VALUE, dataObject);
    }

  }
  public OnboardingFormDAO getOnboardingFormDAO() {
    return onboardingFormDAO;
  }
  public void setOnboardingFormDAO(OnboardingFormDAO onboardingFormDAO) {
    this.onboardingFormDAO = onboardingFormDAO;
  }
}
