package com.auzmor.impl.bo;

import com.auzmor.bo.PTOCommentBO;
import com.auzmor.dao.EmployeeDAO;
import com.auzmor.dao.PTOCommentsDAO;
import com.auzmor.impl.dao.EmployeeDAOImpl;
import com.auzmor.impl.dao.PTOCommentsDAOImpl;
import com.auzmor.impl.enumarator.bo.EmployeePTOEnum;
import com.auzmor.impl.enumarator.bo.PTOCommentsEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.PTO.PTOComments;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class PTOCommentsBOImpl implements PTOCommentBO{

  private DbHandle dbHandle;
  private  PTOCommentsDAO ptoCommentsDAO;
  public PTOCommentsBOImpl (DbHandle dbHandle)
  {
    this.dbHandle = dbHandle;
  }

  @Override
  public ReturnObject<PTOCommentsEnum, JsonObject> addComment(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    PTOComments ptoComments = new PTOComments();
    ptoComments.setPtoId(Long.valueOf(params.get("pto_id")));
    ptoComments.setCreatorId(Long.valueOf(params.get("creator_id")));
    ptoComments.setCommentDescription(params.get("comment_description"));
    ptoComments.setCommentsFilePath(StringUtil.printValidString
        (params.get("pto_comments_file_path")));

    PTOCommentsDAO ptoCommentsDAO = new PTOCommentsDAOImpl(dbHandle);
    boolean alreadyExists = ptoCommentsDAO.alreadyExists(ptoComments);
    if (alreadyExists) {
      return new ReturnObjectImpl<PTOCommentsEnum, JsonObject>
          (PTOCommentsEnum.EXISTS_ALREADY, new JsonObject());
    }

    int insertRows = ptoCommentsDAO.addComment(ptoComments);
    if (insertRows > 0) {
      return new ReturnObjectImpl<PTOCommentsEnum, JsonObject>
          (PTOCommentsEnum.ADDITION_SUCCESS, new JsonObject());
    } else {
      return new ReturnObjectImpl<PTOCommentsEnum, JsonObject>
          (PTOCommentsEnum.ADDITION_FAILED, new JsonObject());
    }

  }

  @Override
  public ReturnObject<PTOCommentsEnum, JsonArray> getAllComments(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException, ParseException {
    PTOComments ptoComments = new PTOComments();
    JsonArray resultArray = new JsonArray();
    ptoComments.setPtoId(Long.valueOf(params.get("pto_id")));

    PTOCommentsDAO ptoCommentsDAO = new PTOCommentsDAOImpl(dbHandle);
    List<JsonObject> resultList = new ArrayList<>();
    resultList = ptoCommentsDAO.getAllComments(ptoComments);
    if (resultList.size() == 0) {
      return new ReturnObjectImpl<PTOCommentsEnum, JsonArray>
          (PTOCommentsEnum.NO_SUCH_VALUE, new JsonArray());
    }

    EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);

    for (JsonObject jsonObject : resultList) {
      JsonObject resultObject = new JsonObject();
      resultObject.put("id", jsonObject.getLong("id"));
      resultObject.put("pto_id", jsonObject.getLong("pto_id"));
      resultObject.put("comment_description", jsonObject.getString("comment_description"));
      resultObject.put("comments_file_path", jsonObject.getString("attachment_file"));

      long empId = jsonObject.getLong("creator_id");
      Employee employee = new Employee();
      employee = employeeDAO.getEmployeeDetails(empId);
      resultObject.put("commented_by", employee.getFirst_name() + " " + employee.getLast_name());
      resultObject.put("commented_by_id", empId);
      resultObject.put("profile_image", employee.getProfile_image());


      SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
      SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
      java.util.Date date = simpleDateFormat.parse(jsonObject.getString("created_date"));
      resultObject.put("created_on", simpleDateFormat1.format(date));
      
      resultArray.add(resultObject);
    }
    return new ReturnObjectImpl<PTOCommentsEnum, JsonArray>
        (PTOCommentsEnum.VALUE_SUCCESS, resultArray);
  }

  @Override
  public ReturnObject<PTOCommentsEnum, JsonObject> deleteComment(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    PTOComments ptoComments = new PTOComments();
    ptoComments.setId(Long.valueOf(params.get("comment_id")));
    ptoComments.setCreatorId(Long.valueOf(params.get("creator_id")));

    PTOComments ptoCommentsFromDb = new PTOComments();


    PTOCommentsDAO ptoCommentsDAO = new PTOCommentsDAOImpl(dbHandle);
    ptoCommentsFromDb = ptoCommentsDAO.getPTOComment(ptoComments);

    if (ptoCommentsFromDb == null) {
      return new ReturnObjectImpl<PTOCommentsEnum, JsonObject>
          (PTOCommentsEnum.NO_SUCH_VALUE, new JsonObject());
    }

    if (ptoCommentsFromDb.getCreatorId() != ptoComments.getCreatorId()) {
      return new ReturnObjectImpl<PTOCommentsEnum, JsonObject>
          (PTOCommentsEnum.NO_ACCESS, new JsonObject());
    }

    int resultRows = 0;
    resultRows = ptoCommentsDAO.deleteComment(ptoComments);

    if(resultRows > 0) {
      return new ReturnObjectImpl<PTOCommentsEnum, JsonObject>
          (PTOCommentsEnum.DELETE_SUCCESS, new JsonObject());
    } else {
      return new ReturnObjectImpl<PTOCommentsEnum, JsonObject>
          (PTOCommentsEnum.CANNOT_DELETE, new JsonObject());
    }
  }

  @Override
  public ReturnObject<PTOCommentsEnum, JsonObject> updateComment(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    PTOComments ptoComments = new PTOComments();
    ptoComments.setId(Long.valueOf(params.get("id")));
    ptoComments.setPtoId(Long.valueOf(params.get("pto_id")));
    ptoComments.setCreatorId(Long.valueOf(params.get("creator_id")));
    ptoComments.setCommentDescription(params.get("comment_description"));
    ptoComments.setCommentsFilePath(StringUtil.printValidString
        (params.get("pto_comments_file_path")));

    PTOCommentsDAO ptoCommentsDAO = new PTOCommentsDAOImpl(dbHandle);
    PTOComments ptoCommentsFromDb = new PTOComments();
    ptoCommentsFromDb = ptoCommentsDAO.getPTOComment(ptoComments);
    if (ptoCommentsFromDb.equals(null)) {
      return new ReturnObjectImpl<PTOCommentsEnum, JsonObject>
          (PTOCommentsEnum.NO_SUCH_VALUE, new JsonObject());
    }

    if (ptoCommentsFromDb.getCreatorId() != ptoComments.getCreatorId()
        && ptoCommentsFromDb.getPtoId() != ptoComments.getPtoId()) {
      return new ReturnObjectImpl<PTOCommentsEnum, JsonObject>
          (PTOCommentsEnum.NO_ACCESS, new JsonObject());
    }
    int updateRows = 0;
    updateRows = ptoCommentsDAO.updateComment(ptoComments);
    if(updateRows > 0) {
      return new ReturnObjectImpl<PTOCommentsEnum, JsonObject>
          (PTOCommentsEnum.UPDATE_SUCCESS, new JsonObject());
    } else {
      return new ReturnObjectImpl<PTOCommentsEnum, JsonObject>
          (PTOCommentsEnum.NO_UPDATE, new JsonObject());
    }
  }

  public PTOCommentsDAO getPtoCommentsDAO() {
    return ptoCommentsDAO;
  }
  public void setPtoCommentsDAO(PTOCommentsDAO ptoCommentsDAO) {
    this.ptoCommentsDAO= ptoCommentsDAO;
  }
}
