package com.auzmor.impl.bo;

import com.auzmor.bo.ReportBO;
import com.auzmor.dao.ReportDAO;
import com.auzmor.impl.dao.OrganizationDAOImpl;
import com.auzmor.impl.dao.ReportDAOImpl;
import com.auzmor.impl.enumarator.ReportEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.handler.ReportHandler;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.impl.util.db.DatabaseUtil;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class ReportBOImpl implements ReportBO {

  private static Logger logger = LoggerFactory.getLogger(ReportBOImpl.class);

  private DbHandle dbHandle;

  private ReportDAO reportDAO;

  public ReportDAO getReportDAO() {
    return reportDAO;
  }

  public void setReportDAO(ReportDAO reportDAO) {
    this.reportDAO= reportDAO;
  }

  public ReportBOImpl(DbHandle dbHandle) {
    this.dbHandle = dbHandle;
  }

  /**
   * getJobPostReports - Get Job Post Reports Business Object
   * @param jobModel
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Balasubramanian
   * @lastModifiedDate 07/MAR/2018
   */
  @Override
  public ReturnObject<ReportEnum, JsonObject> getJobPostReports(Job jobModel)
      throws SQLException, DbHandleException {

    ReportDAO reportDAO = new ReportDAOImpl(dbHandle);
    List<JsonObject> jsonObjectList = reportDAO.getJobReports(jobModel);
    logger.info("Job Reports displayed for organization id " + jobModel.getOrganization_id());
    ListIterator<JsonObject> jsonObjectListIterator = jsonObjectList.listIterator();
    JsonArray resultArray = new JsonArray();
    int totalJobs = 0;
    List<String> postJobStatusList = new ArrayList<>();
    ReportEnum reportEnum = ReportEnum.VALUE_SUCCESS;

    while(jsonObjectListIterator.hasNext()){
      resultArray.add(jsonObjectListIterator.next());
    }

    for(int i=0;i<resultArray.size();i++){
      postJobStatusList.add(resultArray.getJsonObject(i).getString("label"));
      totalJobs += Integer.parseInt(resultArray.getJsonObject(i).getString("count"));
    }

    for(int j=0;j<resultArray.size();j++){
      int count = Integer.parseInt(resultArray.getJsonObject(j).getString("count"));
      double share = new ReportHandler().getSourceShare(count,totalJobs);
      resultArray.getJsonObject(j).put("share",share);
    }

    boolean isOrganizationAvailable = false;
    Organization organization = new OrganizationDAOImpl(dbHandle)
        .getOrganizationById(jobModel.getOrganization_id());
    if(organization != null) {
      isOrganizationAvailable = true;
    }

    JsonObject dataObject = new JsonObject();
    if(resultArray.size()>0){
      reportEnum = ReportEnum.DATA_AVAILABLE;
    } else {
      reportEnum = ReportEnum.DATA_NOT_AVAILABLE;
    }

    boolean isPublishedJobAvailable = postJobStatusList.contains("Published");
    boolean isDraftJobAvailable = postJobStatusList.contains("Draft");
    boolean isUnpublishedJobAvailable = postJobStatusList.contains("Unpublished");
    boolean isClosedJobAvailable = postJobStatusList.contains("Closed");

    if(!isPublishedJobAvailable) {
      if(isOrganizationAvailable && reportEnum == ReportEnum.DATA_NOT_AVAILABLE) {
        resultArray.add(new JsonObject().put("label", "Published")
          .put("count", "1").put("share", 100));
        totalJobs = 1;
        reportEnum = ReportEnum.DATA_AVAILABLE;
      } else {
        resultArray.add(new JsonObject().put("label", "Published")
            .put("count", "0").put("share", 0));
      }
    }

    if(!isDraftJobAvailable) {
      resultArray.add(new JsonObject().put("label","Draft")
          .put("count","0").put("share",0));
    }

    if(!isUnpublishedJobAvailable) {
      resultArray.add(new JsonObject().put("label","Unpublished")
          .put("count","0").put("share",0));
    }

    if(!isClosedJobAvailable) {
      resultArray.add(new JsonObject().put("label","Closed")
          .put("count","0").put("share",0));
    }

    dataObject.put("job", String.valueOf(totalJobs));
    dataObject.put("reports", resultArray);

    return new ReturnObjectImpl<ReportEnum, JsonObject>(reportEnum, dataObject);
  }

  /**
   * getCandidateStatusReports - get Candidate Status Report Business Object
   * @param candidate
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Balasubramanian
   * @lastModifiedDate 07/MAR/2018
   */
  @Override
  public ReturnObject<ReportEnum, JsonArray> getCandidateStatusReports(CandidateR candidate)
      throws SQLException, DbHandleException {

    ReportDAO reportDAO = new ReportDAOImpl(dbHandle);
    List<JsonObject> jsonObjectList = reportDAO.getCandidateStatusReports(candidate);
    logger.info("Candidate Status reports displayed for organization id "
        + candidate.getOrganizationId());

    ListIterator<JsonObject> jsonObjectListIterator = jsonObjectList.listIterator();
    JsonArray resultArray = new JsonArray();
    List<String> statusList = new ArrayList<>();
    int totalItems = 0;
    ReportEnum reportEnum = ReportEnum.VALUE_SUCCESS;

    while(jsonObjectListIterator.hasNext()){
      resultArray.add(jsonObjectListIterator.next());
    }

    for(int i=0;i<resultArray.size();i++){
      String label = resultArray.getJsonObject(i).getString("label");
      statusList.add(label);
      totalItems += Integer.parseInt(resultArray.getJsonObject(i).getString("count"));
    }

    for(int j=0;j<resultArray.size();j++){
      int count = Integer.parseInt(resultArray.getJsonObject(j).getString("count"));
      double share = new ReportHandler().getSourceShare(count,totalItems);
      resultArray.getJsonObject(j).put("share",share);
    }

    boolean isOrganizationAvailable = false;
    Organization organization = new OrganizationDAOImpl(dbHandle)
        .getOrganizationById(candidate.getOrganizationId());
    if(organization != null) {
      isOrganizationAvailable = true;
    }

    JsonObject noDataIndicator = new JsonObject();
    if(resultArray.size()>0){
      reportEnum = ReportEnum.DATA_AVAILABLE;
    } else {
      reportEnum = ReportEnum.DATA_NOT_AVAILABLE;

    }

    boolean isApplicantAvailable = statusList.contains("Applicant");
    boolean isInterviewAvailable = statusList.contains("Interview");
    boolean isNotHiredAvailable = statusList.contains("Not Hired");
    boolean isOfferAvailable = statusList.contains("Offer");
    boolean isScreeningAvailable = statusList.contains("Screening");
    boolean isHiredAvailable = statusList.contains("Hire");


    if(!isHiredAvailable) {
      resultArray.add(new JsonObject().put("label", "Hire")
          .put("count", "0")
          .put("share", 0));
    }

    int jobCount = Integer.parseInt(new DatabaseUtil().
        getTablename("Job", "count(1)",
            "organization_id =" + candidate.getOrganizationId()));

    if(!isApplicantAvailable) {
      if(isOrganizationAvailable && reportEnum == ReportEnum.DATA_NOT_AVAILABLE && jobCount == 0) {
        resultArray.add(new JsonObject().put("label", "Applicant")
            .put("count", "1").put("share", 100));
        reportEnum = ReportEnum.DATA_AVAILABLE;
      } else {
        resultArray.add(new JsonObject().put("label", "Applicant")
            .put("count", "0")
            .put("share", 0));
      }
    }

    if(!isInterviewAvailable) {
      resultArray.add(new JsonObject().put("label","Interview")
          .put("count","0")
          .put("share",0));
    }

    if(!isNotHiredAvailable) {
      resultArray.add(new JsonObject().put("label","Not Hired")
          .put("count", "0")
          .put("share", 0));
    }

    if(!isOfferAvailable) {
      resultArray.add(new JsonObject().put("label","Offer")
          .put("count", "0")
          .put("share", 0));
    }

    if(!isScreeningAvailable) {
      resultArray.add(new JsonObject().put("label","Screening")
          .put("count", "0")
          .put("share", 0));
    }

    return new ReturnObjectImpl<>(reportEnum, resultArray);
  }
}
