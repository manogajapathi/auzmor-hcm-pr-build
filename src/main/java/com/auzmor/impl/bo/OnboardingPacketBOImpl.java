package com.auzmor.impl.bo;

import com.auzmor.bo.OnboardingPacketBO;
import com.auzmor.dao.*;
import com.auzmor.impl.dao.*;
import com.auzmor.impl.enumarator.bo.OnboardingPacketEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.EmailFormat;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.model.onboarding.OnboardingDocument;
import com.auzmor.impl.model.onboarding.OnboardingForm;
import com.auzmor.impl.model.onboarding.OnboardingPacket;
import com.auzmor.impl.model.onboarding.OnboardingTask;
import com.auzmor.impl.util.Mail;
import com.auzmor.impl.util.StringUtil;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OnboardingPacketBOImpl implements OnboardingPacketBO {
  /**
   * To add a packet with default and mandatory forms
   * @param dbHandle
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate Mar 8, 2018
   */
  @Override
  public ReturnObject<OnboardingPacketEnum, JsonObject> addPacket(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    JsonObject dataObject = new JsonObject();
    OnboardingPacket onboardingPacket = new OnboardingPacket();
    onboardingPacket.setPacket_name(params.get("packet_name"));
    onboardingPacket.setOrganization_id(Integer.valueOf(params.get("organization_id")));
    onboardingPacket.setCreator_id(Integer.valueOf(params.get("creator_id")));

    OnboardingPacketDAO onboardingPacketDAO = new OnboardingPacketDAOImpl(dbHandle);
    boolean alreadyExists = onboardingPacketDAO.isAlreadyExists(onboardingPacket);

    if (alreadyExists) {
      return new ReturnObjectImpl<OnboardingPacketEnum, JsonObject>
          (OnboardingPacketEnum.ALREADY_EXISTS, dataObject);
    }

    OnboardingWelcomeDAO onboardingWelcomeDAO = new OnboardingWelcomeDAOImpl(dbHandle);
    OnboardingPacket onboardingPacketPresent = onboardingWelcomeDAO.getWelcomeDetails(onboardingPacket);
    
    onboardingPacket.setWelcome_header(onboardingPacketPresent.getWelcome_header());
    onboardingPacket.setWelcome_description(onboardingPacketPresent.getWelcome_description());
    onboardingPacket.setWelcome_video(onboardingPacketPresent.getWelcome_video());
    onboardingPacket.setLatitude(onboardingPacketPresent.getLatitude());
    onboardingPacket.setLongitude(onboardingPacketPresent.getLongitude());
    
    int packetId = onboardingPacketDAO.addPacket(onboardingPacket);
    if (packetId > 0) {

      OnboardingFormDAO onboardingFormDAO = new OnboardingFormDAOImpl(dbHandle);
      OnboardingForm onboardingForm = new OnboardingForm();
      onboardingForm.setForm_name("Welcome Page");
      onboardingForm.setForm_required(true);
      onboardingForm.setPacket_id(packetId);
      int insertNext = onboardingFormDAO.addForm(onboardingForm);
      onboardingForm = new OnboardingForm();
      onboardingForm.setForm_name("Personal Information");
      onboardingForm.setForm_required(true);
      onboardingForm.setPacket_id(packetId);
      insertNext = onboardingFormDAO.addForm(onboardingForm);
      onboardingForm = new OnboardingForm();
      onboardingForm.setForm_name("Employment Information");
      onboardingForm.setForm_required(true);
      onboardingForm.setPacket_id(packetId);
      insertNext = onboardingFormDAO.addForm(onboardingForm);

      dataObject.put("id", packetId);
      dataObject.put("message", "Packet added successfully");
      return new ReturnObjectImpl<OnboardingPacketEnum, JsonObject>
          (OnboardingPacketEnum.ADD_SUCCESS, dataObject);
    } else {
      dataObject.put("message", "Failed to add packet due to internal server issues");
      return new ReturnObjectImpl<OnboardingPacketEnum, JsonObject>
          (OnboardingPacketEnum.ADD_FAILURE, dataObject);
    }
  }

  /**
   * To get packet details along with tasks,documents,forms
   * @param dbHandle
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   *
   * @author Pradeep Sudhakaran
   * @lastModifiedDate Mar 8, 2018
   */
  @Override
  public ReturnObject<OnboardingPacketEnum, JsonObject> getPacket(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    int organizationId = Integer.valueOf(params.get("organization_id"));
    int packetId = StringUtil.printValidString(params.get("packet_id")) == "" ?
        0 : Integer.valueOf(params.get("packet_id"));

    OnboardingPacket onboardingPacket = new OnboardingPacket();
    onboardingPacket.setOrganization_id(organizationId);
    onboardingPacket.setPacket_id(packetId);

    OnboardingPacketDAO onboardingPacketDAO = new OnboardingPacketDAOImpl(dbHandle);
    OnboardingFormDAO onboardingFormDAO = new OnboardingFormDAOImpl(dbHandle);
    OnboardingDocumentDAO onboardingDocumentDAO = new OnboardingDocumentDAOImpl(dbHandle);
    OnboardingTaskDAO onboardingTaskDAO = new OnboardingTaskDAOImpl(dbHandle);

    JsonArray packetArray = new JsonArray();
    JsonArray resultArray = new JsonArray();
    packetArray = onboardingPacketDAO.getPackets(onboardingPacket);

    for (int i =0 ; i < packetArray.size(); i++) {
      JsonObject dataObject = new JsonObject();
      dataObject = packetArray.getJsonObject(i);
      packetId = dataObject.getInteger("id");

      OnboardingForm onboardingForm = new OnboardingForm();
      onboardingForm.setPacket_id(packetId);
      List<String> availableForms = new ArrayList<>();
      availableForms = onboardingFormDAO.getAvailableForms(onboardingForm);
      JsonArray formArray = new JsonArray();

      for(String auzmorForm : availableForms) {
        onboardingForm.setForm_name(auzmorForm);
        JsonObject formObject = new JsonObject();
        formObject = onboardingFormDAO.getForm(onboardingForm);
        formArray.add(formObject);
      }
      dataObject.put("forms", formArray);

      OnboardingDocument onboardingDocument = new OnboardingDocument();
      onboardingDocument.setPacket_id(packetId);
      JsonArray documentArray = new JsonArray();
      documentArray = onboardingDocumentDAO.getDocuments(onboardingDocument);
      dataObject.put("documents", documentArray);

      OnboardingTask onboardingTask = new OnboardingTask();
      onboardingTask.setPacket_id(packetId);
      JsonArray taskArray = new JsonArray();
      taskArray = onboardingTaskDAO.getTasks(onboardingTask);
      dataObject.put("tasks", taskArray);
      resultArray.add(dataObject);
    }
    JsonObject responseObject = new JsonObject();
    responseObject.put("packets", resultArray);
    return new ReturnObjectImpl<OnboardingPacketEnum, JsonObject>
        (OnboardingPacketEnum.VALUE_SUCCESS, responseObject);
  }

  /**
   * Send remainder mail to onboarding employee
   * @param dbHandle
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 01/MAR/2018
   */
  @Override
  public ReturnObject<OnboardingPacketEnum, JsonObject> sendRemainderMail(DbHandle dbHandle,
                                                                   MultiMap params)
      throws SQLException, DbHandleException {
    EmailFormat emailFormat = new EmailFormat();
    JsonObject dataObject = new JsonObject();
    Mail sendMail = new Mail();
    String ccAddress = "";
    emailFormat.setFrom_address("no-reply@auzmor.com");
    if(params.get("to_address").contains(",")) {
      String str=params.get("to_address");
      ArrayList aList = new ArrayList(Arrays.asList(str.split(",")));
      for (int i = 0; i < aList.size(); i++) {
        emailFormat.setTo_address(aList.get(0).toString());
        ccAddress = aList.get(1).toString();
      }
    } else {
      emailFormat.setTo_address(params.get("to_address"));
      ccAddress = "";
    }
    emailFormat.setMail_description(params.get("mail_description"));
    emailFormat.setSubject_name(params.get("subject_name"));

    String message = sendMail.sendMailwithCc(Vertx.vertx(), emailFormat.getMail_description(),
        emailFormat.getFrom_address(), emailFormat.getTo_address(),
        ccAddress, emailFormat.getSubject_name());

    MailDetailsDAO mailDetailsDAO = new MailDetailsDAOImpl(dbHandle);
    int insertRows = mailDetailsDAO.insertRemainder(emailFormat, ccAddress);
    if (insertRows > 0) {
      dataObject.put("message", "Remainder sent!");
      return new ReturnObjectImpl<OnboardingPacketEnum, JsonObject>
          (OnboardingPacketEnum.REMAINDER_SENT, dataObject);
    } else {
      dataObject.put("message", "No such value");
      return new ReturnObjectImpl<OnboardingPacketEnum, JsonObject>
          (OnboardingPacketEnum.NO_SUCH_VALUE, dataObject);
    }
  }

  /**
   * Delete onboarding packet along with forms,tasks,documents present in packet
   * @param dbHandle
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 01/MAR/2018
   */
  @Override
  public ReturnObject<OnboardingPacketEnum, JsonObject> deletePacket(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    JsonObject dataObject = new JsonObject();
    OnboardingPacketDAO onboardingPacketDAO = new OnboardingPacketDAOImpl(dbHandle);
    int packetId = Integer.valueOf(params.get("packetId"));
    OnboardingPacket onboardingPacket = new OnboardingPacket();
    onboardingPacket.setPacket_id(packetId);

    int deleteRows = onboardingPacketDAO.deletePacket(onboardingPacket);
    if (deleteRows > 0) {
      OnboardingFormDAO onboardingFormDAO = new OnboardingFormDAOImpl(dbHandle);
      OnboardingForm onboardingForm = new OnboardingForm();
      onboardingForm.setPacket_id(packetId);
      deleteRows = onboardingFormDAO.deletePacket(onboardingForm);

      OnboardingTaskDAO onboardingTaskDAO = new OnboardingTaskDAOImpl(dbHandle);
      OnboardingTask onboardingTask = new OnboardingTask();
      onboardingTask.setPacket_id(packetId);
      deleteRows = onboardingTaskDAO.deletePacket(onboardingTask);

      OnboardingDocumentDAO onboardingDocumentDAO = new OnboardingDocumentDAOImpl(dbHandle);
      OnboardingDocument onboardingDocument = new OnboardingDocument();
      onboardingDocument.setPacket_id(packetId);
      deleteRows = onboardingDocumentDAO.deletePacket(onboardingDocument);

      dataObject.put("message", "Onboarding packet deleted");
      return new ReturnObjectImpl<OnboardingPacketEnum, JsonObject>
          (OnboardingPacketEnum.DELETE_SUCCESS, dataObject);
    } else {
      dataObject.put("message", "Packet does not exists");
      return new ReturnObjectImpl<OnboardingPacketEnum, JsonObject>
          (OnboardingPacketEnum.DELETE_FAILURE, dataObject);
    }
  }

  /**
   * Send packet to specific employee
   * @param dbHandle
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   * @author Pradeep Sudhakaran
   * @lastModifiedDate 01/MAR/2018
   */
  @Override
  public ReturnObject<OnboardingPacketEnum, JsonObject> sendPacket(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    JsonObject dataObject = new JsonObject();
    EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
    Employee employee = new Employee();
    employee = employeeDAO.getEmployeeDetails(Long.valueOf(params.get("employeeId")));

    String packetId = params.get("packet_name");
    if(employee.getOnboarding_packet().equals(packetId)) {
      return new ReturnObjectImpl<OnboardingPacketEnum, JsonObject>
          (OnboardingPacketEnum.PACKET_ALREADY_SENT, new JsonObject());
    }

    employee.setOnboarding_packet(packetId);
    int updateRows = employeeDAO.updateEmployeePacket(employee);
    if (updateRows > 0) {
      dataObject.put("message", "Packet sent to employee(s)");
      return new ReturnObjectImpl<OnboardingPacketEnum, JsonObject>
          (OnboardingPacketEnum.PACKET_SENT, dataObject);
    } else {
      dataObject.put("message", "Employee does not exists");
      return new ReturnObjectImpl<OnboardingPacketEnum, JsonObject>
          (OnboardingPacketEnum.NO_SUCH_VALUE, dataObject);
    }
  }

  /**
   * Update Packet details
   * @param dbHandle
   * @param params
   * @return
   * @throws SQLException
   * @throws DbHandleException
   */
  @Override
  public ReturnObject<OnboardingPacketEnum, JsonObject> updatePacket(DbHandle dbHandle, MultiMap params)
      throws SQLException, DbHandleException {
    JsonObject dataObject = new JsonObject();
    OnboardingPacketDAO onboardingPacketDAO = new OnboardingPacketDAOImpl(dbHandle);
    int updateRows = onboardingPacketDAO.updatePacket(params);
    if (updateRows > 0) {
      dataObject.put("message", "Packet details updated successfully");
      return new ReturnObjectImpl<OnboardingPacketEnum, JsonObject>
          (OnboardingPacketEnum.UPDATE_SUCCESS, dataObject);
    } else {
      dataObject.put("message", "No new details to update");
      return new ReturnObjectImpl<OnboardingPacketEnum, JsonObject>
          (OnboardingPacketEnum.UPDATE_FAILURE, dataObject);
    }
  }
}
