package com.auzmor.impl.elasticsearch;

import com.auzmor.impl.constant.db.ConnectionConstant;
import com.auzmor.impl.helper.ConfigurationJsonHelper;
import com.auzmor.impl.model.AnnouncementModel;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.util.ModelUtil;
import com.auzmor.impl.util.StringUtil;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.http.HttpHost;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ConnectException;
import java.sql.*;
import java.util.*;

/**
 * {@link IndexCreator} A class which helps to add index items using elastic search
 */
public class IndexCreator {

  public static void main(String...args) throws SQLException {

    Connection connection = null;
    try {
      RestHighLevelClient restClient = null;
      List<String> fields = new ArrayList<>();
      Map<String, String> queryParameters = null;
      ResultSet resultSet = null;

      io.vertx.core.json.JsonObject config = ConfigurationJsonHelper.getConf(
          "db.json", "db_secrets.json", "secrets.json");

      String connectionUrl = config.getString(ConnectionConstant.Environment.CONNECTION_URL);
      String driverClassName = (ConnectionConstant.Environment.DRIVER_CLASS_NAME);
      String dbUser = config.getString(ConnectionConstant.Environment.USER);
      String dbPassword = config.getString(ConnectionConstant.Environment.PASSWORD);

      Class.forName(driverClassName);

      connection = DriverManager.getConnection(connectionUrl, dbUser, dbPassword);
      RestClient restLowLevelClient = RestClient.builder(
          new HttpHost("localhost", 9200, "http")).build();

      if(restLowLevelClient == null) {
        throw new Exception("Elastic Search Rest Client Not Connected");
      }

      boolean doesAnnouncementIndicesExist = true;
      boolean doesOnboardingIndicesExist = true;
      boolean doesJobTasksIndicesExist = true;
      try {
        restLowLevelClient.performRequest("GET", "/announcement");
      } catch (IOException ioe){
        doesAnnouncementIndicesExist = false;
      }

      try {
        restLowLevelClient.performRequest("GET", "/onboarding");
      } catch (IOException ioe){
        doesOnboardingIndicesExist = false;
      }
      try {
        restLowLevelClient.performRequest("GET", "/jobtasks");
      } catch (IOException ioe){
        doesJobTasksIndicesExist = false;
      }

      restLowLevelClient.close();

      restClient = new RestHighLevelClient(RestClient.builder(
          new HttpHost("localhost", 9200, "http")));
      if(restClient == null) {
        throw new Exception("Elastic Search Rest Client Not Connected");
      }

      if(doesAnnouncementIndicesExist) {
        DeleteIndexRequest deleteAnnouncementRequest = new DeleteIndexRequest("announcement");
        DeleteIndexResponse deleteResponse = restClient.indices().deleteIndex(
            deleteAnnouncementRequest);
        if(deleteResponse.isAcknowledged()) {
          System.out.println("Announcement delete request is acknowledged");
        }
      }

      if(doesOnboardingIndicesExist) {
        DeleteIndexRequest deleteOnboardingRequest = new DeleteIndexRequest("onboarding");
        DeleteIndexResponse deleteResponse = restClient.indices().deleteIndex(deleteOnboardingRequest);
        if(deleteResponse.isAcknowledged()) {
          System.out.println("Onboarding delete request is acknowledged");
        }
      }

      if(doesJobTasksIndicesExist) {
        DeleteIndexRequest deleteJobTasksRequest = new DeleteIndexRequest("jobtasks");
        DeleteIndexResponse deleteResponse = restClient.indices().deleteIndex(deleteJobTasksRequest);
        if(deleteResponse.isAcknowledged()) {
          System.out.println("JobTasks delete request is acknowledged");
        }
      }

      fields = new ArrayList<>();
      fields.add("announcement_id");
      fields.add("announcement_title");
      fields.add("announcement_details");
      fields.add("start_date");
      fields.add("end_date");
      fields.add("audience");
      fields.add("specific_audience");
      fields.add("organization_id");

      StringBuilder announcementBuilder = new StringBuilder();
      announcementBuilder.append("select announcement_id, announcement_title, " +
          "announcement_details, ");
      announcementBuilder.append("start_date, end_date, audience, specific_audience," +
          " organization_id ");
      announcementBuilder.append("from Announcement where now() between ");
      announcementBuilder.append("str_to_date(start_date, ?) and str_to_date(end_date, ?)");

      PreparedStatement preparedStatement = connection.prepareStatement(announcementBuilder.toString());
      preparedStatement.setString(1, "%Y-%m-%d");
      preparedStatement.setString(2, "%Y-%m-%d");

      resultSet = preparedStatement.executeQuery();

      BulkRequest bulkRequest = new BulkRequest();
      while (resultSet.next()) {
        Map<String, Object> announcementMap = new HashMap<>();
        for (int i = 0; i < fields.size(); i++) {
          String fieldType = ModelUtil.getFieldType(fields.get(i), new AnnouncementModel());
          if ("boolean".equals(fieldType)) {
            announcementMap.put(fields.get(i), resultSet.getBoolean(fields.get(i)));
          } else if ("int".equals(fieldType)) {
            announcementMap.put(fields.get(i), resultSet.getInt(fields.get(i)));
          } else {
            announcementMap.put(fields.get(i),
                StringUtil.printValidString(resultSet.getString(fields.get(i))));
          }

        }

        String indexId = String.valueOf(announcementMap.get("announcement_id"));
        IndexRequest announcementIndexRequest = new
            IndexRequest("announcement", "feeds", indexId).source(announcementMap);

        bulkRequest.add(announcementIndexRequest);

      }

      resultSet.close();
      preparedStatement.close();

      fields = new ArrayList<>();
      fields.add("employee_id");
      fields.add("first_name");
      fields.add("last_name");
      fields.add("preferred_name");
      fields.add("job_title");
      fields.add("organization_id");
      fields.add("hire_date");

      StringBuilder employeeQueryBuilder = new StringBuilder();
      employeeQueryBuilder.append("select employee_id, first_name, last_name, preferred_name,");
      employeeQueryBuilder.append(" job_title, organization_id, hire_date from Employee");
      employeeQueryBuilder.append(" where str_to_date(hire_date,?) ");
      employeeQueryBuilder.append(" >= (now() - interval 10 day) ");

      PreparedStatement employeePrepareStatement = connection.
          prepareStatement(employeeQueryBuilder.toString());
      employeePrepareStatement.setString(1, "%m/%d/%Y");

      resultSet = employeePrepareStatement.executeQuery();

      while (resultSet.next()) {
        Map<String, Object> onboardingMap = new HashMap<>();
        for (int i = 0; i < fields.size(); i++) {
          String fieldType = ModelUtil.getFieldType(fields.get(i), new Employee());
          if ("boolean".equals(fieldType)) {
            onboardingMap.put(fields.get(i), resultSet.getBoolean(fields.get(i)));
          } else if ("int".equals(fieldType)) {
            onboardingMap.put(fields.get(i), resultSet.getInt(fields.get(i)));
          } else {
            onboardingMap.put(fields.get(i),
                StringUtil.printValidString(resultSet.getString(fields.get(i))));
          }
        }
        String indexId = String.valueOf(onboardingMap.get("employee_id"));
        IndexRequest onboardingIndexRequest = new IndexRequest("onboarding", "feeds", indexId)
            .source(onboardingMap);

        bulkRequest.add(onboardingIndexRequest);

      }

      resultSet.close();
      employeePrepareStatement.close();

      StringBuilder jobTasksBuilder = new StringBuilder();
      jobTasksBuilder.append("select jt.id, jt.employee_id, jt.task_name, jt.candidate_id,");
      jobTasksBuilder.append("jt.complete_within, j.organization_id, jt.due_date, jt.all_flag,");
      jobTasksBuilder.append(" jt.complete_flag, jt.job_id from Job_Tasks jt, Job j  ");
      jobTasksBuilder.append("where jt.created_date >= (now() - interval 10 day) and ");
      jobTasksBuilder.append(" jt.job_id = j.id ");

      PreparedStatement jobTasksPrepareStatement = connection.prepareStatement(
          jobTasksBuilder.toString());

      resultSet = jobTasksPrepareStatement.executeQuery();

      while (resultSet.next()){
        Map<String, Object> jobTasksMap = new HashMap<>();
        jobTasksMap.put("id", resultSet.getInt("jt.id"));
        jobTasksMap.put("employee_id", resultSet.getInt("jt.employee_id"));
        jobTasksMap.put("task_name", resultSet.getString("jt.task_name"));
        jobTasksMap.put("candidate_id", resultSet.getInt("jt.candidate_id"));
        jobTasksMap.put("complete_within", resultSet.getString("jt.complete_within"));
        jobTasksMap.put("due_date", resultSet.getString("jt.due_date"));
        jobTasksMap.put("all_flag", resultSet.getBoolean("jt.all_flag"));
        jobTasksMap.put("complete_flag", resultSet.getBoolean("jt.complete_flag"));
        jobTasksMap.put("job_id", resultSet.getInt("jt.job_id"));
        jobTasksMap.put("organization_id",resultSet.getInt("j.organization_id"));

        String indexId = String.valueOf(jobTasksMap.get("id"));
        IndexRequest jobTaskIndexRequest = new
            IndexRequest("jobtasks", "feeds", indexId).source(jobTasksMap);

        bulkRequest.add(jobTaskIndexRequest);

      }

      resultSet.close();
      jobTasksPrepareStatement.close();

      int totalRequest = bulkRequest.numberOfActions();

      System.out.println("Total Requests : " + totalRequest);

      if (totalRequest > 0) {
        BulkResponse response = restClient.bulk(bulkRequest);
        BulkItemResponse[] responses = response.getItems();
        for (int i = 0; i < responses.length; i++) {
          BulkItemResponse bulkItemResponse = responses[i];

          System.out.println(bulkItemResponse.status() + " " + bulkItemResponse.getIndex() + " " +
              bulkItemResponse.getType() + " " + bulkItemResponse.getOpType());
        }
      }

      restClient.close();

    } catch (Exception e) {
      e.printStackTrace();
      System.exit(1);
    } finally {
      System.exit(0);
      if (null != connection && !connection.isClosed()) {
        connection.close();
      }
    }

  }
}
