package com.auzmor.impl.configuration;

import com.auzmor.impl.constant.db.ConnectionConstant;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * {@link MysqldbConf} A Class which configures Database Connection using JDBC
 */

public class MysqldbConf {

  private static final Logger logger = LoggerFactory.getLogger(MysqldbConf.class);

  // Database Properties
  private String driverClassName;
  private String connectionUrl;
  private String dbUser;
  private String dbPwd;

  public MysqldbConf() {
    InputStream inputStream = null;
    //try {
      JsonObject vertxConfig = Vertx.currentContext().config();
      setDriverClassName(vertxConfig.getString(ConnectionConstant.Environment.DRIVER_CLASS_NAME));
      setConnectionUrl(vertxConfig.getString(ConnectionConstant.Environment.CONNECTION_URL));
      setDbUser(vertxConfig.getString(ConnectionConstant.Environment.USER));
      setDbPwd(vertxConfig.getString(ConnectionConstant.Environment.PASSWORD));

      /*Class.forName(getDriverClassName());
    } catch(ClassNotFoundException classNotFoundException){
      logger.error("Class Not Found Exception in Mysql DB Configuration ");
    }*/
  }

  public String getDriverClassName() {
    return driverClassName;
  }

  public void setDriverClassName(String driverClassName) {
    this.driverClassName = driverClassName;
  }

  public String getConnectionUrl() {
    return connectionUrl;
  }

  public void setConnectionUrl(String connectionUrl) {
    this.connectionUrl = connectionUrl;
  }

  public String getDbUser() {
    return dbUser;
  }

  public void setDbUser(String dbUser) {
    this.dbUser = dbUser;
  }

  public String getDbPwd() {
    return dbPwd;
  }

  public void setDbPwd(String dbPwd) {
    this.dbPwd = dbPwd;
  }

  /**
   * getConnection Returns the JDBC Connection Object based on Database Properties
   *
   * @return connection - The connection Object
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public Connection getConnection() throws SQLException, ClassNotFoundException {

    Connection connection = DriverManager.getConnection(connectionUrl, dbUser, dbPwd);
    return connection;
  }
}
