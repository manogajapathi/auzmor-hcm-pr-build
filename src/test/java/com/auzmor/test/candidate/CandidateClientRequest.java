package com.auzmor.test.candidate;

import com.auzmor.test.job.JobPostTest;

import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;

public class CandidateClientRequest {
    public static HttpClientRequest prepareRequestToDeleteCandidate(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.delete(port,"localhost","/candidate/1");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue(statusCode==200||statusCode==406||statusCode==400);
            context.assertTrue(httpClientResponse.headers().get("Content-Type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {
                System.out.println(body.toString());
                JsonObject jsonObject = new JsonObject(body.toString());
                if(statusCode==200)
                    context.assertEquals(jsonObject.getInteger("code"),200);
                async.complete();
            });
        });

        String body = JobPostTest.getFieldsforJobUpdate();
        clientRequest.putHeader("content-length",String.valueOf(body.length()));
        clientRequest.putHeader("content-type","application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();

        return clientRequest;
    }

    public static HttpClientRequest prepareRequestToDisplayCandidate(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.get(port,"localhost","/candidate?id=2");
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("code"),200);
                async.complete();
            });
        });
        clientRequest.end();
        return clientRequest;
    }

    public static HttpClientRequest prepareRequestToUpdateCandidateComments(HttpClient client, TestContext context, Integer port) {

        Async async = context.async();
        HttpClientRequest clientRequest = client.put(port,"localhost","/candidate/comments/deleteaction?id=37" +
                                                            "&candidate_id=1&admin_name=dani");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue(statusCode==202||statusCode==404);

            httpClientResponse.bodyHandler(body -> {


                System.out.println(body.toString());
                JsonObject jsonObject = new JsonObject(body.toString());
                if(statusCode==202) {
                    context.assertEquals(jsonObject.getInteger("code"), 202);
                } else {
                    JsonObject errorObject = jsonObject.getJsonObject("error");
                    boolean expectedErrorCode = errorObject.getInteger("code")==406;
                    context.assertTrue(expectedErrorCode);
                }
                async.complete();
            });
        });

        String body = CandidateApiTest.getFieldsforCandidateUpdate();

        clientRequest.putHeader("content-length",String.valueOf(body.length()));
        clientRequest.putHeader("content-type","application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();

        return clientRequest;
    }



    public static HttpClientRequest prepareRequestToUpdateCandidate(HttpClient client, TestContext context, Integer port) {

        Async async = context.async();
        HttpClientRequest clientRequest = client.put(port,"localhost","/candidate?rating=3" +
                "&candidate_id=1&admin_name=dani&candidate_name=sana");
        clientRequest.handler(httpClientResponse -> {
            context.assertEquals(httpClientResponse.statusCode(),201);
            context.assertTrue(httpClientResponse.headers().get("Content-Type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {

                System.out.println(body.toString());
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("status"),201);
                async.complete();
            });
        });

        String body = CandidateApiTest.getFieldsforCandidateUpdate();

        clientRequest.putHeader("content-length",String.valueOf(body.length()));
        clientRequest.putHeader("content-type","application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();

        return clientRequest;
    }

}
