package com.auzmor.test.candidate;

/*
* @author sadhana Venkatesan
* @created on 28-10-2017
* */

public class CandidateApiTest {

    //candidate post method
    public static String getRequiredFieldsForCandidatePost() {
        return "{" +
                "first_name:'sadhana'," +
                "'last_name':'venkatesan'" +
                "'email':'sadhana@auzmor.com'"+    // need to cehck the correct email address
                "'phone_number':'8668020332'"+
                "'address_line':'Anna Nagar,Chennai'"+
                "'city':'Chennai'"+
                "'zip':'600113'"+
                "'job_status':'Open'"+
                "'country':'India'"+
                "'state':'Tamil Nadu'"+
                "'job_id':'33'"+
                "'job_title':'Developer'"+
                "'hiring_lead':'Asif'"+
                "'employment_type':'Full-Time'"+
                "'location':'Chennai'"+
                "'department':'IT'"+                                // check these mandatory or not
                "'minimum_experience':'3+ years of experience'"+   //  check these mandatory or not
                "'internal_jobcode' : 'Testing'"+
                "}";
    }

    // candidate get method
    public static String getIdForCandidate(int id) {
        return "{" +
                "'candidate_id' : '"+id+"'"+
                "}";
    }

    // candidate put method

    public static String getFieldsforCandidateUpdate() {
        return "{" +

                "'location':'Arakkonam'" +
                "'address_line' : 'Jothi Nagar,Arakkonam'" +
                "}";
    }

}
