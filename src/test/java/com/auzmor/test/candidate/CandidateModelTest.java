package com.auzmor.test.candidate;

import com.auzmor.impl.exception.util.ApplicationUtilException;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.impl.util.ModelUtil;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class CandidateModelTest {
    CandidateR candidateModel = new CandidateR();
    
    public void testCandidateModel(){
        try {
            ModelUtil.setValues("first_name", "Sadhana",candidateModel);
            ModelUtil.setValues("last_name","Venkatesan",candidateModel);
            ModelUtil.setValues("email","sadhana@auzmor.com",candidateModel);
            ModelUtil.setValues("phone_number","8668020332",candidateModel);
        } catch (ApplicationUtilException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertEquals("Sadhana",candidateModel.getFirstName());
        Assert.assertTrue("Venkatesan".equals(candidateModel.getLastName()));
    }

  /*  @Ignore
    public void testCandidateDeleted() throws Exception {
        int deletedEntrys = candidateModel.getOrganizationId();
        Assert.assertEquals(1,deletedEntrys);
    }*/
}
