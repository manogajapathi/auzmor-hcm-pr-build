package com.auzmor.test.job;


import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;

public class JobPostTest {


    public static String getRequiredFieldsForPostJob() {

        return "{" +
                "'title':'Java Developer'" +
                "'hiring_lead':'Asif'" +
                "'employment_type':'Permanent'" +
                "'location':'IIT MRP'" +
                "'city':'Chennai'" +
                "'zip':'600113'" +
                "'job_status':'Open'" +
                "'country':'India'" +
                "'state':'Tamil Nadu'" +
                "'job_description':'3+ years of experience'" +

                "}";
    }



    public static String getIdForJobDetails(int id) {
        return "{" +
                "'id' : '" + id + "'" +
                "}";
    }

    public static String getFieldsforJobUpdate() {
        return "{" +

                "'country':'TestC'" +
                "'college' : true" +
                "}";
    }


}
