package com.auzmor.test.job;

import com.auzmor.impl.exception.util.ApplicationUtilException;
import com.auzmor.impl.util.ModelUtil;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.handler.JobDisplayHandler;


import org.junit.Assert;
import org.junit.Test;

public class JobModelTest {
    
    public void testJobModel(){
        Job job = new Job();
        try {
            ModelUtil.setValues("title", "Java Developer",job);
            ModelUtil.setValues("hiring_lead","Java Developer",job);
            ModelUtil.setValues("internal_job_code","Java Developer",job);
        } catch (ApplicationUtilException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertEquals("Java Developer",job.getTitle());
        Assert.assertTrue("Java Developer".equals(job.getTitle()));
    }

    
    public void testJobDeleted() throws Exception {
        int deletedEntrys = new JobDisplayHandler().deleteJobPost(30);
        Assert.assertEquals(1,deletedEntrys);
    }
}

