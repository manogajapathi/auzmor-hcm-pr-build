package com.auzmor.test.job;

import io.vertx.core.Handler;
import io.vertx.core.MultiMap;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.*;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;

public class JobClientRequest {
    public static HttpClientRequest prepareRequestForJobBasedOnId(HttpClient client, TestContext context, int port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.get(port,"localhost","/job?id=4");
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                System.out.println("Response Body : " + body);
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("code"),200);
                async.complete();
            });
        });
        clientRequest.end();

        return clientRequest;

    }

    public static HttpClientRequest prepareRequestForJobBasedOnConstraints(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();
        HttpClientRequest clientRequest = client.get(port,"localhost",
                "/job?job_status=Published&fields=candidate_count,title");
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("code"),200);
                async.complete();
            });
        });
        clientRequest.end();

        return clientRequest;
    }

    public static HttpClientRequest prepareRequestToUpdateJob(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();
        HttpClientRequest clientRequest = client.put(port,"localhost","/job/87");
        clientRequest.handler(httpClientResponse -> {
            context.assertEquals(httpClientResponse.statusCode(),202);
            context.assertTrue(httpClientResponse.headers().get("Content-Type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {

                System.out.println(body.toString());
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("code"),202);
                async.complete();
            });
        });

        String body = JobPostTest.getFieldsforJobUpdate();

        clientRequest.putHeader("content-length",String.valueOf(body.length()));
        clientRequest.putHeader("content-type","application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();

        return clientRequest;

    }


    public static HttpClientRequest prepareRequestToDeleteJobPost(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.delete(port,"localhost","/job/78");
        clientRequest.handler(httpClientResponse -> {
            context.assertEquals(httpClientResponse.statusCode(),200);
            context.assertTrue(httpClientResponse.headers().get("Content-Type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {

                System.out.println(body.toString());
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("code"),200);
                async.complete();
            });
        });

        String body = JobPostTest.getFieldsforJobUpdate();

        clientRequest.putHeader("content-length",String.valueOf(body.length()));
        clientRequest.putHeader("content-type","application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();

        return clientRequest;

    }


    public static HttpClientRequest prepareRequestToJobDescription(HttpClient client, TestContext context, int port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.get(port,"localhost","/jobdesc");
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                System.out.println("Response Body : " + body);
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getString("status"),"success");
                async.complete();
            });
        });
        clientRequest.end();

        return clientRequest;

    }


    public static HttpClientRequest prepareRequestToJobDescriptionDetails(HttpClient client, TestContext context, int port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.get(port,"localhost","/jobdesc/4");
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                System.out.println("Response Body : " + body);
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("code"),200);
                async.complete();
            });
        });
        clientRequest.end();

        return clientRequest;

    }

    public static HttpClientRequest prepareRequestForJobPost(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();
        HttpClientRequest clientRequest =client.post(port,"localhost","/job/create");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue((statusCode==201)||(statusCode==406));
            context.assertTrue(httpClientResponse.headers().get("Content-type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());
                if(statusCode == 201) {
                    context.assertEquals(jsonObject.getInteger("code"), 201);
                } else {
                    JsonObject errorObject = jsonObject.getJsonObject("error");
                    context.assertTrue(errorObject.getInteger("code")==406);
                }
                async.complete();
            });
        });
        String body = JobPostTest.getRequiredFieldsForPostJob();
        clientRequest.putHeader("content-length", String.valueOf(body.length()));
        clientRequest.putHeader("content-type", "application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();
        return clientRequest;
    }



}
