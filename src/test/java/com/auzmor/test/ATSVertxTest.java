package com.auzmor.test;
import com.auzmor.AuzmorMain;
import com.auzmor.test.announcement.AnnouncementClientRequest;
import com.auzmor.test.candidate.CandidateApiTest;
import com.auzmor.test.candidate.CandidateClientRequest;
import com.auzmor.test.holiday.HolidayListClientRequest;
import com.auzmor.test.report.ReportClientRequest;
import com.auzmor.test.tasks.TaskClientRequest;
import com.auzmor.test.employee.EmployeeClientRequest;
import com.auzmor.test.job.JobClientRequest;
import com.auzmor.test.login.LoginClientRequest;

import com.auzmor.test.onboarding.OnboardingClientRequest;
import com.auzmor.test.organization.OrganizationClientRequest;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

import org.junit.*;
import org.junit.runner.RunWith;


import java.io.IOException;
import java.net.ServerSocket;


    //@RunWith(VertxUnitRunner.class)
    public class ATSVertxTest {

        /*private Vertx vertx;
        private Integer port;

        *//**
         * Before executing our test, let's deploy our verticle.
         * <p/>
         * This method instantiates a new Vertx and deploy the verticle. Then, it waits in the verticle has successfully
         * completed its start sequence (thanks to `context.asyncAssertSuccess`).
         *
         * @param context the test context.
         *//*
        @Before
        public void setUp(TestContext context) throws IOException {
            vertx = Vertx.vertx();

            // Let's configure the verticle to listen on the 'test' port (randomly picked).
            // We create deployment options and set the _configuration_ json object:
            ServerSocket socket = new ServerSocket(8000);
            port = socket.getLocalPort();
            socket.close();

            DeploymentOptions options = new DeploymentOptions()
                    .setConfig(new JsonObject().put("http.port", port)
                    );

            // We pass the options as the second parameter of the deployVerticle method.
            vertx.deployVerticle(AuzmorMain.class.getName(), options, context.asyncAssertSuccess());
        }


        *//**
         * This method, called after our test, just cleanup everything by closing the vert.x instance
         *
         * @param context the test context
         *//*
        @After
        public void tearDown(TestContext context) {
            vertx.close(context.asyncAssertSuccess());
        }

        *//**
         * Let's ensure that our application behaves correctly.
         *
         * @param context the test context
         *//*
        @Test
        public void testMyApplication(TestContext context) {
            // This test is asynchronous, so get an async handler to inform the test when we are done.
            final Async async = context.async();

            // We create a HTTP client and query our application. When we get the response we check it contains the 'Hello'
            // message. Then, we call the `complete` method on the async handler to declare this async (and here the test) done.
            // Notice that the assertions are made on the 'context' object and are not Junit assert. This ways it manage the
            // async aspect of the test the right way.
            vertx.createHttpClient().getNow(port, "localhost", "/", response -> {
                response.handler(body -> {
                    async.complete();
                });
            });
        }

        *//**
         * Test Cases for Job Related CRUD Operations
         * @param context
         *//*

        @Test
        public void checkJobUpdate(TestContext context){
            HttpClient client = vertx.createHttpClient();
            JobClientRequest.prepareRequestToUpdateJob(client,context,port);
        }

        @Test
        public void deleteJobPost(TestContext context){
            HttpClient client = vertx.createHttpClient();
            JobClientRequest.prepareRequestToDeleteJobPost(client,context,port);

        }

        @Test
        public void checkDisplayJobBasedOnId(TestContext context){

            HttpClient client = vertx.createHttpClient();
            JobClientRequest.prepareRequestForJobBasedOnId(client,context,port);
        }

        @Test
        public void checkDisplayJobBasedOnConstraints(TestContext context) {

            HttpClient client = vertx.createHttpClient();
            JobClientRequest.prepareRequestForJobBasedOnConstraints(client, context, port);
        }

        @Test
        public void checkJobPost(TestContext context){

            HttpClient client = vertx.createHttpClient();
            JobClientRequest.prepareRequestForJobPost(client,context,port);
        }

        *//**
         * Test Cases for Candidate Related CRUD Operations
         * @param context
         *//*

        @Test
        public void checkThatWeCanPostCandidate(TestContext context) {
            Async async = context.async();

            HttpClient client = vertx.createHttpClient();
            client.post(port,"localhost","/candidate/create");
            HttpClientRequest clientRequest = client.post(port,"localhost","/candidate/create");
            clientRequest.handler(httpClientResponse -> {
                int statusCode = httpClientResponse.statusCode();

                context.assertTrue(statusCode==201||statusCode==406);
                context.assertTrue(httpClientResponse.headers().get("Content-type").contains("application/json"));
                httpClientResponse.bodyHandler(body -> {
                    JsonObject jsonObject = new JsonObject(body.toString());
                    if(statusCode==201){
                        context.assertTrue(jsonObject.getInteger("code")==201);
                    } else {
                        JsonObject errorObject = jsonObject.getJsonObject("error");
                        context.assertTrue(errorObject.getInteger("code")==406);
                    }
//                    context.assertEquals(jsonObject.getInteger("code"),201);
                    async.complete();
                });
            });

            String body = "{" +
                    "'first_name':'sadhana'," +
                    "'last_name':'v'" +
                    "'email':'sadhana@auzmor.com'"+
                    "'phone_number':'8668020332'"+
                    "'job_id':'1'"+
                    "'employment_type':'Permanent'"+
                    "'job_status':'Open'"+
                    "'location':'IIT MRP'"+
                    "'city':'Chennai'"+
                    "'state':'Tamil Nadu'"+
                    "'zip':'600113'"+
                    "'country':'India'"+
                    "'job_title':'Java Developer'"+

                    "}";
            clientRequest.putHeader("content-length", String.valueOf(body.length()));
            clientRequest.putHeader("content-type", "application/x-www-form-urlencoded");
            clientRequest.write(body);
            clientRequest.end();
        }


        @Ignore@Test
        public void checkCandidateResumeUpload(TestContext context) {
            Async async = context.async();

            HttpClient client = vertx.createHttpClient();
            client.post(port,"localhost","/candidate/resume");
            HttpClientRequest clientRequest = client.post(port,"localhost","/candidate/resume");
            clientRequest.handler(httpClientResponse -> {
                context.assertEquals(httpClientResponse.statusCode(),201);
                httpClientResponse.bodyHandler(body -> {
                    JsonObject jsonObject = new JsonObject(body.toString());
                    //    context.assertEquals(jsonObject.getInteger("code"),201);
                    async.complete();
                });
            });
            String body = "{" +
                    "'resume':'/home/auzmor/Downloads/OfferLetter.pdf'" +
                    "}";
            clientRequest.putHeader("content-length", String.valueOf(body.length()));
            clientRequest.putHeader("content-type", "form-data");
            clientRequest.write(body);
            clientRequest.end();
        }

        @Ignore
        @Test
        public void checkCandidateCoverLetterUpload(TestContext context) {
            Async async = context.async();

            HttpClient client = vertx.createHttpClient();
            client.post(port,"localhost","/candidate/coverletter");
            HttpClientRequest clientRequest = client.post(port,"localhost","/candidate/coverletter");
            clientRequest.handler(httpClientResponse -> {
                context.assertEquals(httpClientResponse.statusCode(),201);
                context.assertTrue(httpClientResponse.headers().get("Content-type").contains("application/json"));
                httpClientResponse.bodyHandler(body -> {
                    JsonObject jsonObject = new JsonObject(body.toString());
                    //    context.assertEquals(jsonObject.getInteger("code"),201);
                    async.complete();
                });
            });
            String body = "{" +
                    "'cover_letter':'/home/auzmor/Downloads/OfferLetter.pdf'" +
                    "}";
            clientRequest.putHeader("content-length", String.valueOf(body.length()));
            clientRequest.putHeader("content-type", "form-data");
            clientRequest.write(body);
            clientRequest.end();
        }

        @Test
        public void checkThatHiringProcessCandidate(TestContext context) {

            HttpClient client = vertx.createHttpClient();

            Async async = context.async();
            client.post(port,"localhost","/candidate/hiringprocess/create");
            HttpClientRequest clientRequest = client.post(port,"localhost","/candidate/hiringprocess/create");
            clientRequest.handler(httpClientResponse -> {
                int statusCode = httpClientResponse.statusCode();
                context.assertTrue(statusCode==201||statusCode==406);
                context.assertTrue(httpClientResponse.headers().get("Content-type").contains("application/json"));
                httpClientResponse.bodyHandler(body -> {
                    JsonObject jsonObject = new JsonObject(body.toString());
                    if(statusCode==201){
                        context.assertTrue(jsonObject.getInteger("status")==201);
                    } else {
                        JsonObject errorObject = jsonObject.getJsonObject("error");
                        context.assertTrue(errorObject.getInteger("code")==406);
                    }
                    async.complete();
                });
            });

            String body = "{" +
                    "status:'applicant'," +
                    "'candidate_process':'New'," +
                    "'candidate_id':'1'" +
                    "}";
            clientRequest.putHeader("content-length", String.valueOf(body.length()));
            clientRequest.putHeader("content-type", "application/x-www-form-urlencoded");
            clientRequest.write(body);
            clientRequest.end();
        }


        @Test
        public void checkThatInterviewProcessCandidate(TestContext context) {
            Async async = context.async();

            HttpClient client = vertx.createHttpClient();
            client.post(port,"localhost","/candidate/interviewprocess/create");
            HttpClientRequest clientRequest = client.post(port,"localhost","/candidate/interviewprocess/create");
            clientRequest.handler(httpClientResponse -> {
                int statusCode = httpClientResponse.statusCode();
                context.assertTrue(statusCode==201||statusCode==406);
                context.assertTrue(httpClientResponse.headers().get("Content-type").contains("application/json"));
                httpClientResponse.bodyHandler(body -> {
                    JsonObject jsonObject = new JsonObject(body.toString());
               //    context.assertEquals(jsonObject.getInteger("code"),201);
                    async.complete();
                });
            });

            String body = "{" +
                    "'interview_date':'28-10-2017'," +
                    "'interview_time'':'15:45:03'," +
                    "'interview_duration':'35 minutes'," +
                    "'interview_type':'In-Person'," +
                    "'interview_location':'Chennai'," +
                    "'interview_mail_description':'Testing Dump Data'," +
                    "'candidate_id':'1'" +
                    "}";
            clientRequest.putHeader("content-length", String.valueOf(body.length()));
            clientRequest.putHeader("content-type", "application/x-www-form-urlencoded");
            clientRequest.write(body);
            clientRequest.end();
        }


        @Test
        public void checkThatOfferProcessCandidate(TestContext context) {
            Async async = context.async();

            HttpClient client = vertx.createHttpClient();
            client.post(port,"localhost","/candidate/offerprocess/create");
            HttpClientRequest clientRequest = client.post(port,"localhost","/candidate/offerprocess/create");
            clientRequest.handler(httpClientResponse -> {
                int statusCode = httpClientResponse.statusCode();
                context.assertTrue(statusCode==201||statusCode==406);
                context.assertTrue(httpClientResponse.headers().get("Content-type").contains("application/json"));
                httpClientResponse.bodyHandler(body -> {
                    JsonObject jsonObject = new JsonObject(body.toString());
                    if(statusCode==201){
                        context.assertTrue(jsonObject.getInteger("status")==201);
                    } else {
                        JsonObject errorObject = jsonObject.getJsonObject("error");
                        context.assertTrue(errorObject.getInteger("code")==406);
                    }
                    async.complete();
                });
            });
            String body = "{" +
                    "'offer_pay_rate':'15000'," +
                    "'offer_position'':'Developer'," +
                    "'offer_start_date':'23-10-2017'," +
                    "'offer_job_location':'Chennai'," +
                    "'offer_contract_details':'Chennai'," +
                    "'offer_attachment_name':'file-uploads/82f348c7-eced-4705-8a17-08a0a03a9564'," +
                    "'admin_name':'Dani'," +
                    "'candidate_id':'1'" +
                    "}";
            clientRequest.putHeader("content-length", String.valueOf(body.length()));
            clientRequest.putHeader("content-type", "application/x-www-form-urlencoded");
            clientRequest.write(body);
            clientRequest.end();
        }

        @Test
        public void checkThatCandidateComment(TestContext context) {
            Async async = context.async();
            HttpClient client = vertx.createHttpClient();
            client.post(port,"localhost","/candidate/comments/create");
            HttpClientRequest clientRequest = client.post(port,"localhost","/candidate/comments/create");
            clientRequest.handler(httpClientResponse -> {
                int statusCode = httpClientResponse.statusCode();
                context.assertTrue(statusCode==201||statusCode==406);
                context.assertTrue(httpClientResponse.headers().get("Content-type").contains("application/json"));
                httpClientResponse.bodyHandler(body -> {
                    JsonObject jsonObject = new JsonObject(body.toString());
                    if(statusCode==201){
                        context.assertTrue(jsonObject.getInteger("code")==201);
                    } else {
                        JsonObject errorObject = jsonObject.getJsonObject("error");
                        context.assertTrue(errorObject.getInteger("code")==406);
                    }

                    async.complete();
                });
            });
            String body = "{" +
                    "'comments':'checking current status'," +
                    "'admin_name'':'Dani'," +
                    "'candidate_id':'1'" +
                    "}";
            clientRequest.putHeader("content-length", String.valueOf(body.length()));
            clientRequest.putHeader("content-type", "application/x-www-form-urlencoded");
            clientRequest.write(body);
            clientRequest.end();
        }

        // put method
        @Test
        public void checkCandidateUpdateDetails(TestContext context){
            Async async = context.async();

            HttpClient client = vertx.createHttpClient();
            HttpClientRequest clientRequest = client.put(port,"localhost","/candidate/1");
            clientRequest.handler(httpClientResponse -> {
                int statusCode = httpClientResponse.statusCode();
                context.assertTrue(statusCode==202||statusCode==406);
                context.assertTrue(httpClientResponse.headers().get("Content-Type").contains("application/json"));
                httpClientResponse.bodyHandler(body -> {

                    System.out.println(body.toString());
                    JsonObject jsonObject = new JsonObject(body.toString());
                    if(statusCode==202){
                        context.assertTrue(jsonObject.getInteger("code")==202);
                    } else {
                        JsonObject errorObject = jsonObject.getJsonObject("error");
                        context.assertTrue(errorObject.getInteger("code")==406);
                    }
                    async.complete();
                });
            });

            String body = CandidateApiTest.getFieldsforCandidateUpdate();

            clientRequest.putHeader("content-length",String.valueOf(body.length()));
            clientRequest.putHeader("content-type","application/x-www-form-urlencoded");
            clientRequest.write(body);
            clientRequest.end();

        }

        @Test
        public void checkCandidateInterviewProcessUpdateDetails(TestContext context){
            Async async = context.async();

            HttpClient client = vertx.createHttpClient();
            HttpClientRequest clientRequest = client.put(port,"localhost","/candidate/interviewprocess/1");
            clientRequest.handler(httpClientResponse -> {
                int statusCode = httpClientResponse.statusCode();
                context.assertTrue(statusCode==202||statusCode==406);
                context.assertTrue(httpClientResponse.headers().get("Content-Type").contains("application/json"));
                httpClientResponse.bodyHandler(body -> {

                    System.out.println(body.toString());
                    JsonObject jsonObject = new JsonObject(body.toString());
                    if(statusCode==202)
                        context.assertEquals(jsonObject.getInteger("code"),202);
                    else {
                        JsonObject errorObject = jsonObject.getJsonObject("error");
                        context.assertTrue(errorObject.getInteger("code")==406);
                    }
                    async.complete();
                });
            });

            String body = CandidateApiTest.getFieldsforCandidateUpdate();

            clientRequest.putHeader("content-length",String.valueOf(body.length()));
            clientRequest.putHeader("content-type","application/x-www-form-urlencoded");
            clientRequest.write(body);
            clientRequest.end();
        }

        @Test
        public void checkCandidateOfferProcessUpdateDetails(TestContext context){

            HttpClient client = vertx.createHttpClient();
            Async async = context.async();

            HttpClientRequest clientRequest = client.put(port,"localhost","/candidate/offerprocess/1");
            clientRequest.handler(httpClientResponse -> {
                int statusCode = httpClientResponse.statusCode();
                context.assertTrue(statusCode==202||(statusCode==406));
                context.assertTrue(httpClientResponse.headers().get("Content-Type").contains("application/json"));
                httpClientResponse.bodyHandler(body -> {

                    System.out.println(body.toString());
                    JsonObject jsonObject = new JsonObject(body.toString());
                    if(statusCode==202){
                        context.assertEquals(jsonObject.getInteger("code"),202);
                    } else {
                        JsonObject errorObject = jsonObject.getJsonObject("error");
                        context.assertTrue(errorObject.getInteger("code")==406);
                    }

                    async.complete();
                });
            });

            String body = CandidateApiTest.getFieldsforCandidateUpdate();
            clientRequest.putHeader("content-length",String.valueOf(body.length()));
            clientRequest.putHeader("content-type","application/x-www-form-urlencoded");
            clientRequest.write(body);
            clientRequest.end();
        }


        @Ignore@Test
        public void checkCandidateCommentsUpdateDetails(TestContext context) {
            HttpClient client = vertx.createHttpClient();
            CandidateClientRequest.prepareRequestToUpdateCandidateComments(client, context, port);
        }

        @Test
        public void checkCandidateRatingUpdateDetails(TestContext context) {
            HttpClient client = vertx.createHttpClient();
            CandidateClientRequest.prepareRequestToUpdateCandidate(client, context, port);
        }

        @Test
        public void checkDisplayCandidateDetails(TestContext context){
            HttpClient client = vertx.createHttpClient();
            CandidateClientRequest.prepareRequestToDisplayCandidate(client,context,port);

        }

        @Ignore@Test
        public void checkDeleteCandidate(TestContext context){
            HttpClient client = vertx.createHttpClient();
            CandidateClientRequest.prepareRequestToDeleteCandidate(client,context,port);
        }

        *//**
         * Test Cases for Employee Related CRUD Operations
         * @param context
         *//*

        @Test
        public void checkThatWeCanPostEmployee(TestContext context) {

            HttpClient client = vertx.createHttpClient();
            EmployeeClientRequest.prepareRequestToPostEmployeeDetails(client,context,port);
        }

        @Test
        public void displayEmployeeDetailsBasedOnId(TestContext context){
            HttpClient client = vertx.createHttpClient();
            EmployeeClientRequest.prepareRequestforEmployeeDetailsBasedOnId(client,context,port);
        }

        @Test
        public void checkDeleteEmployee(TestContext context){
            HttpClient client = vertx.createHttpClient();
            EmployeeClientRequest.prepareRequestToDeleteEmployeeDetails(client,context,port);
        }

        @Test
        public void checkUpdateEmployeeDetails(TestContext context){
            HttpClient client = vertx.createHttpClient();
            EmployeeClientRequest.prepareRequestToUpdateEmployeeDetails(client,context,port);
        }

        *//**
         * Test cases for My Team
         * @param context
         *//*
        @Test
        public void checkWeCanGetMyTeamEmployee(TestContext context){
            HttpClient client = vertx.createHttpClient();
            EmployeeClientRequest.prepareRequestforTeamOfEmployees(client,context,port);
        }

        *//**
         * Test cases for organization chart
         * @param context
         *//*
        @Test
        public void checkCanWeGetOrganizationChart(TestContext context){
            HttpClient client = vertx.createHttpClient();
            EmployeeClientRequest.prepareRequestforOrganizationChart(client,context,port);
        }
        *//**
         * Test Cases for Job Description Reports
         * @param context
         *//*

        @Test
        public void displayJobDescTypeReport(TestContext context){
            HttpClient client = vertx.createHttpClient();
            JobClientRequest.prepareRequestToJobDescription(client,context,port);
        }

        @Test
        public void displayJobDescDetailsReport(TestContext context){
            HttpClient client = vertx.createHttpClient();
            JobClientRequest.prepareRequestToJobDescriptionDetails(client,context,port);
        }

        *//**
         * Test Cases for Reports
         * @param context
         *//*
        @Test
        public void displayJobPostingDetailsReport(TestContext context){
            HttpClient client = vertx.createHttpClient();
            ReportClientRequest.prepareRequestToJobPostingReport(client,context,port);
        }

        @Test
        public void displayJobPostingReport(TestContext context){
            HttpClient client = vertx.createHttpClient();
            ReportClientRequest.prepareRequestToJobPostingStatusReport(client,context,port);
        }

        @Test
        public void displayCurrentStatusReport(TestContext context){
            HttpClient client = vertx.createHttpClient();
            ReportClientRequest.prepareRequestToCurrentStatusReport(client,context,port);
        }

        @Test
        public void displayAverageStatusReport(TestContext context){
            HttpClient client = vertx.createHttpClient();
            ReportClientRequest.prepareRequestToAverageStatusReport(client,context,port);
        }

        @Test
        public void displayApplicantStatusReport(TestContext context){
            HttpClient client = vertx.createHttpClient();
            ReportClientRequest.prepareRequestToApplicantStatusReport(client,context,port);
        }

        *//**
         * Tests based on Source Efficiency Report
         * @param context
         *//*

        @Test
        public void displaySourceEfficiencyReport(TestContext context){
            HttpClient client = vertx.createHttpClient();
            ReportClientRequest.prepareRequestToDisplaySourceEfficiency(client,context,port);
        }

        @Test
        public void displaySourceEfficiencyReportBasedOnJobPost(TestContext context){
            HttpClient client = vertx.createHttpClient();
            ReportClientRequest.requestToDisplaySourceEfficiencyBasedOnJobPost(client,context,port);
        }

        @Test
        public void displaySourceEfficiencyReportBasedOnAllConstraints(TestContext context){
            HttpClient client = vertx.createHttpClient();
            ReportClientRequest.requestToDisplaySourceEfficiencyWithAllConstraints(client,context,port);
        }

        @Test
        public void checkUpdateSources(TestContext context){
            HttpClient client = vertx.createHttpClient();
            ReportClientRequest.requestToCheckUpdateSources(client,context,port);
        }


        *//**
         * checkOrganizationStatus  - Unit Test to see whether valid organization is selected
         * @param context
         *//*
        @Test
        public void checkOrganizationStatus(TestContext context){
            OrganizationClientRequest.requestToCheckOrganizationStatus(vertx.createHttpClient(),context,port);
        }

        *//**
         *  checkUserSignup
         *//*

        @Test
        public void checkUserSignup(TestContext context){
            LoginClientRequest.requestToCheckUserSignup(vertx.createHttpClient(),context,port);
        }

        *//**
         * Test case for Create Announcement
         * @param context
         *//*
        @Test
        public void checkWeCanCreateAnnouncement(TestContext context){
            AnnouncementClientRequest.prepareRequestToPostAnnouncementDetails(vertx.createHttpClient(),context,port);
        }

        *//**
         * Test case for Edit Announcement
         * @param context
         *//*
        @Test
        public void checkWeCanEditAnnouncement(TestContext context){
            AnnouncementClientRequest.prepareRequestToUpdateAnnouncementDetails(vertx.createHttpClient(),context,port);
        }

        *//**
         * Test case for delete Announcement
         * @param context
         *//*
        @Test
        public void checkWeCanDeleteAnnouncement(TestContext context){
            AnnouncementClientRequest.prepareRequestToDeleteAnnouncementDetails(vertx.createHttpClient(),context,port);
        }

        *//**
         * Test case for details of announcement
         * @param context
         *//*
        @Test
        public void checkWeCanGetAnAnnouncement(TestContext context){
            AnnouncementClientRequest.prepareRequestforAnnouncementsBasedOnId(vertx.createHttpClient(),context,port);
        }

        *//**
         * Test case to check Onboarding Packet addition
         * @param context
         *//*
        @Test
        public void checkAddPacket(TestContext context){
            OnboardingClientRequest.prepareRequestToCheckAddPacket(vertx.createHttpClient(),context,port);
        }

        *//**
         * Test case to check Form addition in Onboarding Packet
         * @param context
         *//*
        @Test
        public void checkAddFormInOnboardingPacket(TestContext context){
            OnboardingClientRequest.prepareRequestToCheckAddForm(vertx.createHttpClient(),context,port);
        }

        *//**
         * Test Case to check Document addition in Onboarding Packet
         * @param context
         *//*
        @Test
        public void checkAddDocumentInOnboardingPacket(TestContext context){
            OnboardingClientRequest.prepareRequestToCheckAddDocument(vertx.createHttpClient(),context,port);
        }

        *//**
         * Test Case to check addition of task in Onboarding Packet
         * @param context
         *//*
        @Test
        public void checkAddTaskInOnboardingPacket(TestContext context){
            OnboardingClientRequest.prepareRequestToCheckAddTask(vertx.createHttpClient(),context,port);
        }

        *//**
         * Test Case to check display of onboarding Packet based on organization
         * @param context
         *//*
        @Test
        public void displayOnboardingPackets(TestContext context){
            OnboardingClientRequest.displayOnboardingPacket(vertx.createHttpClient(),context,port);
        }

        *//**
         * Test case to check display of Auzmor Forms in an onboarding packet
         * @param context the Test Context
         *//*
        @Test
        public void displayAuzmorFormsInOnboardingPacket(TestContext context){
            OnboardingClientRequest.displayAuzmorForms(vertx.createHttpClient(),context,port);
        }

        *//**
         * Test case to check display of Auzmor Documents in an onboarding packet
         *//*
        @Test
        public void displayAuzmorDocumentsInOnboardingPacket(TestContext context){
            OnboardingClientRequest.displayAuzmorDocuments(vertx.createHttpClient(),context,port);
        }

        *//**
         * Test case to check display of Inhouse in an onboarding packet
         * @param context the Test Context
         *//*
        @Test
        public void displayInhouseDocumentsInOnboardingPacket(TestContext context){
            OnboardingClientRequest.displayInhouseDocuments(vertx.createHttpClient(),context,port);
        }

        *//**
         * Test case to delete onboarding packet
         * @param context
         *//*
        @Test
        public void deleteOnboardingPacket(TestContext context){
            OnboardingClientRequest.prepareRequestToDeleteOnboardingPacket(vertx.createHttpClient(),context,port);
        }

        *//**
         * Test case to delete Onboarding Form in an onboarding Packet
         *//*
        @Test
        public void deleteFormInOnboardingPacket(TestContext context){
            OnboardingClientRequest.prepareRequestToDeleteOnboardingForm(vertx.createHttpClient(),context,port);
        }


        *//**
         * Test Case to delete onboarding task in an onboarding packet
         * @param context
         *//*
        @Test
        public void deleteTaskInOnboardingPacket(TestContext context){
            OnboardingClientRequest.prepareRequestToDeleteOnboardingTask(vertx.createHttpClient(),context,port);
        }

        *//**
         * Test case to delete Onboarding document in an onboarding packet
         * @param context
         *//*
        @Test
        public void deleteDocumentInOnboardingPacket(TestContext context){
            OnboardingClientRequest.prepareRequestToDeleteOnboardingDocument(vertx.createHttpClient(),context,port);
        }

        *//**
         * Test case to check update task
         * @param context
         *//*
        @Test
        public void checkUpdateTaskDetails(TestContext context){
            TaskClientRequest.prepareRequestToUpdateTaskDetails(vertx.createHttpClient(),context,port);

        }

        *//**
         * Test case to check post task
         * @param context
         *//*
        @Test
        public void checkPostTaskDetails(TestContext context){
            TaskClientRequest.prepareRequestToPostTaskDetails(vertx.createHttpClient(),context,port);
        }

        *//**
         * Test case to display task based on constraint
         * @param context
         *//*
        @Test
        public void checkDisplayTaskBasedOnConstraints(TestContext context){
            TaskClientRequest.prepareRequestForTaskBasedOnConstraints(vertx.createHttpClient(),context,port);
        }

        *//**
         * Test case to display task based on Task
         * @param context
         *//*
        @Test
        public void checkDisplayTaskBasedOnId(TestContext context){
            TaskClientRequest.prepareRequestForTaskBasedOnId(vertx.createHttpClient(),context,port);
        }

        *//**
         * Test case to check post holiday details
         * @param context
         *//*
        @Test
        public void prepareRequestToPostHoliday(TestContext context){
            HolidayListClientRequest.prepareRequestToPostHolidayList(vertx.createHttpClient(),context,port);
        }

        *//**
         * Test case to check delete holiday list
         * @param context
         *//*
        @Test
        public void prepareRequestToDeleteHoliday(TestContext context){
            HolidayListClientRequest.prepareRequestToDeleteHolidayList(vertx.createHttpClient(),context,port);
        }

        *//**
         * Test case to check display holiday list
         * @param context
         *//*
        @Test
        public void prepareRequestToCheckDisplayHolidays(TestContext context){
            HolidayListClientRequest.prepareRequestForAllHolidayList(vertx.createHttpClient(),context,port);
        }


        *//**
         * Test case to check display holiday details
         * @param context
         *//*
        @Test
        public void prepareRequestToCheckDisplayHolidayDetails(TestContext context){
            HolidayListClientRequest.prepareRequestForHolidayList(vertx.createHttpClient(),context,port);
        }*/

    }


