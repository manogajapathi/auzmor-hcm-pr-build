package com.auzmor.test.organization;

import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;

public class OrganizationClientRequest {
    public static HttpClientRequest requestToCheckOrganizationStatus(HttpClient httpClient, TestContext context, Integer port) {
        Async async = context.async();
        HttpClientRequest clientRequest = httpClient.get(port,"localhost",
                "/organization/check?organization_name=auzmor");
        clientRequest.handler(httpClientResponse -> {
            int responseCode = httpClientResponse.statusCode();
            context.assertTrue(responseCode==200||responseCode==400);
            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());
                if(responseCode==200) {
                    context.assertEquals(jsonObject.getInteger("code"), 200);
                } else {
                    JsonObject errorObject = jsonObject.getJsonObject("error");
                    context.assertTrue(errorObject.getInteger("code")==400);
                }
                async.complete();
            });
        });
        clientRequest.end();

        return clientRequest;
    }
    // sadhana created these on 18/11/2017
    public static HttpClientRequest prepareRequestToPostOrganizationDetails(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();
        client.post(port,"localhost","/organization/create");
        HttpClientRequest clientRequest = client.post(port,"localhost","/organization/create");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue(statusCode==406||statusCode==201);
            context.assertTrue(httpClientResponse.headers().get("Content-type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());
                async.complete();
            });
        });

        String body = "{" +
                "'domain_name':'auzmor'" +
                "'hris_url':'ats.auzmor.com'" +
                "'organization_name':'Aumor Technology'"+
                "'file_size':'150 mb'"+
                "'account_owner_name':'Darrly'"+
                "'account_owner_email':'asif@auzmor.com'"+
                "'ein':'142356955'"+
                "'phone_no':'8668202252'"+
                "'company_address_line1':'Anna Nagar'"+
                "'city':'Chennai'"+
                "'state':'Tamil Nadu'"+
                "'zip':'600113'"+
                "'country':'India'"+
                "'billing_credit_card':'1444444444444444'"+
                "'billing_expiry_date' : '2017/09'"+
                "'billing_cvv':'252'"+
                "'billing_contact_person1':'Asif'"+
                "'billing_city':'Chennai'"+
                "'billing_state':'Tamil Nadu'"+
                "'billing_zip':'600113'"+
                "'billing_country':'India'"+
                "}";
        clientRequest.putHeader("content-length", String.valueOf(body.length()));
        clientRequest.putHeader("content-type", "application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();
        return clientRequest;
    }

// update the organization done by sadhana on 18/11/2017
    public static HttpClientRequest prepareRequestToUpdateOrganizationDetails(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();
        HttpClientRequest clientRequest = client.put(port,"localhost","/organization/87");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue((statusCode==202)||(statusCode==406));
            context.assertTrue(httpClientResponse.headers().get("Content-Type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {
                System.out.println(body.toString());
                JsonObject jsonObject = new JsonObject(body.toString());
                async.complete();
            });
        });
        String body = "{" +
                "'domain_name':'auzmor'" +
                "'phone_no' : '8668020332'" +
                "}";
        clientRequest.putHeader("content-length",String.valueOf(body.length()));
        clientRequest.putHeader("content-type","application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();
        return clientRequest;
    }

}
