package com.auzmor.test.holiday;

import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;

public class HolidayListClientRequest {

    /**
     * Get all holiday lists
     * @param client
     * @param context
     * @param port
     * @return
     */
    public static HttpClientRequest prepareRequestForAllHolidayList(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.get(port,"localhost","/holidayList/list?organization_id=1");
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                System.out.println(body);
                async.complete();
            });
        });
        clientRequest.end();
        return clientRequest;
    }

    /**
     * Get particular holiday List
     * @param client
     * @param context
     * @param port
     * @return
     */
    public static HttpClientRequest prepareRequestForHolidayList(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.get(port,"localhost","/holidayList?list_id=1");
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                System.out.println(body);
                async.complete();
            });
        });
        clientRequest.end();
        return clientRequest;
    }

    /**
     * Create a new Holiday List
     * @param client
     * @param context
     * @param port
     * @return
     */
    public static HttpClientRequest prepareRequestToPostHolidayList(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();
        client.post(port,"localhost","/holidayList/create");
        HttpClientRequest clientRequest = client.post(port,"localhost","/holidayList/create");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue(statusCode==406||statusCode==201);
            context.assertTrue(httpClientResponse.headers().get("Content-type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());

                async.complete();
            });
        });

        String body = "{" +
                "'list_name':'Holiday List'" +
                "'year':'2018'" +
                "'assign_to':'Specific Group or Employees'"+
                "'department':'Developers'"+
                "'division':'All Employees'"+
                "'pay_group':'ITIS'"+
                "'location':Chennai"+
                "'employees':'30,32'"+
                "'paid_flag':true"+
                "'organization_id':'1'"+

                "}";
        clientRequest.putHeader("content-length", String.valueOf(body.length()));
        clientRequest.putHeader("content-type", "application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();
        return clientRequest;
    }

    /**
     * delete a holiday list
     * @param client
     * @param context
     * @param port
     * @return
     */
    public static HttpClientRequest prepareRequestToDeleteHolidayList(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.delete(port,"localhost","/holidayList/1");
        clientRequest.handler(httpClientResponse -> {
            context.assertEquals(httpClientResponse.statusCode(),200);
            context.assertTrue(httpClientResponse.headers().get("Content-Type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {

                System.out.println(body.toString());
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("status"),200);
                context.assertTrue(jsonObject.getInteger("status")==200);
                async.complete();
            });
        });

        clientRequest.end();

        return clientRequest;

    }
}
