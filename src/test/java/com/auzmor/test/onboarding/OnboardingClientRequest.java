package com.auzmor.test.onboarding;

import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.apache.http.protocol.HTTP;

public class OnboardingClientRequest {

    public static HttpClientRequest prepareRequestToCheckAddPacket(HttpClient httpClient, TestContext context, Integer port) {
        Async async = context.async();
        HttpClientRequest clientRequest =httpClient.post(port,"localhost","/onboardingPacket/add");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue((statusCode==201)||(statusCode==406));
            context.assertTrue(httpClientResponse.headers().get("Content-type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());
                if(statusCode == 201) {
                    context.assertEquals(jsonObject.getInteger("code"), 201);
                } else {
                    JsonObject errorObject = jsonObject.getJsonObject("error");
                    context.assertTrue(errorObject.getInteger("code")==406);
                }
                async.complete();
            });
        });
        String body = "{" +
            "'packet_name':'Test Organization Onboarding'" +
            "'organization_id':'1'" +
            "'creator_id':'4'" +
            "}";
        clientRequest.putHeader("content-length", String.valueOf(body.length()));
        clientRequest.putHeader("content-type", "application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();
        return clientRequest;
    }

    public static HttpClientRequest prepareRequestToCheckAddForm(HttpClient httpClient, TestContext context, Integer port) {
        Async async = context.async();
        HttpClientRequest clientRequest =httpClient.post(port,"localhost","/onboardingPacket/form");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue((statusCode==201)||(statusCode==406));
            context.assertTrue(httpClientResponse.headers().get("Content-type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());
                if(statusCode == 201) {
                    context.assertEquals(jsonObject.getInteger("code"), 201);
                } else {
                    JsonObject errorObject = jsonObject.getJsonObject("error");
                    context.assertTrue(errorObject.getInteger("code")==406);
                }
                async.complete();
            });
        });
        String body = "{" +
            "'packet_id':'104'" +
            "'form_name':'Test Case Form'" +
            "'form_required':true" +
            "}";
        clientRequest.putHeader("content-length", String.valueOf(body.length()));
        clientRequest.putHeader("content-type", "application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();
        return clientRequest;
    }

    public static HttpClientRequest prepareRequestToCheckAddDocument(HttpClient httpClient, TestContext context, Integer port) {
        Async async = context.async();
        HttpClientRequest clientRequest =httpClient.post(port,"localhost","/onboardingPacket/document");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue((statusCode==201)||(statusCode==406));
            context.assertTrue(httpClientResponse.headers().get("Content-type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());
                if(statusCode == 201) {
                    context.assertEquals(jsonObject.getInteger("code"), 201);
                } else {
                    JsonObject errorObject = jsonObject.getJsonObject("error");
                    context.assertTrue(errorObject.getInteger("code")==406);
                }
                async.complete();
            });
        });
        String body = "{" +
            "'document_name':'Test Case Document'" +
            "'viewing_reqd':true" +
            "'document_path':'/path/test'" +
            "'packet_id':'104'" +
            "}";
        clientRequest.putHeader("content-length", String.valueOf(body.length()));
        clientRequest.putHeader("content-type", "application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();
        return clientRequest;
    }

    public static HttpClientRequest prepareRequestToCheckAddTask(HttpClient httpClient, TestContext context, Integer port) {
        Async async = context.async();
        HttpClientRequest clientRequest =httpClient.post(port,"localhost","/onboardingPacket/task");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue((statusCode==201)||(statusCode==406));
            context.assertTrue(httpClientResponse.headers().get("Content-type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());
                if(statusCode == 201) {
                    context.assertEquals(jsonObject.getInteger("code"), 201);
                } else {
                    JsonObject errorObject = jsonObject.getJsonObject("error");
                    context.assertTrue(errorObject.getInteger("code")==406);
                }
                async.complete();
            });
        });
        String body = "{" +
            "'task_name':'Test Case Task'" +
            "'task_description':'Test Case Task Description'"+
            "'viewing_required':true" +
            "'allow_doc_upload':true" +
            "'doc_upload_required':true" +
            "'packet_id':'104'" +
            "}";
        clientRequest.putHeader("content-length", String.valueOf(body.length()));
        clientRequest.putHeader("content-type", "application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();
        return clientRequest;
    }

    public static HttpClientRequest displayOnboardingPacket(HttpClient client,TestContext context,int port){
        Async async = context.async();

        HttpClientRequest clientRequest = client.get(port,"localhost","/onboardingPacket?organization_id=1");
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                System.out.println("Response Body : " + body);
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("code"),200);
                async.complete();
            });
        });
        clientRequest.end();

        return clientRequest;
    }

    public static HttpClientRequest displayAuzmorForms(HttpClient client,TestContext context,int port){
        Async async = context.async();

        HttpClientRequest clientRequest = client.get(port,"localhost","/onboardingPacket/forms/auzmor?packet_id=1");
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                System.out.println("Response Body : " + body);
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("code"),200);
                async.complete();
            });
        });
        clientRequest.end();

        return clientRequest;
    }

    public static HttpClientRequest displayAuzmorDocuments(HttpClient client,TestContext context,int port){
        Async async = context.async();

        HttpClientRequest clientRequest = client.get(port,"localhost","/onboardingPacket/documents/auzmor?packet_id=1");
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                System.out.println("Response Body : " + body);
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("code"),200);
                async.complete();
            });
        });
        clientRequest.end();

        return clientRequest;
    }

    public static HttpClientRequest displayInhouseDocuments(HttpClient client,TestContext context,int port){
        Async async = context.async();
        String requestPath = "/onboardingPacket/documents/in-house?packet_id=91&organization_id=1";
        HttpClientRequest clientRequest = client.get(port,"localhost",requestPath);
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                System.out.println("Response Body : " + body);
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("code"),200);
                async.complete();
            });
        });
        clientRequest.end();

        return clientRequest;
    }

    public static HttpClientRequest prepareRequestToDeleteOnboardingPacket(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.delete(port,"localhost","/onboardingPacket/100");
        clientRequest.handler(httpClientResponse -> {
            context.assertEquals(httpClientResponse.statusCode(),200);
            context.assertTrue(httpClientResponse.headers().get("Content-Type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {

                System.out.println(body.toString());
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("status"),200);
                context.assertTrue(jsonObject.getInteger("status")==200);
                async.complete();
            });
        });

        clientRequest.end();

        return clientRequest;

    }

    public static HttpClientRequest prepareRequestToDeleteOnboardingForm(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.delete(port,"localhost","/onboardingPacket/form/100");
        clientRequest.handler(httpClientResponse -> {
            context.assertEquals(httpClientResponse.statusCode(),200);
            context.assertTrue(httpClientResponse.headers().get("Content-Type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {

                System.out.println(body.toString());
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("status"),200);
                context.assertTrue(jsonObject.getInteger("status")==200);
                async.complete();
            });
        });

        clientRequest.end();

        return clientRequest;

    }

    public static HttpClientRequest prepareRequestToDeleteOnboardingTask(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.delete(port,"localhost","/onboardingPacket/task/100");
        clientRequest.handler(httpClientResponse -> {
            context.assertEquals(httpClientResponse.statusCode(),200);
            context.assertTrue(httpClientResponse.headers().get("Content-Type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {

                System.out.println(body.toString());
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("status"),200);
                context.assertTrue(jsonObject.getInteger("status")==200);
                async.complete();
            });
        });

        clientRequest.end();

        return clientRequest;

    }

    public static HttpClientRequest prepareRequestToDeleteOnboardingDocument(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.delete(port,"localhost","/onboardingPacket/document/100");
        clientRequest.handler(httpClientResponse -> {
            context.assertEquals(httpClientResponse.statusCode(),200);
            context.assertTrue(httpClientResponse.headers().get("Content-Type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {

                System.out.println(body.toString());
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("status"),200);
                context.assertTrue(jsonObject.getInteger("status")==200);
                async.complete();
            });
        });

        clientRequest.end();

        return clientRequest;

    }


}
