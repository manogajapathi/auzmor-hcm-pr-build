package com.auzmor.test.login;

import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;

public class LoginClientRequest {


    public static HttpClientRequest requestToCheckUserSignup(HttpClient httpClient, TestContext context,int port) {
        Async async = context.async();
        String bodyString = "";
        HttpClientRequest clientRequest =httpClient.post(port,"localhost","/signup/create");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue((statusCode==200)||(statusCode==406));
            context.assertTrue(httpClientResponse.headers().get("Content-type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {

                JsonObject jsonObject = new JsonObject(body.toString());
                if(statusCode == 200) {
                    context.assertEquals(jsonObject.getInteger("code"), 200);
                } else {
                    JsonObject errorObject = jsonObject.getJsonObject("error");
                    context.assertTrue(errorObject.getInteger("code")==406);
                }
                async.complete();
            });
        });
        String body =  "{" +

                "'first_name':'John'" +
                "'last_name' : 'Smith'" +
                "'email':'john.smith@auzmor.com'"+
                "'password':'john@123'"+
                "'organization_id':1"+
                "}";
        clientRequest.putHeader("content-length", String.valueOf(body.length()));
        clientRequest.putHeader("content-type", "application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();
        return clientRequest;
    }

    public static HttpClientRequest requestToCheckLoginInformation(HttpClient httpClient, TestContext context, Integer port) {
        Async async = context.async();
        HttpClientRequest clientRequest =httpClient.post(port,"localhost","/login/check");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue((statusCode==200)||(statusCode==404));
            context.assertTrue(httpClientResponse.headers().get("Content-type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());
                if(statusCode == 200) {
                    context.assertEquals(jsonObject.getInteger("code"), 200);
                } else {
                    JsonObject errorObject = jsonObject.getJsonObject("error");
                    context.assertTrue(errorObject.getInteger("code")==404);
                }
                async.complete();
            });
        });
        String body =  "{" +

                "'first_name':'John'" +
                "'last_name' : 'Smith'" +
                "'email':'john.smith@auzmor.com'"+
                "'password':'john@123'"+
                "'organization_id':1"+
                "}";
        clientRequest.putHeader("content-length", String.valueOf(body.length()));
        clientRequest.putHeader("content-type", "application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();
        return clientRequest;
    }


}
