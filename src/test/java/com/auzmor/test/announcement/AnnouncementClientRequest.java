package com.auzmor.test.announcement;

import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;

public class AnnouncementClientRequest {
    /**
     * Get particular announcement
     * @param client
     * @param context
     * @param port
     * @return
     */
    public static HttpClientRequest prepareRequestforAnnouncementsBasedOnId(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.get(port,"localhost","/announcement/1");
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                System.out.println(body);
                async.complete();
            });
        });
        clientRequest.end();
        return clientRequest;
    }

    /**
     * Create a new announcement
     * @param client
     * @param context
     * @param port
     * @return
     */
    public static HttpClientRequest prepareRequestToPostAnnouncementDetails(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();
        client.post(port,"localhost","/employee/create");
        HttpClientRequest clientRequest = client.post(port,"localhost","/annoucement/create");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue(statusCode==406||statusCode==201);
            context.assertTrue(httpClientResponse.headers().get("Content-type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());

                async.complete();
            });
        });

        String body = "{" +
                "'annoucement_title':'Hiring Process'" +
                "'annoucement_details':'Opening for Front end developers Click here for more info'" +
                "'start_date':'16-11-2017'"+
                "'end_date':'01-12-2017'"+
                "'audience':'All Employees'"+
                "'specific_audience':'None'"+
                "'organization_id':'1'"+

                "}";
        clientRequest.putHeader("content-length", String.valueOf(body.length()));
        clientRequest.putHeader("content-type", "application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();
        return clientRequest;
    }

    /**
     * Delete an announcement
     * @param client
     * @param context
     * @param port
     * @return
     */
    public static HttpClientRequest prepareRequestToDeleteAnnouncementDetails(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.delete(port,"localhost","/announcement/1");
        clientRequest.handler(httpClientResponse -> {
            context.assertEquals(httpClientResponse.statusCode(),200);
            context.assertTrue(httpClientResponse.headers().get("Content-Type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {

                System.out.println(body.toString());
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("status"),200);
                context.assertTrue(jsonObject.getInteger("status")==200);
                async.complete();
            });
        });

        clientRequest.end();

        return clientRequest;

    }

    /**
     * Update an announcement
     * @param client
     * @param context
     * @param port
     * @return
     */
    public static HttpClientRequest prepareRequestToUpdateAnnouncementDetails(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();
        HttpClientRequest clientRequest = client.put(port,"localhost","/announcement/1");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue((statusCode==202)||(statusCode==406));
            context.assertTrue(httpClientResponse.headers().get("Content-Type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {

                System.out.println(body.toString());
                JsonObject jsonObject = new JsonObject(body.toString());
                async.complete();
            });
        });

        String body = "{" +

                "'annoucement_details':'India'" +
                "'start_date' : '17-11-2017'" +
                "}";

        clientRequest.putHeader("content-length",String.valueOf(body.length()));
        clientRequest.putHeader("content-type","application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();

        return clientRequest;
    }
}
