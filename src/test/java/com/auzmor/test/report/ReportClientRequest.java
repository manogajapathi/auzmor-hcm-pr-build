package com.auzmor.test.report;

import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;

public class ReportClientRequest {

    public static HttpClientRequest prepareRequestToJobPostingReport(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.get(port,"localhost","/report/jobposting/details?limit=1&offset=0");
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("code"),200);
                async.complete();
            });
        });
        clientRequest.end();
        return clientRequest;
    }

    public static HttpClientRequest prepareRequestToJobPostingStatusReport(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.get(port,"localhost","/report/jobposting?fields=job_status");
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("code"),200);
                async.complete();
            });
        });
        clientRequest.end();
        return clientRequest;
    }

    public static HttpClientRequest prepareRequestToCurrentStatusReport(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.get(port,"localhost","/report/currentStatus?limit=10&offset=0");
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("code"),200);
                async.complete();
            });
        });
        clientRequest.end();
        return clientRequest;
    }

    public static HttpClientRequest prepareRequestToAverageStatusReport(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.get(port,"localhost","/report/averageStatus?limit=10&offset=0");
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("code"),200);
                async.complete();
            });
        });
        clientRequest.end();
        return clientRequest;
    }

    public static HttpClientRequest prepareRequestToApplicantStatusReport(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.get(port,"localhost","/report/applicantstatus?fields=status");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue(statusCode==200||statusCode==400);

            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());
                System.out.println(body);
                if(!(statusCode==200)) {
                    JsonObject errorObject = jsonObject.getJsonObject("error");
                    context.assertTrue(errorObject.getInteger("code")==400);
                }
                async.complete();
            });
        });
        clientRequest.end();
        return clientRequest;
    }

    public static HttpClientRequest prepareRequestToDisplaySourceEfficiency(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.get(port,"localhost","/report/source");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue(statusCode==200||statusCode==400);

            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());
                System.out.println(body);
                if(!(statusCode==200)) {
                    JsonObject errorObject = jsonObject.getJsonObject("error");
                    context.assertTrue(errorObject.getInteger("code")==400);
                }
                async.complete();
            });
        });
        clientRequest.end();
        return clientRequest;
    }

    public static HttpClientRequest requestToDisplaySourceEfficiencyBasedOnJobPost(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.get(port,"localhost","/report/source?id=49");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue(statusCode==200||statusCode==400);

            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());
                System.out.println(body);
                if(!(statusCode==200)) {
                    JsonObject errorObject = jsonObject.getJsonObject("error");
                    context.assertTrue(errorObject.getInteger("code")==400);
                }
                async.complete();
            });
        });
        clientRequest.end();
        return clientRequest;
    }

    public static HttpClientRequest requestToDisplaySourceEfficiencyWithAllConstraints(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.get(port,"localhost","/report/source?id=49&" +
                                                                "source_date=25/10/2017&end_date=25/10/2017");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue(statusCode==200||statusCode==400);

            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());
                System.out.println(body);
                if(!(statusCode==200)) {
                    JsonObject errorObject = jsonObject.getJsonObject("error");
                    context.assertTrue(errorObject.getInteger("code")==400);
                }
                async.complete();
            });
        });
        clientRequest.end();
        return clientRequest;
    }

    public static HttpClientRequest requestToCheckUpdateSources(HttpClient client, TestContext context, Integer port) {

        Async async = context.async();

        HttpClientRequest clientRequest = client.put(port,"localhost","/report/source/49");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            System.out.println("Status Code : " + statusCode);
            context.assertTrue(statusCode==202||(statusCode==406));
            context.assertTrue(httpClientResponse.headers().get("Content-Type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {

                System.out.println(body.toString());
                JsonObject jsonObject = new JsonObject(body.toString());
                if(statusCode==202){
                    context.assertEquals(jsonObject.getInteger("code"),202);
                } else {
                    JsonObject errorObject = jsonObject.getJsonObject("error");
                    context.assertTrue(errorObject.getInteger("code")==406);
                }

                async.complete();
            });
        });

        String body = "{" +
                "'source' : 'facebook'"+
                "}";
        clientRequest.putHeader("content-length",String.valueOf(body.length()));
        clientRequest.putHeader("content-type","application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();

        return clientRequest;
    }
}
