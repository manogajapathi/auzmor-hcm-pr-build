package com.auzmor.test.configuration;

import java.sql.SQLException;

import com.auzmor.impl.configuration.MysqldbConf;

import org.junit.Assert;
import org.junit.Test;

public class DBConnectionTest {
    /**
     * connectiontest - checks whether the connection is valid after 10 seconds
     * @throws SQLException
     * @throws ClassNotFoundException
     *
     * @author Pradeep Balasubramanian
     */

    public void connectiontest() throws SQLException, ClassNotFoundException {
        MysqldbConf mysqldbConf = new MysqldbConf();
        Assert.assertEquals(mysqldbConf.getConnection().isValid(10), true);

    }
}
