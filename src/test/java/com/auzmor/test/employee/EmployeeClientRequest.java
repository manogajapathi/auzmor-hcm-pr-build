package com.auzmor.test.employee;

import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;

public class EmployeeClientRequest {

    public static HttpClientRequest prepareRequestforEmployeeDetailsBasedOnId(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.get(port,"localhost","/employee/4");
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                System.out.println(body);
                async.complete();
            });
        });
        clientRequest.end();
        return clientRequest;
    }

    public static HttpClientRequest prepareRequestToPostEmployeeDetails(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();
        client.post(port,"localhost","/employee/create");
        HttpClientRequest clientRequest = client.post(port,"localhost","/employee/create");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue(statusCode==406||statusCode==201);
            context.assertTrue(httpClientResponse.headers().get("Content-type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());

                async.complete();
            });
        });

        String body = "{" +
                "'first_name':'pradeep'" +
                "'last_name':'b'" +
                "'personal_email':'pradeep@auzmor.com'"+
                "'phone_number':'9889157456'"+
                "'candidate_id':'1'"+
                "'employment_type':'Permanent'"+
                "'job_status':'Open'"+
                "'location':'IIT MRP'"+
                "'city':'Chennai'"+
                "'state':'Tamil Nadu'"+
                "'zip':'600113'"+
                "'country':'India'"+
                "'job_title':'Java Developer'"+
                "'hire_date' : '20-09-2017'"+
                "'pay_rate':'50000'"+
                "'pay_type':'Monthly'"+
                "'manager':'Asif'"+

                "}";
        clientRequest.putHeader("content-length", String.valueOf(body.length()));
        clientRequest.putHeader("content-type", "application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();
        return clientRequest;
    }

    public static HttpClientRequest prepareRequestToDeleteEmployeeDetails(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.delete(port,"localhost","/employee/8");
        clientRequest.handler(httpClientResponse -> {
            context.assertEquals(httpClientResponse.statusCode(),200);
            context.assertTrue(httpClientResponse.headers().get("Content-Type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {

                System.out.println(body.toString());
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("status"),200);
                context.assertTrue(jsonObject.getInteger("status")==200);
                async.complete();
            });
        });

        clientRequest.end();

        return clientRequest;

    }

    public static HttpClientRequest prepareRequestToUpdateEmployeeDetails(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();
        HttpClientRequest clientRequest = client.put(port,"localhost","/employee/87");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue((statusCode==202)||(statusCode==406));
            context.assertTrue(httpClientResponse.headers().get("Content-Type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {

                System.out.println(body.toString());
                JsonObject jsonObject = new JsonObject(body.toString());
                async.complete();
            });
        });

        String body = "{" +

                "'country':'India'" +
                "'location' : 'Chennai'" +
                "}";

        clientRequest.putHeader("content-length",String.valueOf(body.length()));
        clientRequest.putHeader("content-type","application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();

        return clientRequest;
    }

    public static HttpClientRequest prepareRequestToTerminateEmployee(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();
        HttpClientRequest clientRequest = client.put(port,"localhost","/employee/terminate/40");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue((statusCode==202)||(statusCode==406));
            context.assertTrue(httpClientResponse.headers().get("Content-Type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {

                System.out.println(body.toString());
                async.complete();
            });
        });

        String body = "{" +

                "'termination_effective_date':'India'" +
                "'termination_type' : 'Voluntary'" +
                "'termination_reason' : 'Personal'" +
                "'rehire_eligibility' : 'Yes'" +
                "'termination_comment' : 'Personal Reasons'" +
                "}";

        clientRequest.putHeader("content-length",String.valueOf(body.length()));
        clientRequest.putHeader("content-type","application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();

        return clientRequest;
    }

    public static HttpClientRequest prepareSearchEmployee(HttpClient client, TestContext context, Integer port,String searchString) {
        Async async = context.async();

        String searchUrl = "/employee?fields=employee_id,first_name,last_name&search_string="+searchString;
        HttpClientRequest clientRequest = client.get(port,"localhost",searchUrl);
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                System.out.println(body);
                async.complete();
            });
        });
        clientRequest.end();
        return clientRequest;
    }

    public static HttpClientRequest getEmployeeBasedOnIndex(HttpClient client, TestContext context, Integer port, String index) {
        Async async = context.async();

        String indexSearchUrl = "/employee?fields=employee_id,first_name&sorter="+index;
        HttpClientRequest clientRequest = client.get(port,"localhost",indexSearchUrl);
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                System.out.println(body);
                async.complete();
            });
        });
        clientRequest.end();
        return clientRequest;
    }

    public static HttpClientRequest prepareRequestforTeamOfEmployees(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.get(port,"localhost","/TeamEmployee/admin@auzmor.com");
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                System.out.println(body);
                async.complete();
            });
        });
        clientRequest.end();
        return clientRequest;
    }

    public static HttpClientRequest prepareRequestforOrganizationChart(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.get(port,"localhost","/OrganizationChart/1");
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                System.out.println(body);
                async.complete();
            });
        });
        clientRequest.end();
        return clientRequest;
    }

// sadhana created these on 28/11/2017

    public static HttpClientRequest getEmployeeOnboardingPendingStatus(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        String indexSearchUrl = "/employee/list?fields=employee_id,first_name,last_name,sent_date,last_login&status=pending&orgaization_id=1";
        HttpClientRequest clientRequest = client.get(port,"localhost",indexSearchUrl);
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                System.out.println(body);
                async.complete();
            });
        });
        clientRequest.end();
        return clientRequest;
    }


    public static HttpClientRequest getEmployeeBasedOnIndex(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        String indexSearchUrl = "/employee/list?fields=employee_id,first_name,last_name,sent_date,last_login&status=complete&orgaization_id=1";
        HttpClientRequest clientRequest = client.get(port,"localhost",indexSearchUrl);
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                System.out.println(body);
                async.complete();
            });
        });
        clientRequest.end();
        return clientRequest;
    }



    public static HttpClientRequest getLastEmployeeNO(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();

        String indexSearchUrl = "/employee/lastemployeeno?orgaization_id=1";
        HttpClientRequest clientRequest = client.get(port,"localhost",indexSearchUrl);
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                System.out.println(body);
                async.complete();
            });
        });
        clientRequest.end();
        return clientRequest;
    }


}
