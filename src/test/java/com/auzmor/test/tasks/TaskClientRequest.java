package com.auzmor.test.tasks;

import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;

public class TaskClientRequest {

    // sadhana created these on 20/12/2017
    public static HttpClientRequest prepareRequestToPostTaskDetails(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();
        client.post(port,"localhost","/task/create");
        HttpClientRequest clientRequest = client.post(port,"localhost","/task/create");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue(statusCode==406||statusCode==201);
            context.assertTrue(httpClientResponse.headers().get("Content-type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());
                async.complete();
            });
        });

        String body = "{" +
                "'task_title':'Reviews'" +
                "'due_date':'20/12/2017'" +
                "'time_due':'1:30 pm'" +
                "'recurrence':'Everyday'" +
                "'priority':'Medium'" +
                "'project_id':'1'" +
                "'assign_to':'12'" +
                "'organization_id':'1'" +
                "}";
        clientRequest.putHeader("content-length", String.valueOf(body.length()));
        clientRequest.putHeader("content-type", "application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();
        return clientRequest;
    }


    public static HttpClientRequest prepareRequestToUpdateTaskDetails(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();
        HttpClientRequest clientRequest = client.put(port,"localhost","/task/update?task_id=1");
        clientRequest.handler(httpClientResponse -> {
            int statusCode = httpClientResponse.statusCode();
            context.assertTrue((statusCode==202)||(statusCode==406));
            context.assertTrue(httpClientResponse.headers().get("Content-Type").contains("application/json"));
            httpClientResponse.bodyHandler(body -> {
                System.out.println(body.toString());
                JsonObject jsonObject = new JsonObject(body.toString());
                async.complete();
            });
        });
        String body = "{" +
                "'due_date':'22/12/2017'" +
                "'time_due':'2:30 pm'" +
                "}";
        clientRequest.putHeader("content-length",String.valueOf(body.length()));
        clientRequest.putHeader("content-type","application/x-www-form-urlencoded");
        clientRequest.write(body);
        clientRequest.end();
        return clientRequest;
    }


    public static HttpClientRequest prepareRequestForTaskBasedOnId(HttpClient client, TestContext context, int port) {
        Async async = context.async();

        HttpClientRequest clientRequest = client.get(port,"localhost","/task?task_id=1");
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                System.out.println("Response Body : " + body);
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("code"),200);
                async.complete();
            });
        });
        clientRequest.end();

        return clientRequest;

    }

    public static HttpClientRequest prepareRequestForTaskBasedOnConstraints(HttpClient client, TestContext context, Integer port) {
        Async async = context.async();
        HttpClientRequest clientRequest = client.get(port,"localhost",
                "/task?fields=task_title,recurrence,time_left");
        clientRequest.handler(httpClientResponse -> {

            context.assertEquals(httpClientResponse.statusCode(),200);
            httpClientResponse.bodyHandler(body -> {
                JsonObject jsonObject = new JsonObject(body.toString());
                context.assertEquals(jsonObject.getInteger("code"),200);
                async.complete();
            });
        });
        clientRequest.end();

        return clientRequest;
    }

}
