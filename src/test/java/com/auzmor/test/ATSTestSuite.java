package com.auzmor.test;

import com.auzmor.test.candidate.CandidateModelTest;
import com.auzmor.test.configuration.DBConnectionTest;
import com.auzmor.test.job.JobModelTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        DBConnectionTest.class,
        ATSVertxTest.class,
        JobModelTest.class,
        CandidateModelTest.class
})

public class ATSTestSuite {


}
