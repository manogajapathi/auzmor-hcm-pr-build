package com.auzmor.unittest;

import com.auzmor.impl.bo.EmployeeBOImpl;
import com.auzmor.impl.dao.EmployeeDAOImpl;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.employee.Employee;

import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EmployeeBOImplTest {
  @Mock
  EmployeeDAOImpl employeeDAO;
  private Employee employee;
  private JsonArray jsonArray;
  private EmployeeBOImpl employeeBO;
  private MultiMap params;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    employee = new Employee();
  }


  public void createEmployee(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          employeeBO = new EmployeeBOImpl(dbHandle);
          employeeDAO = new EmployeeDAOImpl(dbHandle);
          employeeBO.setEmployeeDAO(employeeDAO);
          params = null;
          when(employeeDAO.create(employee)).thenReturn(new Integer(1));
          Employee employee = employeeBO.addNewEmployee(params);
          assert (employee != null);
          verify(employeeDAO).create(employee);
        });
  }


  public void notCreateEmployee(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          employeeBO = new EmployeeBOImpl(dbHandle);
          employeeDAO = new EmployeeDAOImpl(dbHandle);
          employeeBO.setEmployeeDAO(employeeDAO);
          MultiMap params = null;
          when(employeeDAO.create(employee)).thenReturn(new Integer(0));

          Employee employee = employeeBO.addNewEmployee(params);

          assert (employee == null);
          verify(employeeDAO).create(employee);
        });
  }

  public void alreadyExistsEmployee(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          employeeBO = new EmployeeBOImpl(dbHandle);
          employeeDAO = new EmployeeDAOImpl(dbHandle);
          employeeBO.setEmployeeDAO(employeeDAO);
          String email = " ";
          long organizationId = 0;
          when(employeeDAO.isEmployeeExistsInOrganization(email, organizationId)).thenReturn(true);

          boolean isExists = employeeBO.isEmployeeExists(email, organizationId);

          assert (isExists);
          verify(employeeDAO).create(employee);
        });
  }

  public void alreadyNotExistsEmployee(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          employeeBO = new EmployeeBOImpl(dbHandle);
          employeeDAO = new EmployeeDAOImpl(dbHandle);
          employeeBO.setEmployeeDAO(employeeDAO);
          String email = " ";
          long organizationId = 0;
          when(employeeDAO.isEmployeeExistsInOrganization(email, organizationId)).thenReturn(false);

          boolean isExists = employeeBO.isEmployeeExists(email, organizationId);

          assert (!isExists);
          verify(employeeDAO).create(employee);
        });
  }

  public void getEmployeeList(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {

          employeeBO = new EmployeeBOImpl(dbHandle);
          employeeBO.setEmployeeDAO(employeeDAO);
          employeeDAO = new EmployeeDAOImpl(dbHandle);
          MultiMap map = null;
          when(employeeDAO.getAllEmployees(map)).thenReturn(jsonArray);

          JsonArray resultArray = employeeBO.getAllEmployees(map);
          assert (resultArray != null);
          verify(employeeDAO).getAllEmployees(map);
        });
  }

  public void notgetEmployeeList(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          employeeBO = new EmployeeBOImpl(dbHandle);
          employeeDAO = new EmployeeDAOImpl(dbHandle);
          employeeBO.setEmployeeDAO(employeeDAO);
          MultiMap map = null;
          when(employeeDAO.getAllEmployees(map)).thenReturn(jsonArray);

          JsonArray resultArray = employeeBO.getAllEmployees(map);

          assert (resultArray == null);
          verify(employeeDAO).getAllEmployees(map);
        });
  }


  public void getEmployeeDetails(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          employeeBO = new EmployeeBOImpl(dbHandle);
          employeeDAO = new EmployeeDAOImpl(dbHandle);
          employeeBO.setEmployeeDAO(employeeDAO);
          int employeeId = 0;
          when(employeeDAO.getEmployeeDetails(employeeId)).thenReturn(employee);

          Employee employeeModel = employeeBO.getEmployeeDetails(employeeId);

          assert (employeeModel != null);
          verify(employeeDAO).getEmployeeDetails(employeeId);
        });
  }

  public void notGetEmployeeDetails(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          employeeBO = new EmployeeBOImpl(dbHandle);
          employeeDAO = new EmployeeDAOImpl(dbHandle);
          employeeBO.setEmployeeDAO(employeeDAO);
          int employeeId = 0;
          when(employeeDAO.getEmployeeDetails(employeeId)).thenReturn(employee);

          Employee employeeModel = employeeBO.getEmployeeDetails(employeeId);

          assert (employeeModel != null);
          verify(employeeDAO).getEmployeeDetails(employeeId);
        });
  }

  public void getEmployeeDetailsByLoginId(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          employeeBO = new EmployeeBOImpl(dbHandle);
          employeeDAO = new EmployeeDAOImpl(dbHandle);
          employeeBO.setEmployeeDAO(employeeDAO);
          int loginId = 0;
          when(employeeDAO.getEmployeeDetailsByLoginId(loginId)).thenReturn(employee);

          Employee employeeModel = employeeBO.getEmployeeDetailsByLoginId(loginId);

          assert (employeeModel != null);
          verify(employeeDAO).getEmployeeDetailsByLoginId(loginId);
        });
  }

  public void notGetEmployeeDetailsByLoginId(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          employeeBO = new EmployeeBOImpl(dbHandle);
          employeeDAO = new EmployeeDAOImpl(dbHandle);
          employeeBO.setEmployeeDAO(employeeDAO);
          int loginId = 0;
          when(employeeDAO.getEmployeeDetailsByLoginId(loginId)).thenReturn(employee);

          Employee employeeModel = employeeBO.getEmployeeDetailsByLoginId(loginId);

          assert (employeeModel == null);
          verify(employeeDAO).getEmployeeDetailsByLoginId(loginId);
        });
  }

  public void getUsersList(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          employeeBO = new EmployeeBOImpl(dbHandle);
          employeeDAO = new EmployeeDAOImpl(dbHandle);
          employeeBO.setEmployeeDAO(employeeDAO);
          MultiMap param = null;
          when(employeeDAO.getAllUsersList(param)).thenReturn(jsonArray);

          JsonArray resultArray = employeeBO.getAllUsersList(param);

          assert (resultArray != null);
          verify(employeeDAO).getAllUsersList(param);
        });
  }

  public void notGetUsersList(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          employeeBO = new EmployeeBOImpl(dbHandle);
          employeeDAO = new EmployeeDAOImpl(dbHandle);
          employeeBO.setEmployeeDAO(employeeDAO);
          MultiMap param = null;
          when(employeeDAO.getAllUsersList(param)).thenReturn(jsonArray);

          JsonArray resultArray = employeeBO.getAllUsersList(param);

          assert (resultArray == null);
          verify(employeeDAO).getAllUsersList(param);
        });
  }

  public void updateUser(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          employeeBO = new EmployeeBOImpl(dbHandle);
          employeeDAO = new EmployeeDAOImpl(dbHandle);
          employeeBO.setEmployeeDAO(employeeDAO);
          MultiMap param = null;
          when(employeeDAO.editUser(param)).thenReturn(new Integer(1));

          int updateCount = employeeBO.editUser(vertx, param);

          assert (updateCount > 0);
          verify(employeeDAO).editUser(param);
        });
  }


  public void notUpdateUser(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          employeeBO = new EmployeeBOImpl(dbHandle);
          employeeDAO = new EmployeeDAOImpl(dbHandle);
          employeeBO.setEmployeeDAO(employeeDAO);
          MultiMap param = null;
          when(employeeDAO.editUser(param)).thenReturn(new Integer(0));

          int updateCount = employeeBO.editUser(vertx, param);

          assert (updateCount == 0);
          verify(employeeDAO).editUser(param);
        });
  }


  public void createUser(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          employeeBO = new EmployeeBOImpl(dbHandle);
          employeeDAO = new EmployeeDAOImpl(dbHandle);
          employeeBO.setEmployeeDAO(employeeDAO);
          MultiMap params = null;
          when(employeeDAO.addNewUser(params)).thenReturn(new Integer(1));

          int createdCount = employeeBO.addNewUser(params);

          assert (createdCount > 0);
          verify(employeeDAO).addNewUser(params);
        });
  }


  public void notCreateUser(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          employeeBO = new EmployeeBOImpl(dbHandle);
          employeeDAO = new EmployeeDAOImpl(dbHandle);
          employeeBO.setEmployeeDAO(employeeDAO);
          MultiMap params = null;
          when(employeeDAO.addNewUser(params)).thenReturn(new Integer(0));

          int createdCount = employeeBO.addNewUser(params);

          assert (createdCount == 0);
          verify(employeeDAO).addNewUser(params);
        });
  }


  public void deleteUser(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          employeeBO = new EmployeeBOImpl(dbHandle);
          employeeDAO = new EmployeeDAOImpl(dbHandle);
          employeeBO.setEmployeeDAO(employeeDAO);
          int organizationId = 0;
          int employeeId = 0;
          MultiMap map = null;
          when(employeeDAO.deleteUser(organizationId, employeeId)).thenReturn(new Integer(1));

          int updateCount = employeeBO.deleteUser(map);

          assert (updateCount > 0);
          verify(employeeDAO).deleteUser(organizationId, employeeId);
        });
  }


  public void notDeleteUser(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          employeeBO = new EmployeeBOImpl(dbHandle);
          employeeDAO = new EmployeeDAOImpl(dbHandle);
          employeeBO.setEmployeeDAO(employeeDAO);
          int organizationId = 0;
          int employeeId = 0;
          MultiMap map = null;
          when(employeeDAO.deleteUser(organizationId, employeeId)).thenReturn(new Integer(0));

          int updateCount = employeeBO.deleteUser(map);

          assert (updateCount == 0);
          verify(employeeDAO).deleteUser(organizationId, employeeId);
        });
  }

  public void testGetDashBoardCount(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          employeeBO = new EmployeeBOImpl(dbHandle);
          employeeDAO = new EmployeeDAOImpl(dbHandle);
          employeeBO.setEmployeeDAO(employeeDAO);
          JsonObject dataObject = new JsonObject();
          dataObject.put("job_count", 1);
          dataObject.put("candidate_count", 1);
          dataObject.put("task_count", 1);
          dataObject.put("interview_count", 1);
          when(employeeDAO.getDashBoardCount(employee)).thenReturn(dataObject);
          verify(employeeDAO).getDashBoardCount(employee);
        });
  }

  public void testGetDashBoardCountNoSuchValues(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          employeeBO = new EmployeeBOImpl(dbHandle);
          employeeDAO = new EmployeeDAOImpl(dbHandle);
          employeeBO.setEmployeeDAO(employeeDAO);
          JsonObject dataObject = new JsonObject();
          when(employeeDAO.getDashBoardCount(employee)).thenReturn(dataObject);
          verify(employeeDAO).getDashBoardCount(employee);
        });
  }

  public void testGetDashBoardCountSQLException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          employeeBO = new EmployeeBOImpl(dbHandle);
          employeeDAO = new EmployeeDAOImpl(dbHandle);
          employeeBO.setEmployeeDAO(employeeDAO);
          JsonObject dataObject = new JsonObject();
          when(employeeDAO.getDashBoardCount(employee)).thenThrow(SQLException.class);
          verify(employeeDAO).getDashBoardCount(employee);
        });
  }

  public void testGetDashBoardCountDbHandleException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          employeeBO = new EmployeeBOImpl(dbHandle);
          employeeDAO = new EmployeeDAOImpl(dbHandle);
          employeeBO.setEmployeeDAO(employeeDAO);
          JsonObject dataObject = new JsonObject();
          when(employeeDAO.getDashBoardCount(employee)).thenThrow(DbHandleException.class);
          verify(employeeDAO).getDashBoardCount(employee);
        });
  }
}
