package com.auzmor.unittest;

import com.auzmor.bo.OnboardingDocumentBO;
import com.auzmor.dao.OnboardingDocumentDAO;
import com.auzmor.impl.bo.OnboardingDocumentBOImpl;
import com.auzmor.impl.dao.OnboardingDocumentDAOImpl;
import com.auzmor.impl.enumarator.bo.OnboardingDocumentEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.onboarding.OnboardingDocument;
import com.auzmor.util.bo.ReturnObject;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class OnboardingDocumentBOImplTest {
    @Mock
    OnboardingDocumentDAO onboardingDocumentDAO;
    private OnboardingDocumentBO onboardingDocumentBO;
    private OnboardingDocument onboardingDocument;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        onboardingDocument = new OnboardingDocument();
    }

    public void addDocument(Vertx vertx) throws SQLException, DbHandleException {
        DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
                dbHandle -> {
                    onboardingDocumentBO = new OnboardingDocumentBOImpl();
                    onboardingDocumentDAO = new OnboardingDocumentDAOImpl(dbHandle);
                    MultiMap params = null;
                    when(onboardingDocumentDAO.addDocument(onboardingDocument)).thenReturn(new Integer(1));

                    ReturnObject<OnboardingDocumentEnum, JsonObject> boReponse =
                            onboardingDocumentBO.addDocument(dbHandle, params);

                    assert (boReponse.getStatusEnum() == OnboardingDocumentEnum.ADD_SUCCESS);
                    verify(onboardingDocumentDAO).addDocument(onboardingDocument);
                }
        );
    }



    public void createDocumentNoSuchValues(Vertx vertx) throws SQLException, DbHandleException {
      DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
          dbHandle -> {
                    onboardingDocumentBO = new OnboardingDocumentBOImpl();
                    onboardingDocumentDAO = new OnboardingDocumentDAOImpl(dbHandle);
                    MultiMap params = null;
                    when(onboardingDocumentDAO.addDocument(onboardingDocument)).thenReturn(new Integer(0));

                    ReturnObject<OnboardingDocumentEnum, JsonObject> boReponse =
                            onboardingDocumentBO.addDocument(dbHandle, params);

                    assert (boReponse.getStatusEnum() == OnboardingDocumentEnum.ADD_FAILURE);
                    verify(onboardingDocumentDAO).addDocument(onboardingDocument);
                }
        );
    }

    public void addDocumentAlreadyExists(Vertx vertx) throws SQLException, DbHandleException {
      DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
          dbHandle -> {
                    onboardingDocumentBO = new OnboardingDocumentBOImpl();
                    onboardingDocumentDAO = new OnboardingDocumentDAOImpl(dbHandle);
                    MultiMap params = null;
                    when(onboardingDocumentDAO.addDocument(onboardingDocument)).thenReturn(new Integer(-1));

                    ReturnObject<OnboardingDocumentEnum, JsonObject> boReponse =
                            onboardingDocumentBO.addDocument(dbHandle, params);

                    assert (boReponse.getStatusEnum() == OnboardingDocumentEnum.ALREADY_EXISTS);
                    verify(onboardingDocumentDAO).addDocument(onboardingDocument);
                }
        );
    }

    public void deletePacket(Vertx vertx) throws SQLException, DbHandleException {
      DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
          dbHandle -> {
                    onboardingDocumentBO = new OnboardingDocumentBOImpl();
                    onboardingDocumentDAO = new OnboardingDocumentDAOImpl(dbHandle);
                    MultiMap params = null;
                    when(onboardingDocumentDAO.deleteDocument(onboardingDocument)).thenReturn(new Integer(1));

                    ReturnObject<OnboardingDocumentEnum, JsonObject> boReponse =
                            onboardingDocumentBO.deleteDocument(dbHandle, params);

                    assert (boReponse.getStatusEnum() == OnboardingDocumentEnum.DELETE_SUCCESS);
                    verify(onboardingDocumentDAO).deletePacket(onboardingDocument);
                }
        );
    }

    public void deletePacketNoSuchValues(Vertx vertx) throws SQLException, DbHandleException {
      DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
          dbHandle -> {
                    onboardingDocumentBO = new OnboardingDocumentBOImpl();
                    onboardingDocumentDAO = new OnboardingDocumentDAOImpl(dbHandle);
                    MultiMap params = null;
                    when(onboardingDocumentDAO.deleteDocument(onboardingDocument)).thenReturn(new Integer(0));

                    ReturnObject<OnboardingDocumentEnum, JsonObject> boReponse =
                            onboardingDocumentBO.deleteDocument(dbHandle, params);

                    assert (boReponse.getStatusEnum() == OnboardingDocumentEnum.DELETE_FAILURE);
                    verify(onboardingDocumentDAO).deletePacket(onboardingDocument);
                }
        );
    }

    public void deleteDocumentThrowException(Vertx vertx) throws SQLException, DbHandleException {
      DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
          dbHandle -> {
                    onboardingDocumentBO = new OnboardingDocumentBOImpl();
                    onboardingDocumentDAO = new OnboardingDocumentDAOImpl(dbHandle);
                    MultiMap params = null;
                    when(onboardingDocumentDAO.deleteDocument(onboardingDocument)).thenReturn(new Integer(-1));

                    ReturnObject<OnboardingDocumentEnum, JsonObject> boReponse =
                            onboardingDocumentBO.deleteDocument(dbHandle, params);

                    assert (boReponse.getStatusEnum() == OnboardingDocumentEnum.NO_SUCH_VALUE);
                    verify(onboardingDocumentDAO).deletePacket(onboardingDocument);
                }
        );
    }

    public void updateDocument(Vertx vertx) throws SQLException, DbHandleException {
      DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
          dbHandle -> {
                    onboardingDocumentBO = new OnboardingDocumentBOImpl();
                    onboardingDocumentDAO = new OnboardingDocumentDAOImpl(dbHandle);
                    MultiMap params = null;
                    when(onboardingDocumentDAO.updateDocument(params)).thenReturn(new Integer(1));

                    ReturnObject<OnboardingDocumentEnum, JsonObject> boReponse =
                            onboardingDocumentBO.updateDocument(dbHandle, params);

                    assert (boReponse.getStatusEnum() == OnboardingDocumentEnum.UPDATE_SUCCESS);
                    verify(onboardingDocumentDAO).updateDocument(params);
                }
        );
    }

    public void updateDocumentNoSuchValues(Vertx vertx) throws SQLException, DbHandleException {
      DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
          dbHandle -> {
                    onboardingDocumentBO = new OnboardingDocumentBOImpl();
                    onboardingDocumentDAO = new OnboardingDocumentDAOImpl(dbHandle);
                    MultiMap params = null;
                    when(onboardingDocumentDAO.updateDocument(params)).thenReturn(new Integer(-1));

                    ReturnObject<OnboardingDocumentEnum, JsonObject> boReponse =
                            onboardingDocumentBO.updateDocument(dbHandle, params);

                    assert (boReponse.getStatusEnum() == OnboardingDocumentEnum.UPDATE_FAILURE);
                    verify(onboardingDocumentDAO).updateDocument(params);
                }
        );
    }
}


