package com.auzmor.unittest;

import com.auzmor.dao.EmailTemplateDAO;
import com.auzmor.impl.bo.EmailTemplateBOImpl;
import com.auzmor.impl.dao.EmailTemplateDAOImpl;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.EmailTemplate;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EmailTemplatesBOImplTest {
    @Mock
    EmailTemplateDAO emailTemplateDAO;
    private EmailTemplateBOImpl emailTemplateBO;
    EmailTemplate emailTemplate;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }


    public void addTemplate(Vertx vertx) throws SQLException,DbHandleException {
      DbHelper.safeExecuteDbHandleMethod(vertx, false, false,
          dbHandle ->  {
                    String name = "sada";
                    String subject = "adsa";
                    String header = "sadsad";
                    String footer = "asdasd";
                    String template = "hagdhsagd";
                    long organizationId = 0;
                    emailTemplateDAO = new EmailTemplateDAOImpl(dbHandle);
                    emailTemplateBO = new EmailTemplateBOImpl(dbHandle);
                    emailTemplateBO.setEmailTemplateDAO(emailTemplateDAO);
                    when(emailTemplateDAO.create(name,subject,header,footer,template,organizationId)).
                            thenReturn(new Long(1));

                    /*Long createdCount = emailTemplateBO.addTemplate(name,subject,header,footer,template,organizationId);

                    assert (createdCount > 0 );
                    verify(emailTemplateDAO).create(name,subject,header,footer,template,organizationId);*/
                });
    }

    public void notAddTemplate(Vertx vertx) throws SQLException,DbHandleException {
      DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
          dbHandle ->  {
                    EmailTemplateBOImpl emailTemplateBO = new EmailTemplateBOImpl(dbHandle);
                    String name = "";
                    String subject = "";
                    String header = "";
                    String footer = "";
                    String template = "";
                    long organizationId = 0;
                    emailTemplateDAO = new EmailTemplateDAOImpl(dbHandle);
                    emailTemplateBO.setEmailTemplateDAO(emailTemplateDAO);
                    when(emailTemplateDAO.create(name,subject,header,footer,template,organizationId)).
                            thenReturn(new Long(0));

                    Long createdCount = emailTemplateBO.addTemplate(name,subject,header,footer,template,organizationId);

                    assert (createdCount == 0 );
                    verify(emailTemplateDAO).create(name,subject,header,footer,template,organizationId);
                });
    }

    public void editTemplate(Vertx vertx) throws SQLException,DbHandleException {
      DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
          dbHandle ->  {
                    EmailTemplateBOImpl emailTemplateBO = new EmailTemplateBOImpl(dbHandle);
                    String name = "";
                    String subject = "";
                    String header = "";
                    String footer = "";
                    String template = "";
                    long organizationId = 0;
                    emailTemplateDAO = new EmailTemplateDAOImpl(dbHandle);
                    emailTemplateBO.setEmailTemplateDAO(emailTemplateDAO);
                    when(emailTemplateDAO.update(name,subject,header,footer,template,organizationId)).
                            thenReturn(new Integer(1));

                    Boolean isEdited = emailTemplateBO.editTemplate(name,subject,header,footer,template,organizationId);

                    assert (isEdited);
                    verify(emailTemplateDAO).update(name,subject,header,footer,template,organizationId);
                });
    }

    public void notEditTemplate(Vertx vertx) throws SQLException,DbHandleException {
        DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
                dbHandle -> {
                    EmailTemplateBOImpl emailTemplateBO = new EmailTemplateBOImpl(dbHandle);
                    String name = "";
                    String subject = "";
                    String header = "";
                    String footer = "";
                    String template = "";
                    long organizationId = 0;
                    emailTemplateBO.setEmailTemplateDAO(emailTemplateDAO);
                    when(emailTemplateDAO.update(name,subject,header,footer,template,organizationId)).
                            thenReturn(new Integer(0));

                    Boolean isEdited = emailTemplateBO.editTemplate(name,subject,header,footer,template,organizationId);

                    assert (!isEdited);
                    verify(emailTemplateDAO).update(name,subject,header,footer,template,organizationId);
                });
    }

    public void deleteTemplate(Vertx vertx) throws SQLException,DbHandleException {
      DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
          dbHandle -> {
                    EmailTemplateBOImpl emailTemplateBO = new EmailTemplateBOImpl(dbHandle);
                    long templateId = 0;
                    emailTemplateBO.setEmailTemplateDAO(emailTemplateDAO);
                    when(emailTemplateDAO.delete(templateId)).
                            thenReturn(new Integer(1));

                    Boolean isDeleted = emailTemplateBO.deleteTemplate(templateId);

                    assert (isDeleted);
                    verify(emailTemplateDAO).delete(templateId);
                });
    }

    public void notDeleteTemplate(Vertx vertx) throws SQLException,DbHandleException {
      DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
          dbHandle -> {
                    EmailTemplateBOImpl emailTemplateBO = new EmailTemplateBOImpl(dbHandle);
                    long templateId = 0;
                    emailTemplateBO.setEmailTemplateDAO(emailTemplateDAO);
                    when(emailTemplateDAO.delete(templateId)).
                            thenReturn(new Integer(0));

                    Boolean isDeleted = emailTemplateBO.deleteTemplate(templateId);

                    assert (!isDeleted);
                    verify(emailTemplateDAO).delete(templateId);
                });
    }

    public void getTemplate(Vertx vertx) throws SQLException,DbHandleException {
        DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
                dbHandle -> {
                    EmailTemplateBOImpl emailTemplateBO = new EmailTemplateBOImpl(dbHandle);
                    long templateId = 0;
                    emailTemplateBO.setEmailTemplateDAO(emailTemplateDAO);
                    when(emailTemplateDAO.getTemplate(templateId)).
                            thenReturn(emailTemplate);

                    JsonObject resultObject = emailTemplateBO.getTemplate(templateId);

                    assert (resultObject != null);
                    verify(emailTemplateDAO).getTemplate(templateId);
                });
    }

    public void notGetTemplate(Vertx vertx) throws SQLException,DbHandleException {
      DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
          dbHandle -> {
                    EmailTemplateBOImpl emailTemplateBO = new EmailTemplateBOImpl(dbHandle);
                    long templateId = 0;
                    emailTemplateBO.setEmailTemplateDAO(emailTemplateDAO);
                    when(emailTemplateDAO.getTemplate(templateId)).
                            thenReturn(emailTemplate);

                    JsonObject resultObject = emailTemplateBO.getTemplate(templateId);

                    assert (resultObject == null);
                    verify(emailTemplateDAO).getTemplate(templateId);
                });
    }


    public void getTemplates(Vertx vertx) throws SQLException,DbHandleException {
      DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
          dbHandle -> {
                    EmailTemplateBOImpl emailTemplateBO = new EmailTemplateBOImpl(dbHandle);
                    long organizationId = 0;
                    List<EmailTemplate> list = null;
                    emailTemplateBO.setEmailTemplateDAO(emailTemplateDAO);
                    when(emailTemplateDAO.getTemplates(organizationId)).
                            thenReturn(list);

                    JsonArray resultArray = emailTemplateBO.getTemplates(organizationId);

                    assert (!(resultArray.isEmpty()));
                    verify(emailTemplateDAO).getTemplates(organizationId);
                });
    }

    public void notGetTemplates(Vertx vertx) throws SQLException,DbHandleException {
      DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
          dbHandle -> {
                    EmailTemplateBOImpl emailTemplateBO = new EmailTemplateBOImpl(dbHandle);
                    long organizationId = 0;
                    List<EmailTemplate> list = null;
                    emailTemplateBO.setEmailTemplateDAO(emailTemplateDAO);
                    when(emailTemplateDAO.getTemplates(organizationId)).
                            thenReturn(list);

                    JsonArray resultArray = emailTemplateBO.getTemplates(organizationId);

                    assert (resultArray.isEmpty());
                    verify(emailTemplateDAO).getTemplates(organizationId);
                });
    }

    public void generateEmails(Vertx vertx) throws SQLException,DbHandleException {
      DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
          dbHandle -> {
                    EmailTemplateBOImpl emailTemplateBO = new EmailTemplateBOImpl(dbHandle);
                    long templateId = 0;
                    List<Long> candidateIds = null;
                    emailTemplateBO.setEmailTemplateDAO(emailTemplateDAO);
                    when(emailTemplateDAO.getTemplate(templateId)).
                            thenReturn(emailTemplate);

                    JsonArray resultArray = emailTemplateBO.generateEmails(candidateIds,templateId);

                    assert (!(resultArray.isEmpty()));
                    verify(emailTemplateDAO).getTemplate(templateId);
                });
    }


    public void notGenerateEmails(Vertx vertx) throws SQLException,DbHandleException {
      DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
          dbHandle -> {
                    EmailTemplateBOImpl emailTemplateBO = new EmailTemplateBOImpl(dbHandle);
                    long templateId = 0;
                    List<Long> candidateIds = null;
                    emailTemplateBO.setEmailTemplateDAO(emailTemplateDAO);
                    when(emailTemplateDAO.getTemplate(templateId)).
                            thenReturn(emailTemplate);

                    JsonArray resultArray = emailTemplateBO.generateEmails(candidateIds,templateId);

                    assert (resultArray.isEmpty());
                    verify(emailTemplateDAO).getTemplate(templateId);
                });
    }
}
