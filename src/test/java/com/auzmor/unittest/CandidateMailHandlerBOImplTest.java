package com.auzmor.unittest;

import com.auzmor.dao.MailDetailsDAO;
import com.auzmor.impl.bo.MailDetailsBOImpl;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.models.CandidateR.MailDetails;
import io.vertx.core.Vertx;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CandidateMailHandlerBOImplTest {
  @Mock
  MailDetailsDAO mailDetailsDAO;
  private MailDetailsBOImpl mailDetailsBO;
  private MailDetails mailDetails;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    mailDetails = new MailDetails();
  }

  
  public void testCreateMail(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx,  false,true, dbHandle -> {
      MailDetailsBOImpl mailDetailsBO = new MailDetailsBOImpl(dbHandle);
      mailDetailsBO.setMailDetailsDAO(mailDetailsDAO);

      when(mailDetailsDAO.create(mailDetails)).thenReturn(1);
      assert(mailDetailsDAO.create(mailDetails) > 0);

      verify(mailDetailsDAO.create(mailDetails));
    });
  }

  
  public void testNoMailCreated(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx,  false,true, dbHandle -> {
      MailDetailsBOImpl mailDetailsBO = new MailDetailsBOImpl(dbHandle);
      mailDetailsBO.setMailDetailsDAO(mailDetailsDAO);

      when(mailDetailsDAO.create(mailDetails)).thenReturn(0);
      assert(mailDetailsDAO.create(mailDetails) == 0);

      verify(mailDetailsDAO.create(mailDetails));
    });
  }

  
  public void testCreateMailProcessThrowsSQLException(Vertx vertx) throws SQLException, DbHandleException {
DbHelper.safeExecuteDbHandleMethod(vertx, false,true, dbHandle -> {
      MailDetailsBOImpl mailDetailsBO = new MailDetailsBOImpl(dbHandle);
      mailDetailsBO.setMailDetailsDAO(mailDetailsDAO);

      when(mailDetailsDAO.create(mailDetails)).thenThrow(SQLException.class);
    });
  }

  
  public void testCreateMailProcessThrowsDbHandleException(Vertx vertx)
      throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx,  false,true, dbHandle -> {
      MailDetailsBOImpl mailDetailsBO = new MailDetailsBOImpl(dbHandle);
      mailDetailsBO.setMailDetailsDAO(mailDetailsDAO);

      when(mailDetailsDAO.create(mailDetails)).thenThrow(DbHandleException.class);
    });
  }

  
  public void testGetMailDetails(Vertx vertx) throws SQLException, DbHandleException {
DbHelper.safeExecuteDbHandleMethod(vertx, false,true, dbHandle -> {
      MailDetailsBOImpl mailDetailsBO = new MailDetailsBOImpl(dbHandle);
      mailDetailsBO.setMailDetailsDAO(mailDetailsDAO);

      List<HashMap<String,String>> mailDetailsMap = mailDetailsDAO.getMailDetails(1);
      assert(mailDetailsMap.size() > 0);
    });
  }
}
