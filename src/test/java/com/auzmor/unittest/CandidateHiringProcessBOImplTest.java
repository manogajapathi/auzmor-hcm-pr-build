package com.auzmor.unittest;

import com.auzmor.dao.CandidateHiringDAO;
import com.auzmor.impl.bo.CandidateHiringProcessBOImpl;
import com.auzmor.impl.dao.CandidateHiringProcessDAOImpl;
import com.auzmor.impl.enumarator.bo.CandidateHiringProcessEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.models.CandidateR.HiringProcessCandidate;
import io.vertx.core.Vertx;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CandidateHiringProcessBOImplTest {
  @Mock
  private CandidateHiringDAO candidateHiringDAO;
  @Mock
  private CandidateHiringProcessBOImpl candidateHiringProcessBO;
  private HiringProcessCandidate hiringProcessCandidate;
  private CandidateHiringProcessDAOImpl candidateHiringProcessDAOImpl;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    hiringProcessCandidate = new HiringProcessCandidate();
  }

  public void createHiringProcessForCandidate(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          CandidateHiringProcessBOImpl candidateHiringProcessBO = new CandidateHiringProcessBOImpl(dbHandle);
          candidateHiringProcessBO.setCandidateHiringDAO(candidateHiringDAO);
          when(candidateHiringDAO.create(hiringProcessCandidate)).thenReturn(new Integer(1));

          CandidateHiringProcessEnum insertCount = candidateHiringProcessBO.
              insertCandidateStatus(dbHandle, hiringProcessCandidate);

          assert (insertCount == insertCount.SUCCESS);
          verify(candidateHiringDAO).create(hiringProcessCandidate);
        });
  }


  public void notCreateHiringProcessForCandidate(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          candidateHiringProcessBO = new CandidateHiringProcessBOImpl(dbHandle);
          candidateHiringProcessBO.setCandidateHiringDAO(candidateHiringDAO);
          when(candidateHiringDAO.create(hiringProcessCandidate)).thenReturn(new Integer(0));

          CandidateHiringProcessEnum insertCount = candidateHiringProcessBO.
              insertCandidateStatus(dbHandle, hiringProcessCandidate);
          assert (insertCount == CandidateHiringProcessEnum.FAILED);
          verify(candidateHiringDAO).create(hiringProcessCandidate);
        });
  }

  public void hiringProcessForCandidateThrowsException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          candidateHiringProcessBO = new CandidateHiringProcessBOImpl(dbHandle);
          candidateHiringProcessBO.setCandidateHiringDAO(candidateHiringDAO);
          when(candidateHiringDAO.create(hiringProcessCandidate)).thenThrow(SQLException.class);

          CandidateHiringProcessEnum insertCount = candidateHiringProcessBO.
              insertCandidateStatus(dbHandle, hiringProcessCandidate);
        });
  }

  public void getHiringProcessForCandidate(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          candidateHiringProcessBO.setCandidateHiringDAO(candidateHiringDAO);
          when(candidateHiringDAO.getHiringProcess(21)).thenReturn(hiringProcessCandidate);

          assert (hiringProcessCandidate != null);
          verify(candidateHiringDAO).getHiringProcess(21);
        });
  }

  public void getStatusForCandidate(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          candidateHiringProcessBO.setCandidateHiringDAO(candidateHiringDAO);
          when(candidateHiringDAO.getCandidateProcess(21, "Offer")).thenReturn(
              String.valueOf(hiringProcessCandidate.getCandidateProcess()));
          assert (hiringProcessCandidate.getCandidateProcess() != null);
          verify(candidateHiringDAO).getCandidateProcess(21, "Offer");
        });
  }

  @After
  public void close() {
    System.out.println("After Method Hiring");
  }
}
