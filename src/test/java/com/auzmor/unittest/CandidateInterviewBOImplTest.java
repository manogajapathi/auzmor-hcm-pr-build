package com.auzmor.unittest;

import com.auzmor.dao.CandidateHiringDAO;
import com.auzmor.dao.CandidateInterviewDAO;
import com.auzmor.impl.bo.CandidateInterviewBOImpl;
import com.auzmor.impl.enumarator.bo.CandidateInterviewEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.models.CandidateR.HiringProcessCandidate;
import com.auzmor.impl.models.CandidateR.InterviewProcessCandidate;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.ext.web.RoutingContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CandidateInterviewBOImplTest {
  @Mock
  private CandidateInterviewDAO candidateInterviewDAO;
  @Mock
  private CandidateHiringDAO candidateHiringDAO;
  @Mock
  private CandidateInterviewBOImpl candidateInterviewBO;
  private InterviewProcessCandidate interviewProcessCandidate;
  private HiringProcessCandidate hiringProcessCandidate;


  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    interviewProcessCandidate = new InterviewProcessCandidate();
    hiringProcessCandidate = new HiringProcessCandidate();
  }

  
  public void addInterviewProcess_Sucess(Vertx vertx) throws SQLException, DbHandleException, ParseException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {

          candidateInterviewBO.setCandidateInterviewDAO(candidateInterviewDAO);
          MultiMap multiMap = null;
          when(candidateInterviewDAO.create(interviewProcessCandidate)).thenReturn(1);
          when(candidateInterviewDAO.updateActiveStatus(1)).thenReturn(1);
          try {
            long result = candidateInterviewBO.addInterviewProcess(multiMap);
            assert (result != 0);
          } catch (ParseException e) {
            e.printStackTrace();
          }
          verify(candidateInterviewDAO).create(interviewProcessCandidate);
          verify(candidateInterviewDAO).updateActiveStatus(1);
        });
  }

  
  public void addInterviewProcess_Fail(Vertx vertx) throws SQLException, DbHandleException, ParseException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {

          candidateInterviewBO.setCandidateInterviewDAO(candidateInterviewDAO);
          MultiMap multiMap = null;
          when(candidateInterviewDAO.create(interviewProcessCandidate)).thenReturn(0);
          when(candidateInterviewDAO.updateActiveStatus(1)).thenReturn(0);
          try {
            long result = candidateInterviewBO.addInterviewProcess(multiMap);
            assert (result == 0);
          } catch (ParseException e) {
            e.printStackTrace();
          }
          verify(candidateInterviewDAO).create(interviewProcessCandidate);
          verify(candidateInterviewDAO).updateActiveStatus(1);
        });
  }



  public void updateInterviewProcess(Vertx vertx) throws SQLException, DbHandleException, ParseException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {

          candidateInterviewBO.setCandidateInterviewDAO(candidateInterviewDAO);
          candidateInterviewBO.setCandidateHiringDAO(candidateHiringDAO);
          when(candidateInterviewDAO.updateCancelStatus("Need Java", "Not Hired",
              (long) 1, (long) 1)).thenReturn(1);
          when(candidateInterviewDAO.updateStatus("Not Hired", 1)).thenReturn(1);
          when(candidateHiringDAO.create(hiringProcessCandidate)).thenReturn(1);
          MultiMap multiMap = null;
          RoutingContext routingContext = null;
          CandidateInterviewEnum result = candidateInterviewBO.updateInterviewProcess(multiMap,routingContext);
          assert (result == CandidateInterviewEnum.SUCCESS);
          verify(candidateInterviewDAO.updateCancelStatus("Need Java", "Not Hired",
              (long) 1, (long) 1));
          verify(candidateInterviewDAO.updateStatus("Not Hired", 1));
          verify(candidateHiringDAO.create(hiringProcessCandidate));
        });
  }

  
  public void updateInterviewProcess_Fail(Vertx vertx) throws SQLException, DbHandleException, ParseException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {

          candidateInterviewBO.setCandidateInterviewDAO(candidateInterviewDAO);
          candidateInterviewBO.setCandidateHiringDAO(candidateHiringDAO);
          when(candidateInterviewDAO.updateCancelStatus("Need Java", "Not Hired",
              (long) 1, (long) 1)).thenReturn(1);
          when(candidateInterviewDAO.updateStatus("Not Hired", 1)).thenReturn(1);
          when(candidateHiringDAO.create(hiringProcessCandidate)).thenReturn(1);
          MultiMap multiMap = null;
          RoutingContext routingContext = null;
          CandidateInterviewEnum result = candidateInterviewBO.updateInterviewProcess(multiMap,routingContext);
          assert (result == CandidateInterviewEnum.FAILED);
          verify(candidateInterviewDAO.updateCancelStatus("Need Java",
              "Not Hired", (long) 1, (long) 1));
          verify(candidateInterviewDAO.updateStatus("Not Hired", 1));
          verify(candidateHiringDAO.create(hiringProcessCandidate));
        });
  }

  
  public void getInterviewDetails_Sucess(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {

          candidateInterviewBO.setCandidateInterviewDAO(candidateInterviewDAO);
          when(candidateInterviewDAO.getInterviewProcessDetails(1)).thenReturn(interviewProcessCandidate);
          interviewProcessCandidate = candidateInterviewBO.getInterviewDetails(1);
          assertNotNull(interviewProcessCandidate);
          verify(candidateInterviewDAO).getInterviewProcessDetails(1);
        });
  }

  
  public void getInterviewDetails_Fail(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          candidateInterviewBO = new CandidateInterviewBOImpl(dbHandle);
          candidateInterviewBO.setCandidateInterviewDAO(candidateInterviewDAO);
          when(candidateInterviewDAO.getInterviewProcessDetails(1)).thenReturn(interviewProcessCandidate);
          interviewProcessCandidate = candidateInterviewBO.getInterviewDetails(1);
          assertNull(interviewProcessCandidate);
          verify(candidateInterviewDAO).getInterviewProcessDetails(1);
        });
  }

  
  public void getListOfInterviewDetails_Sucess(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          StringBuilder str = new StringBuilder("interview_date");
          List<HashMap<String, String>> list = null;
          List<String> fieldValue = new ArrayList<>();
          fieldValue.add("interview_date");
          candidateInterviewBO.setCandidateInterviewDAO(candidateInterviewDAO);
          when(candidateInterviewDAO.getListOfInterviewProcess("MM/dd/yyyy", str, (long) 1, 0,
              1, fieldValue,2)).thenReturn(list);
          list = candidateInterviewBO.getListOfInterviewDetails("MM/dd/yyyy", "interview_date",
              1, 0, 1,2);
          HashMap<String,String> dumpData = new HashMap<>();
          dumpData.put("interview_date", "30-Jan-2018");
          dumpData.put("interview_time", "08:00 pm");
          dumpData.put("interview_duration", "45 minutes");
          dumpData.put("job_title", "Developer");
          dumpData.put("interview_location", "Web");
          dumpData.put("first_name", "John");
          dumpData.put("last_name", "Smith");
          dumpData.put("candidate_id", "0");
          dumpData.put("job_id", "0");
          list.add(dumpData);
          assertNotNull(list);
          verify(candidateInterviewDAO).getListOfInterviewProcess("MM/dd/yyyy", str, (long) 1, 0,
              1, fieldValue,2);
        });
  }

  
  public void getListOfInterviewDetails_Fail(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          StringBuilder str = new StringBuilder("interview_date");
          List<HashMap<String, String>> list = null;
          List<String> fieldValue = new ArrayList<>();
          fieldValue.add("interview_date");
          candidateInterviewBO = new CandidateInterviewBOImpl(dbHandle);
          candidateInterviewBO.setCandidateInterviewDAO(candidateInterviewDAO);
          when(candidateInterviewDAO.getListOfInterviewProcess("MM/dd/yyyy", str, (long) 1,
              0, 1, fieldValue,2)).thenReturn(list);
          list = candidateInterviewBO.getListOfInterviewDetails("MM/dd/yyyy","interview_date",
              1, 0, 1,2);
          HashMap<String,String> dumpData = new HashMap<>();
          dumpData.put("interview_date", "30-Jan-2018");
          dumpData.put("interview_time", "08:00 pm");
          dumpData.put("interview_duration", "45 minutes");
          dumpData.put("job_title", "Developer");
          dumpData.put("interview_location", "Web");
          dumpData.put("first_name", "John");
          dumpData.put("last_name", "Smith");
          dumpData.put("candidate_id", "0");
          dumpData.put("job_id", "0");
          list.add(dumpData);
          assertNull(list);
          verify(candidateInterviewDAO).getListOfInterviewProcess(null, str, (long) 1, 0,
              1, anyList(),2);
        });
  }

  
  public void getInterviewMailDetails_Sucess(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          List<HashMap<String, String>> list = null;
          candidateInterviewBO.setCandidateInterviewDAO(candidateInterviewDAO);
          when(candidateInterviewDAO.getInterviewMailList(1)).thenReturn(list);
          list = candidateInterviewBO.getInterviewMailDetails(1);
          assertNotNull(list);
          verify(candidateInterviewDAO).getInterviewMailList(1);
        });
  }

  
  public void getInterviewMailDetails_Fail(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          List<HashMap<String, String>> list = null;
          candidateInterviewBO = new CandidateInterviewBOImpl(dbHandle);
          candidateInterviewBO.setCandidateInterviewDAO(candidateInterviewDAO);
          when(candidateInterviewDAO.getInterviewMailList(1)).thenReturn(list);
          list = candidateInterviewBO.getInterviewMailDetails(1);
          assertNull(list);
          verify(candidateInterviewDAO).getInterviewMailList(1);
        });
  }

  @After
  public void close() {
    System.out.println("After Method");
  }

}
