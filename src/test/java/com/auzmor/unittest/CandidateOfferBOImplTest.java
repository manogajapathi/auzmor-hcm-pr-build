package com.auzmor.unittest;

import com.auzmor.dao.CandidateOfferDAO;
import com.auzmor.impl.bo.CandidateOfferBOImpl;
import com.auzmor.impl.enumarator.bo.CandidateOfferEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.models.CandidateR.OfferProcessCandidate;
import com.auzmor.impl.util.Mail;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.ext.web.RoutingContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CandidateOfferBOImplTest {
  @Mock
  private CandidateOfferDAO candidateOfferDAO;
  @Mock
  private Mail mail;
  private CandidateOfferBOImpl candidateOfferBO;
  private OfferProcessCandidate offerProcessCandidate;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    offerProcessCandidate = new OfferProcessCandidate();
  }

  
  public void addOfferProcess_Sucess(Vertx vertx) throws SQLException, DbHandleException {
DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          MultiMap multiMap = null;
          candidateOfferBO = new CandidateOfferBOImpl(dbHandle);
          when(candidateOfferDAO.create(offerProcessCandidate)).thenReturn(1);
          long result = candidateOfferBO.addOfferProcess(multiMap);
          assert (result != 0);
          verify(candidateOfferDAO.create(offerProcessCandidate));
        });
  }

  
  public void addOfferProcess_Fail(Vertx vertx) throws SQLException, DbHandleException {
DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          MultiMap multiMap = null;
          candidateOfferBO = new CandidateOfferBOImpl(dbHandle);
          when(candidateOfferDAO.create(offerProcessCandidate)).thenReturn(0);
          long result = candidateOfferBO.addOfferProcess(multiMap);
          assert (result == 0);
          verify(candidateOfferDAO.create(offerProcessCandidate));
        });
  }

  
  public void updateOfferProcess_Sucess(Vertx vertx) throws SQLException, DbHandleException {
DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          MultiMap multiMap = null;
          RoutingContext routingContext = null;
          candidateOfferBO = new CandidateOfferBOImpl(dbHandle);
          when(candidateOfferDAO.updateStatus("Not Hired", 1)).thenReturn(1);
          CandidateOfferEnum result = candidateOfferBO.updateOfferProcess(multiMap,routingContext);
          assert (result == CandidateOfferEnum.SUCCESS);
          verify(candidateOfferDAO.updateStatus("Not Hired", 1));
        });
  }

  
  public void updateOfferProcess_Fail(Vertx vertx) throws SQLException, DbHandleException {
DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          MultiMap multiMap = null;
          RoutingContext routingContext = null;
          candidateOfferBO = new CandidateOfferBOImpl(dbHandle);
          when(candidateOfferDAO.updateStatus("Not Hired", 1)).thenReturn(0);
          CandidateOfferEnum result = candidateOfferBO.updateOfferProcess(multiMap,routingContext);
          assert (result == CandidateOfferEnum.FAILED);
          verify(candidateOfferDAO.updateStatus("Not Hired", 1));
        });
  }


  
  public void getOfferDetails_Sucess(Vertx vertx) throws SQLException, DbHandleException {
DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          MultiMap multiMap = null;
          candidateOfferBO = new CandidateOfferBOImpl(dbHandle);
          when(candidateOfferDAO.getOfferProcessDetails(1)).thenReturn(offerProcessCandidate);
          offerProcessCandidate = candidateOfferBO.getOfferDetails(1);
          assertNotNull(offerProcessCandidate);
          verify(candidateOfferDAO.create(offerProcessCandidate));
        });
  }

  
  public void getOfferDetails_Fail(Vertx vertx) throws SQLException, DbHandleException {
DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          MultiMap multiMap = null;
          candidateOfferBO = new CandidateOfferBOImpl(dbHandle);
          when(candidateOfferDAO.getOfferProcessDetails(1)).thenReturn(offerProcessCandidate);
          offerProcessCandidate = candidateOfferBO.getOfferDetails(1);
          assertNull(offerProcessCandidate);
          verify(candidateOfferDAO.create(offerProcessCandidate));
        });
  }

  
  public void mailSent(Vertx vertx) throws SQLException, DbHandleException {
DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          mail = new Mail();
          candidateOfferBO = new CandidateOfferBOImpl(dbHandle);
          when(mail.offerLetter("","xyz@gmail.com", "XYZ", offerProcessCandidate,
              "Auzmore", "https://ats.auzmor-hcm.com/auth/login/", vertx,2, 170)).thenReturn("sucess");
          CandidateOfferEnum result = candidateOfferBO.mailSent(1,"");
          assert (result == CandidateOfferEnum.SUCCESS);
          verify(mail.offerLetter("","xyz@gmail.com", "XYZ", offerProcessCandidate,
              "Auzmore", "https://ats.auzmor-hcm.com/auth/login/", vertx,2,170));
        });
  }

  
  public void mailSent_Fail(Vertx vertx) throws SQLException, DbHandleException {
DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          mail = new Mail();
          candidateOfferBO = new CandidateOfferBOImpl(dbHandle);
          when(mail.offerLetter("","xyz@gmail.com", "XYZ", offerProcessCandidate,
              "Auzmore", "https://ats.auzmor-hcm.com/auth/login/", vertx,2,170)).thenReturn("sucess");
          CandidateOfferEnum result = candidateOfferBO.mailSent(1,"");
          assert (result == CandidateOfferEnum.SEND_FAILED);
          verify(mail.offerLetter("","xyz@gmail.com", "XYZ", offerProcessCandidate,
              "Auzmore", "https://ats.auzmor-hcm.com/auth/login/", vertx,2,170));
        });
  }

  
  public void getOfferMailDetails_Sucess(Vertx vertx) throws SQLException, DbHandleException {
DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          List<HashMap<String, String>> list = null;
          MultiMap multiMap = null;
          candidateOfferBO = new CandidateOfferBOImpl(dbHandle);
          when(candidateOfferDAO.getOfferMailList(1)).thenReturn(list);
          list = candidateOfferBO.getOfferMailDetails(1);
          assertNotNull(list);
          verify(candidateOfferDAO.create(offerProcessCandidate));
        });
  }

  
  public void getOfferMailDetails_Fail(Vertx vertx) throws SQLException, DbHandleException {
DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          List<HashMap<String, String>> list = null;
          MultiMap multiMap = null;
          candidateOfferBO = new CandidateOfferBOImpl(dbHandle);
          when(candidateOfferDAO.getOfferMailList(1)).thenReturn(list);
          list = candidateOfferBO.getOfferMailDetails(1);
          assertNull(list);
          verify(candidateOfferDAO.create(offerProcessCandidate));
        });
  }


  @After
  public void close() {
    System.out.println("After Method");
  }

}
