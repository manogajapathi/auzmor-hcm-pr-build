package com.auzmor.unittest;

import com.auzmor.bo.OnboardingFormBO;
import com.auzmor.dao.OnboardingFormDAO;
import com.auzmor.impl.bo.OnboardingFormBOImpl;
import com.auzmor.impl.enumarator.bo.OnboardingFormEnum;
import com.auzmor.impl.model.onboarding.OnboardingForm;
import com.auzmor.util.bo.ReturnObject;
import io.vertx.core.MultiMap;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.exception.db.DbHandleException;
import io.vertx.core.json.JsonObject;
import io.vertx.core.Vertx;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.sql.SQLException;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class OnboardingFormBOImplTest {

  @Mock
  OnboardingFormDAO onboardingFormDAO;
  private OnboardingFormBOImpl onboardingFormBO;
  private OnboardingForm onboardingForm;
  MultiMap requestParams;
  ReturnObject<OnboardingFormEnum, JsonObject> returnObj;


  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    onboardingForm = new OnboardingForm();
  }


  public void addForm(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          onboardingFormBO = new OnboardingFormBOImpl(dbHandle);
          onboardingFormBO.setOnboardingFormDAO(onboardingFormDAO);

          when(onboardingFormDAO.addForm(onboardingForm)).thenReturn(new Integer(1));

          ReturnObject<OnboardingFormEnum, JsonObject> addcount =
              onboardingFormBO.addForm(dbHandle, requestParams);

          assert (addcount == returnObj);
          verify(onboardingFormDAO).addForm(onboardingForm);
        }
    );
  }


  public void notAddForm(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          onboardingFormBO = new OnboardingFormBOImpl(dbHandle);
          onboardingFormBO.setOnboardingFormDAO(onboardingFormDAO);

          when(onboardingFormDAO.addForm(onboardingForm)).thenReturn(new Integer(0));

          ReturnObject<OnboardingFormEnum, JsonObject> addcoount =
              onboardingFormBO.addForm(dbHandle, requestParams);

          assert (addcoount == returnObj);
          verify(onboardingFormDAO).addForm(onboardingForm);
        }
    );
  }

  public void getForm(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
        onboardingFormBO = new OnboardingFormBOImpl(dbHandle);
        onboardingFormBO.setOnboardingFormDAO(onboardingFormDAO);

        when(onboardingFormDAO.getForm(onboardingForm)).thenReturn(JsonObject.mapFrom(onboardingForm));

        assert (onboardingForm!=null);
        verify(onboardingFormDAO).getForm(onboardingForm);
        });
  }


  public void notGetForm(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          onboardingFormBO = new OnboardingFormBOImpl(dbHandle);
          onboardingFormBO.setOnboardingFormDAO(onboardingFormDAO);

          when(onboardingFormDAO.getForm(onboardingForm)).thenReturn(JsonObject.mapFrom(onboardingForm));

          assert (onboardingForm == null);
          verify(onboardingFormDAO).getForm(onboardingForm);
        });
  }


  public void deleteForm(Vertx vertx) throws  SQLException,DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          onboardingFormBO = new OnboardingFormBOImpl(dbHandle);
          onboardingFormBO.setOnboardingFormDAO(onboardingFormDAO);

          when(onboardingFormDAO.deleteForm(onboardingForm)).thenReturn(new Integer(1));

          ReturnObject<OnboardingFormEnum, JsonObject> deleteCount =
              onboardingFormBO.deleteForm(dbHandle, requestParams);
          assert (deleteCount == returnObj);
          verify(onboardingFormDAO).deleteForm(onboardingForm);

        });
  }


  public void notDeleteForm(Vertx vertx) throws SQLException,DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          onboardingFormBO = new OnboardingFormBOImpl(dbHandle);
          onboardingFormBO.setOnboardingFormDAO(onboardingFormDAO);

          when(onboardingFormDAO.deleteForm(onboardingForm)).thenReturn(new Integer(0));

          ReturnObject<OnboardingFormEnum, JsonObject> deleteCount =
              onboardingFormBO.deleteForm(dbHandle, requestParams);
          assert (deleteCount == returnObj);
          verify(onboardingFormDAO).deleteForm(onboardingForm);

        });
  }

  public void updateForm(Vertx vertx) throws  SQLException,DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          onboardingFormBO = new OnboardingFormBOImpl(dbHandle);
          onboardingFormBO.setOnboardingFormDAO(onboardingFormDAO);

          when(onboardingFormDAO.updateForm(requestParams)).thenReturn(new Integer(1));

          ReturnObject<OnboardingFormEnum, JsonObject> deleteCount =
              onboardingFormBO.deleteForm(dbHandle, requestParams);
          assert (deleteCount == returnObj);
          verify(onboardingFormDAO).updateForm(requestParams);

        });
  }

  public void notUpdateForm(Vertx vertx) throws  SQLException,DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          onboardingFormBO = new OnboardingFormBOImpl(dbHandle);
          onboardingFormBO.setOnboardingFormDAO(onboardingFormDAO);

          when(onboardingFormDAO.updateForm(requestParams)).thenReturn(new Integer(0));

          ReturnObject<OnboardingFormEnum, JsonObject> deleteCount =
              onboardingFormBO.deleteForm(dbHandle, requestParams);
          assert (deleteCount == returnObj);
          verify(onboardingFormDAO).updateForm(requestParams);

        });
  }

  public void addOnboardingFormThrowsException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          onboardingFormBO = new OnboardingFormBOImpl(dbHandle);
          onboardingFormBO.setOnboardingFormDAO(onboardingFormDAO);
          when(onboardingFormDAO.addForm(onboardingForm)).thenThrow(SQLException.class);

          ReturnObject <OnboardingFormEnum, JsonObject> insertCount = onboardingFormBO.addForm(dbHandle,requestParams);
        });
  }
}
