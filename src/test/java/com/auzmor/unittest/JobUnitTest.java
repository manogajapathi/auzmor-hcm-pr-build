package com.auzmor.unittest;

import com.auzmor.impl.handler.JobPostHandler;
import com.auzmor.impl.model.Job.Collaborator;
import com.auzmor.impl.model.Job.CustomQuestion;
import com.auzmor.impl.model.Job.Job;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//@RunWith(VertxUnitRunner.class)
public class JobUnitTest {

  
  public void testPostJobWithCustomQuestionsAndCollaborators()
      throws SQLException, ClassNotFoundException {

    Job job = new Job();
    job.setOrganization_id(1);
    job.setInternal_job_code("");
    job.setTitle("Tester");
    job.setHiring_lead("");
    job.setEmployment_type("");
    job.setLocation("Chennai");
    job.setCity("");
    job.setZip("");
    job.setJob_status("Published");
    job.setDepartment("");
    job.setMinimum_experience("");
    job.setCountry("");
    job.setState("");
    job.setCompensation("");
    job.setJob_description("This is test");
    job.setJob_desc_template("");
    job.setCompany_type("");
    job.setApplicant("");
    job.setInterview("");
    job.setOffer("");
    job.setHire("");
    job.setResume(true);
    job.setBlog_required(true);
    List<CustomQuestion> customQuestionList  = new ArrayList<>();
    for(int i = 0; i < 3; i++) {
      CustomQuestion customQuestion = new CustomQuestion();
      customQuestion.setCustomQuestion("Is this a Custom Question " + i + "?");
      customQuestion.setCustomAnswer("Custom Answer " + i);
      customQuestion.setCustomOption2("Opt 2");
      customQuestion.setCustomOption1("Opt 1");
      customQuestion.setCustomKnockout(false);
      customQuestionList.add(customQuestion);
    }

    job.setCustomQuestionModels(customQuestionList);

    Collaborator collaborator = new Collaborator();
    collaborator.setCollaborator("New Collaborator");

    List<Collaborator> collaboratorList = new ArrayList<>();
    collaboratorList.add(collaborator);

    job.setCollaborators(collaboratorList);

    JobPostHandler postHandler = new JobPostHandler();

    Assert.assertEquals(1, postHandler.addJobDetails(job));

  }


  
  public void testPostJobWithoutCustomQuestionsAndCollaborators()
      throws SQLException, ClassNotFoundException {

    Job job = new Job();
    job.setOrganization_id(1);
    job.setInternal_job_code("");
    job.setTitle("Tester");
    job.setHiring_lead("");
    job.setEmployment_type("");
    job.setLocation("Chennai");
    job.setCity("");
    job.setZip("");
    job.setJob_status("Published");
    job.setDepartment("");
    job.setMinimum_experience("");
    job.setCountry("");
    job.setState("");
    job.setCompensation("");
    job.setJob_description("This is test");
    job.setJob_desc_template("");
    job.setCompany_type("");
    job.setApplicant("");
    job.setInterview("");
    job.setOffer("");
    job.setHire("");
    job.setResume(true);
    job.setBlog_required(true);

    JobPostHandler postHandler = new JobPostHandler();

    Assert.assertEquals(1, postHandler.addJobDetails(job));

  }

  
  public void testPostJobWithRequiredFields() throws SQLException, ClassNotFoundException {
    Job job = new Job();
    job.setOrganization_id(1);
    job.setTitle("Tester");
    job.setLocation("Chennai");
    job.setJob_status("Published");
    job.setJob_description("This is test");
    JobPostHandler postHandler = new JobPostHandler();

    Assert.assertEquals(1, postHandler.addJobDetails(job));

  }



}
