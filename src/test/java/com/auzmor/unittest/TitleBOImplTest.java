package com.auzmor.unittest;

import com.auzmor.bo.TitleBO;
import com.auzmor.dao.TitleDAO;
import com.auzmor.impl.bo.TitleBOImpl;
import com.auzmor.impl.dao.TitleDAOImpl;
import com.auzmor.impl.enumarator.TitleEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.TitleModel;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.util.bo.ReturnObject;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.isNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TitleBOImplTest {
  @Mock
  TitleDAO titleDAO;
  @Mock
  private TitleBO titleBO;
  private TitleModel titleModel;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    titleModel = new TitleModel();
  }

  
  public void createTitle(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          titleModel.setTitle("New Title");
          titleModel.setOrganizationId(1);
          titleModel.setActiveStatus("true");
          when(titleDAO.insert(titleModel)).thenReturn(1);

          ReturnObject<TitleEnum, JsonObject> boReponse =
              titleBO.addTitle(dbHandle, titleModel);

          assert (boReponse.getStatusEnum() == TitleEnum.ADDITION_SUCCESS);
          verify(titleDAO).insert(titleModel);
        }
    );
  }

  
  public void createTitleNoValues(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          titleBO = new TitleBOImpl();
          titleDAO = new TitleDAOImpl(dbHandle);
          when(titleDAO.insert(titleModel)).thenReturn(new Integer(0));

          ReturnObject<TitleEnum, JsonObject> boReponse =
              titleBO.addTitle(dbHandle, titleModel);

          assert (boReponse.getStatusEnum() == TitleEnum.ADDITION_SUCCESS);
          verify(titleDAO).insert(titleModel);
        }
    );
  }

  
  public void createTitleThrowsException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          titleBO = new TitleBOImpl();
          titleDAO = new TitleDAOImpl(dbHandle);
          when(titleDAO.insert(titleModel)).thenThrow(SQLException.class);

          ReturnObject<TitleEnum, JsonObject> boReponse =
              titleBO.addTitle(dbHandle, titleModel);

          assert (boReponse.getStatusEnum() == TitleEnum.ADDITION_SUCCESS);
          verify(titleDAO).insert(titleModel);
        }
    );
  }

  
  public void getAllTitlesThrowsException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          titleBO = new TitleBOImpl();
          titleDAO = new TitleDAOImpl(dbHandle);
          List<TitleModel> titleList = new ArrayList<>();
          when(titleDAO.getAllTitles(titleModel)).thenThrow(SQLException.class);

          ReturnObject<TitleEnum, JsonArray> boReponse =
              titleBO.getAllTitles(dbHandle, titleModel);
        }
    );
  }

  
  public void getAllTitles(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          titleBO = new TitleBOImpl();
          titleDAO = new TitleDAOImpl(dbHandle);
          List<TitleModel> titleList = new ArrayList<>();
          when(titleDAO.getAllTitles(titleModel)).thenReturn(titleList);

          ReturnObject<TitleEnum, JsonArray> boReponse =
              titleBO.getAllTitles(dbHandle, titleModel);

          assert (boReponse.getStatusEnum() == TitleEnum.VALUE_SUCCESS);
          verify(titleDAO).getAllTitles(titleModel);
        }
    );
  }

  
  public void getAllTitlesNoSuchValues(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          titleBO = new TitleBOImpl();
          titleDAO = new TitleDAOImpl(dbHandle);
          List<TitleModel> titleList = new ArrayList<>();
          when(titleDAO.getAllTitles(titleModel)).thenReturn(titleList);

          ReturnObject<TitleEnum, JsonArray> boReponse =
              titleBO.getAllTitles(dbHandle, titleModel);

          assert (boReponse.getStatusEnum() == TitleEnum.NO_SUCH_VALUE);
          verify(titleDAO).getAllTitles(titleModel);
        }
    );
  }

  
  public void getTitle(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          titleBO = new TitleBOImpl();
          titleDAO = new TitleDAOImpl(dbHandle);
          when(titleDAO.getTitleDetails(titleModel)).thenReturn(titleModel);

          ReturnObject<TitleEnum, JsonObject> boReponse =
              titleBO.getTitle(dbHandle, titleModel);

          assert (boReponse.getStatusEnum() == TitleEnum.VALUE_SUCCESS);
          verify(titleDAO).getTitleDetails(titleModel);
        }
    );
  }

  
  public void getTitleNoSuchValues(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          titleBO = new TitleBOImpl();
          titleDAO = new TitleDAOImpl(dbHandle);
          when(titleDAO.getTitleDetails(titleModel)).thenReturn(titleModel);

          ReturnObject<TitleEnum, JsonObject> boReponse =
              titleBO.getTitle(dbHandle, titleModel);

          assert (boReponse.getStatusEnum() == TitleEnum.NO_SUCH_VALUE);
          verify(titleDAO).getTitleDetails(titleModel);
        }
    );
  }

  
  public void getTitleThrowException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          titleBO = new TitleBOImpl();
          titleDAO = new TitleDAOImpl(dbHandle);
          when(titleDAO.getTitleDetails(titleModel)).thenThrow(SQLException.class);

          ReturnObject<TitleEnum, JsonObject> boReponse =
              titleBO.getTitle(dbHandle, titleModel);
        }
    );
  }

  
  public void updateTitle(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          titleBO = new TitleBOImpl();
          titleDAO = new TitleDAOImpl(dbHandle);
          MultiMap params = null;
          when(titleDAO.updateTitleDetails(params)).thenReturn(new Integer(1));

          ReturnObject<TitleEnum, JsonObject> boReponse =
              titleBO.updateTitle(dbHandle, params);

          assert (boReponse.getStatusEnum() == TitleEnum.UPDATE_SUCCESS);
          verify(titleBO).updateTitle(dbHandle, params);
        }
    );
  }

  
  public void updateTitleNoSuchValues(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          titleBO = new TitleBOImpl();
          titleDAO = new TitleDAOImpl(dbHandle);
          MultiMap params = null;
          when(titleDAO.updateTitleDetails(params)).thenReturn(new Integer(0));

          ReturnObject<TitleEnum, JsonObject> boReponse =
              titleBO.updateTitle(dbHandle, params);

          assert (boReponse.getStatusEnum() == TitleEnum.UPDATE_SUCCESS);
          verify(titleBO).updateTitle(dbHandle, params);
        }
    );
  }

  
  public void updateTitleThrowsException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          titleBO = new TitleBOImpl();
          titleDAO = new TitleDAOImpl(dbHandle);
          MultiMap params = null;
          when(titleDAO.updateTitleDetails(params)).thenThrow(SQLException.class);

          ReturnObject<TitleEnum, JsonObject> boReponse =
              titleBO.updateTitle(dbHandle, params);
        }
    );
  }

  
  public void deleteTitle(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          titleBO = new TitleBOImpl();
          titleDAO = new TitleDAOImpl(dbHandle);
          when(titleDAO.deleteTitle(titleModel)).thenReturn(new Integer(1));

          ReturnObject<TitleEnum, JsonObject> boReponse =
              titleBO.deleteTitle(dbHandle, titleModel);

          assert (boReponse.getStatusEnum() == TitleEnum.DELETE_SUCCESS);
          verify(titleDAO).deleteTitle(titleModel);
        }
    );
  }

  
  public void deleteTitleNoSuchValues(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          titleBO = new TitleBOImpl();
          titleDAO = new TitleDAOImpl(dbHandle);
          when(titleDAO.deleteTitle(titleModel)).thenReturn(new Integer(0));

          ReturnObject<TitleEnum, JsonObject> boReponse =
              titleBO.deleteTitle(dbHandle, titleModel);

          assert (boReponse.getStatusEnum() == TitleEnum.DELETE_SUCCESS);
          verify(titleDAO).deleteTitle(titleModel);
        }
    );
  }

  
  public void deleteTitleThrowsException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          titleBO = new TitleBOImpl();
          titleDAO = new TitleDAOImpl(dbHandle);
          when(titleDAO.deleteTitle(titleModel)).thenThrow(SQLException.class);

          ReturnObject<TitleEnum, JsonObject> boReponse =
              titleBO.deleteTitle(dbHandle, titleModel);
        }
    );
  }
}
