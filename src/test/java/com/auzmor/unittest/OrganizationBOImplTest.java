package com.auzmor.unittest;

import com.auzmor.impl.bo.OrganizationBOImpl;
import com.auzmor.impl.dao.OrganizationDAOImpl;
import com.auzmor.impl.enumarator.bo.OrganizationBOEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.Organization;
import com.auzmor.impl.model.OrganizationR;
import io.vertx.core.Vertx;
import org.junit.Before;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class OrganizationBOImplTest {
  @Mock
  OrganizationDAOImpl organizationDAO;
  private OrganizationBOImpl organizationBO;
  private Organization organization;
  private OrganizationR organizationR;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    organization = new Organization();
    organizationR = new OrganizationR();
  }

  public void getOrganizationDetails_Success(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          organizationBO = new OrganizationBOImpl(dbHandle);
          when(organizationDAO.getOrganizationById(1)).thenReturn(organization);
          Organization result = organizationBO.getOrganizationDetails((long) 1);
          assertNotNull(result);
          verify(organizationDAO.getOrganizationById(1));
        });
  }

  public void getOrganizationDetails_Fail(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          organizationBO = new OrganizationBOImpl(dbHandle);
          when(organizationDAO.getOrganizationById(1)).thenReturn(organization);
          Organization result = organizationBO.getOrganizationDetails((long) 1);
          assertNull(result);
          verify(organizationDAO.getOrganizationById(1));
        });
  }

  public void getOrganizationWithDomainName_Success(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          organizationBO = new OrganizationBOImpl(dbHandle);
          when(organizationDAO.getOrganizationByDomainName("")).thenReturn(organization);
          Organization result = organizationBO.getOrganizationWithDomainName("");
          assertNotNull(result);
          verify(organizationDAO.getOrganizationByDomainName(""));
        });
  }

  public void getOrganizationWithDomainName_Fail(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          organizationBO = new OrganizationBOImpl(dbHandle);
          when(organizationDAO.getOrganizationByDomainName("")).thenReturn(organization);
          Organization result = organizationBO.getOrganizationWithDomainName("");
          assertNotNull(result);
          verify(organizationDAO.getOrganizationByDomainName(""));
        });
  }

  public void isDomainAvailable_Success(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          organizationBO = new OrganizationBOImpl(dbHandle);
          when(organizationDAO.isDomainRegistered("")).thenReturn(true);
          boolean result = organizationBO.isDomainAvailable("");
          assert (result);
          verify(organizationDAO.isDomainRegistered(""));
        });
  }

  public void isDomainAvailable_Fail(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          organizationBO = new OrganizationBOImpl(dbHandle);
          when(organizationDAO.isDomainRegistered("")).thenReturn(true);
          boolean result = organizationBO.isDomainAvailable("");
          assert (result);
          verify(organizationDAO.isDomainRegistered(""));
        });
  }

  public void addNewAtsDomain(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          organizationBO = new OrganizationBOImpl(dbHandle);
          organizationBO.addNewAtsDomain("", (long) 1);
        });
  }

  public void updateDomains(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          organizationBO = new OrganizationBOImpl(dbHandle);
          when(organizationDAO.getOrganizationById(1)).thenReturn(organization);
          when(organizationDAO.updateDomains("", "", (long) 1)).thenReturn(1);
          OrganizationBOEnum result = organizationBO.updateDomains("", "", (long) 1, "", "", "", "");
          assert (result == OrganizationBOEnum.DOMAINS_UPDATE_SUCCESS);
          verify(organizationDAO.isDomainRegistered(""));
        });
  }

  public void addOrganizationDetails_Success(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          organizationBO = new OrganizationBOImpl(dbHandle);
          when(organizationDAO.addOrganizationDetails(organizationR)).thenReturn(1);
          OrganizationBOEnum result = organizationBO.addOrganizationDetails(organizationR);
          assert (result == OrganizationBOEnum.SUCCESSFUL);
          verify(organizationDAO.addOrganizationDetails(organizationR));
        });
  }

  public void addOrganizationDetails_Fail(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          organizationBO = new OrganizationBOImpl(dbHandle);
          when(organizationDAO.addOrganizationDetails(organizationR)).thenReturn(0);
          OrganizationBOEnum result = organizationBO.addOrganizationDetails(organizationR);
          assert (result == null);
          verify(organizationDAO.addOrganizationDetails(organizationR));
        });
  }
}
