package com.auzmor.unittest;

import com.auzmor.bo.MinimumExperienceBO;
import com.auzmor.dao.MinimumExperienceDAO;
import com.auzmor.impl.bo.LocationBOImpl;
import com.auzmor.impl.bo.MinimumExperienceBOImpl;
import com.auzmor.impl.dao.LocationDAOImpl;
import com.auzmor.impl.dao.MinimumExperienceDAOImpl;
import com.auzmor.impl.enumarator.LocationEnum;
import com.auzmor.impl.enumarator.bo.MinimumExperienceEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.FieldOptions.MinimumExperienceModel;
import com.auzmor.util.bo.ReturnObject;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.junit.Before;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MinimumExperienceBOImplTest {
  @Mock
  MinimumExperienceDAO minimumExperienceDAO;
  private MinimumExperienceBO minimumExperienceBO;
  private MinimumExperienceModel minimumExperienceModel;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    minimumExperienceModel = new MinimumExperienceModel();
  }

  public void createMinimumExperience(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          minimumExperienceBO = new MinimumExperienceBOImpl();
          minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
          when(minimumExperienceDAO.insert(minimumExperienceModel)).thenReturn(new Integer(1));

          MultiMap params = null;
          params.add("minimum_experience", "Entry Level");
          params.add("organization_id", "2");

          ReturnObject<MinimumExperienceEnum, JsonObject> boReponse =
              minimumExperienceBO.addMinimumExperience(dbHandle, params);

          assert (boReponse.getStatusEnum() == MinimumExperienceEnum.ADDITION_SUCCESS);
          verify(minimumExperienceDAO).insert(minimumExperienceModel);
        }
    );
  }


  public void createMinimumExperienceNotSuccessful(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          minimumExperienceBO = new MinimumExperienceBOImpl();
          minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
          when(minimumExperienceDAO.insert(minimumExperienceModel)).thenReturn(new Integer(0));

          MultiMap params = null;
          params.add("minimum_experience", "Entry Level");
          params.add("organization_id", "2");

          ReturnObject<MinimumExperienceEnum, JsonObject> boReponse =
              minimumExperienceBO.addMinimumExperience(dbHandle, params);

          assert (boReponse.getStatusEnum() == MinimumExperienceEnum.ADDITION_FAILED);
          verify(minimumExperienceDAO).insert(minimumExperienceModel);
        }
    );
  }


  public void createMinimumExperienceThrowsSQLException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          minimumExperienceBO = new MinimumExperienceBOImpl();
          minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
          when(minimumExperienceDAO.insert(minimumExperienceModel)).thenThrow(SQLException.class);

          MultiMap params = null;
          params.add("minimum_experience", "Entry Level");
          params.add("organization_id", "2");

          ReturnObject<MinimumExperienceEnum, JsonObject> boReponse =
              minimumExperienceBO.addMinimumExperience(dbHandle, params);

          assert (boReponse.getStatusEnum() == MinimumExperienceEnum.ADDITION_FAILED);
          verify(minimumExperienceDAO).insert(minimumExperienceModel);
        }
    );
  }

  public void createMinimumExperienceThrowsDbHandleException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          minimumExperienceBO = new MinimumExperienceBOImpl();
          minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
          when(minimumExperienceDAO.insert(minimumExperienceModel)).thenThrow(DbHandleException.class);

          MultiMap params = null;
          params.add("minimum_experience", "Entry Level");
          params.add("organization_id", "2");

          ReturnObject<MinimumExperienceEnum, JsonObject> boReponse =
              minimumExperienceBO.addMinimumExperience(dbHandle, params);

          assert (boReponse.getStatusEnum() == MinimumExperienceEnum.ADDITION_FAILED);
          verify(minimumExperienceDAO).insert(minimumExperienceModel);
        }
    );
  }

  public void getAllMinimumExperiences(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          minimumExperienceBO = new MinimumExperienceBOImpl();
          minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
          List<JsonObject> experienceModelList = new ArrayList<>();
          JsonObject dataObject = new JsonObject();
          dataObject.put("minimum_experience", "Entry Level");
          dataObject.put("organization_id", 2);
          dataObject.put("id", 5);
          experienceModelList.add(dataObject);
          when(minimumExperienceDAO.getAllMinimumExperience(minimumExperienceModel)).thenReturn(experienceModelList);

          MultiMap params = null;
          params.add("organization_id", "2");

          ReturnObject<MinimumExperienceEnum, JsonArray> boReponse =
              minimumExperienceBO.getAllMinimumExperience(dbHandle, params);

          assert (boReponse.getStatusEnum() == MinimumExperienceEnum.VALUE_SUCCESS);
          verify(minimumExperienceDAO).getAllMinimumExperience(minimumExperienceModel);
        }
    );
  }

  public void getAllMinimumExperiencesNoValues(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          minimumExperienceBO = new MinimumExperienceBOImpl();
          minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
          List<JsonObject> experienceModelList = new ArrayList<>();
          JsonObject dataObject = new JsonObject();
          experienceModelList.add(dataObject);
          when(minimumExperienceDAO.getAllMinimumExperience(minimumExperienceModel)).thenReturn(experienceModelList);

          MultiMap params = null;
          params.add("organization_id", "2");

          ReturnObject<MinimumExperienceEnum, JsonArray> boReponse =
              minimumExperienceBO.getAllMinimumExperience(dbHandle, params);

          assert (boReponse.getStatusEnum() == MinimumExperienceEnum.VALUE_SUCCESS);
          verify(minimumExperienceDAO).getAllMinimumExperience(minimumExperienceModel);
        }
    );
  }

  public void getAllMinimumExperiencesThrowsSQLException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          minimumExperienceBO = new MinimumExperienceBOImpl();
          minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
          List<JsonObject> experienceModelList = new ArrayList<>();
          JsonObject dataObject = new JsonObject();
          experienceModelList.add(dataObject);
          when(minimumExperienceDAO.getAllMinimumExperience(minimumExperienceModel)).thenThrow(SQLException.class);

          MultiMap params = null;
          params.add("organization_id", "2");

          ReturnObject<MinimumExperienceEnum, JsonArray> boReponse =
              minimumExperienceBO.getAllMinimumExperience(dbHandle, params);

          assert (boReponse.getStatusEnum() == MinimumExperienceEnum.VALUE_SUCCESS);
          verify(minimumExperienceDAO).getAllMinimumExperience(minimumExperienceModel);
        }
    );
  }

  public void getAllMinimumExperiencesThrowsDbHandleException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          minimumExperienceBO = new MinimumExperienceBOImpl();
          minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
          List<JsonObject> experienceModelList = new ArrayList<>();
          JsonObject dataObject = new JsonObject();
          experienceModelList.add(dataObject);
          when(minimumExperienceDAO.getAllMinimumExperience(minimumExperienceModel)).thenThrow(DbHandleException.class);

          MultiMap params = null;
          params.add("organization_id", "2");

          ReturnObject<MinimumExperienceEnum, JsonArray> boReponse =
              minimumExperienceBO.getAllMinimumExperience(dbHandle, params);

          assert (boReponse.getStatusEnum() == MinimumExperienceEnum.VALUE_SUCCESS);
          verify(minimumExperienceDAO).getAllMinimumExperience(minimumExperienceModel);
        }
    );
  }

  public void getMinimumExperience(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          minimumExperienceBO = new MinimumExperienceBOImpl();
          minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
          JsonObject dataObject = new JsonObject();
          dataObject.put("minimum_experience", "Entry Level");
          dataObject.put("organization_id", 2);
          dataObject.put("id", 5);
          when(minimumExperienceDAO.getMinimumExperience(minimumExperienceModel)).thenReturn(dataObject);

          MultiMap params = null;
          params.add("id", "5");

          ReturnObject<MinimumExperienceEnum, JsonObject> boReponse =
              minimumExperienceBO.getMinimumExperience(dbHandle, params);

          assert (boReponse.getStatusEnum() == MinimumExperienceEnum.VALUE_SUCCESS);
          verify(minimumExperienceDAO).getMinimumExperience(minimumExperienceModel);
        }
    );
  }

  public void getMinimumExperienceNoSuchValues(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          minimumExperienceBO = new MinimumExperienceBOImpl();
          minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
          JsonObject dataObject = new JsonObject();
          when(minimumExperienceDAO.getMinimumExperience(minimumExperienceModel)).thenReturn(dataObject);

          MultiMap params = null;
          params.add("id", "5");

          ReturnObject<MinimumExperienceEnum, JsonObject> boReponse =
              minimumExperienceBO.getMinimumExperience(dbHandle, params);

          assert (boReponse.getStatusEnum() == MinimumExperienceEnum.VALUE_SUCCESS);
          verify(minimumExperienceDAO).getMinimumExperience(minimumExperienceModel);
        }
    );
  }

  public void getMinimumExperienceThrowsSQLException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          minimumExperienceBO = new MinimumExperienceBOImpl();
          minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
          JsonObject dataObject = new JsonObject();
          when(minimumExperienceDAO.getMinimumExperience(minimumExperienceModel)).thenThrow(SQLException.class);

          MultiMap params = null;
          params.add("id", "5");

          ReturnObject<MinimumExperienceEnum, JsonObject> boReponse =
              minimumExperienceBO.getMinimumExperience(dbHandle, params);

          assert (boReponse.getStatusEnum() == MinimumExperienceEnum.VALUE_SUCCESS);
          verify(minimumExperienceDAO).getMinimumExperience(minimumExperienceModel);
        }
    );
  }

  public void getMinimumExperienceThrowsDbHandleException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          minimumExperienceBO = new MinimumExperienceBOImpl();
          minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
          JsonObject dataObject = new JsonObject();
          when(minimumExperienceDAO.getMinimumExperience(minimumExperienceModel)).thenThrow(DbHandleException.class);

          MultiMap params = null;
          params.add("id", "5");

          ReturnObject<MinimumExperienceEnum, JsonObject> boReponse =
              minimumExperienceBO.getMinimumExperience(dbHandle, params);

          assert (boReponse.getStatusEnum() == MinimumExperienceEnum.VALUE_SUCCESS);
          verify(minimumExperienceDAO).getMinimumExperience(minimumExperienceModel);
        }
    );
  }

  public void updateMinimumExperience(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          minimumExperienceBO = new MinimumExperienceBOImpl();
          minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
          MultiMap params = null;
          minimumExperienceModel.setMinimumExperience("Entry Level");
          when(minimumExperienceDAO.update(minimumExperienceModel)).thenReturn(new Integer(1));

          params.add("minimum_experience", "Entry Level");
          ReturnObject<MinimumExperienceEnum, JsonObject> boReponse =
              minimumExperienceBO.updateMinimumExperience(dbHandle, params);

          assert (boReponse.getStatusEnum() == MinimumExperienceEnum.UPDATE_SUCCESS);
          verify(minimumExperienceDAO).update(minimumExperienceModel);
        }
    );
  }

  public void updateMinimumExperienceNoChanges(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          minimumExperienceBO = new MinimumExperienceBOImpl();
          minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
          MultiMap params = null;
          minimumExperienceModel.setMinimumExperience("Entry Level");
          when(minimumExperienceDAO.update(minimumExperienceModel)).thenReturn(new Integer(0));

          params.add("minimum_experience", "Entry Level");
          ReturnObject<MinimumExperienceEnum, JsonObject> boReponse =
              minimumExperienceBO.updateMinimumExperience(dbHandle, params);

          assert (boReponse.getStatusEnum() == MinimumExperienceEnum.UPDATE_SUCCESS);
          verify(minimumExperienceDAO).update(minimumExperienceModel);
        }
    );
  }

  public void updateMinimumExperienceThrowsSQLException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          minimumExperienceBO = new MinimumExperienceBOImpl();
          minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
          MultiMap params = null;
          minimumExperienceModel.setMinimumExperience("Entry Level");
          when(minimumExperienceDAO.update(minimumExperienceModel)).thenThrow(SQLException.class);

          params.add("minimum_experience", "Entry Level");
          ReturnObject<MinimumExperienceEnum, JsonObject> boReponse =
              minimumExperienceBO.updateMinimumExperience(dbHandle, params);

          assert (boReponse.getStatusEnum() == MinimumExperienceEnum.UPDATE_SUCCESS);
          verify(minimumExperienceDAO).update(minimumExperienceModel);
        }
    );
  }

  public void updateMinimumExperienceThrowsDbHandleException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          minimumExperienceBO = new MinimumExperienceBOImpl();
          minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
          MultiMap params = null;
          minimumExperienceModel.setMinimumExperience("Entry Level");
          when(minimumExperienceDAO.update(minimumExperienceModel)).thenThrow(DbHandleException.class);

          params.add("minimum_experience", "Entry Level");
          ReturnObject<MinimumExperienceEnum, JsonObject> boReponse =
              minimumExperienceBO.updateMinimumExperience(dbHandle, params);

          assert (boReponse.getStatusEnum() == MinimumExperienceEnum.UPDATE_SUCCESS);
          verify(minimumExperienceDAO).update(minimumExperienceModel);
        }
    );
  }


  public void deleteMinimumExperience(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          minimumExperienceBO = new MinimumExperienceBOImpl();
          minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
          MultiMap params = null;
          minimumExperienceModel.setId(5);
          when(minimumExperienceDAO.delete(minimumExperienceModel)).thenReturn(new Integer(1));

          params.add("id", "5");
          ReturnObject<MinimumExperienceEnum, JsonObject> boReponse =
              minimumExperienceBO.deleteMinimumExperience(dbHandle, params);

          assert (boReponse.getStatusEnum() == MinimumExperienceEnum.UPDATE_SUCCESS);
          verify(minimumExperienceDAO).delete(minimumExperienceModel);
        }
    );
  }

  public void deleteMinimumExperienceWithNoChanges(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          minimumExperienceBO = new MinimumExperienceBOImpl();
          minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
          MultiMap params = null;
          minimumExperienceModel.setId(5);
          when(minimumExperienceDAO.delete(minimumExperienceModel)).thenReturn(new Integer(0));

          params.add("id", "5");
          ReturnObject<MinimumExperienceEnum, JsonObject> boReponse =
              minimumExperienceBO.deleteMinimumExperience(dbHandle, params);

          assert (boReponse.getStatusEnum() == MinimumExperienceEnum.UPDATE_SUCCESS);
          verify(minimumExperienceDAO).delete(minimumExperienceModel);
        }
    );
  }

  public void deleteMinimumExperienceThrowsSQLException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          minimumExperienceBO = new MinimumExperienceBOImpl();
          minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
          MultiMap params = null;
          minimumExperienceModel.setId(5);
          when(minimumExperienceDAO.delete(minimumExperienceModel)).thenThrow(SQLException.class);

          params.add("id", "5");
          ReturnObject<MinimumExperienceEnum, JsonObject> boReponse =
              minimumExperienceBO.deleteMinimumExperience(dbHandle, params);

          assert (boReponse.getStatusEnum() == MinimumExperienceEnum.UPDATE_SUCCESS);
          verify(minimumExperienceDAO).delete(minimumExperienceModel);
        }
    );
  }

  public void deleteMinimumExperienceThrowsDbHandleException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          minimumExperienceBO = new MinimumExperienceBOImpl();
          minimumExperienceDAO = new MinimumExperienceDAOImpl(dbHandle);
          MultiMap params = null;
          minimumExperienceModel.setId(5);
          when(minimumExperienceDAO.delete(minimumExperienceModel)).thenThrow(DbHandleException.class);

          params.add("id", "5");
          ReturnObject<MinimumExperienceEnum, JsonObject> boReponse =
              minimumExperienceBO.deleteMinimumExperience(dbHandle, params);

          assert (boReponse.getStatusEnum() == MinimumExperienceEnum.UPDATE_SUCCESS);
          verify(minimumExperienceDAO).delete(minimumExperienceModel);
        }
    );
  }
}
