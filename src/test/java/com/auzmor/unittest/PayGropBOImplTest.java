package com.auzmor.unittest;

import com.auzmor.dao.PayGroupDAO;
import com.auzmor.impl.bo.PayGroupBOImpl;
import com.auzmor.impl.enumarator.PayGroupEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.model.PayGroupModel;
import com.auzmor.util.bo.ReturnObject;
import io.vertx.core.MultiMap;
import com.auzmor.impl.helper.DbHelper;
import io.vertx.core.json.JsonObject;
import io.vertx.core.Vertx;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.sql.SQLException;
import java.util.List;


import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PayGropBOImplTest {

  @Mock
  PayGroupDAO payGroupDAO;
  private PayGroupBOImpl payGroupBO;
  private PayGroupModel payGroupModel;
  ReturnObject<PayGroupEnum, JsonObject> returnObj;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    payGroupModel = new PayGroupModel();
  }

  
  public void addPayGroup(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          PayGroupBOImpl payGroupBO = new PayGroupBOImpl(dbHandle);
          payGroupBO.setPayGroupDAO(payGroupDAO);
          when(payGroupDAO.insert(payGroupModel)).thenReturn(new Integer(1));

          ReturnObject <PayGroupEnum, JsonObject> insertCount = payGroupBO.addPayGroup(dbHandle,payGroupModel);

          assert (insertCount == returnObj);
          verify(payGroupDAO.insert(payGroupModel));
        });
  }

  
  public void notAddPayGroup(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          PayGroupBOImpl payGroupBO = new PayGroupBOImpl(dbHandle);
          payGroupBO.setPayGroupDAO(payGroupDAO);
          when(payGroupDAO.insert(payGroupModel)).thenReturn(new Integer(0));

          ReturnObject <PayGroupEnum, JsonObject> insertCount = payGroupBO.addPayGroup(dbHandle,payGroupModel);

          assert (insertCount == returnObj);
          verify(payGroupDAO).insert(payGroupModel);
        });
  }

  
  public void getPayGroup(Vertx vertx) throws SQLException,DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          payGroupBO.setPayGroupDAO(payGroupDAO);
          when(payGroupDAO.getPayGroupDetails(payGroupModel)).thenReturn(payGroupModel);

          assert(payGroupModel!=null);
          verify(payGroupDAO).getPayGroupDetails(payGroupModel);
        });
  }

  
  public void updatePayGroupDetails(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          MultiMap requestParams = null;
          PayGroupBOImpl payGroupBO = new PayGroupBOImpl(dbHandle);
          payGroupBO.setPayGroupDAO(payGroupDAO);
          when(payGroupDAO.updatePayGroupDetails(requestParams)).thenReturn(new Integer(1));

          ReturnObject <PayGroupEnum, JsonObject> updateCount = payGroupBO.updatePayGroup(dbHandle,requestParams);

          assert (updateCount==returnObj);
          verify(payGroupDAO).updatePayGroupDetails(requestParams);
        });
  }

  
  public void notUpdatePayGroupDetails(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          MultiMap requestParams = null;
          PayGroupBOImpl payGroupBO = new PayGroupBOImpl(dbHandle);
          payGroupBO.setPayGroupDAO(payGroupDAO);
          when(payGroupDAO.updatePayGroupDetails(requestParams)).thenReturn(new Integer(0));

          ReturnObject <PayGroupEnum, JsonObject> updateCount = payGroupBO.updatePayGroup(dbHandle,requestParams);

          assert (updateCount==returnObj);
          verify(payGroupDAO).updatePayGroupDetails(requestParams);
        });
  }

  
  public void deletePayGroup(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          PayGroupBOImpl payGroupBO = new PayGroupBOImpl(dbHandle);
          payGroupBO.setPayGroupDAO(payGroupDAO);
          when(payGroupDAO.deletePayGroup(payGroupModel)).thenReturn(new Integer(1));

          ReturnObject <PayGroupEnum, JsonObject> deleteCount = payGroupBO.deletePayGroup(dbHandle,payGroupModel);
          assert (deleteCount==returnObj);
          verify(payGroupDAO).deletePayGroup(payGroupModel);
        });
  }

  
  public void notDeletePayGroup(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          PayGroupBOImpl payGroupBO = new PayGroupBOImpl(dbHandle);
          payGroupBO.setPayGroupDAO(payGroupDAO);
          when(payGroupDAO.deletePayGroup(payGroupModel)).thenReturn(new Integer(0));

          ReturnObject <PayGroupEnum, JsonObject> deleteCount = payGroupBO.deletePayGroup(dbHandle,payGroupModel);
          assert (deleteCount==returnObj);
          verify(payGroupDAO).deletePayGroup(payGroupModel);
        });
  }

  
  public void getAllPayGroups (Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          payGroupBO.setPayGroupDAO(payGroupDAO);
          List<PayGroupModel> resultList = payGroupDAO.getAllPayGroups(payGroupModel);
          when(payGroupDAO.getAllPayGroups(payGroupModel)).thenReturn(resultList);

          assert(resultList!=null);
          verify(payGroupDAO).getAllPayGroups(payGroupModel);

        });
  }

  
  public void payGroupThrowsException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          payGroupBO = new PayGroupBOImpl(dbHandle);
          payGroupBO.setPayGroupDAO(payGroupDAO);
          when(payGroupDAO.insert(payGroupModel)).thenThrow(SQLException.class);

          ReturnObject <PayGroupEnum, JsonObject> insertCount = payGroupBO.addPayGroup(dbHandle,payGroupModel);
        });
  }
}

