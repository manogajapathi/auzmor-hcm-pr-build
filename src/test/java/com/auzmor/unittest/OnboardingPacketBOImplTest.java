package com.auzmor.unittest;

import com.auzmor.bo.OnboardingPacketBO;
import com.auzmor.dao.EmployeeDAO;
import com.auzmor.dao.OnboardingPacketDAO;
import com.auzmor.impl.bo.OnboardingPacketBOImpl;
import com.auzmor.impl.dao.EmployeeDAOImpl;
import com.auzmor.impl.dao.OnboardingPacketDAOImpl;
import com.auzmor.impl.enumarator.bo.OnboardingPacketEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.impl.model.onboarding.OnboardingPacket;
import com.auzmor.util.bo.ReturnObject;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.elasticsearch.action.admin.cluster.repositories.verify.VerifyRepositoryAction;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class OnboardingPacketBOImplTest {
  @Mock
  OnboardingPacketDAO onboardingPacketDAO;
  private OnboardingPacketBO onboardingPacketBO;
  private OnboardingPacket onboardingPacket;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    onboardingPacket = new OnboardingPacket();
  }



  public void createPacket(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          onboardingPacketBO = new OnboardingPacketBOImpl();
          onboardingPacketDAO = new OnboardingPacketDAOImpl(dbHandle);
          MultiMap params = null;
          when(onboardingPacketDAO.addPacket(onboardingPacket)).thenReturn(new Integer(1));

          ReturnObject<OnboardingPacketEnum, JsonObject> boReponse =
              onboardingPacketBO.addPacket(dbHandle, params);

          assert (boReponse.getStatusEnum() == OnboardingPacketEnum.ADD_SUCCESS);
          verify(onboardingPacketDAO).addPacket(onboardingPacket);
        }
    );
  }

  public void createPacketNoSuchValues(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          onboardingPacketBO = new OnboardingPacketBOImpl();
          onboardingPacketDAO = new OnboardingPacketDAOImpl(dbHandle);
          MultiMap params = null;
          when(onboardingPacketDAO.addPacket(onboardingPacket)).thenReturn(new Integer(0));

          ReturnObject<OnboardingPacketEnum, JsonObject> boReponse =
              onboardingPacketBO.addPacket(dbHandle, params);

          assert (boReponse.getStatusEnum() == OnboardingPacketEnum.ADD_FAILURE);
          verify(onboardingPacketDAO).addPacket(onboardingPacket);
        }
    );
  }

  public void addPacketAlreadyExists(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          onboardingPacketBO = new OnboardingPacketBOImpl();
          onboardingPacketDAO = new OnboardingPacketDAOImpl(dbHandle);
          MultiMap params = null;
          when(onboardingPacketDAO.isAlreadyExists(onboardingPacket)).thenReturn(true);

          ReturnObject<OnboardingPacketEnum, JsonObject> boReponse =
              onboardingPacketBO.addPacket(dbHandle, params);

          assert (boReponse.getStatusEnum() == OnboardingPacketEnum.ALREADY_EXISTS);
          verify(onboardingPacketDAO).addPacket(onboardingPacket);
        }
    );
  }

  public void createPacketThrowException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          onboardingPacketBO = new OnboardingPacketBOImpl();
          onboardingPacketDAO = new OnboardingPacketDAOImpl(dbHandle);
          MultiMap params = null;
          when(onboardingPacketDAO.addPacket(onboardingPacket)).thenThrow(SQLException.class);

          ReturnObject<OnboardingPacketEnum, JsonObject> boReponse =
              onboardingPacketBO.addPacket(dbHandle, params);
        }
    );
  }

  public void getPacket(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          onboardingPacketBO = new OnboardingPacketBOImpl();
          onboardingPacketDAO = new OnboardingPacketDAOImpl(dbHandle);
          MultiMap params = null;
          when(onboardingPacketDAO.getPackets(onboardingPacket)).thenReturn(new JsonArray());

          ReturnObject<OnboardingPacketEnum, JsonObject> boReponse =
              onboardingPacketBO.getPacket(dbHandle, params);

          assert (boReponse.getStatusEnum() == OnboardingPacketEnum.VALUE_SUCCESS);
          verify(onboardingPacketDAO).getPackets(onboardingPacket);
        }
    );
  }

  public void getPacketNoSuchValues(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          onboardingPacketBO = new OnboardingPacketBOImpl();
          onboardingPacketDAO = new OnboardingPacketDAOImpl(dbHandle);
          MultiMap params = null;
          when(onboardingPacketDAO.getPackets(onboardingPacket)).thenReturn(null);

          ReturnObject<OnboardingPacketEnum, JsonObject> boReponse =
              onboardingPacketBO.getPacket(dbHandle, params);

          assert (boReponse.getStatusEnum() == OnboardingPacketEnum.NO_SUCH_VALUE);
          verify(onboardingPacketDAO).getPackets(onboardingPacket);
        }
    );
  }

  public void getPacketThrowException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          onboardingPacketBO = new OnboardingPacketBOImpl();
          onboardingPacketDAO = new OnboardingPacketDAOImpl(dbHandle);
          MultiMap params = null;
          when(onboardingPacketDAO.getPackets(onboardingPacket)).thenThrow(SQLException.class);

          ReturnObject<OnboardingPacketEnum, JsonObject> boReponse =
              onboardingPacketBO.getPacket(dbHandle, params);
        }
    );
  }

  public void deletePacket(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          onboardingPacketBO = new OnboardingPacketBOImpl();
          onboardingPacketDAO = new OnboardingPacketDAOImpl(dbHandle);
          MultiMap params = null;
          when(onboardingPacketDAO.deletePacket(onboardingPacket)).thenReturn(new Integer(1));

          ReturnObject<OnboardingPacketEnum, JsonObject> boReponse =
              onboardingPacketBO.deletePacket(dbHandle, params);

          assert (boReponse.getStatusEnum() == OnboardingPacketEnum.DELETE_SUCCESS);
          verify(onboardingPacketDAO).deletePacket(onboardingPacket);
        }
    );
  }

  public void deletePacketNoSuchValues(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          onboardingPacketBO = new OnboardingPacketBOImpl();
          onboardingPacketDAO = new OnboardingPacketDAOImpl(dbHandle);
          MultiMap params = null;
          when(onboardingPacketDAO.deletePacket(onboardingPacket)).thenReturn(new Integer(0));

          ReturnObject<OnboardingPacketEnum, JsonObject> boReponse =
              onboardingPacketBO.deletePacket(dbHandle, params);

          assert (boReponse.getStatusEnum() == OnboardingPacketEnum.NO_SUCH_VALUE);
          verify(onboardingPacketDAO).deletePacket(onboardingPacket);
        }
    );
  }

  public void deletePacketThrowException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          onboardingPacketBO = new OnboardingPacketBOImpl();
          onboardingPacketDAO = new OnboardingPacketDAOImpl(dbHandle);
          MultiMap params = null;
          when(onboardingPacketDAO.deletePacket(onboardingPacket)).thenThrow(SQLException.class);

          ReturnObject<OnboardingPacketEnum, JsonObject> boReponse =
              onboardingPacketBO.deletePacket(dbHandle, params);

          assert (boReponse.getStatusEnum() == OnboardingPacketEnum.NO_SUCH_VALUE);
          verify(onboardingPacketDAO).deletePacket(onboardingPacket);
        }
    );
  }

  public void sendPacket(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          onboardingPacketBO = new OnboardingPacketBOImpl();
          EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
          Employee employee = new Employee();
          MultiMap params = null;
          when(employeeDAO.updateEmployeePacket(employee)).thenReturn(new Integer(1));

          ReturnObject<OnboardingPacketEnum, JsonObject> boReponse =
              onboardingPacketBO.sendPacket(dbHandle, params);

          assert (boReponse.getStatusEnum() == OnboardingPacketEnum.PACKET_SENT);
          verify(employeeDAO).updateEmployeePacket(employee);
        }
    );
  }

  public void sendPacketAlreadySent(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          onboardingPacketBO = new OnboardingPacketBOImpl();
          EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
          Employee employee = new Employee();
          MultiMap params = null;
          when(employeeDAO.getEmployeeDetails(1)).thenReturn(employee);

          ReturnObject<OnboardingPacketEnum, JsonObject> boReponse =
              onboardingPacketBO.sendPacket(dbHandle, params);

          assert (boReponse.getStatusEnum() == OnboardingPacketEnum.PACKET_ALREADY_SENT);
          verify(employeeDAO).updateEmployeePacket(employee);
        }
    );
  }


  public void sendPacketNoSuchValue(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          onboardingPacketBO = new OnboardingPacketBOImpl();
          EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
          Employee employee = new Employee();
          MultiMap params = null;
          when(employeeDAO.updateEmployeePacket(employee)).thenReturn(new Integer(0));

          ReturnObject<OnboardingPacketEnum, JsonObject> boReponse =
              onboardingPacketBO.sendPacket(dbHandle, params);

          assert (boReponse.getStatusEnum() == OnboardingPacketEnum.NO_SUCH_VALUE);
          verify(employeeDAO).updateEmployeePacket(employee);
        }
    );
  }


  public void sendPacketThrowsException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          onboardingPacketBO = new OnboardingPacketBOImpl();
          EmployeeDAO employeeDAO = new EmployeeDAOImpl(dbHandle);
          Employee employee = new Employee();
          MultiMap params = null;
          when(employeeDAO.updateEmployeePacket(employee)).thenThrow(SQLException.class);

          ReturnObject<OnboardingPacketEnum, JsonObject> boReponse =
              onboardingPacketBO.sendPacket(dbHandle, params);
        }
    );
  }

  public void updatePacket(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          onboardingPacketBO = new OnboardingPacketBOImpl();
          onboardingPacketDAO = new OnboardingPacketDAOImpl(dbHandle);
          MultiMap params = null;
          when(onboardingPacketDAO.updatePacket(params)).thenReturn(new Integer(1));

          ReturnObject<OnboardingPacketEnum, JsonObject> boReponse =
              onboardingPacketBO.updatePacket(dbHandle, params);

          assert (boReponse.getStatusEnum() == OnboardingPacketEnum.UPDATE_SUCCESS);
          verify(onboardingPacketDAO).updatePacket(params);
        }
    );
  }

  public void updatePacketNoSuchValues(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          onboardingPacketBO = new OnboardingPacketBOImpl();
          onboardingPacketDAO = new OnboardingPacketDAOImpl(dbHandle);
          MultiMap params = null;
          when(onboardingPacketDAO.updatePacket(params)).thenReturn(new Integer(0));

          ReturnObject<OnboardingPacketEnum, JsonObject> boReponse =
              onboardingPacketBO.updatePacket(dbHandle, params);

          assert (boReponse.getStatusEnum() == OnboardingPacketEnum.UPDATE_FAILURE);
          verify(onboardingPacketDAO).updatePacket(params);
        }
    );
  }

  public void updatePacketThrowException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true ,
        dbHandle -> {
          onboardingPacketBO = new OnboardingPacketBOImpl();
          onboardingPacketDAO = new OnboardingPacketDAOImpl(dbHandle);
          MultiMap params = null;
          when(onboardingPacketDAO.updatePacket(params)).thenThrow(SQLException.class);

          ReturnObject<OnboardingPacketEnum, JsonObject> boReponse =
              onboardingPacketBO.updatePacket(dbHandle, params);
        }
    );
  }
}
