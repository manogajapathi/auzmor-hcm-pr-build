package com.auzmor.unittest;

import com.auzmor.dao.CandidateDAO;
import com.auzmor.impl.bo.CandidateBOImpl;
import com.auzmor.impl.enumarator.bo.CandidateStatusEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.handler.Candidate.CandidateHandlerR;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.models.CandidateR.CandidateR;
import com.auzmor.util.db.DbHandle;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.web.RoutingContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CandidateBOImplTest {
  @Mock
  CandidateDAO candidateDAO;
  @Mock
  private CandidateBOImpl candidateBO;
  private CandidateR candidate;
  private RoutingContext context;
  MultiMap multiMap;

  @Before
  public void setup(){
    MockitoAnnotations.initMocks(this);
    candidate = new CandidateR();
  }

  public void createCandidate(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.create(candidate)).thenReturn(new Integer(1));

          CandidateStatusEnum status = candidateBO.
              createCandidate(context, candidate);

          assert (status == CandidateStatusEnum.SUCCESS);
          verify(candidateDAO).create(candidate);
        });
  }

  public void notCreateCandidate(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.create(candidate)).thenReturn(new Integer(0));

          CandidateStatusEnum status = candidateBO.
              createCandidate(context, candidate);

          assert (status == CandidateStatusEnum.FAILED);
          verify(candidateDAO).create(candidate);
        });
  }

  public void createCandidateThrowsSQLException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.create(candidate)).thenThrow(SQLException.class);

          CandidateStatusEnum status = candidateBO.
              createCandidate(context, candidate);

          assert (status == CandidateStatusEnum.FAILED);
          verify(candidateDAO).create(candidate);
        });
  }

  public void createCandidateThrowsDbHandleException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.create(candidate)).thenThrow(DbHandleException.class);

          CandidateStatusEnum status = candidateBO.
              createCandidate(context, candidate);

          assert (status == CandidateStatusEnum.FAILED);
          verify(candidateDAO).create(candidate);
        });
  }

  public void getCandidateDetails(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.getCandidateDetails(10,1)).thenReturn(candidate);

          CandidateR candidate = candidateBO.
              getCandidateId(dbHandle, 10,1);
          assert (candidate != null);
          verify(candidateDAO).getCandidateDetails(10,1);
        });
  }

  public void getCandidateDetailsThrowsSQLException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,

        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.getCandidateDetails(10,1)).thenThrow(SQLException.class);

          CandidateR candidate = candidateBO.
              getCandidateId(dbHandle, 10,1);
          assert (candidate != null);
          verify(candidateDAO).getCandidateDetails(10,1);
        });
  }

  public void getCandidateDetailsThrowsDbHandleException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.getCandidateDetails(10,1)).thenThrow(DbHandleException.class);

          CandidateR candidate = candidateBO.
              getCandidateId(dbHandle, 10,1);
          assert (candidate != null);
          verify(candidateDAO).getCandidateDetails(10,1);
        });
  }

  public void getCandidate(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.getCandidate(10)).thenReturn(candidate);

          CandidateR candidate = candidateBO.
              getCandidate(dbHandle, 10);
          assert (candidate != null);
          verify(candidateDAO).getCandidate(10);
        });
  }


  public void getCandidateThrowsDbHandleException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.getCandidate(10)).thenThrow(DbHandleException.class);

          CandidateR candidate = candidateBO.
              getCandidate(dbHandle, 10);
          assert (candidate != null);
          verify(candidateDAO).getCandidate(10);
        });
  }


  public void getCandidateThrowsSQLException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.getCandidate(10)).thenThrow(SQLException.class);

          CandidateR candidate = candidateBO.
              getCandidate(dbHandle, 10);
          assert (candidate != null);
          verify(candidateDAO).getCandidate(10);
        });
  }


  public void getUserId(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          List<HashMap<String, String>> candidateList = null;
          when(candidateDAO.getUserIdCandidateDetails(10,1)).thenReturn(candidateList);

          candidateList = candidateBO.
              getUserId(dbHandle, 10,1);
          assert (candidateList != null);
          verify(candidateDAO).getUserIdCandidateDetails(10,1);
        });
  }


  public void getUserIdThrowsSQLException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          List<HashMap<String, String>> candidateList = null;
          when(candidateDAO.getUserIdCandidateDetails(10,1)).thenThrow(SQLException.class);

          candidateList = candidateBO.
              getUserId(dbHandle, 10,1);
          assert (candidateList != null);
          verify(candidateDAO).getUserIdCandidateDetails(10,1);
        });
  }


  public void getUserIdThrowsDbHandleException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          List<HashMap<String, String>> candidateList = null;
          when(candidateDAO.getUserIdCandidateDetails(10,1)).thenThrow(DbHandleException.class);

          candidateList = candidateBO.
              getUserId(dbHandle, 10,1);
          assert (candidateList != null);
          verify(candidateDAO).getUserIdCandidateDetails(10,1);
        });
  }


  public void getJobId(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          List<HashMap<String, String>> candidateList = null;
          when(candidateDAO.getJobIdCandidateDetails(10)).thenReturn(candidateList);

          candidateList = candidateBO.
              getJobId(dbHandle, 10,1);
          assert (candidateList != null);
          verify(candidateDAO).getJobIdCandidateDetails(10);
        });
  }


  public void getJobIdThrowsSQLException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          List<HashMap<String, String>> candidateList = null;
          when(candidateDAO.getJobIdCandidateDetails(10)).thenThrow(SQLException.class);

          candidateList = candidateBO.
              getJobId(dbHandle, 10,1);
          assert (candidateList != null);
          verify(candidateDAO).getJobIdCandidateDetails(10);
        });
  }


  public void getJobIdThrowsDbHandleException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          List<HashMap<String, String>> candidateList = null;
          when(candidateDAO.getJobIdCandidateDetails(10)).thenThrow(DbHandleException.class);

          candidateList = candidateBO.
              getJobId(dbHandle, 10,1);
          assert (candidateList != null);
          verify(candidateDAO).getJobIdCandidateDetails(10);
        });
  }


  public void deleteCandidate(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          List<HashMap<String, String>> candidateList = null;
          when(candidateDAO.deleteCandidate(10)).thenReturn(1);

          CandidateStatusEnum candidateStatusEnum = candidateBO.
              deleteCandidate(10);
          assert (candidateStatusEnum == candidateStatusEnum.CANDIDATE_DELETED_SUCESSFULLY);
          verify(candidateDAO).deleteCandidate(10);
        });
  }


  public void notDeleteCandidate(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          List<HashMap<String, String>> candidateList = null;
          when(candidateDAO.deleteCandidate(10)).thenReturn(0);

          CandidateStatusEnum candidateStatusEnum = candidateBO.
              deleteCandidate(10);
          assert (candidateStatusEnum == candidateStatusEnum.CANDIDATE_DELETED_FAILED);
          verify(candidateDAO).deleteCandidate(10);
        });
  }


  public void deleteCandidateThrowsSQLException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          List<HashMap<String, String>> candidateList = null;
          when(candidateDAO.deleteCandidate(10)).thenThrow(SQLException.class);

          CandidateStatusEnum candidateStatusEnum = candidateBO.
              deleteCandidate(10);
          assert (candidateStatusEnum == candidateStatusEnum.CANDIDATE_DELETED_FAILED);
          verify(candidateDAO).deleteCandidate(10);
        });
  }



  public void deleteCandidateThrowsDbHandleException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          List<HashMap<String, String>> candidateList = null;
          when(candidateDAO.deleteCandidate(10)).thenThrow(DbHandleException.class);

          CandidateStatusEnum candidateStatusEnum = candidateBO.
              deleteCandidate(10);
          assert (candidateStatusEnum == candidateStatusEnum.CANDIDATE_DELETED_FAILED);
          verify(candidateDAO).deleteCandidate(10);
        });
  }


  public void getPostJobFilterDetails(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          List<HashMap<String, String>> candidateList = null;
          when(candidateDAO.getPostJobFilterDetails(1)).thenReturn(candidateList);

          candidateList = candidateBO.
              getPostJobFilterDetails(dbHandle, 1);
          assert (candidateList != null);
          verify(candidateDAO).getPostJobFilterDetails(1);
        });
  }


  public void getPostJobFilterDetailsThrowsSQLException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          List<HashMap<String, String>> candidateList = null;
          when(candidateDAO.getPostJobFilterDetails(1)).thenThrow(SQLException.class);

          candidateList = candidateBO.
              getPostJobFilterDetails(dbHandle, 1);
          assert (candidateList != null);
          verify(candidateDAO).getPostJobFilterDetails(1);
        });
  }


  public void getPostJobFilterDetailsThrowsDbHandleException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          List<HashMap<String, String>> candidateList = null;
          when(candidateDAO.getPostJobFilterDetails(1)).thenThrow(DbHandleException.class);

          candidateList = candidateBO.
              getPostJobFilterDetails(dbHandle, 1);
          assert (candidateList != null);
          verify(candidateDAO).getPostJobFilterDetails(1);
        });
  }



  public void updateRating(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.updateRating(4,10)).thenReturn(new Integer(1));

          CandidateStatusEnum status = candidateBO.
              updateRating(4, 10,1);

          assert (status == CandidateStatusEnum.UPDATE_SUCCESS);
          verify(candidateDAO).updateRating(4,10);
        });
  }

  public void updateRatingThrowsSQLException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.updateRating(4,10)).thenThrow(SQLException.class);

          CandidateStatusEnum status = candidateBO.
              updateRating(4, 10,1);

          assert (status == CandidateStatusEnum.UPDATE_SUCCESS);
          verify(candidateDAO).updateRating(4,10);
        });
  }

 
  public void updateRatingThrowsDBHandleException(Vertx vertx) throws SQLException, DbHandleException {
   DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
       dbHandle -> {
         CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
         candidateBO.setCandidateDAO(candidateDAO);
         when(candidateDAO.updateRating(4, 10)).thenThrow(DbHandleException.class);

         CandidateStatusEnum status = candidateBO.
             updateRating(4, 10, 1);

         assert (status == CandidateStatusEnum.UPDATE_SUCCESS);
         verify(candidateDAO).updateRating(4,10);
       });
 }

  public void updateForwardEmployee(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.updateForwardEmployee(4,"forward","1")).thenReturn(new Integer(1));

          CandidateStatusEnum status = candidateBO.
              updateForwardEmployee(4,"forward","1",1);

          assert (status == CandidateStatusEnum.UPDATE_SUCCESS);
          verify(candidateDAO).updateForwardEmployee(4,"forward","1");
        });
  }

  public void updateForwardEmployeeThrowsSQLException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.updateForwardEmployee(4,"forward","1")).thenThrow(SQLException.class);

          CandidateStatusEnum status = candidateBO.
              updateForwardEmployee(4,"forward","1",1);

          assert (status == CandidateStatusEnum.UPDATE_SUCCESS);
          verify(candidateDAO).updateForwardEmployee(4,"forward","1");
        });
  }

  public void updateForwardEmployeeThrowsDBHandleException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.updateForwardEmployee(4,"forward","1")).thenThrow
              (DbHandleException.class);

          CandidateStatusEnum status = candidateBO.
              updateForwardEmployee(4,"forward","1",1);

          assert (status == CandidateStatusEnum.UPDATE_SUCCESS);
          verify(candidateDAO).updateForwardEmployee(4,"forward","1");
        });
  }


  public void updateMoveCandidate(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.updateMoveCandidate(4,10,"Moved")).thenReturn(new Integer(1));

          CandidateStatusEnum status = candidateBO.
              updateMoveCandidate(4,10,1);

          assert (status == CandidateStatusEnum.UPDATE_SUCCESS);
          verify(candidateDAO).updateMoveCandidate(4,10,"Moved");
        });
  }

  public void updateMoveCandidateThrowsSQLException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.updateMoveCandidate(4,10,"Moved")).thenThrow(SQLException.class);

          CandidateStatusEnum status = candidateBO.
              updateMoveCandidate(4,10,1);

          assert (status == CandidateStatusEnum.UPDATE_SUCCESS);
          verify(candidateDAO).updateMoveCandidate(4,10,"Moved");
        });
  }

  public void updateMoveCandidateThrowsDBHandleException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.updateMoveCandidate(4,10,"Moved")).thenThrow(DbHandleException.class);

          CandidateStatusEnum status = candidateBO.
              updateMoveCandidate(4,10,1);

          assert (status == CandidateStatusEnum.UPDATE_SUCCESS);
          verify(candidateDAO).updateMoveCandidate(4,10,"Moved");
        });
  }

  public void updateCandidate(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.updateCandidate(candidate)).thenReturn(new Integer(1));

          CandidateStatusEnum status = candidateBO.
              updateCandidate(null, multiMap);

          assert (status == CandidateStatusEnum.UPDATE_SUCCESS);
          verify(candidateDAO).updateCandidate(candidate);
        });
  }


  public void updateCandidateThrowsSQLException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.updateCandidate(candidate)).thenThrow(SQLException.class);

          CandidateStatusEnum status = candidateBO.
              updateCandidate(null, multiMap);

          assert (status == CandidateStatusEnum.UPDATE_SUCCESS);
          verify(candidateDAO).updateCandidate(candidate);
        });
  }

  public void updateCandidateThrowsDBHandleException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.updateCandidate(candidate)).thenThrow(DbHandleException.class);

          CandidateStatusEnum status = candidateBO.
              updateCandidate(null, multiMap);

          assert (status == CandidateStatusEnum.UPDATE_SUCCESS);
          verify(candidateDAO).updateCandidate(candidate);
        });
  }

  /*public void getCandidateList(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          List<HashMap<String, String>> candidateList = null;
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.getCandidateList(new StringBuilder(""),new StringBuilder(""),1,
              "")).thenReturn(candidateList);
          JsonArray candidateLists = candidateBO.
              getCandidateList(null, multiMap);

          assert (candidateLists != null);
          verify(candidateDAO).getCandidateList(new StringBuilder(""),new StringBuilder(""),1,"");
        });
  }

  public void getCandidateListThrowsSQLException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          List<HashMap<String, String>> candidateList = null;
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.getCandidateList(new StringBuilder(""),new StringBuilder(""),1,
              "")).thenThrow(SQLException.class);
          JsonArray candidateLists = candidateBO.
              getCandidateList(null, multiMap);

          assert (candidateLists != null);
          verify(candidateDAO).getCandidateList(new StringBuilder(""),new StringBuilder(""),1,"");
        });
  }

  public void getCandidateListThrowsDbHandleException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          List<HashMap<String, String>> candidateList = null;
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.getCandidateList(new StringBuilder(""),new StringBuilder(""),1,
              "")).thenThrow(DbHandleException.class);
          JsonArray candidateLists = candidateBO.
              getCandidateList(null, multiMap);

          assert (candidateLists != null);
          verify(candidateDAO).getCandidateList(new StringBuilder(""),new StringBuilder(""),1,"");
        });
  }*/

  public void updateStatus(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.updateStatus("",1);
          when(candidateDAO.updateCandidateStatus("",1)).thenReturn(new Integer(1));

          int status = candidateBO.
               updateStatus("",1);

          assert (status == 1);
          verify(candidateDAO).updateCandidateStatus("",1);
        });
  }

  public void updateStatusThrowsSQLException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.updateStatus("",1);
          when(candidateDAO.updateCandidateStatus("",1)).thenThrow(SQLException.class);

          int status = candidateBO.
              updateStatus("",1);

          assert (status == 1);
          verify(candidateDAO).updateCandidateStatus("",1);
        });
  }

  public void updateStatusThrowsDBHandleException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.updateStatus("",1);
          when(candidateDAO.updateCandidateStatus("",1)).thenThrow(DbHandleException.class);

          int status = candidateBO.
              updateStatus("",1);

          assert (status == 1);
          verify(candidateDAO).updateCandidateStatus("",1);
        });
  }

  public void getCandidateTalentList(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          List<HashMap<String, String>> candidateList = null;
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          String format = "dd-MMM-yyyy";
          String searchFilter ="and Candidate.first_name like '%a%' ";
          String stateId ="35";
          when(candidateDAO.getCandidateTalentList(1,format,searchFilter,10,0,stateId)).thenReturn(candidateList);

          List candidate = candidateBO.
              getCandidateTalentList( 1,format,searchFilter,10,0,stateId);
          assert (candidate != null);
          verify(candidateDAO).getCandidateTalentList(1,format,searchFilter,10,0,stateId);
        });
  }

  public void getCandidateTalentListThrowsSQLException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          List<HashMap<String, String>> candidateList = null;
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          String format = "dd-MMM-yyyy";
          String searchFilter ="and Candidate.first_name like '%a%' ";
          String stateId ="35";
          when(candidateDAO.getCandidateTalentList(1,format,searchFilter,10,0,stateId)).thenThrow(SQLException.class);

          List candidate = candidateBO.
              getCandidateTalentList( 1,format,searchFilter,10,0,stateId);
          assert (candidate != null);
          verify(candidateDAO).getCandidateTalentList(1,format,searchFilter,10,0,stateId);
        });
  }

  public void getCandidateTalentListThrowsDbHandleException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          List<HashMap<String, String>> candidateList = null;
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          String format = "dd-MMM-yyyy";
          String searchFilter ="and Candidate.first_name like '%a%' ";
          String stateId ="35";
          when(candidateDAO.getCandidateTalentList(1,format,searchFilter,10,0,stateId)).thenThrow(DbHandleException.class);

          List candidate = candidateBO.
              getCandidateTalentList( 1,format,searchFilter,10,0,stateId);
          assert (candidate != null);
          verify(candidateDAO).getCandidateTalentList(1,format,searchFilter,10,0,stateId);
        });
  }

  public void getCandidateListBasedOnStatus(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          List<HashMap<String, String>> candidateList = null;
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          String format = "dd-MMM-yyyy";
          String searchFilter ="and Candidate.first_name like '%a%' ";
          String stateId ="35";
          when(candidateDAO.getCandidateTalentList(1,format,searchFilter,10,0,stateId)).thenReturn(candidateList);

          List candidate = candidateBO.
              getCandidateTalentList( 1,format, searchFilter,10,0,stateId);
          assert (candidate != null);
          verify(candidateDAO).getCandidateTalentList(1,format,searchFilter,10,0,stateId);
        });
  }

  public void getCandidateListBasedOnStatusThrowsSQLException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          List<HashMap<String, String>> candidateList = null;
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          String format = "dd-MMM-yyyy";
          String searchFilter ="and Candidate.first_name like '%a%' ";
          String stateId ="35";
          when(candidateDAO.getCandidateTalentList(1,format,searchFilter,10,0,stateId)).thenThrow(SQLException.class);

          List candidate = candidateBO.
              getCandidateTalentList( 1,format,searchFilter,10,0,stateId);
          assert (candidate != null);
          verify(candidateDAO).getCandidateTalentList(1,format,searchFilter,10,0,stateId);
        });
  }

  public void getCandidateListBasedOnStatusThrowsDbHandleException(Vertx vertx) throws SQLException,
      DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          List<HashMap<String, String>> candidateList = null;
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          String format = "dd-MMM-yyyy";
          String searchFilter ="and Candidate.first_name like '%a%' ";
          String stateId ="35";
          when(candidateDAO.getCandidateTalentList(1,format,searchFilter,10,0,stateId)).thenThrow(DbHandleException.class);

          List candidate = candidateBO.
              getCandidateTalentList( 1,format,searchFilter,10,0,stateId);
          assert (candidate != null);
          verify(candidateDAO).getCandidateTalentList(1,format,searchFilter,10,0,stateId);
        });
  }

  public void getCandidatewithLoginId(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          List<HashMap<String, String>> candidateList = null;
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);

          when(candidateDAO.getCandidatewithLoginId(1)).thenReturn(1);

          int candidate = candidateBO.
              getCandidatewithLoginId( 1);
          assert (candidate != 0);
          verify(candidateDAO).getCandidatewithLoginId(1);
        });
  }


  public void getCandidatewithLoginIdThrowsSQLException(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          List<HashMap<String, String>> candidateList = null;
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.getCandidatewithLoginId(1)).thenReturn(1);

          int candidate = candidateBO.
              getCandidatewithLoginId( 1);
          assert (candidate != 0);
          verify(candidateDAO).getCandidatewithLoginId(1);
        });
  }

  
  public void getCandidatewithLoginIdThrowsDbHandleException(Vertx vertx) throws SQLException,
      DbHandleException {
DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          List<HashMap<String, String>> candidateList = null;
          CandidateBOImpl candidateBO = new CandidateBOImpl(dbHandle);
          candidateBO.setCandidateDAO(candidateDAO);
          when(candidateDAO.getCandidatewithLoginId(1)).thenReturn(1);

          int candidate = candidateBO.
              getCandidatewithLoginId( 1);
          assert (candidate != 0);
          verify(candidateDAO).getCandidatewithLoginId(1);
        });
  }

  @After
  public void close() {
    System.out.println("After Method Candidate");
  }
}