package com.auzmor.unittest;

import com.auzmor.bo.DivisionBO;
import com.auzmor.dao.DivisionDAO;
import com.auzmor.impl.bo.DivisionBOImpl;
import com.auzmor.impl.dao.DivisionDAOImpl;
import com.auzmor.impl.enumarator.DivisionEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.FieldOptions.DivisionModel;
import com.auzmor.util.bo.ReturnObject;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.elasticsearch.common.recycler.Recycler;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.bouncycastle.asn1.ua.DSTU4145NamedCurves.params;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class DivisionBOImplTest {
  @Mock
  DivisionDAO divisionDAO;
  private DivisionBO divisionBO;
  private DivisionModel divisionModel;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    divisionModel = new DivisionModel();
  }

  
  public void createDivision(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
             divisionBO = new DivisionBOImpl();
             divisionDAO = new DivisionDAOImpl(dbHandle);
             when(divisionDAO.insert(divisionModel)).thenReturn(new Integer(1));

          ReturnObject<DivisionEnum, JsonObject> boReponse = 
              divisionBO.addDivision(dbHandle, divisionModel);

          assert (boReponse.getStatusEnum() == DivisionEnum.ADDITION_SUCCESS);
          verify(divisionDAO).insert(divisionModel);
        }
    );
  }

  
  public void createDivisionNotSuccessful(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          divisionBO = new DivisionBOImpl();
          divisionDAO = new DivisionDAOImpl(dbHandle);
          when(divisionDAO.insert(divisionModel)).thenReturn(new Integer(0));

          ReturnObject<DivisionEnum, JsonObject> boReponse = 
              divisionBO.addDivision(dbHandle, divisionModel);

          assert (boReponse.getStatusEnum() == DivisionEnum.ADDITION_FAILED);
          verify(divisionDAO).insert(divisionModel);
        }
    );
  }

  
  public void createDivisionThrowsException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          divisionBO = new DivisionBOImpl();
          divisionDAO = new DivisionDAOImpl(dbHandle);
          when(divisionDAO.insert(divisionModel)).thenThrow(SQLException.class);

          ReturnObject<DivisionEnum, JsonObject> boReponse = 
              divisionBO.addDivision(dbHandle, divisionModel);
        }
    );
  }

  
  public void getAllDivisions(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          divisionBO = new DivisionBOImpl();
          divisionDAO = new DivisionDAOImpl(dbHandle);
          List<DivisionModel> divisionList = new ArrayList<>();
          when(divisionDAO.getAllDivisions(divisionModel)).thenReturn(divisionList);

          ReturnObject<DivisionEnum, JsonArray> boReponse =
              divisionBO.getAllDivisions(dbHandle, divisionModel);

          assert (boReponse.getStatusEnum() == DivisionEnum.VALUE_SUCCESS);
          verify(divisionDAO).getAllDivisions(divisionModel);
        }
    );
  }

  
  public void getAllDivisionsNoValues(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          divisionBO = new DivisionBOImpl();
          divisionDAO = new DivisionDAOImpl(dbHandle);
          List<DivisionModel> divisionList = new ArrayList<>();
          when(divisionDAO.getAllDivisions(divisionModel)).thenReturn(null);

          ReturnObject<DivisionEnum, JsonArray> boReponse =
              divisionBO.getAllDivisions(dbHandle, divisionModel);

          assert (boReponse.getStatusEnum() == DivisionEnum.NO_SUCH_VALUE);
          verify(divisionDAO).getAllDivisions(divisionModel);
        }
    );
  }

  
  public void getAllDivisionsThrowsException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          divisionBO = new DivisionBOImpl();
          divisionDAO = new DivisionDAOImpl(dbHandle);
          List<DivisionModel> divisionList = new ArrayList<>();
          when(divisionDAO.getAllDivisions(divisionModel)).thenThrow(SQLException.class);

          ReturnObject<DivisionEnum, JsonArray> boReponse =
              divisionBO.getAllDivisions(dbHandle, divisionModel);
        }
    );
  }

  
  public void getDivision(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          divisionBO = new DivisionBOImpl();
          divisionDAO = new DivisionDAOImpl(dbHandle);
          List<DivisionModel> divisionList = new ArrayList<>();
          when(divisionDAO.getDivisionDetails(divisionModel)).thenReturn(divisionModel);

          ReturnObject<DivisionEnum, JsonArray> boReponse =
              divisionBO.getAllDivisions(dbHandle, divisionModel);

          assert (boReponse.getStatusEnum() == DivisionEnum.VALUE_SUCCESS);
          verify(divisionDAO).getAllDivisions(divisionModel);
        }
    );
  }

  
  public void getDivisionNoValues(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          divisionBO = new DivisionBOImpl();
          divisionDAO = new DivisionDAOImpl(dbHandle);
          List<DivisionModel> divisionList = new ArrayList<>();
          when(divisionDAO.getAllDivisions(divisionModel)).thenReturn(null);

          ReturnObject<DivisionEnum, JsonArray> boReponse =
              divisionBO.getAllDivisions(dbHandle, divisionModel);

          assert (boReponse.getStatusEnum() == DivisionEnum.NO_SUCH_VALUE);
          verify(divisionDAO).getAllDivisions(divisionModel);
        }
    );
  }

  
  public void getDivisionThrowsException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          divisionBO = new DivisionBOImpl();
          divisionDAO = new DivisionDAOImpl(dbHandle);
          List<DivisionModel> divisionList = new ArrayList<>();
          when(divisionDAO.getAllDivisions(divisionModel)).thenThrow(SQLException.class);

          ReturnObject<DivisionEnum, JsonArray> boReponse =
              divisionBO.getAllDivisions(dbHandle, divisionModel);
        }
    );
  }

  
  public void updateDivision(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          divisionBO = new DivisionBOImpl();
          divisionDAO = new DivisionDAOImpl(dbHandle);
          MultiMap params = null;
          when(divisionDAO.updateDivisionDetails(params)).thenReturn(new Integer(1));

          ReturnObject<DivisionEnum, JsonObject> boReponse =
              divisionBO.updateDivision(dbHandle, params);

          assert (boReponse.getStatusEnum() == DivisionEnum.UPDATE_SUCCESS);
          verify(divisionDAO).getAllDivisions(divisionModel);
        }
    );
  }

  
  public void updateDivisionNoValues(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          divisionBO = new DivisionBOImpl();
          divisionDAO = new DivisionDAOImpl(dbHandle);
          MultiMap params = null;
          when(divisionDAO.updateDivisionDetails(params)).thenReturn(new Integer(0));

          ReturnObject<DivisionEnum, JsonObject> boReponse =
              divisionBO.updateDivision(dbHandle, params);

          assert (boReponse.getStatusEnum() == DivisionEnum.UPDATE_SUCCESS);
          verify(divisionDAO).getAllDivisions(divisionModel);
        }
    );
  }

  
  public void updateDivisionThrowsException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          divisionBO = new DivisionBOImpl();
          divisionDAO = new DivisionDAOImpl(dbHandle);
          MultiMap params = null;
          when(divisionDAO.updateDivisionDetails(params)).thenThrow(SQLException.class);

          ReturnObject<DivisionEnum, JsonArray> boReponse =
              divisionBO.getAllDivisions(dbHandle, divisionModel);
        }
    );
  }

  
  public void deleteDivisionThrowsException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          divisionBO = new DivisionBOImpl();
          divisionDAO = new DivisionDAOImpl(dbHandle);
          List<DivisionModel> divisionList = new ArrayList<>();
          when(divisionDAO.deleteDivision(divisionModel)).thenThrow(SQLException.class);

          ReturnObject<DivisionEnum, JsonArray> boReponse =
              divisionBO.getAllDivisions(dbHandle, divisionModel);
        }
    );
  }

  
  public void deleteDivision(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          divisionBO = new DivisionBOImpl();
          divisionDAO = new DivisionDAOImpl(dbHandle);
          when(divisionDAO.deleteDivision(divisionModel)).thenReturn(new Integer(1));

          ReturnObject<DivisionEnum, JsonObject> boReponse =
              divisionBO.deleteDivision(dbHandle, divisionModel);

          assert (boReponse.getStatusEnum() == DivisionEnum.VALUE_SUCCESS);
          verify(divisionDAO).getAllDivisions(divisionModel);
        }
    );
  }

  
  public void deleteDivisionNoValues(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          divisionBO = new DivisionBOImpl();
          divisionDAO = new DivisionDAOImpl(dbHandle);
          when(divisionDAO.deleteDivision(divisionModel)).thenReturn(new Integer(1));

          ReturnObject<DivisionEnum, JsonArray> boReponse =
              divisionBO.getAllDivisions(dbHandle, divisionModel);

          assert (boReponse.getStatusEnum() == DivisionEnum.NO_SUCH_VALUE);
          verify(divisionDAO).getAllDivisions(divisionModel);
        }
    );
  }
}
