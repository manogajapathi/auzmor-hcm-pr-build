package com.auzmor.unittest;

import com.auzmor.impl.bo.ReportBOImpl;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.ConfigurationJsonHelper;
import com.auzmor.service.ATSServer;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

@RunWith(VertxUnitRunner.class)
public class HCMVertxTest {
  private Vertx vertx;
  private Integer port;

  @Before
  public void setUp(TestContext context) throws IOException {
    vertx = Vertx.vertx();
    vertx.deployVerticle(new ATSServer(), new DeploymentOptions()
        .setConfig(ConfigurationJsonHelper.getConf("ats.json", "db.json", "db_secrets.json",
            "secrets.json", "services.json")), atsAsyncResult -> {
      if (!atsAsyncResult.succeeded()) {
      }
    });
  }

  /**
   * This method, called after our test, just cleanup everything by closing the vert.x instance
   *
   * @param context the test context
   */
  @After
  public void tearDown(TestContext context) {
    vertx.close(context.asyncAssertSuccess());
  }

  /*@Test
  public void testCandidateHiringProcess() throws SQLException, DbHandleException {
    CandidateHiringProcessBOImplTest boImplTest = new CandidateHiringProcessBOImplTest();
    boImplTest.setup();
    boImplTest.createHiringProcessForCandidate();
  }*/
  @Test
  public void testTitle() throws SQLException, DbHandleException {
    TitleBOImplTest boImplTest = new TitleBOImplTest();
    boImplTest.setup();
    boImplTest.createTitle(vertx);
    boImplTest.createTitleNoValues(vertx);
    boImplTest.createTitleThrowsException(vertx);
    boImplTest.getAllTitles(vertx);
    boImplTest.getAllTitlesNoSuchValues(vertx);
    boImplTest.getAllTitlesThrowsException(vertx);
    boImplTest.getTitle(vertx);
    boImplTest.getTitleNoSuchValues(vertx);
    boImplTest.deleteTitle(vertx);
    boImplTest.deleteTitleNoSuchValues(vertx);
    boImplTest.deleteTitleThrowsException(vertx);
  }

  @Test
  public void testEmployee() throws SQLException, DbHandleException {
    EmployeeBOImplTest employeeBOImplTest = new EmployeeBOImplTest();
    employeeBOImplTest.setup();
    employeeBOImplTest.createEmployee(vertx);
    employeeBOImplTest.createUser(vertx);
    employeeBOImplTest.notCreateUser(vertx);
    employeeBOImplTest.deleteUser(vertx);
    employeeBOImplTest.notDeleteUser(vertx);
    employeeBOImplTest.updateUser(vertx);
    employeeBOImplTest.notUpdateUser(vertx);
    employeeBOImplTest.getUsersList(vertx);
    employeeBOImplTest.notGetUsersList(vertx);
    employeeBOImplTest.alreadyExistsEmployee(vertx);
    employeeBOImplTest.alreadyNotExistsEmployee(vertx);
    employeeBOImplTest.notCreateEmployee(vertx);
    employeeBOImplTest.getEmployeeList(vertx);
    employeeBOImplTest.notgetEmployeeList(vertx);
    employeeBOImplTest.getEmployeeDetails(vertx);
    employeeBOImplTest.notGetEmployeeDetails(vertx);
    employeeBOImplTest.getEmployeeDetailsByLoginId(vertx);
    employeeBOImplTest.notGetEmployeeDetailsByLoginId(vertx);
    employeeBOImplTest.testGetDashBoardCount(vertx);
    employeeBOImplTest.testGetDashBoardCountNoSuchValues(vertx);
    employeeBOImplTest.testGetDashBoardCountSQLException(vertx);
    employeeBOImplTest.testGetDashBoardCountDbHandleException(vertx);
  }

  @Test
  public void testEmailTemplates() throws SQLException, DbHandleException {
    EmailTemplatesBOImplTest emailTemplatesBOImplTest = new EmailTemplatesBOImplTest();
    emailTemplatesBOImplTest.setup();
    emailTemplatesBOImplTest.addTemplate(vertx);
    emailTemplatesBOImplTest.notAddTemplate(vertx);
    emailTemplatesBOImplTest.editTemplate(vertx);
    emailTemplatesBOImplTest.notEditTemplate(vertx);
    emailTemplatesBOImplTest.deleteTemplate(vertx);
    emailTemplatesBOImplTest.notDeleteTemplate(vertx);
    emailTemplatesBOImplTest.getTemplates(vertx);
    emailTemplatesBOImplTest.notGetTemplates(vertx);
    emailTemplatesBOImplTest.getTemplate(vertx);
    emailTemplatesBOImplTest.notGetTemplate(vertx);
    emailTemplatesBOImplTest.generateEmails(vertx);
    emailTemplatesBOImplTest.notGenerateEmails(vertx);
    //emailTemplatesBOImplTest.addTemplateThrowException(vertx);
  }

  @Test
  public void testOnboardingDocumentTemplates() throws SQLException, DbHandleException {
    OnboardingDocumentBOImplTest onboardingDocumentBOImplTest = new OnboardingDocumentBOImplTest();
    onboardingDocumentBOImplTest.setup();
    onboardingDocumentBOImplTest.addDocument(vertx);
    onboardingDocumentBOImplTest.createDocumentNoSuchValues(vertx);
    onboardingDocumentBOImplTest.addDocumentAlreadyExists(vertx);
    onboardingDocumentBOImplTest.deletePacket(vertx);
    onboardingDocumentBOImplTest.deletePacketNoSuchValues(vertx);
    onboardingDocumentBOImplTest.deleteDocumentThrowException(vertx);
    onboardingDocumentBOImplTest.updateDocument(vertx);
    onboardingDocumentBOImplTest.updateDocumentNoSuchValues(vertx);
  }

  @Test
  public void testOnboardingFormTemplates() throws SQLException, DbHandleException {
    OnboardingFormBOImplTest onboardingFormBOImplTest = new OnboardingFormBOImplTest();
    onboardingFormBOImplTest.setup();
    onboardingFormBOImplTest.addForm(vertx);
    onboardingFormBOImplTest.getForm(vertx);
    onboardingFormBOImplTest.deleteForm(vertx);
    onboardingFormBOImplTest.notDeleteForm(vertx);
    onboardingFormBOImplTest.updateForm(vertx);
    onboardingFormBOImplTest.notUpdateForm(vertx);
    onboardingFormBOImplTest.notAddForm(vertx);
    onboardingFormBOImplTest.notGetForm(vertx);
    onboardingFormBOImplTest.addOnboardingFormThrowsException(vertx);
  }

  @Test

  public void testOnboardingPacketTemplates() throws SQLException, DbHandleException {

    OnboardingPacketBOImplTest onboardingPacketBOImplTest = new OnboardingPacketBOImplTest();
    onboardingPacketBOImplTest.createPacket(vertx);
    onboardingPacketBOImplTest.createPacketNoSuchValues(vertx);
    onboardingPacketBOImplTest.createPacketThrowException(vertx);
    onboardingPacketBOImplTest.addPacketAlreadyExists(vertx);
    onboardingPacketBOImplTest.getPacket(vertx);
    onboardingPacketBOImplTest.getPacketNoSuchValues(vertx);
    onboardingPacketBOImplTest.getPacketThrowException(vertx);
    onboardingPacketBOImplTest.deletePacket(vertx);
    onboardingPacketBOImplTest.deletePacketNoSuchValues(vertx);
    onboardingPacketBOImplTest.deletePacketThrowException(vertx);
    onboardingPacketBOImplTest.sendPacket(vertx);
    onboardingPacketBOImplTest.sendPacketAlreadySent(vertx);
    onboardingPacketBOImplTest.sendPacketNoSuchValue(vertx);
    onboardingPacketBOImplTest.sendPacketThrowsException(vertx);
    onboardingPacketBOImplTest.updatePacket(vertx);
    onboardingPacketBOImplTest.updatePacketNoSuchValues(vertx);
    onboardingPacketBOImplTest.updatePacketThrowException(vertx);
  }

  @Test
  public void testCandidate() throws SQLException, DbHandleException {
    CandidateBOImplTest candBOImplTest = new CandidateBOImplTest();

    candBOImplTest.setup();

    candBOImplTest.createCandidate(vertx);
    candBOImplTest.createCandidateThrowsDbHandleException(vertx);
    candBOImplTest.createCandidateThrowsSQLException(vertx);
    candBOImplTest.notCreateCandidate(vertx);

    candBOImplTest.deleteCandidate(vertx);
    candBOImplTest.deleteCandidateThrowsDbHandleException(vertx);
    candBOImplTest.deleteCandidateThrowsSQLException(vertx);
    candBOImplTest.notDeleteCandidate(vertx);

    candBOImplTest.getCandidate(vertx);
    candBOImplTest.getCandidateThrowsDbHandleException(vertx);
    candBOImplTest.getCandidateThrowsSQLException(vertx);

    candBOImplTest.getCandidateDetails(vertx);
    candBOImplTest.getCandidateThrowsSQLException(vertx);
    candBOImplTest.getCandidateThrowsDbHandleException(vertx);

    candBOImplTest.getCandidateTalentList(vertx);
    candBOImplTest.getCandidateTalentListThrowsDbHandleException(vertx);
    candBOImplTest.getCandidateTalentListThrowsSQLException(vertx);

    /*candBOImplTest.getCandidateList(vertx);
    candBOImplTest.getCandidateListThrowsDbHandleException(vertx);
    candBOImplTest.getCandidateListThrowsSQLException(vertx);
*/
    candBOImplTest.getCandidateListBasedOnStatus(vertx);
    candBOImplTest.getCandidateListBasedOnStatusThrowsDbHandleException(vertx);
    candBOImplTest.getCandidateListBasedOnStatusThrowsSQLException(vertx);

    candBOImplTest.getJobId(vertx);
    candBOImplTest.getJobIdThrowsDbHandleException(vertx);
    candBOImplTest.getJobIdThrowsSQLException(vertx);

    candBOImplTest.getCandidatewithLoginId(vertx);
    candBOImplTest.getCandidatewithLoginIdThrowsSQLException(vertx);
    candBOImplTest.getCandidatewithLoginIdThrowsDbHandleException(vertx);

    candBOImplTest.getPostJobFilterDetails(vertx);
    candBOImplTest.getPostJobFilterDetailsThrowsDbHandleException(vertx);
    candBOImplTest.getPostJobFilterDetailsThrowsSQLException(vertx);

    candBOImplTest.updateCandidate(vertx);
    candBOImplTest.updateCandidateThrowsDBHandleException(vertx);
    candBOImplTest.updateCandidateThrowsSQLException(vertx);

    candBOImplTest.updateStatus(vertx);
    candBOImplTest.updateStatusThrowsDBHandleException(vertx);
    candBOImplTest.updateStatusThrowsSQLException(vertx);

    candBOImplTest.updateForwardEmployee(vertx);
    candBOImplTest.updateForwardEmployeeThrowsDBHandleException(vertx);
    candBOImplTest.updateForwardEmployeeThrowsSQLException(vertx);

    candBOImplTest.updateMoveCandidate(vertx);
    candBOImplTest.updateMoveCandidateThrowsDBHandleException(vertx);
    candBOImplTest.updateMoveCandidateThrowsSQLException(vertx);


    candBOImplTest.updateRating(vertx);
    candBOImplTest.updateRatingThrowsDBHandleException(vertx);
    candBOImplTest.updateRatingThrowsSQLException(vertx);

    // candBOImplTest.close();
  }


  @Test
  public void testCandidateHiringProcess() throws SQLException, DbHandleException {
    CandidateHiringProcessBOImplTest candHiringBOImplTest = new CandidateHiringProcessBOImplTest();

    candHiringBOImplTest.setup();

    candHiringBOImplTest.createHiringProcessForCandidate(vertx);

    candHiringBOImplTest.getStatusForCandidate(vertx);
    candHiringBOImplTest.getHiringProcessForCandidate(vertx);

    candHiringBOImplTest.hiringProcessForCandidateThrowsException(vertx);
    candHiringBOImplTest.notCreateHiringProcessForCandidate(vertx);

    // candHiringBOImplTest.close();
  }

  @Test
  public void testCandidateInterview() throws SQLException, DbHandleException, ParseException {
    CandidateInterviewBOImplTest candInterviewBOImplTest = new CandidateInterviewBOImplTest();

    candInterviewBOImplTest.setup();

    candInterviewBOImplTest.addInterviewProcess_Sucess(vertx);
    candInterviewBOImplTest.addInterviewProcess_Fail(vertx);

    candInterviewBOImplTest.getInterviewDetails_Sucess(vertx);
    candInterviewBOImplTest.getInterviewDetails_Fail(vertx);

    candInterviewBOImplTest.getInterviewMailDetails_Sucess(vertx);
    candInterviewBOImplTest.getInterviewMailDetails_Fail(vertx);

    candInterviewBOImplTest.getListOfInterviewDetails_Sucess(vertx);
    candInterviewBOImplTest.getListOfInterviewDetails_Fail(vertx);

    candInterviewBOImplTest.updateInterviewProcess(vertx);
    candInterviewBOImplTest.updateInterviewProcess_Fail(vertx);

    candInterviewBOImplTest.close();
  }

  @Test
  public void testCandidateMailHandler() throws SQLException, DbHandleException {
    CandidateMailHandlerBOImplTest candMailBOImplTest = new CandidateMailHandlerBOImplTest();

    candMailBOImplTest.setup();

    candMailBOImplTest.testCreateMail(vertx);
    candMailBOImplTest.testCreateMailProcessThrowsDbHandleException(vertx);
    candMailBOImplTest.testCreateMailProcessThrowsSQLException(vertx);
    candMailBOImplTest.testNoMailCreated(vertx);

    candMailBOImplTest.testGetMailDetails(vertx);

  }

  @Test
  public void testCandidateOffer() throws SQLException, DbHandleException {
    CandidateOfferBOImplTest candOfferBOImplTest = new CandidateOfferBOImplTest();

    candOfferBOImplTest.setup();

    candOfferBOImplTest.addOfferProcess_Sucess(vertx);
    candOfferBOImplTest.addOfferProcess_Fail(vertx);

    candOfferBOImplTest.getOfferDetails_Sucess(vertx);
    candOfferBOImplTest.getOfferDetails_Fail(vertx);

    candOfferBOImplTest.getOfferMailDetails_Sucess(vertx);
    candOfferBOImplTest.getOfferMailDetails_Fail(vertx);

    candOfferBOImplTest.mailSent(vertx);
    candOfferBOImplTest.mailSent_Fail(vertx);

    candOfferBOImplTest.updateOfferProcess_Sucess(vertx);
    candOfferBOImplTest.updateOfferProcess_Fail(vertx);

    candOfferBOImplTest.close();

  }

  @Test
  public void testRole() throws SQLException, DbHandleException {
    RoleBOImplTest roleBOImplTest = new RoleBOImplTest();
    roleBOImplTest.setup();
    roleBOImplTest.getRoleByName(vertx);
    roleBOImplTest.notGetRoleByName(vertx);
  }

  @Test
  public void testPTOComments() throws SQLException, DbHandleException {
    PTOCommentsBOImplTest ptoCommentsBOImplTest = new PTOCommentsBOImplTest();
    ptoCommentsBOImplTest.setup();
    ptoCommentsBOImplTest.addComment(vertx);
    ptoCommentsBOImplTest.deleteComment(vertx);
    ptoCommentsBOImplTest.addCommentisEmployeeExists(vertx);
    ptoCommentsBOImplTest.getAllComments(vertx);
    ptoCommentsBOImplTest.notAddComment(vertx);
    ptoCommentsBOImplTest.notDeleteComment(vertx);
    ptoCommentsBOImplTest.notGetAllComments(vertx);
    ptoCommentsBOImplTest.updateComment(vertx);
    ptoCommentsBOImplTest.notUpdateComment(vertx);
  }

  @Test
  public void testPayGroup() throws SQLException, DbHandleException {
    PayGropBOImplTest payGropBOImplTest = new PayGropBOImplTest();
    payGropBOImplTest.setup();
    payGropBOImplTest.addPayGroup(vertx);
    payGropBOImplTest.getAllPayGroups(vertx);
    payGropBOImplTest.notAddPayGroup(vertx);
    payGropBOImplTest.updatePayGroupDetails(vertx);
    payGropBOImplTest.notUpdatePayGroupDetails(vertx);
    payGropBOImplTest.deletePayGroup(vertx);
    payGropBOImplTest.notDeletePayGroup(vertx);
    payGropBOImplTest.payGroupThrowsException(vertx);
  }

  @Test
  public void testLocation() throws SQLException, DbHandleException {
    LocationBOImplTest locationBOImplTest = new LocationBOImplTest();
    locationBOImplTest.setup();
    locationBOImplTest.createLocation(vertx);
    locationBOImplTest.createLocationNotSuccessful(vertx);
    locationBOImplTest.createLocationThrowsException(vertx);
    locationBOImplTest.deleteLocation(vertx);
    locationBOImplTest.deleteLocationNoValues(vertx);
    locationBOImplTest.deleteLocationThrowsException(vertx);
    locationBOImplTest.getAllLocations(vertx);
    locationBOImplTest.getAllLocationsNoValues(vertx);
    locationBOImplTest.getAllLocationsThrowsException(vertx);
    locationBOImplTest.getLocation(vertx);
    locationBOImplTest.getLocationNoValues(vertx);
    locationBOImplTest.updateLocation(vertx);
    locationBOImplTest.getLocationThrowsException(vertx);
    locationBOImplTest.updateLocationNoValues(vertx);
    locationBOImplTest.updateLocationThrowsException(vertx);
  }

  @Test
  public void testInvite() throws SQLException, DbHandleException {
    InviteBOImplTest inviteBOImplTest = new InviteBOImplTest();
    inviteBOImplTest.setup();
    inviteBOImplTest.addInvite(vertx);
    inviteBOImplTest.addInvitedEmployeeAdded(vertx);
    inviteBOImplTest.addInvitedEmployeeExistsAlready(vertx);
    inviteBOImplTest.addInvitedEmployeeNoInvitePending(vertx);
    inviteBOImplTest.cancelInviteCancelSuccess(vertx);
    inviteBOImplTest.cancelInviteNoInvitePending(vertx);
    inviteBOImplTest.emailPendingForInviteNewEmployee(vertx);
    inviteBOImplTest.hasInviteForMailAvilable(vertx);
    inviteBOImplTest.hasInviteForMailNotAvilable(vertx);
    inviteBOImplTest.invitedEmployeeNotAdded(vertx);
    inviteBOImplTest.inviteEmployeeExists(vertx);
    inviteBOImplTest.inviteNewEmployeeAdded(vertx);
    inviteBOImplTest.inviteResendEmployeeExists(vertx);
    inviteBOImplTest.inviteResendNoInvitePending(vertx);
    inviteBOImplTest.inviteSendFailure(vertx);
  }

  @Test
  public void testDivision() throws SQLException, DbHandleException {
    DivisionBOImplTest divisionBOImplTest = new DivisionBOImplTest();
    divisionBOImplTest.setup();
    divisionBOImplTest.createDivision(vertx);
    divisionBOImplTest.createDivisionNotSuccessful(vertx);
    divisionBOImplTest.createDivisionThrowsException(vertx);
    divisionBOImplTest.deleteDivision(vertx);
    divisionBOImplTest.deleteDivisionNoValues(vertx);
    divisionBOImplTest.deleteDivisionThrowsException(vertx);
    divisionBOImplTest.getAllDivisions(vertx);
    divisionBOImplTest.getAllDivisionsNoValues(vertx);
    divisionBOImplTest.getAllDivisionsThrowsException(vertx);
    divisionBOImplTest.getDivision(vertx);
    divisionBOImplTest.getDivisionNoValues(vertx);
    divisionBOImplTest.getDivisionThrowsException(vertx);
    divisionBOImplTest.updateDivision(vertx);
    divisionBOImplTest.updateDivisionNoValues(vertx);
    divisionBOImplTest.updateDivisionThrowsException(vertx);
  }

  @Test
  public void testDepartment() throws SQLException, DbHandleException {
    DepartmentBOImplTest departmentBOImplTest = new DepartmentBOImplTest();
    departmentBOImplTest.setup();
    departmentBOImplTest.getAllDeparments(vertx);
    departmentBOImplTest.getDepartmentDetails(vertx);
    departmentBOImplTest.deleteDepartment(vertx);
    departmentBOImplTest.departmentCreateThrowsException(vertx);
    departmentBOImplTest.insertDepartment(vertx);
    departmentBOImplTest.notDeleteDepartment(vertx);
    departmentBOImplTest.notInsertDepartment(vertx);
    departmentBOImplTest.notUpdateDepartmentDetails(vertx);
    departmentBOImplTest.updateDepartmentDetails(vertx);
  }

  @Test
  public void testJob() throws SQLException, DbHandleException {
    JobBOImplTest jobBOImplTest = new JobBOImplTest();
    jobBOImplTest.setup();
    jobBOImplTest.getHiringDetails(vertx);
    jobBOImplTest.getJobAvilableStatus(vertx);
    jobBOImplTest.getJobCount(vertx);
    jobBOImplTest.getJobDetails(vertx);
    jobBOImplTest.getJobDetailsWithoutStatus(vertx);
    jobBOImplTest.getJobStatus(vertx);
    jobBOImplTest.getJobWithCandidateLoginId(vertx);
    jobBOImplTest.notGetAvilableJobStatus(vertx);
    jobBOImplTest.notGetHiringDetails(vertx);
    jobBOImplTest.notGetJobCount(vertx);
    jobBOImplTest.notGetJobDetails(vertx);
    jobBOImplTest.notGetJobStatus(vertx);
    jobBOImplTest.notGetJobDetailsWithoutStatus(vertx);
    jobBOImplTest.notGetJobWithCandidateLoginId(vertx);
    jobBOImplTest.testUpdateCollaborator(vertx);
    jobBOImplTest.testUpdateCollaboratorFails(vertx);
    jobBOImplTest.testJobPostDuplication(vertx);
    jobBOImplTest.testJobPostStatusValidation(vertx);
    jobBOImplTest.testGetJobCollaboratorsBasedOnId(vertx);
    jobBOImplTest.testHiringLeadExists(vertx);
    jobBOImplTest.testJobDetailsForEmployee(vertx);
    jobBOImplTest.testJobDetailsForEmployeeNoValues(vertx);
    jobBOImplTest.testJobDetailsForEmployeeSQLException(vertx);
    jobBOImplTest.testJobDetailsForEmployeeDbHandleException(vertx);
    jobBOImplTest.testCloneJobWorks(vertx);
    jobBOImplTest.testValidCloneJob(vertx);
    jobBOImplTest.testCloneJobThrowsSqlException(vertx);
    jobBOImplTest.testCloneJobThrowsDbHandleException(vertx);
  }

  @Test
  public void testLogin() throws SQLException, DbHandleException {
    LoginBOImplTest loginBOImplTest = new LoginBOImplTest();
    loginBOImplTest.setup();
    loginBOImplTest.addNewUserSuccess(vertx);
    loginBOImplTest.addNewUserFail(vertx);
    loginBOImplTest.updateConsumerId(vertx);
    loginBOImplTest.isUserExistsSuccess(vertx);
    loginBOImplTest.isUserExistsFail(vertx);
    loginBOImplTest.changePasswordSuccess(vertx);
    loginBOImplTest.changePasswordSuccessOldPasswordFails(vertx);
    loginBOImplTest.approveEmployeeSignupSuccess(vertx);
    loginBOImplTest.approveEmployeeSignupFail(vertx);
    loginBOImplTest.validateCredentialsAndGetDetailsSuccess(vertx);
    loginBOImplTest.validateCredentialsAndGetDetailsFail(vertx);
    loginBOImplTest.getLoginByIdSuccess(vertx);
    loginBOImplTest.getLoginByIdFail(vertx);
    loginBOImplTest.candidateLoginCheckSuccess(vertx);
    loginBOImplTest.candidateLoginCheckFail(vertx);
    loginBOImplTest.resetPasswordSuccess(vertx);
    loginBOImplTest.resetPasswordEmployeeNotExists(vertx);
    loginBOImplTest.resetPasswordInvalidData(vertx);
    loginBOImplTest.generateTokenSuccess(vertx);
    loginBOImplTest.generateTokenSuccessFail(vertx);
    loginBOImplTest.checkResetMailSuccess(vertx);
    loginBOImplTest.checkResetMailFail(vertx);
    loginBOImplTest.checkResetMailWithExpiry(vertx);
    loginBOImplTest.checkResetUsedForCandidate(vertx);
    loginBOImplTest.checkResetUsedForEmployee(vertx);
    loginBOImplTest.changePasswordForResetSuccess(vertx);
    loginBOImplTest.changePasswordForResetFail(vertx);
  }

  @Test
  public void testOrganization() throws SQLException, DbHandleException {
    OrganizationBOImplTest organizationBOImplTest = new OrganizationBOImplTest();
    organizationBOImplTest.setup();
    organizationBOImplTest.getOrganizationDetails_Success(vertx);
    organizationBOImplTest.getOrganizationDetails_Fail(vertx);
    organizationBOImplTest.getOrganizationWithDomainName_Success(vertx);
    organizationBOImplTest.getOrganizationWithDomainName_Fail(vertx);
    organizationBOImplTest.isDomainAvailable_Success(vertx);
    organizationBOImplTest.isDomainAvailable_Fail(vertx);
    organizationBOImplTest.addNewAtsDomain(vertx);
    organizationBOImplTest.updateDomains(vertx);
    organizationBOImplTest.addOrganizationDetails_Success(vertx);
    organizationBOImplTest.addOrganizationDetails_Fail(vertx);
  }

  @Test
  public void testReports() throws SQLException, DbHandleException {
    ReportBOImplTest reportBOImplTest = new ReportBOImplTest();
    reportBOImplTest.setup();
    reportBOImplTest.testCandidateStatusReportsIsEmpty();
    reportBOImplTest.testCandidateStatusReportsIsNotEmpty();
    reportBOImplTest.testJobPostReportsIsEmpty();
    reportBOImplTest.testJobPostReportsIsNotEmpty();
  }


  @Test
  public void testMinimumExperience() throws SQLException, DbHandleException {
    MinimumExperienceBOImplTest minimumExperienceBOImplTest = new MinimumExperienceBOImplTest();
    minimumExperienceBOImplTest.setup();
    minimumExperienceBOImplTest.createMinimumExperience(vertx);
    minimumExperienceBOImplTest.createMinimumExperienceNotSuccessful(vertx);
    minimumExperienceBOImplTest.createMinimumExperienceThrowsSQLException(vertx);
    minimumExperienceBOImplTest.createMinimumExperienceThrowsDbHandleException(vertx);
    minimumExperienceBOImplTest.getAllMinimumExperiences(vertx);
    minimumExperienceBOImplTest.getAllMinimumExperiencesNoValues(vertx);
    minimumExperienceBOImplTest.getAllMinimumExperiencesThrowsSQLException(vertx);
    minimumExperienceBOImplTest.getAllMinimumExperiencesThrowsDbHandleException(vertx);
    minimumExperienceBOImplTest.getMinimumExperience(vertx);
    minimumExperienceBOImplTest.getMinimumExperienceNoSuchValues(vertx);
    minimumExperienceBOImplTest.getMinimumExperienceThrowsSQLException(vertx);
    minimumExperienceBOImplTest.getMinimumExperienceThrowsDbHandleException(vertx);
    minimumExperienceBOImplTest.updateMinimumExperience(vertx);
    minimumExperienceBOImplTest.updateMinimumExperienceNoChanges(vertx);
    minimumExperienceBOImplTest.updateMinimumExperienceThrowsSQLException(vertx);
    minimumExperienceBOImplTest.updateMinimumExperienceThrowsDbHandleException(vertx);
    minimumExperienceBOImplTest.deleteMinimumExperience(vertx);
    minimumExperienceBOImplTest.deleteMinimumExperienceWithNoChanges(vertx);
    minimumExperienceBOImplTest.deleteMinimumExperienceThrowsSQLException(vertx);
    minimumExperienceBOImplTest.deleteMinimumExperienceThrowsDbHandleException(vertx);
  }
}
