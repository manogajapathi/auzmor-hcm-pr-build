package com.auzmor.unittest;


import com.auzmor.dao.DepartmentDAO;
import com.auzmor.impl.bo.DepartmentBOImpl;
import com.auzmor.impl.enumarator.DepartmentEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.DepartmentModel;
import com.auzmor.util.bo.ReturnObject;

import java.util.List;

import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonObject;
import io.vertx.core.Vertx;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.sql.SQLException;


import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DepartmentBOImplTest {

  @Mock
  DepartmentDAO departmentDAO;
  private DepartmentBOImpl departmentBO;
  private DepartmentModel departmentModel;
  ReturnObject <DepartmentEnum, JsonObject> returnObj;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    departmentModel = new DepartmentModel();
  }


  public void insertDepartment(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          DepartmentBOImpl departmentBO = new DepartmentBOImpl(dbHandle);
          departmentBO.setDepartmentDAO(departmentDAO);
          when(departmentDAO.insert(departmentModel)).thenReturn(new Integer(1));

          ReturnObject <DepartmentEnum, JsonObject> insertCount = departmentBO.addDepartment(dbHandle,departmentModel);

          assert (insertCount == returnObj);
          verify(departmentDAO.insert(departmentModel));
        });
  }


  public void notInsertDepartment(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          DepartmentBOImpl departmentBO = new DepartmentBOImpl(dbHandle);
          departmentBO.setDepartmentDAO(departmentDAO);
          when(departmentDAO.insert(departmentModel)).thenReturn(new Integer(0));

          ReturnObject <DepartmentEnum, JsonObject> insertCount = departmentBO.addDepartment(dbHandle,departmentModel);

          assert (insertCount == returnObj);
          verify(departmentDAO).insert(departmentModel);
        });
  }


  public void getDepartmentDetails(Vertx vertx) throws SQLException,DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          departmentBO.setDepartmentDAO(departmentDAO);
          when(departmentDAO.getDepartmentDetails(departmentModel)).thenReturn(departmentModel);

          assert(departmentModel!=null);
          verify(departmentDAO).getDepartmentDetails(departmentModel);
        });
  }


  public void updateDepartmentDetails(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          MultiMap requestParams = null;
          DepartmentBOImpl departmentBO = new DepartmentBOImpl(dbHandle);
          departmentBO.setDepartmentDAO(departmentDAO);
          when(departmentDAO.updateDepartmentDetails(requestParams)).thenReturn(new Integer(1));

          ReturnObject <DepartmentEnum, JsonObject> updateCount = departmentBO.updateDepartment(dbHandle,requestParams);

          assert (updateCount==returnObj);
          verify(departmentDAO).updateDepartmentDetails(requestParams);
        });
  }


  public void notUpdateDepartmentDetails(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          MultiMap requestParams = null;
          DepartmentBOImpl departmentBO = new DepartmentBOImpl(dbHandle);
          departmentBO.setDepartmentDAO(departmentDAO);
          when(departmentDAO.updateDepartmentDetails(requestParams)).thenReturn(new Integer(0));

          ReturnObject <DepartmentEnum, JsonObject> updateCount = departmentBO.updateDepartment(dbHandle,requestParams);

          assert (updateCount==returnObj);
          verify(departmentDAO).updateDepartmentDetails(requestParams);
        });
  }


  public void deleteDepartment(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          DepartmentBOImpl departmentBO = new DepartmentBOImpl(dbHandle);
          departmentBO.setDepartmentDAO(departmentDAO);
          when(departmentDAO.deleteDepartment(departmentModel)).thenReturn(new Integer(1));

          ReturnObject <DepartmentEnum, JsonObject> deleteCount = departmentBO.deleteDepartment(dbHandle,departmentModel);
          assert (deleteCount==returnObj);
          verify(departmentDAO).deleteDepartment(departmentModel);
        });
  }


  public void notDeleteDepartment(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          DepartmentBOImpl departmentBO = new DepartmentBOImpl(dbHandle);
          departmentBO.setDepartmentDAO(departmentDAO);
          when(departmentDAO.deleteDepartment(departmentModel)).thenReturn(new Integer(0));

          ReturnObject <DepartmentEnum, JsonObject> deleteCount = departmentBO.deleteDepartment(dbHandle,departmentModel);
          assert (deleteCount==returnObj);
          verify(departmentDAO).deleteDepartment(departmentModel);
        });
  }


  public void getAllDeparments (Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          departmentBO.setDepartmentDAO(departmentDAO);
          List<DepartmentModel> resultList = departmentDAO.getAllDepartments(departmentModel);
          when(departmentDAO.getAllDepartments(departmentModel)).thenReturn(resultList);

          assert(resultList!=null);
          verify(departmentDAO).getAllDepartments(departmentModel);

        });
  }


  public void departmentCreateThrowsException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
           departmentBO = new DepartmentBOImpl(dbHandle);
          departmentBO.setDepartmentDAO(departmentDAO);
          when(departmentDAO.insert(departmentModel)).thenThrow(SQLException.class);

          ReturnObject <DepartmentEnum, JsonObject> insertCount = departmentBO.addDepartment(dbHandle,departmentModel);
        });
  }
}
