package com.auzmor.unittest;

import com.auzmor.dao.PTOCommentsDAO;
import com.auzmor.impl.bo.PTOCommentsBOImpl;
import com.auzmor.impl.dao.PTOCommentsDAOImpl;
import com.auzmor.impl.enumarator.bo.PTOCommentsEnum;
import com.auzmor.impl.model.PTO.PTOComments;
import com.auzmor.util.bo.ReturnObject;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.Vertx;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.sql.SQLException;


import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PTOCommentsBOImplTest {

  @Mock
  PTOCommentsDAO ptoCommentsDAO;
  private PTOCommentsBOImpl ptoCommentsBO;
  private PTOComments ptoComments;
  MultiMap params;
  List<JsonObject> ptoCommentsList = new ArrayList<>();
  ReturnObject <PTOCommentsEnum, JsonObject> returnObj;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    ptoComments = new PTOComments();
  }



  public void addComment(Vertx vertx){
  DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
      dbHandle -> {
         ptoCommentsBO = new PTOCommentsBOImpl(dbHandle);
        ptoCommentsBO.setPtoCommentsDAO(ptoCommentsDAO);
        when(ptoCommentsDAO.addComment(ptoComments)).thenReturn(new Integer(1));

        ReturnObject <PTOCommentsEnum, JsonObject> addCount = ptoCommentsBO.addComment(dbHandle,params);

        assert (addCount == returnObj);
        verify(ptoCommentsDAO.addComment(ptoComments));
      });
}


  public void notAddComment(Vertx vertx){
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          ptoCommentsBO = new PTOCommentsBOImpl(dbHandle);
          ptoCommentsBO.setPtoCommentsDAO(ptoCommentsDAO);
          when(ptoCommentsDAO.addComment(ptoComments)).thenReturn(new Integer(0));

          ReturnObject <PTOCommentsEnum, JsonObject> addCount = ptoCommentsBO.addComment(dbHandle,params);

          assert (addCount == returnObj);
          verify(ptoCommentsDAO.addComment(ptoComments));
        });
  }


  public void addCommentisEmployeeExists(Vertx vertx){
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          ptoCommentsBO = new PTOCommentsBOImpl(dbHandle);
          ptoCommentsDAO = new PTOCommentsDAOImpl(dbHandle);
          when(ptoCommentsDAO.alreadyExists(ptoComments)).thenReturn(true);

          ReturnObject <PTOCommentsEnum, JsonObject> addCount = ptoCommentsBO.addComment(dbHandle,params);

          assert (addCount == returnObj);
          verify(ptoCommentsDAO.addComment(ptoComments));
        });
  }


  public void getAllComments(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          ptoCommentsBO = new PTOCommentsBOImpl(dbHandle);
          ptoCommentsBO.setPtoCommentsDAO(ptoCommentsDAO);
          when(ptoCommentsDAO.getAllComments(ptoComments)).thenReturn(ptoCommentsList);

          try {
            ReturnObject<PTOCommentsEnum, JsonArray> returnComments = ptoCommentsBO.getAllComments(dbHandle, params);
            assert (null != returnComments);
            verify(ptoCommentsDAO.getAllComments(ptoComments));
          }
          catch (ParseException pe){
            pe.printStackTrace();
          }
        });
  }


  public void notGetAllComments(Vertx vertx) {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          ptoCommentsBO = new PTOCommentsBOImpl(dbHandle);
          ptoCommentsBO.setPtoCommentsDAO(ptoCommentsDAO);
          when(ptoCommentsDAO.getAllComments(ptoComments)).thenReturn(ptoCommentsList);
          try {
            ReturnObject<PTOCommentsEnum, JsonArray> returnComments = ptoCommentsBO.getAllComments(dbHandle, params);
            assert (null == returnComments);
            verify(ptoCommentsDAO.getAllComments(ptoComments));
          }
          catch (ParseException pe){
            pe.printStackTrace();
          }
        });
  }


  public void updateComment(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          ptoCommentsBO = new PTOCommentsBOImpl(dbHandle);
          ptoCommentsBO.setPtoCommentsDAO(ptoCommentsDAO);
          when(ptoCommentsDAO.updateComment(ptoComments)).thenReturn(new Integer(1));

          ReturnObject <PTOCommentsEnum, JsonObject> updateCount = ptoCommentsBO.updateComment(dbHandle,params);

          assert (updateCount == returnObj);
          verify(ptoCommentsDAO.updateComment(ptoComments));
        });
}


  public void notUpdateComment(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          ptoCommentsBO = new PTOCommentsBOImpl(dbHandle);
          ptoCommentsBO.setPtoCommentsDAO(ptoCommentsDAO);
          when(ptoCommentsDAO.updateComment(ptoComments)).thenReturn(new Integer(0));

          ReturnObject <PTOCommentsEnum, JsonObject> updateCount = ptoCommentsBO.updateComment(dbHandle,params);

          assert (updateCount == returnObj);
          verify(ptoCommentsDAO.updateComment(ptoComments));
        });
  }


  public void deleteComment(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          ptoCommentsBO = new PTOCommentsBOImpl(dbHandle);
          ptoCommentsBO.setPtoCommentsDAO(ptoCommentsDAO);
          when(ptoCommentsDAO.deleteComment(ptoComments)).thenReturn(new Integer(1));

          ReturnObject <PTOCommentsEnum, JsonObject> deleteCount = ptoCommentsBO.deleteComment(dbHandle,params);

          assert (deleteCount == returnObj);
          verify(ptoCommentsDAO.deleteComment(ptoComments));
        });
  }


  public void notDeleteComment(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          ptoCommentsBO = new PTOCommentsBOImpl(dbHandle);
          ptoCommentsBO.setPtoCommentsDAO(ptoCommentsDAO);
          when(ptoCommentsDAO.deleteComment(ptoComments)).thenReturn(new Integer(0));

          ReturnObject <PTOCommentsEnum, JsonObject> deleteCount = ptoCommentsBO.deleteComment(dbHandle,params);

          assert (deleteCount == returnObj);
          verify(ptoCommentsDAO.deleteComment(ptoComments));
        });
  }

}

