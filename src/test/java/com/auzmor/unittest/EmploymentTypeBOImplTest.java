package com.auzmor.unittest;

import com.auzmor.bo.EmploymentTypeBO;
import com.auzmor.dao.EmploymentTypeDAO;
import com.auzmor.impl.bo.EmploymentTypeBOImpl;
import com.auzmor.impl.dao.EmploymentTypeDAOImpl;
import com.auzmor.impl.enumarator.EmploymentTypeEnum;
import com.auzmor.impl.enumarator.LocationEnum;
import com.auzmor.impl.enumarator.TitleEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.EmploymentModel;
import com.auzmor.util.bo.ReturnObject;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EmploymentTypeBOImplTest {
  @Mock
  EmploymentTypeDAO employmentTypeDAO;
  private EmploymentTypeBO employmentTypeBO;
  private EmploymentModel employmentModel;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    employmentModel = new EmploymentModel();
  }

  
  public void createEmploymentType() throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,
        dbHandle -> {
          employmentTypeBO = new EmploymentTypeBOImpl();
          employmentTypeDAO = new EmploymentTypeDAOImpl(dbHandle);
          when(employmentTypeDAO.insert(employmentModel)).thenReturn(new Integer(1));

          ReturnObject<EmploymentTypeEnum, JsonObject> boReponse =
              employmentTypeBO.addEmploymentType(dbHandle, employmentModel);

          assert (boReponse.getStatusEnum() == EmploymentTypeEnum.ADDITION_SUCCESS);
          verify(employmentTypeDAO).insert(employmentModel);
        }
    );
  }

  
  public void createEmploymentTypeNoSuchValues() throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,
        dbHandle -> {
          employmentTypeBO = new EmploymentTypeBOImpl();
          employmentTypeDAO = new EmploymentTypeDAOImpl(dbHandle);
          when(employmentTypeDAO.insert(employmentModel)).thenReturn(new Integer(0));

          ReturnObject<EmploymentTypeEnum, JsonObject> boReponse =
              employmentTypeBO.addEmploymentType(dbHandle, employmentModel);

          assert (boReponse.getStatusEnum() == EmploymentTypeEnum.ADDITION_SUCCESS);
          verify(employmentTypeDAO).insert(employmentModel);
        }
    );
  }

  
  public void createEmploymentTypeThrowsException() throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,
        dbHandle -> {
          employmentTypeBO = new EmploymentTypeBOImpl();
          employmentTypeDAO = new EmploymentTypeDAOImpl(dbHandle);
          when(employmentTypeDAO.insert(employmentModel)).thenThrow(SQLException.class);

          ReturnObject<EmploymentTypeEnum, JsonObject> boReponse =
              employmentTypeBO.addEmploymentType(dbHandle, employmentModel);
        }
    );
  }

  
  public void getAllEmploymentTypes() throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,
        dbHandle -> {
          employmentTypeBO = new EmploymentTypeBOImpl();
          employmentTypeDAO = new EmploymentTypeDAOImpl(dbHandle);
          List<EmploymentModel> employmentModelList = new ArrayList<>();
          when(employmentTypeDAO.getAllEmploymentTypes(employmentModel)).thenReturn(employmentModelList);

          ReturnObject<EmploymentTypeEnum, JsonArray> boReponse =
              employmentTypeBO.getAllEmploymentTypes(dbHandle, employmentModel);

          assert (boReponse.getStatusEnum() == EmploymentTypeEnum.VALUE_SUCCESS);
          verify(employmentTypeDAO).getAllEmploymentTypes(employmentModel);
        }
    );
  }

  
  public void getAllEmploymentTypesNoSuchValues() throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,
        dbHandle -> {
          employmentTypeBO = new EmploymentTypeBOImpl();
          employmentTypeDAO = new EmploymentTypeDAOImpl(dbHandle);
          List<EmploymentModel> employmentModelList = new ArrayList<>();
          when(employmentTypeDAO.getAllEmploymentTypes(employmentModel)).thenReturn(null);

          ReturnObject<EmploymentTypeEnum, JsonArray> boReponse =
              employmentTypeBO.getAllEmploymentTypes(dbHandle, employmentModel);

          assert (boReponse.getStatusEnum() == EmploymentTypeEnum.NO_SUCH_VALUE);
          verify(employmentTypeDAO).getAllEmploymentTypes(employmentModel);
        }
    );
  }

  
  public void getAllEmploymentTypesThrowException() throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,
        dbHandle -> {
          employmentTypeBO = new EmploymentTypeBOImpl();
          employmentTypeDAO = new EmploymentTypeDAOImpl(dbHandle);
          List<EmploymentModel> employmentModelList = new ArrayList<>();
          when(employmentTypeDAO.getAllEmploymentTypes(employmentModel)).thenThrow(SQLException.class);

          ReturnObject<EmploymentTypeEnum, JsonArray> boReponse =
              employmentTypeBO.getAllEmploymentTypes(dbHandle, employmentModel);
        }
    );
  }

	
	public void getEmploymentType() throws SQLException, DbHandleException {
		DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,
				dbHandle -> {
					employmentTypeBO = new EmploymentTypeBOImpl();
					employmentTypeDAO = new EmploymentTypeDAOImpl(dbHandle);
					when(employmentTypeDAO.getEmploymentTypeDetails(employmentModel)).thenReturn(employmentModel);

					ReturnObject<EmploymentTypeEnum, JsonObject> boReponse =
							employmentTypeBO.getEmploymentType(dbHandle, employmentModel);

					assert (boReponse.getStatusEnum() == EmploymentTypeEnum.VALUE_SUCCESS);
					verify(employmentTypeDAO).getEmploymentTypeDetails(employmentModel);
				}
		);
	}

	
	public void getEmploymentTypeNoSuchValues() throws SQLException, DbHandleException {
		DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,
				dbHandle -> {
					employmentTypeBO = new EmploymentTypeBOImpl();
					employmentTypeDAO = new EmploymentTypeDAOImpl(dbHandle);
					when(employmentTypeDAO.getEmploymentTypeDetails(employmentModel)).thenReturn(null);

					ReturnObject<EmploymentTypeEnum, JsonObject> boReponse =
							employmentTypeBO.getEmploymentType(dbHandle, employmentModel);

					assert (boReponse.getStatusEnum() == EmploymentTypeEnum.NO_SUCH_VALUE);
					verify(employmentTypeDAO).getEmploymentTypeDetails(employmentModel);
				}
		);
	}

	
	public void getEmploymentTypeThrowException() throws SQLException, DbHandleException {
		DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,
				dbHandle -> {
					employmentTypeBO = new EmploymentTypeBOImpl();
					employmentTypeDAO = new EmploymentTypeDAOImpl(dbHandle);
					when(employmentTypeDAO.getEmploymentTypeDetails(employmentModel)).thenThrow(SQLException.class);

					ReturnObject<EmploymentTypeEnum, JsonObject> boReponse =
							employmentTypeBO.getEmploymentType(dbHandle, employmentModel);
				}
		);
	}

	
	public void updateEmploymentType() throws SQLException, DbHandleException {
		DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,
				dbHandle -> {
					employmentTypeBO = new EmploymentTypeBOImpl();
					employmentTypeDAO = new EmploymentTypeDAOImpl(dbHandle);
					MultiMap params = null;
					when(employmentTypeDAO.updateEmploymentTypeDetails(params)).thenReturn(new Integer(1));

					ReturnObject<EmploymentTypeEnum, JsonObject> boReponse =
							employmentTypeBO.updateEmploymentType(dbHandle, params);

					assert (boReponse.getStatusEnum() == EmploymentTypeEnum.UPDATE_SUCCESS);
					verify(employmentTypeDAO).updateEmploymentTypeDetails(params);
				}
		);
	}

	
	public void updateEmploymentTypeNoSuchValues() throws SQLException, DbHandleException {
		DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,
				dbHandle -> {
					employmentTypeBO = new EmploymentTypeBOImpl();
					employmentTypeDAO = new EmploymentTypeDAOImpl(dbHandle);
					MultiMap params = null;
					when(employmentTypeDAO.updateEmploymentTypeDetails(params)).thenReturn(new Integer(0));

					ReturnObject<EmploymentTypeEnum, JsonObject> boReponse =
							employmentTypeBO.updateEmploymentType(dbHandle, params);

					assert (boReponse.getStatusEnum() == EmploymentTypeEnum.NO_SUCH_VALUE);
					verify(employmentTypeDAO).updateEmploymentTypeDetails(params);
				}
		);
	}

	
	public void updateEmploymentTypeThrowException() throws SQLException, DbHandleException {
		DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,
				dbHandle -> {
					employmentTypeBO = new EmploymentTypeBOImpl();
					employmentTypeDAO = new EmploymentTypeDAOImpl(dbHandle);
					MultiMap params = null;
					when(employmentTypeDAO.updateEmploymentTypeDetails(params)).thenThrow(SQLException.class);

					ReturnObject<EmploymentTypeEnum, JsonObject> boReponse =
							employmentTypeBO.updateEmploymentType(dbHandle, params);
				}
		);
	}

	
	public void deleteEmploymentType() throws SQLException, DbHandleException {
		DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,
				dbHandle -> {
					employmentTypeBO = new EmploymentTypeBOImpl();
					employmentTypeDAO = new EmploymentTypeDAOImpl(dbHandle);
					when(employmentTypeDAO.deleteEmploymentType(employmentModel)).thenReturn(new Integer(1));

					ReturnObject<EmploymentTypeEnum, JsonObject> boReponse =
							employmentTypeBO.deleteEmploymentType(dbHandle, employmentModel);

					assert (boReponse.getStatusEnum() == EmploymentTypeEnum.DELETE_SUCCESS);
					verify(employmentTypeDAO).deleteEmploymentType(employmentModel);
				}
		);
	}
}
