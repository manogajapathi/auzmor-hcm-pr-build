package com.auzmor.unittest;

import com.auth0.jwt.exceptions.InvalidClaimException;
import com.auzmor.impl.bo.LoginBOImpl;
import com.auzmor.impl.dao.LoginDAOImpl;
import com.auzmor.impl.enumarator.bo.LoginBOEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.Login;
import com.auzmor.impl.util.bo.ReturnObjectImpl;
import com.auzmor.util.bo.ReturnObject;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.junit.Before;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LoginBOImplTest {
  @Mock
  private LoginDAOImpl loginDAO;
  private LoginBOImpl loginBO;
  private Login login;
  private MultiMap params;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    login = new Login();
  }

  public void addNewUserSuccess(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          when(loginDAO.create(login)).thenReturn(new Integer(1));
          long check = loginBO.addNewUser(params);
          assert (check == 1);
          verify(loginDAO.create(login));
        });
  }

  public void addNewUserFail(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          when(loginDAO.create(login)).thenReturn(new Integer(0));
          long check = loginBO.addNewUser(params);
          assert (check == 0);
          verify(loginDAO.create(login));
        });
  }

  public void updateConsumerId(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          loginDAO.updateConsumerId("", Long.parseLong("1"));
          loginBO.updateConsumerId("", Long.parseLong("1"));
        });
  }

  public void isUserExistsSuccess(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          when(loginDAO.isLoginAlreadyExists("abc@gmail.com", Long.parseLong("1"))).thenReturn(true);
          boolean check = loginBO.isUserExists("abc@gmail.com", Long.parseLong("1"));
          assert (check);
          verify(loginDAO.isLoginAlreadyExists("abc@gmail.com", Long.parseLong("1")));
        });
  }

  public void isUserExistsFail(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          when(loginDAO.isLoginAlreadyExists("abc@gmail.com", Long.parseLong("1"))).thenReturn(false);
          boolean check = loginBO.isUserExists("abc@gmail.com", Long.parseLong("1"));
          assert (!check);
          verify(loginDAO.isLoginAlreadyExists("abc@gmail.com", Long.parseLong("1")));
        });
  }

  public void changePasswordSuccess(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          when(loginDAO.updatePassword("abc@12", 1)).thenReturn(1);
          LoginBOEnum check = loginBO.changePassword(params);
          assert (check == LoginBOEnum.PASSWORD_CHANGED);
          verify(loginDAO.updatePassword("abc@12", 1));
        });
  }

  public void changePasswordSuccessOldPasswordFails(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          when(loginDAO.updatePassword("abc@12", 1)).thenReturn(0);
          LoginBOEnum check = loginBO.changePassword(params);
          assert (check == LoginBOEnum.PASSWORD_INCORRECT);
          verify(loginDAO.updatePassword("abc@12", 1));
        });
  }

  public void approveEmployeeSignupSuccess(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          LoginBOEnum loginBOEnum = null;
          when(loginDAO.approveEmployeeSignup(params)).thenReturn(loginBOEnum);
          LoginBOEnum check = loginBO.approveEmployeeSignup(params);
          assertNotNull(check);
          verify(loginDAO.approveEmployeeSignup(params));
        });
  }

  public void approveEmployeeSignupFail(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          LoginBOEnum loginBOEnum = null;
          when(loginDAO.approveEmployeeSignup(params)).thenReturn(loginBOEnum);
          LoginBOEnum check = loginBO.approveEmployeeSignup(params);
          assertNull(check);
          verify(loginDAO.approveEmployeeSignup(params));
        });
  }

  public void validateCredentialsAndGetDetailsSuccess(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          when(loginDAO.isValidCredentialAndGetLogin("abc@12", "pwd", Long.parseLong("1"))).thenReturn(login);
          login = loginBO.validateCredentialsAndGetDetails("abc@12", "pwd", Long.parseLong("1"));
          assertNotNull(login);
          verify(loginDAO.isValidCredentialAndGetLogin("abc@12", "pwd", Long.parseLong("1")));
        });
  }

  public void validateCredentialsAndGetDetailsFail(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          when(loginDAO.isValidCredentialAndGetLogin("abc@12", "pwd", Long.parseLong("1"))).thenReturn(login);
          login = loginBO.validateCredentialsAndGetDetails("abc@12", "pwd", Long.parseLong("1"));
          assertNull(login);
          verify(loginDAO.isValidCredentialAndGetLogin("abc@12", "pwd", Long.parseLong("1")));
        });
  }

  public void getLoginByIdSuccess(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          when(loginDAO.getLoginDetails(Long.parseLong("1"))).thenReturn(login);
          login = loginBO.getLoginById(Long.parseLong("1"));
          assertNotNull(login);
          verify(loginDAO.getLoginDetails(Long.parseLong("1")));
        });
  }

  public void getLoginByIdFail(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          when(loginDAO.getLoginDetails(Long.parseLong("1"))).thenReturn(login);
          login = loginBO.getLoginById(Long.parseLong("1"));
          assertNull(login);
          verify(loginDAO.getLoginDetails(Long.parseLong("1")));
        });
  }

  public void candidateLoginCheckSuccess(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          when(loginDAO.candidateLoginCheck(login)).thenReturn(true);
          boolean check = loginBO.candidateLoginCheck(login);
          assert (check);
          verify(loginDAO.candidateLoginCheck(login));
        });
  }

  public void candidateLoginCheckFail(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          when(loginDAO.candidateLoginCheck(login)).thenReturn(true);
          boolean check = loginBO.candidateLoginCheck(login);
          assert (!check);
          verify(loginDAO.candidateLoginCheck(login));
        });
  }

  public void resetPasswordSuccess(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          when(loginDAO.updateKeyValue(login)).thenReturn(1);
          LoginBOEnum check = null;
          try {
            check = loginBO.resetPassword(params);
          } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
          }
          assert (check == LoginBOEnum.RESET_MAIL_SENT);
          verify(loginDAO.updateKeyValue(login));
        });
  }

  public void resetPasswordEmployeeNotExists(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          when(loginDAO.updateKeyValue(login)).thenReturn(0);
          LoginBOEnum check = null;
          try {
            check = loginBO.resetPassword(params);
          } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
          }
          assert (check == LoginBOEnum.EMPLOYEE_NOT_EXISTS);
          verify(loginDAO.updateKeyValue(login));
        });
  }

  public void resetPasswordInvalidData(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          when(loginDAO.updateKeyValue(login)).thenReturn(0);
          LoginBOEnum check = null;
          try {
            check = loginBO.resetPassword(params);
          } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
          }
          assert (check == LoginBOEnum.INVALID_DATA);
          verify(loginDAO.updateKeyValue(login));
        });
  }

  public void generateTokenSuccess(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          String check = null;
          try {
            check = loginBO.generateToken(login);
          } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
          }
          assertNotNull(check);
        });
  }

  public void generateTokenSuccessFail(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          String check = null;
          try {
            check = loginBO.generateToken(login);
          } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
          }
          assertNull(check);
        });
  }

  public void checkResetMailSuccess(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          when(loginDAO.getKeyValues(login)).thenReturn(new Login());
          ReturnObject<LoginBOEnum, JsonObject> check = null;
          try {
            check = loginBO.checkResetMail(params);
          } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
          }
          assertNotNull(check);
          verify(loginDAO.getKeyValues(login));
        });
  }

  public void checkResetMailFail(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          when(loginDAO.getKeyValues(login)).thenReturn(null);
          ReturnObject<LoginBOEnum, JsonObject> check = null;
          try {
            check = loginBO.checkResetMail(params);
          } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
          }
          assertNull(check);
          verify(loginDAO.getKeyValues(login));
        });
  }

  public void checkResetMailWithExpiry(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          when(loginDAO.getKeyValues(login)).thenThrow(InvalidClaimException.class);
          ReturnObject<LoginBOEnum, JsonObject> check = null;
          try {
            check = loginBO.checkResetMail(params);
          } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
          }
          assertNull(check);
          verify(loginDAO.getKeyValues(login));
        });
  }

  public void checkResetUsedForCandidate(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          login.setKeyValue("7d5111b1-7d85-442f-95b1-45a3a1a7f796");
          login.setHashValue("");
          when(loginDAO.getKeyValues(login)).thenReturn(login);
          JsonObject jsonObject = new JsonObject();
          jsonObject.put("hris_url", "ats.auzmor-hcm.com");
          jsonObject.put("user_type", "candidate");
          ReturnObject<LoginBOEnum, JsonObject> check = new ReturnObjectImpl<LoginBOEnum, JsonObject>
              (LoginBOEnum.LINK_USED_CANDIDATE, jsonObject);
          try {
            check = loginBO.checkResetMail(params);
          } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
          }
          assertNull(check);
          verify(loginDAO.getKeyValues(login));
        });
  }

  public void checkResetUsedForEmployee(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          login.setKeyValue("7d5111b1-7d85-442f-95b1-45a3a1a7f796");
          login.setHashValue("");
          when(loginDAO.getKeyValues(login)).thenReturn(login);
          JsonObject jsonObject = new JsonObject();
          jsonObject.put("hris_url", "ats.auzmor-hcm.com");
          jsonObject.put("user_type", "employee");
          ReturnObject<LoginBOEnum, JsonObject> check = new ReturnObjectImpl<LoginBOEnum, JsonObject>
              (LoginBOEnum.LINK_USED_EMPLOYEE, jsonObject);
          try {
            check = loginBO.checkResetMail(params);
          } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
          }
          assertNull(check);
          verify(loginDAO.getKeyValues(login));
        });
  }

  public void changePasswordForResetSuccess(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          when(loginDAO.updatePasswordWithKeyValue(login)).thenReturn(0);
          ReturnObject<LoginBOEnum, JsonObject> check = null;
          check = loginBO.changePasswordForReset(params);
          assert (check.getStatusEnum() == LoginBOEnum.PASSWORD_CHANGED);
          verify(loginDAO.updatePasswordWithKeyValue(login));
        });
  }

  public void changePasswordForResetFail(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          loginBO = new LoginBOImpl(dbHandle);
          loginBO.setLoginDAO(loginDAO);
          when(loginDAO.updatePasswordWithKeyValue(login)).thenReturn(0);
          ReturnObject<LoginBOEnum, JsonObject> check = null;
          check = loginBO.changePasswordForReset(params);
          assert (check.getStatusEnum() == LoginBOEnum.PASSWORD_NOCHANGE);
          verify(loginDAO.updatePasswordWithKeyValue(login));
        });
  }


}
