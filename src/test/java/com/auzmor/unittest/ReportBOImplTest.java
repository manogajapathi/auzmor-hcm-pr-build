package com.auzmor.unittest;

import com.auzmor.dao.ReportDAO;
import com.auzmor.impl.bo.ReportBOImpl;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.Job.Job;
import com.auzmor.impl.models.CandidateR.CandidateR;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.junit.Before;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ReportBOImplTest {
  @Mock
  ReportDAO reportDAO;
  private ReportBOImpl reportBO;
  private Job job;
  private CandidateR candidate;
  List<JsonObject> jobPostReportList;
  List<JsonObject> candidateStatusReportList;



  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    job = new Job();
    candidate = new CandidateR();
  }

  public void testJobPostReportsIsEmpty() throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false, true,
        dbHandle -> {
          reportBO = new ReportBOImpl(dbHandle);
          reportBO.setReportDAO(reportDAO);
          when(reportDAO.getJobReports(job)).thenReturn(jobPostReportList);
          assert(jobPostReportList.isEmpty());
          verify(reportDAO.getJobReports(job));
        });
  }

  public void testJobPostReportsIsNotEmpty() throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false, true,
        dbHandle ->{
          reportBO = new ReportBOImpl(dbHandle);
          reportBO.setReportDAO(reportDAO);
          when(reportDAO.getJobReports(job)).thenReturn(jobPostReportList);
          assert(!jobPostReportList.isEmpty());
          verify(reportDAO.getJobReports(job));
        });
  }

  public void testCandidateStatusReportsIsEmpty() throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false, true,
        dbHandle -> {
          reportBO = new ReportBOImpl(dbHandle);
          reportBO.setReportDAO(reportDAO);
          when(reportDAO.getCandidateStatusReports(candidate)).thenReturn(candidateStatusReportList);
          assert(candidateStatusReportList.isEmpty());
          verify(reportDAO.getCandidateStatusReports(candidate));
        });
  }

  public void testCandidateStatusReportsIsNotEmpty() throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false, true,
        dbHandle -> {
          reportBO = new ReportBOImpl(dbHandle);
          reportBO.setReportDAO(reportDAO);
          when(reportDAO.getCandidateStatusReports(candidate)).thenReturn(candidateStatusReportList);
          assert(!candidateStatusReportList.isEmpty());
          verify(reportDAO.getCandidateStatusReports(candidate));
        });
  }
}
