package com.auzmor.unittest;




import com.auzmor.dao.RoleDAO;
import com.auzmor.impl.bo.RoleBOImpl;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.Role;
import io.vertx.core.Vertx;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RoleBOImplTest {

  @Mock
  RoleDAO roleDAO;
  private RoleBOImpl roleBO;
  public Role role;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    role = new Role();
  }

  
  public void getRoleByName(Vertx vertx) throws  SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          String name = null;
          long organizationId = 0L;
          roleBO = new RoleBOImpl(dbHandle);
          roleBO.setRoleDAO(roleDAO);

          when(roleBO.getByName(name,organizationId)).thenReturn(role);

          assert (null==role);
          verify(roleDAO).getRoleByName(name, organizationId);

        });
  }

  
  public void notGetRoleByName(Vertx vertx) throws  SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          String name = null;
          long organizationId = 0L;
          roleBO = new RoleBOImpl(dbHandle);
          roleBO.setRoleDAO(roleDAO);

          when(roleBO.getByName(name,organizationId)).thenReturn(role);

          assert (null!=role);
          verify(roleDAO).getRoleByName(name, organizationId);

        });
  }

}
