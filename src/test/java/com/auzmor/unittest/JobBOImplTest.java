package com.auzmor.unittest;


import com.auzmor.dao.JobDAO;
import com.auzmor.impl.bo.JobBOImpl;
import com.auzmor.impl.model.Job.Collaborator;
import com.auzmor.impl.model.Job.Job;
import io.vertx.core.Vertx;
import com.auzmor.impl.helper.DbHelper;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import com.auzmor.impl.exception.db.DbHandleException;
import org.mockito.MockitoAnnotations;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class JobBOImplTest {

  @Mock
  JobDAO jobDAO;
  private JobBOImpl jobBO;
  private Job job;
  List<HashMap<String, String>> jobHiringDetails;
  List<HashMap<String, String>> candidateLogin;
  JsonObject employeeObject;
  List<Collaborator> removableCollaborators;
  boolean collaboratorExists;
  boolean jobPostExists;
  boolean jobValidationStatus;
  boolean jobCollaboratorContainsData;
  boolean isHiringLeadValid;
  int cloneJobId;
  boolean isJobValid;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    job = new Job();
  }


  public void getHiringDetails(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          int candidateId = 4;
          JsonObject dataObject = new JsonObject();
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          when(jobDAO.getJobHiringDetails(candidateId)).thenReturn(jobHiringDetails);

          dataObject = jobBO.getJobHiringDetails(candidateId);

          assert (null != dataObject);
          verify(jobDAO.getJobHiringDetails(candidateId));
        });
  }


  public void notGetHiringDetails(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          int candidateId = 0;
          JsonObject dataObject = new JsonObject();
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          when(jobDAO.getJobHiringDetails(candidateId)).thenReturn(jobHiringDetails);

          dataObject = jobBO.getJobHiringDetails(candidateId);

          assert (null == dataObject);
          verify(jobDAO.getJobHiringDetails(candidateId));
        });
  }


  public void getJobDetails(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          long jobID = 1;
          JsonObject dataObject = new JsonObject();
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          when(jobDAO.getJobDetails(jobID)).thenReturn(job);

          job = jobBO.getJobDetails(jobID);

          assert (null != job);
          verify(jobDAO.getJobDetails(jobID));
        });
  }


  public void notGetJobDetails(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          long jobID = 0;
          JsonObject dataObject = new JsonObject();
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          when(jobDAO.getJobDetails(jobID)).thenReturn(job);

          job = jobBO.getJobDetails(jobID);

          assert (null == job);
          verify(jobDAO.getJobDetails(jobID));
        });
  }


  public void getJobDetailsWithoutStatus(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          long jobID = 1;
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          when(jobDAO.getJobDetailsWithoutStatus(jobID)).thenReturn(job);

          job = jobBO.getJobDetailsWithoutStatus(jobID);

          assert (null != job);
          verify(jobDAO.getJobDetailsWithoutStatus(jobID));
        });
  }


  public void notGetJobDetailsWithoutStatus(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          long jobID = 0;
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          when(jobDAO.getJobDetailsWithoutStatus(jobID)).thenReturn(job);

          job = jobBO.getJobDetailsWithoutStatus(jobID);

          assert (null == job);
          verify(jobDAO.getJobDetailsWithoutStatus(jobID));
        });
  }


  public void getJobStatus(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          long jobID = 1;
          String status = "Published";
          long organizationId = 1;
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          JsonArray statusList = new JsonArray();
          when(jobDAO.getJobStatus(status,jobID,organizationId)).thenReturn(statusList);

          statusList = jobBO.getJobStatus(status,jobID,organizationId);

          assert (null != statusList);
          verify(jobDAO.getJobStatus(status,jobID,organizationId));
        });
  }


  public void notGetJobStatus(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          long jobID = 0;
          String status = "";
          long organizationId = 0;
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          JsonArray statusList = new JsonArray();
          when(jobDAO.getJobStatus(status,jobID,organizationId)).thenReturn(statusList);

          statusList = jobBO.getJobStatus(status,jobID,organizationId);

          assert (null == statusList);
          verify(jobDAO.getJobStatus(status,jobID,organizationId));
        });
  }


  public void getJobAvilableStatus(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          String constraintString = "Published";
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          JsonArray statusList = new JsonArray();
          when(jobDAO.getJobStatus(constraintString)).thenReturn(statusList);

          statusList = jobBO.getAvailableJobStatus(constraintString);

          assert (null != statusList);
          verify(jobDAO.getJobStatus(constraintString));
        });
  }


  public void notGetAvilableJobStatus(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          String constraintString = "";
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          JsonArray statusList = new JsonArray();
          when(jobDAO.getJobStatus(constraintString)).thenReturn(statusList);

          statusList = jobBO.getAvailableJobStatus(constraintString);

          assert (null == statusList);
          verify(jobDAO.getJobStatus(constraintString));
        });
  }


  public void getJobCount(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          long organizationId = 1;
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          when(jobDAO.getJobCount(organizationId)).thenReturn(new Long(1));

          long jobCount = jobBO.getJobCount(organizationId);

          assert (jobCount>=1);
          verify(jobDAO.getJobCount(organizationId));
        });
  }


  public void notGetJobCount(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          long organizationId = 0;
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          when(jobDAO.getJobCount(organizationId)).thenReturn(new Long(1));

          long jobCount = jobBO.getJobCount(organizationId);

          assert (jobCount ==0);
          verify(jobDAO.getJobCount(organizationId));
        });
  }



  public void getJobWithCandidateLoginId(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          long organizationId = 1;
          long candidateLoginId = 1;
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          when(jobDAO.getJobWithCandidateLoginId(candidateLoginId,organizationId)).thenReturn(candidateLogin);

          candidateLogin = jobBO.getJobWithCandidateLoginId(candidateLoginId,organizationId);

          assert (null != candidateLogin);
          verify(jobDAO.getJobWithCandidateLoginId(candidateLoginId,organizationId));
        });
  }


  public void notGetJobWithCandidateLoginId(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(vertx, false,true,
        dbHandle -> {
          long organizationId = 0;
          long candidateLoginId = 0;
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          when(jobDAO.getJobWithCandidateLoginId(candidateLoginId,organizationId)).thenReturn(candidateLogin);

          candidateLogin = jobBO.getJobWithCandidateLoginId(candidateLoginId,organizationId);

          assert (null == candidateLogin);
          verify(jobDAO.getJobWithCandidateLoginId(candidateLoginId,organizationId));
        });
  }


  public void testUpdateCollaborator(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          String collaborator = "2";
          int jobId = 1;

          when(jobDAO.isCollaboratorAvailable(collaborator, jobId)).thenReturn(collaboratorExists);
          assert(collaboratorExists == true);
          verify(jobDAO.isCollaboratorAvailable(collaborator, jobId));

          when(jobDAO.getHiringLeadName(collaborator)).thenReturn(employeeObject);
          assert(!employeeObject.isEmpty());
          verify(jobDAO.getHiringLeadName(collaborator));


          List<String> availableCollaborators = new ArrayList<String>();
          availableCollaborators.add(collaborator);

          when(jobDAO.getRemovableCollaboratorList(jobId, availableCollaborators))
              .thenReturn(removableCollaborators);
          assert (!removableCollaborators.isEmpty());

          verify(jobDAO.getRemovableCollaboratorList(jobId, availableCollaborators));

          assert(!jobDAO.getTitleBasedOnId(jobId).isEmpty());
        });
  }

  public void testUpdateCollaboratorFails(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          String collaborator = "100";
          int jobId = 1;

          when(jobDAO.isCollaboratorAvailable(collaborator, jobId)).thenReturn(collaboratorExists);
          assert(collaboratorExists == false);
          verify(jobDAO.isCollaboratorAvailable(collaborator, jobId));

          when(jobDAO.getHiringLeadName(collaborator)).thenReturn(employeeObject);
          assert(employeeObject.isEmpty());
          verify(jobDAO.getHiringLeadName(collaborator));

        });
  }

  public void testJobPostDuplication(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle ->{
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          when(jobBO.doesJobPostExists(job)).thenReturn(jobPostExists);
          assert(jobPostExists == false);
          verify(jobDAO.doesJobPostExists(job));

        });
  }

  public void testJobPostStatusValidation(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          when(jobBO.validateJobStatus(job.getId(), job.getJob_status())).thenReturn(jobValidationStatus);
          assert(jobValidationStatus == false);
        });
  }


  public void testGetJobCollaboratorsBasedOnId(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          Job job = new Job();
          job.setOrganization_id(4);
          job.setId(60);
          JsonObject jsonObject = jobBO.getCollaboratorList(job).getData();
          if(jsonObject.containsKey("data")) {
            boolean noDataValue = jsonObject.getBoolean("no_data");
            when(noDataValue).thenReturn(jobCollaboratorContainsData);
            assert(noDataValue == true);
          }

          verify(jobBO.getCollaboratorList(job).getData());

          job = new Job();
          job.setOrganization_id(3);
          job.setId(60);
          jsonObject = jobBO.getCollaboratorList(job).getData();
          boolean containsError = jsonObject.getBoolean("error");
          when(containsError).thenReturn(jobCollaboratorContainsData);
          assert(containsError == true);

          verify(jobBO.getCollaboratorList(job).getData());

          job = new Job();
          job.setOrganization_id(2);
          job.setId(10);
          jsonObject = jobBO.getCollaboratorList(job).getData();
          if(jsonObject.containsKey("data")) {
            boolean noDataValue = jsonObject.getBoolean("no_data");
            when(noDataValue).thenReturn(jobCollaboratorContainsData);
            assert(noDataValue == false);
          }

          verify(jobBO.getCollaboratorList(job).getData());

        });
  }

  public void testHiringLeadExists(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(vertx, false, true,
        dbHandle -> {
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          when(jobBO.isHiringLeadValid(job.getOrganization_id(), job.getHiring_lead()))
              .thenReturn(isHiringLeadValid);
          assert(isHiringLeadValid == false);
        });
  }

  public void testJobDetailsForEmployee(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false, true,
        dbHandle -> {
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          List<JsonObject> experienceModelList = new ArrayList<>();
          JsonObject dataObject = new JsonObject();
          dataObject.put("title", "Entry Level");
          dataObject.put("location", 2);
          dataObject.put("id", 5);
          dataObject.put("candidate_count", 2);
          dataObject.put("hiring_lead", "John Smith");
          dataObject.put("date_posted", "04/02/2018");
          dataObject.put("job_status", "Published");
          experienceModelList.add(dataObject);
          when(jobDAO.getJobDetailsForEmployee(17)).thenReturn(experienceModelList);
          verify(jobDAO.getJobDetailsForEmployee(17));
        });
  }

  public void testJobDetailsForEmployeeNoValues(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false, true,
        dbHandle -> {
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          List<JsonObject> experienceModelList = new ArrayList<>();
          JsonObject dataObject = new JsonObject();
          experienceModelList.add(dataObject);
          when(jobDAO.getJobDetailsForEmployee(17)).thenReturn(experienceModelList);
          verify(jobDAO.getJobDetailsForEmployee(17));
        });
  }

  public void testJobDetailsForEmployeeSQLException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false, true,
        dbHandle -> {
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          List<JsonObject> experienceModelList = new ArrayList<>();
          JsonObject dataObject = new JsonObject();
          experienceModelList.add(dataObject);
          when(jobDAO.getJobDetailsForEmployee(17)).thenThrow(SQLException.class);
          verify(jobDAO.getJobDetailsForEmployee(17));
        });
  }

  public void testJobDetailsForEmployeeDbHandleException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false, true,
        dbHandle -> {
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);

          when(jobDAO.getJobDetailsForEmployee(17)).thenThrow(DbHandleException.class);
          verify(jobDAO.getJobDetailsForEmployee(17));
        });
  }

  public void testCloneJobWorks(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false, true,
        dbHandle -> {
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          Job jobCloneObject = new Job();
          jobCloneObject.setId(26);
          jobCloneObject.setOrganization_id(1);
          when(jobDAO.cloneJob(jobCloneObject)).thenReturn(cloneJobId);
          assert(cloneJobId > 0);
          verify(jobDAO.cloneJob(jobCloneObject));
        });
  }

  public void testValidCloneJob(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false, true,
        dbHandle -> {
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          Job jobCloneObject = new Job();
          jobCloneObject.setId(26);
          jobCloneObject.setOrganization_id(2);
          when(jobDAO.isJobIDValid(job)).thenReturn(isJobValid);
          assert(isJobValid == false);
          verify(jobDAO.isJobIDValid(job));
        });
  }

  public void testCloneJobThrowsSqlException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false, true,
        dbHandle -> {
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          Job jobCloneObject = new Job();
          jobCloneObject.setId(26);
          jobCloneObject.setOrganization_id(1);
          when(jobDAO.cloneJob(jobCloneObject)).thenThrow(SQLException.class);
          verify(jobDAO.cloneJob(jobCloneObject));
        });
  }

  public void testCloneJobThrowsDbHandleException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false, true,
        dbHandle -> {
          jobBO = new JobBOImpl(dbHandle);
          jobBO.setJobDAO(jobDAO);
          Job jobCloneObject = new Job();
          jobCloneObject.setId(26);
          jobCloneObject.setOrganization_id(1);
          when(jobDAO.cloneJob(jobCloneObject)).thenThrow(DbHandleException.class);
          verify(jobDAO.cloneJob(jobCloneObject));
        });
  }
}
