package com.auzmor.unittest;

import com.auzmor.bo.EmployeeBO;

import com.auzmor.dao.InviteDAO;
import com.auzmor.impl.bo.EmployeeBOImpl;
import com.auzmor.impl.bo.InviteBOImpl;

import com.auzmor.impl.dao.InviteDAOImpl;
import com.auzmor.impl.enumarator.bo.InviteBOEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.Invite;
import com.auzmor.impl.model.employee.Employee;
import com.auzmor.util.bo.ReturnObject;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;

import java.util.UUID;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
public class InviteBOImplTest {

  @Mock
  InviteDAO inviteDAO;
  private InviteBOImpl inviteBO;
  ReturnObject <InviteBOEnum, Employee> returnObj;
  private Invite invite;
  String email = null;
  Long organizationId = 0L;
  MultiMap params = null;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    invite = new Invite();
  }

  
  public void inviteNewEmployeeAdded(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {

          inviteBO = new InviteBOImpl(dbHandle);
          inviteBO.setInviteDAO(inviteDAO);
          when(inviteDAO.create(invite)).thenReturn(new Integer(1));

          InviteBOEnum status = inviteBO.
              inviteNewEmployee(email,organizationId);
          assert (status == InviteBOEnum.INVITE_SUCCESS);
          verify(inviteDAO).create(invite);
        });
  }

  
  public void emailPendingForInviteNewEmployee(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {

          inviteBO = new InviteBOImpl(dbHandle);
          inviteBO.setInviteDAO(inviteDAO);
          inviteDAO = new InviteDAOImpl(dbHandle);
          when(inviteDAO.getActiveInviteForEmail(email,organizationId)).thenReturn(invite);

          InviteBOEnum status = inviteBO.
              inviteNewEmployee(email,organizationId);
          assert (status == InviteBOEnum.PENDING);
          verify(inviteDAO).create(invite);
        });
  }

  
  public void inviteEmployeeExists(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {

          EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
          when(employeeBO.isEmployeeExists(email,organizationId)).thenReturn(true);

          InviteBOEnum status = inviteBO.inviteNewEmployee(email,organizationId);
          assert (status == InviteBOEnum.EXISTS_ALREADY);
          verify(inviteDAO).create(invite);
        });
  }

  
  public void inviteSendFailure(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {

          InviteDAO inviteDAO = new InviteDAOImpl(dbHandle);
          Invite invite = inviteDAO.getActiveInviteForEmail(email,organizationId);

          when(null!=invite).thenReturn(true);

          InviteBOEnum status = inviteBO.inviteNewEmployee(email,organizationId);
          assert (status == InviteBOEnum.SEND_FAILED);
          verify(inviteDAO).create(invite);
        });
  }

  
  public void addInvitedEmployeeNoInvitePending(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          String token = params.get("token");
          inviteBO = new InviteBOImpl(dbHandle);
          String inviteKey = inviteBO.getKeyFromToken(token);
          InviteDAO inviteDAO = new InviteDAOImpl(dbHandle);
          Invite invite = inviteDAO.getInvite(inviteKey);
          when(invite==null).thenReturn(true);

          ReturnObject <InviteBOEnum, Employee> status = inviteBO.addInvitedEmployee(params);
          assert (status==returnObj);
          verify(inviteDAO).getInvite(inviteKey);
        });
  }


  
  public void addInvitedEmployeeExistsAlready(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {

          String token = params.get("token");
          inviteBO = new InviteBOImpl(dbHandle);
          String inviteKey = inviteBO.getKeyFromToken(token);
          InviteDAO inviteDAO = new InviteDAOImpl(dbHandle);
          Invite invite = inviteDAO.getInvite(inviteKey);
          EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
          when(employeeBO.isEmployeeExists(email,organizationId)).thenReturn(true);

          ReturnObject <InviteBOEnum, Employee> status = inviteBO.addInvitedEmployee(params);
          assert (status==returnObj);
          verify(inviteDAO).getInvite(inviteKey);
        });
  }

  
  public void addInvitedEmployeeAdded(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {

          String token = params.get("token");
          inviteBO = new InviteBOImpl(dbHandle);
          String inviteKey = inviteBO.getKeyFromToken(token);
          InviteDAO inviteDAO = new InviteDAOImpl(dbHandle);
          when(inviteDAO.updateInviteAsAccepted(inviteKey)).thenReturn(new Integer(1));

          ReturnObject <InviteBOEnum, Employee> insertCount = inviteBO.addInvitedEmployee(params);
          assert (insertCount==returnObj);
          verify(inviteDAO).getInvite(inviteKey);
        });
  }

  
  public void invitedEmployeeNotAdded(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {

          String token = params.get("token");
          inviteBO = new InviteBOImpl(dbHandle);
          String inviteKey = inviteBO.getKeyFromToken(token);
          InviteDAO inviteDAO = new InviteDAOImpl(dbHandle);
          when(inviteDAO.updateInviteAsAccepted(inviteKey)).thenReturn(new Integer(0));

          ReturnObject <InviteBOEnum, Employee> insertCount = inviteBO.addInvitedEmployee(params);
          assert (insertCount==returnObj);
          verify(inviteDAO).getInvite(inviteKey);
        });
  }

  
  public void inviteResendEmployeeExists(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          inviteBO = new InviteBOImpl(dbHandle);
          inviteBO.setInviteDAO(inviteDAO);
          EmployeeBO employeeBO = new EmployeeBOImpl(dbHandle);
          when(employeeBO.isEmployeeExists(email, organizationId)).thenReturn(true);

          InviteBOEnum status = inviteBO.
              inviteResend(email, organizationId);
          assert (status == InviteBOEnum.EXISTS_ALREADY);
          verify(inviteDAO).getActiveInviteForEmail(email, organizationId);
        });
  }

    
    public void inviteResendNoInvitePending(Vertx vertx) throws SQLException, DbHandleException {
      DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
          dbHandle -> {
            inviteBO = new InviteBOImpl(dbHandle);
            InviteDAO inviteDAO = new InviteDAOImpl(dbHandle);
            Invite invite = inviteDAO.getActiveInviteForEmail(email, organizationId);

            when(null == invite).thenReturn(true);

            InviteBOEnum status = inviteBO.
                inviteResend(email,organizationId);
            assert (status == InviteBOEnum.NO_INVITE_PENDING);
            verify(inviteDAO).getActiveInviteForEmail(email,organizationId);
          });
  }



public void cancelInviteNoInvitePending(Vertx vertx) throws SQLException, DbHandleException {
  DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
      dbHandle -> {
        inviteBO = new InviteBOImpl(dbHandle);
        inviteBO.setInviteDAO(inviteDAO);
        InviteDAO inviteDAO = new InviteDAOImpl(dbHandle);
        Invite invite = inviteDAO.getActiveInviteForEmail(email, organizationId);
        when(null==invite).thenReturn(true);

        InviteBOEnum status = inviteBO.
            inviteResend(email, organizationId);
        assert (status == InviteBOEnum.NO_INVITE_PENDING);
        verify(inviteDAO).delete(invite);
      });
}

  
  public void cancelInviteCancelSuccess(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          inviteBO = new InviteBOImpl(dbHandle);
          inviteBO.setInviteDAO(inviteDAO);
          InviteDAO inviteDAO = new InviteDAOImpl(dbHandle);
          Invite invite = inviteDAO.getActiveInviteForEmail(email, organizationId);
          when(null!=invite).thenReturn(false);

          InviteBOEnum status = inviteBO.
              inviteResend(email, organizationId);
          assert (status == InviteBOEnum.CANCEL_SUCCESS);
          verify(inviteDAO).delete(invite);
        });
  }

  
  public void addInvite(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle ->{
          inviteBO = new InviteBOImpl(dbHandle);
          inviteBO.setInviteDAO(inviteDAO);
          invite = new Invite();
          invite.setEmail(email);
          invite.setOrganizationId(organizationId);
          invite.setKey(UUID.randomUUID().toString());

          when(inviteBO.addInvite(email,organizationId)).thenReturn(invite);

          assert (null == invite);
          verify(inviteDAO.create(invite));
        });
  }

  
  public void hasInviteForMailAvilable(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle ->{
          inviteBO = new InviteBOImpl(dbHandle);
          inviteBO.setInviteDAO(inviteDAO);
          when(inviteBO.hasInviteForEmail(email,organizationId)).thenReturn(true);

          assert (true);
          verify(inviteDAO.hasInviteForEmail(email,organizationId));
        });
  }

  
  public void hasInviteForMailNotAvilable(Vertx vertx) throws SQLException, DbHandleException{
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle ->{
          inviteBO = new InviteBOImpl(dbHandle);
          inviteBO.setInviteDAO(inviteDAO);
          when(inviteBO.hasInviteForEmail(email,organizationId)).thenReturn(false);

          assert (false);
          verify(inviteDAO.hasInviteForEmail(email,organizationId));
        });
  }
}
