package com.auzmor.unittest;

import com.auzmor.bo.LocationBO;
import com.auzmor.dao.LocationDAO;
import com.auzmor.impl.bo.LocationBOImpl;
import com.auzmor.impl.dao.LocationDAOImpl;
import com.auzmor.impl.enumarator.LocationEnum;
import com.auzmor.impl.exception.db.DbHandleException;
import com.auzmor.impl.helper.DbHelper;
import com.auzmor.impl.model.LocationModel;
import com.auzmor.util.bo.ReturnObject;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LocationBOImplTest {
  @Mock
  LocationDAO locationDAO;
  private LocationBO locationBO;
  private LocationModel locationModel;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    locationModel = new LocationModel();
  }

  public void createLocation(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          locationBO = new LocationBOImpl();
          locationDAO = new LocationDAOImpl(dbHandle);
          when(locationDAO.insert(locationModel)).thenReturn(new Integer(1));

          ReturnObject<LocationEnum, JsonObject> boReponse =
              locationBO.addLocation(dbHandle, locationModel);

          assert (boReponse.getStatusEnum() == LocationEnum.ADDITION_SUCCESS);
          verify(locationDAO).insert(locationModel);
        }
    );
  }

  
  public void createLocationNotSuccessful(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          locationBO = new LocationBOImpl();
          locationDAO = new LocationDAOImpl(dbHandle);
          when(locationDAO.insert(locationModel)).thenReturn(new Integer(0));

          ReturnObject<LocationEnum, JsonObject> boReponse =
              locationBO.addLocation(dbHandle, locationModel);

          assert (boReponse.getStatusEnum() == LocationEnum.ADDITION_SUCCESS);
          verify(locationDAO).insert(locationModel);
        }
    );
  }

  
  public void createLocationThrowsException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          locationBO = new LocationBOImpl();
          locationDAO = new LocationDAOImpl(dbHandle);
          when(locationDAO.insert(locationModel)).thenThrow(SQLException.class);

          ReturnObject<LocationEnum, JsonObject> boReponse =
              locationBO.addLocation(dbHandle, locationModel);
        }
    );
  }

  
  public void getAllLocations(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          locationBO = new LocationBOImpl();
          locationDAO = new LocationDAOImpl(dbHandle);
          List<LocationModel> locationList = new ArrayList<>();
          when(locationDAO.getAllLocations(locationModel)).thenReturn(locationList);

          ReturnObject<LocationEnum, JsonArray> boReponse =
              locationBO.getAllLocations(dbHandle, locationModel);

          assert (boReponse.getStatusEnum() == LocationEnum.VALUE_SUCCESS);
          verify(locationDAO).getAllLocations(locationModel);
        }
    );
  }

  
  public void getAllLocationsNoValues(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          locationBO = new LocationBOImpl();
          locationDAO = new LocationDAOImpl(dbHandle);
          List<LocationModel> locationList = new ArrayList<>();
          when(locationDAO.getAllLocations(locationModel)).thenReturn(null);

          ReturnObject<LocationEnum, JsonArray> boReponse =
              locationBO.getAllLocations(dbHandle, locationModel);

          assert (boReponse.getStatusEnum() == LocationEnum.NO_SUCH_VALUE);
          verify(locationDAO).getAllLocations(locationModel);
        }
    );
  }

  
  public void getAllLocationsThrowsException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          locationBO = new LocationBOImpl();
          locationDAO = new LocationDAOImpl(dbHandle);
          List<LocationModel> divisionList = new ArrayList<>();
          when(locationDAO.getAllLocations(locationModel)).thenThrow(SQLException.class);

          ReturnObject<LocationEnum, JsonArray> boReponse =
              locationBO.getAllLocations(dbHandle, locationModel);
        }
    );
  }

  
  public void getLocation(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          locationBO = new LocationBOImpl();
          locationDAO = new LocationDAOImpl(dbHandle);
          when(locationDAO.getLocationDetails(locationModel)).thenReturn(locationModel);

          ReturnObject<LocationEnum, JsonArray> boReponse =
              locationBO.getAllLocations(dbHandle, locationModel);

          assert (boReponse.getStatusEnum() == LocationEnum.VALUE_SUCCESS);
          verify(locationDAO).getLocationDetails(locationModel);
        }
    );
  }

  
  public void getLocationNoValues(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          locationBO = new LocationBOImpl();
          locationDAO = new LocationDAOImpl(dbHandle);
          when(locationDAO.getLocationDetails(locationModel)).thenReturn(null);

          ReturnObject<LocationEnum, JsonObject> boReponse =
              locationBO.getLocation(dbHandle, locationModel);

          assert (boReponse.getStatusEnum() == LocationEnum.NO_SUCH_VALUE);
          verify(locationDAO).getLocationDetails(locationModel);
        }
    );
  }

  
  public void getLocationThrowsException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          locationBO = new LocationBOImpl();
          locationDAO = new LocationDAOImpl(dbHandle);
          when(locationDAO.getLocationDetails(locationModel)).thenThrow(SQLException.class);

          ReturnObject<LocationEnum, JsonObject> boReponse =
              locationBO.getLocation(dbHandle, locationModel);
        }
    );
  }

  
  public void updateLocation(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          locationBO = new LocationBOImpl();
          locationDAO = new LocationDAOImpl(dbHandle);
          MultiMap params = null;
          when(locationDAO.updateLocationDetails(params)).thenReturn(new Integer(1));

          ReturnObject<LocationEnum, JsonObject> boReponse =
              locationBO.updateLocation(dbHandle, params);

          assert (boReponse.getStatusEnum() == LocationEnum.UPDATE_SUCCESS);
          verify(locationDAO).updateLocationDetails(params);
        }
    );
  }

  
  public void updateLocationNoValues(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          locationBO = new LocationBOImpl();
          locationDAO = new LocationDAOImpl(dbHandle);
          MultiMap params = null;
          when(locationDAO.updateLocationDetails(params)).thenReturn(new Integer(0));

          ReturnObject<LocationEnum, JsonObject> boReponse =
              locationBO.updateLocation(dbHandle, params);

          assert (boReponse.getStatusEnum() == LocationEnum.NO_SUCH_VALUE);
          verify(locationDAO).updateLocationDetails(params);
        }
    );
  }

  
  public void updateLocationThrowsException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          locationBO = new LocationBOImpl();
          locationDAO = new LocationDAOImpl(dbHandle);
          MultiMap params = null;
          when(locationDAO.updateLocationDetails(params)).thenThrow(SQLException.class);

          ReturnObject<LocationEnum, JsonObject> boReponse =
              locationBO.updateLocation(dbHandle, params);
        }
    );
  }

  
  public void deleteLocationThrowsException(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          locationBO = new LocationBOImpl();
          locationDAO = new LocationDAOImpl(dbHandle);
          when(locationDAO.deleteLocation(locationModel)).thenThrow(SQLException.class);

          ReturnObject<LocationEnum, JsonObject> boReponse =
              locationBO.deleteLocation(dbHandle, locationModel);
        }
    );
  }

  
  public void deleteLocation(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          locationBO = new LocationBOImpl();
          locationDAO = new LocationDAOImpl(dbHandle);
          when(locationDAO.deleteLocation(locationModel)).thenReturn(new Integer(1));

          ReturnObject<LocationEnum, JsonObject> boReponse =
              locationBO.deleteLocation(dbHandle, locationModel);

          assert (boReponse.getStatusEnum() == LocationEnum.DELETE_SUCCESS);
          verify(locationDAO).deleteLocation(locationModel);
        }
    );
  }

  
  public void deleteLocationNoValues(Vertx vertx) throws SQLException, DbHandleException {
    DbHelper.safeExecuteDbHandleMethod(Vertx.vertx(), false,true,
        dbHandle -> {
          locationBO = new LocationBOImpl();
          locationDAO = new LocationDAOImpl(dbHandle);
          when(locationDAO.deleteLocation(locationModel)).thenReturn(new Integer(0));

          ReturnObject<LocationEnum, JsonObject> boReponse =
              locationBO.deleteLocation(dbHandle, locationModel);

          assert (boReponse.getStatusEnum() == LocationEnum.NO_SUCH_VALUE);
          verify(locationDAO).deleteLocation(locationModel);
        }
    );
  }
}
