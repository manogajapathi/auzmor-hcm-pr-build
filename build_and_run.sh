#!/usr/bin/env bash
mvn clean package -DskipTests;
nohup java -jar target/auzmor-1.0-SNAPSHOT-fat.jar > ./log.out &
PID=$!
echo ${PID} > pid.txt
